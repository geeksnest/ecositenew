'use strict';

/* Controllers */

app.controller('eventscollectionCtrl', function ($http, $modal, $state, $stateParams, $scope, $parse, $location, $anchorScroll, $timeout) {

  console.log("events");
  console.log($stateParams.eventID);
  $scope.data = {};
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  var num = 10;
  var off = 1;
  var keyword = null;
  
  var sort = 'id';
  var sortto = 'ASC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;
  
  var eventID = $stateParams.eventID

  $scope.updatedamount = false;

  Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
      total = total + parseInt(this[i][prop])
    }
    return total
  }
  var putcomma = function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

  var paginate = function (off, keyword) {
    $http({
      url: API_URL + "/events/eventsCollection/" + num + '/' + off + '/' + keyword + '/' + eventID + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data) {
      $scope.title = data.title;
      $scope.totals = data.totals;
      $scope.collections = putcomma(data.collections.sum('amount'));
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data) {
      
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.refresh = function(searchtext,year,month,day) {

    num = 10;
    off = 1;
    keyword = null;
    searchtext = null;
    $scope.searchtext = [];
    year = null;
    month = null;
    day = null;
    paginate(off, keyword);
    if($scope.timestamp.year == undefined){
      $scope.timestamp.year = null;
    }else{
      $scope.timestamp.year = [];
    }
    if($scope.timestamp.month == undefined){
      $scope.timestamp.month = null;
    }else{
      $scope.timestamp.month = [];
    }
    if($scope.timestamp.day == undefined){
      $scope.timestamp.day = null;
    }else{
      $scope.timestamp.day = [];
    }
  } 

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword, timestamp, advancesearch) {
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword, timestamp, advancesearch) {
    var off = pageNo;
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  };


  $scope.searchtimestamp = function (timestamp,advancesearch) {

    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{
        var keyword = year + '-' + month + '-' + day;
      }

      console.log(keyword);
      paginate(off, keyword);
    }
  }


  $scope.delete = function(transactionId){
    $scope.transactionId
    var modalInstance = $modal.open({
      templateUrl: 'deleteEventCollectionModal.html',
      controller: deleteEventCollectionInstanceCTRL,
      resolve: {
        transactionId: function () {
          return transactionId
        }
      }
    });
  }
  var deleteEventCollectionInstanceCTRL = function ($scope, $modalInstance, transactionId, $state) {
    $scope.transactionId = transactionId;
    $scope.ok = function (transactionId) {
      $http({
        url: API_URL + "/events/deleteEventCollection/" + transactionId,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        alertclose();
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
  $scope.alerts = [];
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }


})