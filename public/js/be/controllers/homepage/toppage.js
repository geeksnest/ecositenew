'use strict';

/* Controllers */

app.controller('ToppageCtrl', function($scope, $http, $modal,Upload,Config ){
    console.log("--Toppage--");
    $scope.userscount = 123;
     var html = $scope.userscount;


    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $http({
        url: API_URL + "/toppage/show1",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
         $scope.msg1 = data;
    });

    $http({
        url: API_URL + "/toppage/show2",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        $scope.msg2 = data;
    });
    var cked;


    var cked1;
    $scope.Save1 = function(content){
        cked1 = CKEDITOR.instances['myeditor1'].getData();
        content['content'] = cked1;
         $http({
            url: API_URL +"/toppage/savedata1",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(content)
        }).success(function (data, status, headers, config) {
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Top page Box2 Updated!' });
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }
    var cked2;
    $scope.Save2 = function(content){
     cked2 = CKEDITOR.instances['myeditor2'].getData();
     content['content'] = cked2;
         $http({
            url: API_URL +"/toppage/savedata2",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(content)
        }).success(function (data, status, headers, config) {
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Top page Box3 Updated!' });
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }


$scope.projImg = [];
for (var i = 1; i <= 3; i++) {
    $scope.projImg.push({image:'',files:[],title:'',amount:'',desc:''});
};


$http({
    url: API_URL + "/toppage/show",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
}).success(function (data, status, headers, config) {
   $scope.msg = data;
   $scope.projImg[0].image = data.image0;
   $scope.projImg[1].image = data.image1;
   $scope.projImg[2].image = data.image2;

   $scope.projImg[0].title = data.title0;
   $scope.projImg[1].title = data.title1;
   $scope.projImg[2].title = data.title2;

   $scope.projImg[0].desc = data.desc0;
   $scope.projImg[1].desc = data.desc1;
   $scope.projImg[2].desc = data.desc2;

   $scope.projImg[0].amount = data.count0;
   $scope.projImg[1].amount = data.count1;
   $scope.projImg[2].amount = data.count2;


});
$scope.imageselected = {}
$scope.imageselected[0] = false;
$scope.imageselected[1] = false;
$scope.imageselected[2] = false;
var tracker = [];
$scope.closeimgAlerts = function (index) {
    $scope.imgAlerts.splice(index, 1);
};
     $scope.imgAlerts = [];
    $scope.prepare = function(num,file) {
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.num = num;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
      } else {
        // console.log($scope.p[0].image);
        $scope.projImg[num].files = file;
        tracker.push(file);
        console.log($scope.projImg[num]);
        $scope.imgAlerts.splice(0, 1);
        $scope.imageselected[num] = true;
      }
    }
  }
    $scope.flag = 1;
    var alldata;
    $scope.Save = function(data,files){
       /* console.log(data);
        console.log(tracker[0][0].name);
        console.log(tracker[1][0].name);
        console.log(tracker[2][0].name);
        */
        $scope.flag = 1;
        console.log($scope.projImg);
        console.log($scope.projImg[0].files[0]);


        var image0,image1,image2;
        if($scope.projImg[0].files[0] == undefined){
            image0 = $scope.projImg[0].image;
        }else{
            image0 = $scope.projImg[0].files[0]['name'];
        }
        if($scope.projImg[1].files[0] == undefined){
            image1 = $scope.projImg[1].image
        }else{
            image1 = $scope.projImg[1].files[0]['name'];
        }
        if($scope.projImg[2].files[0] == undefined){
            image2 = $scope.projImg[2].image
        }else{
            image2 = $scope.projImg[2].files[0]['name'];
        }

        alldata = {
            title:data.title,
            bgcolor:data.bgcolor,
            title0:$scope.projImg[0].title,
            desc0:$scope.projImg[0].desc,
            count0:$scope.projImg[0].amount,
            image0:image0,
            title1:$scope.projImg[1].title,
            desc1:$scope.projImg[1].desc,
            count1:$scope.projImg[1].amount,
            image1:image1,
            title2:$scope.projImg[2].title,
            desc2:$scope.projImg[2].desc,
            count2:$scope.projImg[2].amount,
            image2:image2
        };

        if(tracker.length != 0){
           upload(tracker); 
        }else{
            $scope.flag = 2;
        }
        
        $scope.$watch('flag', function(val) {
            if(val == 2){
                $http({
                    url: API_URL +"/toppage/savedata",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:$.param(alldata)
                }).success(function (data, status, headers, config) {
                 alldata = undefined;
                 $scope.alerts.splice(0, 1);
                 $scope.alerts.push({ type: 'success', msg: 'Top page Box1 Updated!' });
                 document.body.scrollTop = document.documentElement.scrollTop = 0;
                });
            }
            $scope.flag = 1;
        }); 
        // cked = CKEDITOR.instances['myeditor'].getData();
        // content['content'] = cked;
        // $http({
        //     url: API_URL +"/toppage/savedata",
        //     method: "POST",
        //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        //     data:$.param(content)
        // }).success(function (data, status, headers, config) {
        //     $scope.alerts.splice(0, 1);
        //     $scope.alerts.push({ type: 'success', msg: 'Top page Box1 Updated!' });
        // });
    }
    $scope.imageloader=false;
    $scope.imagecontent=true;

    var upload = function(files)
    {
        var filecount = 0;
        if (files && files.length)
        {
          $scope.imageloader=true;
          $scope.imagecontent=false;

          for (var i = 0; i < files.length; i++)
          {
            var file = files[i][0];
            var promises;

            promises = Upload.upload({

                url: Config.amazonlink,
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                  var headers = headersGetter();
                  delete headers['Authorization'];
                  return data;
              },
              fields : {
                  key: 'uploads/toppage/' + file.name,
                  AWSAccessKeyId: Config.AWSAccessKeyId,
                  acl: 'private',
                  policy: Config.policy,
                  signature: Config.signature,
                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
          })
            promises.then(function(data){
                $scope.imageloader=false;
                $scope.imagecontent=true;
            });
          }
          $scope.flag = 2;
        }
    };
});
app.directive('onlyDigits', function () {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
      if (!ngModel) return;
      ngModel.$parsers.unshift(function (inputValue) {
        var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
        ngModel.$viewValue = digits;
        ngModel.$render();
        return digits;
      });
    }
  };
});