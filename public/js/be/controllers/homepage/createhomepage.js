'use strict';

/* Controllers */

app.controller('CreatehomepageCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload){
  $scope.isSaving = false;

  $scope.imageloader=false;
  $scope.imagecontent=true;

  $scope.page = {};
  $scope.sidebar = [];
  $scope.page.subpagecolor = '#ffffff';
  $scope.master = {};
  var orig = angular.copy($scope.master);

  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/pageimages/';
    $scope.page.banner = Text.substring(texttocut.length);
  }
  var cked;
  $scope.savePage = function(page){
     $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    cked = CKEDITOR.instances['myeditor'].getData();
    page['content'] = cked;
    if ($scope.sidebar.img) {
       console.log("error")
       $scope.addme = true;
       if ($scope.addbtn == false) {
        $scope.notvalid = 'Images not yet updated. Please finish your update';
       }else{
         $scope.notvalid = 'Please fill up required fields and click add';
       }
       window.scrollTo(0, $("#addme").offset().top);
    }
    else{
         $scope.addme = false;
         if(page.title != 'NEWS')
         {
           $scope.isSaving = true;
           $http({
            url: API_URL + "/homepage/create",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
          }).success(function (data, status, headers, config) {
            console.log(data)
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'Content successfully saved!'});
            $scope.reset();
            $scope.formPage.$setPristine(true);
            $scope.page.image= [];
            document.body.scrollTop = document.documentElement.scrollTop = 0;
          }).error(function (data, status, headers, config) {
            $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
          });
        }else{
          $scope.alerts.push({type: 'danger', msg: 'Please change the Title!'});
        }
    }
    
  }

  $http({
    url: API_URL + "/homepage/listsubmenu",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {
    $scope.submenulink = data.data;
  });




  var loadImages = function(){
    $http({
      url: API_URL + "/pages/listpageimages",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
      console.log(data);
      $scope.data = data;
    }).error(function (data) {
      $scope.status = status;
    });
  }
  loadImages();

  $scope.$watch('files', function () {
    $scope.upload($scope.files);

  });



  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.upload = function (files)
  {
    $scope.alertss = [];

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }
        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/pageimages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : filename
            };
            $http({
              url: API_URL + "/pages/ajaxfileuploader",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });

        }
      }
    }
  };

  $scope.deletepageimg = function(pageid) {

    var modalInstance = $modal.open({
      templateUrl: 'deletepageimgModal.html',
      controller: deletepageimgCTRL,
      resolve: {
        pageid: function() {
          return pageid;
        }
      }
    });
  }

  var deletepageimgCTRL = function($scope, $modalInstance, pageid) {
    $scope.pageid = pageid;
    $scope.ok = function(pageid) {
      $http({
        url: API_URL + "/pages/dltpagephoto",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(pageid)
      }).success(function (data, status, headers, config) {

        loadImages();
        $modalInstance.dismiss('cancel');
      }).error(function (data, status, headers, config) {
        data = data;
      });


    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.reset = function () {
    $scope.page.content = '';
    $scope.formPage.$setPristine(true);
    $scope.page = angular.copy(orig);
  };


  $scope.dltPhoto = function (dlt){

    $('.buttonPanel'+dlt.id).hide();
    $http({
      url: API_URL + "/pages/dltpagephoto",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(dlt)
    }).success(function (data, status, headers, config) {
      loadImages();
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };

  $scope.banners = function convertToSlug(Text)
  {
    $scope.page.banners = Text;
  }

//images

$scope.page.image = [];
$scope.addbtn = true;
$scope.addimage = function(sidebar){
        $scope.page.image.push({imglink:sidebar.imglink,img:sidebar.img,title:sidebar.title,
          desc:sidebar.desc});
        $scope.sidebar = [];
        $scope.total = $scope.page.image.length;
        $scope.addme = false;
    }
    $scope.deleteleft = function(index){
        $scope.page.image.splice(index, 1);
        $scope.addme = false;
        $scope.total = $scope.page.image.length;
    }

    $scope.editleft = function(index){
        $scope.currentEdit = index;
        $scope.addbtn = false;
        $scope.sidebar.imglink = $scope.page.image[index].imglink;
        $scope.sidebar.title =  $scope.page.image[index].title;
        $scope.sidebar.desc =  $scope.page.image[index].desc;
        $scope.sidebar.img =  $scope.page.image[index].img;
        $scope.currentEdit = index;
        $scope.addbtn = false;
        setTimeout(function(){
          $("#left").focus();
        }, 0);
        $scope.addme = false;
    }
    $scope.updateleftsidebar = function(index,sidebar){
        
      $scope.page.image[index].imglink = sidebar.imglink;
      $scope.page.image[index].title = sidebar.title;
      $scope.page.image[index].desc = sidebar.desc;
      $scope.page.image[index].img = sidebar.img;  
      $scope.currentEdit = 99999;
      $scope.addbtn = true;
      $scope.sidebar = [];
      $scope.addme = false;
    }

    $scope.leftCancel = function(){
        $scope.currentEdit = 99999;
        $scope.addbtn = true;
        $scope.sidebar = [];
        $scope.addme = false
    }
    $scope.cancelprep = function(){
        $scope.sidebar = [];
        $scope.addme = false
    }

$scope.media = function(type, thumb,sidebar){
  var modalInstance = $modal.open({
    templateUrl: 'mediagallery.html',
    controller: function($scope, $modalInstance,Config, type) {
      $scope.imageloader=false;
      $scope.directory = 'pageimages';
      $scope.videoenabled=false;
      $scope.mediaGallery = 'images';
      $scope.imagecontent=true;
      $scope.invalidvideo = false;
      $scope.currentSelected = '';
      $scope.currentDeleting = '';
      $scope.type = type;
      $scope.s3link = Config.amazonlink;
                // console.log($scope.s3link);

                $scope.set = function(id){
                  $scope.currentSelected = id;
                  $scope.contentvideo = id;
                    // console.log(id)
                  }

                  $scope.returnImageThumb = function(){
                    return function (item) {
                      if(item){
                        return Config.amazonlink + "/uploads/" + $scope.directory + "/" + item;
                      }else{
                        return item;
                      }
                    };
                  }

                  var loadImages = function(){
                    $http({
                      url: API_URL + "/pages/listpageimages",
                      method: "GET",
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                    $scope.imagelength = data.length;
                    $scope.data = data;
                  }).error(function (data) {
                    $scope.status = status;
                  });
                }
                loadImages();

                
                $scope.upload = function (files)
                {
                  $scope.alertss = [];

                  var filename
                  var filecount = 0;
                  if (files && files.length)
                  {
                    $scope.imageloader=true;
                    $scope.imagecontent=false;

                    for (var i = 0; i < files.length; i++)
                    {
                      var file = files[i];

                      if (file.size >= 2000000)
                      {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;

                        if(filecount == files.length)
                        {
                          $scope.imageloader=false;
                          $scope.imagecontent=true;
                        }
                      }
                      else

                      {
                        var promises;

                        promises = Upload.upload({

                          url: Config.amazonlink,
                          method: 'POST',
                          transformRequest: function (data, headersGetter) {
                            var headers = headersGetter();
                            delete headers['Authorization'];
                            return data;
                          },
                          fields : {
                            key: 'uploads/pageimages/' + file.name,
                            AWSAccessKeyId: Config.AWSAccessKeyId,
                            acl: 'private',
                            policy: Config.policy,
                            signature: Config.signature,
                            "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
                          },
                          file: file
                        })
                        promises.then(function(data){

                          filecount = filecount + 1;
                          filename = data.config.file.name;
                          var fileout = {
                            'imgfilename' : filename
                          };
                          $http({
                            url: API_URL + "/pages/ajaxfileuploader",
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param(fileout)
                          }).success(function (data, status, headers, config) {
                            loadImages();
                            if(filecount == files.length)
                            {
                              $scope.imageloader=false;
                              $scope.imagecontent=true;
                            }

                          }).error(function (data, status, headers, config) {
                            $scope.imageloader=false;
                            $scope.imagecontent=true;
                          });

                        });

     }
    }
  }
};

$scope.closeAlerts = function (index) {
        $scope.alertss.splice(index, 1);
    };
$scope.deletenewsimg = function (dataimg, $event)
{
    var fileout = {
        'imgfilename' : dataimg
    };
    Special.deleteimage(fileout, function(data) {
        loadimages();
    });
    $event.stopPropagation();
}

$scope.ok = function() {
    $modalInstance.close($scope.contentvideo);
}

$scope.cancel = function() {
    $modalInstance.dismiss('cancel');
}
},
resolve: {
    type: function(){
        return type;
    }
},
windowClass: 'xxx-modal-window'
}).result.then(function(res) {
    console.log("============================");
    if(res.filename) {
        $scope.sidebar.img = res.filename;
    }
});
}


})