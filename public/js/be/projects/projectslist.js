'use strict';

/* Controllers */

app.controller('projectslistCtrl', function ($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout, $sce, $q, Config, Upload, $filter) {

  console.log("Projects");
  $scope.data = {};

  $scope.bannerFolder = 'projImages';
  var loadBanner = function () {
    $http({
      url: API_URL+"/projects/banner",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.dataBanner = data[0];
      // console.log(data[0]);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadBanner();

  $scope.editimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'editNewsBanner.html',
      controller: editImageCTRL,
      windowClass: 'app-modal-window',

      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }


  $scope.toview = function(slugs){
    console.log(slugs);
    window.open('/projects/' + slugs, '_blank');
  }

  var editImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.isLoading = false;
    $scope.imgID = imgID;
    $scope.imgtype = 'banner';
    $scope.bannerFolder = 'projImages';
    $scope.amazonlink = Config.amazonlink;
    $scope.tfsize = [];
    $scope.dfsize = [];
    $scope.imageselected = false;
    $scope.bigImages = false;
    $scope.changepic = false;

    for (var x = 30; x <= 45; x++) {
      $scope.tfsize.push({'val': x})
    }

    for (var y = 12; y <= 20; y++) {
      $scope.dfsize.push({'val': y})
    }

    var loadBannerIMG = function () {
      $http({
        url: API_URL+"/projects/banner",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0];
        // console.log(data);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadBannerIMG();

    $scope.save = function (imgdata,banner) {

      if($scope.changepic == true){
        console.log('initializing..');
        console.log('checking image size..');
        console.log(banner);
        var file = banner[0];
        if (file && file.length) {
          if (file.size >= 2000000) {
            console.log('image size too large');
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            cope.imagecontent=true;
          }
        }
        else{
          console.log('uploading image..');
          $scope.isLoading = true;
          var promises;
          promises = Upload.upload({
            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/projImages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){
            console.log('saving data..');
            imgdata['img'] = data.config.file.name;
            $http({
              url: API_URL + "/banner/bannerupdateImage",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imgdata)
            }).success(function (data, status, headers, config) {
              $modalInstance.dismiss('cancel');      
              $scope.isLoading = false;
              loadBanner();
            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });
          });
        }
      }else{

        console.log(imgdata);
        $scope.isLoading = true;

        $http({
          url: API_URL + "/banner/bannerupdateImage",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(imgdata)
        }).success(function (data, status, headers, config) {
          $modalInstance.dismiss('cancel');      
          $scope.isLoading = false;
          loadBanner();
        }).error(function (data, status, headers, config) {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        });

      }

      

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.prepare = function(file) {

      if (file && file.length) {
        if (file[0].size >= 2000000) {
          console.log('File is too big!');          
          $scope.imgbanner = [];
          $scope.changepic = false;
          $scope.bigImages = true;
        } else {
          console.log("below maximum");
          $scope.imgbanner = file;
          $scope.imgname = file[0].name;
          $scope.imageselected = true;
          $scope.changepic = true;
          $scope.bigImages = false;
          console.log(file);
        }
      }
    }
  };

  var num = 10;
  var off = 1;
  var keyword = null;
  
  var sort = 'date_created';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword) {
    $scope.loadingdata = true;

    if (keyword == ''){ keyword == null; }
    
    $http({
      url: API_URL+"/projects/list/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      for(var key in data.data){
        var n = {
          projStatus: data.data[key]['projStatus'],
          projTitle: data.data[key]['projTitle'],
          firstname: data.data[key]['firstname'],
          lastname: data.data[key]['lastname'],
          projLoc: data.data[key]['projLoc'],
          projID: data.data[key]['projID'],
          projSlugs: data.data[key]['projSlugs'],
          date_created: moment(data.data[key]['date_created']).format('MMM DD, YYYY')
        }
                data.data[key] = n;
  }
        
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.refresh = function(searchtext,year,month,day) {

    num = 10;
    off = 1;
    keyword = null;
    searchtext = null;
    $scope.searchtext = [];
    year = null;
    month = null;
    day = null;
    paginate(off, keyword);
    if($scope.timestamp.year == undefined){
      $scope.timestamp.year = null;
    }else{
      $scope.timestamp.year = [];
    }
    if($scope.timestamp.month == undefined){
      $scope.timestamp.month = null;
    }else{
      $scope.timestamp.month = [];
    }
    if($scope.timestamp.day == undefined){
      $scope.timestamp.day = null;
    }else{
      $scope.timestamp.day = [];
    }
  } 

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword, timestamp, advancesearch) {
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword, timestamp, advancesearch) {
    var off = pageNo;
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  };


  $scope.searchtimestamp = function (timestamp,advancesearch) {

    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{
        var keyword = year + '-' + month + '-' + day;
      }

      console.log(keyword);
      paginate(off, keyword);
    }
  }



  // $scope.review = function(projID){
  //   $scope.randNumber = Math.floor((Math.random()*6)+1);
  // }

  $scope.review = function(projID){
    $scope.projID = projID;
    var modalInstance = $modal.open({
      templateUrl: 'reviewProj.html',
      controller: reviewProjCTRL,
      windowClass: 'app-modal-window',
      resolve: {
        projID: function () {
          return projID
        }
      }
    });
  }

  var reviewProjCTRL = function ($scope, $modalInstance, projID, $state) {
    $scope.projID = projID;
    $scope.amazonlink = Config.amazonlink;

    var loadProj = function(){
      $http({
        url: API_URL+"/project/review/" + projID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        console.log(data);
        $scope.data = data.data[0];
        $scope.daystogo = parseInt(data.data[0].countStart);
        if(data.donations[0].total != null){
          $scope.projdon = data.donations[0].total;
        }else{
          $scope.projdon = '0';
        }
        $scope.projdonors = data.donations[0].donors;
        var dur = data.data[0].projDuration.toString().split(" ");
        $scope.duration = dur[1];
        $scope.projDesc = $sce.trustAsHtml(data.data[0].projDesc);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadProj();

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };    

    $scope.resolution = function(projID, projStatus){
      $scope.projID = projID;
      $scope.projStatus = projStatus;
      var modalInstance = $modal.open({
        templateUrl: 'appDisappProj.html',
        controller: appDisappProjCTRL,
        resolve: {
          projID: function () {return projID},
          projStatus: function () {return projStatus}
        }
      });
    }
    var appDisappProjCTRL = function ($scope, $modalInstance, projID, projStatus, $state) {
      $scope.projID = projID;
      $scope.projStatus = projStatus;
      $scope.msgsent = false;
      if(projStatus == '1'){
        $scope.modalTitle = "Approve Project";
        $scope.panelMode = "panel-success";
        $scope.note = "Approve this project?";
        $scope.disapprove = false;
        $scope.bott = 'Yes';
        var msg = "";
      }else{
        $scope.modalTitle = "Disapprove Project";
        $scope.panelMode = "panel-danger";
        $scope.note = "Please compose a letter why you disapprove this Project to inform the user.";
        $scope.disapprove = true;
        $scope.bott = 'Send';
      }
      $scope.send = function (msg) {
        if(msg==undefined){
          msg = "";
        }
        $http({
          url: API_URL+"/project/resolution/" + projID + "/" + projStatus,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(msg)
        }).success(function (data, status, headers, config) {
          console.log(data);
          $scope.msgsent = true;
          loadProj();
          paginate(off, keyword);
          $modalInstance.dismiss('cancel');
        }).error(function (data, status, headers, config) {
          $scope.status = status;
        });

      };
      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };
    }
  }

  $scope.checkdon = function(projID){
    $scope.projID = projID;
    var modalInstance = $modal.open({
      templateUrl: 'checkdonProj.html',
      controller: checkdonProjCTRL,
      resolve: {
        projID: function () {
          return projID
        }
      }
    });
  }
  var checkdonProjCTRL = function ($scope, $modalInstance, projID, $state) {
    $scope.projID = projID;
    $scope.ok = function (projID) {
      $scope.projID = projID;
      $state.go('projectdonations', {projID: projID });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.delete = function (projID) {
    var modalInstance = $modal.open({
      templateUrl: 'deleteMyProj.html',
      controller: deleteMyProjCTRL,
      resolve: {
        projID: function () {
          return projID
        }
      }
    });
  };
  var deleteMyProjCTRL = function ($scope, $modalInstance, projID) {
    $scope.projID = projID;
    $scope.ok = function (projID) {
      $http({
        url: API_URL + "/projects/deleteMyProj/" + projID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        paginate(off, keyword);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

});

app.filter('daysleft', function(){    
  return function (item) {
   var days = item.toString().split(" ");
   return days[1];
 };
});

app.filter('putcomma', function(){    
  return function (item) {
    if(item){
      var parts = item.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }

  };
});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});