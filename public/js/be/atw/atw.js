'use strict';

/* Controllers */

app.controller('AtwCtrl', function($scope, $http, $modal, $state, $sce, $q,$filter){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;  
  
  var sort = 'id';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.datefrom = null;
  $scope.dateto= null ;

  var paginate = function(off, keyword) {
     if(keyword){
      keyword = keyword;
    }else{
      keyword = null;
    }
    // console.log(keyword);
    $http({
      url: API_URL+"/atw/list/" + num + '/' + off + '/' + keyword+ '/' + $scope.sortIn + '/' + $scope.sortBy+ '/' + $scope.datefrom + '/' + $scope.dateto,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      $scope.data = data;
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.filterdate = function(_gdate_f, _gdate_t){
  if(_gdate_f == undefined || _gdate_f == _gdate_t){
      $scope.alerts.splice(0, 1);
      $scope.alerts.push({ type: 'danger', msg: 'Select Date From Range To Date.' });
  }else if(_gdate_f > _gdate_t){
     $scope.alerts.splice(0, 1);
      $scope.alerts.push({ type: 'danger', msg: 'Invalid Date Range Filter.' });
  }else{
    $scope.alerts.splice(0, 1);
    $scope.datefrom = $filter('date')(_gdate_f,'yyyy-MM-dd');
    $scope.dateto = $filter('date')(_gdate_t,'yyyy-MM-dd');
    paginate(off, keyword);
  }
}

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

$scope.numpages = function (off, keyword) {
  paginate(off, keyword);
}

$scope.setPage = function (pageNo, keyword) {
  var off = pageNo;
  paginate(off, keyword);
};

$scope.refresh = function() {
  keyword = null;  
  $scope.dfrom = "";
  $scope.dto = "";
  $scope.datefrom = null;
  $scope.dateto= null ;
  var off = 1;
  $scope.searchtext = null;
  paginate(off, keyword);
}


$scope.deletepage = function(pageid,curpage,keyword,dfrom,dto){
  $scope.pageid
  var modalInstance = $modal.open({
    templateUrl: 'deletepage.html',
    controller: deletepageInstanceCTRL,
    resolve: {
      pageid: function () {
        return pageid
      },
      curpage: function () {
        return curpage
      },
      keyword: function () {
        return keyword
      },
      dfrom: function () {
        return dfrom
      },
      dto: function () {
        return dto
      }
    }
  });
}

var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, curpage, keyword, dfrom,dto, $state) {
  $scope.pageid = pageid;
  $scope.ok = function (pageid) {
    $http({
      url: API_URL + "/atw/delete/" + pageid,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      closeAlert();
      $scope.sortBy = dfrom;
      $scope.sortIn = dto;
      $modalInstance.close();
      paginate(curpage, keyword);
      $scope.success = true;
      deleteloadalert();
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}

$scope.alerts = [];
var closeAlert = function(index) {
  $scope.alerts.splice(index, 1);
}

var deleteloadalert = function(){
  $scope.alerts.push({ type: 'success', msg: 'Successfully Deleted!' });
}
var errorloadalert = function(){
  $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
}

$scope.closeAlert = function(index) {
  $scope.alerts.splice(index, 1);
}


$scope.review = function(pageid, curpage, searchtext){
  $scope.pageid
  var modalInstance = $modal.open({
    templateUrl: 'atwModalReview.html',
    size: 'lg',
    controller: reviewCTRL,
    resolve: {
      pageid: function () {
        return pageid
      },
      curpage: function () {return curpage},
      searchtext: function () {return searchtext},
    }
  });
}
var reviewCTRL = function ($scope, $modalInstance, pageid, curpage,searchtext, $state) {
  $scope.amzon = Amazon_link;


  $http({
    url: API_URL+"/atw/revie/" + pageid,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {
 
    $scope.review = data[0];
    paginate(curpage, searchtext);
  }).error(function (data, status, headers, config) {
    $scope.status = status;
  });
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}
//DATE PICKER
  $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    
    $scope.toggleMin();

    $scope.open = function($event, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[opened] = true;
    };


    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };


    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})
app.controller('FaCtrl', function($scope, $http, $modal, $state, $sce, $q,$filter){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;  
  var sort = 'id';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.datefrom = null;
  $scope.dateto= null ;

  var paginate = function(off, keyword) {
     if(keyword){
      keyword = keyword;
    }else{
      keyword = null;
    }
    // console.log(keyword);
    $http({
      url: API_URL+"/fa/list/" + num + '/' + off + '/' + keyword+ '/' + $scope.sortIn + '/' + $scope.sortBy+ '/' + $scope.datefrom + '/' + $scope.dateto,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      // console.log(data);
      $scope.data = data;
      $scope.maxSize = 10;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
// console.log(keyword)
    paginate(off, keyword);
  }

  $scope.filterdate = function(_gdate_f, _gdate_t){
  if(_gdate_f == undefined || _gdate_f == _gdate_t){
      $scope.alerts.splice(0, 1);
      $scope.alerts.push({ type: 'danger', msg: 'Select Date From Range To Date.' });
  }else if(_gdate_f > _gdate_t){
     $scope.alerts.splice(0, 1);
      $scope.alerts.push({ type: 'danger', msg: 'Invalid Date Range Filter.' });
  }else{
    $scope.alerts.splice(0, 1);
    $scope.datefrom = $filter('date')(_gdate_f,'yyyy-MM-dd');
    $scope.dateto = $filter('date')(_gdate_t,'yyyy-MM-dd');
    paginate(off, keyword);
  }
}

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword);
  }

$scope.numpages = function (off, keyword) {
  paginate(off, keyword);
}

$scope.setPage = function (pageNo, keyword, searchcat) {
 
  var off = pageNo;
  if (keyword==undefined || keyword==null || keyword=="") {
    keyword = searchcat;
  };

  paginate(off, keyword);
};

$scope.refresh = function() {
  keyword = null;  
  $scope.dfrom = "";
  $scope.dto = "";
  $scope.datefrom = null;
  $scope.dateto= null ;
  var off = 1;
  $scope.searchtext = null;
  paginate(off, keyword);
}


$scope.deletepage = function(pageid,curpage,keyword,dfrom,dto){
  $scope.pageid
  var modalInstance = $modal.open({
    templateUrl: 'deletepage.html',
    controller: deletepageInstanceCTRL,
    resolve: {
      pageid: function () {
        return pageid
      },
      curpage: function () {
        return curpage
      },
      keyword: function () {
        return keyword
      },
      dfrom: function () {
        return dfrom
      },
      dto: function () {
        return dto
      }
    }
  });
}

var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid,curpage, keyword, dfrom,dto, $state) {
  $scope.pageid = pageid;
  $scope.ok = function (pageid) {
    $http({
      url: API_URL + "/fa/delete/" + pageid,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      closeAlert();
      $scope.sortBy = dfrom;
      $scope.sortIn = dto;
      $modalInstance.close();
      paginate(curpage, keyword);
      $scope.success = true;
      deleteloadalert();
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}

$scope.alerts = [];
var closeAlert = function(index) {
  $scope.alerts.splice(index, 1);
}

var deleteloadalert = function(){
  $scope.alerts.push({ type: 'success', msg: 'Successfully Deleted!' });
}
var errorloadalert = function(){
  $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
}

$scope.closeAlert = function(index) {
  $scope.alerts.splice(index, 1);
}

$scope.review = function(pageid, curpage, searchtext){
  $scope.pageid
  var modalInstance = $modal.open({
    templateUrl: 'review.html',
    size: 'lg',
    controller: reviewCTRL,
    resolve: {
      pageid: function () {
        return pageid
      },
      curpage: function () {return curpage},
      searchtext: function () {return searchtext},
    }
  });
}
var reviewCTRL = function ($scope, $modalInstance, pageid, curpage,searchtext, $state) {
  $scope.amzon = Amazon_link;
  $http({
    url: API_URL+"/fa/getinfo/" + pageid,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {
    $scope.review = data[0];
    // console.log(data)
     paginate(curpage, searchtext);
  }).error(function (data, status, headers, config) {
    $scope.status = status;
  });
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}
//DATE PICKER
  $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    
    $scope.toggleMin();

    $scope.open = function($event, opened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[opened] = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})