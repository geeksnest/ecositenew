'use strict';

/* Controllers */

app.controller('createAlbumCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload, $filter) {

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.addphoto = false;
  $scope.bigImages = false;
  $scope.file = [];
  $scope.bigfile = [];
  $scope.imageselected = false;
  $scope.delete = function(index) {
    $scope.file.splice(index, 1);
    console.log('dasdasd');
  }
  $scope.uploadAlbum2 = function(files,albumname) {
    console.log('ito upload');
    console.log(files);
    console.log('ito album name');
    console.log(albumname);
  }
  $scope.onalbumname = function convertToSlug(Text)
  {
    if(Text != null)
    {
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.album.foldername = angular.lowercase(text1.replace(/ +/g,'-'));
    }
  }
  $scope.uploadAlbum = function (files,album)
  {
    console.log("=====upload======");

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
            },
            fields : {
                key: 'uploads/slider/'+ album.foldername +'/'+ file.name, // the key to store the file on S3, could be file name or customized
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                policy: Config.policy, // base64-encoded json policy (see article below)
                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
              },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'img' : filename,
              'albumname' : album.albumname,
              'foldername' : album.foldername,
              'filecount' : filecount
            };

            console.log(fileout);

            $http({
              url: API_URL + "/slider/addAlbum",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              // loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                loadAlbums();
                $scope.imageselected = false;
                $scope.addphoto = false;
                $scope.file = [];
                $scope.bigfile = [];
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
  };

  $scope.prepare = function(files,file) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (files && files.length) {
      for (var i = 0; i < files.length; i++)
      {
        console.log(files[i].size);

        if (files[i].size >= 2000000) {
          console.log(files[i]);
          $scope.bigfile.push(files[i]);
          $scope.bigImages = true;
        }else{
          $scope.file.push(files[i]);
        }
      }
      $scope.addphoto = true;
      $scope.closeAlert();
      $scope.imageselected = true;
    }
  }

  var loadAlbums = function () {
    $http({
      url: API_URL+"/slider/albumslist/all",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.album = data;
      $scope.item = data.item;
      console.log(data);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadAlbums();

  $scope.editalbum = function (albumid){
    var modalInstance = $modal.open({
      templateUrl: 'editAlbum.html',
      controller: editalbumCTRL,
      resolve: {
        albumid: function () {
          return albumid;
        }
      }
    });
  }
            
  var editalbumCTRL = function ($scope, $modalInstance, albumid, $state) {
    $scope.albumid = albumid;
    $scope.ok = function (albumid) {
      $scope.albumid = albumid;
      $state.go('edit_album', {id:albumid});
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.deleteAlbum = function (album_id){
    var modalInstance = $modal.open({
      templateUrl: 'deleteAlbum.html',
      controller: deleteAlbumCTRL,
      resolve: {
        album_id: function () {
          return album_id;
        }
      }
    });
  }

  var deleteAlbumCTRL = function ($scope, $modalInstance, album_id, $state) {
    $scope.album_id = album_id;
    $scope.delete = function (album_id) {
      console.log(album_id);

      $http({
        url: API_URL + "/slider/deleteAlbum/" + album_id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        loadAlbums();
      }).error(function (data, status, headers, config) {

      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.setMainSlider = function (album_id){
    var modalInstance = $modal.open({
      templateUrl: 'setMainSlider.html',
      controller: setMainSliderCTRL,
      resolve: {
        album_id: function () {
          return album_id;
        }
      }
    });
  }

  var setMainSliderCTRL = function ($scope, $modalInstance, album_id, $state) {
    $scope.album_id = album_id;
    $scope.set = function (album_id) {
      console.log(album_id);

      $http({
        url: API_URL + "/slider/setMainSlider/" + album_id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        loadAlbums();
      }).error(function (data, status, headers, config) {

      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };



})
////////////////////////////===============end===============////////////////////////////////


app.directive('onlyDigits', function () {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
      if (!ngModel) return;
      ngModel.$parsers.unshift(function (inputValue) {
        var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
        ngModel.$viewValue = digits;
        ngModel.$render();
        return digits;
      });
    }
  };
});