'use strict';

/* Controllers */

app.controller('editbannerCtrl', function ($scope, $http, $modal, $stateParams, $sce, $q, $timeout, Config, Upload, $filter) {

  // console.log($stateParams.id);
  $scope.data = {};

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.addphoto = false;
  $scope.bigImages = false;
  $scope.file = [];
  $scope.bigfile = [];
  $scope.imageselected = false;

  var loadAlbumsImages = function (id) {
    $http({
      url: API_URL+"/banner/banneralbumsimages/" + $stateParams.id,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.img = data.item;
      console.log(data.itemcount);
      $scope.images = [];
      for (var i = 1; i <= data.itemcount; i++)
      {
        $scope.images.push(i);
        console.log(i);        
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadAlbumsImages();

  $scope.remove = function(index) {
    $scope.file.splice(index, 1);
    console.log('dasdasd');
    console.log($scope.file);
    if($scope.file.length == 0){
      $scope.addphoto = false;
    }
    
  }
  $scope.uploadAlbum2 = function(files,albumname) {
    console.log('ito upload');
    console.log(files);
    console.log('ito album name');
    console.log(albumname);
  }

  $scope.sortSlider = function(imgID,sort,album_id) {
    console.log(imgID);
    console.log(sort);
    $http({
        url: API_URL + "/banner/sortBanner/" + imgID + "/" + sort + "/" + album_id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {        
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {
        
      });
  }

  $scope.onalbumname = function convertToSlug(Text)
  {
    if(Text != null)
    {
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.album.foldername = angular.lowercase(text1.replace(/ +/g,'-'));
    }
  }
  $scope.uploadAlbum = function (files,album)
  {
    console.log("=====upload======");

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
            //Headers change here
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
            },
            fields : {
                key: 'uploads/banner/'+ album.foldername +'/'+ file.name, // the key to store the file on S3, could be file name or customized
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                policy: Config.policy, // base64-encoded json policy (see article below)
                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
              },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'img' : filename,
              'album_name' : album.album_name,
              'foldername' : album.foldername
            };

            console.log(fileout);

            $http({
              url: API_URL + "/banner/banneraddAlbumItem",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                loadAlbumsImages();
                $scope.imageselected = false;
                $scope.addphoto = false;
                $scope.file = [];
                $scope.bigfile = [];
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });
        }
      }
    }
  };

  $scope.prepare = function(files,file) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (files && files.length) {
      for (var i = 0; i < files.length; i++)
      {
        console.log(files[i].size);

        if (files[i].size >= 2000000) {
          console.log(files[i]);
          $scope.bigfile.push(files[i]);
          $scope.bigImages = true;
        }else{
          $scope.file.push(files[i]);
        }
      }
      $scope.addphoto = true;
      $scope.closeAlert();
      $scope.imageselected = true;
    }
  }

  $scope.editimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'editAlbumImage.html',
      controller: editImageCTRL,
      windowClass: 'app-modal-window',

      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }
            
  var editImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.isLoading = false;
    $scope.imgID = imgID;
    $scope.imgtype = 'banner';
    $scope.amazonlink = Config.amazonlink;
    $scope.tfsize = [];
    $scope.dfsize = [];

    for (var x = 30; x <= 45; x++) {
      $scope.tfsize.push({'val': x})
    }

    for (var y = 12; y <= 20; y++) {
      $scope.dfsize.push({'val': y})
    }

    var loadImage = function () {
      $http({
        url: API_URL+"/banner/bannereditImage/" + imgID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data[0];
        console.log(data[0]);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadImage();

    $scope.save = function (data) {
      console.log(data);
      $scope.isLoading = true;

      $http({
        url: API_URL + "/banner/bannerupdateImage",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(data)
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');      
        $scope.isLoading = false;
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };


   $scope.deleteimage = function (imgID){
    var modalInstance = $modal.open({
      templateUrl: 'deleteAlbumImage.html',
      controller: deleteImageCTRL,
      resolve: {
        imgID: function () {
          return imgID;
        }
      }
    });
  }

  var deleteImageCTRL = function ($scope, $modalInstance, imgID, $state) {
    $scope.imgID = imgID;
    $scope.delete = function (imgID) {
      console.log(imgID);

      $http({
        url: API_URL + "/banner/bannerdeleteImage/" + imgID,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        loadAlbumsImages();
      }).error(function (data, status, headers, config) {

      });

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };



})
/////////////////////////////===============end===============/////////////////////////////////


app.directive('onlyDigits', function () {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function (scope, element, attrs, ngModel) {
      if (!ngModel) return;
      ngModel.$parsers.unshift(function (inputValue) {
        var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
        ngModel.$viewValue = digits;
        ngModel.$render();
        return digits;
      });
    }
  };
});