'use strict';

/* Controllers */

app.controller('NYearthcitizenwalkCtrl', function ($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout) {


  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var listIn = 'normal';
  
  var sort = 'id';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword) {

    $scope.loadingdata = true;
    if (keyword == ''){
      keyword == null;
    }

    $http({
      url: API_URL+"/donation/donationlistny/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy + '/' + listIn,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {     
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }  
  paginate(off, keyword, sort, sortto, listIn);
  
  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sort, sortto, listIn);
  }
  
  $scope.refresh = function(searchtext,year,month,day) {

    num = 10;
    off = 1;
    keyword = null;
    searchtext = null;
    $scope.searchtext = [];
    year = null;
    month = null;
    day = null;
    paginate(off, keyword);
    if($scope.timestamp.year == undefined){
      $scope.timestamp.year = null;
    }else{
      $scope.timestamp.year = [];
    }
    if($scope.timestamp.month == undefined){
      $scope.timestamp.month = null;
    }else{
      $scope.timestamp.month = [];
    }
    if($scope.timestamp.day == undefined){
      $scope.timestamp.day = null;
    }else{
      $scope.timestamp.day = [];
    }
  } 
  $scope.search = function (keyword) {
    paginate(off, keyword, sort, sortto, listIn);
  }

  $scope.numpages = function (off, keyword, timestamp, advancesearch) {
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword, timestamp, advancesearch) {
    var off = pageNo;
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
    }
    paginate(off, keyword);
  };

$scope.searchtimestamp = function (timestamp,advancesearch) {  
    
    if(advancesearch == true){
      if(timestamp['year'] == undefined){
        var year = '';
      }else{
        var year = timestamp['year'];
      }
      if(timestamp['month'] == undefined){
        var month = '';
      }else{
        var month = timestamp['month'];
      }
      if(timestamp['day'] == undefined){
        var day = '';
      }else{
        var day = timestamp['day'];
      }
      if(year != '' && month == '' && day == ''){
        var keyword = year + '-';
      }else{        
        var keyword = year + '-' + month + '-' + day;
      }
      
      console.log(keyword);
      paginate(off, keyword);   
    }  
  }

        // paginatebytimestamp(off, keyword);

        var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
          $scope.pageid = pageid;
            // console.log(pageid);
            $scope.ok = function (pageid) {
              var page = {'page': pageid};
              $http({
                url: API_URL + "/donation/deleteotherdonor1/" + pageid,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(page)
              }).success(function (data, status, headers, config) {
                $modalInstance.close();
                paginate(off, keyword);
                $scope.success = true;
              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }

          $scope.delete = function(pageid){
            $scope.pageid
            var modalInstance = $modal.open({
              templateUrl: 'deleteOtherDonationModal.html',
              controller: deletepageInstanceCTRL,
              resolve: {
                pageid: function () {
                  return pageid
                }
              }
            });
          }

        // donate now
        $scope.updateAmounts = function (donate) {
            // console.log(donate);
            var config = {
              params: {
                user: donate
              }
            };
            $http({
              url: API_URL + "/members/updatedonations1",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(donate)
            }).success(function (data, status, headers, config) {
                // $scope.donate.amount = data.amount;
                // $scope.donate.users = data.users;
                $scope.updatedamount = true;

                $timeout(function() {
                  $scope.updatedamount = false;
                }, 3000);

              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            }

          })