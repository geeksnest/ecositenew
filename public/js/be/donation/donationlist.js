'use strict';

/* Controllers */

app.controller('DonationsCtrl', function ($http, $modal, $state, $scope, $parse, $location, $anchorScroll, $timeout) {

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var listIn = 'normal';

  var sort = 'id';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  $scope.updatedamount = false;
  $scope.loadingdata = false;

  var paginate = function (off, keyword, sort, sortto, listIn) {

    $scope.loadingdata = true;
    if (listIn == "recurr"){
      var recurr = keyword;
    }else{
      var recurr = [];
    }
    $http({
      url: API_URL+"/donation/donationlist/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy + '/' + listIn,
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(recurr)
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
      $scope.loadingdata = false;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword, sort, sortto, listIn);

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sort, sortto, listIn);
  }

  $scope.search = function (keyword) {
    var listIn = "normal";
    paginate(off, keyword, sort, sortto, listIn);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword, sort, sortto, listIn);
  }

  $scope.setPage = function (pageNo, keyword, sort, sortto) {
    if(keyword == ''){keyword = null}
      console.log(sort);
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(pageNo, keyword, sort, sortto, listIn);
  };

  $scope.refresh = function(advancesearch, searchby, recurring) {
    console.log(advancesearch);
    console.log(searchby);
    console.log(recurring);
    var num = 10;
    var off = 1;
    var keyword = null;
    var listIn = 'normal';
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.searchtext = null;
    if(advancesearch == true){
      if(recurring != undefined){
        $scope.recurring.count = [];
        $scope.recurring.year = [];
        $scope.recurring.month = [];
        $scope.recurring.day = [];
        $scope.advancesearch == false;
      }
    }
    $scope.advancesearch == false;
    paginate(off, keyword, sort, sortto);
  }


  ///////search by time stamp
  $scope.searchbytimestamp = function (timestamp) {
    console.log(timestamp);

    if(timestamp.month !== undefined && timestamp.day == undefined && timestamp.year == undefined){
      var y = '';
      var m = timestamp.month + '-';
      var d = '';
    }
    else if(timestamp.month !== undefined && timestamp.day !== undefined && timestamp.year == undefined){
      var y = '';
      var m = timestamp.month + '-';
      var d = timestamp.day;
    }
    else if(timestamp.month == undefined && timestamp.day == undefined && timestamp.year !== undefined){
      var y = timestamp.year + '-';
      var m = '';
      var d = '';
    }
    else if(timestamp.month !== undefined && timestamp.day !== undefined && timestamp.year !== undefined){
      var y = timestamp.year + '-';
      var m = timestamp.month + '-';
      var d = timestamp.day;
    }
    else if(timestamp.month !== undefined && timestamp.day == undefined && timestamp.year !== undefined){
      var y = timestamp.year + '-';
      var m = timestamp.month + '-';
      var d = '';
    }else{
      var y = '';
      var m = '';
      var d = '';
    }

    var keyword = y + '' + m + '' + d;
    paginate(off, keyword, sort, sortto, listIn);

  }


  ///////search by Recurring
  $scope.searchbyrecurr = function (keyword) {
    console.log(keyword);
    var listIn = "recurr";
    paginate(off, keyword, sort, sortto, listIn);
  }

  /////////////////////////////////////////////

  $scope.delete = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'deletepage.html',
      controller: deletepageInstanceCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }
  var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
    $scope.ok = function (pageid) {
      var page = {'page': pageid};
      $http({
        url: API_URL + "/donation/deletedonor/" + pageid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(page)
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        $scope.success = true;
        paginate(off, keyword);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }


  // donate now
  $scope.updateAmounts = function (donate) {
    var config = {
      params: {
        user: donate
      }
    };
    $http({
      url: API_URL + "/members/updatedonations",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(donate)
    }).success(function (data, status, headers, config) {
      $scope.updatedamount = true;

      $timeout(function() {
        $scope.updatedamount = false;
      }, 3000);

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

})

