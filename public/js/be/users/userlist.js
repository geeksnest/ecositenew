'use strict';

/* Controllers */

app.controller('UserlistCtrl', function($scope, $http,$modal, $stateParams, $state) {
  
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null; 

  var sort = 'num';
  var sortto = 'DESC';
  $scope.sortBy = sortto;
  $scope.sortIn = sort;

  var paginate = function (off, keyword, sort, sortto) {
    $http({
      url: API_URL+"/users/manageusers/" + num + '/' + off + '/' + keyword + '/' + $scope.sortIn + '/' + $scope.sortBy,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  paginate(off, keyword, sort, sortto); 

  $scope.sortType = function(keyword, sort, sortto) {
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sort, sortto);
  }

        
  $scope.refresh = function() {
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.searchtext = [];
    var sort = 'num';
    var sortto = 'DESC';
    $scope.sortBy = sortto;
    $scope.sortIn = sort;
    paginate(off, keyword, sort, sortto);
  } 

  $scope.search = function (keyword) {
    paginate(off, keyword, sort, sortto);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword, sort, sortto);
  }

  $scope.setPage = function (pageNo, keyword) {
    var off = pageNo;
    if(keyword == ''){keyword = null}
    paginate(off, keyword, sort, sortto);
  };

  var updateuserInstanceCTRL = function ($scope, $modalInstance, userid, $state) {
    $scope.userid = userid;
        // console.log(userid);
        $scope.ok = function (userid) {
          $scope.userid = userid;
          $state.go('edituser', {userid: userid });
          $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.updateuser = function(userid){
        $scope.userid
        var modalInstance = $modal.open({
          templateUrl: 'updateuser.html',
          controller: updateuserInstanceCTRL,
          resolve: {
            userid: function () {
              return userid
            }
          }
        });
      }

      var deleteuserInstanceCTRL = function ($scope, $modalInstance, userid, $state) {
        $scope.userid = userid;
        // console.log(userid);
        $scope.ok = function (userid) {
          var user = {'user': userid};
          $http({
            url: API_URL + "/users/deleteuser/" + userid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(user)
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            paginate(off, keyword);
            $scope.success = true;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.deleteuser = function(userid){
        $scope.userid
        var modalInstance = $modal.open({
          templateUrl: 'deleteuser.html',
          controller: deleteuserInstanceCTRL,
          resolve: {
            userid: function () {
              return userid
            }
          }
        });
      }

      $scope.setstatus = function (userid, userLevel, status, off){
        $http({
          url: API_URL+"/users/status/"+userid+"/"+userLevel+"/"+status,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
          paginate(off, keyword);
        }).error(function (data) {

        });
      }
})
