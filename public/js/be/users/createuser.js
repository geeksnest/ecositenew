'use strict';

/* Controllers */

app.controller('CreateUserCtrl', function ($http, $scope, $parse, $location, $anchorScroll, Config, Upload, $modal, $filter, $timeout, $sce, $q) {
  
  $scope.user = {};
  $scope.master = {};
  $scope.isSaving = false;
  $scope.usernametaken = false;
  $scope.emailtaken = false;
  $scope.invalidemail = false;
  $scope.passwordInvalid = false;
  $scope.passwordMin = false;
  $scope.imageselected = false;


  var checksub = function (user) {
    console.log(user);
    if (user['donationlist'] == true){
      console.log('donationlist');
      var donationlist = 'donationlist,';
    }else{var donationlist = '';}

    if (user['bnbregistration'] == true){
      console.log('bnbregistration');
      var bnbregistration = 'bnbregistration,';
    }else{var bnbregistration = '';}

    if (user['donationothers'] == true){
      console.log('donationothers');
      var donationothers = 'donationothers,';
    }else{var donationothers = '';}

    if (user['houstonearthcitizenswalk'] == true){
      console.log('houstonearthcitizenswalk');
      var houstonearthcitizenswalk = 'houstonearthcitizenswalk,';
    }else{var houstonearthcitizenswalk = '';}

    if (user['nyearthcitizenwalk'] == true){
      console.log('nyearthcitizenwalk');
      var nyearthcitizenwalk = 'nyearthcitizenwalk,';
    }else{var nyearthcitizenwalk = '';}

    if (user['seattlenaturalhealingexpo'] == true){
      console.log('seattlenaturalhealingexpo');
      var seattlenaturalhealingexpo = 'seattlenaturalhealingexpo,';
    }else{var seattlenaturalhealingexpo = '';}

    user['donationAccess'] = donationlist + bnbregistration + donationothers + houstonearthcitizenswalk + nyearthcitizenwalk + seattlenaturalhealingexpo;
    console.log(user['donationAccess']);
  };


  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];


  $scope.prepare = function(file) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('File is too big!');
        $scope.alerts = [{
          type: 'danger',
          msg: 'File ' + file.name + ' is too big'
        }];
        $scope.file = '';
      } else {
        console.log("below maximum");
        $scope.file = file;
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }

  $scope.chkUsername = function(user) {
    console.log(user);
    $http({
      url: API_URL + "/users/chkUsername",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.userform.username.$setValidity("buttonsubmit",false);
        $scope.usernametaken = true;
      } else{
        $scope.userform.username.$setValidity("buttonsubmit",true);
        $scope.usernametaken = false;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };

  $scope.chkEmail = function(user) {
    console.log(user);
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.userform.email.$setValidity("buttonsubmit",false);
      $scope.invalidemail = true;
      console.log('invalid');
    }else{
      $scope.invalidemail = false;

      console.log('valid');
    }
    if(user.email == undefined || user.email == null){
      $scope.userform.email.$setValidity("buttonsubmit",false);
      $scope.invalidemail = false;
      console.log('invalid wala laman');
    }
    $http({
      url: API_URL + "/users/chkEmail",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.userform.email.$setValidity("buttonsubmit",false);
        $scope.emailtaken = true;
      } else{
        $scope.userform.email.$setValidity("buttonsubmit",true);
        $scope.emailtaken = false;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };

  $scope.chkPass = function(user) {

    if(user.password != user.confirm_password){
      $scope.passwordInvalid = true;
    }else{
      $scope.passwordInvalid = false;
    }

  };

  $scope.minPass = function(user) {

    if(user.password.length < 6){
      $scope.passwordMin = true;
    }else{
      $scope.passwordMin = false;
    }

  };

  $scope.chkimage = function(files) {
    // console.log(files[0].name);

    $scope.imageselected = true;
    // files[0].name = 'default-page-thumb.png';
    // console.log(files);
    // $scope.files = files;
  };

  $scope.submitData = function (user, files){
    // console.log(files);
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $anchorScroll();
    $scope.isSaving = true;
    console.log('initializing..');

    console.log('checking image size..');
    console.log(files);
    var file = files[0];
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('image size too large');
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        cope.imagecontent=true;
      }
    }
    else{
      console.log('uploading image..');
      $scope.imageloader=true;

      var promises;
      promises = Upload.upload({

        url: Config.amazonlink,
        method: 'POST',
        transformRequest: function (data, headersGetter) {
          var headers = headersGetter();
          delete headers['Authorization'];
          return data;
        },
        fields : {
          key: 'uploads/userimages/' + file.name,
          AWSAccessKeyId: Config.AWSAccessKeyId,
          acl: 'private',
          policy: Config.policy,
          signature: Config.signature,
          "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
        },
        file: file
      })
      promises.then(function(data){
        console.log('saving data..');
        checksub(user);
        user['image'] = data.config.file.name;
        $http({
          url: API_URL + "/users/create",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(user)
        }).success(function (data, status, headers, config) {
          $scope.isSaving = false;
          $scope.alerts.push({type: 'success', msg: 'User Added.'});
          $scope.user = angular.copy($scope.master);
          $scope.imageselected = false;
          $scope.userform.$setPristine(true);
          $scope.isSaving = false;
          console.log('data saved..');
        }).error(function (data, status, headers, config) {
          $scope.status = status;
          $scope.status = status;
          console.log(data);
        });
        $scope.imageloader=false;
      });
    }


  }

})