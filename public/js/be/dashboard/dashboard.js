'use strict';

/* Controllers */

app.controller('loadDonationsCtrl', function ($scope, $http, $modal, $stateParams, $state, $timeout) {
  console.log('loadDonationsCtrl');

  $timeout(function () {
    $('.emailconfirmation').hide();
  }, 10000);

  $scope.userscount = 0;
  $scope.userscountNepal = 0;
  $scope.userscountNY = 0;
  $scope.userscountHouston = 0;
  $scope.userscountSeattle = 0;
  $scope.donationcount = 0;
  $scope.donationcountNepal = 0;
  $scope.donationcountNY = 0;
  $scope.donationcountHouston = 0;
  $scope.donationcountSeattle = 0;
  var countUp = function () {
    $http.get(API_URL + '/members/userdonation').success(function (response) {
      $scope.userscount = response.user;

      $scope.donationcount = putcomma(response.donations);
      $scope.timeInMs += 5000;
      $timeout(countUp, 5000);

    });
  }
  countUp();
  $timeout(countUp, 5000);

  var countUpNepal = function () {
    $http.get(API_URL + '/members/userdonationNepal').success(function (response) {
      $scope.userscountNepal = response.user;

      $scope.donationcountNepal = putcomma(response.donations);
      $scope.timeInMs += 5000;
      $timeout(countUpNepal, 5000);

    });
  }
  countUpNepal();
  $timeout(countUpNepal, 5000);

  var countUpNY = function () {
    $http.get(API_URL + '/members/userdonationNY').success(function (response) {
      $scope.userscountNy = response.user;

      $scope.donationcountNy = putcomma(response.donations);
      $scope.timeInMs += 5000;
      $timeout(countUpNY, 5000);

    });
  }
  countUpNY();
  $timeout(countUpNY, 5000);

  var countUpHouston = function () {
    $http.get(API_URL + '/members/userdonationHouston').success(function (response) {
      $scope.userscountHouston = response.user;

      $scope.donationcountHouston = putcomma(response.donations);
      $scope.timeInMs += 5000;
      $timeout(countUpHouston, 5000);

    });
  }
  countUpHouston();
  $timeout(countUpHouston, 5000);

  var countUpSeattle = function () {
    $http.get(API_URL + '/members/userdonationSeattle').success(function (response) {
      $scope.userscountSeattle = response.user;

      $scope.donationcountSeattle = putcomma(response.donations);
      $scope.timeInMs += 5000;
      $timeout(countUpSeattle, 5000);

    });
  }
  countUpSeattle();
  $timeout(countUpSeattle, 5000);

  var putcomma = function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };
  var loadmemberscount = function(){
    $http({
      url: API_URL+"/members/memberscount",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.memberscount = data.total_items;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadmemberscount();


})