'use strict';

/* Controllers */

app.controller('CreatePageCtrl', function ($scope, $http, $modal, $state, $sce, $q, $timeout, Config, Upload){
  $scope.isSaving = false;

  $scope.imageloader=false;
  $scope.imagecontent=true;

  $scope.page = {};
  $scope.master = {};
  var orig = angular.copy($scope.master);

  $scope.tfsize = [];
  $scope.dfsize = [];
  for (var x = 30; x <= 45; x++) {
    $scope.tfsize.push({'val': x})
  }

  for (var y = 12; y <= 30; y++) {
    $scope.dfsize.push({'val': y})
  } 

  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/pageimages/';
    $scope.page.banner = Text.substring(texttocut.length);
  }

var cked ;
  var saveme = function(page){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.isSaving = true;
    $http({
      url: API_URL + "/pages/create",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(page)
    }).success(function (data, status, headers, config) {
      $scope.isSaving = false;
      $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
      $scope.reset();
      $scope.formPage.$setPristine(true)
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
    });
  }
  $scope.savePage = function(page){
    cked = CKEDITOR.instances['myeditor'].getData();
    page['body'] = cked;
    if (page['banneropt']==true)
    {
      if (page['banner']==undefined || page['banner']==null) {
        $scope.n = true;
        $('#banner').focus();
      }
      else{
        $scope.n = false
        saveme(page);
      }

    }
    else{
      saveme(page);
    }
   
  }

  var loadImages = function(){
    $http({
      url: API_URL + "/pages/listpageimages",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
      console.log(data);
      $scope.data = data;
    }).error(function (data) {
      $scope.status = status;
    });
  }
  loadImages();

  $scope.$watch('files', function () {
    $scope.upload($scope.files);

  });



  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.upload = function (files)
  {
    $scope.alertss = [];

    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }
        }
        else

        {
          var promises;

          promises = Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/pageimages/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : filename
            };
            $http({
              url: API_URL + "/pages/ajaxfileuploader",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadImages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }

            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });

          });

        }
      }
    }
  };

  $scope.deletepageimg = function(pageid) {

    var modalInstance = $modal.open({
      templateUrl: 'deletepageimgModal.html',
      controller: deletepageimgCTRL,
      resolve: {
        pageid: function() {
          return pageid;
        }
      }
    });
  }

  var deletepageimgCTRL = function($scope, $modalInstance, pageid) {
    $scope.pageid = pageid;
    $scope.ok = function(pageid) {
      $http({
        url: API_URL + "/pages/dltpagephoto",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(pageid)
      }).success(function (data, status, headers, config) {

        loadImages();
        $modalInstance.dismiss('cancel');
      }).error(function (data, status, headers, config) {
        data = data;
      });


    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.reset = function () {
    $scope.page = {};
    $scope.formPage.$setPristine(true);
    $scope.page = angular.copy(orig);
  };

var slugstorage="";
 $scope.editpageslug = function(slugs){
        $scope.editslug = true;
        slugstorage = slugs;
    }

    $scope.setslug = function(slug){
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.page.slugs = slug.replace(/\s+/g, '-').toLowerCase();
            slugstorage = $scope.page.slugs;
        }
    }
    
    $scope.cancelpageslug = function(title) {
        if(title != null)
        {
            $scope.page.slugs = slugstorage;
        }else {
            $scope.page.slugs = '';
        }
        $scope.editslug = false;
    }

var checkslug = function(pagetitle){
  $http({
    url: API_URL + "/pages/checkpagetitles/"+ pagetitle,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (response) {

    if (response.hasOwnProperty('pagealreadyexist')) {
      $scope.alerts.push({type: 'danger', msg: 'The page title is already exist. Please enter another page title.'});
      $scope.formPage.title.$setValidity("buttonsubmit",false);
    } else{
      $scope.formPage.title.$setValidity("buttonsubmit",true);
    }

  }).error(function (response) {
    // $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your page title please refresh the page.'});
  });
}
  $scope.onpagetitle = function convertToSlug(Text, page)
  {
    if(Text != null)
    {
      var text1 = Text.replace(/[^\w /]+/g,'');
      $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
    var pagetitle = $scope.page.title;
    $scope.alerts = [];
    checkslug(pagetitle);

  }
  $scope.onslugs = function(Text){ 
    if(Text != null){
        var text1 = Text.replace(/[^\w /-]+/g,'');
       $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-')); 
     }

   }


  $scope.dltPhoto = function (dlt){

    $('.buttonPanel'+dlt.id).hide();
    $http({
      url: API_URL + "/pages/dltpagephoto",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(dlt)
    }).success(function (data, status, headers, config) {
      loadImages();
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };

  $scope.banners = function convertToSlug(Text)
  {
    $scope.page.banners = Text;
  }
})