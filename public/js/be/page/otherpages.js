'use strict';

/* Controllers */

app.controller('otherpagesCtrl', function($scope, $http, $modal, $state, $sce, $q, $timeout){

  $scope.data = {};
  $scope.showFrom = false;
  $scope.uploadbot = false;
  $scope.savebot = false;
  var num = 10;
  var off = 1;
  var keyword = null; 
  

  var paginate = function(off, keyword) {
    $http({
      url: API_URL+"/pages/pageslist/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.search = function (keyword) {
    paginate(off, keyword);
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo, keyword) {
    var off = pageNo;
    paginate(off, keyword);
  };

  $scope.refresh = function() {
    var off = 1;
    $scope.searchtext = [];
    paginate(off, keyword);
  }


  $scope.savepage = function (page) {
    console.log(page);
    $http({
      url: API_URL + "/pages/addOtherPage",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(page)
    }).success(function (data, status, headers, config) {      
      paginate(off, keyword);
      $scope.success = true;
      $scope.page = {};
      $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };

  $scope.updatepage = function (page) {
    $scope.alerts = [];
    console.log(page);
    $http({
      url: API_URL + "/pages/updateOtherPage",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(page)
    }).success(function (response) {      
      paginate(off, keyword);
      $scope.success = true;
      if (response.hasOwnProperty('pagealreadyexist')) {
        $scope.alerts.push({type: 'danger', msg: 'The page title is already exist. Please enter another page title.'});

      } else{
        $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }).error(function (response) {
      $scope.status = status;
    });
  };

  $scope.onpagetitle = function convertToSlug(Text, page){
    if(Text != null){
      var text1 = Text.replace(/[^\w ]+/g,'');
      $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
    var pagetitle = $scope.page.title;
    $scope.alerts = [];
    $http({
      url: API_URL + "/pages/checkpagetitles/"+ pagetitle,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (response) {
      if (response.hasOwnProperty('pagealreadyexist')) {
        $scope.alerts.push({type: 'danger', msg: 'The page title is already exist. Please enter another page title.'});
        $scope.savebtn = true;         
      } else{
        $scope.savebtn = false; 
      }
    });
  }

  $scope.newpage = function() { 
    $scope.showFrom = false;
    $scope.uploadbot = false;
    $scope.savebot = true; 
    $scope.showFrom = true;
    $scope.cancelbot = true;    
    $scope.page = {};
    $scope.alerts = [];
  }

  $scope.cancel = function() {
    $scope.showFrom = false; 
    $scope.cancelbot = false;  
    $scope.page = {};
    $scope.alerts = [];
  }

  $scope.editpage = function(pageID,title,slugs,content_1,content_2,content_3,content_4,content_5) {
    $scope.alerts = [];
    $scope.page = {
      pageID: pageID,
      title: title, 
      slugs: slugs, 
      content_1: content_1, 
      content_2: content_2, 
      content_3: content_3, 
      content_4: content_4, 
      content_5: content_5
    };
    $scope.showFrom = true;
    $scope.uploadbot = true;
    $scope.savebot = false;
    $scope.cancelbot = true;  
  }



  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    alertclose(index);
  }
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }
  var successloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Added!' });
  }
  var updateloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Page successfully Updated!' });
  }

  var deleteloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Page successfully Deleted!' });
  }
  var errorloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Something went wrong processing your data.' });
  }

  var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
    $scope.ok = function (pageid) {
      var page = {'page': pageid};
      $http({
        url: API_URL + "/pages/deletePage/" + pageid,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(page)
      }).success(function (data, status, headers, config) {
        alertclose();
        $modalInstance.close();
        paginate(off, keyword);
        $scope.success = true;
        deleteloadalert();
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.deletepage = function(pageid){
    $scope.pageid
    var modalInstance = $modal.open({
      templateUrl: 'deletePageModal.html',
      controller: deletepageInstanceCTRL,
      resolve: {
        pageid: function () {
          return pageid
        }
      }
    });
  }



  $scope.setstatus = function (stat,pageid,keyword,off) {
    console.log(stat);

    $scope.currentstatusshow = pageid;
    $http({
      url: API_URL + "/page/updatepagestatus/" + stat + '/' + pageid,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var i = 2;
      setInterval(function(){
        i--;
        if(i == 0)
        {
          $scope.currentstatusshow = 0;
          paginate(off, keyword);
        }
      },100)
    }).error(function (data, status, headers, config) {

    });
  }


})