'use strict';

/* Controllers */


app.controller('Createauthor', function($scope, $state, $q, $http, $modal, $anchorScroll, Config, Upload){

  $scope.imageloader=false;
  $scope.imagecontent=true;

  $scope.author = {};
  $scope.master = {};
  var orig = angular.copy($scope.master);

  $scope.author = {
    name: ""
  };

  $scope.cutlink = function convertToSlug(Text)
  {
    var texttocut = Config.amazonlink + '/uploads/saveauthorimage/';
    $scope.author.photo = Text.substring(texttocut.length);
  }



  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };
  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];


  $scope.saveAuthor = function(author)
  {

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.isSaving = true;

    $http({
      url: API_URL + "/news/createauthor",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(author)
    }).success(function (data, status, headers, config) {
      $scope.alerts.push({type: 'success', msg: 'Author successfully saved!'});
      $scope.author = angular.copy(orig);
      $scope.authorForm.$setPristine();
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.isSaving = false;
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
    });

  }

  var loadimages = function() {

    $http({
      url: API_URL + "/news/authorlistimages",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.imagelist = data;
      console.log("image list");
    }).error(function(data) {
      $scope.status = status;
    });
  }

  loadimages();

  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };


  $scope.$watch('files', function () {
    $scope.upload($scope.files);

  });



  $scope.alertss = [];

  $scope.closeAlerts = function (index) {
    $scope.alertss.splice(index, 1);
  };

  $scope.upload = function (files)
  {



    var filename
    var filecount = 0;
    if (files && files.length)
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++)
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;

          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }


        }
        else

        {



          var promises;

          promises = Upload.upload({

                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                  key: 'uploads/saveauthorimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                              })
promises.then(function(data){

  filecount = filecount + 1;
  filename = data.config.file.name;
  var fileout = {
    'imgfilename' : filename
  };
  $http({
    url: API_URL + "/news/uploadauthorimage",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(fileout)
  }).success(function (data, status, headers, config) {
    loadimages();
    if(filecount == files.length)
    {
      $scope.imageloader=false;
      $scope.imagecontent=true;
    }

  }).error(function (data, status, headers, config) {
    $scope.imageloader=false;
    $scope.imagecontent=true;
  });

});


}



}
}


};



$scope.deleteauthorimg = function (dlt)

{
  console.log(dlt);


  $http({
    url: API_URL + "/news/deleteauthorimg",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(dlt)
  }).success(function (data, status, headers, config) {
    loadimages();
  }).error(function (data, status, headers, config) {
    loadimages();
  });

}




})