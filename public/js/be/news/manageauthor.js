'use strict';

/* Controllers */

app.controller('Manageauthor', function($scope, $state ,$q, $http, $log, $interval, $modal){

  $scope.alerts = [];
  console.log("===MANAGE AUTHOR===");

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';
  
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.currentstatusshow = '';
  $scope.sortType     = 'authorsince';
  $scope.sortReverse  = true;
  $scope.searchFish   = '';


  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }

  var successloadalert = function(){
    $scope.alerts.push({ type: 'success', msg: 'Author successfully Deleted!' });
  }

  var errorloadalert = function(){
    $scope.alerts.push({ type: 'danger', msg: 'Something went wrong Author not Deleted!' });
  }


  var loadauthor = function(){

    $http({
      url: API_URL+"/news/manageauthor/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("news list now na");
      console.log(data);
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadauthor();



  $scope.refresh = function () {
    loadauthor();
  }
  $scope.search = function (keyword) {
    var off = 0;
    $http({
      url: API_URL+"/news/manageauthor/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("news list now na");
      console.log(data);
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  $scope.numpages = function (off, keyword) {
    $http({
      url: API_URL+"/news/manageauthor/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("news list now na");
      console.log(data);
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  $scope.setPage = function (pageNo) {
    $http({
      url: API_URL+"/news/manageauthor/" + num + '/' + pageNo + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("news list now na");
      console.log(data);
      $scope.data = data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  };

  $scope.deleteauthor = function(authorid) {
    var modalInstance = $modal.open({
      templateUrl: 'authorDelete.html',
      controller: authorDeleteCTRL,
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }

  var authorDeleteCTRL = function($scope, $modalInstance, authorid) {

    $scope.alerts = [];
    $scope.ok = function() {
      $http({
        url: API_URL  + "/news/authordelete/" + authorid,
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      }).success(function(data, status, headers, config) {
        console.log("deleted");
        alertclose();
        loadauthor();
        $modalInstance.close();
        successloadalert();

      }).error(function(data, status, headers, config) {

        $scope.alerts = [];
        loadauthor();
        $modalInstance.close();
        errorloadalert();
      });

    };

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };


  };





  var authorEditCTRL = function($scope, $modalInstance, authorid, $state) {
    $scope.authorid = authorid;
    $scope.ok = function(authorid) {
      $scope.authorid = authorid;
      console.log(authorid);
      $state.go('editauthor', {authorid: authorid });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }

  $scope.editauthor = function(authorid) {
    $scope.authorid = authorid;
    console.log(authorid);
    var modalInstance = $modal.open({
      templateUrl: 'authorEdit.html',
      controller: authorEditCTRL,
      resolve: {
        authorid: function() {
          return authorid
        }
      }
    });
  }

})