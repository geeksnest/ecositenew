'use strict';

/* Controllers */

app.controller('Tags', function($scope, $state ,$q, $http, $stateParams, $modal){


  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.editcategoryshow = false;


  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;

  
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.currentstatusshow = '';
  $scope.sortType     = 'tags';
  $scope.sortReverse  = false;
  $scope.searchFish   = '';


  var paginate = function(off, keyword) {
    $http({
      url: API_URL + "/news/managetags/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }).success(function(data, status, headers, config) {
      $scope.data = data;

      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    }).error(function(data, status, headers, config) {
      $scope.status = status;
    });
  }

  paginate(off, keyword);

  $scope.refresh = function () {
    var off = 0;
    paginate(off, keyword);
  }

  $scope.search = function (keyword) {
    var off = 0;
    paginate(off, keyword);
    keyword = keyword;
  }

  $scope.numpages = function (off, keyword) {
    paginate(off, keyword);
  }

  $scope.setPage = function (pageNo) {
    paginate(pageNo, keyword);
    off = pageNo;
  };

  $scope.addtags = function() {
    var modalInstance = $modal.open({
      templateUrl: 'addtags.html',
      controller: addtagsCTRL,
      resolve: {

      }
    });
  }


  var addtagsCTRL = function($scope, $modalInstance) {

    var tagslugs = '';


    $scope.ontags = function convertToSlug(Text){
      if(Text != null){
        var text1 = Text.replace(/[^\w ]+/g,'');
        tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
      }

      $scope.disablebutton = false;

      var tagstitle = Text;
      $scope.alerts = [];

      $http({
        url: API_URL + "/news/tagstitle/"+ tagstitle,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (response) {
        if (response.hasOwnProperty('tagsexist')) {
          $scope.alerts.push({type: 'danger', msg: 'The tag is already exist. Please enter another tag.'});
          $scope.disablebutton = true;
          paginate(off, keyword);
        } else{
          $scope.disablebutton = false;
          paginate(off, keyword);
        }

      }).error(function (response) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong checking your data please refresh the page.'});
      });
    }

    $scope.ok = function(tags) {
      $scope.alerts = [];
      tags['slugs'] = tagslugs;
      $http({
        url: API_URL + "/news/savetags",
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(tags)
      }).success(function(data, status, headers, config) {
        alertclose();
        paginate(off, keyword);
        $modalInstance.close();
        $scope.success = true;
        loadalertaddtags();
      }).error(function(data, status, headers, config) {
        $scope.status = status;
      });
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }

  }

  var loadalertdeletetags = function(){
    $scope.alerts.push({ type: 'success', msg: 'Tag successfully Deleted!' });
  }
  var loadalertaddtags = function(){
    $scope.alerts.push({ type: 'success', msg: 'Tag successfully Added!' });
  }
  var alertclose = function(index) {
    $scope.alerts.splice(index, 1);
  }


  var deletetagsInstanceCTRL = function($scope, $modalInstance, id, $state) {

    $scope.ok = function() {
      $scope.alerts = [];
      var tags = {
        'tags': id
      };
      $http({
        url: API_URL + "/news/tagsdelete/" + id,
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(tags)
      }).success(function(data, status, headers, config) {
        alertclose();
        paginate(off, keyword);
        $modalInstance.close();
        loadalertdeletetags();
      }).error(function(data, status, headers, config) {
        $scope.status = status;
      });
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }
  }

  $scope.tagsDelete = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'tagsDelete.html',
      controller: deletetagsInstanceCTRL,
      resolve: {
        id: function() {
          return id
        }
      }
    });
  }



  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.ontags = function convertToSlug(Text)
  {
    if(Text == null){

    }else{
      var text1 = Text.replace(/[^\w ]+/g,'');
      var tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
      $scope.mem.slugs=tagslugs;
      console.log('adasdasd');
    }

  }


  $scope.updatetags = function(data,memid,tag) {

    $scope.alerts = [];

    if(data != null){
      var text1 = data.replace(/[^\w ]+/g,'');
      var tagslugs = angular.lowercase(text1.replace(/ +/g,'-'));
      tag['tags'] = data;
      tag['slugs'] = tagslugs;
    }

    $http({
      url: API_URL + "/news/updatetags",

      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(tag)

    }).success(function(data, status, headers, config) {
      if(data.hasOwnProperty('error')){
        $scope.alerts = [{ type: 'danger', msg: 'Tag name already in use.' }];
        paginate(off, keyword);
      }else{
        $scope.alerts = [{ type: 'success', msg: 'Tag successfully updated!' }];
        paginate(off, keyword);
      }
    }).error(function(data, status, headers, config) {


    });

  }

})