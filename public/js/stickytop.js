$(document).ready(function() {
        // grab the initial top offset of the navigation 
        var stickyNavTop = $('.navsticky').offset().top;
        var stickyNavTopiCare = $('.navsticky-iCare').offset().top;
        var stickyNavTopiCare2 = $('.navsticky-iCare2').offset().top;
            $('.navsticky').removeClass('navsticky'); 
        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var stickyNav = function(){
            var scrollTop = $(window).scrollTop(); // our current vertical position from the top

            // if we've scrolled more than the navigation, change its position to fixed to stick to top,
            // otherwise change it back to relative
            
            if (scrollTop > stickyNavTopiCare) { 
                $('.navsticky-iCare').addClass('sticky-iCare');
                $(".navhide").attr("style", "visibility: hidden")
            } else {
                $('.navsticky-iCare').removeClass('sticky-iCare'); 
                $(".navhide").attr("style", "visibility: visible")
            }
            
            if (scrollTop > stickyNavTopiCare2) { 
                $('.navsticky-iCare2').removeClass('menu-false');
                $('.navsticky').addClass('menu-false');
            } else {                
                $('.navsticky-iCare2').addClass('menu-false');
                $('.navsticky').removeClass('menu-false');
            }
        };
        stickyNav();
        // and run it again every time you scroll
        $(window).scroll(function() {
            stickyNav();
        });
    });