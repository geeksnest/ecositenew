'use strict';

var init_module = [
    'app.directives',
    'ui.validate',
    'ui.calendar',
    'ui.bootstrap',
    'ui.router',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'ui.select',
    'ngSanitize',
    'localytics.directives',
    'timepickerPop',
    'ngFileUpload',
    'angular-jwt',
    'angular-storage',
    'ckeditor',
    'timer'
];

var app = angular.module('app', init_module, function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
     var param = function (obj) {
      var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

      for (name in obj) {
        value = obj[name];

        if (value instanceof Array) {
          for (i = 0; i < value.length; ++i) {
            subValue = value[i];
            fullSubName = name + '[' + i + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if (value instanceof Object) {
          for (subName in value) {
            subValue = value[subName];
            fullSubName = name + '[' + subName + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if (value !== undefined && value !== null)
          query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
      }

      return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
      return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
  }).config(
  ['$interpolateProvider',
  function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  }]); 

 

  app.controller("indexMainCtrl", function ($timeout, $scope, $modal, $http, $anchorScroll, $location, Countries, MDYbday, Config, Upload, store, jwtHelper, $window) {

    // console.log('+=============+');
    // console.log('MAIN PAGE');
    // console.log('+=============+');

      // _________________BOX1___________________
    $http({
      url: API_URL + "/toppage/show",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
     $scope.box1 = data;
     $scope.image0 = data.image0;
     $scope.image1 = data.image1;
     $scope.image2 = data.image2;

     $scope.title0 = data.title0;
     $scope.title1 = data.title1;
     $scope.title2 = data.title2;

     $scope.desc0 = data.desc0;
     $scope.desc1 = data.desc1;
     $scope.desc2 = data.desc2;

     $scope.count0 = data.count0;
     $scope.count1 = data.count1;
     $scope.count2 = data.count2;
   });
      // _________________BOX2___________________
    $http({
      url: API_URL + "/toppage/show1",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
     $scope.box2 = data;
   });
      // _________________BOX3___________________
    $http({
      url: API_URL + "/toppage/show2",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.box3 = data;
    });

    var homepagecontent = function () {
      $http({
        url: API_URL+"/homepage/getdata",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        // console.log(data);
        // console.log("homepage")
          $scope.homepagecontent = data;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      }); 
    }
    homepagecontent();

    $scope.iCareMenu = false; 

    var checkExpired = function () {
      $http({
        url: API_URL+"/projects/checkExpired",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
          
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      }); 
    }
    checkExpired();


    var shortCode = document.getElementById('shortCode').value;
    var loadSubMenuList = function () { 
      $http({
        url: API_URL + "/menu/viewfrontendmenu/" + shortCode,
        method: "GET",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).success(function(data) {
       $scope.webmenu = data.data; 
       $scope.mobmenu = data.data;
       // console.log(data.data);
     })
    }

    loadSubMenuList();


    $timeout(function(){
      var maxHeight = 400;
    $(".dropdown > li").hover(function() {
    
         var $container = $(this),
             $list = $container.find("ul"),
             $anchor = $container.find("a"),
             height = $list.height() * 1.1,       // make sure there is enough room at the bottom
             multiplier = height / maxHeight;     // needs to move faster if list is taller
        
        // need to save height here so it can revert on mouseout            
        $container.data("origHeight", $container.height());
        
        // so it can retain it's rollover color all the while the dropdown is open
        $anchor.addClass("hover");
        
        // make sure dropdown appears directly below parent list item    
        $list
            .show()
            .css({
                paddingTop: $container.data("origHeight")
            });
        
        // don't do any animation if list shorter than max
        if (multiplier > 1) {
            $container
                .css({
                    height: maxHeight,
                    overflow: "hidden"
                })
                .mousemove(function(e) {
                    var offset = $container.offset();
                    var relativeY = ((e.pageY - offset.top) * multiplier) - ($container.data("origHeight") * multiplier);
                    if (relativeY > $container.data("origHeight")) {
                        $list.css("top", -relativeY + $container.data("origHeight"));
                    };
                });
        }
        
    }, function() {
    
        var $el = $(this);
        
        // put things back to normal
        $el
            .height($(this).data("origHeight"))
            .find("ul")
            .css({ top: 0 })
            .hide()
            .end()
            .find("a")
            .removeClass("hover");
    
    }); 
  

    $('#cssmenu li.has-sub>a').on('click', function(){
    $(this).removeAttr('href');
    var element = $(this).parent('li');
    if (element.hasClass('open')) {
      element.removeClass('open');
      element.find('li').removeClass('open');
      element.find('ul').slideUp();
    }
    else {
      element.addClass('open');
      element.children('ul').slideDown();
      element.siblings('li').children('ul').slideUp();
      element.siblings('li').removeClass('open');
      element.siblings('li').find('li').removeClass('open');
      element.siblings('li').find('ul').slideUp();
    }
  });

  $('#cssmenu>ul>li.has-sub>a').append('<span class="holder"></span>');

  (function getColor() {
    var r, g, b;
    var textColor = $('#cssmenu').css('color');
    textColor = textColor.slice(4);
    r = textColor.slice(0, textColor.indexOf(','));
    textColor = textColor.slice(textColor.indexOf(' ') + 1);
    g = textColor.slice(0, textColor.indexOf(','));
    textColor = textColor.slice(textColor.indexOf(' ') + 1);
    b = textColor.slice(0, textColor.indexOf(')'));
    var l = rgbToHsl(r, g, b);
    if (l > 0.7) {
      $('#cssmenu>ul>li>a').css('text-shadow', '0 1px 1px rgba(0, 0, 0, .35)');
      $('#cssmenu>ul>li>a>span').css('border-color', 'rgba(0, 0, 0, .35)');
    }
    else
    {
      $('#cssmenu>ul>li>a').css('text-shadow', '0 1px 0 rgba(255, 255, 255, .35)');
      $('#cssmenu>ul>li>a>span').css('border-color', 'rgba(255, 255, 255, .35)');
    }
  })();

  function rgbToHsl(r, g, b) {
      r /= 255, g /= 255, b /= 255;
      var max = Math.max(r, g, b), min = Math.min(r, g, b);
      var h, s, l = (max + min) / 2;

      if(max == min){
          h = s = 0;
      }
      else {
          var d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
          switch(max){
              case r: h = (g - b) / d + (g < b ? 6 : 0); break;
              case g: h = (b - r) / d + 2; break;
              case b: h = (r - g) / d + 4; break;
          }
          h /= 6;
      }
      return l;
  }



  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });


  var hash = location.hash.replace('#/','#');
  if(hash != ''){
    $('html, body').animate({ scrollTop: $(hash).offset().top}, 1000);
  }

},2000);


    $scope.amazonlink = Config.amazonlink;

    //user section start
    if (store.get('AccessToken')) {
      var token = store.get('AccessToken');
      // $scope.agent = token;      
      // console.log(token);
      var loadMyProfile = function () {
        $http({
          url: API_URL+"/projects/myprofile/" + token.userid,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data) {
          $scope.myProfile = data[0];
          $scope.agent  = data[0];
          // console.log(data[0]); 
        }).error(function (data) {
          $scope.error = data;
        }); 
      }
      loadMyProfile();

      // Notification
      $scope.notiCount = 0;
      var notification = function () {
        console.log('Notification')
        var loadNotifications = function () {
          $http({
            url: API_URL+"/member/notification/" + token.userid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $scope.myNotes = data;
            $scope.notiCount = data.length;     
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          }); 
        }
        loadNotifications();
        $('#notify').addClass('animated rubberBand');
        $timeout(notification, 10000);
      }
      notification();
      $scope.clickNotify = function(username,projSlugs,slugs,noteType,noteID,mark){
        $http({
          url: API_URL+"/member/marknotification/" + noteID +"/"+ mark,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data,response) { 
          if(data.success == '1'){
            if(noteType == "10" || noteType == "11"){
              $window.location = '/medialibrary/' + username + '/' + slugs;
            }else{
              $window.location = '/projects/' + username + '/' + projSlugs + '##' + noteType;
            }
          }

        }).error(function (data,response) {

        }); 
      }
      $scope.markNotify = function(noteID,mark){        
        $http({
          url: API_URL+"/member/marknotification/" + noteID +"/"+ mark,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data,response) { 
          notification();
        }).error(function (data,response) {

        }); 
      }
    }    

    $scope.logout = function(){
      // Login.logout(function(data){
      //     if(data.hasOwnProperty('success')){
        store.remove('AccessToken');
        // $window.location = '/projects/login';
        $window.location = '/login';
      //     }
      // });
    }
    
     

    $scope.myNotification = function(){
      var modalInstance = $modal.open({
        templateUrl: 'myNotification.html',
        controller: myNotificationCTRL,
        windowClass: 'noti-modal-window',
        backdrop : 'static',
        scope: $scope
      });
    }
    var myNotificationCTRL = function ($scope, $modalInstance) {
      var offset = 1;
      var num = 10;
      var keyword = undefined;
      $scope.loading = false;
      $scope.loadmorebot = true;
      var list = [];
      var loadAllNotifications = function (offset,keyword) {
        $scope.loading = true;
        $http({
          url: API_URL+"/member/allnotification/" + token.userid +"/"+ offset +"/"+ num + "/" + keyword,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
          // console.log(data.data);
          $scope.myAllNotes = data.data;
          $scope.currentNotiCount = data.data.length;  
          $scope.allNotiCount = data.total_items;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index; 
          if(data.data.length == data.count){
            $scope.loadmorebot = false;
          }             
        }).error(function (data, status, headers, config) {
          $scope.status = status;
        }); 
      }
      loadAllNotifications(offset,keyword);

      $scope.setPage = function(offsetz){
        offset = offsetz;
        loadAllNotifications(offsetz,keyword);
      }

      $scope.search = function (keywordz) {
        offset = 1;
       if(keywordz==""){keyword=undefined}else{
        keyword = keywordz;
       }
       loadAllNotifications(offset, keyword);
      }

      $scope.refresh = function () {
        $scope.readStatus = "";
        document.getElementById('readStatus').value = "";
        keyword = undefined;
        offset = 1;
        loadAllNotifications(offset, undefined);
      }
      /*$scope.showmore = function(){
        num = num + num;
        loadAllNotifications(offset,keyword);
      }  */

      $scope.clickNotify = function(username,projSlugs,slugs,noteType,noteID,mark){
        $http({
          url: API_URL+"/member/marknotification/" + noteID +"/"+ mark,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data,response) { 
          if(data.success == '1'){
            if(noteType == "10" || noteType == "11"){
              $window.location = '/medialibrary/' + token.username + '/' + slugs;
            }else{
              $window.location = '/projects/' + token.username + '/' + projSlugs + '##' + noteType;
            }
          }
        }).error(function (data,response) {

        }); 
      }
      $scope.markNotify = function(noteID,mark){        
        $http({
          url: API_URL+"/member/marknotification/" + noteID +"/"+ mark,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data,response) { 
          loadAllNotifications(offset,keyword);
        }).error(function (data,response) {

        }); 
      }

      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };      
    }  

    $scope.myproj = function(){
      var modalInstance = $modal.open({
        templateUrl: 'myProj.html',
        controller: myProjCTRL,
        windowClass: 'proj-modal-window',
        backdrop : 'static'
      });
    }
    var myProjCTRL = function ($scope, $modalInstance) {
      var off = 1;

      var loadMyProject = function (off) {
        $http({
          url: API_URL+"/projects/myproj/" + token.userid + "/" + off,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
          $scope.myProj = data.data;
          $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
          $scope.projLink = BASE_URL+'/projects/'+token.username;        
        }).error(function (data, status, headers, config) {
          $scope.status = status;
        }); 
      }
      loadMyProject(off);

      $scope.setPage = function(pageNo){
        off = pageNo
        loadMyProject(off);
      }

      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.delete = function (projID) {
        var modalInstance = $modal.open({
          templateUrl: 'deleteMyProj.html',
          controller: deleteMyProjCTRL,
          resolve: {
            projID: function () {
              return projID
            }
          }
        });
      };
      $scope.alerts = [];
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };
      var alertdelete = function(){
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({type: 'success', msg: 'Project successfully deleted.'});
      }

      var deleteMyProjCTRL = function ($scope, $modalInstance, projID) {
        $scope.projID = projID;
        $scope.ok = function (projID) {
          $http({
            url: API_URL + "/projects/deleteMyProj/" + projID,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            loadMyProject(off);
            alertdelete();
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }
    }      

    
    $scope.viewmyprof = function(){
      var modalInstance = $modal.open({
        templateUrl: 'viewMyProfile.html',
        controller: myProfileCTRL,
        windowClass: 'proj-modal-window',
        backdrop : 'static',
        keyboard: false
      });
    };
    var myProfileCTRL = function ($scope, $modalInstance) {
      // console.log('myProfile');
      $scope.amazonlink = Config.amazonlink;
      $scope.countries = Countries.list();          
      $scope.year = MDYbday.year();
      $scope.day = MDYbday.day();
      $scope.month = MDYbday.month();

      var loadMyProfile = function () {
        $http({
          url: API_URL+"/projects/myprofile/" + token.userid,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data) {
          $scope.myProfile = data[0];    
          $scope.curUname = data[0].username;    
          $scope.curEmail = data[0].email;
          var pic = data[0].photo;   
          var bd = data[0].birthday.toString().split("-");
          $scope.myProfile['byear'] = bd[0];
          $scope.myProfile['bmonth'] = bd[1];
          $scope.myProfile['bday'] = bd[2];
        }).error(function (data) {
          $scope.error = data;
        }); 
      }

      loadMyProfile();

      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      }
      $scope.profileEdit = false;
      $scope.isProcessing = false;
      $scope.editmyprofile = function () {  
        $scope.isProcessing = true;      
        $timeout(function(){
          $scope.profileEdit = true;
          $scope.isProcessing = false; 
        }, 1000); 
      }
      $scope.cancel = function () {        
        $scope.profileEdit = false;             
      }
      
      $scope.imageselected = false;
      $scope.changepic = false;
      $scope.imageselected = false;
      $scope.bigImages = false;
      $scope.prepare = function(file) {
        if (file && file.length) {
          if (file[0].size >= 2000000) {
            console.log('File is too big!'); 
            $scope.alerts = [{
              type: 'danger',
              msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
            }];         
            $scope.profilepic = [];
            $scope.changepic = false;
          } else {
            console.log("below maximum");
            $scope.profilepic = file;
            $scope.imgname = file[0].name;
            $scope.imageselected = true;
            $scope.changepic = true;
            $scope.alerts = [];
            console.log(file);
          }
        }
      }
   

      $scope.alerts = [];
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      $scope.errorget = '';
      $scope.successget = '';

      $scope.updateProf = function (prof, files){
        $scope.alerts = [];
        $scope.errorget = '';
        $scope.successget = '';        
        $scope.isProcessing = true;
        prof['birthday'] = prof.byear+'-'+prof.bmonth+'-'+prof.bday;
        if(!files){          
          if(!prof.username || !prof.email || !prof.firstname || !prof.firstname || !prof.lastname || !prof.byear || !prof.gender || !prof.location){
            $scope.alerts.push({type: 'danger', msg: 'Something went wrong. Please check the fields with (*) and fill it up.'});
            $location.hash('uptop');
            $anchorScroll();
            $scope.isProcessing = false;
          }else{                
            prof['photochange'] = 0; 
            $http({
              url: API_URL + "/member/updateprofile/" + token.userid,
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(prof)
            }).success(function (data, status, headers, config) {
              loadMyProfile();
              $location.hash('uptop');
              $anchorScroll();
              $scope.isProcessing = false;
              $scope.alerts.push({type: 'success', msg: 'Your profile is successfully updated.'});
            }).error(function (data, status, headers, config) {
              // console.log(data);
            });
          }
        }else{          
          if(!prof.username || !prof.email || !prof.firstname || !prof.firstname || !prof.lastname || !prof.byear || !prof.gender || !prof.location){
            $scope.alerts.push({type: 'danger', msg: 'Something went wrong. Please check the fields with (*) and fill it up.'});
            $location.hash('uptop');
            $anchorScroll();
            $scope.isProcessing = false;
          }else{
            var file = files[0];
            if (file && file.length) {
              if (file.size >= 2000000) {
                console.log('image size too large');
                $scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
              }
            }
            else{
              var promises;
              promises = Upload.upload({
                url: Config.amazonlink,
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                  var headers = headersGetter();
                  delete headers['Authorization'];
                  return data;
                },
                fields : {
                  key: 'uploads/memberphotos/' + file.name,
                  AWSAccessKeyId: Config.AWSAccessKeyId,
                  acl: 'private',
                  policy: Config.policy,
                  signature: Config.signature,
                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
                },
                file: file
              })
              promises.then(function(data){
                prof['photo'] = data.config.file.name;                
                prof['photochange'] = 1;                
                $http({
                  url: API_URL + "/member/updateprofile/" + token.userid,
                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(prof)
                }).success(function (data, status, headers, config) {
                  loadMyProfile();
                  $location.hash('uptop');
                  $anchorScroll();
                  $scope.isProcessing = false;
                  $scope.alerts.push({type: 'success', msg: 'Your profile is successfully updated.'});
                }).error(function (data, status, headers, config) {
                  console.log(data);
                });
              });
            }
          }
        }
      }

      $scope.alertA = [];                   
      $scope.unavailable = false;    
      $scope.chkUsername = function(data,curUname) {
        $scope.alertA = [];
        if(data.username == undefined || data.username == null || data.username == ""){
          $scope.unavailable = true;
          $scope.alertA = [{
            Aclass: 'label bg-danger',
            msg: 'This field is required.'
          }]; 
        }else if(data.username == curUname){          
          $scope.alertA = [];
          $scope.unavailable = false;
        }else{          
          $timeout(function(){
            $http({
              url: API_URL + "/member/chkUsername",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(data)
            }).success(function (data,response) {
              if(data.unavailable){
                $scope.alertA = [{
                  Aclass: 'label bg-danger',
                  msg: 'Username unavailable.'
                }]; 
                $scope.unavailable = true;                    
              }else{
                $scope.alertA = [{
                  Aclass: 'label bg-success',
                  msg: 'Username is available.'
                }]; 
                $scope.unavailable = false;                 
              }
            }).error(function (data,response) {
              $scope.alerts = [{
                type: 'danger',
                msg: 'Something went wrong. Please refresh the page.'
              }]; 
            });              
          }, 500); 
        }
      }

      $scope.alertB = [];  
      $scope.chkEmail = function(data,curEmail) {
        $scope.alertB = [];
        if(data.email == undefined || data.email == null || data.email == ""){
          $scope.alertB = [{
            Aclass: 'label bg-danger',
            msg: 'This field is required.'
          }];
          $scope.unavailable = true; 
        }else if(data.email == curEmail){
          $scope.alertB = []; 
          $scope.unavailable = false;  
        }else{
          $timeout(function(){
            var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(data.email);
            if(validemail == false){
              $scope.alertB = [{
                Aclass: 'label bg-danger',
                msg: 'Invalid email address.'
              }];  
              $scope.unavailable = true;
            }else{              
              $scope.alertB = [];
              $http({
                url: API_URL + "/member/chkEmail",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
              }).success(function (data,response) {
                console.log(data);
                if (data.unavailable) {
                  $scope.alertB = [{
                    Aclass: 'label bg-danger',
                    msg: 'Email address is unavailable.'
                  }];  
                  $scope.unavailable = true;                   
                } else{
                  $scope.alertB = [{
                    Aclass: 'label bg-success',
                    msg: 'Email address is available.'
                  }]; 
                  $scope.unavailable = false;                  
                }
              }).error(function (data,response) {
                $scope.alerts = [{
                  type: 'danger',
                  msg: 'Something went wrong. Please refresh the page.'
                }]; 
              });
            }
          }, 500); 
        }
      }

      $scope.changePass = function (userid) {
        var modalInstance = $modal.open({
          templateUrl: 'changeMemberPass.html',
          controller: changePassCTRL,
          resolve: {
            userid: function () {
              return userid
            }
          }
        });
      };
      var changePassCTRL = function ($scope, $modalInstance, userid) {
        $scope.userid = userid;
        $scope.alertspass = [];
        $scope.closeAlert = function(index) {
          $scope.alertspass.splice(index, 1);
        };
        $scope.changdone = false; 
        $scope.changepass = function (pass) {
          if(!pass){
            $scope.alertspass = [{
              type: 'danger',
              msg: 'Please fill all the fields.'
            }]; 
          }else{
            if(!pass.currentpassword || !pass.newpassword || !pass.retypepassword ){
              $scope.alertspass = [{
                type: 'danger',
                msg: 'Please fill all the fields.'
              }]; 
            }else{
              if(pass.newpassword != pass.retypepassword){
                $scope.alertspass = [{
                  type: 'danger',
                  msg: 'New passwords did not match.'
                }]; 
              }else{
                $http({
                  url: API_URL + "/member/changePass/" + userid,
                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(pass)
                }).success(function (data) {
                  console.log(data);
                  if (data.incorrectpass) {
                    $scope.alertspass = [{
                      type: 'danger',
                      msg: data.incorrectpass
                    }];                    
                  } else{
                    $scope.alertspass = [{
                      type: 'success',
                      msg: data.success
                    }];
                    $scope.changdone = true;                   
                  }
                }).error(function (data) {
                  $scope.alertspass = [{
                    type: 'danger',
                    msg: 'Something went wrong. Please refresh the page.'
                  }]; 
                });
              }              
            }            
          }
        };
        $scope.close = function () {
          $modalInstance.dismiss('cancel');
        };
      }

    };
    /**/

    //user section end
    

    $timeout(function () {
      $('.emailconfirmation').hide();
    }, 10000);
    $scope.userscount = 0;
    $scope.donationcount = 0;
    var countUp = function () {
      $http.get(API_URL + '/members/userdonation').success(function (response) {
        $scope.userscount = response.user;
        $scope.heroescount = response.heroes;
        $scope.donationcount = putcomma(response.donations);
        // $scope.timeInMs += 5000;
        $timeout(countUp, 10000);

      });
    }
    countUp();
    // $timeout(countUp, 5000);

    var putcomma = function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }

  }); 



app.controller('CaroController', function ($scope, $timeout, $transition, $q) {
  console.log('CaroController')
  var self = this,
  slides = self.slides = [],
  currentIndex = -1,
  currentTimeout, isPlaying;
  self.currentSlide = null;

  var destroyed = false;
  /* direction: "prev" or "next" */
  self.select = function(nextSlide, direction) {
    var nextIndex = slides.indexOf(nextSlide);
    //Decide direction if it's not given
    if (direction === undefined) {
      direction = nextIndex > currentIndex ? "next" : "prev";
    }
    if (nextSlide && nextSlide !== self.currentSlide) {
      if ($scope.$currentTransition) {
        $scope.$currentTransition.cancel();
        //Timeout so ng-class in template has time to fix classes for finished slide
        $timeout(goNext);
      } else {
        goNext();
      }
    }
    function goNext() {
      // Scope has been destroyed, stop here.
      if (destroyed) { return; }
      //If we have a slide to transition from and we have a transition type and we're allowed, go
      if (self.currentSlide && angular.isString(direction) && !$scope.noTransition && nextSlide.$element) {
        //We shouldn't do class manip in here, but it's the same weird thing bootstrap does. need to fix sometime
        nextSlide.$element.addClass(direction);
        var reflow = nextSlide.$element[0].offsetWidth; //force reflow

        //Set all other slides to stop doing their stuff for the new transition
        angular.forEach(slides, function(slide) {
          angular.extend(slide, {direction: '', entering: false, leaving: false, active: false});
        });
        angular.extend(nextSlide, {direction: direction, active: true, entering: true});
        angular.extend(self.currentSlide||{}, {direction: direction, leaving: true});

        $scope.$currentTransition = $transition(nextSlide.$element, {});
        //We have to create new pointers inside a closure since next & current will change
        (function(next,current) {
          $scope.$currentTransition.then(
            function(){ transitionDone(next, current); },
            function(){ transitionDone(next, current); }
            );
        }(nextSlide, self.currentSlide));
      } else {
        transitionDone(nextSlide, self.currentSlide);
      }
      self.currentSlide = nextSlide;
      currentIndex = nextIndex;
      //every time you change slides, reset the timer
      restartTimer();
    }
    function transitionDone(next, current) {
      angular.extend(next, {direction: '', active: true, leaving: false, entering: false});
      angular.extend(current||{}, {direction: '', active: false, leaving: false, entering: false});
      $scope.$currentTransition = null;
    }
  };


  $scope.$on('$destroy', function () {
    destroyed = true;
  });

  /* Allow outside people to call indexOf on slides array */
  self.indexOfSlide = function(slide) {
    return slides.indexOf(slide);
  };

  $scope.next = function() {
    var newIndex = (currentIndex + 1) % slides.length;

    //Prevent this user-triggered transition from occurring if there is already one in progress
    if (!$scope.$currentTransition) {
      return self.select(slides[newIndex], 'next');
    }
  };

  $scope.prev = function() {
    var newIndex = currentIndex - 1 < 0 ? slides.length - 1 : currentIndex - 1;

    //Prevent this user-triggered transition from occurring if there is already one in progress
    if (!$scope.$currentTransition) {
      return self.select(slides[newIndex], 'prev');
    }
  };

  $scope.select = function(slide) {
    self.select(slide);
  };

  $scope.isActive = function(slide) {
   return self.currentSlide === slide;
 };

 $scope.slides = function() {
  return slides;
};

$scope.$watch('interval', restartTimer);
$scope.$on('$destroy', resetTimer);

function restartTimer() {
  resetTimer();
  var interval = +$scope.interval;
  if (!isNaN(interval) && interval>=0) {
    currentTimeout = $timeout(timerFn, interval);
  }
}

function resetTimer() {
  if (currentTimeout) {
    $timeout.cancel(currentTimeout);
    currentTimeout = null;
  }
}

function timerFn() {
  if (isPlaying) {
    $scope.next();
    restartTimer();
  } else {
    $scope.pause();
  }
}

$scope.play = function() {
  if (!isPlaying) {
    isPlaying = true;
    restartTimer();
  }
};
$scope.pause = function() {
  if (!$scope.noPause) {
    isPlaying = false;
    resetTimer();
  }
};

self.addSlide = function(slide, element) {
  slide.$element = element;
  slides.push(slide);
    //if this is the first slide or the slide is set to active, select it
    if(slides.length === 1 || slide.active) {
      self.select(slides[slides.length-1]);
      if (slides.length == 1) {
        $scope.play();
      }
    } else {
      slide.active = false;
    }
  };

  self.removeSlide = function(slide) {
    //get the index of the slide inside the carousel
    var index = slides.indexOf(slide);
    slides.splice(index, 1);
    if (slides.length > 0 && slide.active) {
      if (index >= slides.length) {
        self.select(slides[index-1]);
      } else {
        self.select(slides[index]);
      }
    } else if (currentIndex > index) {
      currentIndex--;
    }
  };

});

app.controller("loadcenternameCTRL", function ($scope, $http) {

  // load centername to select item from centername table
  var loadcentername = function () {

    $http({
      url: API_URL + "/bnb/cnamelist",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
      $scope.dataCentername = data;
    }).error(function (data) {
      $scope.status = status;
    });
  }
  loadcentername();

});

app.controller("emailcheckCTRL", function ($scope, $http) {

  $scope.ccemailcheck = function (ccworeg) {
    $scope.invalidemail = false;
    if (ccworeg.email == null){
      $scope.invalidemail = false;
    }else{
      var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(ccworeg.email);
      if(validemail == false){
        $scope.invalidemail = true;
      }
      else{                
        $scope.invalidemail = false;
      }
    }

  };

  $scope.echeckemailcheck = function (checkworeg) {
    $scope.invalidemail = false;
    if (checkworeg.email == null){
      $scope.invalidemail = false;
    }else{
      var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(checkworeg.email);
      if(validemail == false){
        $scope.invalidemail = true;
      }
      else{                
        $scope.invalidemail = false;
      }
    }

  };

});


app.controller("registrationCtrl", function ($scope, $http, Countries, MDY, MDYform, $timeout) {
  console.log("registered");
  $scope.usernametaken = '';
  $scope.emailtaken = '';
  $scope.notyetconfirm = '';
  $scope.notyetcompleted = '';
  $scope.success = false;
  $scope.process = false;
  $scope.countries = Countries.list();
  $scope.dataCentername = '';
  $scope.day = MDY.day();
  $scope.month = MDY.month();
  $scope.year = MDY.year();
  $scope.passmin = false;
  $scope.invalidemail = false;
  $scope.emailtaken = false;
  $scope.notyetconfirm = false;
  $scope.notyetcompleted = false;
  $scope.comparepass = false;

  $scope.retypepass = function (user) {
    console.log(user.password);
    console.log(user.repassword);
    if(user.password != user.repassword){        
      $scope.comparepass = true;
    }else{        
      $scope.comparepass = false;
    }       
  }; 

    
  $scope.emailcheck = function (user) {
    console.log("checking for email");
    console.log(user.email);
    $scope.invalidemail = false;
    $scope.emailtaken = false;
    $scope.notyetconfirm = false;
    $scope.notyetcompleted = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.invalidemail = true;
    }else{                   
      $scope.invalidemail = false;
    }

    if(user.email == undefined){
      $scope.invalidemail = false;
    }

    $http.post(API_URL + '/members/registration/check', user).success(function (response) {

      if (response.hasOwnProperty('emailtaken') || response.hasOwnProperty('notyetconfirm') || response.hasOwnProperty('notyetcompleted') ) {

        if (response.hasOwnProperty('emailtaken')) {
          $scope.emailtaken = true;
        } else {
          $scope.emailtaken = false;
        }
        if (response.hasOwnProperty('notyetconfirm')) {
          $scope.notyetconfirm =true;
        } else {
          $scope.notyetconfirm = false;
        }
        if (response.hasOwnProperty('notyetcompleted')) {
          $scope.notyetcompleted = true;
        } else {
          $scope.notyetcompleted = false;
        }
        $scope.process = false;
        $scope.passmin = false;
      }

    });
  };

  //for resendconfirmationAction
  $scope.chtr = function (user) {
    $http.post(API_URL + '/members/confirmation/resend', user).success(function (response) {
      if (response.hasOwnProperty('confirmation')) {
        $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <br/><span style="color:#000;font-size:30px;"></span> <div style="margin-top:-10px;padding:20px;font-size:15px;">The confirmation email has been successfully re-send to your email. Please check your inbox.<a href="" onclick="javascript:window.location.reload()"  tyle="position: absolute; top: 15px; right: 30px; text-align:center;">Close</a></div></div>', {
          opacity: 80,
          overlayCss: {backgroundColor: "#fff"},
          overlayClose: false
        });
      }  
      $scope.response = response;
    });
  };
  //for resendcompletion
  $scope.chtrc = function (user) {

    $http.post(API_URL + '/members/completion/resend', user).success(function (response) {
      if (response.hasOwnProperty('completion')) {
        $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <br/><span style="color:#000;font-size:30px;"></span> <div style="margin-top:-10px;padding:20px;font-size:15px;">The confirmation email has been successfully re-send to your email. Please check your inbox.<a href="" onclick="javascript:window.location.reload()" style="position: absolute; top: 15px; right: 30px; text-align:center;">Close</a></div></div>', {
          opacity: 80,
          overlayCss: {backgroundColor: "#fff"},
          overlayClose: false
        });
      }  
      $scope.response = response;  
    });
  };

  // Here starts the 
  $scope.typetransact2 = '';
  $scope.userecheckreg = {};
  $scope.userccreg = {};
  $scope.pp = {};
  $scope.errorpayment='';

  var d = new Date();
  var n = d.getFullYear();
  $scope.donday = MDYform.day();
  $scope.donmonth = MDYform.month();
  $scope.donyear = MDYform.year();

  $scope.t3 = '';
  $scope.a3 = '';
  $scope.srt = '';

  $scope.t3paypal = [
  {'text': 'Daily', 'val': 'D'},
  {'text': 'Monthly', 'val': 'M'},
  ]

  // load centername to select item from centername table
  $http({
    url: API_URL + "/bnb/cnamelist",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {
    $scope.dataCentername = data;
  }).error(function (data) {
    $scope.status = status;
  });
  $scope.passmin = false;
  var oriUser = angular.copy($scope.user);
  var oriuserccreg= angular.copy($scope.userccreg);
  var oriuserecheckreg= angular.copy($scope.userecheckreg);

    
  $scope.register = function (user, amount, reg_inden) {
    console.log($scope.userccreg);
    user['reg_inden'] = reg_inden;
    if (user.password.length < 6) {
      $scope.passmin = true;
      return
    } else {
      $scope.process = true;
      $http.post(API_URL + '/members/registration/check', user).success(function (response) {

        if (response.hasOwnProperty('usernametaken') || response.hasOwnProperty('emailtaken')) {
          if (response.hasOwnProperty('usernametaken')) {
            $scope.usernametaken = response.usernametaken;
          } else {
            $scope.usernametaken = '';
          }
          if (response.hasOwnProperty('emailtaken')) {
            $scope.emailtaken = response.emailtaken;
          } else {
            $scope.emailtaken = '';
          }
          $scope.process = false;
          $scope.passmin = false;
        } else {
          // CREDIT CARD CHECK IF CREDIT CARD
          if($scope.typetransact2==''){
            $scope.userccreg['amount'] = amount;
            $scope.userccreg['email'] = user.email;
            $scope.userccreg['fName'] = user.fname;
            $scope.userccreg['lName'] = user.lname;
            $http.post(API_URL + '/donate/creditcard', $scope.userccreg).success(function (response) {
              console.log(response);
              if($scope.userccreg.recurpayment){
                if(response.code=='I00001'){
                  $scope.back_button = 1;
                  saveUser(user, amount);
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.text;
                }
              }else{
                if(response.hasOwnProperty('success')){
                  $scope.back_button = 1;
                  saveUser(user, amount);
                }else if(response.error == null){
                  $scope.process = false;
                  $scope.errorpayment  = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                }else{
                  $scope.process = false;
                  $scope.errorpayment  = response.error;
                }
              }
            });
          }else if($scope.typetransact2=='ach2'){
            $scope.userecheckreg['amount'] = amount;
            $scope.userecheckreg['email'] = user.email;
            $scope.userecheckreg['fName'] = user.fname;
            $scope.userecheckreg['lName'] = user.lname;
            $http.post(API_URL + '/donate/echeck', $scope.userecheckreg).success(function (response) {
              if($scope.userecheckreg.recurpayment2){
                if(response.code=='I00001'){
                  saveUser(user, amount);
                  $scope.reccur_message = "Your donation has been successfully processed.";
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.text;
                }
              }else{
                if(response.hasOwnProperty('success')){
                  saveUser(user, amount);
                  $scope.reccur_message = response.success;
                }else if(response.error == null){
                  $scope.process = false;
                  $scope.errorpayment= 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                }else{
                  $scope.process = false;
                  $scope.errorpayment = response.error;
                }
              }
            });
          }else if($scope.typetransact2=='paypal2'){
            if($scope.pp.recurpayment3){
              saveUser(user);
            }else{
              saveUser(user);
            }
          }
        }
        $scope.response = response;
      });
    }
  }

  var saveUser = function(user, amount){

    return $http.post(API_URL + '/members/registration', user).success(function (response) {
      $scope.usernametaken = '';
      $scope.emailtaken = '';
      $scope.success = true;
      if($scope.typetransact2=='paypal2'){
        if($scope.pp.recurpayment3){
          $('#paypalreoccur').submit();
        }else{
          $('#paypalsingle').submit();
        }
      }else{
        $scope.user = angular.copy(oriUser);
        $scope.userccreg = angular.copy(oriuserccreg);
        $scope.userecheckreg = angular.copy(oriuserecheckreg);
        $scope.form.$setPristine();
        window.location = "/registrationcomplete/" + amount;
      }
    });
  }

});

// NEW MEMBER
app.controller("newmemberCtrl", function ($scope, $http, Countries, MDY) {
  $scope.usernametaken = '';
  $scope.emailtaken = '';
  $scope.emailtaken2 = '';
  $scope.temppass = '';
  $scope.success = false;
  $scope.process = false;
  $scope.countries = Countries.list();

  $scope.day = MDY.day();
  $scope.month = MDY.month();
  $scope.year = MDY.year();
  $scope.passmin = false;
  var oriUser = angular.copy($scope.user);

  $http({
    url: API_URL + "/bnb/cnamelist",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {
    $scope.dataCentername = data;
  }).error(function (data) {
    $scope.status = status;
  });

  $scope.register = function (user) {

    if (user.password.length < 6) {
      $scope.passmin = true;
      return
    } else {
      $scope.process = true;
      $http.post(API_URL + '/members/newmember', user).success(function (response) {
        if (response.hasOwnProperty('usernametaken')) {
          $scope.usernametaken = response.usernametaken;
          $scope.process = false;
          $scope.passmin = false;
        } else if (response.hasOwnProperty('emailtaken')) {
          $scope.emailtaken = response.emailtaken;
          $scope.process = false;
          $scope.passmin = false;
        } else if (response.hasOwnProperty('emailtaken2')) {
          $scope.emailtaken = response.emailtaken2;
          $scope.process = false;
          $scope.passmin = false;
        } else if (response.hasOwnProperty('temppass')) {
          $scope.temppass = response.temppass;
          $scope.process = false;
          $scope.passmin = false;
        } else {
          $scope.usernametaken = '';
          $scope.emailtaken = '';
          $scope.emailtaken2 = '';
          $scope.temppass = '';
          $scope.success = true;
          $('html, body').animate({
            scrollTop: $("#donat").offset().top
          }, 2000);
          $scope.user = angular.copy(oriUser);
          $scope.form.$setPristine();
        }
        $scope.response = response;
      });
    }
  }
    // console.log($scope.day);
    $scope.passmin = false;
    var oriUser = angular.copy($scope.user);
    $scope.register = function (user) {
        // console.log(user);

        if (user.password.length < 6) {
          $scope.passmin = true;
          return
        } else {
          $scope.process = true;
          $http.post(API_URL + '/members/newmember', user).success(function (response) {
            if (response.hasOwnProperty('usernametaken')) {
              $scope.usernametaken = response.usernametaken;
              $scope.process = false;
              $scope.passmin = false;
            } else if (response.hasOwnProperty('emailtaken')) {
              $scope.emailtaken = response.emailtaken;
              $scope.process = false;
              $scope.passmin = false;
            } else if (response.hasOwnProperty('emailtaken2')) {
              $scope.emailtaken2 = response.emailtaken2;
              $scope.process = false;
              $scope.passmin = false;
            } else if (response.hasOwnProperty('temppass')) {
              $scope.temppass = response.temppass;
              $scope.process = false;
              $scope.passmin = false;
            } else {
              $scope.usernametaken = '';
              $scope.emailtaken = '';
              $scope.emailtaken2 = '';
              $scope.temppass = '';
              $scope.success = true;
              $scope.process = false;
              $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <h1 style="color:#000;font-size:30px;">Thank you for registering!</h1> <div style="margin-top:-10px;padding:20px;font-size:15px;">You are now completely registered you can proceed logging in and make your donation. <a href="/donation"  style="position: absolute; top: 15px; right: 30px; text-align:center;">Click here to login</a></div></div>', {
                opacity: 80,
                overlayCss: {backgroundColor: "#fff"},
                overlayClose: true
              });
              $scope.user = angular.copy(oriUser);
              $scope.form.$setPristine();
            }
            $scope.response = response;
                // console.log($scope.response);
              });
}
}

var memsid = document.getElementById("memsid").value;
$http({
  url: API_URL + "/members/getinfo/" + memsid ,
  method: "GET",
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
}).success(function (data, status, headers, config) {
  $scope.user = data;

        // console.log(data.location);
        for(var x in $scope.countries){
          if($scope.countries[x].name == data.location){
            $scope.user.location = $scope.countries[x];
                // console.log(x);
              }
            }
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });

        });

app.controller("DonateCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {

  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 10.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';


  var d = new Date();
  var n = d.getFullYear();
  $scope.donday = MDYform.day();
  $scope.donmonth = MDYform.month();
  $scope.donyear = MDYform.year();

  $scope.chtli = function () {

    $scope.new = "already";
      // $('#donate2').val('already');        
    };
    $scope.reccur_unit = [
    {
      'name': 'months'
    },
    {
      'name': 'days'
    }
    ]

    $scope.scrollToHash = function(){
        // console.log('scroll to triggered');
        $location.hash('signup');
        $anchorScroll();
      }

      $scope.submitecheck = function(data, amount, email){
        // console.log('Submit Echeck');
        data['amount'] = amount;
        data['email'] = email;
        $scope.reccur_success = 2;
        $http.post(API_URL + '/donate/echeck', data).success(function (response) {
            // console.log('Should Echeck Recieve Respon');
            // console.log(response);
            if(data.recurpayment2){
              $scope.reccur_success = 3;
              if(response.code=='I00001'){
                $scope.back_button = 1;
                window.location = "/complete/" + amount;
                    //$scope.reccur_message = "Your donation has been successfully processed.";
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.text;
                  }
                }else{
                  $scope.reccur_success = 3;
                  if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + amount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 2;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.error;
                  }
                }
              });
}

$scope.submitcredit = function(data, amount, email){
  data['amount'] = amount;
  data['email'] = email;
        // console.log(data);
        $scope.reccur_success = 2;

        $http.post(API_URL + '/donate/creditcard', data).success(function (response) {
        // console.log('Should Recieve Respon');
        // console.log(response);
        if(data.recurpayment){
          $scope.reccur_success = 3;
          if(response.code=='I00001'){
            $scope.back_button = 1;
            window.location = "/complete/" + amount;
                //$scope.reccur_message = "Your donation has been successfully processed.";
              }else{
                $scope.back_button = 3;
                $scope.reccur_message = response.text;
              }
            }else{
              $scope.reccur_success = 3;
              if(response.hasOwnProperty('success')){
            // console.log('success');
            $scope.back_button = 1;
            window.location = "/complete/" + amount;
            //$scope.reccur_message = response.success;
          }else if(response.error == null){
            $scope.back_button = 3;
            $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
          }else{
            $scope.back_button = 3;
            $scope.reccur_message = response.error;
          }
        }
      });
}



$scope.submitcreditworeg = function(ccworeg,donateamount){
  console.log('creditcard woreg');
  console.log(ccworeg);
  console.log(donateamount);
  $scope.reccur_success = 2;
  $scope.process = true;
  ccworeg['amount'] = donateamount;

  $http.post(API_URL + '/donate/creditcardworeg', ccworeg).success(function (response) {
      // console.log('Should Recieve Respon');
      // console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // console.log('success');
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
        //$scope.reccur_message = response.success;
      }else if(response.error == null){
        $scope.process = false;
        $scope.back_button = 3;
        $scope.reccur_success = 1;
        $scope.errorpayment = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        // console.log(response);
        $scope.back_button = 3;        
        $scope.reccur_success = 1;
        $scope.process = false;
        $scope.reccur_message = response.error;        
        $scope.errorpayment = response.error + " And try again.";
      }
    });
}


$scope.submitecheckworeg = function(checkworeg,donateamount){
  console.log('echeck woreg');
  console.log(checkworeg);
  console.log(donateamount);
  $scope.reccur_success = 2;
  $scope.process = true;

  checkworeg['amount'] = donateamount;

  $http.post(API_URL + '/donate/echeckcardworeg', checkworeg).success(function (response) {
      // console.log('Should Recieve Respon');
      // console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // console.log('success');
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
        //$scope.reccur_message = response.success;
      }else if(response.error == null){
        $scope.process = false;
        $scope.back_button = 3;
        $scope.reccur_success = 1;
        $scope.errorpayment = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 3;        
        $scope.reccur_success = 1;
        $scope.process = false;
        $scope.reccur_message = response.error;        
        $scope.errorpayment = response.error + " And try again.";
      }
    });
}
$scope.submitpaypalworeg = function(howdidyoulearn, cname){
  var cn='';
  if(cname == undefined){
    cn = '';
  }else{
    cn = cname.replace(/\s+/g, '*');
  }

  var hdyl='';

  if(howdidyoulearn == undefined){
    hdyl = '';
  }else{
    hdyl = howdidyoulearn.replace(/\s+/g, '*');
  }
  var val = 'user@email.com other ' + hdyl + ' ' + cn;
  $('#paypalcustom').val(val);
  console.log(val);
  $('#paypalsingle').submit();
  $scope.paypalbutton = true;
}

$scope.back_click = function(){
        // console.log('back click');
        $scope.reccur_success =1;
      }

      $scope.clickDonate = function (data)
      {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
          if (response.hasOwnProperty('error')) {
            $scope.errormessage = response.error;
          } else {
            $scope.creditcard123 = true;
            $scope.newalready = false;
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.paypalformshow1 = true;
            $scope.membername = response.success;
            $scope.statusmessage = '';
            $scope.forget = true;
            $scope.create = true;
            $scope.process = false;
            $scope.chossemet = false;

          }
          $scope.process = false;
        });
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.errormessage = '';
          $scope.paypalformshow = true;
          $scope.membername = response.success;
        }
        $scope.process = false;
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.minimumdonation = '#999';
        }
      };
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
      $scope.statusmessage = '';
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        // console.log($scope.showemail);

      }
      $scope.paypalformshow1 = false;
      $scope.create = false;
      $scope.forget = false;
      $scope.statusmessage = '';
      $scope.disp = false;
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

      }
      $scope.sendemail = function (data) {
        // console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
          if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
            $scope.statusmessage = response.error;
          } else {
            $scope.paypalformshow1 = false;
            $scope.statusmessage = 'Your requested details have been sent to your email!';
            $scope.errormessage = '';
          }
          $scope.emailPurpose = null;
          $scope.showemail = false;
          $scope.emailSent = false;
        });
      }
      $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
      }
    });

app.controller("DonateOthersCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {
  $scope.paypalcustom = '';
  $scope.paypalbutton = false;
  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 20.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';
  $scope.others = false;
  $scope.amountlimit = false;
  $scope.dataCentername = [
  {'centername':'NJ-CGI'},
  {'centername' : 'NJ-Madison'},
  {'centername': 'NJ-Ramsey'},
  {'centername' : 'NJ-Ridgefield'},
  {'centername':'NJ-Wayne'},
  {'centername':'NJ-Wyckoff'},
  {'centername':'NY-Babylon Village'},
  {'centername': 'NY-Stony Point'},
  {'centername':'NY-Bay Ridge'},
  {'centername':'NY-Bayside'},
  {'centername':'NY-Bronx'},
  {'centername':'NY-Brooklyn Heights'},
  {'centername':'NY-East Meadow'},
  {'centername':'NY-Flushing'},
  {'centername':'NY-Forest Hills'},
  {'centername':'NY-Franklin Square'},
  {'centername':'NY-Great Neck'},
  {'centername':'NY-Kew Gardens Hills'},
  {'centername':'NY-Lynbrook'},
  {'centername':'NY-Manhattan'},
  {'centername':'NY-Mineola'},
  {'centername':'NY-New City'},
  {'centername':'NY-New Rochelle'},
  {'centername':'NY-Smithtown'},
  {'centername':'NY-Sunnyside'},
  {'centername':'NY-Syosset'},
  {'centername':'NY-Union Square'},
  {'centername':'NY-Westchester'}
  ];
  $scope.emailcheck = function (user) {
   $scope.invalidemail = false;
   var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
   if(validemail == false){
    $scope.invalidemail = true;
  }
  else{                
    $scope.invalidemail = false;
  }

};

$scope.otheramount = function(){
  $scope.others = true;  
  $scope.donateamount = '';  
  $scope.amountlimit = false;     
} 



$scope.fixamount = function(amount, others){
  $scope.donateamount = amount;
  if(amount < 20){
    $scope.paypalbutton = true;
    $scope.amountlimit = true;
  }else{
    $scope.donateamount = amount;
    $scope.amountlimit = false;            
    $scope.paypalbutton = false;  
  }
  if(amount == '' ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;            
  }
  if(amount <= 19 ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;            
  }
  $scope.others=others;
}

$scope.fixamount2 = function(amount, others){

  $scope.donateamount = amount;
  console.log($scope.donateamount);
  if(amount < 30){
    $scope.paypalbutton = true;
    $scope.amountlimit = true;
  }else{
    $scope.donateamount = amount;
    $scope.amountlimit = false;
    $scope.paypalbutton = false;
  }
  if(amount == '' ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  if(amount <= 29 ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  $scope.others=others;
}

$scope.scrollToHash = function(){
  $location.hash('signup');
  $anchorScroll();
}

$scope.submitecheck = function(data, email, target){

  console.log(data);
  console.log($scope.donateamount);
  data['amount'] = $scope.donateamount;

  data['email'] = email;
  data['target'] = target;
        // console.log('Submit Echeck');
        //data['amount'] = amount;
        //data['email'] = email;
        //

        $scope.reccur_success = 2;
        $http.post(API_URL + '/donate/other/echeck', data).success(function (response) {
            // console.log('Should Echeck Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 2;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.submitcredit = function(data, email, target){

        data['amount'] = $scope.donateamount;
        data['email'] = email;
        //data['email'] = email;
        // console.log(data);
        $scope.reccur_success = 2;
        data['target'] = target;
        $http.post(API_URL + '/donate/other/creditcard', data).success(function (response) {
            // console.log('Should Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 3;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 3;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.back_click = function(){
        // console.log('back click');
        $scope.reccur_success =1;
      }

      $scope.clickDonate = function (data)
      {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
          if (response.hasOwnProperty('error')) {
            $scope.errormessage = response.error;
          } else {
            $scope.creditcard123 = true;
            $scope.newalready = false;
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.paypalformshow1 = true;
            $scope.membername = response.success;
            $scope.statusmessage = '';
            $scope.forget = true;
            $scope.create = true;
            $scope.process = false;
            $scope.chossemet = false;

          }
          $scope.process = false;
        });
      };
      $scope.change = function (amount) {
        if (amount <= 20) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.errormessage = '';
          $scope.paypalformshow = true;
          $scope.membername = response.success;
        }
        $scope.process = false;
      };
      $scope.change = function (amount) {
        if (amount <= 20) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.minimumdonation = '#999';
        }
      };
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
      $scope.statusmessage = '';
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        // console.log($scope.showemail);

      }
      $scope.paypalformshow1 = false;
      $scope.create = false;
      $scope.forget = false;
      $scope.statusmessage = '';
      $scope.disp = false;
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

      }
      $scope.sendemail = function (data) {
        // console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
          if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
            $scope.statusmessage = response.error;
          } else {
            $scope.paypalformshow1 = false;
            $scope.statusmessage = 'Your requested details have been sent to your email!';
            $scope.errormessage = '';
          }
          $scope.emailPurpose = null;
          $scope.showemail = false;
          $scope.emailSent = false;
        });
      }
      $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
      }
      $scope.submitpaypal = function(howdidyoulearn, cname){
        var cn='';
        if(cname == undefined){
          cn = '';
        }else{
          cn = cname.replace(/\s+/g, '*');
        }

        var hdyl='';

        if(howdidyoulearn == undefined){
          hdyl = '';
        }else{
          hdyl = howdidyoulearn.replace(/\s+/g, '*');
        }
        var val = 'user@email.com other ' + hdyl + ' ' + cn;
        $('#paypalcustom').val(val);
        console.log(val);
        $('#paypalsingle').submit();
        $scope.paypalbutton = true;
      }
    });

app.controller("DonateHoustonCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {
  $scope.paypalcustom = '';
  $scope.paypalbutton = false;
  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 20.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';
  $scope.others = false;
  $scope.amountlimit = false;
  $scope.dataCentername = [
  {'centername':'TX - Belt Line'},
  {'centername':'TX - Champion'},
  {'centername':'TX - Copperfield'},
  {'centername':'TX - Katy'},
  {'centername':'TX - Kingwood'},
  {'centername':'TX - Missouri City'},
  {'centername':'TX - The Woodlands'},
  {'centername':'TX - Voss'},
  {'centername':'TX - Westpark'}
  ];
  $scope.emailcheck = function (user) {
   $scope.invalidemail = false;
   var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
   if(validemail == false){
    $scope.invalidemail = true;
  }
  else{                
    $scope.invalidemail = false;
  }

};

$scope.otheramount = function(){
  $scope.others = true;  
  $scope.donateamount = '';  
  $scope.amountlimit = false;     
} 


$scope.fixamount2 = function(amount, others){

  $scope.donateamount = amount;
  console.log($scope.donateamount);
  if(amount < 25){
    $scope.paypalbutton = true;
    $scope.amountlimit = true;
  }else{
    $scope.donateamount = amount;
    $scope.amountlimit = false;
    $scope.paypalbutton = false;
  }
  if(amount == '' ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  if(amount <= 24 ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  $scope.others=others;
}

$scope.scrollToHash = function(){
  $location.hash('signup');
  $anchorScroll();
}

$scope.submitecheck = function(data, email, target){

  console.log(data);
  console.log($scope.donateamount);
  data['amount'] = $scope.donateamount;

  data['email'] = email;
  data['target'] = target;
        // console.log('Submit Echeck');
        //data['amount'] = amount;
        //data['email'] = email;
        //

        $scope.reccur_success = 2;
        $http.post(API_URL + '/donate/other/echeck', data).success(function (response) {
            // console.log('Should Echeck Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 2;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.submitcredit = function(data, email, target){

        data['amount'] = $scope.donateamount;
        data['email'] = email;
        //data['email'] = email;
        console.log(data);
        $scope.reccur_success = 2;
        data['target'] = target;
        console.log(target);
        $http.post(API_URL + '/donate/other/creditcard', data).success(function (response) {
            // console.log('Should Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 3;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 3;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.back_click = function(){
        // console.log('back click');
        $scope.reccur_success =1;
      }

      $scope.clickDonate = function (data)
      {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
          if (response.hasOwnProperty('error')) {
            $scope.errormessage = response.error;
          } else {
            $scope.creditcard123 = true;
            $scope.newalready = false;
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.paypalformshow1 = true;
            $scope.membername = response.success;
            $scope.statusmessage = '';
            $scope.forget = true;
            $scope.create = true;
            $scope.process = false;
            $scope.chossemet = false;

          }
          $scope.process = false;
        });
      };
      $scope.change = function (amount) {
        if (amount <= 20) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.errormessage = '';
          $scope.paypalformshow = true;
          $scope.membername = response.success;
        }
        $scope.process = false;
      };
      $scope.change = function (amount) {
        if (amount <= 20) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.minimumdonation = '#999';
        }
      };
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
      $scope.statusmessage = '';
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        // console.log($scope.showemail);

      }
      $scope.paypalformshow1 = false;
      $scope.create = false;
      $scope.forget = false;
      $scope.statusmessage = '';
      $scope.disp = false;
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

      }
      $scope.sendemail = function (data) {
        // console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
          if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
            $scope.statusmessage = response.error;
          } else {
            $scope.paypalformshow1 = false;
            $scope.statusmessage = 'Your requested details have been sent to your email!';
            $scope.errormessage = '';
          }
          $scope.emailPurpose = null;
          $scope.showemail = false;
          $scope.emailSent = false;
        });
      }
      $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
      }
      $scope.submitpaypal = function(howdidyoulearn, cname){
        var cn='';
        if(cname == undefined){
          cn = '';
        }else{
          cn = cname.replace(/\s+/g, '*');
        }

        var hdyl='';

        if(howdidyoulearn == undefined){
          hdyl = '';
        }else{
          hdyl = howdidyoulearn.replace(/\s+/g, '*');
        }
        var val = 'user@email.com houston ' + hdyl + ' ' + cn;
        $('#paypalcustom').val(val);
        console.log(val);
        $('#paypalsingle').submit();
        $scope.paypalbutton = true;
      }
    });

app.controller("DonateSeattleCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {
  $scope.paypalcustom = '';
  $scope.paypalbutton = false;
  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 10.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';
  $scope.others = false;
  $scope.amountlimit = false;
  $scope.dataCentername = [
  {'centername':'WA - Everett'},
  {'centername':'WA - Kirkland'},
  {'centername':'WA - Lynnwood'},
  {'centername':'WA - Mill Creek'},
  {'centername':'WA - Ravenna Park'},
  {'centername':'WA - Tacoma'}
  ];
  $scope.emailcheck = function (user) {
   $scope.invalidemail = false;
   var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
   if(validemail == false){
    $scope.invalidemail = true;
  }
  else{                
    $scope.invalidemail = false;
  }

};

$scope.otheramount = function(){
  $scope.others = true;  
  $scope.donateamount = '';  
  $scope.amountlimit = false;     
} 


$scope.fixamount2 = function(amount, others){

  $scope.donateamount = amount;
  console.log($scope.donateamount);
  if(amount < 10){
    $scope.paypalbutton = true;
    $scope.amountlimit = true;
  }else{
    $scope.donateamount = amount;
    $scope.amountlimit = false;
    $scope.paypalbutton = false;
  }
  if(amount == '' ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  if(amount <= 10 ){
    $scope.paypalbutton = true;
  }else{
    $scope.paypalbutton = false;
  }
  $scope.others=others;
}

$scope.scrollToHash = function(){
  $location.hash('signup');
  $anchorScroll();
}

$scope.submitecheck = function(data, email, target){

  console.log(data);
  console.log($scope.donateamount);
  data['amount'] = $scope.donateamount;

  data['email'] = email;
  data['target'] = target;
        // console.log('Submit Echeck');
        //data['amount'] = amount;
        //data['email'] = email;
        //

        $scope.reccur_success = 2;
        $http.post(API_URL + '/donate/other/echeck', data).success(function (response) {
            // console.log('Should Echeck Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 2;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.submitcredit = function(data, email, target){

        data['amount'] = $scope.donateamount;
        data['email'] = email;
        //data['email'] = email;
        console.log(data);
        $scope.reccur_success = 2;
        data['target'] = target;
        console.log(target);
        $http.post(API_URL + '/donate/other/creditcard', data).success(function (response) {
            // console.log('Should Recieve Respon');
            // console.log(response);
            $scope.reccur_success = 3;
            if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + $scope.donateamount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 3;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 3;
                    $scope.reccur_message = response.error;
                  }
                });
      }

      $scope.back_click = function(){
        // console.log('back click');
        $scope.reccur_success =1;
      }

      $scope.clickDonate = function (data)
      {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
          if (response.hasOwnProperty('error')) {
            $scope.errormessage = response.error;
          } else {
            $scope.creditcard123 = true;
            $scope.newalready = false;
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.paypalformshow1 = true;
            $scope.membername = response.success;
            $scope.statusmessage = '';
            $scope.forget = true;
            $scope.create = true;
            $scope.process = false;
            $scope.chossemet = false;

          }
          $scope.process = false;
        });
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.errormessage = '';
          $scope.paypalformshow = true;
          $scope.membername = response.success;
        }
        $scope.process = false;
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.minimumdonation = '#999';
        }
      };
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
      $scope.statusmessage = '';
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        // console.log($scope.showemail);

      }
      $scope.paypalformshow1 = false;
      $scope.create = false;
      $scope.forget = false;
      $scope.statusmessage = '';
      $scope.disp = false;
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

      }
      $scope.sendemail = function (data) {
        // console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
          if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
            $scope.statusmessage = response.error;
          } else {
            $scope.paypalformshow1 = false;
            $scope.statusmessage = 'Your requested details have been sent to your email!';
            $scope.errormessage = '';
          }
          $scope.emailPurpose = null;
          $scope.showemail = false;
          $scope.emailSent = false;
        });
      }
      $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
      }
      $scope.submitpaypal = function(howdidyoulearn, cname){
        var cn='';
        if(cname == undefined){
          cn = '';
        }else{
          cn = cname.replace(/\s+/g, '*');
        }

        var hdyl='';

        if(howdidyoulearn == undefined){
          hdyl = '';
        }else{
          hdyl = howdidyoulearn.replace(/\s+/g, '*');
        }
        var val = 'user@email.com seattle ' + hdyl + ' ' + cn;
        $('#paypalcustom').val(val);
        console.log(val);
        $('#paypalsingle').submit();
        $scope.paypalbutton = true;
      }
    });


app.controller("hltCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {
  console.log('hltCtrl');
  $scope.paypalcustom = '';
  $scope.paypalbutton = false;
  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 10.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';
  $scope.others = false;
  $scope.amountlimit = false;
  $scope.trainingDate2 = false;
  // load centername to select item from centername table
  $http({
    url: API_URL + "/bnb/cnamelist",
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).success(function (data) {
    $scope.dataCentername = data;
  }).error(function (data) {
    $scope.status = status;
  });

 
  $scope.ccBillInfo = function (user,cc) {
    console.log(user);
    cc['billingfname'] = user.fname;
    cc['billinglname'] = user.lname;
    cc['al1'] = user.address1;
    cc['al2'] = user.address2;
    cc['city'] = user.city;
    cc['state'] = user.state;
    cc['zip'] = user.userzip;
    cc['country'] = user.country;
    cc['billingemail'] = user.email;
    $scope.cc = cc;
    
  }; 

  $scope.checkBillInfo = function (user,check) {
    console.log(user);
    check['billingfname'] = user.fname;
    check['billinglname'] = user.lname;
    check['al1'] = user.address1;
    check['al2'] = user.address2;
    check['city'] = user.city;
    check['state'] = user.state;
    check['zip'] = user.userzip;
    check['country'] = user.country;
    check['billingemail'] = user.email;
    $scope.check = check;
    
  };

  $scope.td = function (trainingDate) {
    if(trainingDate != undefined || trainingDate != '' ){
      $scope.trainingDate2 = true;
    console.log('trainingDate');
    }
  };

  $scope.emailcheck = function (email) {
    console.log(email);
    $scope.invalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email);
    if(validemail == false){
      $scope.invalidemail = true;
    }
    else{                
      $scope.invalidemail = false;
    }

  };

  $scope.billingemailcheck = function (email) {
    console.log(email);
    $scope.billingemailcheckinvalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email);
    if(validemail == false){
      $scope.billingemailcheckinvalidemail = true;
    }
    else{                
      $scope.billingemailcheckinvalidemail = false;
    }

  };


  $scope.scrollToHash = function(){
    $location.hash('signup');
    $anchorScroll();
  }

  $scope.submitecheck = function(data, user){

    console.log($scope.inclusivepay);
    console.log($scope.trainingDate);
    console.log(user);
    data['amount'] = $scope.inclusivepay;
    data['trainingDate'] = $scope.trainingDate;
    data['userfname'] = user.fname;
    data['userlname'] = user.lname;
    data['useraddress'] = user.address1;
    data['useraddress2'] = user.address2;
    data['usercity'] = user.city;
    data['userzipcode'] = user.userzip;
    data['usercountry'] = user.country.name;
    data['useremail'] = user.email;
    data['userstate'] = user.state;
    data['howdidyoulearn'] = user.howdidyoulearn;
    data['cname'] = user.cname;
    console.log(data);

    $scope.reccur_success = 2;
    $http.post(API_URL + '/programPayment/echeck', data).success(function (response) {
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.inclusivepay;
      }else if(response.error == null){
        $scope.back_button = 2;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 2;
        $scope.reccur_message = response.error;
      }
    });
  }

  $scope.submitcredit = function(data, user){
    
    console.log($scope.inclusivepay);
    console.log($scope.trainingDate);
    console.log(user);
    data['amount'] = $scope.inclusivepay;
    data['trainingDate'] = $scope.trainingDate;
    data['userfname'] = user.fname;
    data['userlname'] = user.lname;
    data['useraddress'] = user.address1;
    data['useraddress2'] = user.address2;
    data['usercity'] = user.city;
    data['userzipcode'] = user.userzip;
    data['usercountry'] = user.country.name;
    data['useremail'] = user.email;
    data['userstate'] = user.state;
    data['howdidyoulearn'] = user.howdidyoulearn;
    data['cname'] = user.cname;
    console.log(data);


    $scope.reccur_success = 2;
    $http.post(API_URL + '/programPayment/creditcard', data).success(function (response) {
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.inclusivepay;
      }else if(response.error == null){
        $scope.back_button = 3;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 3;
        $scope.reccur_message = response.error;
      }
    });
  }

  $scope.back_click = function(){
    $scope.reccur_success =1;
  }

  $scope.emailPurpose = null;
  $scope.showemail = false;
  $scope.emailSent = false;
  $scope.statusmessage = '';
  $scope.setEmailFunc = function (purpose) {
    $scope.statusmessage = '';
    $scope.emailPurpose = purpose;
    $scope.showemail = true;

  }
  $scope.paypalformshow1 = false;
  $scope.create = false;
  $scope.forget = false;
  $scope.statusmessage = '';
  $scope.disp = false;
  $scope.setEmailFunc = function (purpose) {
    $scope.statusmessage = '';
    $scope.emailPurpose = purpose;
    $scope.showemail = true;
    $scope.paypalformshow1 = true;

  }
 
  $scope.hideForm = function () {
    $scope.emailPurpose = null;
    $scope.showemail = false;
    $scope.emailSent = false;
    $scope.paypalformshow1 = false;
  }
  $scope.submitpaypal = function(howdidyoulearn, cname){
    var cn='';
    if(cname == undefined){
      cn = 'null';
    }else{
      cn = cname.replace(/\s+/g, '*');
    }

    var hdyl='';

    if(howdidyoulearn == undefined){
      hdyl = 'null';
    }else{
      hdyl = howdidyoulearn.replace(/\s+/g, '*');
    }

    var trainingDate = $scope.trainingDate;
    
    var val = 'user@email.com hlt ' + hdyl + ' ' + cn + ' ' + trainingDate;
    $('#paypalcustom').val(val);
    console.log(val);
    $('#paypalsingle').submit();
    $scope.paypalbutton = true;
  }
});

app.controller("eventslistCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries, $compile, $sce) {
  console.log('events list')
  var offset = 0;
  var num = 6;
  $scope.loading = false;
  $scope.loadmorebot = true;
  $scope.BASE_URL = BASE_URL;
  var loadEvents = function () {
    $http({
      url: API_URL+"/events/eventslist/"+ offset +"/"+ num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data.data;
      $scope.eventcounts = data.count; 
      if(data.data.length == data.count){
        $scope.loadmorebot = false;
      }       
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  loadEvents();
  $scope.showmore = function(){
    num = num + num;
    loadEvents();
  } 
});

app.controller("eventsCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries, $compile, $sce) {

  $scope.paypalcustom = '';
  $scope.paypalbutton = false;
  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 10.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';
  $scope.others = false;
  $scope.amountlimit = false;

  $scope.emailcheck = function (user) {
    $scope.invalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.invalidemail = true;
    }
    else{                
      $scope.invalidemail = false;
    }

  };

  $scope.otheramount = function(){
    $scope.others = true;  
    $scope.donateamount = '';  
    $scope.amountlimit = false;     
  } 

  $scope.fixamount2 = function(amount, others, min){
    console.log(min);

    $scope.donateamount = amount;
    console.log($scope.donateamount);
    if(amount < min){
      $scope.paypalbutton = true;
      $scope.amountlimit = true;
    }else{
      $scope.donateamount = amount;
      $scope.amountlimit = false;
      $scope.paypalbutton = false;
    }
    if(amount == '' ){
      $scope.paypalbutton = true;
    }else{
      $scope.paypalbutton = false;
    }
    if(amount < min ){
      $scope.paypalbutton = true;
    }else{
      $scope.paypalbutton = false;
    }
    $scope.others=others;
  }

  $scope.scrollToHash = function(){
    $location.hash('signup');
    $anchorScroll();
  }

  $scope.submitecheck = function(data, email, target, donatedto, title){

    data['amount'] = $scope.donateamount;
    data['email'] = email;
    data['target'] = target;

    $scope.reccur_success = 2;
    $http.post(API_URL + '/donate/other/echeck', data).success(function (response) {
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
      }else if(response.error == null){
        $scope.back_button = 2;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 2;
        $scope.reccur_message = response.error;
      }
    });
  }

  $scope.submitcredit = function(data, email, target, donatedto, title){

    data['amount'] = $scope.donateamount;
    data['email'] = email;
    $scope.reccur_success = 2;
    data['target'] = target;
    $http.post(API_URL + '/donate/other/creditcard', data).success(function (response) {
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
      }else if(response.error == null){
        $scope.back_button = 3;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 3;
        $scope.reccur_message = response.error;
      }
    });
  }

  $scope.back_click = function(){
    $scope.reccur_success =1;
  }

  $scope.clickDonate = function (data)
  {

    $scope.process = true;
    $http.post(API_URL + '/members/checkuser', data).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.errormessage = response.error;
      } else {
        $scope.creditcard123 = true;
        $scope.newalready = false;
        $scope.errormessage = '';
        $scope.paypalformshow = true;
        $scope.paypalformshow1 = true;
        $scope.membername = response.success;
        $scope.statusmessage = '';
        $scope.forget = true;
        $scope.create = true;
        $scope.process = false;
        $scope.chossemet = false;

      }
      $scope.process = false;
    });
  };
  $scope.emailPurpose = null;
  $scope.showemail = false;
  $scope.emailSent = false;
  $scope.statusmessage = '';
  $scope.setEmailFunc = function (purpose) {
    $scope.statusmessage = '';
    $scope.emailPurpose = purpose;
    $scope.showemail = true;

  }
  $scope.paypalformshow1 = false;
  $scope.create = false;
  $scope.forget = false;
  $scope.statusmessage = '';
  $scope.disp = false;
  $scope.setEmailFunc = function (purpose) {
    $scope.statusmessage = '';
    $scope.emailPurpose = purpose;
    $scope.showemail = true;
    $scope.paypalformshow1 = true;

  }
  $scope.sendemail = function (data) {
    data.type = $scope.emailPurpose;
    $http.post(API_URL + '/members/sendmail', data).success(function (response) {
      if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
        $scope.statusmessage = response.error;
      } else {
        $scope.paypalformshow1 = false;
        $scope.statusmessage = 'Your requested details have been sent to your email!';
        $scope.errormessage = '';
      }
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
    });
  }
  $scope.hideForm = function () {
    $scope.emailPurpose = null;
    $scope.showemail = false;
    $scope.emailSent = false;
    $scope.paypalformshow1 = false;
  }
  $scope.submitpaypal = function(howdidyoulearn, cname, donateto){
    var cn='';
    if(cname == undefined){
      cn = '';
    }else{
      cn = cname.replace(/\s+/g, '*');
    }
    var hdyl='';
    if(howdidyoulearn == undefined){
      hdyl = '';
    }else{
      hdyl = howdidyoulearn.replace(/\s+/g, '*');
    }
    var val = 'user@email.com events ' + donateto + ' ' + hdyl + ' ' + cn;
    $('#paypalcustom').val(val);
    console.log(val);
    $('#paypalsingle').submit();
    $scope.paypalbutton = true;
  }

});

app.controller("SeattleVendorCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries, $q) {

  $scope.processing = false;
  $scope.backtoform = false;
  $scope.sucess_process = false;

  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();

  $scope.dataCentername = [
  {'centername':'WA - Everett'},
  {'centername':'WA - Kirkland'},
  {'centername':'WA - Lynnwood'},
  {'centername':'WA - Mill Creek'},
  {'centername':'WA - Ravenna Park'},
  {'centername':'WA - Tacoma'}
  ];

  $scope.invalidemail = false;
  $scope.ccinvalidemail = false;
  $scope.master = {};
  var orig = angular.copy($scope.master);

  $scope.back_click = function () {    
    $scope.processing = false;
    $scope.backtoform = false;
    $scope.sucess_process = false;
  };

  $scope.emailcheck = function (vendor) {
    $scope.invalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(vendor.email);
    if(validemail == false){
      $scope.invalidemail = true;
    }else{                
      $scope.invalidemail = false;
    }
  };

  $scope.ccemailcheck = function (cc) {
    $scope.ccinvalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(cc.ccemail);
    if(validemail == false){
      $scope.ccinvalidemail = true;
    }else{                
      $scope.ccinvalidemail = false;
    }
  };

  $scope.computepayment = function (vendor,vendortype,electricity) {
    console.log(vendor.table); 
    console.log(electricity);
    if(vendor.table != null || vendor.table != undefined){
      if(vendortype == 'Profit'){
        if(electricity == undefined || electricity == 'no'){
          var tablepay = 100 * vendor.table;
        }else{
          var tablepay = 100 * vendor.table + 56;
        }
      }else if(vendortype == 'Non-Profit'){ 
        if(electricity == undefined || electricity == 'no'){
          var tablepay = 50 * vendor.table;
        }else{
          var tablepay = 50 * vendor.table + 56;
        }
      }      
    }else{
      var tablepay = '';
      $scope.refreshradio = true;
    }
    vendor.totalpay = tablepay;      
    vendor.totalpayment = vendor.totalpay;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
  };

  $scope.addelectricity = function (vendor,electricity,value,vendortype) {
    console.log(vendor.totalpayment);
    console.log(vendor.table);
    console.log(electricity);
    // console.log(vendor.totalpay);
    // console.log(value);

    if(vendortype == 'Profit'){
      var total = vendor.table * 100;
      if(vendor.totalpayment > total){
        vendor.totalpayment = vendor.totalpayment - 56;
        $scope.electricity = 'no';
      }else if(vendor.totalpayment == total && value == 'yes'){
        vendor.totalpayment = vendor.totalpayment + 56;
        $scope.electricity = 'yes';
      }
    }else if(vendortype == 'Non-Profit'){ 
      var total = vendor.table * 50;
      if(vendor.totalpayment > total){
        vendor.totalpayment = vendor.totalpayment - 56;
        $scope.electricity = 'no';
      }else if(vendor.totalpayment == total && value == 'yes'){
        vendor.totalpayment = vendor.totalpayment + 56;
        $scope.electricity = 'yes';
      }
    } 

    // if(vendor.table != null || vendor.table != undefined){
    //   var tabletotal = vendor.totalpayment;
    //   if(value == 'yes'){
    //     var tablepay = tabletotal + 56;
    //     $scope.electricity = 'yes';
    //   }else if(value == 'no'){        
    //     var tablepay = vendor.totalpay;
    //     $scope.electricity = 'no';
    //   }
    //   vendor.totalpayment = tablepay;
    //   console.log(vendor.totalpayment);
    // }else{
    //   var tablepay = '';
    //   vendor.totalpayment = tablepay;      
    // }
  };

  $scope.selecttype = function (vendor) {
    $('input[name=electricity]').attr('checked',false);
    vendor.table = []; 
    vendor.totalpayment = []; 
    vendor.totalpay = []; 

  };

  $scope.submitvendor = function (vendor,vendortype,electricity) {
    console.log(vendor);
    // console.log(ccvendor);
    console.log(vendortype);
    console.log(electricity);
    console.log('-======-=-=-=-=-=-=-=-=============-');
    vendor['electricity'] = electricity;
    vendor['vendortype'] = vendortype;
  
    console.log(vendor);

    $scope.processing = true;

    $http.post(API_URL + '/vendor/mailcheck', vendor).success(function (response) {
      console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // $scope.back_button = 1;
        $scope.processing = false;
        $scope.sucess_process = true;
        $scope.sucess_msg = 'Thank you for joining us! Please mail your payment check to 10702 NE 68th St, Kirkland, WA 98033 and make the checks payable to ECO.';
        // window.location = "/complete/" + $scope.donateamount;
      }else if(response.error == null){
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = response.error;
      }
    });
  };


  $scope.submitvendorcc = function (vendor,ccvendor,vendortype,electricity) {
    console.log(vendor);
    console.log(ccvendor);
    console.log(vendortype);
    console.log(electricity);
    console.log('-======-=-=-=-=-=-=-=-=============-');
    ccvendor['orgname'] = vendor['orgname'];
    ccvendor['expooffer'] = vendor['expooffer'];
    ccvendor['contactperson'] = vendor['contactperson'];
    ccvendor['phone'] = vendor['phone'];
    ccvendor['email'] = vendor['email'];
    ccvendor['address'] = vendor['address'];
    ccvendor['note'] = vendor['note'];
    ccvendor['table'] = vendor['table'];
    ccvendor['totalpayment'] = vendor['totalpayment'];
    ccvendor['electricity'] = electricity;
    ccvendor['vendortype'] = vendortype;
    console.log('-======-=-=-=-=-=NEW CC VENDOR-=-=-=====-');
    console.log(ccvendor);

    $scope.processing = true;

    $http.post(API_URL + '/vendor/seattlecreditcard', ccvendor).success(function (response) {
      console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // $scope.back_button = 1;
        $scope.processing = false;
        $scope.sucess_process = true;
        $scope.sucess_msg = 'Thank you for joining us!. Your payment was accepted.';
        // window.location = "/complete/" + $scope.donateamount;
      }else if(response.error == null){
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = response.error;
      }
    });
  };

  $scope.submitvendorecheck = function (vendor,echeckvendor,vendortype,electricity) {
    console.log(vendor);
    console.log(echeckvendor);
    console.log(vendortype);
    console.log(electricity);
    console.log('-======-=-=-=-=-=-=-=-=============-');
    echeckvendor['orgname'] = vendor['orgname'];
    echeckvendor['expooffer'] = vendor['expooffer'];
    echeckvendor['contactperson'] = vendor['contactperson'];
    echeckvendor['phone'] = vendor['phone'];
    echeckvendor['email'] = vendor['email'];
    echeckvendor['address'] = vendor['address'];
    echeckvendor['note'] = vendor['note'];
    echeckvendor['table'] = vendor['table'];
    echeckvendor['totalpayment'] = vendor['totalpayment'];
    echeckvendor['electricity'] = electricity;
    echeckvendor['vendortype'] = vendortype;
    console.log('-======-=-=-=-=-=NEW CC VENDOR-=-=-=====-');
    console.log(echeckvendor);

    $http.post(API_URL + '/vendor/seattleecheck', echeckvendor).success(function (response) {
      console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // $scope.back_button = 1;
        $scope.processing = false;
        $scope.sucess_process = true;
        $scope.sucess_msg = 'Thank you for joining us!. Your payment was accepted.';
        // window.location = "/complete/" + $scope.donateamount;
      }else if(response.error == null){
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.backtoform = true;
        $scope.processing = false;
        $scope.gateway_error = response.error;
      }
    });
  };

  $scope.submitvendorpaypal = function(vendor, ventype, howdidyoulearn, cname){
    console.log(vendor);
    console.log(ventype);
    var cn='';
    if(cname == undefined){
      cn = '';
    }else{
      cn = cname.replace(/\s+/g, '*');
    }

    var hdyl='';

    if(howdidyoulearn == undefined){
      hdyl = '';
    }else{
      hdyl = howdidyoulearn.replace(/\s+/g, '*');
    }

    var orgname = vendor['orgname'];
    var expooffer = vendor['expooffer'];
    var contactperson = vendor['contactperson'];
    var phone = vendor['phone'];
    var email = vendor['email'];
    var address = vendor['address'];
    var table = vendor['table'];
    var totalpayment = vendor['totalpayment'];
    var electricity = vendor['electricity'];
    var vendortype = ventype;

    var val = 'user@email.com seattleexpovendor ' + orgname.replace(/\s+/g, '*') + ' ' + expooffer.replace(/\s+/g, '*') + ' ' + contactperson.replace(/\s+/g, '*') + ' ' + phone + ' ' + email + ' ' + address.replace(/\s+/g, '*') + ' ' + table + ' ' + totalpayment + ' ' + electricity + ' ' + vendortype.replace(/\s+/g, '*') + ' ' + hdyl + ' ' + cn;
    $('#paypalcustom').val(val);
    console.log(val);
    $('#formSeattleVendor').submit();
    // alert('submit paypal');
    $scope.paypalbutton = true;
  };

});

app.controller("TestimonialCtrl", function ($scope, $http) {

    // console.log('Inside TestimonialCtrl');

    $scope.testi = '';
    $scope.manen = '';
    var oriUser = angular.copy($scope.user);
    $scope.submit = function (user) {
        // console.log(user);
        $http.post(API_URL + '/utility/submit', user).success(function (response) {
          if(response==""){
            $scope.isSaving = false;
            $scope.manen = response.manen;
            $scope.testi = '';
          }else{
            $scope.isSaving = false;
            $scope.testi = response.testi;
            $scope.manen = '';
            $scope.user = angular.copy(oriUser);
            $scope.formtesti.$setPristine(true);
          }
        })
      }

      $scope.notendresult = true;
      $scope.showloading = false;
      $scope.endresult = false;
      var offset = 5;
      $scope.showmore = function (data) {
        $scope.showloading = true;
        $http.get(API_URL + '/utility/testimonial/' + offset).success(function (response) {
          var obj = JSON.parse(JSON.stringify(response));
          if (response) {
            for (var data in obj) {
              $('#testimonials').append("<div class='media' style='background-color:#eee'><span class='pull-left thumb-sm'><img src='../public/img/a0.jpg' alt='...'></span><div class='media-body'><div class='pull-right text-center text-muted'></div><a href=' class='h4'>"
                + obj[data].name + "</a><div class='block'><em class='text-xs'>Company/Organization: <span class='text-danger'>"
                + obj[data].comporg + "</span></em><i class='fa fa-envelope fa-fw m-r-xs'></i>:"
                + obj[data].comporg + "</div><blockquote><small class='block m-t-sm'>"
                + obj[data].message + "</small></blockquote></div></div>");
            }
          }

          offset = offset + obj.length;
          $scope.showloading = false;
          if (obj.length < 5) {
            $scope.notendresult = false;
            $scope.endresult = true;
          } else {
            $scope.notendresult = true;
            $scope.endresult = false;
          }
        });
}
});




app.controller("NewsCtrl", function ($http, $scope, $window, $parse, $sce) {
  console.log("news list");


  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomorenews = false;

  var list = [];

    // list news

    $scope.data = {};
    
    $scope.currentstatusshow = '';

    var loadnewslist = function () {

      $http({
        url: API_URL+"/news/news/" + num + '/' + offset + '/' + page,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {

        console.log(data);
        $scope.data = data;

        if(data.length < 10){
          $scope.hideloadmore = true;
          $scope.nomorenews = true; 
        } else if (data.total_items <= page){
          $scope.nomorenews = true;   
          $scope.hideloadmore = true;
        } else {
          $scope.loading = false;
        }

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  }   

});





app.controller("NewsCtrlbycategory",function ($scope, $http, $compile,$sce){
  var url = window.location.href;
  var auth = url.match(/\/news\/category\/(.*)+/);
  console.log(auth);
  var newsslugs = auth[1];

  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomorenews = false;
  $scope.data = {};  
  $scope.currentstatusshow = '';

  if(newsslugs == "upcoming-programs" || newsslugs == "recent-activities" || newsslugs == "webinar-archive" || newsslugs == "mindful-living-tips"){
    console.log("featured category");

    if(newsslugs == "upcoming-programs"){
      var featslugs = 1;
    }else if(newsslugs == "recent-activities"){
      var featslugs = 2;
    }else if(newsslugs == "webinar-archive"){
      var featslugs = 3;
    }else if(newsslugs == "mindful-living-tips"){
      var featslugs = 4;
    }

    var loadnewslist = function () {

      $http({
        url: API_URL+"/news/showlistfeaturedcategory/" + num + '/' + offset + '/' + page + '/' + featslugs ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        console.log(data);
        $scope.data = data;

        if(data.length < 10){
          $scope.hideloadmore = true;
          $scope.nomorenews = true; 
        } else if (data.total_items <= page){
          $scope.nomorenews = true;   
          $scope.hideloadmore = true;
        } else {
          $scope.loading = false;
        }

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  } 

}else{

  console.log("news category");

  var loadnewslist = function () {

    $http({
      url: API_URL+"/news/showlistnewsbycategory/" + num + '/' + offset + '/' + page + '/' + newsslugs ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("==============");
      console.log(data);
      console.log("==============");
      $scope.data = data;

      if(data.length < 10){
        $scope.hideloadmore = true;
        $scope.nomorenews = true; 
      } else if (data.total_items <= page){
        $scope.nomorenews = true;   
        $scope.hideloadmore = true;
      } else {
        $scope.loading = false;
      }  

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  } 

}

});

app.controller("NewsCtrlbytags",function ($scope, $http, $compile,$sce){
  var url = window.location.href;
  var auth = url.match(/\/news\/tags\/(.*)+/);
  console.log(auth[1]);

  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomorenews = false;
  $scope.data = {};  
  $scope.currentstatusshow = '';

  var loadnewslist = function () {

    $http({
      url: API_URL+"/news/showlistnewsbytags/" + num + '/' + offset + '/' + page + '/' + auth[1] ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("==============");
      console.log(data);
      console.log("==============");
      $scope.data = data;

      if(data.length < 10){
        $scope.hideloadmore = true;
        $scope.nomorenews = true; 
      } else if (data.total_items <= page){
        $scope.nomorenews = true;   
        $scope.hideloadmore = true;
      } else {
        $scope.loading = false;
      }  

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  } 

});

app.controller("NewsCtrlbyArchive",function ($scope, $http, $compile,$sce){
  var url = window.location.href;
  var auth = url.match(/\/news\/archive\/(.*)+\/(.*)+/);
  console.log(auth[1]);
  console.log(auth[2]);
  console.log(num + '/' + offset + '/' + page + '/' + auth[1] + "/" +  auth[2]);

  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomorenews = false;
  $scope.data = {};  
  $scope.currentstatusshow = '';

  $scope.archive = auth[1] + " " + auth[2];

  var loadnewslist = function () {

    $http({
      url: API_URL + '/news/frontend/listnewsbyarchive/' + num + '/' + offset + '/' + page + '/' + auth[1] + "/" +  auth[2],
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("==============");
      console.log(data);
      console.log("==============");
      $scope.data = data.data;

      if(data.length < 10){
        $scope.hideloadmore = true;
        $scope.nomorenews = true; 
      } else if (data.total_items <= page){
        $scope.nomorenews = true;   
        $scope.hideloadmore = true;
      } else {
        $scope.loading = false;
      }  

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  } 

});

app.controller("author", function($scope, $http, $compile,$sce, uiCalendarConfig){

  var url = window.location.href;
  var auth = url.match(/\/news\/author\/(.*)+/);
  console.log(auth[1]);

  var offset = 0;
  var page = 10;
  var num = 10;
  $scope.loading = false;
  $scope.hideloadmore = false;
  $scope.nomorenews = false;
  $scope.data = {};  
  $scope.currentstatusshow = '';

  var loadauthorinfo = function () {
    $http({
      url: API_URL+"/news/getauthorbyid/" + auth[1],
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      $scope.name = data.name;
      $scope.authorid = data.authorid;
      $scope.location = data.location;
      $scope.about = $sce.trustAsHtml(data.about);
      $scope.photo = data.photo;
      $scope.occupation = data.occupation
    }).error(function (data, status, headers, config) {

    });
  }
  loadauthorinfo();

  var loadnewslist = function () {

    $http({
      url: API_URL+"/news/showlistofauthornews/" + num + '/' + offset + '/' + page + '/' + auth[1],
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log("==============");
      console.log(data);
      console.log("==============");
      $scope.data = data;

      if(data.length < 10){
        $scope.hideloadmore = true;
        $scope.nomorenews = true; 
      } else if (data.total_items <= page){
        $scope.nomorenews = true;   
        $scope.hideloadmore = true;
      }  

      // for (var x in data.data){
      //   data.data[x].videothumb = $sce.trustAsHtml(data.data[x].videothumb);        
      // }

    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadnewslist();

  $scope.showmorenews = function(){
    $scope.loading = true;
    page = page + page;
    loadnewslist();
  } 


});


// jimmy

app.controller("CalenCtrl", function($scope, $http, $compile, uiCalendarConfig){

  $scope.lisview1 = false;
  $scope.calview1 = true;
  $scope.lisview = function(){
    $scope.lisview1 = true;
    $scope.calview1 = false;   
  }
  $scope.calview = function(){
    $scope.calview1 = true;
    $scope.lisview1 = false;
  }

  // begin code here
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  /* event source that pulls from google.com */
  $scope.eventSource = {
    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
    className: 'gcal-event',           // an option!
    currentTimezone: 'America/Chicago' // an option!
  };  
  
  $scope.events = [];
  
  $http.get(API_URL + '/calendar/manageactivity').success(function(listview3){
    var data = JSON.parse(JSON.stringify(listview3));
    if(listview3){
      var arrevents = [];
      for(var d in data){
        if(data[d].datef == data[d].datet){
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            location: data[d].loc,
            info: data[d].info,
            dstart: data[d].fyea2+'-'+data[d].fmon2+'-'+data[d].fday2,
            dend: 'One Day Activity'
          });
        }else{
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            end: new Date(data[d].tyea, data[d].tmon-1, data[d].tday, data[d].hhr, data[d].hmin),
            allDay: false,
            location: data[d].loc,
            info: data[d].info,
            dstart: data[d].fyea2+'-'+data[d].fmon2+'-'+data[d].fday2,
            dend: '<i class="icon-time text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
            +data[d].tyea2+'-'+data[d].tmon2+'-'+data[d].tday2+', '+data[d].hhr+':'+data[d].hmin+' '+data[d].hap
          });
        }
      }
    }
    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: false,
        header:{
          left: 'title',
          center: 'Activity',
          right: 'today, prev, next'
        },
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: function(event, element) {
          element.qtip({
            content: '<div class="h4 font-thin m-b-sm">'
            +event.title+'</div><div class="line b-b b-light"></div><div><i class="icon-calendar text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
            +event.dstart+'</div><div>'
            +event.dend+'</div><div><i class="icon-location-arrow text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
            +event.location+'</div><div class="m-t-sm"> '
            +event.info+'</div>'
          });
        }
      }
    };

  });

/* event sources array*/
$scope.eventSources = [$scope.events];
  // end code here

});

app.controller("MenuCtrl", function ($http, $scope, $parse, $sce , $timeout) {
  var shortCode = document.getElementById('shortCode').value;
  var loadSubMenuList = function () { 
    $http({
      url: API_URL + "/menu/viewfrontendmenu/" + shortCode,
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
     $scope.webmenu = data.data; 
     $scope.mobmenu = data.data;
     // console.log(data.data);
   })
  }
  loadSubMenuList();
 });



app.factory('Utils', function($q) {
  return {
    isImage: function(src) {

      var deferred = $q.defer();

      var image = new Image();
      image.onerror = function() {
        deferred.resolve(false);
      };
      image.onload = function() {
        deferred.resolve(true);
      };
      image.src = src;

      return deferred.promise;
    }
  };
});

app.factory('MDYbday', function () {
  return {
    month: function () {
      return [      
      {name: 'January', val: '01'},
      {name: 'February', val: '02'},
      {name: 'March', val: '03'},
      {name: 'April', val: '04'},
      {name: 'May', val: '05'},
      {name: 'June', val: '06'},
      {name: 'July', val: '07'},
      {name: 'August', val: '08'},
      {name: 'September', val: '09'},
      {name: 'October', val: '10'},
      {name: 'November', val: '11'},
      {name: 'December', val: '12'}
      ]
    },
    day: function () {
      var day = [
      {'val': '01'},
      {'val': '02'},
      {'val': '03'},
      {'val': '04'},
      {'val': '05'},
      {'val': '06'},
      {'val': '07'},
      {'val': '08'},
      {'val': '09'},
      {'val': '10'},
      {'val': '11'},
      {'val': '12'},
      {'val': '13'},
      {'val': '14'},
      {'val': '15'},
      {'val': '16'},
      {'val': '17'},
      {'val': '18'},
      {'val': '19'},
      {'val': '20'},
      {'val': '21'},
      {'val': '22'},
      {'val': '23'},
      {'val': '24'},
      {'val': '25'},
      {'val': '26'},
      {'val': '27'},
      {'val': '28'},
      {'val': '29'},
      {'val': '30'},
      {'val': '31'}
      ];
      return day;
    },
    year: function () {
      var year = [];
      for (var y = 2015; y >=1900; y--) {
        year.push({'val': y})
      }
      return year;
    }
  }
});

app.factory('MDY', function () {
  return {
    month: function () {
      return [
      {name: '', val: ''},
      {name: 'January', val: 1},
      {name: 'February', val: 2},
      {name: 'March', val: 3},
      {name: 'April', val: 4},
      {name: 'May', val: 5},
      {name: 'June', val: 6},
      {name: 'July', val: 7},
      {name: 'August', val: 8},
      {name: 'September', val: 9},
      {name: 'October', val: 10},
      {name: 'November', val: 11},
      {name: 'December', val: 12}
      ]
    },
    day: function () {
      var day = [];
      day.push({'val': ''})
      for (var x = 1; x <= 31; x++) {
        day.push({'val': x})
      }
      return day;
    },
    year: function () {
      var year = [];
      year.push({'val': ''})
      for (var y = 2015; y >=1900; y--) {
        year.push({'val': y})
      }
      return year;
    }
  }
});

app.factory('MDYform', function () {
  return {
    month: function () {
      return [
      {name: 'January', val: 1},
      {name: 'February', val: 2},
      {name: 'March', val: 3},
      {name: 'April', val: 4},
      {name: 'May', val: 5},
      {name: 'June', val: 6},
      {name: 'July', val: 7},
      {name: 'August', val: 8},
      {name: 'September', val: 9},
      {name: 'October', val: 10},
      {name: 'November', val: 11},
      {name: 'December', val: 12}
      ]
    },
    day: function () {
      var day = [];
      for (var x = 1; x <= 31; x++) {
        day.push({'val': x})
      }
      return day;
    },
    year: function () {
      var year = [];
      for (var y = 2015; y <= 2050; y++) {
        year.push({'val': y})
      }
      return year;
    }
  }
});

app.factory('Countries', function () {

  return {
    list: function () {
      return [
      {
        "name": "Afghanistan",
        "code": "AF"
      },
      {
        "name": "Åland Islands",
        "code": "AX"
      },
      {
        "name": "Albania",
        "code": "AL"
      },
      {
        "name": "Algeria",
        "code": "DZ"
      },
      {
        "name": "American Samoa",
        "code": "AS"
      },
      {
        "name": "AndorrA",
        "code": "AD"
      },
      {
        "name": "Angola",
        "code": "AO"
      },
      {
        "name": "Anguilla",
        "code": "AI"
      },
      {
        "name": "Antarctica",
        "code": "AQ"
      },
      {
        "name": "Antigua and Barbuda",
        "code": "AG"
      },
      {
        "name": "Argentina",
        "code": "AR"
      },
      {
        "name": "Armenia",
        "code": "AM"
      },
      {
        "name": "Aruba",
        "code": "AW"
      },
      {
        "name": "Australia",
        "code": "AU"
      },
      {
        "name": "Austria",
        "code": "AT"
      },
      {
        "name": "Azerbaijan",
        "code": "AZ"
      },
      {
        "name": "Bahamas",
        "code": "BS"
      },
      {
        "name": "Bahrain",
        "code": "BH"
      },
      {
        "name": "Bangladesh",
        "code": "BD"
      },
      {
        "name": "Barbados",
        "code": "BB"
      },
      {
        "name": "Belarus",
        "code": "BY"
      },
      {
        "name": "Belgium",
        "code": "BE"
      },
      {
        "name": "Belize",
        "code": "BZ"
      },
      {
        "name": "Benin",
        "code": "BJ"
      },
      {
        "name": "Bermuda",
        "code": "BM"
      },
      {
        "name": "Bhutan",
        "code": "BT"
      },
      {
        "name": "Bolivia",
        "code": "BO"
      },
      {
        "name": "Bosnia and Herzegovina",
        "code": "BA"
      },
      {
        "name": "Botswana",
        "code": "BW"
      },
      {
        "name": "Bouvet Island",
        "code": "BV"
      },
      {
        "name": "Brazil",
        "code": "BR"
      },
      {
        "name": "British Indian Ocean Territory",
        "code": "IO"
      },
      {
        "name": "Brunei Darussalam",
        "code": "BN"
      },
      {
        "name": "Bulgaria",
        "code": "BG"
      },
      {
        "name": "Burkina Faso",
        "code": "BF"
      },
      {
        "name": "Burundi",
        "code": "BI"
      },
      {
        "name": "Cambodia",
        "code": "KH"
      },
      {
        "name": "Cameroon",
        "code": "CM"
      },
      {
        "name": "Canada",
        "code": "CA"
      },
      {
        "name": "Cape Verde",
        "code": "CV"
      },
      {
        "name": "Cayman Islands",
        "code": "KY"
      },
      {
        "name": "Central African Republic",
        "code": "CF"
      },
      {
        "name": "Chad",
        "code": "TD"
      },
      {
        "name": "Chile",
        "code": "CL"
      },
      {
        "name": "China",
        "code": "CN"
      },
      {
        "name": "Christmas Island",
        "code": "CX"
      },
      {
        "name": "Cocos (Keeling) Islands",
        "code": "CC"
      },
      {
        "name": "Colombia",
        "code": "CO"
      },
      {
        "name": "Comoros",
        "code": "KM"
      },
      {
        "name": "Congo",
        "code": "CG"
      },
      {
        "name": "Congo, The Democratic Republic of the",
        "code": "CD"
      },
      {
        "name": "Cook Islands",
        "code": "CK"
      },
      {
        "name": "Costa Rica",
        "code": "CR"
      },
      {
        "name": "Cote D\"Ivoire",
        "code": "CI"
      },
      {
        "name": "Croatia",
        "code": "HR"
      },
      {
        "name": "Cuba",
        "code": "CU"
      },
      {
        "name": "Cyprus",
        "code": "CY"
      },
      {
        "name": "Czech Republic",
        "code": "CZ"
      },
      {
        "name": "Denmark",
        "code": "DK"
      },
      {
        "name": "Djibouti",
        "code": "DJ"
      },
      {
        "name": "Dominica",
        "code": "DM"
      },
      {
        "name": "Dominican Republic",
        "code": "DO"
      },
      {
        "name": "Ecuador",
        "code": "EC"
      },
      {
        "name": "Egypt",
        "code": "EG"
      },
      {
        "name": "El Salvador",
        "code": "SV"
      },
      {
        "name": "Equatorial Guinea",
        "code": "GQ"
      },
      {
        "name": "Eritrea",
        "code": "ER"
      },
      {
        "name": "Estonia",
        "code": "EE"
      },
      {
        "name": "Ethiopia",
        "code": "ET"
      },
      {
        "name": "Falkland Islands (Malvinas)",
        "code": "FK"
      },
      {
        "name": "Faroe Islands",
        "code": "FO"
      },
      {
        "name": "Fiji",
        "code": "FJ"
      },
      {
        "name": "Finland",
        "code": "FI"
      },
      {
        "name": "France",
        "code": "FR"
      },
      {
        "name": "French Guiana",
        "code": "GF"
      },
      {
        "name": "French Polynesia",
        "code": "PF"
      },
      {
        "name": "French Southern Territories",
        "code": "TF"
      },
      {
        "name": "Gabon",
        "code": "GA"
      },
      {
        "name": "Gambia",
        "code": "GM"
      },
      {
        "name": "Georgia",
        "code": "GE"
      },
      {
        "name": "Germany",
        "code": "DE"
      },
      {
        "name": "Ghana",
        "code": "GH"
      },
      {
        "name": "Gibraltar",
        "code": "GI"
      },
      {
        "name": "Greece",
        "code": "GR"
      },
      {
        "name": "Greenland",
        "code": "GL"
      },
      {
        "name": "Grenada",
        "code": "GD"
      },
      {
        "name": "Guadeloupe",
        "code": "GP"
      },
      {
        "name": "Guam",
        "code": "GU"
      },
      {
        "name": "Guatemala",
        "code": "GT"
      },
      {
        "name": "Guernsey",
        "code": "GG"
      },
      {
        "name": "Guinea",
        "code": "GN"
      },
      {
        "name": "Guinea-Bissau",
        "code": "GW"
      },
      {
        "name": "Guyana",
        "code": "GY"
      },
      {
        "name": "Haiti",
        "code": "HT"
      },
      {
        "name": "Heard Island and Mcdonald Islands",
        "code": "HM"
      },
      {
        "name": "Holy See (Vatican City State)",
        "code": "VA"
      },
      {
        "name": "Honduras",
        "code": "HN"
      },
      {
        "name": "Hong Kong",
        "code": "HK"
      },
      {
        "name": "Hungary",
        "code": "HU"
      },
      {
        "name": "Iceland",
        "code": "IS"
      },
      {
        "name": "India",
        "code": "IN"
      },
      {
        "name": "Indonesia",
        "code": "ID"
      },
      {
        "name": "Iran, Islamic Republic Of",
        "code": "IR"
      },
      {
        "name": "Iraq",
        "code": "IQ"
      },
      {
        "name": "Ireland",
        "code": "IE"
      },
      {
        "name": "Isle of Man",
        "code": "IM"
      },
      {
        "name": "Israel",
        "code": "IL"
      },
      {
        "name": "Italy",
        "code": "IT"
      },
      {
        "name": "Jamaica",
        "code": "JM"
      },
      {
        "name": "Japan",
        "code": "JP"
      },
      {
        "name": "Jersey",
        "code": "JE"
      },
      {
        "name": "Jordan",
        "code": "JO"
      },
      {
        "name": "Kazakhstan",
        "code": "KZ"
      },
      {
        "name": "Kenya",
        "code": "KE"
      },
      {
        "name": "Kiribati",
        "code": "KI"
      },
      {
        "name": "Korea, Democratic People\"S Republic of",
        "code": "KP"
      },
      {
        "name": "Korea, Republic of",
        "code": "KR"
      },
      {
        "name": "Kuwait",
        "code": "KW"
      },
      {
        "name": "Kyrgyzstan",
        "code": "KG"
      },
      {
        "name": "Lao People\"S Democratic Republic",
        "code": "LA"
      },
      {
        "name": "Latvia",
        "code": "LV"
      },
      {
        "name": "Lebanon",
        "code": "LB"
      },
      {
        "name": "Lesotho",
        "code": "LS"
      },
      {
        "name": "Liberia",
        "code": "LR"
      },
      {
        "name": "Libyan Arab Jamahiriya",
        "code": "LY"
      },
      {
        "name": "Liechtenstein",
        "code": "LI"
      },
      {
        "name": "Lithuania",
        "code": "LT"
      },
      {
        "name": "Luxembourg",
        "code": "LU"
      },
      {
        "name": "Macao",
        "code": "MO"
      },
      {
        "name": "Macedonia, The Former Yugoslav Republic of",
        "code": "MK"
      },
      {
        "name": "Madagascar",
        "code": "MG"
      },
      {
        "name": "Malawi",
        "code": "MW"
      },
      {
        "name": "Malaysia",
        "code": "MY"
      },
      {
        "name": "Maldives",
        "code": "MV"
      },
      {
        "name": "Mali",
        "code": "ML"
      },
      {
        "name": "Malta",
        "code": "MT"
      },
      {
        "name": "Marshall Islands",
        "code": "MH"
      },
      {
        "name": "Martinique",
        "code": "MQ"
      },
      {
        "name": "Mauritania",
        "code": "MR"
      },
      {
        "name": "Mauritius",
        "code": "MU"
      },
      {
        "name": "Mayotte",
        "code": "YT"
      },
      {
        "name": "Mexico",
        "code": "MX"
      },
      {
        "name": "Micronesia, Federated States of",
        "code": "FM"
      },
      {
        "name": "Moldova, Republic of",
        "code": "MD"
      },
      {
        "name": "Monaco",
        "code": "MC"
      },
      {
        "name": "Mongolia",
        "code": "MN"
      },
      {
        "name": "Montserrat",
        "code": "MS"
      },
      {
        "name": "Morocco",
        "code": "MA"
      },
      {
        "name": "Mozambique",
        "code": "MZ"
      },
      {
        "name": "Myanmar",
        "code": "MM"
      },
      {
        "name": "Namibia",
        "code": "NA"
      },
      {
        "name": "Nauru",
        "code": "NR"
      },
      {
        "name": "Nepal",
        "code": "NP"
      },
      {
        "name": "Netherlands",
        "code": "NL"
      },
      {
        "name": "Netherlands Antilles",
        "code": "AN"
      },
      {
        "name": "New Caledonia",
        "code": "NC"
      },
      {
        "name": "New Zealand",
        "code": "NZ"
      },
      {
        "name": "Nicaragua",
        "code": "NI"
      },
      {
        "name": "Niger",
        "code": "NE"
      },
      {
        "name": "Nigeria",
        "code": "NG"
      },
      {
        "name": "Niue",
        "code": "NU"
      },
      {
        "name": "Norfolk Island",
        "code": "NF"
      },
      {
        "name": "Northern Mariana Islands",
        "code": "MP"
      },
      {
        "name": "Norway",
        "code": "NO"
      },
      {
        "name": "Oman",
        "code": "OM"
      },
      {
        "name": "Pakistan",
        "code": "PK"
      },
      {
        "name": "Palau",
        "code": "PW"
      },
      {
        "name": "Palestinian Territory, Occupied",
        "code": "PS"
      },
      {
        "name": "Panama",
        "code": "PA"
      },
      {
        "name": "Papua New Guinea",
        "code": "PG"
      },
      {
        "name": "Paraguay",
        "code": "PY"
      },
      {
        "name": "Peru",
        "code": "PE"
      },
      {
        "name": "Philippines",
        "code": "PH"
      },
      {
        "name": "Pitcairn",
        "code": "PN"
      },
      {
        "name": "Poland",
        "code": "PL"
      },
      {
        "name": "Portugal",
        "code": "PT"
      },
      {
        "name": "Puerto Rico",
        "code": "PR"
      },
      {
        "name": "Qatar",
        "code": "QA"
      },
      {
        "name": "Reunion",
        "code": "RE"
      },
      {
        "name": "Romania",
        "code": "RO"
      },
      {
        "name": "Russian Federation",
        "code": "RU"
      },
      {
        "name": "RWANDA",
        "code": "RW"
      },
      {
        "name": "Saint Helena",
        "code": "SH"
      },
      {
        "name": "Saint Kitts and Nevis",
        "code": "KN"
      },
      {
        "name": "Saint Lucia",
        "code": "LC"
      },
      {
        "name": "Saint Pierre and Miquelon",
        "code": "PM"
      },
      {
        "name": "Saint Vincent and the Grenadines",
        "code": "VC"
      },
      {
        "name": "Samoa",
        "code": "WS"
      },
      {
        "name": "San Marino",
        "code": "SM"
      },
      {
        "name": "Sao Tome and Principe",
        "code": "ST"
      },
      {
        "name": "Saudi Arabia",
        "code": "SA"
      },
      {
        "name": "Senegal",
        "code": "SN"
      },
      {
        "name": "Serbia and Montenegro",
        "code": "CS"
      },
      {
        "name": "Seychelles",
        "code": "SC"
      },
      {
        "name": "Sierra Leone",
        "code": "SL"
      },
      {
        "name": "Singapore",
        "code": "SG"
      },
      {
        "name": "Slovakia",
        "code": "SK"
      },
      {
        "name": "Slovenia",
        "code": "SI"
      },
      {
        "name": "Solomon Islands",
        "code": "SB"
      },
      {
        "name": "Somalia",
        "code": "SO"
      },
      {
        "name": "South Africa",
        "code": "ZA"
      },
      {
        "name": "South Georgia and the South Sandwich Islands",
        "code": "GS"
      },
      {
        "name": "Spain",
        "code": "ES"
      },
      {
        "name": "Sri Lanka",
        "code": "LK"
      },
      {
        "name": "Sudan",
        "code": "SD"
      },
      {
        "name": "Suriname",
        "code": "SR"
      },
      {
        "name": "Svalbard and Jan Mayen",
        "code": "SJ"
      },
      {
        "name": "Swaziland",
        "code": "SZ"
      },
      {
        "name": "Sweden",
        "code": "SE"
      },
      {
        "name": "Switzerland",
        "code": "CH"
      },
      {
        "name": "Syrian Arab Republic",
        "code": "SY"
      },
      {
        "name": "Taiwan, Province of China",
        "code": "TW"
      },
      {
        "name": "Tajikistan",
        "code": "TJ"
      },
      {
        "name": "Tanzania, United Republic of",
        "code": "TZ"
      },
      {
        "name": "Thailand",
        "code": "TH"
      },
      {
        "name": "Timor-Leste",
        "code": "TL"
      },
      {
        "name": "Togo",
        "code": "TG"
      },
      {
        "name": "Tokelau",
        "code": "TK"
      },
      {
        "name": "Tonga",
        "code": "TO"
      },
      {
        "name": "Trinidad and Tobago",
        "code": "TT"
      },
      {
        "name": "Tunisia",
        "code": "TN"
      },
      {
        "name": "Turkey",
        "code": "TR"
      },
      {
        "name": "Turkmenistan",
        "code": "TM"
      },
      {
        "name": "Turks and Caicos Islands",
        "code": "TC"
      },
      {
        "name": "Tuvalu",
        "code": "TV"
      },
      {
        "name": "Uganda",
        "code": "UG"
      },
      {
        "name": "Ukraine",
        "code": "UA"
      },
      {
        "name": "United Arab Emirates",
        "code": "AE"
      },
      {
        "name": "United Kingdom",
        "code": "GB"
      },
      {
        "name": "United States",
        "code": "US"
      },
      {
        "name": "United States Minor Outlying Islands",
        "code": "UM"
      },
      {
        "name": "Uruguay",
        "code": "UY"
      },
      {
        "name": "Uzbekistan",
        "code": "UZ"
      },
      {
        "name": "Vanuatu",
        "code": "VU"
      },
      {
        "name": "Venezuela",
        "code": "VE"
      },
      {
        "name": "Viet Nam",
        "code": "VN"
      },
      {
        "name": "Virgin Islands, British",
        "code": "VG"
      },
      {
        "name": "Virgin Islands, U.S.",
        "code": "VI"
      },
      {
        "name": "Wallis and Futuna",
        "code": "WF"
      },
      {
        "name": "Western Sahara",
        "code": "EH"
      },
      {
        "name": "Yemen",
        "code": "YE"
      },
      {
        "name": "Zambia",
        "code": "ZM"
      },
      {
        "name": "Zimbabwe",
        "code": "ZW"
      }
      ];
    }
  }
})

