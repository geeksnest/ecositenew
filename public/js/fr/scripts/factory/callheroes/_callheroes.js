/**
 * Created by ebautistajr on 3/30/15.
 */

 app.factory('CallheroesFactory', function ($http, store, jwtHelper, $window, Config) {
    return {
        loginmenu: "",
        saveHeroesOnDuty: function(_gdata, callback){
            $http({
                url: API_URL + "/callheroes/heroesonduty",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(_gdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback("data");
            });

        },
        saveHeroes: function(_gdata, callback){
            $http({
                url: API_URL + "/callheroes/heroescall",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(_gdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback("data");
            });

        },
        facreate: function(_gdata, callback){
            $http({
                url: API_URL + "/fa/save",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(_gdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback("data");
            });

        },
        atwcreate: function(_gdata, callback){
            $http({
                url: API_URL + "/atw/org",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(_gdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback("data");
            });

        },
        clubcreate: function(club, callback){
            $http({
                url: API_URL + "/club/submit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(club)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback("data");
            });

        },
  

    }
});
