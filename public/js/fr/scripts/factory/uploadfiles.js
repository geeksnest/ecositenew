/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('uploadfiles', function ($http, store, Upload, $window, Config) {
    return {
        loginmenu: "",
        uploadFiles: function (callback) {
            Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'videos/' + renamedFile,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
        }
    }
});
