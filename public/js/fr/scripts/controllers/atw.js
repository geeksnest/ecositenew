'use strict';

/* Controllers */

app.controller('AtwCtrl', function ($scope, $location, $http,  store, jwtHelper, $window, Countries,Config, MDYform, CallheroesFactory, Upload) {
  $('form').validate({
    rules: {
      organization: {
        required: true
      },
      selyear: {
        required: true
      },
      city: {
        required: true,
      },
      state: {
        required: true,
      },
      zip: {
        required: true,
      },
      country: {
        required: true,
      },
      activities: {
        required: true
      },
      mission: {
        required: true
      },
      email: {
        required: true,
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
$scope.countries = Countries.list();

  /**
  * Image Uploading
  * 
  */
  $scope.imageselected = false;
  $scope.loadingg = false;
  $scope.imgAlerts = [];
  $scope.closeAlert = function(index) {
    $scope.imgAlerts.splice(index, 1);
  };

  $scope.imageselected = false;

  $scope.prepare = function(file) {
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.imageselected = false;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      } else {
        console.log("below maximum");
        $scope.projImg = file;
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }


  $scope.submitatw =function(_gdata,files){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    if(_gdata.org==""         || _gdata.org== undefined 
      || _gdata.syear==""     || _gdata.syear== undefined 
      || _gdata.mission==""   || _gdata.mission== undefined 
      || _gdata.city==""      || _gdata.city== undefined
      || _gdata.country==""   || _gdata.country== undefined
      || _gdata.state==""     || _gdata.state== undefined
      || _gdata.zip==""       || _gdata.zip== undefined
      ||_gdata.activities=="" || _gdata.activities== undefined
      || _gdata.email==""     || _gdata.email== undefined ){
     $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!!'}];
      try{
        if($scope.user.syear){
          $scope.ddate = false;
        }else{
          $scope.ddate = true;
        }
      }catch (e) {
        $scope.ddate = true;
      }
      try{
        if($scope.user.country){
          $scope.nd = false;
        }else{
          $scope.nd = true;
        }
      }catch (e) {
        $scope.nd = true;
      }
   }else{
     $scope.loadingMsg = 'initializing..';  
      if(!files){
        $scope.errorget = 'Please choose image to upload.';
        $scope.alerts =[{type: 'success', msg: 'Please choose image to upload.'}];
      }else{
        $scope.loadingg = true;
        var file = files[0];
        if(file==undefined || file == ""){
          // $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
          // $location.hash('errorget');
          // $anchorScroll();
          $scope.loadingg = false;
          $scope.imagecontent=true;
        }else{
          if (file.size >= 2000000) {
            // $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
            // $location.hash('errorget');
            // $anchorScroll();
            $scope.alerts =[{type: 'danger', msg: 'Image File is too big.'}];
            $scope.loadingg = false;
            $scope.imagecontent=true;
          }else{
            $scope.loadingMsg = 'Submitting please wait...';
            console.log('uploading image bwahahahhahahhahah..');
            $scope.imageloader=true;
            var promises;
            var f = file.name;
            var fname=f.replace(/\s+/g, "-");
            promises = Upload.upload({
              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter, AllowedHeader) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/atw/' + fname,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
            })
            promises.then(function(data){
              var ff = data.config.file.name;
              var fn = ff;
             _gdata['image']=fn.replace(/\s+/g, "-");
              $scope.alerts.splice(0, 1);
              $scope.chkerror = false;
              $scope.isSaving = true;
              CallheroesFactory.atwcreate(_gdata, function(data){
                if(data.success){
                  $scope.isSaving = false;
                  $scope.form.$setPristine();
                  $scope.user = "";
                  $scope.alerts =[{type: 'success', msg: 'Thank you. Your information has been received. We will contact you shortly.'}];
                  $scope.projImg = [];
                  $scope.files = undefined;
                  $scope.loadingg = false;
                  $scope.imageselected = false;
                  $scope.isSaving = false;
                }
              });
            });
          }
        }
      }
  }
}

}).controller('FaCtrl', function ($scope, Countries, $location, Upload, $http,  store, jwtHelper, $window, Config, MDYform, CallheroesFactory) {

  $scope.countries = Countries.list();
  $('form').validate({
    rules: {
      organization: {
        required: true
      },
      add1: {
        required: true
      },
      city: {
        required: true,
      },
      state: {
        required: true
      },
      zip: {
        required: true
      },
      country: {
        required: true
      },
      year: {
        required: true
      },
      website: {
        required: true
      },
      mission: {
        required: true
      },
      contactperson: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true
      },
      reason: {
        required: true
      },
      agree: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $scope.chkbox= function(_gdata){
    if(_gdata){
      $scope.chkerror = true;
    }else{
       $scope.chkerror = false;
       $scope.alerts =[];
    }
  }
  
  $scope.imageselected = false;
  $scope.loadingg = false;
  $scope.imgAlerts = [];
$scope.prepare = function(file) {
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.imageselected = false;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      } else {
        $scope.projImg = file;
        $scope.closeAlerts();
        $scope.imageselected = true;
      }
    }
  }
  $scope.closeAlerts = function (index) {
    $scope.imgAlerts.splice(index, 1);
  };
  $scope.submitfa =function(_gdata,files){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    // if(_gdata == undefined || Object.keys(_gdata).length<15){
    //   $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the required fields before submitting!!'}];
    //   $scope.chkerror = true;
    // }
    // else
     if(!_gdata.agree || _gdata.agree==false){
      $scope.alerts =[{type: 'danger', msg: 'Click the checkbox to certify that the above information is correct.'}];
      $scope.chkerror = true;
    }
    else if(
      _gdata.organization==""    || _gdata.organization==undefined || _gdata.organization==null
      || _gdata.add1==""         || _gdata.add1==undefined         || _gdata.add1==null
      || _gdata.city==""         || _gdata.city==undefined         || _gdata.city==null
      || _gdata.state==""        || _gdata.state==undefined        || _gdata.state==null
      || _gdata.zip==""          || _gdata.zip==undefined          || _gdata.zip==null
      || _gdata.country==""      || _gdata.country==undefined      || _gdata.country==null
      || _gdata.cat==""          || _gdata.cat==undefined          || _gdata.cat==null
      || _gdata.year==""         || _gdata.year==undefined         || _gdata.year==null
      || _gdata.web==""          || _gdata.web==undefined          || _gdata.web==null
      || _gdata.mission==""      || _gdata.mission==undefined      || _gdata.mission==null
      || _gdata.contactperson=="" || _gdata.contactperson==undefined || _gdata.contactperson==null
      || _gdata.email==""        || _gdata.email==undefined        || _gdata.email==null
      || _gdata.reason==""       || _gdata.reason==undefined       || _gdata.reason==null
      || _gdata.phone==""        || _gdata.phone==undefined        || _gdata.phone==null
      )
    {
      $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
      $scope.chkerror = true;
      try {
        if($scope.user.year){
         $scope.nbday = false;
       }else{
        $scope.nbday = true;
      }
    }
    catch (e) {
      $scope.nbday = true;
    }
    try {
      if($scope.user.cat){
       $scope.n = false;
     }else{
      $scope.n = true;
    }
  }
  catch (e) {
    $scope.n = true;
  }
  try {
    if($scope.user.country=='' || $scope.user.country==undefined){
      $scope.nd = true;
      $('#country').focus();
      $('#country').blur();
    }else{
      $scope.nd = false;
    }
  }
  catch (e) {
    $scope.nd = true;
    $('#country').focus();
    $('#country').blur();
  }
    }else{
    $scope.n = false;
    $scope.nbday= false;
    $scope.alerts.splice(0, 1);
    $scope.chkerror = false;
    $scope.isSaving = true;
    $scope.loader = true;
    $scope.loadingMsg = 'initializing..';  
    if(!files){
      $scope.errorget = 'Please choose image to upload.';
      $scope.alerts =[{type: 'success', msg: 'Please choose image to upload.'}];
    }else{
      var file = files[0];
      if(file==undefined || file == ""){
        $scope.loadingg = false;
        $scope.imagecontent=true;
      }else{
        $scope.loadingg = true;
        if (file.size >= 2000000) {
          $scope.loadingg = false;
          $scope.imagecontent=true;
          $scope.alerts =[{type: 'danger', msg: 'Image File is too big.'}];
        }else{
            $scope.loadingMsg = 'Submitting please wait...';
            // console.log('uploading image bwahahahhahahhahah..');
            $scope.imageloader=true;
            var promises;
            var f = file.name;
            var fname=f.replace(/\s+/g, "-");
            promises = Upload.upload({
              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter, AllowedHeader) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/friends/' + fname,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
            })
            promises.then(function(data){
             var ff = data.config.file.name;
             var fn = ff;
             _gdata['image']=fn.replace(/\s+/g, "-");
             $scope.alerts.splice(0, 1);

      CallheroesFactory.facreate(_gdata, function(data){
        console.log(data)
        if(data.success){
          $scope.loadingg = false;
          $scope.imageselected = false;
          $scope.isSaving = false;
          $scope.loader = false;
          $scope.form.$setPristine();
          $scope.user = "";
          $scope.projImg = [];
          $scope.files = undefined;
          $scope.imageselected = false;
          $scope.alerts =[{type: 'success', msg: 'Thank you. Your information has been received. We will contact you shortly.'}];
        }

      });
      });
          }
        }
      }

  }
}

}).directive('phone', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

      ngModelCtrl.$parsers.push(function(viewValue) {
              // console.log(viewValue)
              if (viewValue!=undefined) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
              }


            });

      ngModelCtrl.$render = function() {
        $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
      };

      $element.bind('change', listener);
      $element.bind('keydown', function(event) {
        var key = event.keyCode;
        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
          return;
        }
        $browser.defer(listener);
      });

      $element.bind('paste cut', function() {
        $browser.defer(listener);
      });
    }

  };
}).filter('tel', function () {
  return function (tel) {
          // console.log(tel);

          if (!tel){ 
            return ''; 
          }else{
           var value = tel.toString().trim().replace(/^\+/, '');

           if (value.match(/[^0-9]/)) {
            return tel;
          }

          var country, city, number;

          switch (value.length) {
            case 1:
            case 2:
            case 3:
            city = value;
            break;

            default:
            city = value.slice(0, 3);
            number = value.slice(3);
          }

          if(number){
            if(number.length>3){
              number = number.slice(0, 3) + ' ' + number.slice(3,7);
            }
            else{
              number = number;
            }

            return ("(" + city + ") " + number).trim();
          }
          else{
            return "(" + city;
          }
        }



      };
    });
