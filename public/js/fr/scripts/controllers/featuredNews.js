'use strict';

/* Controllers */

app.controller("featuredblogCtrl", function ($http, $scope, $parse, $sce, Config) {

  var returnYoutubeThumb = function numberWithCommas(item) {
    if(item){
      var newdata = item;
      var x;
      x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
      return 'http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
    }else{
      return x;
    }
  };

  var upcoming = function () {
    console.log("upcoming");
    var num = 1;
    $http({
      url: API_URL+"/news/featuredblog/" + num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.upcomingtitle = data.title;
      $scope.upcomingnewsslugs = data.newsslugs;
      $scope.upcomingsummary = data.summary;
      $scope.upcomingimagethumb = data.imagethumb;
      $scope.upcomingvideothumb = data.videothumb;
      if(data.imagethumb){
        $scope.upComingNewsThumb = Config.amazonlink+'/uploads/newsimages/'+data.imagethumb;
      }else{
        $scope.upComingNewsThumb = returnYoutubeThumb(data.videothumb);
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  upcoming();
  var recent = function () {
    console.log("recent");
    var num = 2;
    $http({
      url: API_URL+"/news/featuredblog/" + num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.recenttitle = data.title;
      $scope.recentnewsslugs = data.newsslugs;
      $scope.recentsummary = data.summary;
      $scope.recentimagethumb = data.imagethumb;
      $scope.recentvideothumb = data.videothumb;
      if(data.imagethumb){
        $scope.recentNewsThumb = Config.amazonlink+'/uploads/newsimages/'+data.imagethumb;
      }else{
        $scope.recentNewsThumb = returnYoutubeThumb(data.videothumb);
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  recent();
  var webinar = function () {
    console.log("webinar");
    var num = 3;
    $http({
      url: API_URL+"/news/featuredblog/" + num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.webinartitle = data.title;
      $scope.webinarnewsslugs = data.newsslugs;
      $scope.webinarsummary = data.summary;
      $scope.webinarimagethumb = data.imagethumb;
      $scope.webinarvideothumb = data.videothumb;
      if(data.imagethumb){
        $scope.webinarNewsThumb = Config.amazonlink+'/uploads/newsimages/'+data.imagethumb;
      }else{
        $scope.webinarNewsThumb = returnYoutubeThumb(data.videothumb);
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  webinar();
  var mindful = function () {
    console.log("mindfull");
    var num = 4;
    $http({
      url: API_URL+"/news/featuredblog/" + num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.mindfultitle = data.title;
      $scope.mindfulnewsslugs = data.newsslugs;
      $scope.mindfulsummary = data.summary;
      $scope.mindfulimagethumb = data.imagethumb;
      $scope.mindfulvideothumb = data.videothumb;
      if(data.imagethumb){
        $scope.mindfulNewsThumb = Config.amazonlink+'/uploads/newsimages/'+data.imagethumb;
      }else{
        $scope.mindfulNewsThumb = returnYoutubeThumb(data.videothumb);
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  mindful();

  var latestnews = function () {
    console.log("latestnews");
    $http({
      url: API_URL+"/news/latestnews",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      $scope.latestnews = data;
      if(data[0].imagethumb){
        $scope.latestnewsThumb = Config.amazonlink+'/uploads/newsimages/'+data[0].imagethumb;
      }else{
        $scope.latestnewsThumb = returnYoutubeThumb(data[0].videothumb);
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    }); 
  }
  latestnews();

});
