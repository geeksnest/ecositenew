'use strict';

/* Controllers */
app.controller("editvideoCtrl", function ($scope, $http, $anchorScroll, $location, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {

  $scope.amazonlink = Config.amazonlink;  

  $scope.story = false;
  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
  }else{
    $window.location = '/login';
  }
  $scope.loadingg = false;

  //get the slugs
  var url = window.location.href;
  var auth = url.match(/\/medialibrary\/(.*)\/(.*)+/);
  var slugs = auth[2]; 
    console.log(slugs);
  if(slugs == 'null'){
    $window.location = '/page404';
  }
  //get data
  var loadvideo = function(){
    $http({
      url: API_URL+"/mymedialibrary/viewvideo/" + slugs,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      if(data != "NODATA"){
        $scope.media = data[0]; 
        $scope.videoname = data[0].video; 
        $scope.preview = $sce.trustAsHtml(data[0].embed);
        if(data[0].videotype == 'upload'){
          var playerInstance = jwplayer("myElement");
          playerInstance.setup({
            file: $scope.amazonlink+"/videos/"+data[0].video,
            image: $scope.amazonlink+"/videos/thumbs/"+data[0].videothumb,
            title: data[0].title,
            description: data[0].title,
            mediaid: data[0].id
          });
        }

        var taglist = [];
        data[0].tag.map(function(val){
            taglist.push(val.tags);
        });

        console.log(taglist);
        $scope.media.tag = taglist;
      }else{
        $window.location = '/page404';
      }
    }).error(function (data, status, headers, config) {
    });
  }
  loadvideo();

  
  $scope.errorget = '';
  $scope.successget = '';
  $scope.loadingMsg = 'Saving video Please wait...';
  $scope.closeAlert1 = function(index) {
    $scope.resultget.splice(index, 1);
  };
  $scope.cNTS = [];  
  
  $scope.resultget = [];
$('form').validate({
    rules: {
      name: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true
      },
      description: {
        required: true,
      },
      title: {
        required: true
      },
      agree: {
        required: true
      },
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $scope.save = function(media,stat,thumbImg,thumbName){
    $scope.errorget = '';
    $scope.successget = ''; 
    var valid = true;
    $scope.cNTS = [];  
    $scope.borderError = [];
    $scope.resultget = [];  
    if(
    media.name==""             || media.name==undefined
    || media.title==""         || media.title==undefined
    || media.description==""   || media.description==undefined
    || media.phone==""         || media.phone==undefined 
    || media.email==""         || media.email==undefined
    // || media.agree==false      || media.agree==undefined
    || media.tag==undefined    || media.tag==""
    || media.category==""      || media.category==undefined
    )
  {
    $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill out all required fields before submitting'}];
    $scope.chkerror = true;
    $scope.n = true;
    try {
        if($scope.media.description){
         $scope.n = false;
       }else{
        $scope.n = true;
        $('#desc').focus();
        $('#desc').blur();
      }
    }
    catch (e) {
      $scope.n = true;
    }
    try {
        if($scope.media.tag == "" || !$scope.media.tag || $scope.media.tag==undefined){
        $scope.t = true;
        $('#tags').focus();
        $('#tags').blur();
       }else{
         $scope.t = false;
         console.log("mali")
      }
    }
    catch (e) {
      $scope.n = true;
    }
    try {
        if($scope.media.category == "" || !$scope.media.category || $scope.media.category==undefined){
         $scope.cat = true;
         $('#cats').focus();
         $('#cats').blur();

       }else{
        $scope.cat = false;
      }
    }
    catch (e) {
      $scope.n = true;
    }
  }
  else{
      $scope.loadingg = true;
      var slugs = media.title;
      var text = slugs.replace(/[^\w ]+/g,'');
      var slugs = angular.lowercase(text.replace(/ +/g,'-'));
      media['slugs'] = slugs;
      media['stat'] = stat;
      if(thumbImg){
        var fileName = thumbName.split('.');
        var fileIMG = '.' + thumbImg.name.split('.').pop();
        var newThumbName = fileName[0] + fileIMG;
        var promisesThumb;
        promisesThumb = Upload.upload({

          url: Config.amazonlink,
          method: 'POST',
          transformRequest: function (data, headersGetter) {
            var headers = headersGetter();
            delete headers['Authorization'];
            return data;
          },
          fields : {
            key: 'videos/thumbs/' + newThumbName,
            AWSAccessKeyId: Config.AWSAccessKeyId,
            acl: 'private',
            policy: Config.policy,
            signature: Config.signature,
            "Content-Type": thumbImg.type != '' ? thumbImg.type : 'application/octet-stream'
          },
          file: thumbImg
        })
        promisesThumb.then(function(data){
          console.log('video uploaded..');
          console.log('saving data..');
          media['videothumb'] = newThumbName;
          $scope.loadingMsg = 'saving project please wait...';
          $http({
            url: API_URL + "/medialibrary/update",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(media)
          }).success(function (data, status, headers, config) {
            $scope.loadingg = false;
            $scope.isSaving = false;
            $location.hash('successget');
            $anchorScroll();
            console.log('data saved..');
            $scope.successget = 'Video successfully Updated.'; 
            $location.hash('');
          });
        });
      }else{
        $http({
          url: API_URL + "/medialibrary/update",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(media)
        }).success(function (data, status, headers, config) {
          $scope.loadingg = false;
          $scope.isSaving = false;
          $scope.cNTS = [];
          $scope.closeAlert1(); 
          $location.hash('successget');
          $anchorScroll();
          console.log('data saved..');
          $scope.successget = 'Video successfully Updated.'; 
          $location.hash('');
        });
      }
    }

    
  }

  $scope.category = [];
  var loadcategory = function() {

    $http({
      url: API_URL + "/medialibrary/listcategoryall",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function(data) {
      $scope.category = data;
    }).error(function(data) {
      $scope.status = status;
    });
  }
  loadcategory();

  $scope.tag = [];
  var loadtags = function() {
    $http({
      url: API_URL + "/medialibrary/listtagsall",
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (data, status, headers, config) {
      var newlist = [];
      data.map(function(val){
        newlist.push(val.tags);
      });
      $scope.select2Options = {
        'multiple': true,
        'simple_tags': true,
        'tags': newlist
      };
      $scope.tag = newlist;

    }).error(function (data, status, headers, config) {
      data = data;
    });
  }
  loadtags();

  $scope.videoAlerts = [];
  $scope.closeAlert = function(index) {
    $scope.videoAlerts.splice(index, 1);
  };
  $scope.imageselected = false;
  $scope.prepareThumb = function(file) {
    console.log(file[0]);
    if (file && file.length) {
      if (file[0].size > 2000000) {
        console.log('File is too big!');
        $scope.videoAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.thumbImg = [];
      } else {
        console.log("below maximum");
        $scope.thumbImg = file[0];
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }

  // Editor options.
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false,
    toolbarGroups: [
        { name: 'editing',     groups: [ 'selection', 'spellchecker' ] },
        { name: 'colors' },
        { name: 'insert', groups: [ 'Image', 'Youtube', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'links' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'tools',    groups: [ 'mode' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] },
        { name: 'document',    groups: [ 'doctools' ] },
        { name: 'styles' }
        ]
  };

  // Called when the editor is completely ready.
  $scope.onReady = function () {
    // ...
  };


}).directive('phone', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

      ngModelCtrl.$parsers.push(function(viewValue) {
              // console.log(viewValue)
              if (viewValue!=undefined) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
              }


            });

      ngModelCtrl.$render = function() {
        $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
      };

      $element.bind('change', listener);
      $element.bind('keydown', function(event) {
        var key = event.keyCode;
        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
          return;
        }
        $browser.defer(listener);
      });

      $element.bind('paste cut', function() {
        $browser.defer(listener);
      });
    }

  };
}).filter('tel', function () {
  return function (tel) {
          // console.log(tel);

          if (!tel){ 
            return ''; 
          }else{
           var value = tel.toString().trim().replace(/^\+/, '');

           if (value.match(/[^0-9]/)) {
            return tel;
          }

          var country, city, number;

          switch (value.length) {
            case 1:
            case 2:
            case 3:
            city = value;
            break;

            default:
            city = value.slice(0, 3);
            number = value.slice(3);
          }

          if(number){
            if(number.length>3){
              number = number.slice(0, 4) + ' ' + number.slice(4,7);
            }
            else{
              number = number;
            }

            return ("(" + city + ") " + number).trim();
          }
          else{
            return "(" + city;
          }
        }



      };
    }); 