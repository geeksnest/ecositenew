'use strict';

/* Controllers */
app.controller("mymedialistCtrl", function ($scope, $http, $anchorScroll,$modal, Countries,store, MDYform, Config, Upload, $filter, $timeout, $sce, $q) {

	console.log('===index===');

	$scope.amazonlink = Config.amazonlink;
    
  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    // console.log(token);
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
    $scope.username = usernameAgent;
  }else{    
    $window.location = '/login';  
  }  
  //Percentage Formula
  // percentage = (value/totalvalue)* 100

  var offset = 0;
  var num = 4;
  $scope.loading = false;
  $scope.loadingGIF = false;

  var loadMorevideo = function(){    
    $http({
      url: API_URL+"/mymedialibrary/felistmore/"+offset+"/"+num+"/"+ $scope.userAgent,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.oyaaa = data.data;
      $scope.allCount = data.count;
      $scope.previewlink = BASE_URL+'/medialibrary/preview/'+token.username;
      $scope.mymedialink = BASE_URL+'/medialibrary/'+token.username;   
      $scope.loadingGIF = false;
      if(data.count == data.data.length){
        $scope.loading = true;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMorevideo();

  $scope.loadmorebot = function(){
    num = num + num;
    $scope.loadingGIF = true;
    loadMorevideo();
  }

  $scope.showvideo = function(id,stat){
     ///mymedialibrary/changeshowvideo/
     $http({
      url: API_URL+"/mymedialibrary/changeshowvideo/"+id+"/"+stat,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      loadMorevideo();
    })
  }

  $scope.deletevideo = function (id) {
    var modalInstance = $modal.open({
      templateUrl: 'deleteMyVideo.html',
      controller: deleteMyVideoCtrl,
      resolve: {
        id: function () {
          return id
        }
      }
    });
  };
  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  var alertdelete = function(){
    $scope.alerts.push({type: 'success', msg: 'Video successfully deleted.'});
  }

  var deleteMyVideoCtrl = function ($scope, $modalInstance, id) {
        $scope.id = id;
        $scope.ok = function (id) {
          $scope.closeAlert();
          $http({
            url: API_URL + "/mymedialibrary/deleteMyVideo/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            loadMorevideo();
            alertdelete();
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }


});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});
