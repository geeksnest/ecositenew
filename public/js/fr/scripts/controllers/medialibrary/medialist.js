'use strict';

/* Controllers */
app.controller("medialistCtrl", function ($scope, $http, $anchorScroll,$modal, Countries,store, MDYform, Config, Upload, $filter, $timeout, $sce, $q, $window) {

	console.log('===index===');

  $scope.amazonlink = Config.amazonlink;
  var loadvideos = function(){    
    $http({
      url: API_URL+"/medialibrary/index",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      // console.log(data.featured[0]);
      $scope.featured = data.featured[0];
       $scope.views = data.featured[0].viewscount;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadvideos();


  var pupular = function(){    
    $http({
      url: API_URL+"/medialibrary/mostpopular",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log('popular');
      // console.log(data.data[0]);
      $scope.pupularcheck = data.data[0];
      $scope.pupular = data.data;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  pupular();

  var offset = 0;
  var num = 12;
  $scope.loading = false;
  $scope.loadingGIF = false;

  var loadMorevideo = function(){    
    $http({
      url: API_URL+"/medialibrary/felist/"+offset+"/"+num,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data.data);
      $scope.allList = data.data;
      $scope.allCount = data.count;
      $scope.loadingGIF = false;
      if(data.count == data.data.length){
        $scope.loading = true;
      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadMorevideo();

  $scope.toview = function(slugs){
    console.log('click');
    $window.location = '/medialibrary/' + slugs;
  }
  $scope.loadmorebot = function(){
    num = num + num;
    $scope.loadingGIF = true;
    loadMorevideo();
  }




});

app.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});
