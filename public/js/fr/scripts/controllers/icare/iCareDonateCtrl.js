

app.controller("iCareDonateCtrl", function ($scope, $http, $location, $anchorScroll, MDYform, Countries) {

  $scope.chossemet = true;
  $scope.newalready = true;
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.reccur_success = 1;
  $scope.echeck_success = 1;
  $scope.back_button = 1;
  $scope.errormessage = '';
  $scope.donateamount = 10.00;
  $scope.paypalformshow = false;
  $scope.minimumdonation = '#999';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  $scope.new = 'new';


  var d = new Date();
  var n = d.getFullYear();
  $scope.donday = MDYform.day();
  $scope.donmonth = MDYform.month();
  $scope.donyear = MDYform.year();

  $scope.chtli = function () {

    $scope.new = "already";
      // $('#donate2').val('already');        
    };
    $scope.reccur_unit = [
    {
      'name': 'months'
    },
    {
      'name': 'days'
    }
    ]

    $scope.scrollToHash = function(){
        // console.log('scroll to triggered');
        $location.hash('signup');
        $anchorScroll();
      }

      $scope.submitecheck = function(data, amount, email){
        // console.log('Submit Echeck');
        data['amount'] = amount;
        data['email'] = email;
        $scope.reccur_success = 2;
        $http.post(API_URL + '/donate/echeck', data).success(function (response) {
            // console.log('Should Echeck Recieve Respon');
            // console.log(response);
            if(data.recurpayment2){
              $scope.reccur_success = 3;
              if(response.code=='I00001'){
                $scope.back_button = 1;
                window.location = "/complete/" + amount;
                    //$scope.reccur_message = "Your donation has been successfully processed.";
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.text;
                  }
                }else{
                  $scope.reccur_success = 3;
                  if(response.hasOwnProperty('success')){
                    // console.log('success');
                    $scope.back_button = 1;
                    window.location = "/complete/" + amount;
                    //$scope.reccur_message = response.success;
                  }else if(response.error == null){
                    $scope.back_button = 2;
                    $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
                  }else{
                    $scope.back_button = 2;
                    $scope.reccur_message = response.error;
                  }
                }
              });
}

$scope.submitcredit = function(data, amount, email){
  data['amount'] = amount;
  data['email'] = email;
        // console.log(data);
        $scope.reccur_success = 2;

        $http.post(API_URL + '/donate/creditcard', data).success(function (response) {
        // console.log('Should Recieve Respon');
        // console.log(response);
        if(data.recurpayment){
          $scope.reccur_success = 3;
          if(response.code=='I00001'){
            $scope.back_button = 1;
            window.location = "/complete/" + amount;
                //$scope.reccur_message = "Your donation has been successfully processed.";
              }else{
                $scope.back_button = 3;
                $scope.reccur_message = response.text;
              }
            }else{
              $scope.reccur_success = 3;
              if(response.hasOwnProperty('success')){
            // console.log('success');
            $scope.back_button = 1;
            window.location = "/complete/" + amount;
            //$scope.reccur_message = response.success;
          }else if(response.error == null){
            $scope.back_button = 3;
            $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
          }else{
            $scope.back_button = 3;
            $scope.reccur_message = response.error;
          }
        }
      });
}



$scope.submitcreditworeg = function(ccworeg,donateamount){
  console.log('creditcard woreg');
  console.log(ccworeg);
  console.log(donateamount);
  $scope.reccur_success = 2;
  $scope.process = true;
  ccworeg['amount'] = donateamount;

  $http.post(API_URL + '/donate/creditcardworeg', ccworeg).success(function (response) {
      // console.log('Should Recieve Respon');
      // console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // console.log('success');
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
        //$scope.reccur_message = response.success;
      }else if(response.error == null){
        $scope.process = false;
        $scope.back_button = 3;
        $scope.reccur_success = 1;
        $scope.errorpayment = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        // console.log(response);
        $scope.back_button = 3;        
        $scope.reccur_success = 1;
        $scope.process = false;
        $scope.reccur_message = response.error;        
        $scope.errorpayment = response.error + " And try again.";
      }
    });
}


$scope.submitecheckworeg = function(checkworeg,donateamount){
  console.log('echeck woreg');
  console.log(checkworeg);
  console.log(donateamount);
  $scope.reccur_success = 2;
  $scope.process = true;

  checkworeg['amount'] = donateamount;

  $http.post(API_URL + '/donate/echeckcardworeg', checkworeg).success(function (response) {
      // console.log('Should Recieve Respon');
      // console.log(response);
      $scope.reccur_success = 3;
      if(response.hasOwnProperty('success')){
        // console.log('success');
        $scope.back_button = 1;
        window.location = "/complete/" + $scope.donateamount;
        //$scope.reccur_message = response.success;
      }else if(response.error == null){
        $scope.process = false;
        $scope.back_button = 3;
        $scope.reccur_success = 1;
        $scope.errorpayment = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{
        $scope.back_button = 3;        
        $scope.reccur_success = 1;
        $scope.process = false;
        $scope.reccur_message = response.error;        
        $scope.errorpayment = response.error + " And try again.";
      }
    });
}
$scope.submitpaypalworeg = function(howdidyoulearn, cname){
  var cn='';
  if(cname == undefined){
    cn = '';
  }else{
    cn = cname.replace(/\s+/g, '*');
  }

  var hdyl='';

  if(howdidyoulearn == undefined){
    hdyl = '';
  }else{
    hdyl = howdidyoulearn.replace(/\s+/g, '*');
  }
  var val = 'user@email.com other ' + hdyl + ' ' + cn;
  $('#paypalcustom').val(val);
  console.log(val);
  $('#paypalsingle').submit();
  $scope.paypalbutton = true;
}

$scope.back_click = function(){
        // console.log('back click');
        $scope.reccur_success =1;
      }

      $scope.clickDonate = function (data)
      {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
          if (response.hasOwnProperty('error')) {
            $scope.errormessage = response.error;
          } else {
            $scope.creditcard123 = true;
            $scope.newalready = false;
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.paypalformshow1 = true;
            $scope.membername = response.success;
            $scope.statusmessage = '';
            $scope.forget = true;
            $scope.create = true;
            $scope.process = false;
            $scope.chossemet = false;

          }
          $scope.process = false;
        });
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.errormessage = '';
          $scope.paypalformshow = true;
          $scope.membername = response.success;
        }
        $scope.process = false;
      };
      $scope.change = function (amount) {
        if (amount <= 10) {
          $scope.minimumdonation = 'red';
        } else {
          $scope.minimumdonation = '#999';
        }
      };
      $scope.emailPurpose = null;
      $scope.showemail = false;
      $scope.emailSent = false;
      $scope.statusmessage = '';
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        // console.log($scope.showemail);

      }
      $scope.paypalformshow1 = false;
      $scope.create = false;
      $scope.forget = false;
      $scope.statusmessage = '';
      $scope.disp = false;
      $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

      }
      $scope.sendemail = function (data) {
        // console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
          if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
            $scope.statusmessage = response.error;
          } else {
            $scope.paypalformshow1 = false;
            $scope.statusmessage = 'Your requested details have been sent to your email!';
            $scope.errormessage = '';
          }
          $scope.emailPurpose = null;
          $scope.showemail = false;
          $scope.emailSent = false;
        });
      }
      $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
      }
    });
