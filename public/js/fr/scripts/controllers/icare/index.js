'use strict';

/* Controllers */
app.controller("iCareCtrl", function($scope, $http, $sce, $location, $q, $anchorScroll, MDYform, Config, Upload, Countries, $timeout) {

 
  $scope.orangebar = false; 
  $scope.iCareMenu = true; 
  $scope.amazonlink = Config.amazonlink;

  var loadSlider = function(){

    $http({
      url: API_URL+"/utility/iCare",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var min = 0;
      var max = data.length - 1;
      var num1 = Math.floor(Math.random() * (max - min + 1)) + min;
      var num2 = Math.floor(Math.random() * (max - min + 1)) + min;
      $scope.iCareImg1 = data[num1];
      if(num1 == num2){
        if(num1 == max){
          $scope.iCareImg2 = data[num1-1];
        }else{
          $scope.iCareImg2 = data[num1+1];
        } 
      }else{
        $scope.iCareImg2 = data[num2];       
      }
      
      console.log($scope.iCareImg1);
      console.log($scope.iCareImg2);
      console.log(max);
    });
  }
  loadSlider();

  var loadContent = function(){
    var pageID = "634DCF53-0BDF-46A7-9C5C-7DE709DE24F1";
    $http({
      url: API_URL+"/pages/getContent/" + pageID,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {      
      $scope.content_1 = $sce.trustAsHtml(data[0].content_1);
      $scope.content_2 = $sce.trustAsHtml(data[0].content_2);
      $scope.content_3 = $sce.trustAsHtml(data[0].content_3);
      $scope.content_4 = $sce.trustAsHtml(data[0].content_4);
      $scope.content_5 = $sce.trustAsHtml(data[0].content_5);
      // $scope.content = function(string) {
      //   return $sce.trustAsHtml(data[0]);
      // };
      console.log($scope.content);      
    });
  }
  loadContent();

  // $timeout(function() {    
  //   $('.bxslider').bxSlider({
  //       mode: 'fade'
  //     });
  // }, 2000);

});