'use strict';

/* Controllers */
app.controller("Createclubctrl", function ($scope,CallheroesFactory, $http, $anchorScroll, $location, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {

  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    // console.log(token);
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
  }else{    
    $window.location = '/login?club=create';  
  }

  $(".numberonly").keyup(function(){
    var value = $(this).val();
    value = value.replace(/^(0*)/,"");
    $(this).val(value);
  }); 

 
  $scope.imageselected = false;
  $scope.loadingg = false;
  var agentstorage;
  $scope.type = 'days';
  $scope.countries = Countries.list();

  //date picker
 $scope.closeAlerts = function (index) {
    $scope.imgAlerts.splice(index, 1);
  };
  $scope.successget = '';
  $scope.errorget = '';

  $scope.closeAlert = function(index) {
    $scope.resultget.splice(index, 1);
  };
 
  $scope.resultget = [];

  $('form').validate({
    rules: {
      clubname: {
        required: true
      },
      description: {
        required: true
      },
      city: {
        required: true,
      },
      state: {
        required: true
      },
      zip: {
        required: true
      },
      country: {
        required: true
      },
      meeting: {
        required: true
      },
      contactperson: {
        required: true
      },
       email: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $scope.imageselected = false;
  $scope.loadingg = false;
  $scope.imgAlerts = [];
$scope.prepare = function(file) {
  console.log(file)
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.imageselected = false;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      } else {
        $scope.projImg = file;
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }

$scope.uniqueslugs = function(slugs){
    $http({
        url: API_URL+"/club/uniquename/" + slugs,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        if(data==0){
                     $scope.validslugs = false;
                 }
                 else{
                    $scope.validslugs = true;
                }
      });       
}

  $scope.submitclub = function (club,files){
   $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    club['createdby'] = $scope.userAgent;
    var text = club.name;
    var text1 = text.replace(/[^\w -]+/g,'');
    club['slugs'] = angular.lowercase(text1.replace(/ +/g,'-'));
     if(
      club.name==""                || club.name==undefined
      || club.city==""             || club.city==undefined
      || club.state==""            || club.state==undefined 
      || club.postal==""           || club.postal==undefined 
      || club.country==""          || club.country==undefined
      || club.meeting==""          || club.meeting==undefined
      || club.emailadd==""         || club.emailadd==undefined
      || club.contactperson==""    || club.contactperson==undefined
      || club.description==""      || club.description==undefined
      )
      {
      $scope.alerts =[{type: 'danger', msg: 'Please Make sure you fill up out all the fields before submitting!!'}];
      $scope.chkerror = true;
            try {
              if($scope.club.country){
               $scope.nd = false;
             }else{
              $scope.nd = true;
            }
          }
          catch (e) {
            $scope.nd = true;
          }
 }
 else if($scope.validslugs){
  $('#name').focus();
 }
 else if (club.low && club.high) {
      if (parseInt(club.low) > parseInt(club.high)) {
        $scope.edge = true;
        $('#high').focus();
      }else{
        club['age'] = club.low+'-'+club.high;
        saveme(club,files);
        $scope.edge = false;
      }
    }else if(club.low  && !club.high){
      $('#high').focus();
      $scope.edge = true;
    }else if(!club.low  && club.high){
     $('#low').focus();
     $scope.edge = true;
   }else{
      club['age'] = 0+'-'+0;
      saveme(club,files);
  }  

};

  var saveme = function(club,files){
         $scope.edge = false;
       $scope.loadingMsg = 'initializing..';  
      if(!files){
        $scope.errorget = 'Please choose image to upload.';
        $scope.alerts =[{type: 'danger', msg: 'Please choose image to upload.'}];
      }else{
        var file = files[0];
        if(file==undefined || file == ""){
          $scope.alerts =[{type: 'danger', msg: 'Please choose image to upload.'}];
          $scope.loadingg = false;
          $scope.imagecontent=true;
        }else{
          if (file.size >= 2000000) {
            $scope.loadingg = false;
            $scope.imagecontent=true;
            $scope.alerts =[{type: 'danger', msg: 'Image File is too big.'}];
          }else{
            $scope.loadingg = true;
            $scope.loadingMsg = 'Submitting please wait...';
            // console.log('uploading image bwahahahhahahhahah..');
            $scope.imageloader=true;
            var promises;
            var f = file.name;
            var fname=f.replace(/\s+/g, "-");
            promises = Upload.upload({
              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter, AllowedHeader) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/club/' + fname,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
            })
            promises.then(function(data){
             var ff = data.config.file.name;
             var fn = ff;
             club['image']=fn.replace(/\s+/g, "-");
              $scope.alerts.splice(0, 1);
              $scope.chkerror = false;
              $scope.isSaving = true;
              CallheroesFactory.clubcreate(club, function(data){
                // if(data.success){
                  console.log(data)
                  $scope.loadingg = false;
                  $scope.imageselected = false;
                  $scope.isSaving = false;
                  $scope.n = false;
                  $scope.nd = false;
                  $scope.alerts =[{type: 'success', msg: 'Thank you. Your information has been received. We will contact you shortly.'}];
                  $scope.club = "";
                  $scope.projImg = [];
                  $scope.files = undefined;
                  $scope.form.$setPristine();
                  console.log('data saved..');
                // }
              });
              
            });
          }
        }
      }
  }

});