'use strict';

/* Controllers */

app.controller('LoginCtrl', function ($scope, $location, store, jwtHelper, $window, Login, Config, User) {

  console.log('==== Login Page ====');

  // Login.redirectToMainifLogin();  
  
  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    console.log(token);
  } 

  $scope.formStatus = false;
  $scope.master = {};

  $scope.phoneNumberPattern = (function() {
    var regexp = /^[a-zA-Z0-9]+$/;
    return {
      test: function(value) {
        if ($scope.requireTel === false) {
          return true;
        }
        return regexp.test(value);
      }
    };
  })();

  $scope.reset = function(form) {
    $scope.$broadcast('show-errors-reset');
  }
  $scope.loginerror = '';
  $scope.login = function(user) {
    if(user == undefined || user.email == null || user.password == null || user.password == undefined){
      $scope.loginerror = 'Username/Email or password is invalid!';
      console.log('error ahh');
    }else{
      console.log('Login na');
      User.login(user, function(data) {
        if (data.data != null) {
          store.set('AccessToken', data.data[0]);

          var url = window.location.href;
          var auth = url.split('login?videolike=');
          var donatecontent = url.split('login?medialibrary=');
          var club = url.split('login?club=');
          var createproj = url.split('login?projects=');
          var don = donatecontent[1];
          var videoslugs = auth[1]; 
          var clubcreate = club[1]; 
          var projcreate = createproj[1];
          console.log(don);
          if(videoslugs!=null){
            $window.location = '/medialibrary/' +  videoslugs;
          }
          else if(don!=null){
            $window.location = '/medialibrary/addvideo';
          }
          else if(clubcreate!=null){
            $window.location = '/create-your-club';
          }
          else if(projcreate!=null){
            $window.location = 'projects/createproject';
          }
          else{
             $window.location = '/projects'; 
          }                  
          console.log('success');
        } else {
          $scope.loginerror = data.error;
          console.log('error');
        }
      });  
    }

  }

}).controller('ForgotpassCtrl', function ($scope, $location, store, $http, jwtHelper, $window, Login, Config, User) {

  console.log('==== Forgot password Page ====');

  // Login.redirectToMainifLogin();  
  
  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    console.log(token);
  } 

  $scope.formStatus = false;
  $scope.master = {};

$scope.closeAlerts = function(index){
$scope.alertss.splice(index,1)
}

  $scope.alertss = [];
  $scope.showform = true;
  $scope.showsuccess = false;
  $scope.res = function(send) {
    // $scope.$broadcast('show-errors-reset');
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    var ex = re.test(send.email);
    // console.log(ex)
    if (ex == false) {
      $scope.alertss = [{type:'danger',msg:"Please Enter Valid Email"}];
    }else{
      if($scope.alertss.length>0)
      {
        $scope.closeAlerts();
      }
     $scope.sending = true;
       $http({
        url: API_URL + "/forgotpass/resetpasswordfront",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(send)
      }).success(function (data, status, headers, config) { 
        // console.log(data)
        if (data.success) {
          $scope.success = data.msg;
          $scope.showsuccess = true;
          $scope.showform = false;
        }else{
          $scope.formforgot = false;
          $scope.authError = true;
          $scope.alertss = [{type:data.type,msg:data.msg}];
          $scope.sending = false;
          $scope.form.$setPristine();
          $scope.send = "";
        }
      
      }).error(function (data, status, headers, config) {
        console.log('not success');
        $scope.alertss = [{type:data.type,msg:data.msg}];
        $scope.sending = false;
      });
    }
  

  }
  $scope.loginerror = '';

}).controller('ChangepassCtrl', function ($scope, $location, store, $http, jwtHelper, $window, Login, Config, User) {

  console.log('==== Change password Page ====');
  // console.log()
  var token = $('#token').val();
  $http({
    url: API_URL + "/checktoken/front/"+ token,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function (data, status, headers, config) {
    // console.log(data.msg);
    if(data.msg == 'valid')
    {
      $scope.validform = true;
      $scope.authError = false;
      console.log("token found!")
    }
    else
    {
      $scope.validform = false;
      $scope.authError = true;
    }
  }).error(function(data, status, headers, config) {

  });

$scope.closeAlerts = function(index){
$scope.alertss.splice(index,1)
}

  $scope.minPass = function(forgot) {
    if (forgot.password != undefined) {
     if(forgot.password.length < 6){
      $scope.passwordMin = true;
    }else{
      $scope.passwordMin = false;
    }
  }else{
      $scope.passwordMin = false;
    }

};

  $scope.alertss = [];
  $scope.res = function(forgot) {
    if ($scope.passwordMin == true) {

    }
    else if(forgot.password != forgot.repassword){
       $scope.alertss = [{type:'danger',msg:"Passwords do not match"}];
    }
    else{
      if($scope.alertss.length>0)
      {
         $scope.closeAlerts();
      }
     $scope.sending = true;
       $http({
        url: API_URL + "/updatepassword/passwordfront",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(forgot)
      }).success(function (data, status, headers, config) { 
    $scope.validform = false;
    $scope.authsuccess = true;
      }).error(function (data, status, headers, config) {
        console.log('not success');
        $scope.alertss = [{type:'danger',msg:data.msg}];
        $scope.sending = false;
      });
    }
  

  }
  $scope.loginerror = '';

})
