'use strict';

/* Controllers */
app.controller("createCtrl", function ($scope, $http, $anchorScroll, $location, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {


  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    // console.log(token);
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
  }else{    
    $window.location = '/login?projects=create';  
  }

  $(".don5t10n").keyup(function(){
    var value = $(this).val();
    value = value.replace(/^(0*)/,"");
    $(this).val(value);
  }); 
  $scope.limitDays = function(days) {
    if (days < 0) $scope.proj.days = 0;
    if (days > 60) $scope.proj.days = 60;
  };
  $scope.limitAmount = function(amount) {
    if (amount < 0) $scope.proj.projGoal = 0;
    if (amount > 5000) $scope.proj.projGoal = 5000;
  };

  $scope.imageselected = false;
  $scope.loadingg = false;

  $scope.type = 'days';
  $scope.countries = Countries.list();

  //date picker
  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date(Date.now()+1*24*60*60*1000);
  };
  $scope.toggleMin();
  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };
  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };
  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  //time picker
  $scope.time1 = new Date();
  $scope.showMeridian = true;
  $scope.disabled = false;

  //check image
  $scope.imgAlerts = [];
  $scope.closeAlert = function(index) {
    $scope.imgAlerts.splice(index, 1);
  };
  $scope.imageselected = false;
  $scope.prepare = function(file) {
    console.log(file[0]);
    if (file && file.length) {
      if (file[0].size > 2000000) {
        $scope.imageselected = false;
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];       
        $scope.projImg = [];
      } else {
        console.log("below maximum");
        $scope.projImg = file;
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }
  }

  $scope.errorget = '';
  $scope.loadingMsg = 'Saving project Please wait...';
  $scope.saveProj = function (proj, files){
    
    $scope.errorget = '';
    $scope.successget = ''; 

    $scope.loadingMsg = 'initializing..';  

    if(!files){
      $scope.errorget = 'Please choose image to upload.';
      $location.hash('errorget');
      $anchorScroll();
    }else{
      if(proj.datedue){
        var daydate = proj.datedue;
      }else{
        var daydate = proj.days;
      }
      
      if(!proj.projTitle || !proj.projShortDesc || !proj.projLoc || !proj.projGoal || !daydate){
        $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
        $location.hash('errorget');
        $anchorScroll();
      }else{
        $scope.errorget = '';
        $scope.loadingg = true;
        var getTime = document.getElementById('getTime').value;
        var time = getTime;
        console.log(time);
        if(time==''){
          $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
          $location.hash('errorget');
          $anchorScroll();
          $scope.loadingg = false;
        }else{
         var hours = Number(time.match(/^(\d+)/)[1]);
         var minutes = Number(time.match(/:(\d+)/)[1]);
         var AMPM = time.match(/\s(.*)$/)[1];
         if (AMPM == "PM" && hours < 12) hours = hours + 12;
         if (AMPM == "AM" && hours == 12) hours = hours - 12;
         var sHours = hours.toString();
         var sMinutes = minutes.toString();
         if (hours < 10) sHours = "0" + sHours;
         if (minutes < 10) sMinutes = "0" + sMinutes;
         var hh = sHours;
         var mm = sMinutes;

         proj['time'] = hh+':'+mm+':'+'00';

         var slugs = proj.projTitle;

         var text = slugs.replace(/[^\w ]+/g,'');
         var projSlugs = angular.lowercase(text.replace(/ +/g,'-'));

         proj['projSlugs'] = projSlugs;

         if(proj['datedue'] != undefined){
          proj['datedue'] = proj.datedue.toString();
        }

        console.log(files);
        console.log(proj);

        console.log('initializing..');

        $scope.loadingMsg = 'Saving project please wait...';

        console.log('checking image size..');
        var file = files[0];
          if(file==undefined){
            $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
            $location.hash('errorget');
            $anchorScroll();
            $scope.loadingg = false;
            $scope.imagecontent=true;
          }else{
            if (file.size >= 2000000) {
          $scope.errorget = 'Something went wrong. Please check the fields with (*) and fill it up.';
          $location.hash('errorget');
          $anchorScroll();
          $scope.loadingg = false;
          $scope.imagecontent=true;
        }
        else{
            $scope.loadingMsg = 'Saving project please wait...';
            console.log('uploading image..');
            $scope.imageloader=true;

            var promises;
            promises = Upload.upload({

              url: Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
              },
              fields : {
                key: 'uploads/projects/' + file.name,
                AWSAccessKeyId: Config.AWSAccessKeyId,
                acl: 'private',
                policy: Config.policy,
                signature: Config.signature,
                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
              },
              file: file
            })
            promises.then(function(data){
              console.log('image uploaded..');
              console.log('saving data..');
              $scope.loadingMsg = 'Saving project please wait...';
              proj['projThumb'] = data.config.file.name;

              $http({
                url: API_URL + "/project/saveproject",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(proj)
              }).success(function (data, status, headers, config) {
                $scope.loadingg = false;
                $scope.imageselected = false;
                $scope.isSaving = false;
                console.log('data saved..');
                $window.location = '/projects/' + usernameAgent + '/' + projSlugs;
              }).error(function (data, status, headers, config) {
                $scope.status = status;
                console.log(data);
              });
            });
            }
          }
        }
      }
    }


  }

});