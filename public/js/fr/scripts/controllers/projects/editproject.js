'use strict';

/* Controllers */
app.controller("editprojectCtrl", function ($scope, $http, $anchorScroll, $location, Countries, MDYform, Config, Upload, $filter, $timeout, $sce, $q, store, jwtHelper, $window) {

  $scope.amazonlink = Config.amazonlink;
  $scope.story = false;
  if (store.get('AccessToken')) {
    var token = store.get('AccessToken');
    $scope.userAgent = token.userid;
    var usernameAgent = token.username;
  }else{
    $window.location = '/projects';
  }

  $(".don5t10n").keyup(function(){
    var value = $(this).val();
    value = value.replace(/^(0*)/,"");
    $(this).val(value);
  });
   
  $scope.limitDays = function(days) {
    if (days < 0) $scope.proj.days = 0;
    if (days > 60) $scope.proj.days = 60;
  };
  $scope.limitAmount = function(amount) {
    if (amount < 0) $scope.proj.projGoal = 0;
    if (amount > 5000) $scope.proj.projGoal = 5000;
  };

  // CK Editor options.
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false,
    uiColor: '#ffffff',
    toolbarGroups: [
        { name: 'editing',     groups: [ 'selection', 'spellchecker' ] },
        { name: 'colors' },
        { name: 'insert', groups: [ 'Image', 'Youtube', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'links' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'tools',    groups: [ 'mode' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] },
        { name: 'document',    groups: [ 'doctools' ] },
        { name: 'styles' }
        ]
        // NOTE: Remember to leave 'toolbar' property with the default value (null).      
  };
  $scope.readonlyInput = true;
  // Called when the editor is completely ready.
  $scope.onReady = function () {
    // ...
  };
  //get the slugs
  var url = window.location.href;
  var auth = url.match(/\/projects\/(.*)\/(.*)+/);
  var slugs = auth[2];
  var getactive = auth[2].toString().split("##");
    console.log(slugs);
    var newslug = slugs.split('#');

  if(newslug[0] == 'null'){
    $window.location = '/page404';
  }
  $scope.activetab = getactive[1];
  if(getactive[1] == '5' || getactive[1] == 'donation'){
    $('#item1').removeClass('active');
    $('#item4').addClass('active');
    $location.hash('donation');
    $anchorScroll();
  }else if(getactive[1] == '1' || getactive[1] == '3'){
    $location.hash('notification');
    $anchorScroll();
  }

  //get data
  var loadproject = function(){

    $http({
      url: API_URL+"/project/viewMyProject/" + slugs,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      var dur = data.data[0].projDuration.toString().split(" ");
      $scope.type = data.data[0].projDurType;
      $scope.proj = data.data[0];      
      $scope.don = data.donations;      
      $scope.donCount = data.donCounts[0];      
      $scope.projDesc = $sce.trustAsHtml(data.data[0].projDesc);
      $scope.proj['projID'] = data.projID;
      $scope.projstat = data.data[0].projStatus;
      console.log(data.data[0].projDurType);
      if(data.data[0].projDurType != 'days'){
        var getDate = dur[0].toString().split("/");
        $scope.proj['datedue'] = getDate[2]+'-'+getDate[0]+'-'+getDate[1];
        $scope.proj['datedue2'] = getDate[2]+'-'+getDate[0]+'-'+getDate[1];

        var getTime = dur[1].toString().split(":");
        var hh = getTime[0];
        var mm = getTime[1];        
        $scope.time1 = new Date();
        $scope.time1.setHours(hh, mm);
        $scope.showMeridian = true;
      }else{        
        $scope.proj['days'] = dur[0];

        $scope.time1 = new Date();
        $scope.showMeridian = true;
/*        $scope.disabled = false;*/

      }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadproject();

  $scope.imageselected = false;

  $scope.type = 'days';
  $scope.countries = Countries.list();

	//date picker
	$scope.today = function () {
		$scope.dt = new Date();
	};
	$scope.today();
	$scope.clear = function () {
		$scope.dt = null;
	};
	$scope.toggleMin = function () {
		$scope.minDate = $scope.minDate ? null : new Date(Date.now()+1*24*60*60*1000);
	};
	$scope.toggleMin();
	$scope.open = function ($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = true;
	};
	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1,
		class: 'datepicker'
	};


	$scope.initDate = new Date('2016-15-20');
	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];

  //check image
  $scope.imgAlerts = [];
  $scope.closeAlertz = function(index) {
    $scope.imgAlerts.splice(index, 1);
  };
  $scope.imageselected = false;
  $scope.thumbchanged = false;
  $scope.prepare = function(file) {
    console.log(file[0]);
    
    if (file && file.length) {
      if (file[0].size > 2000000) {
        document.getElementById("left").focus();
        document.getElementById("left").blur();
        console.log('File is too big!');
        $scope.imgAlerts = [{
          type: 'danger',
          msg: 'The file "' + file[0].name + '" is too big. Please resize the image below 2MB.'
        }];
        $scope.imageselected = false;       
        $scope.projImg = [];
        $scope.files = undefined;
        $scope.thumbchanged = false;
      } else {
        console.log("below maximum");
        $scope.projImg = file;
        $scope.imgAlerts = [];
        $scope.imageselected = true;
        $scope.thumbchanged = true;
      }
    }
  }

  $scope.errorget = '';
  $scope.successget = '';
  $scope.isLoading = false;
  
  $scope.saveProj = function (proj, files, type){

    $scope.botMsg = 'Saving please wait...';
    $scope.isLoading = true;
    $scope.imgAlerts = [];
    $scope.resultget = [];
    $scope.errorget = '';
    $scope.successget = ''; 

    //validation area
    $scope.isLoading = true;
    $scope.resultget = [];
    $scope.successget = ''; 
    var valid = true;
    $scope.cNTS = [];  
    $scope.borderError = [];  
    if(!proj.projTitle){ 
      $( "#projTitle" ).addClass( "red-border" ); 
      $scope.cNTS.push({id: 'projTitle',msg: 'Project Title'}); 
      valid = false;
    }else{
      $( "#projTitle" ).removeClass( "red-border" );
    }
    if(!proj.projShortDesc){
      $( "#projShortDesc" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Short Description'});
      valid = false;
    }else{      
      $( "#projShortDesc" ).removeClass( "red-border" ); 
    }
    if(!proj.projCName){
      $( "#projCName" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Contact Name'});
      valid = false;
    }else{      
      $( "#projCName" ).removeClass( "red-border" ); 
    }
    if(!proj.projCAddress1){
      $( "#projCAddress1" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Address 1'});
      valid = false;
    }else{      
      $( "#projCAddress1" ).removeClass( "red-border" ); 
    }
    if(!proj.projCCity){
      $( "#projCCity" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'City'});
      valid = false;
    }else{      
      $( "#projCCity" ).removeClass( "red-border" ); 
    }
    if(!proj.projCState){
      $( "#projCState" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'State'});
      valid = false;
    }else{      
      $( "#projCState" ).removeClass( "red-border" ); 
    }
    if(!proj.projCZip){
      $( "#projCZip" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Zip/Postal Code'});
      valid = false;
    }else{      
      $( "#projCZip" ).removeClass( "red-border" ); 
    }
    if(!proj.projCCountry){
      $( "#projCCountry" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Country'});
      valid = false;
    }else{      
      $( "#projCCountry" ).removeClass( "red-border" ); 
    }
    if($scope.type == 'days'){
      if(!proj.days){
        $( "#days" ).addClass( "red-border" ); 
        $scope.cNTS.push({msg: 'Days Duration'});
        valid = false;
      }else{      
        $( "#days" ).removeClass( "red-border" ); 
      }
    }else{
      var getTime = document.getElementById('getTime').value;
      if(!proj.datedue){
        $( "#datedue" ).addClass( "red-border" ); 
        $scope.cNTS.push({msg: 'Date Duration'}); 
        valid = false;
      }else{      
        $( "#datedue" ).removeClass( "red-border" );
      }
      if(!getTime){
        $scope.cNTS.push({msg: 'Time Duration'}); 
        valid = false;
      }
    }
    if(!proj.projGoal){
      $( "#projGoal" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Donation Goal '});
      valid = false;
    }else{      
      $( "#projGoal" ).removeClass( "red-border" );
    }
    if($scope.projstat=='0'){
      if(!proj.projDesc){
        $( "#projDesc" ).addClass( "red-border" ); 
        $scope.cNTS.push({msg: 'Story'});
        valid = false;
      }else{      
        $( "#projDesc" ).removeClass( "red-border" );
      }
    }
    
    //+========
    if(valid){
      var getTime = document.getElementById('getTime').value;
      var time = getTime;
      var hours = Number(time.match(/^(\d+)/)[1]);
      var minutes = Number(time.match(/:(\d+)/)[1]);
      var AMPM = time.match(/\s(.*)$/)[1];
      if (AMPM == "PM" && hours < 12) hours = hours + 12;
      if (AMPM == "AM" && hours == 12) hours = hours - 12;
      var sHours = hours.toString();
      var sMinutes = minutes.toString();
      if (hours < 10) sHours = "0" + sHours;
      if (minutes < 10) sMinutes = "0" + sMinutes;
      var hh = sHours;
      var mm = sMinutes;

      proj['time'] = hh+':'+mm+':'+'00';

      if(type == 'days'){
        proj['datedue'] = null;
      }else{
        if(proj['datedue'] != proj['datedue2']){
          proj['datedue'] = proj.datedue.toString();
          proj['datechange'] = 'true';
        } 
      }

      console.log(files);
      console.log(proj);

      if(!files){
        if(proj.datedue){
          var daydate = proj.datedue;
        }else{
          var daydate = proj.days;
        }

        var slugs = proj.projTitle;
        var text = slugs.replace(/[^\w ]+/g,'');
        var projSlugs = angular.lowercase(text.replace(/ +/g,'-'));
        proj['projSlugs'] = projSlugs;

        console.log('update lang');
        $scope.loadingg = true;
        $http({
          url: API_URL + "/project/updateproject/" + proj.projID,
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(proj)
        }).success(function (data, status, headers, config) {
          console.log('tapos na');
          $scope.loadingg = false;
          $scope.imageselected = false;
          $scope.isSaving = false;
          $scope.isLoading = false;
          console.log('data saved..');
          loadproject();
          $scope.successget = 'Your project is successfully saved.';
        }).error(function (data, status, headers, config) {
          console.log('tapos na error');
          console.log(data);
        });
      }else{

        var slugs = proj.projTitle;
        var text = slugs.replace(/[^\w ]+/g,'');
        var projSlugs = angular.lowercase(text.replace(/ +/g,'-'));
        proj['projSlugs'] = projSlugs;

        $scope.loadingg = true;    

        console.log('initializing..');

        console.log('checking image size..');
        var file = files[0];
 
        if (file.size > 2000000) {
          console.log('update lang');
          $scope.loadingg = true;
          $http({
            url: API_URL + "/project/updateproject/" + proj.projID,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(proj)
          }).success(function (data, status, headers, config) {
            console.log('tapos na');
            $scope.loadingg = false;
            $scope.imageselected = false;
            $scope.isSaving = false;
            $scope.isLoading = false;
            console.log('data saved..');
            loadproject();
            $scope.successget = 'Your project is successfully saved.';
          }).error(function (data, status, headers, config) {
            console.log('tapos na error');
            console.log(data);
          });
        }else{
          console.log('uploading image..');
          $scope.imageloader=true;
          $scope.thumbchanged = false;
          var promises;
          promises = Upload.upload({

            url: Config.amazonlink,
            method: 'POST',
            transformRequest: function (data, headersGetter) {
              var headers = headersGetter();
              delete headers['Authorization'];
              return data;
            },
            fields : {
              key: 'uploads/projects/' + file.name,
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private',
              policy: Config.policy,
              signature: Config.signature,
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
            },
            file: file
          })
          promises.then(function(data){
            console.log('image uploaded..');
            console.log('saving data..');
            proj['projThumb'] = data.config.file.name;
            $http({
              url: API_URL + "/project/updateproject/" + proj.projID,
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(proj)
            }).success(function (data, status, headers, config) {
              $scope.loadingg = false;
              $scope.imageselected = false;
              $scope.isSaving = false;
              $scope.isLoading = false;
              $scope.files = false;
              $scope.projImg = false;
              console.log('data saved..');
              loadproject();
              $scope.successget = 'Your project is successfully saved.';              
            }).error(function (data, status, headers, config) {
              $scope.status = status;
              console.log(data);
            });
          });
        }
      }
    }else{      
      $scope.resultget.push({type: 'danger', msg: 'The follwing fields are required.'});
      $scope.isLoading = false;
    } 
  }


  $scope.updateSocial = function (data){ 
    $scope.errorget = '';
    $scope.successget = ''; 
    $http({
      url: API_URL + "/project/updatesocial",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(data)
    }).success(function (data, status, headers, config) {          
      $scope.successget = 'Your project social media information has been updated.';
    }).error(function (data, status, headers, config) {
      console.log('tapos na error');
      console.log(data);
    });
  }

  $scope.closeAlert = function(index) {
    $scope.resultget.splice(index, 1);
  };
  $scope.cNTS = [];  
  $scope.submitProj = function (proj,projID,thumbchanged){

    $scope.botMsg = 'Processing please wait...';
    $scope.isLoading = true;
    $scope.resultget = [];
    $scope.successget = ''; 
    var valid = true;
    var changeThumb = false;
    $scope.cNTS = [];  
    $scope.borderError = [];  
    if(thumbchanged){  
      changeThumb = true;   
      valid = false;      
    }
    if(!proj.projTitle){ 
      $( "#projTitle" ).addClass( "red-border" ); 
      $scope.cNTS.push({id: 'projTitle',msg: 'Project Title'}); 
      valid = false;
    }else{
      $( "#projTitle" ).removeClass( "red-border" );
    }
    if(!proj.projShortDesc){
      $( "#projShortDesc" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Short Description'});
      valid = false;
    }else{      
      $( "#projShortDesc" ).removeClass( "red-border" ); 
    }
    if(!proj.projCName){
      $( "#projCName" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Contact Name'});
      valid = false;
    }else{      
      $( "#projCName" ).removeClass( "red-border" ); 
    }
    if(!proj.projCAddress1){
      $( "#projCAddress1" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Address 1'});
      valid = false;
    }else{      
      $( "#projCAddress1" ).removeClass( "red-border" ); 
    }
    if(!proj.projCCity){
      $( "#projCCity" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'City'});
      valid = false;
    }else{      
      $( "#projCCity" ).removeClass( "red-border" ); 
    }
    if(!proj.projCState){
      $( "#projCState" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'State'});
      valid = false;
    }else{      
      $( "#projCState" ).removeClass( "red-border" ); 
    }
    if(!proj.projCZip){
      $( "#projCZip" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Zip/Postal Code'});
      valid = false;
    }else{      
      $( "#projCZip" ).removeClass( "red-border" ); 
    }
    if(!proj.projCCountry){
      $( "#projCCountry" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Country'});
      valid = false;
    }else{      
      $( "#projCCountry" ).removeClass( "red-border" ); 
    }
    if($scope.type == 'days'){
      if(!proj.days){
        $( "#days" ).addClass( "red-border" ); 
        $scope.cNTS.push({msg: 'Days Duration'});
        valid = false;
      }else{      
        $( "#days" ).removeClass( "red-border" ); 
      }
    }else{
      var getTime = document.getElementById('getTime').value;
      if(!proj.datedue){
        $( "#datedue" ).addClass( "red-border" ); 
        $scope.cNTS.push({msg: 'Date Duration'}); 
        valid = false;
      }else{      
        $( "#datedue" ).removeClass( "red-border" );
      }
      if(!getTime){
        $scope.cNTS.push({msg: 'Time Duration'}); 
        valid = false;
      }
    }
    if(!proj.projGoal){
      $( "#projGoal" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Donation Goal '});
      valid = false;
    }else{      
      $( "#projGoal" ).removeClass( "red-border" );
    }
    if(!proj.projDesc){
      $( "#projDesc" ).addClass( "red-border" ); 
      $scope.cNTS.push({msg: 'Story'});
      valid = false;
    }else{      
      $( "#projDesc" ).removeClass( "red-border" );
    }

    if(valid){
      var getTime = document.getElementById('getTime').value;
      var time = getTime;
      var hours = Number(time.match(/^(\d+)/)[1]);
      var minutes = Number(time.match(/:(\d+)/)[1]);
      var AMPM = time.match(/\s(.*)$/)[1];
      if (AMPM == "PM" && hours < 12) hours = hours + 12;
      if (AMPM == "AM" && hours == 12) hours = hours - 12;
      var sHours = hours.toString();
      var sMinutes = minutes.toString();
      if (hours < 10) sHours = "0" + sHours;
      if (minutes < 10) sMinutes = "0" + sMinutes;
      var hh = sHours;
      var mm = sMinutes;

      proj['time'] = hh+':'+mm+':'+'00';

      if(type == 'days'){
        proj['datedue'] = null;
      }else{
        if(proj['datedue'] != proj['datedue2']){
          proj['datedue'] = proj.datedue.toString();
          proj['datechange'] = 'true';
        } 
      }

      console.log(proj);
      $http({
        url: API_URL + "/project/submitproject/" + projID,
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(proj)
      }).success(function (data, status, headers, config) {          
        loadproject();
        $scope.isLoading = false;
        $scope.resultget.push({type: 'success', msg: 'Your project is submitted. Please wait for the result. An email will sent to your email address as soon as the admin done reviewing your project proposal.'});
      }).error(function (data, status, headers, config) {
        console.log('tapos na error');
        console.log(data);
      });
    }else{
      if(changeThumb){   
        $scope.resultget.push({type: 'danger', msg: 'You have changed your project thumbnail, save first before you submit.'});
      } else {
        $scope.resultget.push({type: 'danger', msg: 'Something went wrong. The follwing fields are required.'});
      }
      $scope.isLoading = false;
    }  
    
  }

  $scope.publish = function (proj,projID) {
    $scope.errorget = '';
    $scope.successget = '';
    $http({
      url: API_URL+"/project/publish/" + projID,
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(proj)
    }).success(function (data, status, headers, config) {
      loadproject();      
      $scope.successget = 'Your project successfuly published.';
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }


});