'use strict';

/* Controllers */
app.controller("viewProjCtrl", function ($scope, $http, $anchorScroll, Countries, MDYform, Config, Upload, $filter, $timeout, $compile, $sce, $q, store, jwtHelper, $window) {

	console.log('===Project View===');

	var url = window.location.href;
  var auth = url.match(/\/projects\/(.*)+/);
  console.log(auth);
  var slugs = auth[1];
  console.log(slugs);
	$scope.amazonlink = Config.amazonlink;

  var loadproject = function(){

    $http({
      url: API_URL+"/project/view/" + slugs,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      $scope.data = data.data[0];
      $scope.daystogo = parseInt(data.data[0].countStart);
      if(data.donations[0].total != null){
        $scope.projdon = data.donations[0].total;
      }else{
        $scope.projdon = '0';
      }
      $scope.projdonors = data.donations[0].donors;
      var dur = data.data[0].projDuration.toString().split(" ");
      $scope.duration = dur[1];
      $scope.projDesc = $sce.trustAsHtml(data.data[0].projDesc);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  console.log(slugs);
  if(slugs == 'null'){
    $window.location = '/page404';
  }else{
    loadproject();
  }

  $scope.donate = function (amount,projSlugs) {
    $window.location = '/projects/donation/' + projSlugs + '/' + amount;
  }
	

});