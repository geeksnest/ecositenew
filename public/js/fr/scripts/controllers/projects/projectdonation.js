'use strict';

/* Controllers */
app.controller("donationProjCtrl", function ($scope, $http, $anchorScroll, Countries, MDYform, Config, Upload, $filter, $timeout, $compile, $sce, $q, store, jwtHelper, $window) {

	console.log('===Project Donation===');

	var url = window.location.href;
  var auth = url.match(/\/projects\/donation\/(.*)\/(.*)+/);
  console.log(auth);
  var slugs = auth[1];
  $scope.amount = auth[2];
  console.log(slugs);
  console.log($scope.amount);

  $scope.amazonlink = Config.amazonlink;

  var loadproject = function(){

    $http({
      url: API_URL+"/project/view/" + slugs,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      console.log(data);
      $scope.data = data.data[0];
      $scope.data['projID'] = data.projID;
      $scope.socials = data.socials;
      // if(data.data[0].projStatus == '5'){
      //   $window.location = '/projects/'
      // }
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  loadproject();

  $scope.paypalcustom = '';
  $scope.creditcard123 = false;
  $scope.process = false;
  $scope.isProcessing = false;
  
  $scope.errormessage = '';
  $scope.day = MDYform.day();
  $scope.month = MDYform.month();
  $scope.year = MDYform.year();
  $scope.countries = Countries.list();
  var loadcname = function(){
    $http({
      url: API_URL + "/bnb/cnamelist",
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
      $scope.dataCentername = data;
    }).error(function (data) {
      $scope.status = status;
    });
  }
  loadcname();

  $scope.emailcheck = function (user) {
    $scope.invalidemail = false;
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.invalidemail = true;
    }
    else{                
      $scope.invalidemail = false;
    }

  };



  $scope.submitcredit = function(data, email, target, projID, projTitle, amount){

    data['amount'] = amount;
    data['email'] = email;
    data['projID'] = projID;
    data['projTitle'] = projTitle;
    data['target'] = target;
    console.log(data);    

    $scope.isProcessing = true;
    $scope.responseMSG = "";
    $http.post(API_URL + '/projects/donate/creditcard', data).success(function (response) {
      if(response.hasOwnProperty('success')){
        $scope.isProcessing = false;
        window.location = "/complete/" + amount;
      }else if(response.error == null){        
        $scope.isProcessing = false;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{        
        $scope.isProcessing = false;
        $scope.responseMSG = response.error;
      }
    });
  }

  $scope.submitecheck = function(data, email, target, projID, projTitle, amount){

    data['amount'] = amount;
    data['email'] = email;    
    data['projID'] = projID;
    data['projTitle'] = projTitle;
    data['target'] = target;
    console.log(data);
    
    $scope.isProcessing = true;
    $scope.responseMSG = "";
    $http.post(API_URL + '/projects/donate/echeck', data).success(function (response) {
      if(response.hasOwnProperty('success')){
        $scope.isProcessing = false;
        window.location = "/complete/" + amount;
      }else if(response.error == null){
        $scope.isProcessing = false;
        $scope.reccur_message = 'Our payment gateway server did not respond well please try again. If this problem still persist please refresh the page.';
      }else{        
        $scope.isProcessing = false;
        $scope.responseMSG = response.error;
      }
    });
  }
  $scope.submitpaypal = function(howdidyoulearn, cname, projID){
    var cn='';
    if(cname == undefined){
      cn = '';
    }else{
      cn = cname.replace(/\s+/g, '*');
    }

    var hdyl='';

    if(howdidyoulearn == undefined){
      hdyl = '';
    }else{
      hdyl = howdidyoulearn.replace(/\s+/g, '*');
    }
    var val = 'user@email.com projects ' + projID + hdyl + cn;
    $('#paypalcustom').val(val);
    console.log(val);
    $('#paypalsingle').submit();
  }


});