'use strict';
/* Controllers */

angular.module('app.controllers', ['pascalprecht.translate', 'ngCookies','angularFileUpload'])











.controller('editprofileCtrl', ['$http', '$scope', '$stateParams', '$location', '$anchorScroll', 'Config', 'Upload', '$modal', '$filter', '$timeout', '$sce', '$q', function ($http, $scope, $stateParams, $location, $anchorScroll, Config, Upload, $modal, $filter, $timeout, $sce, $q) {
    //Execute Here

  $scope.usernametaken = false;
  $scope.emailtaken = false;
  $scope.invalidemail = false;
  $scope.passwordInvalid = false;
  $scope.passwordMin = false;
  $scope.oldpasswordMin = false;
  $scope.imageselected = false;
  $scope.editusername = false;
  $scope.editemailaddress = false;
  $scope.validemailbot = true;
  $scope.validusernamebot = true;
  $scope.changepassword = false;
  $scope.validpassbot = true;
  $scope.editUserInfo = false;
  $scope.editInfo = false;
  $scope.changepic = false;



    var loadUserInfo = function(){
      $http({
        url: API_URL + "/users/viewuser/" + $stateParams.userid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.user = data;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    loadUserInfo();


  $scope.prepare = function(file) {
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('File is too big!');
        $scope.alerts = [{
          type: 'danger',
          msg: 'File ' + file.name + ' is too big'
        }];
        $scope.file = '';
        $scope.changepic = false;
      } else {
        console.log("below maximum");
        $scope.file = file;
        $scope.closeAlert();
        $scope.imageselected = true;
        $scope.changepic = true;
      }
    }
  }

  $scope.chkUsername = function(user) {
    console.log(user);
    $http({
      url: API_URL + "/users/chkUsername",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.usernametaken = true;
        $scope.validusernamebot = false;
      } else{
        $scope.usernametaken = false;
        $scope.validusernamebot = true;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };

  $scope.chkEmail = function(user) {
    console.log(user);
    var validemail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(user.email);
    if(validemail == false){
      $scope.validemailbot = false;
      $scope.invalidemail = true;
      console.log('invalid');
    }else{
      $scope.invalidemail = false;
      $scope.validemailbot = true;
      console.log('valid');
    }
    if(user.email == undefined || user.email == null){
      $scope.invalidemail = false;
      console.log('invalid wala laman');
    }
    $http({
      url: API_URL + "/users/chkEmail",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (response) {
      if (response.hasOwnProperty('error')) {
        $scope.emailtaken = true;
        $scope.validemailbot = false;
      } else{
        $scope.emailtaken = false;
        $scope.validemailbot = true;
      }
    }).error(function (response) {
      $scope.status = status;
    });
  };


  $scope.cancelEdit = function() {
    $scope.editusername = false;
    $scope.editemailaddress = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
    $scope.changepic = false;
    $scope.imageselected = false;
    loadUserInfo();
  };

  $scope.editUname = function() {
    $scope.editusername = true;
    $scope.editemailaddress = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveUname = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    $http({
      url: API_URL + "/users/updateusername/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Username already in use.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Username successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editusername = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.editEmail = function() {
    $scope.editemailaddress = true;
    $scope.editusername = false;
    $scope.changepassword = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveEmail = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    $http({
      url: API_URL + "/users/updateuseremail/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Email Address already in use.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'Email Address successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editemailaddress = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.changepass = function() {
    $scope.changepassword = true;
    $scope.editemailaddress = false;
    $scope.editusername = false;
    $scope.changeRestrictions = false;
    $scope.editUserInfo = false;
  };
  $scope.saveChangePass = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    if(user.oldpassword != undefined || user.oldpassword != null){

      $scope.imageloader = true;
      $http({
        url: API_URL + "/users/userchangepassword/",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(user)
      }).success(function (data, status, headers, config) {
        if (data.hasOwnProperty('error')) {
          $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again.'});
        }else if (data.hasOwnProperty('wrongpass')) {
          $scope.alerts.push({type: 'warning', msg: 'Invalid old password. Please try again.'});
        }else{
          $scope.alerts.push({type: 'success', msg: 'Password successfully updated.'});
        }
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $scope.imageloader = false;
        $scope.changepassword = false;
        loadUserInfo();
      }).error(function (data, status, headers, config) {
        $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
      });
    }
    else{
      $scope.alerts.push({type: 'warning', msg: 'Please fill up the fields.'});
    }

  };

  $scope.minPass = function(user) {

    if(user.password != undefined || user.password != null ){
      if(user.password.length < 6){
        $scope.passwordMin = true;
        $scope.validpassbot = false;
      }else{
        $scope.passwordMin = false;
        // $scope.validpassbot = true;
      }
    }

  };

  $scope.oldminPass = function(user) {

    if(user.oldpassword != undefined || user.oldpassword != null ){
      if(user.oldpassword.length < 6){
        $scope.oldpasswordMin = true;
        $scope.validpassbot = false;
      }else{
        $scope.oldpasswordMin = false;
        // $scope.validpassbot = true;
      }
    }

  };

  $scope.chkPass = function(user) {
    console.log(user.confirm_password.length);
    console.log(user.password.length);
    if(user.password.length === user.confirm_password.length){
      if(user.password != user.confirm_password){
        $scope.passwordInvalid = true;
        $scope.validpassbot = false;
      }else{
        $scope.passwordInvalid = false;
        $scope.validpassbot = true;
      }
    } else if(user.password.length < user.confirm_password.length){
      $scope.passwordInvalid = true;
      $scope.validpassbot = false;
    }

  };

  $scope.chkPass2 = function() {
    $scope.user.confirm_password = [];
    $scope.validpassbot = false;
  };

  $scope.editRestrictions = function() {
    $scope.changeRestrictions = true;
    $scope.changepassword = false;
    $scope.editemailaddress = false;
    $scope.editusername = false;
    $scope.editUserInfo = false;
  };
  $scope.saveRestrictions = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;
    checksub(user);
    $http({
      url: API_URL + "/users/editRestrictions/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.alerts.push({type: 'success', msg: 'Restrictions successfully updated.'});
      $scope.imageloader = false;
      $scope.changeRestrictions = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };


  $scope.editInfo = function() {
    $scope.editUserInfo = true;
    $scope.changeRestrictions = false;
    $scope.changepassword = false;
    $scope.editemailaddress = false;
    $scope.editusername = false;
  };
  $scope.saveInfo = function(user) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.imageloader = true;


    if(user.birthday == user.birthday2){
      user['birthday2'] = 'dontchange';
    }else{
      user['birthday2'] = 'change';
    }

    $http({
      url: API_URL + "/users/updateuserInfo/",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
    }).success(function (data, status, headers, config) {
      if (data.hasOwnProperty('error')) {
        $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again or refresh the page.'});
      }else{
        $scope.alerts.push({type: 'success', msg: 'User information successfully updated.'});
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      $scope.imageloader = false;
      $scope.editUserInfo = false;
      loadUserInfo();
    }).error(function (data, status, headers, config) {
      $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
    });
  };

  $scope.updatePhoto = function (files, user){
    // console.log(files);
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $anchorScroll();
    console.log('initializing..');

    console.log('checking image size..');
    console.log(files);
    var file = files[0];
    if (file && file.length) {
      if (file.size >= 2000000) {
        console.log('image size too large');
        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        cope.imagecontent=true;
      }
    }
    else{
      console.log('uploading image..');
      $scope.imageloader=true;

      var promises;
      promises = Upload.upload({

        url: Config.amazonlink,
        method: 'POST',
        transformRequest: function (data, headersGetter) {
          var headers = headersGetter();
          delete headers['Authorization'];
          return data;
        },
        fields : {
          key: 'uploads/userimages/' + file.name,
          AWSAccessKeyId: Config.AWSAccessKeyId,
          acl: 'private',
          policy: Config.policy,
          signature: Config.signature,
          "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
        },
        file: file
      })
      promises.then(function(data){
        console.log('saving data..');
        user['image'] = data.config.file.name;
        $http({
          url: API_URL + "/users/updateProfilePhoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(user)
        }).success(function (data, status, headers, config) {
          if (data.hasOwnProperty('error')) {
            $scope.alerts.push({type: 'warning', msg: 'Something went wrong please try again or refresh the page.'});
          }else{
            $scope.alerts.push({type: 'success', msg: 'Profile photo successfully updated.'});
          }
          document.body.scrollTop = document.documentElement.scrollTop = 0;
          $scope.imageloader = false;
          $scope.changepic = false;
          loadUserInfo();
        }).error(function (data, status, headers, config) {
          $scope.status = status;
          console.log(data);
        });
        $scope.imageloader=false;
      });
    }


  }


  $scope.chkimage = function(files) {
    $scope.imageselected = true;
  };



  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.datepicker = {'opened': true};
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];



}])





// jimmytotz

.controller('BNBregistrationCtrl', ['$http', '$scope', '$parse', '$location', '$anchorScroll', 'Countries', function ($http, $scope, $parse, $location, $anchorScroll,Countries) {
  $scope.countries = Countries.list();
    // Paymode view validation
    $scope.tycreditcashcheck = false;
    $scope.tycheck = false;

    $scope.ecoprogram = function(){
      $scope.tycreditcashcheck = false;
      $scope.tycheck = false;
    }
    $scope.tpaynone = function(){
      $scope.tycreditcashcheck = false;
      $scope.tycheck = false;
    }
    $scope.tpaycreditcash = function(){
      $scope.tycreditcashcheck = true;
      $scope.tycheck = false;
    }
    $scope.tpaycheck = function(){
      $scope.tycreditcashcheck = true;
      $scope.tycheck = true;
    }

    // load centername to select item from centername table
    var loadCentername = function(){
      $http({
        url: API_URL +"/bnb/cnamelist",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
            // // console.log(data);
            $scope.dataCentername = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadCentername();

    // insert data to members table and donation table if donation is made then update donation table
    $scope.isSaving = false;
    var runquery=0;
    var runcheckmail=0;
    var checkrefer=0;
    $scope.addToMember = function(bnb){
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };

        $scope.isSaving = true;
        if(bnb.paymode == undefined){ bnb.paymode = 'None'; }
        // // console.log(bnb.location);
        // check if email address is valid
        var mailtest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(bnb.email);
        if (mailtest == false) {
          $scope.isSaving = false;
          $scope.alerts.push({type: 'danger', msg: 'Invalid email address.'});
          runcheckmail = 0;
        }else{
          runcheckmail = 1;
        }
            // check if refer is not null
            var refertest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(bnb.refer);
            if(bnb.refer == undefined){
              bnb.refer=bnb.refer;
              checkrefer=1;
            }else{
              if(refertest==false){
                $scope.isSaving = false;
                    // // console.log(bnb.refer);
                    $scope.alerts.push({type: 'danger', msg: 'Invalid friend email address.'});
                    checkrefer = 0;
                  }else{
                    checkrefer=1;
                  }
                }
                // check paymode
                if(bnb.paymode == 'None'){
                  runquery = 1;
                }else if(bnb.paymode == 'ECO Program'){
                  runquery = 1;
                }else if(bnb.paymode == 'Cash' || bnb.paymode == 'Credit'){
                  if(bnb.amt == undefined){
                    $scope.isSaving = false;
                    $scope.alerts.push({type: 'danger', msg: 'Please fill required fields.'});
                    runquery=0;
                  }else{
                    runquery = 2;
                  }
                }
                if(bnb.paymode == 'Check'){
                  if(bnb.amt == undefined || bnb.rn == undefined || bnb.an == undefined || bnb.cn == undefined){
                    $scope.isSaving = false;
                    $scope.alerts.push({type: 'danger', msg: 'Please fill required fields.'});
                    runquery=0;
                  }else{
                    runquery = 3;
                  }
                }

                if(bnb.fname == undefined){ bnb.fname = ''; }
                if(bnb.lname == undefined){ bnb.lname = ''; }
                if(bnb.bday == undefined){ bnb.bday = ''; }
                if(bnb.bmonth == undefined){ bnb.bmonth = ''; }
                if(bnb.byear == undefined){ bnb.byear = ''; }
                if(bnb.zip == undefined){ bnb.zip = ''; }
                if(bnb.centersname == undefined){ bnb.centersname = ''; }
                if(bnb.gender == undefined){ bnb.gender = ''; }
                if(bnb.howdidyoulearn == undefined){ bnb.howdidyoulearn = ''; }
                if(bnb.refer == undefined){ bnb.refer = undefined; }

                if (runquery != 0 && runcheckmail == 1 && checkrefer == 1) {
                  $http({
                    url: API_URL + "/bnb/insToMember",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(bnb)
                  }).success(function (data, status, headers, config) {
                    if (data.hasOwnProperty('emailtaken')) {
                      $scope.isSaving = false;
                      $scope.alerts.push({type: 'danger', msg: 'Email address already exists.'});
                      $scope.formBNB.$setPristine(false);
                    } else {
                      $scope.isSaving = false;
                      $scope.alerts.push({type: 'success', msg: 'New member has been registered.'});
                      $scope.reset();
                      $scope.formBNB.$setPristine(true);
                    }
                  }).error(function (data, status, headers, config) {
                    $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
                  });
                }
              }

              $scope.reset = function () {
                $scope.bnb = {};
                $scope.formBNB.$setPristine(true);
              };
            }])









.controller('CreateNewsLetterCtrl', ['$scope', '$http', '$modal', function($scope, $http, $modal) {
  var oriNewsletter = angular.copy($scope.newsletter);

  $scope.isSaving = false;
  $scope.saveNewsLetter = function(newsletter)
  {
            //Execute Here
            $scope.alerts = [];
            $scope.closeAlert = function(index) {
              $scope.alerts.splice(index, 1);
            };
            $scope.isSaving = true;
            // console.log(newsletter);
            $http({
              url: API_URL + "/newsletter/create",
              method: "POST",
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(newsletter)
            }).success(function(data, status, headers, config) {
              $scope.isSaving = false;
              $scope.alerts.push({
                type: 'success',
                msg: 'News Letter successfully saved!'
              });
              $scope.newsletter = angular.copy(oriNewsletter);
              $scope.formNewLetter.$setPristine();

            }).error(function(data, status, headers, config) {
              $scope.alerts.push({
                type: 'danger',
                msg: 'Something went wrong please check your fields'
              });
            });
          }

          $scope.previewletter = function(letterbody) {

            var modalInstance = $modal.open({
              templateUrl: 'previewletter.html',
              controller: previewletterCTRL,
              resolve: {
                letterbody: function() {
                  return letterbody
                }
              }
            });
          }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var previewletterCTRL = function($scope, $modalInstance, $sce, letterbody) {
           // console.log(letterbody);
           $scope.htmlbody = $sce.trustAsHtml(letterbody);
           $scope.close = function() {
            $modalInstance.dismiss('cancel');
          };
        };

      }])

.controller('manageNewsLetterCtrl', ['$scope', '$http', '$modal', '$state', function($scope, $http, $modal, $state) {

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var paginate = function(off, keyword) {
    $http({
      url: API_URL + "/newsletter/manage/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }).success(function(data, status, headers, config) {
      $scope.data = data;
            // console.log(data);
          }).error(function(data, status, headers, config) {
            $scope.status = status;
          });
        }
        $scope.search = function(keyword) {
          var off = 0;
          paginate(off, keyword);
        }
        $scope.paging = function(off, keyword) {
          paginate(off, keyword);
        }
        paginate(off, keyword);

        var deletenewsletterInstanceCTRL = function($scope, $modalInstance, newsletterid, $state) {
          $scope.ok = function() {
            var newsletter = {
              'newsletter': newsletterid
            };
            // console.log(newsletterid);
            $http({
              url: API_URL + "/newsletter/newsletterdelete/" + newsletterid,
              method: "POST",
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(newsletter)
            }).success(function(data, status, headers, config) {
              $modalInstance.close();
              window.location.reload();
              $scope.success = true;
            }).error(function(data, status, headers, config) {
              $scope.status = status;
            });
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        }

        $scope.deletenewsletter = function(newsletterid) {

          var modalInstance = $modal.open({
            templateUrl: 'deletenewsletter.html',
            controller: deletenewsletterInstanceCTRL,
            resolve: {
              newsletterid: function() {
                return newsletterid
              }
            }
          });
        }

        var updatenewsletterInstanceCTRL = function($scope, $modalInstance, newsletterid, $state) {
          $scope.newsletterid = newsletterid;
        // console.log(newsletterid);
        $scope.ok = function(newsletterid) {
          $scope.pageid = newsletterid;
          $state.go('editnewsletter', {
            newsletterid: newsletterid
          });
                //$state.go('editpage()')
                $modalInstance.dismiss('cancel');
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            }

            $scope.updatenewsletter = function(newsletterid) {
              $scope.newsletterid
              var modalInstance = $modal.open({
                templateUrl: 'updatenewsletter.html',
                controller: updatenewsletterInstanceCTRL,
                resolve: {
                  newsletterid: function() {
                    return newsletterid
                  }
                }
              });
            }

            var sendnewsletterInstanceCTRL = function($scope, $modalInstance, newsletterid, $state) {
              $scope.newsletterid = newsletterid;
            // console.log(newsletterid);
            $scope.ok = function(newsletterid) {
              $scope.pageid = newsletterid;
              $state.go('sendnewsletter', {
                newsletterid: newsletterid
              });
                //$state.go('editpage()')
                $modalInstance.dismiss('cancel');
              };
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            }

            $scope.sendnewsletter = function(newsletterid) {
              $scope.newsletterid
              var modalInstance = $modal.open({
                templateUrl: 'sendnewsletter.html',
                controller: sendnewsletterInstanceCTRL,
                resolve: {
                  newsletterid: function() {
                    return newsletterid
                  }
                }
              });
            }
          }])

.controller('editNewsletterCtrl', ['$http', '$scope', '$stateParams', '$modal', function($http, $scope, $stateParams, $modal) {


  $http({
    url: API_URL + "/newsletter/editnewsletter/" + $stateParams.newsletterid,
    method: "GET",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.newsletter = data;
        // console.log(data);
      }).error(function(data, status, headers, config) {
        $scope.status = status;
      });

      $scope.previewletter = function(letterbody) {

        var modalInstance = $modal.open({
          templateUrl: 'previewletter.html',
          controller: previewletterCTRL,
          resolve: {
            letterbody: function() {
              return letterbody
            }
          }
        });
      }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var previewletterCTRL = function($scope, $modalInstance, $sce, letterbody) {
           // console.log(letterbody);
           $scope.htmlbody = $sce.trustAsHtml(letterbody);
           $scope.close = function() {
            $modalInstance.dismiss('cancel');
          };
        };

        $scope.alerts = [];
        $scope.closeAlert = function(index) {
          $scope.alerts.splice(index, 1);
        };

        $scope.updatenewsletter = function(newsletter) {
          $http({
            url: API_URL + "/newsletter/updatenewsletter",
            method: "POST",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(newsletter)
          }).success(function(data, status, headers, config) {
            $scope.alerts.push({
              type: 'success',
              msg: 'News successfully saved!'
            });

          }).error(function(data, status, headers, config) {
            $scope.alerts.push({
              type: 'danger',
              msg: 'Something went wrong please check your fields'
            });
          });
        };
      }])


.controller('sendNewsletterCtrl', ['$http', '$scope', '$stateParams', '$modal', '$sce', '$interval', function($http, $scope, $stateParams, $modal, $sce, $interval) {
  $scope.newslettercontent = true;
  $scope.emailonsent = false;
  $scope.refresh1 = false;
  $scope.sendbutton = true;
  $http({
    url: API_URL + "/newsletter/editnewsletter/" + $stateParams.newsletterid,
    method: "GET",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.newsletter = data;
    $scope.htmlbody = $sce.trustAsHtml(data.body);
    console.log($scope.newsletter);
  }).error(function(data, status, headers, config) {
    $scope.status = status;
  });


  var newlistemail = Array();
  $http({
    url: API_URL+"/newsletter/subscriberslist/" + $stateParams.newsletterid,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {

    $scope.data = data;
    var newdata = data.data

    var newlist = Array();
    for(var i in newdata){
      newlistemail.push(newdata[i].NMSemail)
    }

    $scope.newsletter.checked = angular.copy(newlistemail);
  }).error(function (data, status, headers, config) {
    $scope.status = status;
  });



  $http({
    url: API_URL+"/newsletter/memberlist/" + $stateParams.newsletterid,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {
    $scope.data1 = data;
    var newdata = data.memdata
    var newlist = Array();
    for(var i in newdata){
      newlistemail.push(newdata[i].email)


    }

    $scope.newsletter.checked = angular.copy(newlistemail);
  }).error(function (data, status, headers, config) {
    $scope.status = status;
  });




  var onoff = 'off';


  $scope.checkedallLetter = function()
  {


    if (onoff == 'off')
    {
     $scope.newsletter.checked = angular.copy(newlistemail);
     onoff = 'on';
           // console.log(onoff);
           $scope.isSaving = false;
         }
         else
         {
          $scope.newsletter.checked = [];
          onoff = 'off'
        // console.log(onoff);
      }
    }


    $scope.refresh = function()
    {
      window.location.reload();
    }

    $scope.sendNewsLetter = function(newsletter, interval)
    {

      $scope.newslettercontent = false;
      $scope.alerts = [];
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      $scope.alerts1 = [];
      $scope.closeAlert1 = function(index) {
        $scope.alerts1.splice(index, 1);
      };
      $scope.isSaving = true;
      var len = $scope.newsletter.checked.length;
      $scope.total = len;
      var itemdone = 0;

    // console.log($scope.newsletter.checked);
      // // console.log(newsletter);
      var xnum = 0;
      for(var i in $scope.newsletter.checked)
      {

       $scope.emailonsent = true;
            // // console.log($scope.newsletter.checked[i]);


            var emails = $scope.newsletter.checked[i];


            $http({
              url: API_URL + "/newsletter/sendnewsletter/" + emails,
              method: "POST",
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(newsletter)
            }).success(function(data, status, headers, config) {

              $scope.emailsending = $scope.newsletter.checked[xnum + 1];
              xnum++;
                // console.log($scope.emailsending);

                itemdone = itemdone + 1;
                $scope.itemdoneview1 = itemdone;
                $scope.itemdoneview = Math.round(itemdone / len * 100);
                if (itemdone == len)
                {

                  $scope.alerts.push({
                    type: 'success',
                    msg: 'News Letter Sent!'
                  });
                  $scope.isSaving = false;
                  $scope.emailonsent = false;
                  $scope.refresh1 = true;
                  $scope.sendbutton = false;

                }

              }).error(function(data, status, headers, config) {
                $scope.alerts.push({
                  type: 'danger',
                  msg: 'Something went wrong please check your Internet'
                });
                $scope.isSaving = false;
                window.location.reload();
              });
            }
          }
        }])



//// ENDSLIDEUPLOAD CONTROLLER///////////////////////////////////////////////////////////////////////////////////////////
////
.controller('menucreatorCtrl', ['$scope','$http', '$modal', '$compile',function($scope, $http, $modal, $compile){
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////APPEND MENU
   $scope.inputCounter = 0;
   $scope.sub_option=true;
   $scope.showsub= function(menu){
        var menu_sub = $scope.menu;///SHOW OTHER Main Menu Sub ID
        $scope.sub_option=false;///SHOW OTHER SUB MENU'S
      }
      $scope.addNewChoice = function(choice) {
        /////////////////////////////////////////
        var label='<label>Sub-Menu ' + $scope.inputCounter + '</label>';
        var selctemnt='type="text" ng-model="menu.id[' + $scope.inputCounter + ']" name="{[{subs.id}]}"  class="form-control input-sm ng-pristine" ng-change="addNewChoice(menu.name)" style="width:50%" ';
        var dataOpt='<option value="{[{ data.menu_id}]}" ng-repeat="data in menulist" >{[{ data.menu_name }]}</option>';
        var input = angular.element('<div style="margin-top:10px;">'+label+'<select '+selctemnt+'>'+dataOpt+'</select></div> ');
        var compile = $compile(input)($scope);
        $('.add-input').append(input);
        $scope.inputCounter++;




     // Define $scope.telephone as an array
     $scope.menu.id = [];

    // This is just so you can see the array values changing and working! Check your // console as you're typing in the inputs :)
$scope.$watch('menu.id', function (value) {
    // console.log(value);
  }, true);


var id=$scope.menu;
// console.log(id);
};
   ////END APPEND MENU
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


   var loadpagemenu=function(){
     $http({
      url: API_URL +"/utility/getpage",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}

    }).success(function (pageinfo) {
        // console.log(pageinfo);
        $scope.pageinfo = pageinfo;
      }).error(function (pageinfo) {
        $scope.status = status;
      });
    }
    loadpagemenu();


    var loadmainMenu = function(){
     $http({
      url: API_URL + "/utility/listmenu",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (menulist) {
            // // console.log(menulist);
            $scope.menulist = menulist;
          })


  }
  loadmainMenu();

  var loadsubMenu = function(){
   $http({
    url: API_URL + "/utility/submenu",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (submenu) {
 // console.log(submenu);
 $scope.submenu = submenu;
})

}
loadsubMenu();


var oriUser = angular.copy($scope.menu);
$scope.save = function (menu) {
  $http({
    url: API_URL + "/utility/savemenu",
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    data: $.param(menu)
  }).success(function (data) {
        // console.log(data)
        loadmainMenu();
        loadsubMenu();
        loadpagemenu();

            ///Clear Form
            $scope.menu = angular.copy(oriUser);
            $scope.form.$setPristine();
          })
}
$scope.showmain = function() {
   // console.log("This is a test");
 }

}])
.controller('testimonialsCtrl', ['$scope','$http', '$modal',function($scope, $http, $modal){

 $scope.data = {};
 var num = 10;
 var off = 1;
 var keyword = null;
 var paginate = function (off, keyword) {
  $http({
    url: API_URL +"/utility/testimonials/" + num + '/' + off + '/' + keyword,
    method: "GET",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  }).success(function (data, status, headers, config) {
        // console.log(data);
        $scope.data = data;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }
    $scope.search = function (keyword) {
      var off = 0;
      paginate(off, keyword);
    }
    $scope.numpages = function (off, keyword) {

      paginate(off, keyword);
    }
    paginate(off, keyword);

/////////////////////////////////////////////////////////////////////////////////////////////////
            ///MODAL
            $scope.deletepage = function (dlt){
              var modalInstance = $modal.open({
                templateUrl: 'deletetestimonial.html',
                controller: deleteCTRL,
                resolve: {
                  dlt: function () {
                    return dlt;
                  }
                }
              });
            }
            var deleteCTRL = function ($scope, $modalInstance, dlt) {
              var id=dlt;
                // console.log(id);
                //// console.log(albumName);
                $scope.dlt = dlt;
                $scope.ok = function (dlt) {
                  $http({
                    url: API_URL+"/utility/dlt/"+id,
                    method: "GET",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                           // data: $.param(albumName)
                         }).success(function (data, status, headers, config) {
                        // console.log(data);
                        $scope.data = data;
                      })
                         window.location.reload();
                       };
                       $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                      };
                    };

 /////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////
       /////////////////////////////////////////////////////////////////////////////////////////////////
        ///MODAL
        $scope.updatepage = function (update){
          var modalInstance = $modal.open({
            templateUrl: 'updatetestimonial.html',
            controller: updateCTRL,
            resolve: {
              update: function () {
                return update;
              }
            }
          });
        }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var updateCTRL = function ($scope, $modalInstance, update, $state) {
             // console.log(update);
             $scope.update = update;
             $scope.ok = function (update) {
              $scope.update = update;
              $state.go('edit', {id:update});
              $modalInstance.dismiss('cancel');

            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

          };

        /////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////
       /////////////////////////////////////////////////////////////////////////////////////////////////
        ///MODAL
        $scope.publish = function (id, status){
            // console.log(id,status);
            $http({
              url: API_URL+"/utility/status/"+id+"/"+status,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                   //$scope.info = info;
                   // console.log($scope.data);
                 }).error(function (data) {
                       // $scope.status = status;
                     });
                 window.location.reload();
               }
             }])
.controller('edittestimonialCtrl', ['$http', '$scope', '$stateParams', '$modal',  function ($http, $scope, $stateParams, $modal){

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////LOAD ALBUM INFO
        var getinfo = function(id){
         $http({
          url: API_URL +"/utility/view/"+id,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (info) {
         $scope.info = info;
         // console.log($scope.info);
       }).error(function (info) {
                   // $scope.status = status;
                 });
     }
     getinfo($stateParams.id);
       ///////////////////////////////////////////////////////////////////

       $scope.update = function (info){

        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };

        var id=info.id;
        var message=info.message;
            // // console.log(id+"-"+message);
            $('.buttonPanel'+info.id).hide();
            $http({
              url: API_URL + "/utility/updatetestimony/"+id+"/"+message,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(info)
            }).success(function (data, status, headers, config) {
              $('.buttonPanel'+info.id).show();
              $scope.saveboxmessage = info.id;
              $scope.alerts.push({type: 'success', msg: 'Testimonial Updated.'});
            }).error(function (data, status, headers, config) {
              $scope.status = status;
            });

          };


        }])

//CREATE ALBUM CONTROLLER
.controller('Manage_albumCtrl', ['$scope','$http', '$modal',function($scope, $http, $modal){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var paginate = function (off, keyword) {
    $http({
      url: API_URL +"/utility/manage/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  paginate(off, keyword);

  $scope.search = function (keyword) {
    var off = 0;
    paginate(off, keyword);
  }
  $scope.numpages = function (off, keyword) {

    paginate(off, keyword);
  }

    
  $scope.deletealbum = function (album_id){
    var modalInstance = $modal.open({
      templateUrl: 'deletealbum.html',
      controller: albumedeleteCTRL,
      resolve: {
        album_id: function () {
          return album_id;
        }
      }
    });
  }

  var albumedeleteCTRL = function ($scope, $modalInstance, album_id) {
    $scope.album_id = album_id;
    $scope.ok = function (album_id) {
      $http({
        url: API_URL+"/slider/deletealbum/"+album_id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data;
        paginate(off, keyword);
        $modalInstance.dismiss('cancel');
      })
    };
    $scope.cancel = function () {
    };
  };

    
  $scope.editalbum = function (albumid){
    var modalInstance = $modal.open({
      templateUrl: 'updateAlbum.html',
      controller: updateCTRL,
      resolve: {
        albumid: function () {
          return albumid;
        }
      }
    });
  }
            
  var updateCTRL = function ($scope, $modalInstance, albumid, $state) {
    $scope.albumid = albumid;
    $scope.ok = function (albumid) {
      $scope.albumid = albumid;
      $state.go('edit_album', {id:albumid});
      $modalInstance.dismiss('cancel');

    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

}])

.controller('Edit_albumCtrl', ['$http', '$scope', '$stateParams', '$modal',  function ($http, $scope, $stateParams, $modal) {

  $scope.mainset = function (folderid){
        // console.log(folderid);
        $http({
          url: API_URL+"/utility/mainset/"+folderid,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            // $scope.info = info;
            // console.log(data);
          }).error(function (data) {
            // $scope.status = status;
          });
        // window.location.reload();
      }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////LOAD ALBUM INFO
            var getfolderinfo = function(id){
             $http({
              url: API_URL +"/utility/editalbum/"+id,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (album_info) {
             $scope.album_info = album_info;
               // console.log($scope.album_info);
             }).error(function (album_info) {
                   // $scope.status = status;
                 });
           }
           getfolderinfo($stateParams.id);
            ///////////////////////////////////////////////////////////////////////////////
            //////LIST IMAGES
            var loadImage = function(id){
             $http({
              url: API_URL +"/utility/listsliderimages/"+id,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                // console.log(data);
                $scope.data = data;
              }).error(function (data) {
                $scope.status = status;
              });
            }
            loadImage($stateParams.id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///FILE UPLOAD
            var newArray = new Array();
            $('#digitalAssets').fileupload({
              url: API_URL + '/server/php/index.php',
              dataType: 'json',
              disableImageResize: /Android(?!.*Chrome)|Opera/
              .test(window.navigator.userAgent),
              previewMaxWidth: 100,
              previewMaxHeight: 100,
              previewCrop: true,
              limitMultiFileUploads: 4,

              done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                  $.get( API_URL + '/utility/ajaxfileuploader/'+ file.name + '/'+$('#albumtitle').val() +'/'+$stateParams.id).done(function( data ) {
                        //$('.digital-assets-gallery').prepend(data);
                        //// console.log(data);
                        loadImage($stateParams.id);
                      });
                });
              },
              progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                  'width',
                  progress + '%'
                  );
              }
            })
             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Update Photo
        $scope.page = {};
        $scope.saveboxmessage = '';
        $scope.imgInfo = function (info){
            // console.log(info);
            $('.buttonPanel'+info.id).hide();
            $http({
              url: API_URL + "/utility/imginfoupdate",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(info)
            }).success(function (data, status, headers, config) {
              $('.buttonPanel'+info.id).show();
              $scope.saveboxmessage = info.id;

            }).error(function (data, status, headers, config) {
              $scope.status = status;
            });

          };
         ///END Update Photo
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////DELETE ALBUM PHOTO
            $scope.dltPhoto = function (dlt){
                // console.log(dlt);
                $('.buttonPanel'+dlt.id).hide();
                $http({
                  url: API_URL + "/news/dltnewsphoto",
                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(dlt)
                }).success(function (data, status, headers, config) {
                    //$('.buttonPanel'+dlt.id).show();
                    //$scope.saveboxmessage = dlt.id;
                  }).error(function (data, status, headers, config) {
                    $scope.status = status;
                  });
                };
            //////////
            //////MODAL
            $scope.huhuClick = function (imageid){
              var modalInstance = $modal.open({
                templateUrl: 'editMemberModal.html',
                controller: huhuCTRL,
                resolve: {
                  imageid: function () {
                    return imageid;
                  }
                }
              });
            }
            var huhuCTRL = function ($scope, $modalInstance, imageid) {
                // console.log(imageid);
                $scope.imageid = imageid;
                $scope.ok = function (imageid) {
                    // console.log(imageid);
                    $http({
                      url: API_URL + "/utility/dltphoto",
                      method: "POST",
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                      data: $.param(imageid)
                    })
                    window.location.reload();
                  };
                  $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                  };
                };
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////



          }])
.controller('Create_albumCtrl', ['$scope','$http', '$modal',function($scope, $http, $modal){


    $scope.addphoto = false;
    $scope.file = [];
    $scope.delete = function(index) {
      $scope.file.splice(index, 1);
      console.log('dasdasd');
    }
    $scope.uploadAlbum = function(file) {
      console.log('ito upload');
      console.log(file);
    }
    $scope.prepare = function(files,file) {
      $scope.alerts = [];
      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };
      if (files && files.length) {
        for (var i = 0; i < files.length; i++)
        { 
          console.log(files[i].size);

          if (files[i].size >= 2000000) {
            console.log('bigImg'+files[i]);
          }else{            
            $scope.file.push(files[i]);
          } 
        }
        $scope.addphoto = true;        
        $scope.closeAlert();
        $scope.imageselected = true;
      }
    }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         /////////GENERATEID
         var folderid=function(){
          $http({
            url: API_URL + "/utility/generateid",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (genId) {
                // console.log(genId);
                $scope.genId = genId;
              }).error(function (genId) {
                   // error Status
                 });
            }
            folderid();
        ///////// END GENERATEID
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////SAVE FOLDER
        var foldersave= function(albumName,albumId){
         $http({
          url: API_URL +"/utility/createalbum/"+albumName+'/'+albumId,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (albuminfo) {
            // console.log(albuminfo);
                        //$scope.data = data;
                      })
      }

        ///END AUTOLOAD FUNCTION
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////MODALS
        $scope.huhuClick = function (imageid){
          var modalInstance = $modal.open({
            templateUrl: 'editMemberModal.html',
            controller: huhuCTRL,
            resolve: {
              imageid: function () {
                return imageid;
              }
            }
          });
        }
        var huhuCTRL = function ($scope, $modalInstance, imageid) {
            // console.log(imageid);
            $scope.imageid = imageid;
            $scope.ok = function (imageid) {
                // console.log(imageid);
                $http({
                  url: API_URL + "/utility/dltphoto",
                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(imageid)
                })
                window.location.reload();
              };
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            };
         ////MODAL END
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Update Photo
        $scope.page = {};
        $scope.saveboxmessage = '';
        $scope.imgInfo = function (info){
            // console.log(info);
            $('.buttonPanel'+info.id).hide();
            $http({
              url: API_URL + "/utility/imginfoupdate",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(info)
            }).success(function (data, status, headers, config) {
              $('.buttonPanel'+info.id).show();
              $scope.saveboxmessage = info.id;

            }).error(function (data, status, headers, config) {
              $scope.status = status;
            });

          };
         ///END Update Photo
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///AUTOLOAD FUNCTION
        var loadImage = function(id){
         $http({
          url: API_URL +"/utility/listsliderimages/"+id,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
            //loadImage();
        ///END AUTOLOAD FUNCTION
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///FILE UPLOAD
        $scope.setmain = false;
        var newArray = new Array();
        $('#digitalAssets').fileupload({
          url: API_URL + '/server/php/index.php',
          dataType: 'json',
          disableImageResize: /Android(?!.*Chrome)|Opera/
          .test(window.navigator.userAgent),
          previewMaxWidth: 100,
          previewMaxHeight: 100,
          previewCrop: true,
          limitMultiFileUploads: 4,
          send: function (e, data) {
            var albumName= ($('#albumtitle').val());
            var albumId= ($('#albumid').val());
                    //////////Save folder
                    foldersave(albumName,albumId);
                    ////save folder
                    return;
                  },
                  done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                      $.get( API_URL + '/utility/ajaxfileuploader/'+ file.name + '/'+$('#albumtitle').val() +'/'+$('#albumid').val()+'/').done(function( data ) {
                        //$('.digital-assets-gallery').prepend(data);
                        //// console.log(data);
                        loadImage($('#albumid').val());
                        $scope.setmain = true;
                      });
                    });
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                })
        ///END FUNCTION
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      }])
//// END CREATE ALBUM CONTROLLER///////////////////////////////////////////////////////////////////////////////////////////


////SETTINGS CONTROLLER
.controller('settingsCtrl', ['$scope','$http', '$modal', '$state', function ($scope, $http,$modal, $state){

  $scope.data = {};
  var maintenance = function(data, status, headers, config) {
    $http({
      url: API_URL+"/settings/managesettings",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.id = data.id;
      $scope.value1 = data.value1;
      $scope.value2 = data.value2;
      $scope.value3 = data.value3;

    }).error(function (data, status, headers, config) {

    });
  };
  maintenance();

  $scope.savesettings = function (on){
    $http({
      url: API_URL + "/settings/maintenanceon",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(on)
    }).success(function (data, status, headers, config) {
      window.location.reload();
    }).error(function (data, status, headers, config) {

    });
  };

  $scope.savesettingsoff = function (){
    $http({
      url: API_URL + "/settings/maintenanceoff",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data, status, headers, config) {
      window.location.reload();
    }).error(function (data, status, headers, config) {

    });
  };

  $scope.isSaving = false;
  $scope.savePage = function(page){
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };
        $scope.isSaving = true;
        $http({
          url: API_URL + "/featuredprojects/insdata",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(page)
        }).success(function (data, status, headers, config) {
          $scope.isSaving = false;
          $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
          $scope.reset();
          $scope.formFeature.$setPristine(true)
        }).error(function (data, status, headers, config) {
          $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
        });
      }

    //Google Analytics
    $scope.save = function(scrept){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      $scope.isSaving = true;
      $http({
        url: API_URL + "/settings/googleanalytics",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(scrept)
      }).success(function (data, status, headers, config) {
        $scope.isSaving = false;
        $scope.alerts.push({type: 'success', msg: 'Script saved.'});
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }

    $http({
      url: API_URL + "/settings/loadscript" ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.scrept = data;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });

  }])

.controller('settingsMaintenanceCtrl', ['$scope','$http', '$modal',function($scope, $http, $modal){

  $(function() {

    var endDate = "<?php echo $this->view->value4 ?>";

    $('.countdown.styled').countdown({
      date: endDate,
      render: function(data) {
        $(this.el).html("<div>" + this.leadingZeros(data.days, 3) + " <span>days</span></div><div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div><div>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
      }
    });

  });

}])

////MANAGEPROPOSAL CONTROLLER
.controller('manageproposalsCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

  $scope.process = false;
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;
  var paginate = function (off, keyword) {
    $http({
      url: API_URL+"/proposals/manage/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
     $scope.data = data;
   }).error(function (data, status, headers, config) {
    $scope.status = status;
  });
 }
 $scope.search = function (keyword) {
  var off = 0;
  paginate(off, keyword);
}
$scope.paging = function (off, keyword) {

  paginate(off, keyword);
}
paginate(off, keyword);

/////////////////////////////////////////////////////////////////////////////////////////////////
        ///MODAL

        $scope.reviewproposal = function(id) {
          $scope.process = false;
          var modalInstance = $modal.open({
            templateUrl: 'reviewproposalModal.html',
            controller: reviewproposalCTRL,
            resolve: {
              id: function () {
                return id;
              }
            }
          });
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var reviewproposalCTRL = function($scope, $modalInstance, id) {
          $scope.process = false;
          $http({
            url: API_URL+"/proposals/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
                // console.log(data);
                $scope.id = data.id;
                $scope.reply.proposalid = data.id;
                $scope.name = data.name;
                $scope.reply.email = data.email;
                $scope.company = data.company;
                $scope.body = data.proposalbody;
                $scope.date = data.date;
                $scope.file = data.file;
                paginate(off, keyword);

              })

          $scope.reply = function(id) {

          };

          $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);

          };

          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };

          $scope.send = function (reply){
            $scope.process = true;
            $scope.process1 = true;
                // console.log($scope.reply.messages);
                $http({
                  url: API_URL + "/proposals/reply",

                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(reply)
                }).success(function (data, status, headers, config) {
                  $scope.process1 = false;
                  $scope.process2 = true;
                  paginate(off, keyword);
                    // console.log('success');
                    $modalInstance.dismiss('cancel');
                  }).error(function (data, status, headers, config) {

                  });
                };

              };

            }])

        ////END PROPOSAL CONTROLLER

// ---------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- jimmy kyben begin ------------------------------------------------

.controller('addcalendarCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

    // List Uploaded Images
    var loadImage = function(){
      $http({
        url: API_URL +"/calendar/listsimg",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadImage();

    // delete photo

    // delete modal

    $scope.huhuClick = function (imageid){
      var modalInstance = $modal.open({
        templateUrl: 'editMemberModal.html',
        controller: huhuCTRL,
        resolve: {
          imageid: function () {
            return imageid;
          }
        }
      });
    }

    var huhuCTRL = function ($scope, $modalInstance, imageid) {
        // console.log(imageid);
        $scope.imageid = imageid;
        $scope.ok = function (imageid) {
            // console.log(imageid);
            $http({
              url: API_URL+"/calendar/dltpagephoto",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imageid)
            })
            window.location.reload();
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

    // delete photo query

    $scope.dltPhoto = function (dlt){
        // console.log(dlt);
        $('.buttonPanel'+dlt.id).hide();
        $http({
          url: API_URL + "/calendar/dltpagephoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(dlt)
        }).success(function (data, status, headers, config) {
                //$('.buttonPanel'+dlt.id).show();
                //$scope.saveboxmessage = dlt.id;
              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            };

    // Insert Data
    $scope.isSaving = false;
    $scope.SaveActivity = function(act){
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };
        if (act.dto == undefined) {act.dto = act.dfrom;}
        if (act.mytime == undefined) {act.mytime = new Date();}
        // console.log(act.dto);
        // console.log(act.mytime);
        $scope.isSaving = true;
        // console.log(act);
        $http({
          url: API_URL + "/calendar/insdata",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(act)
        }).success(function (data, status, headers, config) {
          if(data.hasOwnProperty('ErrDate')){
            $scope.isSaving = false;
            $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
            $scope.formcalen.$setPristine(false);
          }else{
            $scope.isSaving = false;
            $scope.alerts.push({type: 'success', msg: 'New Activity Added!'});
            $scope.reset();
            $scope.formcalen.$setPristine(true);
          }
        }).error(function (data, status, headers, config, days_between) {
          $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
        });
      }

      $scope.reset = function () {
        $scope.act = {};
        $scope.formcalen.$setPristine(true);
      };

    // Upload Picture

    var newArray = new Array();
    $('#digitalAssets').fileupload({
      url: API_URL + '/server/php/index.php',
      dataType: 'json',
      disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
      previewMaxWidth: 100,
      previewMaxHeight: 100,
      previewCrop: true,
      limitMultiFileUploads: 4,
      done: function (e, data) {
        $.each(data.result.files, function (index, file) {
          $.get( API_URL + '/calendar/ajaxfileuploader/'+ file.name + '/').done(function( data ) {
                            //$('.digital-assets-gallery').prepend(data);
                            // console.log(data);
                            loadImage();
                          });
        });
      },
      progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
          'width',
          progress + '%'
          );
      }
    })

    // list sliders to select

    // var loadSlide = function(){
    //     $http({
    //         url: API_URL +"/calendar/slidelist",
    //         method: "GET",
    //         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    //     }).success(function (data) {
    //         // console.log(data);
    //         $scope.dataSlide = data;
    //     }).error(function (data) {
    //         $scope.status = status;
    //     });
    // }
    // loadSlide();

  }])

.controller('viewcalendarCtrl', ['$scope','$http', '$parse', '$modal', '$state', function($scope, $http, $parse, $modal, $state){

  $scope.data = {};

  var updatepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
        // console.log(pageid);
        $scope.ok = function (pageid) {
          $scope.pageid = pageid;
          $state.go('editcalendar', {pageid: pageid });
            //$state.go('editpage()')
            $modalInstance.dismiss('cancel');
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        }

        $scope.updatepage = function(pageid){
          $scope.pageid
          var modalInstance = $modal.open({
            templateUrl: 'updact.html',
            controller: updatepageInstanceCTRL,
            resolve: {
              pageid: function () {
                return pageid
              }
            }
          });
        }

        var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
          $scope.pageid = pageid;
        // console.log(pageid);
        $scope.ok = function (pageid) {
          var page = {'page': pageid};
          $http({
            url: API_URL + "/calendar/activitydelete/" + pageid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            window.location.reload();
            $scope.success = true;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.deletepage = function(pageid){
        $scope.pageid
        var modalInstance = $modal.open({
          templateUrl: 'delact.html',
          controller: deletepageInstanceCTRL,
          resolve: {
            pageid: function () {
              return pageid
            }
          }
        });
      }

    // list View
    var loadCalendar = function(){
      $http({
        url: API_URL +"/calendar/listview",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
        $scope.dataCalendar = data;
      }).error(function (data) {
        $scope.status = status;
      });
    }
    loadCalendar();

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* event source that pulls from google.com */
    $scope.eventSource = {
      url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
          };

          /* event source that contains custom events on the scope */
          var viewCalen = function(){
            $http({
              url: API_URL+"/calendar/manageactivity",
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
            //$scope.events = data;
            var arrevents = [];
            for(d in data){
              if(data[d].datef == data[d].datet){
                $scope.events.push({
                  title: data[d].title,
                  start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
                  className: ['b-l b-2x b-success'],
                  location: data[d].loc,
                  info: data[d].info
                });
              }else{
                $scope.events.push({
                  title: data[d].title,
                  start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
                  end: new Date(data[d].tyea, data[d].tmon-1, data[d].tday, data[d].hhr, data[d].hmin),
                  className: ['b-l b-2x b-info'],
                  location: data[d].loc,
                  info: data[d].info
                });
              }
            }

          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        }
        viewCalen();


        $scope.events = [];

        /* alert on eventClick info:'Two days dance training class.' */
        $scope.alertOnEventClick = function( event, jsEvent, view ){

        };
        /* alert on Drop */
        $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
          $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
        };
        /* alert on Resize */
        $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
          $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
        };

        /* alert on Drop */
        $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
          $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
        };
        /* alert on Resize */
        $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view){
          $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
        };

        $scope.overlay = $('.fc-overlay');

        $scope.alertOnMouseOver = function( event, jsEvent, view ){

          $scope.event = event;

          $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
          var wrap = $(jsEvent.target).closest('.fc-event');
          var cal = wrap.closest('.calendar');
          var left = wrap.offset().left - cal.offset().left;
          var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
          if( right > $scope.overlay.width() ) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
          }else if ( left > $scope.overlay.width() ) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
          }else{
            $scope.overlay.find('.arrow').addClass('top');
          }
          (wrap.find('.fc-overlay').length == 0) && wrap.append( $scope.overlay );
        }

        /* config object */
        $scope.uiConfig = {
          calendar:{
            height: 450,
            editable: false,
            header:{
              left: 'prev',
              center: 'title',
              right: 'next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventMouseover: $scope.alertOnMouseOver
          }
        };

        /* add custom event*/
        $scope.addEvent = function() {
          $scope.events.push({
            title: 'New Event',
            start: new Date(y, m, d),
            className: ['b-l b-2x b-info']
          });
        };

        /* remove event */
        $scope.remove = function(index) {
          $scope.events.splice(index,1);
        };

        /* Change View */
        $scope.changeView = function(view, calendar) {
          calendar.fullCalendar('changeView', view);
        };

        $scope.today = function(calendar) {
          calendar.fullCalendar('today');
        };

        $scope.renderCalender = function(calendar) {
          if(calendar){
            calendar.fullCalendar('render');
          }
        };

        /* event sources array*/
        $scope.eventSources = [$scope.events];

      }])

.controller('editcalendarCtrl', ['$http', '$scope', '$stateParams', '$modal',  function ($http, $scope, $stateParams, $modal) {
  $scope.onpagetitle = function convertToSlug(Text){
        // console.log('sad');
        if(Text == null)
        {

        }
        else
        {
          var text1 = Text.replace(/[^\w ]+/g,'');
          $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
      }

    //Execute Here
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.updatepage = function (act){
        // // console.log(act.dfrom);
        if (act.dfrom == undefined) {act.dfrom = new Date(act.dfrom2);}else{act.dfrom = new Date(act.dfrom);}
        if (act.dto == undefined) {act.dto = new Date(act.dto2);}else{act.dto = new Date(act.dto)};
        if (act.mytime == undefined) {act.mytime = new Date(act.mytime2);}else{act.mytime = act.mytime;}
        $http({
          url: API_URL + "/calendar/updateactivity/",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(act)
        }).success(function (data, status, headers, config) {
            // console.log(data);
            if(data == 1){
              $scope.alerts.push({type: 'danger', msg: 'Invalid date range.'});
            }else{
              $scope.alerts.push({type: 'success', msg: 'Activity Updated!'});
            }
          }).error(function (data, status, headers, config) {
            $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
          });
        };

        $http({
          url: API_URL + "/calendar/editactivity/" + $stateParams.pageid ,
          method: "GET",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
          $scope.act = data;
        // console.log(data);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });

      var updateInstanceCTRL = function ($scope, $modalInstance, title) {
        // console.log(title);
        $scope.ok = function (title) {
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.updatemodal = function(title){
        // console.log('assad');
        var modalInstance = $modal.open({
          templateUrl: 'huhu.html',
          controller: updateInstanceCTRL,
          resolve: {
            title: function () {
              return title
            }
          }
        });
      }

    // modal

    $scope.huhuClick = function (imageid){
      var modalInstance = $modal.open({
        templateUrl: 'editMemberModal.html',
        controller: huhuCTRL,
        resolve: {
          imageid: function () {
            return imageid;
          }
        }
      });
    }

    var huhuCTRL = function ($scope, $modalInstance, imageid) {
        // console.log(imageid);
        $scope.imageid = imageid;
        $scope.ok = function (imageid) {
            // console.log(imageid);
            $http({
              url: API_URL+"/calendar/dltpagephoto",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imageid)
            })
            window.location.reload();
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        var loadImages = function(){
          $http({
            url: API_URL + "/calendar/listsimg",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadImages();

        var newArray = new Array();
        $('#digitalAssets').fileupload({
          url: API_URL + '/server/php/index.php',
          dataType: 'json',
          disableImageResize: /Android(?!.*Chrome)|Opera/
          .test(window.navigator.userAgent),
          previewMaxWidth: 100,
          previewMaxHeight: 100,
          previewCrop: true,
          limitMultiFileUploads: 4,
          done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $.get( API_URL + '/calendar/ajaxfileuploader/'+ file.name + '/').done(function( data ) {
                            //$('.digital-assets-gallery').prepend(data);
                            // console.log(data);
                            loadImages();
                          });
            });
          },
          progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
              'width',
              progress + '%'
              );
          }
        })



        $scope.dltPhoto = function (dlt){
        // console.log(dlt);
        $('.buttonPanel'+dlt.id).hide();
        $http({
          url: API_URL + "/calendar/dltfeatphoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(dlt)
        }).success(function (data, status, headers, config) {
                //$('.buttonPanel'+dlt.id).show();
                //$scope.saveboxmessage = dlt.id;
              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            };


            $scope.banners = function convertToSlug(Text)
            {
              $scope.page.banners = Text;
            }


          }])
// create feature project controller

.controller('CreatefeaturedprojectCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

    // List Uploaded Images

    var loadImage = function(){
      $http({
        url: API_URL +"/featuredprojects/listsimg",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadImage();

    // delete modal

    $scope.huhuClick = function (imageid){
      var modalInstance = $modal.open({
        templateUrl: 'editMemberModal.html',
        controller: huhuCTRL,
        resolve: {
          imageid: function () {
            return imageid;
          }
        }
      });
    }

    var huhuCTRL = function ($scope, $modalInstance, imageid) {
        // console.log(imageid);
        $scope.imageid = imageid;
        $scope.ok = function (imageid) {
            // console.log(imageid);
            $http({
              url: API_URL+"/featuredprojects/dltpagephoto",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imageid)
            })
            window.location.reload();
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

    // delete photo query

    $scope.dltPhoto = function (dlt){
        // console.log(dlt);
        $('.buttonPanel'+dlt.id).hide();
        $http({
          url: API_URL + "/featuredprojects/dltpagephoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(dlt)
        }).success(function (data, status, headers, config) {
                //$('.buttonPanel'+dlt.id).show();
                //$scope.saveboxmessage = dlt.id;
              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            };

    // Insert Data

    $scope.isSaving = false;
    $scope.savePage = function(page){

        // console.log(page.check2);
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };
        $scope.isSaving = true;
        // console.log(page);
        $http({
          url: API_URL + "/featuredprojects/insdata",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(page)
        }).success(function (data, status, headers, config) {
          $scope.isSaving = false;
          $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
          $scope.reset();
          $scope.formFeature.$setPristine(true)
        }).error(function (data, status, headers, config) {
          $scope.alerts.push({type: 'error', msg: 'All fields is required.'});
        });
      }

      $scope.reset = function () {
        $scope.page = {};
        $scope.formFeature.$setPristine(true);
      };

    // Upload Picture

    var newArray = new Array();
    $('#digitalAssets').fileupload({
      url: API_URL + '/server/php/index.php',
      dataType: 'json',
      disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
      previewMaxWidth: 100,
      previewMaxHeight: 100,
      previewCrop: true,
      limitMultiFileUploads: 4,
      done: function (e, data) {
        $.each(data.result.files, function (index, file) {
          $.get( API_URL + '/featuredprojects/ajaxfileuploader/'+ file.name + '/').done(function( data ) {
            loadImage();
          });
        });
      },
      progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
          'width',
          progress + '%'
          );
      }
    })

    // list sliders to select

    var loadSlide = function(){
      $http({
        url: API_URL +"/featuredprojects/slidelist",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
            // console.log(data);
            $scope.dataSlide = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadSlide();

      }])

 // manage feature project controller

 .controller('ManagefeaturedprojectCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

    // $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;

    var paginate = function (off, keyword) {
      $http({
        url: API_URL+"/featuredprojects/managefeature/" + num + '/' + off + '/' + keyword,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $scope.data = data;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    }

    $scope.search = function (keyword) {
      var off = 0;
      paginate(off, keyword);
    }

    $scope.paging = function (off, keyword) {
      paginate(off, keyword);
    }

    paginate(off, keyword);

    var updatepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
      $scope.pageid = pageid;
      $scope.ok = function (pageid) {
        $scope.pageid = pageid;
        $state.go('editfeaturedproject', {pageid: pageid });
        $modalInstance.dismiss('cancel');
      };
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }

    $scope.updatepage = function(pageid){
      $scope.pageid
      var modalInstance = $modal.open({
        templateUrl: 'updatepage.html',
        controller: updatepageInstanceCTRL,
        resolve: {
          pageid: function () {
            return pageid
          }
        }
      });
    }

    var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
      $scope.pageid = pageid;
        // console.log(pageid);
        $scope.ok = function (pageid) {
          var page = {'page': pageid};
          $http({
            url: API_URL + "/featuredprojects/featuredelete/" + pageid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            window.location.reload();
            $scope.success = true;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.deletepage = function(pageid){
        $scope.pageid
        var modalInstance = $modal.open({
          templateUrl: 'deletepage.html',
          controller: deletepageInstanceCTRL,
          resolve: {
            pageid: function () {
              return pageid
            }
          }
        });
      }

    }])

// edit featured project controller

.controller('editfeatureCtrl', ['$http', '$scope', '$stateParams', '$modal',  function ($http, $scope, $stateParams, $modal) {
  $scope.onpagetitle = function convertToSlug(Text){
        // console.log('sad')
        if(Text == null)
        {

        }
        else
        {
          var text1 = Text.replace(/[^\w ]+/g,'');
          $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
      }

    //Execute Here
    $scope.updatepage = function (page){
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };

      $http({
        url: API_URL + "/featuredprojects/updatefeature/",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(page)
      }).success(function (data, status, headers, config) {
        $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
      }).error(function (data, status, headers, config) {
        $scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
      });
    };

    $http({
      url: API_URL + "/featuredprojects/editfeature/" + $stateParams.pageid ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.page = data;
        // console.log(data);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });

      var updateInstanceCTRL = function ($scope, $modalInstance, title) {
        // console.log(title);
        $scope.ok = function (title) {
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.updatemodal = function(title){
        // console.log('assad');
        var modalInstance = $modal.open({
          templateUrl: 'huhu.html',
          controller: updateInstanceCTRL,
          resolve: {
            title: function () {
              return title
            }
          }
        });
      }

    // modal

    $scope.huhuClick = function (imageid){
      var modalInstance = $modal.open({
        templateUrl: 'editMemberModal.html',
        controller: huhuCTRL,
        resolve: {
          imageid: function () {
            return imageid;
          }
        }
      });
    }

    var huhuCTRL = function ($scope, $modalInstance, imageid) {
        // console.log(imageid);
        $scope.imageid = imageid;
        $scope.ok = function (imageid) {
            // console.log(imageid);
            $http({
              url: API_URL+"/featuredprojects/dltpagephoto",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(imageid)
            })
            window.location.reload();
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        var loadSlide = function(){
          $http({
            url: API_URL +"/featuredprojects/slidelist",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
            // console.log(data);
            $scope.dataSlide = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadSlide();


        var loadImages = function(){
          $http({
            url: API_URL + "/featuredprojects/listsimg",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }
        loadImages();

        var newArray = new Array();
        $('#digitalAssets').fileupload({
          url: API_URL + '/server/php/index.php',
          dataType: 'json',
          disableImageResize: /Android(?!.*Chrome)|Opera/
          .test(window.navigator.userAgent),
          previewMaxWidth: 100,
          previewMaxHeight: 100,
          previewCrop: true,
          limitMultiFileUploads: 4,
          done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $.get( API_URL + '/featuredprojects/ajaxfileuploader/'+ file.name + '/').done(function( data ) {
                            //$('.digital-assets-gallery').prepend(data);
                            // console.log(data);
                            loadImages();
                          });
            });
          },
          progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
              'width',
              progress + '%'
              );
          }
        })

$scope.dltPhoto = function (dlt){
        // console.log(dlt);
        $('.buttonPanel'+dlt.id).hide();
        $http({
          url: API_URL + "/featuredprojects/dltfeatphoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(dlt)
        }).success(function (data, status, headers, config) {
            //$('.buttonPanel'+dlt.id).show();
            //$scope.saveboxmessage = dlt.id;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };

        $scope.banners = function convertToSlug(Text)
        {
          $scope.page.banners = Text;
        }
      }])

// manage subscribers controller

.controller('AddsubscriberCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

    // // Insert Data
    $scope.isSaving = false;
    $scope.addSubscriber = function(subscriber){
        //Execute Here
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };

        var mailtest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(subscriber.NMSemail);
        if (mailtest == false) {
          $scope.isSaving = false;
          $scope.alerts.push({type: 'danger', msg: 'Invalid email address.'});
        }else{
          $scope.isSaving = true;
            // console.log(subscriber);
            $http({
              url: API_URL + "/subscribers/insdata",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(subscriber)
            }).success(function (data, status, headers, config) {
              $scope.isSaving = false;
              $scope.alerts.push({type: 'success', msg: 'New Subscriber Added!'});
              $scope.reset();
              $scope.form.$setPristine(true)
            }).error(function (data, status, headers, config) {
              $scope.alerts.push({type: 'error', msg: 'Enter Email address.'});
            });
          }
        }
        $scope.reset = function () {
          $scope.subscriber = {};
          $scope.form.$setPristine(true);
        };
      }])


.controller('SubscribersListCtrl', ['$scope','$http', '$modal', '$state', function($scope, $http,$modal, $state){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;

  var paginate = function (off, keyword) {
    $http({
      url: API_URL+"/subscribers/subscriberslist/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  $scope.search = function (keyword) {
    var off = 0;
    paginate(off, keyword);
  }

  $scope.paging = function (off, keyword) {
    paginate(off, keyword);
  }

  paginate(off, keyword);

  var updatepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
    $scope.pageid = pageid;
        // console.log(pageid);
        $scope.ok = function (pageid) {
          $scope.pageid = pageid;
          $state.go('editsubscriber', {pageid: pageid });
            //$state.go('editpage()')
            $modalInstance.dismiss('cancel');
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        }

        $scope.updatepage = function(pageid){
          $scope.pageid
          var modalInstance = $modal.open({
            templateUrl: 'updatepage.html',
            controller: updatepageInstanceCTRL,
            resolve: {
              pageid: function () {
                return pageid
              }
            }
          });
        }

        var deletepageInstanceCTRL = function ($scope, $modalInstance, pageid, $state) {
          $scope.pageid = pageid;
        // console.log(pageid);
        $scope.ok = function (pageid) {
          var page = {'page': pageid};
          $http({
            url: API_URL + "/subscribers/subscriberslist/" + pageid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
          }).success(function (data, status, headers, config) {
            $modalInstance.close();
            window.location.reload();
            $scope.success = true;
          }).error(function (data, status, headers, config) {
            $scope.status = status;
          });
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.deletepage = function(pageid){
        $scope.pageid
        var modalInstance = $modal.open({
          templateUrl: 'deletepage.html',
          controller: deletepageInstanceCTRL,
          resolve: {
            pageid: function () {
              return pageid
            }
          }
        });
      }

      var sentletterInstanceCTRL = function ($scope, $modalInstance, NMSemail, $state) {

        // console.log(NMSemail);
        $scope.data = {};
        var num = 10;
        var off = 1;
        var keyword = null;

        var paginate = function (off, keyword) {
          $http({
            url: API_URL+"/subscribers/sentlist/" + num + '/' + off + '/' + NMSemail,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function (data, status, headers, config) {
            $scope.data = data;
                // console.log(data.sentdata);
              }).error(function (data, status, headers, config) {
                $scope.status = status;
              });
            }

            $scope.paging = function (off, keyword) {
              paginate(off, keyword);
            }

            paginate(off, keyword);

          }

          $scope.viewsentletter = function(NMSemail){
            $scope.NMSemail
            var modalInstance = $modal.open({
              templateUrl: 'sentletter.html',
              controller: sentletterInstanceCTRL,
              resolve: {
                NMSemail: function () {
                  return NMSemail
                }
              }
            });
          }

        }])

.controller('EditsubscriberCtrl', ['$http', '$scope', '$stateParams', '$modal',  function ($http, $scope, $stateParams, $modal) {

    //Execute Here
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.updatepage = function (page){

      var mailtest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(page.NMSemail);
      if (mailtest == false) {
        $scope.isSaving = false;
        $scope.alerts.push({type: 'danger', msg: 'Invalid email address.'});
      }else{
        $http({
          url: API_URL + "/subscribers/updatefeature/",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(page)
        }).success(function (data, status, headers, config) {
          if (data.hasOwnProperty('alerts')) {
            $scope.alerts = data.alerts;
          } else {
            $scope.alerts.push({type: 'success', msg: 'Subscriber Updated!'});
            $scope.success = true;
            $modalInstance.close();
            window.location.reload();
          }
        }).error(function (data, status, headers, config) {
          $scope.alerts.push({type: 'danger', msg: 'Enter email address.'});
        });
      }
    };

    $http({
      url: API_URL + "/subscribers/editfeature/" + $stateParams.pageid ,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.page = data;
        // console.log(data);
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });

      var updateInstanceCTRL = function ($scope, $modalInstance, NMSemail) {
        // console.log(NMSemail);
        $scope.ok = function (NMSemail) {
        };
        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }

      $scope.updatemodal = function(NMSemail){
        var modalInstance = $modal.open({
          templateUrl: 'huhu.html',
          controller: updateInstanceCTRL,
          resolve: {
            NMSemail: function () {
              return NMSemail
            }
          }
        });
      }
    }])

// -------------------------------------------------- jimmy end --------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------



    ////DISPLAY IMAGE FROM DATABASE///////////////////////////////////////////////////////////////////////////////////////
    .controller('SlideruploadCtrl', ['$scope','$http', '$modal',function($scope, $http, $modal){


      $scope.huhuClick = function (imageid){
        var modalInstance = $modal.open({
          templateUrl: 'editMemberModal.html',
          controller: huhuCTRL,
          resolve: {
            imageid: function () {
              return imageid;
            }
          }
        });
      }
      var huhuCTRL = function ($scope, $modalInstance, imageid) {
            // console.log(imageid);
            $scope.imageid = imageid;
            $scope.ok = function (imageid) {
                // console.log(imageid);
                $http({
                  url: API_URL + "/utility/dltphoto",
                  method: "POST",
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param(imageid)
                })
                window.location.reload();
              };
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            };

            var loadImages = function(){
             $http({
              url: API_URL + "/utility/listsliderimages",
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            // console.log(data);
            $scope.data = data;
          }).error(function (data) {
            $scope.status = status;
          });
        }

        var newArray = new Array();
        $('#digitalAssets').fileupload({
          url: API_URL + '/server/php/index.php',
          dataType: 'json',
          disableImageResize: /Android(?!.*Chrome)|Opera/
          .test(window.navigator.userAgent),
          previewMaxWidth: 100,
          previewMaxHeight: 100,
          previewCrop: true,
          limitMultiFileUploads: 4,
          done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $.get( API_URL + '/utility/ajaxfileuploader/'+ file.name + '/'+$('#albumtitle').value() ).done(function( data ) {
                          //$('.digital-assets-gallery').prepend(data);
                          // console.log(data);
                          //loadImages();
                        });
            });
          },
          progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
              'width',
              progress + '%'
              );
          }
        }).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');

$scope.page = {};
$scope.saveboxmessage = '';
$scope.imgInfo = function (info){
        // console.log(info);
        $('.buttonPanel'+info.id).hide();
        $http({
          url: API_URL + "/utility/imginfoupdate",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(info)
        }).success(function (data, status, headers, config) {
          $('.buttonPanel'+info.id).show();
          $scope.saveboxmessage = info.id;

        }).error(function (data, status, headers, config) {
          $scope.status = status;
        });

      };
      $scope.dltPhoto = function (dlt){
        // console.log(dlt);
        $('.buttonPanel'+dlt.id).hide();
        $http({
          url: API_URL + "/utility/dltphoto",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(dlt)
        }).success(function (data, status, headers, config) {
                    //$('.buttonPanel'+dlt.id).show();
                    //$scope.saveboxmessage = dlt.id;
                  }).error(function (data, status, headers, config) {
                    $scope.status = status;
                  });



                };


              }])
.controller('GoogleMapCtrl', ['$http', '$scope', '$stateParams', '$modal', '$state',  function ($http, $scope, $stateParams, $modal, $state) {

  $scope.data = {};
  $scope.deletepeacemap = function (id, albumname){
    var modalInstance = $modal.open({
      templateUrl: 'deletepeacemap.html',
      controller: DELModalInstanceCtrl,
      resolve: {
        id: function () {
          return id;
        },
        albumname: function () {
          return albumname;
        },
      }
    });
  }
  var DELModalInstanceCtrl = function ($scope, $modalInstance, id, albumname) {
    $scope.albumname=albumname;
    $scope.process = false;
    $scope.success = false;
    $scope.ok = function () {
      $scope.process = true;
      $http({
        url: API_URL + "/peacemap/delete/" + id,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        $modalInstance.close();
        window.location.reload();
        $scope.success = true;
      }).error(function (data, status, headers, config) {
        $scope.status = status;
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  };

//Update Map

var EditInstanceCtrl = function ($scope, $modalInstance, id) {

  $scope.ok = function () {
    $state.go('editpeacemap', {id: id });
    $scope.process = true;
    $modalInstance.close();

  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};
$scope.editpeacemap = function (id) {
  var modalInstance = $modal.open({
    templateUrl: 'editpeacemap.html',
    controller: EditInstanceCtrl,
    resolve: {
      id: function () {
        return id;
      }
    }
  });
}

$http({
  url: API_URL+"/peacemap/list",
  method: "GET",
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
}).success(function (data, status, headers, config) {
  $scope.data = data;
}).error(function (data, status, headers, config) {
  $scope.status = status;
});
}])

.controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window',
  function ($scope, $translate, $localStorage, $window) {
                // add 'ie' classes to html
                var isIE = !!navigator.userAgent.match(/MSIE/i);
                isIE && angular.element($window.document.body).addClass('ie');
                isSmartDevice($window) && angular.element($window.document.body).addClass('smart');
                // config
                $scope.app = {
                  name: 'P.I.',
                  version: '1.1.2',
                    // for chart colors
                    color: {
                      primary: '#7266ba',
                      info: '#23b7e5',
                      success: '#27c24c',
                      warning: '#fad733',
                      danger: '#f05050',
                      light: '#e8eff0',
                      dark: '#3a3f51',
                      black: '#1c2b36'
                    },
                    settings: {
                      themeID: 2,
                      navbarHeaderColor: 'bg-info dker',
                      navbarCollapseColor: 'bg-info dker',
                      asideColor: 'bg-light dker b-r',
                      headerFixed: true,
                      asideFixed: false,
                      asideFolded: false
                    }
                  }

                // save settings to local storage
                if (angular.isDefined($localStorage.settings)) {
                  $scope.app.settings = $localStorage.settings;
                } else {4
                  $localStorage.settings = $scope.app.settings;
                }
                $scope.$watch('app.settings', function () {
                  $localStorage.settings = $scope.app.settings;
                }, true);
                // angular translate
                $scope.lang = {isopen: false};
                $scope.langs = {en: 'English', de_DE: 'German', it_IT: 'Italian'};
                $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
                $scope.setLang = function (langKey, $event) {
                    // set the current lang
                    $scope.selectLang = $scope.langs[langKey];
                    // You can change the language during runtime
                    $translate.use(langKey);
                    $scope.lang.isopen = !$scope.lang.isopen;
                  };
                  function isSmartDevice($window)
                  {
                    // Adapted from http://www.detectmobilebrowsers.com
                    var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
                    // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
                    return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
                  }

                }])

        // bootstrap controller
        .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
          $scope.oneAtATime = true;
          $scope.groups = [
          {
            title: 'Accordion group header - #1',
            content: 'Dynamic group body - #1'
          },
          {
            title: 'Accordion group header - #2',
            content: 'Dynamic group body - #2'
          }
          ];
          $scope.items = ['Item 1', 'Item 2', 'Item 3'];
          $scope.addItem = function () {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
          };
          $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
          };
        }])
        .controller('AlertDemoCtrl', ['$scope', function ($scope) {
          $scope.alerts = [
          {type: 'success', msg: 'Well done! You successfully read this important alert message.'},
          {type: 'info', msg: 'Heads up! This alert needs your attention, but it is not super important.'},
          {type: 'warning', msg: 'Warning! Best check yo self, you are not looking too good...'}
          ];
          $scope.addAlert = function () {
            $scope.alerts.push({type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.'});
          };
          $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
          };
        }])
        .controller('ButtonsDemoCtrl', ['$scope', function ($scope) {
          $scope.singleModel = 1;
          $scope.radioModel = 'Middle';
          $scope.checkModel = {
            left: false,
            middle: true,
            right: false
          };
        }])
        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
          $scope.myInterval = 5000;
          var slides = $scope.slides = [];
          $scope.addSlide = function () {
            slides.push({
              image: 'img/c' + slides.length + '.jpg',
              text: ['Carousel text #0', 'Carousel text #1', 'Carousel text #2', 'Carousel text #3'][slides.length % 4]
            });
          };
          for (var i = 0; i < 4; i++) {
            $scope.addSlide();
          }
        }])
        .controller('DropdownDemoCtrl', ['$scope', function ($scope) {
          $scope.items = [
          'The first choice!',
          'And another choice for you.',
          'but wait! A third!'
          ];
          $scope.status = {
            isopen: false
          };
          $scope.toggled = function (open) {
                    //// console.log('Dropdown is now: ', open);
                  };
                  $scope.toggleDropdown = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.status.isopen = !$scope.status.isopen;
                  };
                }])
        .controller('ModalDemoCtrl', ['$scope', '$modal', '$log', function ($scope, $modal, $log) {
          $scope.items = ['item1', 'item2', 'item3'];
          var ModalInstanceCtrl = function ($scope, $modalInstance, items) {
            $scope.items = items;
            $scope.selected = {
              item: $scope.items[0]
            };
            $scope.ok = function () {
              $modalInstance.close($scope.selected.item);
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          };
          $scope.open = function (size) {
            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: ModalInstanceCtrl,
              size: size,
              resolve: {
                items: function () {
                  return $scope.items;
                }
              }
            });
            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
              $log.info('Modal dismissed at: ' + new Date());
            });
          };
        }])
.controller('PaginationDemoCtrl', ['$scope', '$log', function ($scope, $log) {
  $scope.totalItems = 64;
  $scope.currentPage = 4;
  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.pageChanged = function () {
    $log.info('Page changed to: ' + $scope.currentPage);
  };
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;
}])
.controller('PopoverDemoCtrl', ['$scope', function ($scope) {
  $scope.dynamicPopover = 'Hello, World!';
  $scope.dynamicPopoverTitle = 'Title';
}])
.controller('ProgressDemoCtrl', ['$scope', function ($scope) {
  $scope.max = 200;
  $scope.random = function () {
    var value = Math.floor((Math.random() * 100) + 1);
    var type;
    if (value < 25) {
      type = 'success';
    } else if (value < 50) {
      type = 'info';
    } else if (value < 75) {
      type = 'warning';
    } else {
      type = 'danger';
    }

    $scope.showWarning = (type === 'danger' || type === 'warning');
    $scope.dynamic = value;
    $scope.type = type;
  };
  $scope.random();
  $scope.randomStacked = function () {
    $scope.stacked = [];
    var types = ['success', 'info', 'warning', 'danger'];
    for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
      var index = Math.floor((Math.random() * 4));
      $scope.stacked.push({
        value: Math.floor((Math.random() * 30) + 1),
        type: types[index]
      });
    }
  };
  $scope.randomStacked();
}])
.controller('TabsDemoCtrl', ['$scope', function ($scope) {
  $scope.tabs = [
  {title: 'Dynamic Title 1', content: 'Dynamic content 1'},
  {title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true}
  ];
}])
.controller('RatingDemoCtrl', ['$scope', function ($scope) {
  $scope.rate = 7;
  $scope.max = 10;
  $scope.isReadonly = false;
  $scope.hoveringOver = function (value) {
    $scope.overStar = value;
    $scope.percent = 100 * (value / $scope.max);
  };
}])
.controller('TooltipDemoCtrl', ['$scope', function ($scope) {
  $scope.dynamicTooltip = 'Hello, World!';
  $scope.dynamicTooltipText = 'dynamic';
  $scope.htmlTooltip = 'I\'ve been made <b>bold</b>!';
}])
.controller('TypeaheadDemoCtrl', ['$scope', '$http', function ($scope, $http) {
  $scope.selected = undefined;
  $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
                // Any function returning a promise object can be used to load values asynchronously
                $scope.getLocation = function (val) {
                  return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                    params: {
                      address: val,
                      sensor: false
                    }
                  }).then(function (res) {
                    var addresses = [];
                    angular.forEach(res.data.results, function (item) {
                      addresses.push(item.formatted_address);
                    });
                    return addresses;
                  });
                };
              }])

.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.today = function () {
    $scope.dt = new Date();
  };
  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;
  };
    // Disable weekend selection
    // $scope.disabled = function (date, mode) {
    //     return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    // };
    $scope.toggleMin = function () {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };
    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
  }])

.controller('TimepickerDemoCtrl', ['$scope', function ($scope) {
  $scope.mytime = new Date();
  $scope.hstep = 1;
  $scope.mstep = 15;
  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };
  $scope.ismeridian = true;
  $scope.toggleMode = function () {
    $scope.ismeridian = !$scope.ismeridian;
  };
  $scope.update = function () {
    var d = new Date();
    d.setHours(14);
    d.setMinutes(0);
    $scope.mytime = d;
  };
  $scope.changed = function () {
        //// console.log('Time changed to: ' + $scope.mytime);
      };
      $scope.clear = function () {
        $scope.mytime = null;
      };
    }])

        // Form controller
        .controller('FormDemoCtrl', ['$scope', function ($scope) {
          $scope.notBlackListed = function (value) {
            var blacklist = ['bad@domain.com', 'verybad@domain.com'];
            return blacklist.indexOf(value) === -1;
          }

          $scope.val = 15;
          var updateModel = function (val) {
            $scope.$apply(function () {
              $scope.val = val;
            });
          };
          angular.element("#slider").on('slideStop', function (data) {
            updateModel(data.value);
          });
          $scope.select2Number = [
          {text: 'First', value: 'One'},
          {text: 'Second', value: 'Two'},
          {text: 'Third', value: 'Three'}
          ];
          $scope.list_of_string = ['tag1', 'tag2']
          $scope.select2Options = {
            'multiple': true,
            'simple_tags': true,
                    'tags': ['tag1', 'tag2', 'tag3', 'tag4']  // Can be empty list.
                  };
                }])

        // Flot Chart controller
        .controller('FlotChartDemoCtrl', ['$scope', function ($scope) {

          $scope.d = [[1, 6.5], [2, 6.5], [3, 7], [4, 8], [5, 7.5], [6, 7], [7, 6.8], [8, 7], [9, 7.2], [10, 7], [11, 6.8], [12, 7]];
          $scope.d0_1 = [[0, 7], [1, 6.5], [2, 12.5], [3, 7], [4, 9], [5, 6], [6, 11], [7, 6.5], [8, 8], [9, 7]];
          $scope.d0_2 = [[0, 4], [1, 4.5], [2, 7], [3, 4.5], [4, 3], [5, 3.5], [6, 6], [7, 3], [8, 4], [9, 3]];
          $scope.d1_1 = [[10, 120], [20, 70], [30, 70], [40, 60]];
          $scope.d1_2 = [[10, 50], [20, 60], [30, 90], [40, 35]];
          $scope.d1_3 = [[10, 80], [20, 40], [30, 30], [40, 20]];
          $scope.d2 = [];
          for (var i = 0; i < 20; ++i) {
            $scope.d2.push([i, Math.sin(i)]);
          }

          $scope.d3 = [
          {label: "iPhone5S", data: 40},
          {label: "iPad Mini", data: 10},
          {label: "iPad Mini Retina", data: 20},
          {label: "iPhone4S", data: 12},
          {label: "iPad Air", data: 18}
          ];
          $scope.getRandomData = function () {
            var data = [],
            totalPoints = 150;
            if (data.length > 0)
              data = data.slice(1);
            while (data.length < totalPoints) {
              var prev = data.length > 0 ? data[data.length - 1] : 50,
              y = prev + Math.random() * 10 - 5;
              if (y < 0) {
                y = 0;
              } else if (y > 100) {
                y = 100;
              }
              data.push(y);
            }
                    // Zip the generated y values with the x values
                    var res = [];
                    for (var i = 0; i < data.length; ++i) {
                      res.push([i, data[i]])
                    }
                    return res;
                  }

                  $scope.d4 = $scope.getRandomData();
                }])

        // jVectorMap controller
        .controller('JVectorMapDemoCtrl', ['$scope', function ($scope) {
          $scope.world_markers = [
          {latLng: [41.90, 12.45], name: 'Vatican City'},
          {latLng: [43.73, 7.41], name: 'Monaco'},
          {latLng: [-0.52, 166.93], name: 'Nauru'},
          {latLng: [-8.51, 179.21], name: 'Tuvalu'},
          {latLng: [43.93, 12.46], name: 'San Marino'},
          {latLng: [47.14, 9.52], name: 'Liechtenstein'},
          {latLng: [7.11, 171.06], name: 'Marshall Islands'},
          {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
          {latLng: [3.2, 73.22], name: 'Maldives'},
          {latLng: [35.88, 14.5], name: 'Malta'},
          {latLng: [12.05, -61.75], name: 'Grenada'},
          {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
          {latLng: [13.16, -59.55], name: 'Barbados'},
          {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
          {latLng: [-4.61, 55.45], name: 'Seychelles'},
          {latLng: [7.35, 134.46], name: 'Palau'},
          {latLng: [42.5, 1.51], name: 'Andorra'},
          {latLng: [14.01, -60.98], name: 'Saint Lucia'},
          {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
          {latLng: [1.3, 103.8], name: 'Singapore'},
          {latLng: [1.46, 173.03], name: 'Kiribati'},
          {latLng: [-21.13, -175.2], name: 'Tonga'},
          {latLng: [15.3, -61.38], name: 'Dominica'},
          {latLng: [-20.2, 57.5], name: 'Mauritius'},
          {latLng: [26.02, 50.55], name: 'Bahrain'},
          {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
          ];
          $scope.usa_markers = [
          {latLng: [40.71, -74.00], name: 'New York'},
          {latLng: [34.05, -118.24], name: 'Los Angeles'},
          {latLng: [41.87, -87.62], name: 'Chicago'},
          {latLng: [29.76, -95.36], name: 'Houston'},
          {latLng: [39.95, -75.16], name: 'Philadelphia'},
          {latLng: [38.90, -77.03], name: 'Washington'},
          {latLng: [37.36, -122.03], name: 'Silicon Valley'}
          ];
        }])

        // signin controller
        .controller('SigninFormController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
          $scope.user = {};
          $scope.authError = null;
          $scope.login = function () {
            $scope.authError = null;
                    // Try to login
                    $http.post('api/login', {
                      email: $scope.user.email,
                      password: $scope.user.password
                    })
                    .then(function (response) {
                      if (!response.data.user) {
                        $scope.authError = 'Email or Password not right';
                      } else {
                        $state.go('app.dashboard');
                      }
                    }, function (x) {
                      $scope.authError = 'Server Error';
                    });
                  };
                }])

        .controller('ForgotPasswordController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
          $scope.user = {};
          $scope.authError = null;
          $scope.send = function(){
              // console.log("send email");
              $http({
                url: API_URL + "/forgotpassword/resetpassword",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
              }).success(function (data, status, headers, config) {
                // console.log('success');
                $modalInstance.dismiss('cancel');
              }).error(function (data, status, headers, config) {

              });


            }
          }])




    // signup controller
    .controller('SignupFormController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
      $scope.user = {};
      $scope.authError = null;
      $scope.signup = function () {
        $scope.authError = null;
            // Try to create
            $http.post('api/signup', {name: $scope.user.name, email: $scope.user.email, password: $scope.user.password})
            .then(function (response) {
              if (!response.data.user) {
                $scope.authError = response;
              } else {
                $state.go('app.dashboard');
              }
            }, function (x) {
              $scope.authError = 'Server Error';
            });
          };
        }])

    app.constant('Config', {
      // This is a generated Config
      // Any changes you make in this file will be replaced
      // Place you changes in app/config.php file
      amazonlink : "https://earthcitizens.s3.amazonaws.com",
      ClientToken : "82c4cb99-8440-41ef-a6a5-0e81d27b4c5f",
      AWSAccessKeyId : "AKIAIC7BVEKDLA4NJPIA",
      policy : "ewogICJleHBpcmF0aW9uIjogIjIwMjAtMDEtMDFUMDA6MDA6MDBaIiwKICAiY29uZGl0aW9ucyI6IFsKICAgIHsiYnVja2V0IjogImVhcnRoY2l0aXplbnMifSwKICAgIFsic3RhcnRzLXdpdGgiLCAiJGtleSIsICIiXSwKICAgIHsiYWNsIjogInByaXZhdGUifSwKICAgIFsic3RhcnRzLXdpdGgiLCAiJENvbnRlbnQtVHlwZSIsICIiXSwKICAgIFsiY29udGVudC1sZW5ndGgtcmFuZ2UiLCAwLCA1MjQyODgwMDBdCiAgXQp9",
      signature : "jUW+7DZVeTdDXSSeiFHwBC0C05o=",
    })




    .factory('MDY', function () {
      return {
        month: function () {
          return [
          {name: 'January', val: 1},
          {name: 'February', val: 2},
          {name: 'March', val: 3},
          {name: 'April', val: 4},
          {name: 'May', val: 5},
          {name: 'June', val: 6},
          {name: 'July', val: 7},
          {name: 'August', val: 8},
          {name: 'September', val: 9},
          {name: 'October', val: 10},
          {name: 'November', val: 11},
          {name: 'December', val: 12}
          ]
        },
        day: function () {
          var day = [];
          for (var x = 1; x <= 31; x++) {
            day.push({'val': x})
          }
          return day;
        },
        year: function () {
          var year = [];
          for (var y = 2013; y >= 1900; y--) {
            year.push({'val': y})
          }
          return year;
        }
      }
    })

    .factory('Countries', function () {
      return {
        list: function () {
          return [
          {
            "name": "Afghanistan",
            "code": "AF"
          },
          {
            "name": "Åland Islands",
            "code": "AX"
          },
          {
            "name": "Albania",
            "code": "AL"
          },
          {
            "name": "Algeria",
            "code": "DZ"
          },
          {
            "name": "American Samoa",
            "code": "AS"
          },
          {
            "name": "AndorrA",
            "code": "AD"
          },
          {
            "name": "Angola",
            "code": "AO"
          },
          {
            "name": "Anguilla",
            "code": "AI"
          },
          {
            "name": "Antarctica",
            "code": "AQ"
          },
          {
            "name": "Antigua and Barbuda",
            "code": "AG"
          },
          {
            "name": "Argentina",
            "code": "AR"
          },
          {
            "name": "Armenia",
            "code": "AM"
          },
          {
            "name": "Aruba",
            "code": "AW"
          },
          {
            "name": "Australia",
            "code": "AU"
          },
          {
            "name": "Austria",
            "code": "AT"
          },
          {
            "name": "Azerbaijan",
            "code": "AZ"
          },
          {
            "name": "Bahamas",
            "code": "BS"
          },
          {
            "name": "Bahrain",
            "code": "BH"
          },
          {
            "name": "Bangladesh",
            "code": "BD"
          },
          {
            "name": "Barbados",
            "code": "BB"
          },
          {
            "name": "Belarus",
            "code": "BY"
          },
          {
            "name": "Belgium",
            "code": "BE"
          },
          {
            "name": "Belize",
            "code": "BZ"
          },
          {
            "name": "Benin",
            "code": "BJ"
          },
          {
            "name": "Bermuda",
            "code": "BM"
          },
          {
            "name": "Bhutan",
            "code": "BT"
          },
          {
            "name": "Bolivia",
            "code": "BO"
          },
          {
            "name": "Bosnia and Herzegovina",
            "code": "BA"
          },
          {
            "name": "Botswana",
            "code": "BW"
          },
          {
            "name": "Bouvet Island",
            "code": "BV"
          },
          {
            "name": "Brazil",
            "code": "BR"
          },
          {
            "name": "British Indian Ocean Territory",
            "code": "IO"
          },
          {
            "name": "Brunei Darussalam",
            "code": "BN"
          },
          {
            "name": "Bulgaria",
            "code": "BG"
          },
          {
            "name": "Burkina Faso",
            "code": "BF"
          },
          {
            "name": "Burundi",
            "code": "BI"
          },
          {
            "name": "Cambodia",
            "code": "KH"
          },
          {
            "name": "Cameroon",
            "code": "CM"
          },
          {
            "name": "Canada",
            "code": "CA"
          },
          {
            "name": "Cape Verde",
            "code": "CV"
          },
          {
            "name": "Cayman Islands",
            "code": "KY"
          },
          {
            "name": "Central African Republic",
            "code": "CF"
          },
          {
            "name": "Chad",
            "code": "TD"
          },
          {
            "name": "Chile",
            "code": "CL"
          },
          {
            "name": "China",
            "code": "CN"
          },
          {
            "name": "Christmas Island",
            "code": "CX"
          },
          {
            "name": "Cocos (Keeling) Islands",
            "code": "CC"
          },
          {
            "name": "Colombia",
            "code": "CO"
          },
          {
            "name": "Comoros",
            "code": "KM"
          },
          {
            "name": "Congo",
            "code": "CG"
          },
          {
            "name": "Congo, The Democratic Republic of the",
            "code": "CD"
          },
          {
            "name": "Cook Islands",
            "code": "CK"
          },
          {
            "name": "Costa Rica",
            "code": "CR"
          },
          {
            "name": "Cote D\"Ivoire",
            "code": "CI"
          },
          {
            "name": "Croatia",
            "code": "HR"
          },
          {
            "name": "Cuba",
            "code": "CU"
          },
          {
            "name": "Cyprus",
            "code": "CY"
          },
          {
            "name": "Czech Republic",
            "code": "CZ"
          },
          {
            "name": "Denmark",
            "code": "DK"
          },
          {
            "name": "Djibouti",
            "code": "DJ"
          },
          {
            "name": "Dominica",
            "code": "DM"
          },
          {
            "name": "Dominican Republic",
            "code": "DO"
          },
          {
            "name": "Ecuador",
            "code": "EC"
          },
          {
            "name": "Egypt",
            "code": "EG"
          },
          {
            "name": "El Salvador",
            "code": "SV"
          },
          {
            "name": "Equatorial Guinea",
            "code": "GQ"
          },
          {
            "name": "Eritrea",
            "code": "ER"
          },
          {
            "name": "Estonia",
            "code": "EE"
          },
          {
            "name": "Ethiopia",
            "code": "ET"
          },
          {
            "name": "Falkland Islands (Malvinas)",
            "code": "FK"
          },
          {
            "name": "Faroe Islands",
            "code": "FO"
          },
          {
            "name": "Fiji",
            "code": "FJ"
          },
          {
            "name": "Finland",
            "code": "FI"
          },
          {
            "name": "France",
            "code": "FR"
          },
          {
            "name": "French Guiana",
            "code": "GF"
          },
          {
            "name": "French Polynesia",
            "code": "PF"
          },
          {
            "name": "French Southern Territories",
            "code": "TF"
          },
          {
            "name": "Gabon",
            "code": "GA"
          },
          {
            "name": "Gambia",
            "code": "GM"
          },
          {
            "name": "Georgia",
            "code": "GE"
          },
          {
            "name": "Germany",
            "code": "DE"
          },
          {
            "name": "Ghana",
            "code": "GH"
          },
          {
            "name": "Gibraltar",
            "code": "GI"
          },
          {
            "name": "Greece",
            "code": "GR"
          },
          {
            "name": "Greenland",
            "code": "GL"
          },
          {
            "name": "Grenada",
            "code": "GD"
          },
          {
            "name": "Guadeloupe",
            "code": "GP"
          },
          {
            "name": "Guam",
            "code": "GU"
          },
          {
            "name": "Guatemala",
            "code": "GT"
          },
          {
            "name": "Guernsey",
            "code": "GG"
          },
          {
            "name": "Guinea",
            "code": "GN"
          },
          {
            "name": "Guinea-Bissau",
            "code": "GW"
          },
          {
            "name": "Guyana",
            "code": "GY"
          },
          {
            "name": "Haiti",
            "code": "HT"
          },
          {
            "name": "Heard Island and Mcdonald Islands",
            "code": "HM"
          },
          {
            "name": "Holy See (Vatican City State)",
            "code": "VA"
          },
          {
            "name": "Honduras",
            "code": "HN"
          },
          {
            "name": "Hong Kong",
            "code": "HK"
          },
          {
            "name": "Hungary",
            "code": "HU"
          },
          {
            "name": "Iceland",
            "code": "IS"
          },
          {
            "name": "India",
            "code": "IN"
          },
          {
            "name": "Indonesia",
            "code": "ID"
          },
          {
            "name": "Iran, Islamic Republic Of",
            "code": "IR"
          },
          {
            "name": "Iraq",
            "code": "IQ"
          },
          {
            "name": "Ireland",
            "code": "IE"
          },
          {
            "name": "Isle of Man",
            "code": "IM"
          },
          {
            "name": "Israel",
            "code": "IL"
          },
          {
            "name": "Italy",
            "code": "IT"
          },
          {
            "name": "Jamaica",
            "code": "JM"
          },
          {
            "name": "Japan",
            "code": "JP"
          },
          {
            "name": "Jersey",
            "code": "JE"
          },
          {
            "name": "Jordan",
            "code": "JO"
          },
          {
            "name": "Kazakhstan",
            "code": "KZ"
          },
          {
            "name": "Kenya",
            "code": "KE"
          },
          {
            "name": "Kiribati",
            "code": "KI"
          },
          {
            "name": "Korea, Democratic People\"S Republic of",
            "code": "KP"
          },
          {
            "name": "Korea, Republic of",
            "code": "KR"
          },
          {
            "name": "Kuwait",
            "code": "KW"
          },
          {
            "name": "Kyrgyzstan",
            "code": "KG"
          },
          {
            "name": "Lao People\"S Democratic Republic",
            "code": "LA"
          },
          {
            "name": "Latvia",
            "code": "LV"
          },
          {
            "name": "Lebanon",
            "code": "LB"
          },
          {
            "name": "Lesotho",
            "code": "LS"
          },
          {
            "name": "Liberia",
            "code": "LR"
          },
          {
            "name": "Libyan Arab Jamahiriya",
            "code": "LY"
          },
          {
            "name": "Liechtenstein",
            "code": "LI"
          },
          {
            "name": "Lithuania",
            "code": "LT"
          },
          {
            "name": "Luxembourg",
            "code": "LU"
          },
          {
            "name": "Macao",
            "code": "MO"
          },
          {
            "name": "Macedonia, The Former Yugoslav Republic of",
            "code": "MK"
          },
          {
            "name": "Madagascar",
            "code": "MG"
          },
          {
            "name": "Malawi",
            "code": "MW"
          },
          {
            "name": "Malaysia",
            "code": "MY"
          },
          {
            "name": "Maldives",
            "code": "MV"
          },
          {
            "name": "Mali",
            "code": "ML"
          },
          {
            "name": "Malta",
            "code": "MT"
          },
          {
            "name": "Marshall Islands",
            "code": "MH"
          },
          {
            "name": "Martinique",
            "code": "MQ"
          },
          {
            "name": "Mauritania",
            "code": "MR"
          },
          {
            "name": "Mauritius",
            "code": "MU"
          },
          {
            "name": "Mayotte",
            "code": "YT"
          },
          {
            "name": "Mexico",
            "code": "MX"
          },
          {
            "name": "Micronesia, Federated States of",
            "code": "FM"
          },
          {
            "name": "Moldova, Republic of",
            "code": "MD"
          },
          {
            "name": "Monaco",
            "code": "MC"
          },
          {
            "name": "Mongolia",
            "code": "MN"
          },
          {
            "name": "Montserrat",
            "code": "MS"
          },
          {
            "name": "Morocco",
            "code": "MA"
          },
          {
            "name": "Mozambique",
            "code": "MZ"
          },
          {
            "name": "Myanmar",
            "code": "MM"
          },
          {
            "name": "Namibia",
            "code": "NA"
          },
          {
            "name": "Nauru",
            "code": "NR"
          },
          {
            "name": "Nepal",
            "code": "NP"
          },
          {
            "name": "Netherlands",
            "code": "NL"
          },
          {
            "name": "Netherlands Antilles",
            "code": "AN"
          },
          {
            "name": "New Caledonia",
            "code": "NC"
          },
          {
            "name": "New Zealand",
            "code": "NZ"
          },
          {
            "name": "Nicaragua",
            "code": "NI"
          },
          {
            "name": "Niger",
            "code": "NE"
          },
          {
            "name": "Nigeria",
            "code": "NG"
          },
          {
            "name": "Niue",
            "code": "NU"
          },
          {
            "name": "Norfolk Island",
            "code": "NF"
          },
          {
            "name": "Northern Mariana Islands",
            "code": "MP"
          },
          {
            "name": "Norway",
            "code": "NO"
          },
          {
            "name": "Oman",
            "code": "OM"
          },
          {
            "name": "Pakistan",
            "code": "PK"
          },
          {
            "name": "Palau",
            "code": "PW"
          },
          {
            "name": "Palestinian Territory, Occupied",
            "code": "PS"
          },
          {
            "name": "Panama",
            "code": "PA"
          },
          {
            "name": "Papua New Guinea",
            "code": "PG"
          },
          {
            "name": "Paraguay",
            "code": "PY"
          },
          {
            "name": "Peru",
            "code": "PE"
          },
          {
            "name": "Philippines",
            "code": "PH"
          },
          {
            "name": "Pitcairn",
            "code": "PN"
          },
          {
            "name": "Poland",
            "code": "PL"
          },
          {
            "name": "Portugal",
            "code": "PT"
          },
          {
            "name": "Puerto Rico",
            "code": "PR"
          },
          {
            "name": "Qatar",
            "code": "QA"
          },
          {
            "name": "Reunion",
            "code": "RE"
          },
          {
            "name": "Romania",
            "code": "RO"
          },
          {
            "name": "Russian Federation",
            "code": "RU"
          },
          {
            "name": "RWANDA",
            "code": "RW"
          },
          {
            "name": "Saint Helena",
            "code": "SH"
          },
          {
            "name": "Saint Kitts and Nevis",
            "code": "KN"
          },
          {
            "name": "Saint Lucia",
            "code": "LC"
          },
          {
            "name": "Saint Pierre and Miquelon",
            "code": "PM"
          },
          {
            "name": "Saint Vincent and the Grenadines",
            "code": "VC"
          },
          {
            "name": "Samoa",
            "code": "WS"
          },
          {
            "name": "San Marino",
            "code": "SM"
          },
          {
            "name": "Sao Tome and Principe",
            "code": "ST"
          },
          {
            "name": "Saudi Arabia",
            "code": "SA"
          },
          {
            "name": "Senegal",
            "code": "SN"
          },
          {
            "name": "Serbia and Montenegro",
            "code": "CS"
          },
          {
            "name": "Seychelles",
            "code": "SC"
          },
          {
            "name": "Sierra Leone",
            "code": "SL"
          },
          {
            "name": "Singapore",
            "code": "SG"
          },
          {
            "name": "Slovakia",
            "code": "SK"
          },
          {
            "name": "Slovenia",
            "code": "SI"
          },
          {
            "name": "Solomon Islands",
            "code": "SB"
          },
          {
            "name": "Somalia",
            "code": "SO"
          },
          {
            "name": "South Africa",
            "code": "ZA"
          },
          {
            "name": "South Georgia and the South Sandwich Islands",
            "code": "GS"
          },
          {
            "name": "Spain",
            "code": "ES"
          },
          {
            "name": "Sri Lanka",
            "code": "LK"
          },
          {
            "name": "Sudan",
            "code": "SD"
          },
          {
            "name": "Suriname",
            "code": "SR"
          },
          {
            "name": "Svalbard and Jan Mayen",
            "code": "SJ"
          },
          {
            "name": "Swaziland",
            "code": "SZ"
          },
          {
            "name": "Sweden",
            "code": "SE"
          },
          {
            "name": "Switzerland",
            "code": "CH"
          },
          {
            "name": "Syrian Arab Republic",
            "code": "SY"
          },
          {
            "name": "Taiwan, Province of China",
            "code": "TW"
          },
          {
            "name": "Tajikistan",
            "code": "TJ"
          },
          {
            "name": "Tanzania, United Republic of",
            "code": "TZ"
          },
          {
            "name": "Thailand",
            "code": "TH"
          },
          {
            "name": "Timor-Leste",
            "code": "TL"
          },
          {
            "name": "Togo",
            "code": "TG"
          },
          {
            "name": "Tokelau",
            "code": "TK"
          },
          {
            "name": "Tonga",
            "code": "TO"
          },
          {
            "name": "Trinidad and Tobago",
            "code": "TT"
          },
          {
            "name": "Tunisia",
            "code": "TN"
          },
          {
            "name": "Turkey",
            "code": "TR"
          },
          {
            "name": "Turkmenistan",
            "code": "TM"
          },
          {
            "name": "Turks and Caicos Islands",
            "code": "TC"
          },
          {
            "name": "Tuvalu",
            "code": "TV"
          },
          {
            "name": "Uganda",
            "code": "UG"
          },
          {
            "name": "Ukraine",
            "code": "UA"
          },
          {
            "name": "United Arab Emirates",
            "code": "AE"
          },
          {
            "name": "United Kingdom",
            "code": "GB"
          },
          {
            "name": "United States",
            "code": "US"
          },
          {
            "name": "United States Minor Outlying Islands",
            "code": "UM"
          },
          {
            "name": "Uruguay",
            "code": "UY"
          },
          {
            "name": "Uzbekistan",
            "code": "UZ"
          },
          {
            "name": "Vanuatu",
            "code": "VU"
          },
          {
            "name": "Venezuela",
            "code": "VE"
          },
          {
            "name": "Viet Nam",
            "code": "VN"
          },
          {
            "name": "Virgin Islands, British",
            "code": "VG"
          },
          {
            "name": "Virgin Islands, U.S.",
            "code": "VI"
          },
          {
            "name": "Wallis and Futuna",
            "code": "WF"
          },
          {
            "name": "Western Sahara",
            "code": "EH"
          },
          {
            "name": "Yemen",
            "code": "YE"
          },
          {
            "name": "Zambia",
            "code": "ZM"
          },
          {
            "name": "Zimbabwe",
            "code": "ZW"
          }
          ];
        }
      }
    })
