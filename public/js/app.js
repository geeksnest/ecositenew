'use strict';
// Declare app level module which depends on filters, and services

var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.filters',
    'app.services',
    'app.directives',
    'app.controllers',
    'angularFileUpload',
    'ngRoute',
    'checklist-model',
    'xeditable',
    'ui.select',
    'ngSanitize',
    'ngFileUpload',
    'localytics.directives',
    'angularUtils.directives.dirPagination',
    'colorpicker.module',
    'timer',
    'ui.sortable'
  ])

.run(['$rootScope', '$state', '$stateParams', 'editableOptions', function ($rootScope,   $state,   $stateParams , editableOptions) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;   
    editableOptions.theme = 'bs3';      
}])
.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' ,
    function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider ) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $urlRouterProvider
            .otherwise('/dashboard');
        $stateProvider     
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/ecoadmin/admin/dashboard',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/dashboard/dashboard.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('editprofile', {
                url: '/editprofile/:userid',
                controller: 'editprofileCtrl',
                templateUrl: '/ecoadmin/admin/editprofile'
            })
            .state('userscreate', {
                url: '/userscreate',
                controller: 'CreateUserCtrl',
                templateUrl: '/ecoadmin/users/create',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/users/createuser.js',
                            '/js/config/config.js',
                            '/js/jquery/jstz.js',
                            '/js/angular/moment.js',
                            '/js/angular/moment-timezone-all-years.js'
                            ]);
                    }]
                }
            })
            .state('userlist', {
                url: '/userlist',
                controller: 'UserlistCtrl',
                templateUrl: '/ecoadmin/users/list',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/users/userlist.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('edituser', {
                url: '/edituser/:userid',
                controller: 'EditUserCtrl',
                templateUrl: '/ecoadmin/users/edituser',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/users/edituser.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('members', {
                url: '/members',
                controller: 'MembersCtrl',
                templateUrl: '/ecoadmin/users/members',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/users/members.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('vendorseattle', {
                url: '/vendorseattle',
                controller: 'VendorseattleCtrl',
                templateUrl: '/ecoadmin/vendor/vendorseattle',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/vendor/vendorseattle.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('donationlist', {
                url: '/donationlist',
                controller: 'DonationsCtrl',
                templateUrl: '/ecoadmin/donation/donationlist',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/donation/donationlist.js'
                            ]);
                    }]
                }
            })
            .state('donationothers', {
                url: '/donationothers',
                controller: 'DonationsOthersCtrl',
                templateUrl: '/ecoadmin/donation/donationothers',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/donation/donationothers.js'
                            ]);
                    }]
                }
            })
            .state('nyearthcitizenwalk', {
                url: '/nyearthcitizenwalk',
                controller: 'NYearthcitizenwalkCtrl',
                templateUrl: '/ecoadmin/donation/nyearthcitizenwalk',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/donation/nyearthcitizenwalk.js'
                            ]);
                    }]
                }
            })
            .state('houstonearthcitizenswalk', {
                url: '/houstonearthcitizenswalk',
                controller: 'DonationshoustonCtrl',
                templateUrl: '/ecoadmin/donation/houstonearthcitizenswalk',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/donation/houstonearthcitizenswalk.js'
                            ]);
                    }]
                }
            })
            .state('seattlenaturalhealingexpo', {
                url: '/seattlenaturalhealingexpo',
                controller: 'DonationsseattleCtrl',
                templateUrl: '/ecoadmin/donation/seattlenaturalhealingexpo',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/donation/seattlenaturalhealingexpo.js'
                            ]);
                    }]
                }
            })
            .state('bnbregistration', {
                url: '/bnbregistration',
                controller: 'BNBregistrationCtrl',
                templateUrl: '/ecoadmin/donation/bnbregistration'
            })

            //Programs
            .state('heroesleadershiptraining', {
                url: '/heroesleadershiptraining',
                controller: 'hltCtrl',
                templateUrl: '/ecoadmin/programs/heroesleadershiptraining',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/programs/heroesleadershiptraining.js'
                            ]);
                    }]
                }
            })

            //Projects
            .state('projectslist', {
                url: '/projectslist',
                controller: 'projectslistCtrl',
                templateUrl: '/ecoadmin/projects/projectslist',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/projects/projectslist.js'
                            ]);
                    }]
                }
            })

            //medialibrary
            .state('medialibrarylist', {
                url: '/medialibrarylist',
                controller: 'medialibrarylistCtrl',
                templateUrl: '/ecoadmin/medialibrary/medialibrarylist',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/medialibrary/medialibrarylist.js'
                            ]);
                    }]
                }
            })


            //CLub
            .state('clublist', {
                url: '/clublist',
                controller: 'clublistCtrl',
                templateUrl: '/ecoadmin/club/clublist',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/club/clublist.js'
                            ]);
                    }]
                }
            })

            //Homepage = Create : rj
            .state('createhomepage', {
                url: '/createhomepage',
                controller: 'CreatehomepageCtrl',
                templateUrl: '/ecoadmin/homepage/createhomepage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/homepage/createhomepage.js'
                            ]);
                    }]
                }
            })

             //Homepage = Manage : rj
            .state('managehomepage', {
                url: '/managehomepage',
                controller: 'ManagehomepageCtrl',
                templateUrl: '/ecoadmin/homepage/managehomepage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/homepage/managehomepage.js'
                            ]);
                    }]
                }
            })

             //Homepage = Edt : rj
            .state('edithomepage', {
                url: '/edithomepage/:contentid',
                controller: 'EdithomepageCtrl',
                templateUrl: '/ecoadmin/homepage/edithomepage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/homepage/edithomepage.js'
                            ]);
                    }]
                }
            })

            .state('toppage', {
                url: '/toppage',
                controller: 'ToppageCtrl',
                templateUrl: '/ecoadmin/homepage/toppage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/homepage/toppage.js'
                            ]);
                    }]
                }
            })

            //MENU
            .state('menu_creator', {
                url: '/menu_creator',
                controller: 'menuCreatorCtrl',
                templateUrl: '/ecoadmin/menu/menu_creator',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/controllers/menu/menu_creator.js',
                            '/js/be/factory/menu/menu_creator.js',
                            '/js/jquery/nestable/jquery.nestable.js'
                            ]);
                    }]
                }
            })

            .state('projectdonations', {
                url: '/projectdonations/:projID',
                controller: 'projectdonationsCtrl',
                templateUrl: '/ecoadmin/projects/projectdonations',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/projects/projectdonations.js'
                            ]);
                    }]
                }
            })


            .state('peacemarks', {
                url: '/peacemarks/:pageid',
                controller: 'GoogleMapCtrl',
                templateUrl: '/ecoadmin/peacemark/create',
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['/js/app/map/load-google-maps.js',
                            '/js/modules/ui-map.js',
                            '/js/app/map/map.js']).then(function () {
                                return loadGoogleMaps();
                            });
                        }]
                    }
                })
            .state('editpeacemap', {
                url: '/peacemarks/edit/:id',
                templateUrl: '/ecoadmin/peacemark/edit',
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['/js/app/map/load-google-maps.js',
                            '/js/modules/ui-map.js',
                            '/js/app/map/map.js']).then(function () {
                                return loadGoogleMaps();
                            });
                        }]
                    }
                })
            .state('managepeacemarks', {
                url: '/managepeacemarks',
                controller: 'GoogleMapCtrl',
                templateUrl: '/ecoadmin/peacemark/managepeacemarks',
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['/js/app/map/load-google-maps.js',
                            '/js/modules/ui-map.js',
                            '/js/app/map/map.js']).then(function () {
                                return loadGoogleMaps();
                            });
                        }]
                    }
                })
            //KYBEN CALL HEROES
             .state('callheroes', {
                url: '/callheroes',
                templateUrl: '/ecoadmin/callheroes/callingheroes',
                controller: 'CallingheroesCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/callingheroes/callingheroes.js',
                            '/js/be/callingheroes/datefrom.js',
                            '/js/be/callingheroes/dateto.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('heroesonduty', {
                url: '/heroesonduty',
                templateUrl: '/ecoadmin/callheroes/heroesonduty',
                controller: 'HeroesonDutyCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/callingheroes/heroesonduty.js',
                            '/js/be/callingheroes/datefrom.js',
                            '/js/be/callingheroes/dateto.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('atw', {
                url: '/atw',
                templateUrl: '/ecoadmin/atw/atw',
                controller: 'AtwCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/atw/atw.js'
                            ]);
                    }]
                }
            })

            .state('friends-and-allies', {
                url: '/friends-and-allies',
                templateUrl: '/ecoadmin/atw/fa',
                controller: 'FaCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/atw/atw.js'
                            ]);
                    }]
                }
            })




            //rainier state

            .state('createpage', {
                url: '/createpage',
                controller: 'CreatePageCtrl',
                templateUrl: '/ecoadmin/pages/createpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/page/createpage.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('managepages', {
                url: '/managepages',
                controller: 'ManagePagesCtrl',
                templateUrl: '/ecoadmin/pages/managepages',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/page/managepages.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('editpage', {
                url: '/editpage/:pageid',
                controller: 'editPageCtrl',
                templateUrl: '/ecoadmin/pages/editpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/page/editpage.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('otherpages', {
                url: '/otherpages',
                controller: 'otherpagesCtrl',
                templateUrl: '/ecoadmin/pages/otherpages',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/page/otherpages.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('createnews', {
                url: '/createnews',
                controller: 'CreateNewsCtrl',
                templateUrl: '/ecoadmin/news/createnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/createnews.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('managenews', {
                url: '/managenews',
                controller: 'ManageNewsCtrl',
                templateUrl: '/ecoadmin/news/managenews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/managenews.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'editNewsCtrl',
                templateUrl: '/ecoadmin/news/editnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/editnews.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('category', {
                url: '/category/',
                controller: 'categoryNewsCtrl',
                templateUrl: '/ecoadmin/news/category',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/category.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('createauthor', {
                url: '/createauthor/',
                controller: 'Createauthor',
                templateUrl: '/ecoadmin/news/createauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/createauthor.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('manageauthor', {
                url: '/manageauthor/',
                controller: 'Manageauthor',
                templateUrl: '/ecoadmin/news/manageauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/manageauthor.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('editauthor', {
                url: '/editauthor/:authorid',
                controller: 'Editauthor',
                templateUrl: '/ecoadmin/news/editauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/editauthor.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('newstags', {
                url: '/newstags/',
                controller: 'Tags',
                templateUrl: '/ecoadmin/news/newstags',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/news/newstags.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('createnewsletter', {
                url: '/createnewsletter',
                controller: 'CreateNewsLetterCtrl',
                templateUrl: '/ecoadmin/newsletter/createnewsletter'
            })
            .state('managenewsletter', {
                url: '/managenewsletter',
                controller: 'manageNewsLetterCtrl',
                templateUrl: '/ecoadmin/newsletter/managenewsletter'
            })
            .state('editnewsletter', {
                url: '/editnewsletter/:newsletterid',
                controller: 'editNewsletterCtrl',
                templateUrl: '/ecoadmin/newsletter/editnewsletter'
            })
            .state('sendnewsletter', {
                url: '/sendnewsletter/:newsletterid',
                controller: 'sendNewsletterCtrl',
                templateUrl: '/ecoadmin/newsletter/sendnewsletter'
            })

            //end rainier state

            //jimmy state

            .state('addcalendar', {
                url: '/addcalendar',
                controller: 'addcalendarCtrl',
                templateUrl: '/ecoadmin/calendar/addcalendar'
            })

            .state('viewcalendar', {
                url: '/viewcalendar',
                controller: 'viewcalendarCtrl',
                templateUrl: '/ecoadmin/calendar/viewcalendar',
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['/js/jquery/fullcalendar/fullcalendar.css',
                            '/js/jquery/jquery-ui-1.10.3.custom.min.js',
                            '/js/jquery/fullcalendar/fullcalendar.min.js',
                            '/js/modules/ui-calendar.js']);
                    }]
                }
            })
            .state('editcalendar', {
                url: '/editcalendar/:pageid',
                controller: 'editcalendarCtrl',
                templateUrl: '/ecoadmin/calendar/editcalendar'
            })

            .state('createfeaturedproject', {
                url: '/createfeaturedproject',
                controller: 'CreatefeaturedprojectCtrl',
                templateUrl: '/ecoadmin/featuredprojects/createfeaturedproject'
            })

            .state('managefeaturedproject', {
                url: '/managefeaturedproject',
                controller: 'ManagefeaturedprojectCtrl',
                templateUrl: '/ecoadmin/featuredprojects/managefeaturedproject'
            })
            .state('editfeaturedproject', {
                url: '/editfeaturedproject/:pageid',
                controller: 'editfeatureCtrl',
                templateUrl: '/ecoadmin/featuredprojects/editfeaturedproject'
            })

            .state('addsubscriber', {
                url: '/addsubscriber',
                controller: 'AddsubscriberCtrl',
                templateUrl: '/ecoadmin/subscribers/addsubscriber'
            })

            .state('subscriberslist', {
                url: '/subscriberslist',
                controller: 'SubscribersListCtrl',
                templateUrl: '/ecoadmin/subscribers/subscriberslist'
            })

            .state('editsubscriber', {
                url: '/editsubscriber/:pageid',
                controller: 'EditsubscriberCtrl',
                templateUrl: '/ecoadmin/subscribers/editsubscriber'
            })

            //end jimmy state

            //uson
            .state('createproposals', {
                url: '/createproposals',
                controller: 'ProposalsCtrl',
                templateUrl: '/ecoadmin/proposals/createproposals'
            })

            .state('manageproposals', {
                url: '/manageproposals',
                controller: 'manageproposalsCtrl',
                templateUrl: '/ecoadmin/proposals/manageproposals'
            })

            .state('managesettings', {
                url: '/managesettings',
                controller: 'settingsCtrl',
                templateUrl: '/ecoadmin/settings/managesettings'
            })

            //end uson



            .state('sliderupload', {
                url: '/sliderupload',
                controller: 'SlideruploadCtrl',
                templateUrl: '/ecoadmin/sliderimage/sliderupload'

            })

            .state('createevent', {
                url: '/createevent',
                controller: 'createeventCtrl',
                templateUrl: '/ecoadmin/eventmanager/createevent',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/eventmanager/createevent.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }

            })

            .state('eventlist', {
                url: '/eventlist',
                controller: 'eventlistCtrl',
                templateUrl: '/ecoadmin/eventmanager/eventlist',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/eventmanager/eventlist.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }

            })

            .state('eventcontents', {
                url: '/eventcontents/:eventID',
                controller: 'eventcontentsCtrl',
                templateUrl: '/ecoadmin/eventmanager/eventcontents',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/eventmanager/eventcontents.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('eventscollection', {
                url: '/eventscollection/:eventID',
                controller: 'eventscollectionCtrl',
                templateUrl: '/ecoadmin/eventmanager/eventscollection',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/eventmanager/eventscollection.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            //SLIDER HERE
            .state('slider', {
                url: '/slider',
                controller: 'createSliderCtrl',
                templateUrl: '/ecoadmin/images/slider',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/images/slider.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })            
            .state('editslider', {
                url: '/editslider/:id',
                controller: 'editSliderCtrl',
                templateUrl: '/ecoadmin/images/editslider',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/images/editslider.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }

            })

            //BANNER HERE
            .state('banner', {
                url: '/banner',
                controller: 'bannerCtrl',
                templateUrl: '/ecoadmin/images/banner',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/images/banner.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })
            .state('editbanner', {
                url: '/editbanner/:id',
                controller: 'editbannerCtrl',
                templateUrl: '/ecoadmin/images/editbanner',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/be/images/editbanner.js',
                            '/js/config/config.js'
                            ]);
                    }]
                }
            })

            .state('testimonials', {
                url: '/testimonials',
                controller: 'testimonialsCtrl',
                templateUrl: '/ecoadmin/testimonials/readtestimonial'

            })
            .state('edit', {
                url: '/edit/:id',
                controller: 'edittestimonialCtrl',
                templateUrl: '/ecoadmin/testimonials/edit'

            })
            .state('menucreator', {
                url: '/menucreator',
                controller: 'menucreatorCtrl',
                templateUrl: '/ecoadmin/menucreator/createMenu'

            })
            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////            

            .state('app.ui.grid', {
                url: '/grid',
                templateUrl: 'tpl/ui_grid.html'
            })
            .state('app.ui.bootstrap', {
                url: '/bootstrap',
                templateUrl: 'tpl/ui_bootstrap.html'
            })
            .state('app.ui.sortable', {
                url: '/sortable',
                templateUrl: 'tpl/ui_sortable.html'
            })
            .state('app.ui.portlet', {
                url: '/portlet',
                templateUrl: 'tpl/ui_portlet.html'
            })
            .state('app.ui.timeline', {
                url: '/timeline',
                templateUrl: 'tpl/ui_timeline.html'
            })
            .state('app.ui.jvectormap', {
                url: '/jvectormap',
                templateUrl: 'tpl/ui_jvectormap.html'
            })
            .state('app.ui.googlemap', {
                url: '/googlemap',
                templateUrl: 'tpl/ui_googlemap.html',
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['js/app/map/load-google-maps.js',
                            'js/modules/ui-map.js',
                            'js/app/map/map.js']).then(function () {
                                return loadGoogleMaps();
                            });
                        }]
                    }
                })
            .state('app.widgets', {
                url: '/widgets',
                templateUrl: 'tpl/ui_widgets.html'
            })
            .state('app.chart', {
                url: '/chart',
                templateUrl: 'tpl/ui_chart.html'
            })
            // table
            .state('app.table', {
                url: '/table',
                template: '<div ui-view></div>'
            })
            .state('app.table.static', {
                url: '/static',
                templateUrl: 'tpl/table_static.html'
            })
            .state('app.table.datatable', {
                url: '/datatable',
                templateUrl: 'tpl/table_datatable.html'
            })
            .state('app.table.footable', {
                url: '/footable',
                templateUrl: 'tpl/table_footable.html'
            })
            // form
            .state('app.form', {
                url: '/form',
                template: '<div ui-view class="fade-in"></div>'
            })
            .state('app.form.elements', {
                url: '/elements',
                templateUrl: 'tpl/form_elements.html'
            })
            .state('app.form.validation', {
                url: '/validation',
                templateUrl: 'tpl/form_validation.html'
            })
            .state('app.form.wizard', {
                url: '/wizard',
                templateUrl: 'tpl/form_wizard.html'
            })
            // pages
            .state('app.page', {
                url: '/page',
                template: '<div ui-view class="fade-in-down"></div>'
            })
            .state('app.page.profile', {
                url: '/profile',
                templateUrl: 'tpl/page_profile.html'
            })
            .state('app.page.post', {
                url: '/post',
                templateUrl: 'tpl/page_post.html'
            })
            .state('app.page.search', {
                url: '/search',
                templateUrl: 'tpl/page_search.html'
            })
            .state('app.page.invoice', {
                url: '/invoice',
                templateUrl: 'tpl/page_invoice.html'
            })
            .state('app.page.price', {
                url: '/price',
                templateUrl: 'tpl/page_price.html'
            })
            .state('app.docs', {
                url: '/docs',
                templateUrl: 'tpl/docs.html'
            })
            // others
            .state('lockme', {
                url: '/lockme',
                templateUrl: 'tpl/page_lockme.html'
            })
            .state('access', {
                url: '/access',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('access.signin', {
                url: '/signin',
                templateUrl: 'tpl/page_signin.html'
            })
            .state('access.signup', {
                url: '/signup',
                templateUrl: 'tpl/page_signup.html'
            })
            .state('access.forgotpwd', {
                url: '/forgotpwd',
                templateUrl: 'tpl/page_forgotpwd.html'
            })
            .state('access.404', {
                url: '/404',
                templateUrl: 'tpl/page_404.html'
            })

            // fullCalendar
            .state('app.calendar', {
                url: '/calendar',
                templateUrl: 'tpl/app_calendar.html',
                // use resolve to load other dependences
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['js/jquery/fullcalendar/fullcalendar.css',
                            'js/jquery/jquery-ui-1.10.3.custom.min.js',
                            'js/jquery/fullcalendar/fullcalendar.min.js',
                            'js/modules/ui-calendar.js',
                            'js/app/calendar/calendar.js']);
                    }]
                }
            })

            // mail
            .state('app.mail', {
                abstract: true,
                url: '/mail',
                templateUrl: 'tpl/mail.html',
                // use resolve to load other dependences
                resolve: {
                    deps: ['uiLoad',
                    function (uiLoad) {
                        return uiLoad.load(['/js/app/mail/mail.js',
                            '/js/app/mail/mail-service.js',
                            '/js/libs/moment.min.js']);
                    }]
                }
            })
            .state('app.mail.list', {
                url: '/inbox/{fold}',
                templateUrl: 'tpl/mail.list.html'
            })
            .state('app.mail.detail', {
                url: '/{mailId:[0-9]{1,4}}',
                templateUrl: 'tpl/mail.detail.html'
            })
            .state('app.mail.compose', {
                url: '/compose',
                templateUrl: 'tpl/mail.new.html'
            })
        }
        ]
        )

        .config(['$translateProvider', function ($translateProvider) {

                // Register a loader for the static files
                // So, the module will search missing translation tables under the specified urls.
                // Those urls are [prefix][langKey][suffix].
                $translateProvider.useStaticFilesLoader({
                    prefix: '/js/jsons/',
                    suffix: '.json'
                });

                // Tell the module what language to use by default
                $translateProvider.preferredLanguage('en');

                // Tell the module to store the language in the local storage
                $translateProvider.useLocalStorage();

            }])

        /**
         * jQuery plugin config use ui-jq directive , config the js and css files that required
         * key: function name of the jQuery plugin
         * value: array of the css js file located
         */
        .constant('JQ_CONFIG', {
            easyPieChart: ['../js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
            sparkline: ['../js/jquery/charts/sparkline/jquery.sparkline.min.js'],
            plot: ['../js/jquery/charts/flot/jquery.flot.min.js',
                '../js/jquery/charts/flot/jquery.flot.resize.js',
                '../js/jquery/charts/flot/jquery.flot.tooltip.min.js',
                '../js/jquery/charts/flot/jquery.flot.spline.js',
                '../js/jquery/charts/flot/jquery.flot.orderBars.js',
                '../js/jquery/charts/flot/jquery.flot.pie.min.js'],
            slimScroll: ['../js/jquery/slimscroll/jquery.slimscroll.min.js'],
            sortable: ['../js/jquery/sortable/jquery.sortable.js'],
            nestable: ['../js/jquery/nestable/nestable.css'],
            filestyle: ['../js/jquery/file/bootstrap-filestyle.min.js'],
            slider: ['../js/jquery/slider/bootstrap-slider.js',
                '../js/jquery/slider/slider.css'],
            chosen: ['../js/jquery/chosen/chosen.jquery.min.js',
                '../js/jquery/chosen/chosen.css'],
            TouchSpin: ['../js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                '../js/jquery/spinner/jquery.bootstrap-touchspin.css'],
            wysiwyg: ['../js/jquery/wysiwyg/bootstrap-wysiwyg.js',
                '../js/jquery/wysiwyg/jquery.hotkeys.js'],
            dataTable: ['../js/jquery/datatables/jquery.dataTables.min.js',
                '../js/jquery/datatables/dataTables.bootstrap.js',
                '../js/jquery/datatables/dataTables.bootstrap.css'],
            vectorMap: ['../js/jquery/jvectormap/jquery-jvectormap.min.js',
                '../js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                '../js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                '../js/jquery/jvectormap/jquery-jvectormap.css'],
            footable: ['../js/jquery/footable/footable.all.min.js',
                '../js/jquery/footable/footable.core.css']
        }
        )


        
        .constant('API_URL', 'https://ecoapi.gotitgenius.com');

