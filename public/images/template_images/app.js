var app = angular.module('app', ['ui.validate','ui.calendar', 'ui.bootstrap', 'angularFileUpload'], function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
}).config(
        ['$interpolateProvider',
            function ($interpolateProvider) {
                $interpolateProvider.startSymbol('{[{');
                $interpolateProvider.endSymbol('}]}');
            }]);

app.controller("indexMainCtrl", function ($timeout, $scope, $http) {
    $timeout(function () {
        $('.emailconfirmation').hide();
    }, 10000);
    $scope.userscount = 0;
    $scope.donationcount = 0;
    var countUp = function () {
        $http.get(API_URL + '/members/userdonation').success(function (response) {
            $scope.userscount = response.user;
            
            $scope.donationcount = putcomma(response.donations);
            $scope.timeInMs += 5000;
            $timeout(countUp, 5000);

        });
    }
    countUp();
    $timeout(countUp, 5000);

    var putcomma = function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

});
app.controller("proposalUploadFileCtrl", function ($scope, $http) {
    console.log("adafdfgdsg");

$scope.submit = function (prop) {

            
            $scope.alerts = [];
            $scope.closeAlert = function(index) {
              $scope.alerts.splice(index, 1);
            };
            $scope.isSaving = true;
            $http({
              url: API_URL + "/proposals/create",
              method: "POST",
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(prop)
            }).success(function(data, status, headers, config) {
              $scope.isSaving = false;
              $scope.alertsmsg ='Your message was sent successfully. Thanks.';
              $scope.showalert = true;
              $scope.prop.name=null;
              $scope.prop.email=null;  
              $scope.prop.company=null;  
              $scope.prop.proposalbody=null;
            }).error(function(data, status, headers, config) {
              scope.alerts.push({
                type: 'danger',
                msg: 'Something went wrong please check your fields'
              });
            });
}

 $(function() {
                    'use strict';
                    var newArray = new Array();
                    $('#digitalAssets').fileupload({
                        url: API_URL + '/server/php/index.php',
                        dataType: 'json',
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                        previewMaxWidth: 100,
                        previewMaxHeight: 100,
                        previewCrop: true,
                        limitMultiFileUploads: 4,
                        done: function(e, data) {
                            $.each(data.result.files, function(index, file) {
                                $.get(API_URL + '/proposals/ajaxfileuploader/' + file.name + '/Sample description').done(function(data) {
                                    $('.digital-assets-gallery').prepend(data);
                                    console.log(data);
                                });
                            });
                            console.log(data);
                        },
                        progressall: function(e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#progress .progress-bar').css(
                                'width',
                                progress + '%'
                                );
                        }
                    }).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});

    


   

});





// console.log("THIS is a test");
app.controller("registrationCtrl", function ($scope, $http, Countries, MDY) {
    $scope.usernametaken = '';
    $scope.emailtaken = '';
    $scope.success = false;
    $scope.process = false;
    $scope.countries = Countries.list();
    $scope.dataCentername = '';
    $scope.day = MDY.day();
    $scope.month = MDY.month();
    $scope.year = MDY.year();
    $scope.passmin = false;
    // load centername to select item from centername table

    $http({
        url: API_URL + "/bnb/cnamelist",
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function (data) {
        $scope.dataCentername = data;
    }).error(function (data) {
        $scope.status = status;
    });
    var oriUser = angular.copy($scope.user);
    $scope.register = function (user) {
        console.log(user);

        if (user.password.length < 6) {
            $scope.passmin = true;
            return
        } else {
            $scope.process = true;
            $http.post(API_URL + '/members/registration', user).success(function (response) {
                if (response.hasOwnProperty('usernametaken') || response.hasOwnProperty('emailtaken')) {
                    $scope.usernametaken = response.usernametaken;
                    $scope.emailtaken = response.emailtaken;
                    $scope.process = false;
                    $scope.passmin = false;
                } else {
                    $scope.usernametaken = '';
                    $scope.emailtaken = '';
                    //$scope.passmin = false;
                    $scope.success = true;
                    //$scope.process = false;
                    $('html, body').animate({
                        scrollTop: $("#donat").offset().top
                    }, 2000);
                    // Enable overlay click-to-close
                    // Change overlay color and opacity
                    //$.modal('<div style="padding: 20px; width:300px; border: 1px solid grey; background-color: #fff">               Thank you for registering! Please check your email for the confirmation message then you can proceed logging in and make your donation. <a href="#" class="simplemodal-close" style="position: absolute; top: 5px; right: 10px;">X</a></div>',{
                    //    opacity:80,
                    //    overlayCss: {backgroundColor:"#fff"},
                    //    overlayClose:true
                    //});
                    $scope.user = angular.copy(oriUser);
                    $scope.form.$setPristine();
                }
                $scope.response = response;
            });
        }
    }
    console.log($scope.day);
    $scope.passmin = false;
    var oriUser = angular.copy($scope.user);
    $scope.register = function (user) {
        console.log(user);

        if (user.password.length < 6) {
            $scope.passmin = true;
            return
        } else {
            $scope.process = true;
            $http.post(API_URL + '/members/registration', user).success(function (response) {

                if (response.hasOwnProperty('usernametaken') || response.hasOwnProperty('emailtaken')) {
                    if (response.hasOwnProperty('usernametaken')) {
                        $scope.usernametaken = response.usernametaken;
                    } else {
                        $scope.usernametaken = '';
                    }
                    if (response.hasOwnProperty('emailtaken')) {
                        $scope.emailtaken = response.emailtaken;
                    } else {
                        $scope.emailtaken = '';
                    }
                    console.log($scope.usernametaken);
                    console.log($scope.emailtaken);
                    $scope.process = false;
                    $scope.passmin = false;
                } else {
                    $scope.usernametaken = '';
                    $scope.emailtaken = '';
                    //$scope.passmin = false;
                    $scope.success = true;
                    $scope.process = false;
                    // $('html, body').animate({
                    //     scrollTop: $("#donat").offset().top
                    // }, 2000);
                    // Enable overlay click-to-close
                    // Change overlay color and opacity
                    $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;text-align: center;"><img src="/images/template_images/ecologo1.png" max-width="100%" height="60px;" /> <h1 style="color:#000;font-size:30px;">Thank you for registering!</h1> <div style="margin-top:-10px;padding:20px;font-size:15px;">Please check your email for the confirmation message then you can proceed logging in and make your donation.</div> <a href="#" class="simplemodal-close" style="position: absolute; top: 15px; right: 30px; text-align:center;">Close</a></div>', {
                        opacity: 80,
                        overlayCss: {backgroundColor: "#fff"},
                        overlayClose: true
                    });
                    $scope.user = angular.copy(oriUser);
                    $scope.form.$setPristine();
                }
                $scope.response = response;
                console.log($scope.response);
            });
        }
    }
});

//NEW MEMBER
// console.log("THIS is a test");
app.controller("newmemberCtrl", function ($scope, $http, Countries, MDY) {
    $scope.usernametaken = '';
    $scope.emailtaken = '';
    $scope.emailtaken2 = '';
    $scope.temppass = '';
    $scope.success = false;
    $scope.process = false;
    $scope.countries = Countries.list();

    $scope.day = MDY.day();
    $scope.month = MDY.month();
    $scope.year = MDY.year();
    $scope.passmin = false;
    var oriUser = angular.copy($scope.user);
    $scope.register = function (user) {
        console.log(user);

        if (user.password.length < 6) {
            $scope.passmin = true;
            return
        } else {
            $scope.process = true;
            $http.post(API_URL + '/members/newmember', user).success(function (response) {
                if (response.hasOwnProperty('usernametaken')) {
                    $scope.usernametaken = response.usernametaken;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('emailtaken')) {
                    $scope.emailtaken = response.emailtaken;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('emailtaken2')) {
                    $scope.emailtaken = response.emailtaken2;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('temppass')) {
                    $scope.temppass = response.temppass;
                    $scope.process = false;
                    $scope.passmin = false;
                } else {
                    $scope.usernametaken = '';
                    $scope.emailtaken = '';
                    $scope.emailtaken2 = '';
                    $scope.temppass = '';
                    //$scope.passmin = false;
                    $scope.success = true;
                    //$scope.process = false;
                    $('html, body').animate({
                        scrollTop: $("#donat").offset().top
                    }, 2000);
                    // Enable overlay click-to-close
                    // Change overlay color and opacity
                    //$.modal('<div style="padding: 20px; width:300px; border: 1px solid grey; background-color: #fff">               Thank you for registering! Please check your email for the confirmation message then you can proceed logging in and make your donation. <a href="#" class="simplemodal-close" style="position: absolute; top: 5px; right: 10px;">X</a></div>',{
                    //    opacity:80,
                    //    overlayCss: {backgroundColor:"#fff"},
                    //    overlayClose:true
                    //});
                    $scope.user = angular.copy(oriUser);
                    $scope.form.$setPristine();
                }
                $scope.response = response;
            });
        }
    }
    console.log($scope.day);
    $scope.passmin = false;
    var oriUser = angular.copy($scope.user);
    $scope.register = function (user) {
        console.log(user);

        if (user.password.length < 6) {
            $scope.passmin = true;
            return
        } else {
            $scope.process = true;
            $http.post(API_URL + '/members/newmember', user).success(function (response) {
                if (response.hasOwnProperty('usernametaken')) {
                    $scope.usernametaken = response.usernametaken;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('emailtaken')) {
                    $scope.emailtaken = response.emailtaken;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('emailtaken2')) {
                    $scope.emailtaken2 = response.emailtaken2;
                    $scope.process = false;
                    $scope.passmin = false;
                } else if (response.hasOwnProperty('temppass')) {
                    $scope.temppass = response.temppass;
                    $scope.process = false;
                    $scope.passmin = false;
                } else {
                    $scope.usernametaken = '';
                    $scope.emailtaken = '';
                    $scope.emailtaken2 = '';
                    $scope.temppass = '';
                    //$scope.passmin = false;
                    $scope.success = true;
                    $scope.process = false;
                    // $('html, body').animate({
                    //     scrollTop: $("#donat").offset().top
                    // }, 2000);
                    // Enable overlay click-to-close
                    // Change overlay color and opacity
                    $.modal('<div style="padding: 10px 20px 20px 20px; width:500px; border: 1px solid grey; background-color: #fff;"><img src="/images/template_images/ecologo1.png" width="120px" height="60px;" /> <h1 style="color:#000;font-size:30px;">Thank you for registering!</h1> <div style="margin-top:-10px;padding:20px;font-size:15px;">You are now completely registered you can proceed logging in and make your donation. <a href="http://www.earthcitizens.org/"  style="position: absolute; top: 15px; right: 30px; text-align:center;">Click here to login</a></div></div>', {
                        opacity: 80,
                        overlayCss: {backgroundColor: "#fff"},
                        overlayClose: true
                    });
                    $scope.user = angular.copy(oriUser);
                    $scope.form.$setPristine();
                }
                $scope.response = response;
                console.log($scope.response);
            });
        }
    }
});

app.controller("DonateCtrl", function ($scope, $http) {
    $scope.process = false;

    $scope.errormessage = '';
    $scope.donateamount = 10.00;
    $scope.paypalformshow = false;
    $scope.minimumdonation = '#999';
    $scope.clickDonate = function (data)
    {

        $scope.process = true;
        $http.post(API_URL + '/members/checkuser', data).success(function (response) {
            if (response.hasOwnProperty('error')) {
                $scope.errormessage = response.error;
            } else {
                $scope.errormessage = '';
                $scope.paypalformshow = true;
                $scope.paypalformshow1 = true;
                $scope.membername = response.success;
                $scope.statusmessage = '';
                $scope.forget = true;
                $scope.create = true;
                $scope.process = false;

            }
            $scope.process = false;
        });
    };
    $scope.change = function (amount) {
        if (amount <= 10) {
            $scope.minimumdonation = 'red';
        } else {
            $scope.errormessage = '';
            $scope.paypalformshow = true;
            $scope.membername = response.success;
        }
        $scope.process = false;
    };
    $scope.change = function (amount) {
        if (amount <= 10) {
            $scope.minimumdonation = 'red';
        } else {
            $scope.minimumdonation = '#999';
        }
    };
    $scope.emailPurpose = null;
    $scope.showemail = false;
    $scope.emailSent = false;
    $scope.statusmessage = '';
    $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        console.log($scope.showemail);

    }
    $scope.paypalformshow1 = false;
    $scope.create = false;
    $scope.forget = false;
    $scope.statusmessage = '';
    $scope.disp = false;
    $scope.setEmailFunc = function (purpose) {
        $scope.statusmessage = '';
        $scope.emailPurpose = purpose;
        $scope.showemail = true;
        $scope.paypalformshow1 = true;

    }
    $scope.sendemail = function (data) {
        console.log(data);
        data.type = $scope.emailPurpose;
        $http.post(API_URL + '/members/sendmail', data).success(function (response) {
            if (response.hasOwnProperty('error') || response.hasOwnProperty('error')) {
                $scope.statusmessage = response.error;
            } else {
                $scope.paypalformshow1 = false;
                $scope.statusmessage = 'Your requested details have been sent to your email!';
                $scope.errormessage = '';
            }
            $scope.emailPurpose = null;
            $scope.showemail = false;
            $scope.emailSent = false;
        });
    }
    $scope.hideForm = function () {
        $scope.emailPurpose = null;
        $scope.showemail = false;
        $scope.emailSent = false;
        $scope.paypalformshow1 = false;
    }
});

app.controller("TestimonialCtrl", function ($scope, $http) {

    console.log('Inside TestimonialCtrl');

    $scope.testi = '';
    $scope.manen = '';
    var oriUser = angular.copy($scope.user);
    $scope.submit = function (user) {
        console.log(user);
        $http.post(API_URL + '/utility/submit', user).success(function (response) {
            if(response==""){
                $scope.isSaving = false;
                $scope.manen = response.manen;
                $scope.testi = '';
            }else{
                $scope.isSaving = false;
                $scope.testi = response.testi;
                $scope.manen = '';
                $scope.user = angular.copy(oriUser);
                $scope.formtesti.$setPristine(true);
            }
        })
    }

    $scope.notendresult = true;
    $scope.showloading = false;
    $scope.endresult = false;
    var offset = 5;
    $scope.showmore = function (data) {
        $scope.showloading = true;
        $http.get(API_URL + '/utility/testimonial/' + offset).success(function (response) {
            var obj = JSON.parse(JSON.stringify(response));
            if (response) {
                for (var data in obj) {
                    $('#testimonials').append("<div class='media' style='background-color:#eee'><span class='pull-left thumb-sm'><img src='../public/img/a0.jpg' alt='...'></span><div class='media-body'><div class='pull-right text-center text-muted'></div><a href=' class='h4'>"
                        + obj[data].name + "</a><div class='block'><em class='text-xs'>Company/Organization: <span class='text-danger'>"
                        + obj[data].comporg + "</span></em><i class='fa fa-envelope fa-fw m-r-xs'></i>:"
                        + obj[data].comporg + "</div><blockquote><small class='block m-t-sm'>"
                        + obj[data].message + "</small></blockquote></div></div>");
                }
            }

            offset = offset + obj.length;
            $scope.showloading = false;
            if (obj.length < 5) {
                $scope.notendresult = false;
                $scope.endresult = true;
            } else {
                $scope.notendresult = true;
                $scope.endresult = false;
            }
        });
    }
});

app.controller("NewsCtrl", function ($scope, $http) {
    $scope.showloading = false;
    $scope.notendresult = true;
    $scope.endresult = false;
    var offset = 5;
    $scope.getnews = function (data) {
        var pageid = data;
        $scope.showloading = true;
        $http.get(API_URL + '/news/news/' + offset).success(function (response) {
            var obj = JSON.parse(JSON.stringify(response));

            if (response) {
                for (var data in obj) {
                    var bodyreplace = obj[data].body.replace('/<img[^>]+\>/i', '');
                    var body = bodyreplace.substring(0, 275);
                    var datereplace = obj[data].date.replace('/<img[^>]+\>/i', '');
                    var dateposted = datereplace.substring(0, 10);
                    if(typeof variable_here === 'undefined'){
  
                    };

                    $('#newslist').append("<table style='margin-top:20px; margin-bottom:30px; width: 100%;'><tr><td><span><div class='fillmyimage'><img src='" 
                        + API_URL + "/images/sliderimages/" 
                        + obj[data].banner + "'></div></span><a href='../../../../news/index/" 
                        + obj[data].newsid + "/" 
                        + pageid + "'><b><u>" 
                        + obj[data].title + "</u></b></a><br><b style='color:#72bb4c'>(Date posted: " 
                        + dateposted + ")</b><br>" 
                        + body + " " + "<br><a href='../../../../news/index/" 
                        + obj[data].newsid + "/" + pageid + "'>Read more...</a></td></tr></table>");

                    console.log('were here');
                }
            }
            offset = offset + obj.length;
            $scope.showloading = false;
            console.log(offset);
            if (obj.length < 5)
            {
                console.log('end');
                $scope.notendresult = false;
                $scope.endresult = true;
            }
            else
            {
                console.log('nah');
                $scope.notendresult = true;
                $scope.endresult = false;
            }
        });
    }
});

// jimmy

// angular.module('calendarDemoApp', ['ui.calendar', 'ui.bootstrap']);
app.controller("CalenCtrl", function($scope, $http, $compile, uiCalendarConfig){

  console.log('wadya ak!! aneng-neng mo ak??');
  
  $scope.lisview1 = false;
  $scope.calview1 = true;

  // list view
  $scope.lisview = function(){
    $scope.lisview1 = true;
    $scope.calview1 = false;   
  }
  
  // calendar view
  $scope.calview = function(){
    $scope.calview1 = true;
    $scope.lisview1 = false;
  }

  // begin code here
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  /* event source that pulls from google.com */
  $scope.eventSource = {
    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
    className: 'gcal-event',           // an option!
    currentTimezone: 'America/Chicago' // an option!
  };  
  
  $scope.events = [];
  
  $http.get(API_URL + '/calendar/manageactivity').success(function(listview3){
    var data = JSON.parse(JSON.stringify(listview3));
    if(listview3){
      var arrevents = [];
      for(var d in data){
        if(data[d].datef == data[d].datet){
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            className: ['b-l b-2x b-success'], 
            location: data[d].loc, 
            info: data[d].info
          });
        }else{
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            end: new Date(data[d].tyea, data[d].tmon-1, data[d].tday, data[d].hhr, data[d].hmin),
            className: ['b-l b-2x b-info'], 
            location: data[d].loc, 
            info: data[d].info
          });
        }
      }
    }

  /* config object */
  $scope.uiConfig = {
    calendar:{
      height: 450,
      editable: false,
      header:{
        left: 'title',
        center: '',
        right: 'today, prev, next'
      },
      eventClick: $scope.alertOnEventClick,
      eventDrop: $scope.alertOnDrop,
      eventResize: $scope.alertOnResize,
      eventRender: $scope.eventRender
    }
  };

  });

  /* Render Tooltip */
  $scope.eventRender = function( event, element, view ) { 
    element.attr({
      'tooltip': event.title,
      'tooltip-append-to-body': true,
    });
    $compile(element)($scope);
  };

  /* event sources array*/
  $scope.eventSources = [$scope.events];
  // end code here

});

app.controller("ProjikCtrl", function($scope, $http, Utils){
    console.log('Your inside the ProjikCtrl');
    $scope.showloading = false;
    $scope.notendresult = true;
    $scope.endresult = false;
    var offset = 5;

    $scope.getProjik = function (data) {
        $scope.showloading = true;
        $http.get(API_URL + '/featuredprojects/lispro/' + offset).success(function (response) {
            var obj = JSON.parse(JSON.stringify(response));
            console.log(response);
            if (response) {
                console.log('end');
                for (var data in obj) {
                    var bodyreplace = obj[data].feat_content.replace('/<img[^>]+\>/i', '');
                    var body = bodyreplace.substring(0, 180);
                    $('#projiklist').append("<table width='100%'><tr height='140'><td><div class='fillmyimage'><img src="
                    + API_URL + "/images/featurebanner/"
                    + obj[data].feat_picpath + "></div><b><u style=font-size:18px>"
                    + obj[data].feat_title + "</u></b><p style=color:#5386c1>Date Posted: "
                    + obj[data].feat_date + "</p>"
                    + body + "... <a href=/projects/viewer/"
                    + obj[data].feat_id + "/$pageslugs><span class='label bg-info'>Read More</span></a></td></tr></table>");
                }
            }

            offset = offset + obj.length;
            $scope.showloading = false;
            console.log(offset);
            if (obj.length < 5) {
                console.log('end');
                $scope.notendresult = false;
                $scope.endresult = true;
            } else {
                console.log('nah');
                $scope.notendresult = true;
                $scope.endresult = false;
            }
        });
    }
});

app.factory('Utils', function($q) {
    return {
        isImage: function(src) {
        
            var deferred = $q.defer();
        
            var image = new Image();
            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;
        
            return deferred.promise;
        }
    };
});

app.controller("NMSubcribersCtrl", function ($scope, $http) {
    $scope.blanko = '';
    $scope.emailtaken = '';
    $scope.emailtaken2 = '';
    $scope.emailregister = '';
    $scope.success = false;
    var oriUser = angular.copy($scope.nms);
    $scope.SaveNMS = function (nms) {
        console.log(nms);
        var mailtest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(nms.NMSemail);
        if (mailtest == false) {
            $scope.blanko = 'Ok';
            $scope.emailtaken = '';
            $scope.emailtaken2 = '';
            $scope.emailregister = '';
            return           
        } else {
            $scope.process = true;
            $http.post(API_URL + '/subscribers/addNMS', nms).success(function (response) {
                if (response.hasOwnProperty('emailtaken')) {
                    $scope.emailtaken = response.emailtaken;
                    $scope.emailtaken2 = '';
                    $scope.emailregister = '';
                    $scope.blanko = '';
                    $scope.success = false;
                    return
                } else if (response.hasOwnProperty('emailtaken2')) {
                    $scope.emailtaken2 = response.emailtaken2;
                    $scope.emailtaken = '';
                    $scope.emailregister = '';
                    $scope.blanko = '';
                    $scope.passmin = false;
                    return
                } else {
                    $scope.emailregister = response.emailregister;
                    $scope.emailtaken = '';
                    $scope.emailtaken2 = '';
                    $scope.blanko = '';
                    $scope.success = true;
                    //$scope.process = false;
                    $('html, body').animate({
                        scrollTop: $("#donat").offset().top
                    }, 2000);
                    $scope.nms = angular.copy(oriUser);
                    $scope.formnms.$setPristine(true);
                }
                $scope.response = response;
            });
        }
    }
});


app.controller("BnbCtrl", function ($scope, $http) {
    console.log('were here');
});



app.factory('MDY', function () {
    return {
        month: function () {
            return [
                {name: '', val: ''},
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ]
        },
        day: function () {
            var day = [];
            day.push({'val': ''})
            for (var x = 1; x <= 31; x++) {
                day.push({'val': x})
            }
            return day;
        },
        year: function () {
            var year = [];
            year.push({'val': ''})
            for (var y = 2013; y >= 1950; y--) {
                year.push({'val': y})
            }
            return year;
        }
    }
});
app.factory('Countries', function () {

    return {
        list: function () {
            return [
                {
                    "name": "Afghanistan",
                    "code": "AF"
                },
                {
                    "name": "Åland Islands",
                    "code": "AX"
                },
                {
                    "name": "Albania",
                    "code": "AL"
                },
                {
                    "name": "Algeria",
                    "code": "DZ"
                },
                {
                    "name": "American Samoa",
                    "code": "AS"
                },
                {
                    "name": "AndorrA",
                    "code": "AD"
                },
                {
                    "name": "Angola",
                    "code": "AO"
                },
                {
                    "name": "Anguilla",
                    "code": "AI"
                },
                {
                    "name": "Antarctica",
                    "code": "AQ"
                },
                {
                    "name": "Antigua and Barbuda",
                    "code": "AG"
                },
                {
                    "name": "Argentina",
                    "code": "AR"
                },
                {
                    "name": "Armenia",
                    "code": "AM"
                },
                {
                    "name": "Aruba",
                    "code": "AW"
                },
                {
                    "name": "Australia",
                    "code": "AU"
                },
                {
                    "name": "Austria",
                    "code": "AT"
                },
                {
                    "name": "Azerbaijan",
                    "code": "AZ"
                },
                {
                    "name": "Bahamas",
                    "code": "BS"
                },
                {
                    "name": "Bahrain",
                    "code": "BH"
                },
                {
                    "name": "Bangladesh",
                    "code": "BD"
                },
                {
                    "name": "Barbados",
                    "code": "BB"
                },
                {
                    "name": "Belarus",
                    "code": "BY"
                },
                {
                    "name": "Belgium",
                    "code": "BE"
                },
                {
                    "name": "Belize",
                    "code": "BZ"
                },
                {
                    "name": "Benin",
                    "code": "BJ"
                },
                {
                    "name": "Bermuda",
                    "code": "BM"
                },
                {
                    "name": "Bhutan",
                    "code": "BT"
                },
                {
                    "name": "Bolivia",
                    "code": "BO"
                },
                {
                    "name": "Bosnia and Herzegovina",
                    "code": "BA"
                },
                {
                    "name": "Botswana",
                    "code": "BW"
                },
                {
                    "name": "Bouvet Island",
                    "code": "BV"
                },
                {
                    "name": "Brazil",
                    "code": "BR"
                },
                {
                    "name": "British Indian Ocean Territory",
                    "code": "IO"
                },
                {
                    "name": "Brunei Darussalam",
                    "code": "BN"
                },
                {
                    "name": "Bulgaria",
                    "code": "BG"
                },
                {
                    "name": "Burkina Faso",
                    "code": "BF"
                },
                {
                    "name": "Burundi",
                    "code": "BI"
                },
                {
                    "name": "Cambodia",
                    "code": "KH"
                },
                {
                    "name": "Cameroon",
                    "code": "CM"
                },
                {
                    "name": "Canada",
                    "code": "CA"
                },
                {
                    "name": "Cape Verde",
                    "code": "CV"
                },
                {
                    "name": "Cayman Islands",
                    "code": "KY"
                },
                {
                    "name": "Central African Republic",
                    "code": "CF"
                },
                {
                    "name": "Chad",
                    "code": "TD"
                },
                {
                    "name": "Chile",
                    "code": "CL"
                },
                {
                    "name": "China",
                    "code": "CN"
                },
                {
                    "name": "Christmas Island",
                    "code": "CX"
                },
                {
                    "name": "Cocos (Keeling) Islands",
                    "code": "CC"
                },
                {
                    "name": "Colombia",
                    "code": "CO"
                },
                {
                    "name": "Comoros",
                    "code": "KM"
                },
                {
                    "name": "Congo",
                    "code": "CG"
                },
                {
                    "name": "Congo, The Democratic Republic of the",
                    "code": "CD"
                },
                {
                    "name": "Cook Islands",
                    "code": "CK"
                },
                {
                    "name": "Costa Rica",
                    "code": "CR"
                },
                {
                    "name": "Cote D\"Ivoire",
                    "code": "CI"
                },
                {
                    "name": "Croatia",
                    "code": "HR"
                },
                {
                    "name": "Cuba",
                    "code": "CU"
                },
                {
                    "name": "Cyprus",
                    "code": "CY"
                },
                {
                    "name": "Czech Republic",
                    "code": "CZ"
                },
                {
                    "name": "Denmark",
                    "code": "DK"
                },
                {
                    "name": "Djibouti",
                    "code": "DJ"
                },
                {
                    "name": "Dominica",
                    "code": "DM"
                },
                {
                    "name": "Dominican Republic",
                    "code": "DO"
                },
                {
                    "name": "Ecuador",
                    "code": "EC"
                },
                {
                    "name": "Egypt",
                    "code": "EG"
                },
                {
                    "name": "El Salvador",
                    "code": "SV"
                },
                {
                    "name": "Equatorial Guinea",
                    "code": "GQ"
                },
                {
                    "name": "Eritrea",
                    "code": "ER"
                },
                {
                    "name": "Estonia",
                    "code": "EE"
                },
                {
                    "name": "Ethiopia",
                    "code": "ET"
                },
                {
                    "name": "Falkland Islands (Malvinas)",
                    "code": "FK"
                },
                {
                    "name": "Faroe Islands",
                    "code": "FO"
                },
                {
                    "name": "Fiji",
                    "code": "FJ"
                },
                {
                    "name": "Finland",
                    "code": "FI"
                },
                {
                    "name": "France",
                    "code": "FR"
                },
                {
                    "name": "French Guiana",
                    "code": "GF"
                },
                {
                    "name": "French Polynesia",
                    "code": "PF"
                },
                {
                    "name": "French Southern Territories",
                    "code": "TF"
                },
                {
                    "name": "Gabon",
                    "code": "GA"
                },
                {
                    "name": "Gambia",
                    "code": "GM"
                },
                {
                    "name": "Georgia",
                    "code": "GE"
                },
                {
                    "name": "Germany",
                    "code": "DE"
                },
                {
                    "name": "Ghana",
                    "code": "GH"
                },
                {
                    "name": "Gibraltar",
                    "code": "GI"
                },
                {
                    "name": "Greece",
                    "code": "GR"
                },
                {
                    "name": "Greenland",
                    "code": "GL"
                },
                {
                    "name": "Grenada",
                    "code": "GD"
                },
                {
                    "name": "Guadeloupe",
                    "code": "GP"
                },
                {
                    "name": "Guam",
                    "code": "GU"
                },
                {
                    "name": "Guatemala",
                    "code": "GT"
                },
                {
                    "name": "Guernsey",
                    "code": "GG"
                },
                {
                    "name": "Guinea",
                    "code": "GN"
                },
                {
                    "name": "Guinea-Bissau",
                    "code": "GW"
                },
                {
                    "name": "Guyana",
                    "code": "GY"
                },
                {
                    "name": "Haiti",
                    "code": "HT"
                },
                {
                    "name": "Heard Island and Mcdonald Islands",
                    "code": "HM"
                },
                {
                    "name": "Holy See (Vatican City State)",
                    "code": "VA"
                },
                {
                    "name": "Honduras",
                    "code": "HN"
                },
                {
                    "name": "Hong Kong",
                    "code": "HK"
                },
                {
                    "name": "Hungary",
                    "code": "HU"
                },
                {
                    "name": "Iceland",
                    "code": "IS"
                },
                {
                    "name": "India",
                    "code": "IN"
                },
                {
                    "name": "Indonesia",
                    "code": "ID"
                },
                {
                    "name": "Iran, Islamic Republic Of",
                    "code": "IR"
                },
                {
                    "name": "Iraq",
                    "code": "IQ"
                },
                {
                    "name": "Ireland",
                    "code": "IE"
                },
                {
                    "name": "Isle of Man",
                    "code": "IM"
                },
                {
                    "name": "Israel",
                    "code": "IL"
                },
                {
                    "name": "Italy",
                    "code": "IT"
                },
                {
                    "name": "Jamaica",
                    "code": "JM"
                },
                {
                    "name": "Japan",
                    "code": "JP"
                },
                {
                    "name": "Jersey",
                    "code": "JE"
                },
                {
                    "name": "Jordan",
                    "code": "JO"
                },
                {
                    "name": "Kazakhstan",
                    "code": "KZ"
                },
                {
                    "name": "Kenya",
                    "code": "KE"
                },
                {
                    "name": "Kiribati",
                    "code": "KI"
                },
                {
                    "name": "Korea, Democratic People\"S Republic of",
                    "code": "KP"
                },
                {
                    "name": "Korea, Republic of",
                    "code": "KR"
                },
                {
                    "name": "Kuwait",
                    "code": "KW"
                },
                {
                    "name": "Kyrgyzstan",
                    "code": "KG"
                },
                {
                    "name": "Lao People\"S Democratic Republic",
                    "code": "LA"
                },
                {
                    "name": "Latvia",
                    "code": "LV"
                },
                {
                    "name": "Lebanon",
                    "code": "LB"
                },
                {
                    "name": "Lesotho",
                    "code": "LS"
                },
                {
                    "name": "Liberia",
                    "code": "LR"
                },
                {
                    "name": "Libyan Arab Jamahiriya",
                    "code": "LY"
                },
                {
                    "name": "Liechtenstein",
                    "code": "LI"
                },
                {
                    "name": "Lithuania",
                    "code": "LT"
                },
                {
                    "name": "Luxembourg",
                    "code": "LU"
                },
                {
                    "name": "Macao",
                    "code": "MO"
                },
                {
                    "name": "Macedonia, The Former Yugoslav Republic of",
                    "code": "MK"
                },
                {
                    "name": "Madagascar",
                    "code": "MG"
                },
                {
                    "name": "Malawi",
                    "code": "MW"
                },
                {
                    "name": "Malaysia",
                    "code": "MY"
                },
                {
                    "name": "Maldives",
                    "code": "MV"
                },
                {
                    "name": "Mali",
                    "code": "ML"
                },
                {
                    "name": "Malta",
                    "code": "MT"
                },
                {
                    "name": "Marshall Islands",
                    "code": "MH"
                },
                {
                    "name": "Martinique",
                    "code": "MQ"
                },
                {
                    "name": "Mauritania",
                    "code": "MR"
                },
                {
                    "name": "Mauritius",
                    "code": "MU"
                },
                {
                    "name": "Mayotte",
                    "code": "YT"
                },
                {
                    "name": "Mexico",
                    "code": "MX"
                },
                {
                    "name": "Micronesia, Federated States of",
                    "code": "FM"
                },
                {
                    "name": "Moldova, Republic of",
                    "code": "MD"
                },
                {
                    "name": "Monaco",
                    "code": "MC"
                },
                {
                    "name": "Mongolia",
                    "code": "MN"
                },
                {
                    "name": "Montserrat",
                    "code": "MS"
                },
                {
                    "name": "Morocco",
                    "code": "MA"
                },
                {
                    "name": "Mozambique",
                    "code": "MZ"
                },
                {
                    "name": "Myanmar",
                    "code": "MM"
                },
                {
                    "name": "Namibia",
                    "code": "NA"
                },
                {
                    "name": "Nauru",
                    "code": "NR"
                },
                {
                    "name": "Nepal",
                    "code": "NP"
                },
                {
                    "name": "Netherlands",
                    "code": "NL"
                },
                {
                    "name": "Netherlands Antilles",
                    "code": "AN"
                },
                {
                    "name": "New Caledonia",
                    "code": "NC"
                },
                {
                    "name": "New Zealand",
                    "code": "NZ"
                },
                {
                    "name": "Nicaragua",
                    "code": "NI"
                },
                {
                    "name": "Niger",
                    "code": "NE"
                },
                {
                    "name": "Nigeria",
                    "code": "NG"
                },
                {
                    "name": "Niue",
                    "code": "NU"
                },
                {
                    "name": "Norfolk Island",
                    "code": "NF"
                },
                {
                    "name": "Northern Mariana Islands",
                    "code": "MP"
                },
                {
                    "name": "Norway",
                    "code": "NO"
                },
                {
                    "name": "Oman",
                    "code": "OM"
                },
                {
                    "name": "Pakistan",
                    "code": "PK"
                },
                {
                    "name": "Palau",
                    "code": "PW"
                },
                {
                    "name": "Palestinian Territory, Occupied",
                    "code": "PS"
                },
                {
                    "name": "Panama",
                    "code": "PA"
                },
                {
                    "name": "Papua New Guinea",
                    "code": "PG"
                },
                {
                    "name": "Paraguay",
                    "code": "PY"
                },
                {
                    "name": "Peru",
                    "code": "PE"
                },
                {
                    "name": "Philippines",
                    "code": "PH"
                },
                {
                    "name": "Pitcairn",
                    "code": "PN"
                },
                {
                    "name": "Poland",
                    "code": "PL"
                },
                {
                    "name": "Portugal",
                    "code": "PT"
                },
                {
                    "name": "Puerto Rico",
                    "code": "PR"
                },
                {
                    "name": "Qatar",
                    "code": "QA"
                },
                {
                    "name": "Reunion",
                    "code": "RE"
                },
                {
                    "name": "Romania",
                    "code": "RO"
                },
                {
                    "name": "Russian Federation",
                    "code": "RU"
                },
                {
                    "name": "RWANDA",
                    "code": "RW"
                },
                {
                    "name": "Saint Helena",
                    "code": "SH"
                },
                {
                    "name": "Saint Kitts and Nevis",
                    "code": "KN"
                },
                {
                    "name": "Saint Lucia",
                    "code": "LC"
                },
                {
                    "name": "Saint Pierre and Miquelon",
                    "code": "PM"
                },
                {
                    "name": "Saint Vincent and the Grenadines",
                    "code": "VC"
                },
                {
                    "name": "Samoa",
                    "code": "WS"
                },
                {
                    "name": "San Marino",
                    "code": "SM"
                },
                {
                    "name": "Sao Tome and Principe",
                    "code": "ST"
                },
                {
                    "name": "Saudi Arabia",
                    "code": "SA"
                },
                {
                    "name": "Senegal",
                    "code": "SN"
                },
                {
                    "name": "Serbia and Montenegro",
                    "code": "CS"
                },
                {
                    "name": "Seychelles",
                    "code": "SC"
                },
                {
                    "name": "Sierra Leone",
                    "code": "SL"
                },
                {
                    "name": "Singapore",
                    "code": "SG"
                },
                {
                    "name": "Slovakia",
                    "code": "SK"
                },
                {
                    "name": "Slovenia",
                    "code": "SI"
                },
                {
                    "name": "Solomon Islands",
                    "code": "SB"
                },
                {
                    "name": "Somalia",
                    "code": "SO"
                },
                {
                    "name": "South Africa",
                    "code": "ZA"
                },
                {
                    "name": "South Georgia and the South Sandwich Islands",
                    "code": "GS"
                },
                {
                    "name": "Spain",
                    "code": "ES"
                },
                {
                    "name": "Sri Lanka",
                    "code": "LK"
                },
                {
                    "name": "Sudan",
                    "code": "SD"
                },
                {
                    "name": "Suriname",
                    "code": "SR"
                },
                {
                    "name": "Svalbard and Jan Mayen",
                    "code": "SJ"
                },
                {
                    "name": "Swaziland",
                    "code": "SZ"
                },
                {
                    "name": "Sweden",
                    "code": "SE"
                },
                {
                    "name": "Switzerland",
                    "code": "CH"
                },
                {
                    "name": "Syrian Arab Republic",
                    "code": "SY"
                },
                {
                    "name": "Taiwan, Province of China",
                    "code": "TW"
                },
                {
                    "name": "Tajikistan",
                    "code": "TJ"
                },
                {
                    "name": "Tanzania, United Republic of",
                    "code": "TZ"
                },
                {
                    "name": "Thailand",
                    "code": "TH"
                },
                {
                    "name": "Timor-Leste",
                    "code": "TL"
                },
                {
                    "name": "Togo",
                    "code": "TG"
                },
                {
                    "name": "Tokelau",
                    "code": "TK"
                },
                {
                    "name": "Tonga",
                    "code": "TO"
                },
                {
                    "name": "Trinidad and Tobago",
                    "code": "TT"
                },
                {
                    "name": "Tunisia",
                    "code": "TN"
                },
                {
                    "name": "Turkey",
                    "code": "TR"
                },
                {
                    "name": "Turkmenistan",
                    "code": "TM"
                },
                {
                    "name": "Turks and Caicos Islands",
                    "code": "TC"
                },
                {
                    "name": "Tuvalu",
                    "code": "TV"
                },
                {
                    "name": "Uganda",
                    "code": "UG"
                },
                {
                    "name": "Ukraine",
                    "code": "UA"
                },
                {
                    "name": "United Arab Emirates",
                    "code": "AE"
                },
                {
                    "name": "United Kingdom",
                    "code": "GB"
                },
                {
                    "name": "United States",
                    "code": "US"
                },
                {
                    "name": "United States Minor Outlying Islands",
                    "code": "UM"
                },
                {
                    "name": "Uruguay",
                    "code": "UY"
                },
                {
                    "name": "Uzbekistan",
                    "code": "UZ"
                },
                {
                    "name": "Vanuatu",
                    "code": "VU"
                },
                {
                    "name": "Venezuela",
                    "code": "VE"
                },
                {
                    "name": "Viet Nam",
                    "code": "VN"
                },
                {
                    "name": "Virgin Islands, British",
                    "code": "VG"
                },
                {
                    "name": "Virgin Islands, U.S.",
                    "code": "VI"
                },
                {
                    "name": "Wallis and Futuna",
                    "code": "WF"
                },
                {
                    "name": "Western Sahara",
                    "code": "EH"
                },
                {
                    "name": "Yemen",
                    "code": "YE"
                },
                {
                    "name": "Zambia",
                    "code": "ZM"
                },
                {
                    "name": "Zimbabwe",
                    "code": "ZW"
                }
            ];
        }
    }
})
        .constant('API_URL', 'http://ecoapi');

