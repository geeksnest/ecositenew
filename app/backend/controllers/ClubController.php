<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class ClubController extends \Phalcon\Mvc\Controller
{
    
    public function clublistAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);	
    }
}

