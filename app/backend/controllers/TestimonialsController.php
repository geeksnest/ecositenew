<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class TestimonialsController extends ControllerBase
{

    public function intialize(){

    }
    public function readtestimonialAction()
    {
       
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editAction()
    {
       
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
   
}

