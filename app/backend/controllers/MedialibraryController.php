<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class MedialibraryController extends \Phalcon\Mvc\Controller
{
    
    public function medialibrarylistAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);	
    }
}

