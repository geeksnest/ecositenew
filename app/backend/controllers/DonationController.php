<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;
use Modules\Backend\Models\Members as Members;
use Modules\Backend\Models\Donationlog as Donationlog;
use Modules\Backend\Models\Donation as Donation;
use Modules\Backend\Models\Donationlogothers as Donationlogothers;
use Modules\Backend\Models\Donationothers as Donationothers;
use Modules\Backend\Models\Donationlogothers1 as Donationlogothers1;
use Modules\Backend\Models\Donationloghouston as Donationloghouston;
use Modules\Backend\Models\Donationlogseattle as Donationlogseattle;
use Modules\Backend\Models\Donationothers1 as Donationothers1;

class DonationController extends ControllerBase
{

    public function indexAction(){

    }
    public function donationlistAction(){
    	// Instantiate the Query
    	$query = new \Phalcon\Mvc\Model\Query("SELECT m.userid, m.email, m.firstname, m.lastname, dl.amount, dl.transactionId, dl.datetimestamp, 
        dl.paymentmode, dl.lastccba, dl.billinginfofname, dl.billinginfolname, dl.recurcount,dl.recurduration,dl.recurtype,dl.recurstartdate FROM Modules\Backend\Models\Donationlog as dl INNER JOIN Modules\Backend\Models\Members as m ON dl.useremail=m.email ORDER BY dl.datetimestamp DESC ", $this->getDI());
    	$dl = $query->execute();
    	$data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'id' => $q->userid,
                'transactionId' => $q->transactionId,
                'email' => $q->email,
                'name' => $q->firstname. ' ' .$q->lastname,
                'amount' => $q->amount,
                'pm' => $q->paymentmode .'/'.$q->lastccba,
                'bname' => $q->billinginfofname.' '.$q->billinginfolname,
                'remarks' => (!empty($q->recurcount) ? $q->recurcount. ' times in '.$q->recurduration.' '.$q->recurtype.' starting '.$q->recurstartdate : '' ),
                'datetimestamp' => $q->datetimestamp
            );
        }

        $don = Donation::findFirst("id=1");
		$this->view->usersdonated = Donationlog::count(array("distinct" => "useremail"));
		$amount = Donationlog::sum(array("column" => "amount"));
		$this->view->amounts = number_format($amount, 2, '.', '');
		$this->view->totaltrans = count($data);

		$this->view->donamounts = $don->amount;
		$this->view->dontotaltrans = $don->usercount;
	
	   // Execute the query returning a result if any
        $this->view->data = str_replace('\'',' ',str_replace("\\/", "/", json_encode($data)));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function bnbregistrationAction(){
        $this->view->setRenderLevel(view::LEVEL_ACTION_VIEW);
    }

    public function donationothersAction(){
        // Instantiate the Query
        $query = new \Phalcon\Mvc\Model\Query("Select dl.id, dl.amount, dl.transactionId, dl.datetimestamp,
        dl.paymentmode, dl.lastccba, dl.billinginfofname, dl.billinginfolname,dl.donatedto FROM Modules\Backend\Models\Donationlogothers as dl ORDER BY dl.datetimestamp DESC ", $this->getDI());
        $dl = $query->execute();
        $data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'id' => $q->userid,
                'transactionId' => $q->transactionId,
                'name' => $q->billinginfofname. ' ' .$q->billinginfolname,
                'amount' => $q->amount,
                'pm' => $q->paymentmode .'/'.$q->lastccba,
                'bname' => $q->billinginfofname.' '.$q->billinginfolname,
                'donateto' => $q->donatedto,
                'datetimestamp' => $q->datetimestamp
            );
        }

        $don = Donationothers::findFirst("id=1");
        $this->view->usersdonated = Donationlogothers::count(array("distinct" => "billinginfofname"));
        $amount = Donationlogothers::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);

        $this->view->donamounts = $don->amount;
        $this->view->dontotaltrans = $don->usercount;

        // Execute the query returning a result if any
        $this->view->data = str_replace('\'',' ',str_replace("\\/", "/", json_encode($data)));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function nyearthcitizenwalkAction(){
        // Instantiate the Query
        $query = new \Phalcon\Mvc\Model\Query("Select dl.id, dl.amount, dl.transactionId, dl.datetimestamp,
        dl.paymentmode, dl.lastccba, dl.billinginfofname, dl.billinginfolname,dl.donatedto, dl.howdidyoulearn, dl.cname FROM Modules\Backend\Models\Donationlogothers1 as dl ORDER BY dl.datetimestamp DESC ", $this->getDI());
        $dl = $query->execute();
        $data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'id' => $q->userid,
                'transactionId' => $q->transactionId,
                'name' => $q->billinginfofname. ' ' .$q->billinginfolname,
                'amount' => $q->amount,
                'pm' => $q->paymentmode .'/'.$q->lastccba,
                'bname' => $q->billinginfofname.' '.$q->billinginfolname,
                'center' => $q->howdidyoulearn . ' ' . $q->cname,
                'datetimestamp' => $q->datetimestamp
            );
        }

        $don = Donationothers1::findFirst("id=1");
        $this->view->usersdonated = Donationlogothers1::count(array("distinct" => "billinginfofname"));
        $amount = Donationlogothers1::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);

        $this->view->donamounts = $don->amount;
        $this->view->dontotaltrans = $don->usercount;

        // Execute the query returning a result if any
        $this->view->data = str_replace('\'',' ',str_replace("\\/", "/", json_encode($data)));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function houstonearthcitizenswalkAction(){
        // Instantiate the Query
        $query = new \Phalcon\Mvc\Model\Query("Select dl.id, dl.amount, dl.transactionId, dl.datetimestamp,
        dl.paymentmode, dl.lastccba, dl.billinginfofname, dl.billinginfolname,dl.donatedto, dl.howdidyoulearn, dl.cname FROM Modules\Backend\Models\Donationloghouston as dl ORDER BY dl.datetimestamp DESC ", $this->getDI());
        $dl = $query->execute();
        $data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'id' => $q->userid,
                'transactionId' => $q->transactionId,
                'name' => $q->billinginfofname. ' ' .$q->billinginfolname,
                'amount' => $q->amount,
                'pm' => $q->paymentmode .'/'.$q->lastccba,
                'bname' => $q->billinginfofname.' '.$q->billinginfolname,
                'center' => $q->howdidyoulearn . ' ' . $q->cname,
                'datetimestamp' => $q->datetimestamp
            );
        }

        $don = Donationloghouston::findFirst("id=1");
        $this->view->usersdonated = Donationloghouston::count(array("distinct" => "useremail"));
        $amount = Donationloghouston::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);

        $this->view->donamounts = $don->amount;
        $this->view->dontotaltrans = $don->usercount;

        // Execute the query returning a result if any
        $this->view->data = str_replace('\'',' ',str_replace("\\/", "/", json_encode($data)));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function seattlenaturalhealingexpoAction(){
        // Instantiate the Query
        $query = new \Phalcon\Mvc\Model\Query("Select dl.id, dl.amount, dl.transactionId, dl.datetimestamp,
        dl.paymentmode, dl.lastccba, dl.billinginfofname, dl.billinginfolname,dl.donatedto, dl.howdidyoulearn, dl.cname FROM Modules\Backend\Models\Donationlogseattle as dl ORDER BY dl.datetimestamp DESC ", $this->getDI());
        $dl = $query->execute();
        $data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'id' => $q->userid,
                'transactionId' => $q->transactionId,
                'name' => $q->billinginfofname. ' ' .$q->billinginfolname,
                'amount' => $q->amount,
                'pm' => $q->paymentmode .'/'.$q->lastccba,
                'bname' => $q->billinginfofname.' '.$q->billinginfolname,
                'center' => $q->howdidyoulearn . ' ' . $q->cname,
                'datetimestamp' => $q->datetimestamp
            );
        }

        $don = Donationlogseattle::findFirst("id=1");
        $this->view->usersdonated = Donationlogseattle::count(array("distinct" => "useremail"));
        $amount = Donationlogseattle::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);

        $this->view->donamounts = $don->amount;
        $this->view->dontotaltrans = $don->usercount;

        // Execute the query returning a result if any
        $this->view->data = str_replace('\'',' ',str_replace("\\/", "/", json_encode($data)));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

