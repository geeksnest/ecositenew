<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class MenuController extends \Phalcon\Mvc\Controller
{
    
    public function menu_creatorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);	
    }
}

