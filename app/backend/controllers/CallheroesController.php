<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;


class CallheroesController extends ControllerBase
{

    public function indexAction(){

    }

    public function callingheroesAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function heroesondutyAction(){
         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}

