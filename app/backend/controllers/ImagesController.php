<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;
use Modules\Backend\Models\Album as Album;

class ImagesController extends ControllerBase
{

    public function intialize(){

    }
    public function sliderAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editsliderAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    
    public function bannerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editbannerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


    public function manage_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

