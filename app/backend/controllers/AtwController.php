<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class AtwController extends ControllerBase
{

    public function intialize(){

    }
    public function atwAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function faAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

