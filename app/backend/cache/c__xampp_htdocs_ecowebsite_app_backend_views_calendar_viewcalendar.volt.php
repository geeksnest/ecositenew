<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="delact.html">
  <div ng-include="'/tpl/delactMod.html'"></div>
</script>
<script type="text/ng-template" id="updact.html">
  <div ng-include="'/tpl/updactMod.html'"></div>
</script>

<!-- Calendar View -->

<div ng-controller="viewcalendarCtrl">
  
  <div class="wrapper-md bg-light b-b">
    <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
      <i class="fa fa-bars"></i> List
    </button>
    <h1 class="m-n font-thin h3">Calendar of Activity</h1>
  </div>

  <div class="hbox hbox-auto-xs hbox-auto-sm">

    <div class="col wrapper-md">

      <div class="clearfix m-b">
        <!-- <button type="button" class="btn btn-sm btn-primary btn-addon" ng-click="addEvent()">
          <i class="fa fa-plus"></i>Add
        </button> -->
        <div class="pull-right">
          <button type="button" class="btn btn-sm btn-default" ng-click="today(calendar1)">today</button>
          <div class="btn-group m-l-xs">
            <button class="btn btn-sm btn-default" ng-click="changeView('agendaDay', calendar1)">Day</button>
            <button class="btn btn-sm btn-default" ng-click="changeView('agendaWeek', calendar1)">Week</button>
            <button class="btn btn-sm btn-default" ng-click="changeView('month', calendar1)">Month</button>
          </div>
        </div>
      </div>         
      <div class="pos-rlt">

        <!-- begin overlay data -->
        <div class="fc-overlay">
          <div class="panel bg-white b-a pos-rlt">
            <span class="arrow"></span>
            <div class="h4 font-thin m-b-sm">{[{ event.title }]}</div>
            <div class="line b-b b-light"></div>
            <div><i class="icon-calendar text-muted m-r-xs"></i> {[{ event.start | date:'dd.MM.yyyy' }]} </div>
            <div class="ng-hide" ng-show='event.end'><i class="icon-clock text-muted m-r-xs"></i> {[{ event.end | date:'dd.MM.yyyy' }]}</div>
            <div class="ng-hide" ng-show='event.location'><i class="icon-pointer text-muted m-r-xs"></i>{[{ event.location }]}</div>
            <div class="m-t-sm">{[{ event.info }]}</div>
            <!-- <div class="m-t-sm">data.url</div> -->
          </div>
        </div>
        <!-- end overlay data -->

        <!-- begin view calendar -->
        <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
        <!-- end view calendar -->

      </div>

    </div>

    <!-- begin list view -->
    <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
      <div class="wrapper">
        <div ng-repeat="data in dataCalendar" class="bg-white-only r r-2x m-b-xs wrapper-sm b-l b-2x b-primary">          
          <input ng-model="data.acttitle" class="form-control m-t-n-xs no-border no-padder no-bg">
          <a class="pull-right text-xs text-muted" ng-click="deletepage(data.actid)"><i class="fa fa-trash-o"></i></a>
          <a class="pull-right text-xs text-muted">&nbsp;</a>
          <a class="pull-right text-xs text-muted">&nbsp;</a>
          <a class="pull-right text-xs text-muted" ng-click="updatepage(data.actid)"><i class="fa fa-edit"></i></a>
          <div class="text-xs text-muted">{[{data.df}]} - {[{data.dt}]}</div>
        </div>
      </div>
    </div>
    <!-- end list view -->
    
  </div>

</div>