<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="updatepage.html">
  <div ng-include="'/tpl/updatepageModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Featured Project List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Featured Projects
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
         <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <!-- <input type="hidden" ng-init='pagedata = <?php echo $data; ?>'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:5%">&nbsp;</th>
            <th style="width:45%">Title</th>
            <th style="width:25%">Status</th>
            <th style="width:25%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="feat in data.data">
            <td align="center"><i class="glyphicon {[{ feat.icnstar }]} icon"></i></td>
            <td>{[{ feat.feat_title }]}</td>
            <td><span ng-class="">{[{ feat.pubtxt }]}</span></td>
            <td>
              <a href="" ng-click="updatepage(feat.feat_id)"><span class="label bg-warning" >Edit</span></a>
              <a href="" ng-click="deletepage(feat.feat_id)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
          </ul>   

        <div>Note: Featured project is mark with <i class="glyphicon icon-star icon"></i></div>

        </div>
     </div>
    </footer>
  </div>
</div>