<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="deleteOtherDonationModal.html">
  <div ng-include="'/tpl/deleteOtherDonationModal.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of Seattle Expo Donations</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
<div class="row text-center">
	<div class="col-xs-4">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">$<?php echo $amounts; ?></div>
                <span class="text-muted text-xs">Total Donations</span>
                <span class="bottom text-right">
                  <i class="fa fa-dollar text-muted m-r-sm"></i>
                </span>
              </div>
            </div>
	<div class="col-xs-4">
              <a href="" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block"><?php echo $usersdonated; ?></span>
                <span class="text-muted text-xs">Total Donors</span>
                <span class="bottom text-right">
                  <i class="fa fa-user text-muted m-r-sm"></i>
                </span>
              </a>
            </div>
<!-- <div class="col-xs-4 form-group">
  <form name="updateamounts" novalidate>
    <div class="col-xs-5 ">
      <label>Amount Added</label>
		  <input type="text" class="form-control" ng-model="donate.amount" name="amount" ng-init="donate.amount=<?php echo $donamounts; ?>">
    </div>
    <div class="col-xs-5">
      <label>Users Added</label>
		  <input type="text" class="form-control" ng-model="donate.users" name="users" ng-init="donate.users=<?php echo $donamounts; ?>">
    </div>
    <div class="col-xs-10" style="text-align: left;">
      <input type="button" value="Add numbers to current results." style="margin-top: 5px;" ng-click="updateAmounts(donate)">
      <i class="fa fa-check-square" ng-show="updatedamount"></i>
    </div>
  </form>
</div> -->
</div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-12 ">
          Manage Donations
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-5 ">
          <div class="input-group" ng-hide="advancesearch">
            <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
            </span>
          </div>

          <div class="checkbox">
            <label class="i-checks">
              <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by
            </label>
          </div>

          <div ng-show="advancesearch">

            <!-- SEARCH BY TIMESTAMP -->
            <div class="form-group">

              <div class="row">

                <div class="col-sm-3">
                  Year:<br/>
                  <select ng-model="timestamp.year" class="input-sm form-control">
                    <?php for ($year=2000; $year < 2025; $year++) { 
                      echo "<option value='".$year."'>".$year."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-5">
                  Month:<br/>
                  <select ng-model="timestamp.month" class="input-sm form-control">
                    <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); ?>
                    <?php foreach ($formonths as $index => $formonth) { 
                      echo "<option value='".$index."'>".$formonth."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-3">
                  Day:<br/>
                  <select ng-model="timestamp.day" class="input-sm form-control">
                    <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                    echo "<option value='".$days."'>".$days."</option>";
                  } ?>
                </select>
              </div>
              <div class="col-sm-1">
                <br/>
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="searchtimestamp(timestamp)">Go!</button>
                </span>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
    </div>
    <div class="table-responsive">
      <input type="hidden" ng-init='pagedata = <?php echo $data; ?>'>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th  style="width:12%">TransactionID</th>
            <th  style="width:24%">Email</th>
            <th  style="width:17%">Payment Mode / Last 4 Digits</th>
            <th  style="width:5%">Amount</th>
            <th  style="width:14%">Billing Info</th>
            <th  style="width:13%">How did he/she know?</th>
            <th  style="width:12%">Timestamp (UTC)</th>
            <th  style="width:5%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="don in data.data">
            <td>{[{ don.transactionId }]}</td>
            <td>{[{ don.email }]}</td>
            <td>{[{ don.pm }]}</td>
            <td>{[{ don.amount }]}</td>
            <td>{[{ don.bname }]}</td>
            <td>{[{ don.center }]}</td>
            <td>{[{ don.datetimestamp }]}</td>
            <td>
              <a href="" ng-click="delete(don.id)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
          <ul class="pagination" ng-hide="advancesearch">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before,searchtext)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="paging(page.num,searchtext)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next,searchtext)"> </a></li>
          </ul>
          
          <ul class="pagination" ng-show="advancesearch">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="pagingtimestamp(data.before,timestamp)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="pagingtimestamp(page.num,timestamp)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="pagingtimestamp(data.next,timestamp)"> </a></li>
          </ul>
        </div>
      </div>
    </footer>

  </div>
</div>