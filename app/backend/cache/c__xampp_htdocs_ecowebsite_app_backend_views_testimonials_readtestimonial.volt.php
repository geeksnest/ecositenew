<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="updatetestimonial.html">
  <div ng-include="'/tpl/updatetestimonial.html'"></div>
</script>
<script type="text/ng-template" id="deletetestimonial.html">
  <div ng-include="'/tpl/deletetestimonial.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Testimonial Lists</h1>
  <a id="top"></a>
</div>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Read Testimonials</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
  <div class="panel panel-default">
  <div class="panel-heading">                    
          <span class="label bg-dark">15</span> Testimonials
  </div>
  <div class="panel-body" ui-jq="slimScroll" ui-options="{height:'850px', size:'8px'}" style="overflow: hidden; width: auto; height: 250px; ">
          <div class="media" ng-repeat="testimonial in data.data" style="background-color:#eee">
            <span class="pull-left thumb-sm"><img src="<?php echo $this->config->application->apiURL; ?>/images/user.png" alt="..."></span>
              <span class=" pull-right">

                <a href="" ng-click="updatepage(testimonial.id )"><span class="label bg-warning" >Edit</span></a>
                <a href="" ng-click="deletepage(testimonial.id)"> <span class="label bg-danger">Delete</span></a>

              </span>
            <div class="media-body">
              <div class="pull-right text-center text-muted">
              </div>
              <a href="" class="h4">{[{ testimonial.name}]}</a>

              <div class="block">
                <em class="text-xs">Company/Organization: <span class="text-danger">{[{ testimonial.comporg}]}</span></em>
                <!-- <i class="icon-home"></i> -->
                <!-- Company/Organization:<label class="label bg-info m-l-xs">{[{ testimonial.comporg}]}</label> -->
                <i class="fa fa-envelope fa-fw m-r-xs"></i>:{[{ testimonial.email}]}
              </div>
              <blockquote>
                <small class="block m-t-sm">{[{ testimonial.message}]}</small>
              </blockquote>
              <medium class="block m-t-sm">Status: <b style="text-transform:uppercase;color:red">{[{ testimonial.publish }]}ed</b> -- <a href="" ng-click="publish(testimonial.id,testimonial.publish)"><small style="color:#31B0D5">click here to change status</small></a></medium>
            </div>
            
          </div>
    </div>
   
  <!-- CONTENT GOES HERE -->
  <!--  
    <footer class="panel-footer">
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a></li>
          </ul>   
    </footer>
  -->
  
  </div>
</div>