<?php echo $this->getContent(); ?>
<style type="text/css">
	.colorpicker-colorbox{
		margin-left: 5px;
    border-radius: 4px;
	}
	.width90px{
		width: 119px;
	}
	.pointer {
		cursor: pointer!important;
	}
</style>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Top page</h1>
	<a id="top"></a>
</div>
<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="row">
		<div class="panel-body">
			<tabset class="tab-container">
				<tab heading="Update 1st Box">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save(msg)">
								<div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-2 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-right" style="width:68px; background: {[{ msg.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor" id="myeditor" ng-model="msg.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>	
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update 2nd Box">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save1(msg1)">
								<div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-2 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg1.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-right" style="width:68px; background: {[{ msg1.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor1" id="myeditor1" ng-model="msg1.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update 3rd Box">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save2(msg2)">
								<div class="row">
									<div class="col-sm-12">
										<label for="title">Background color</label>
									</div>
									<div class="col-sm-2 wrapper-sm">
										<input colorpicker class="form-control width90px pull-left pointer" placeholder="#ffffff" ng-model="msg2.bgcolor" type="text">
										<div class="colorpicker-colorbox form-control pull-right" style="width:68px; background: {[{ msg2.bgcolor }]}"> </div>
									</div>
								</div>
								<div class="row wrapper">
									<textarea class="ck-editor" name="myeditor2" id="myeditor2" ng-model="msg2.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</div>