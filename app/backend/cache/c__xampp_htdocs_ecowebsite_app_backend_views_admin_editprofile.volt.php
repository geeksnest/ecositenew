<?php echo $this->getContent(); ?>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit User</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateuser(user, 'ajaxSubmitResult1')" name="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="loader" ng-show="imageloader">
        <div class="loadercontainer">
          Saving Data...
          <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">

              <div class="form-group">
                <div ng-if="editusername == false">
                  <label class="col-sm-3 control-label">Username</label>
                  <div class="col-sm-7">
                    <b>{[{user.username}]}</b>
                  </div>
                  <div class="col-sm-2">
                    <a href="" class="edit-prof" ng-click="editUname()"><i class="fa fa-edit"></i> Edit</a>
                  </div>
                </div>
                <div ng-if="editusername == true">
                  <label class="col-sm-8 control-label">Username    <span class="label bg-danger" ng-if="usernametaken == true">Username already taken. <br/></span></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="saveUname(user)" ng-if="validusernamebot == true"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a></label>
                  <div class="col-sm-12">
                    <input type="text" id="username" name="username" class="form-control" ng-model="user.username" ng-change="chkUsername(user)" required="required">
                    <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </div>

              <div class="form-group">
                <div ng-if="editemailaddress == false">
                  <label class="col-sm-3 control-label">Email Address</label>
                  <div class="col-sm-7">
                    <b>{[{user.email}]}</b>
                  </div>
                  <div class="col-sm-2">
                    <a href="" class="edit-prof" ng-click="editEmail()"><i class="fa fa-edit"></i> Edit</a>
                  </div>
                </div>
                <div ng-if="editemailaddress == true">
                  <label class="col-sm-8 control-label">Email Address    <span class="label bg-danger" ng-if="emailtaken == true">Email Address already taken.</span><span class="label bg-danger" ng-show="invalidemail == true">Invalid Email Address.</span></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="saveEmail(user)" ng-if="validemailbot == true"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a></label>
                  <div class="col-sm-12">
                    <input type="text" id="email" name="email" class="form-control" ng-model="user.email" ng-change="chkEmail(user)" required="required">
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
              </div>

              <div class="form-group" ng-if="changepassword == false">
                <label class="col-sm-12 control-label"> <a href="" class="change-pass" ng-click="changepass()">Change Password?</a> </label>

                <div class="col-sm-12">
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>

              <div ng-if="changepassword == true">
                <div class="form-group">
                  <label class="col-sm-8 control-label">Old Password   <span class="label bg-danger" ng-if="oldpasswordMin == true">Passwords is too small, minimum of 6 digits.</span></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="saveChangePass(user)" ng-if="validpassbot == true"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a></label>
                  <label class="col-sm-2 control-label"><a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a></label>
                  <div class="col-sm-12">
                    <input type="password" id="oldpassword" name="oldpassword" class="form-control" ng-model="user.oldpassword" ng-blur="oldminPass(user)" ng-change="chkPass2()" required="required">
                  </div>
                  <label class="col-sm-12 control-label">New Password   <span class="label bg-danger" ng-if="passwordMin == true">Passwords is too small, minimum of 6 digits.</span></label>                  
                  <div class="col-sm-12">
                    <input type="password" id="password" name="password" class="form-control" ng-model="user.password" ng-blur="minPass(user)" ng-change="chkPass2()" required="required">
                  </div>
                  <label class="col-sm-12 control-label">Confirm New Password   <span class="label bg-danger" ng-if="passwordInvalid == true">Passwords do not match.</span></label>
                  <div class="col-sm-12">
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control" ng-model="user.confirm_password" ng-change="chkPass(user)" required="required">
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>             

            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              User Information
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-8" ng-if="editUserInfo == false">
                  <div class="form-group">
                    <label class="col-sm-9 control-label"></label>
                    <div class="col-sm-3">
                      <a href="" class="edit-prof" ng-click="editInfo()"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                  </div> 
                </div>
                <div class="col-sm-8" ng-if="editUserInfo == true">
                  <div class="form-group">
                    <label class="col-sm-6 control-label"></label>
                    <label class="col-sm-3 control-label"><a href="" class="edit-prof" ng-click="saveInfo(user)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a></label>
                  <label class="col-sm-3 control-label"><a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a></label>
                  </div> 
                </div> 
                <div class="col-sm-8">
                  <div ng-if="editUserInfo == false">
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Name</label>
                      <div class="col-sm-9">
                        <b>{[{user.firstname}]} {[{user.lastname}]}</b>
                      </div>
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>  
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Gender</label>
                      <div class="col-sm-9">
                        <b>{[{user.gender}]}</b>
                      </div>
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>  
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Birthday</label>
                      <div class="col-sm-9">
                        <b>{[{user.birthday}]}</b>
                      </div>
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">State</label>
                      <div class="col-sm-9">
                        <b>{[{user.state}]}</b>
                      </div>
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>                    
                  </div>
                  <div ng-if="editUserInfo == true">
                    <div class="form-group">
                      <label class="col-sm-12 control-label">Firstname</label>
                      <div class="col-sm-12">
                        <input type="text" id="firstname" name="firstname" class="form-control" ng-model="user.firstname" required="required">
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-12 control-label">Lastname</label>
                      <div class="col-sm-12">
                        <input type="text" id="lastname" name="lastname" class="form-control" ng-model="user.lastname" required="required">
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-12 control-label">Gender</label>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="radio">
                              <label class="i-checks">
                                <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                                <i></i>
                                Male
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="radio">
                              <label class="i-checks">
                                <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                                <i></i>
                                Female
                              </label>
                            </div>
                          </div>
                        </div>
                        <!-- <div class="line line-dashed b-b line-lg pull-in"></div> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 propic">
                  <label class="label_profile_pic btn btn-default" id="change-picture" ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false">Change Photo</label>
                  <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/userimages/{[{user.imgg}]}" name="profpic" id="profpic" ng-if="imageselected == false">
                  <img ngf-src="files[0]" ngf-default-src="{[{base_url}]}/images/default_images/default-page-thumb.png" name="profpic" id="profpic" ng-if="imageselected == true">
                  <div ng-if="changepic == true">
                    <label class="label_profile_pic btn btn-success" ng-click="updatePhoto(files, user)"><i class="glyphicon glyphicon-floppy-saved"></i> Save Change</label>
                    <label class="label_profile_pic btn btn-default" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</label>
                    <!-- <label class="col-sm-6 control-label"><a href="" class="edit-prof" ng-click="saveInfo(user)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a></label> --> 
                    <!-- <label class="col-sm-6 control-label"><a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a></label> -->
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                  <div class="line line-dashed b-b line-lg"></div>
                  </div>
                </div>
              </div>

              <div ng-if="editUserInfo == true">
                <div class="form-group">
                  <label class="col-sm-12 control-label">Birthday</label>
                  <div class="col-sm-12">
                    <div class="input-group w-md">
                      <span class="input-group-btn">
                        <input type="hidden" ng-model="user.birthday2">
                        <input id="date" name="date" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="user.birthday" is-open="datepicker.opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                        <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                      </span>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12 control-label">State</label>
                  <div class="col-lg-12">
                    <select ui-jq="chosen" class="w-md" name="state" ng-model="user.state" required="required">
                      <optgroup label="Alaskan/Hawaiian Time Zone">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                      </optgroup>
                      <optgroup label="Pacific Time Zone">
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                      </optgroup>
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Arizona</option>
                        <option value="CO">Colorado</option>
                        <option value="ID">Idaho</option>
                        <option value="MT">Montana</option><option value="NE">Nebraska</option>
                        <option value="NM">New Mexico</option>
                        <option value="ND">North Dakota</option>
                        <option value="UT">Utah</option>
                        <option value="WY">Wyoming</option>
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="IL">Illinois</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="OK">Oklahoma</option>
                        <option value="SD">South Dakota</option>
                        <option value="TX">Texas</option>
                        <option value="TN">Tennessee</option>
                        <option value="WI">Wisconsin</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="IN">Indiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="NH">New Hampshire</option><option value="NJ">New Jersey</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="OH">Ohio</option>
                        <option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>
                        <option value="VT">Vermont</option><option value="VA">Virginia</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                    <div class="line line-dashed b-b line-lg pull-in"></div>

                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Country</label>
                <label class="col-lg-10 control-label">Soon, Now only in USA</label>
              </div>

            </div>

          </div>
        </div>


<!--         <div class="col-sm-12">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <button type="button" class="btn btn-default" ui-sref="userlist">Cancel</button>
              <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Update</button>
            </footer>
          </div>
        </div> -->

      </div>
    </div>
  </fieldset>
</form>