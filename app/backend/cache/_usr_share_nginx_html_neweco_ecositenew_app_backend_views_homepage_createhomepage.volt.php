<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepageimgModal.html">
  <div ng-include="'/tpl/deletepageimgModal.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Homepage</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formPage" id="formPage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Information
            </div>
            <div class="panel-body">

              <label class="col-sm-12 control-label">
                <label for="title">Title</label>
              </label>
              <div class="col-sm-12">
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required">
                <br>
              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <label class="col-sm-6 control-label">
              	<label for="title">Sub page (optional)</label>
              </label>
              <label class="col-sm-6 control-label">
                <label for="title">Font color</label>
              </label>
                <div class="col-sm-6">
                  <select class="form-control" ng-model="page.subpage" >
                    <option value="" stlye="display:none">-Select-</option>
                    <option ng-repeat="submenulink in submenulink" value="{[{submenulink.submenuID}]}" ng-hide="submenulink.subname == 'NEWS'">{[{ submenulink.subname }]}</option>
                  </select>
                  <br>
                </div>
                <div class="col-sm-6">
                    <input colorpicker class="form-control" placeholder="#ffffff" ng-model="page.subpagecolor" type="text">
                </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="form-group">
                <label class="col-sm-12 control-label">Body Content</label>
                <div class="col-sm-12">

                  <textarea class="ck-editor" ng-model="page.content" name="myeditor" id="myeditor" required="required"></textarea>

                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Background Image
             <!--  <a class="btn m-b-xs btn-xs btn-info pull-right" ng-click="stylepreview(page)"><i class="icon icon-eyeglasses"></i> Quick Preview</a> -->
             </label>
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{ page.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner"  placeholder="Paste Banner link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.banner)" id="banner" required>
              <label ng-show="n" class="control-label p" style="color:#a94442;" ng-cloak>
                This field is required.</label>
                <div class="line line-dashed b-b line-lg"></div>
            </div>
          </div>
        </div>

      </div>


      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-right bg-light lter">
            <button type="button" class="btn btn-default" ng-click="reset()">Clear Form</button>
            <button type="submit" ng-disabled="formPage.$invalid" class="btn btn-success">Submit</button>
          </footer>
        </div>
      </div>
    </div>
  </fieldset>
</form>


<div class="panel-body">
  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
  <div class="loader" ng-show="imageloader">
    <div class="loadercontainer">

      <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
      </div>
      Uploading your images please wait...

    </div>

  </div>

  <div ng-show="imagecontent">

    <div class="col-sml-12">
      <div class="dragdropcenter">
        <div ngf-drop ngf-select ng-model="files" class="drop-box"
        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
        accept="image/*,application/pdf">Drop images here or click to upload</div>
      </div>
    </div>

    <div class="line line-dashed b-b line-lg"></div>

    <div class="col-sm-3" ng-repeat="data in data">
     <a href="" ng-click="deletepageimg(dlt)" class="closebutton">&times;</a>
     <input type="hidden" id="" name="id" ng-init="dlt.id=data.id" class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
     <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
     <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}');">
     </div>
   </div>

 </div>


</div>