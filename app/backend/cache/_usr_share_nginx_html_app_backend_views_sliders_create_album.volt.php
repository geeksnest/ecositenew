<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="editAlbum.html">
  <div ng-include="'/tpl/editAlbum.html'"></div>
</script>

<script type="text/ng-template" id="deleteAlbum.html">
  <div ng-include="'/tpl/deleteAlbum.html'"></div>
</script>

<script type="text/ng-template" id="setMainSlider.html">
  <div ng-include="'/tpl/setMainSlider.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Sliders</h1>
  <a id="top"></a>
</div>

<div class="loader" ng-show="imageloader">
  <div class="loadercontainer">
    <div class="spinner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
    Please wait while uploading your images...
  </div>
</div>

<div class="wrapper-md" ng-hide="imageloader">
  <form name="formUpload" id="formUpload">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        <div class="row">
          <div class="col-sm-12">
            Add Albums

            <button ng-show="addphoto == false" class="btn m-b-xs btn-sm btn-info btn-addon pull-right" id="change-picture" ngf-change="prepare(files,file)" ngf-select ng-model="files" ngf-multiple="true" required="required"><i class="glyphicon glyphicon-plus"></i> New Album</button>

            <button ng-disabled="formUpload.$invalid || file == ''" ng-show="addphoto == true" class="btn m-b-xs btn-sm btn-success btn-addon pull-right" ng-click="uploadAlbum(file,album)"><i class="fa fa-upload"></i> Upload</button>

            <button ng-show="addphoto == true" class="btn m-b-xs btn-sm btn-info btn-addon pull-right mar-right" id="change-picture" ngf-change="prepare(files,file)" ngf-select ng-model="files" ngf-multiple="true" required="required"><i class="glyphicon glyphicon-plus"></i> Add Image</button>
          </div>
        </div>
      </div>
      <div class="panel-body" ng-show="addphoto == true">
        <div class="row">
          <div class="col-sm-12" ng-show="bigImages == true">
            <span class="label bg-danger textAt"><i class="fa fa-exclamation-circle"></i> The following images you wish to upload is too big. Please resize the images below 2MB and try again.</span>
            <div class="col-sm-12" ng-repeat="bigfile in bigfile track by $index">
            <br>
            <label><span ng-bind="bigfile.name"></span></label>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

          </div>
          <div class="col-sm-12 propic">
            <input id="albumname" type="text" name="albumname" ng-model="album.albumname" class="form-control"  ng-show="imageselected == true" ng-change="onalbumname(album.albumname)" required="required">
            <input id="foldername" type="hidden" name="foldername" ng-model="album.foldername" class="form-control">
            <div class="row">
              <div class="col-sm-4 album-padding" ng-repeat="file in file track by $index">
                <a href="" ng-click="delete($index)" ng-if="imageselected == true" class="pull-right remove"><i class="fa fa-times-circle"></i> Remove</a>
                <img ngf-src="file" name="profpic" id="profpic" ng-if="imageselected == true">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

<div class="wrapper-md">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        <div class="row">
          <div class="col-sm-12">
            Albums
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-3" ng-repeat="album in album">
            <div class="row">
              <div class="col-sm-12 album-wrap" ng-repeat="item in album.item">
                <div style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/slider/{[{item.foldername}]}/{[{item.img}]}) 0px 0px;background-size: cover; background-position:center center; position: relative;height:200px;" class="file-control">
                  <div class="col-sm-12 no-border bg-albumtitle">
                    <span class="text-lt text-Ha">{[{album.album_name}]}</span>
                    
                  </div>
                  <div class="file-control-action text-front">
                    <a href="" class="btn opt-btn"  title="Edit" ng-click="editalbum(album.album_id)"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn opt-btn"  title="Delete" ng-click="deleteAlbum(album.album_id)" ng-show="album.main != 1"><i class="fa fa-times-circle"></i> Delete</a>
                  </div>
                  <div class="star" ng-show="album.main == 1"><i class="fa fa-star manage-action text-warning"></i></div>
                  <div class="file-control-action">
                    <div class="star" ng-show="album.main != 1"><i class="fa fa-star-o manage-action" ng-click="setMainSlider(album.album_id)"></i></div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

