<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="helperModal.html">
  <div ng-include="'/tpl/helperModal.html'"></div>
</script>
<script type="text/ng-template" id="deletenewsimgModal.html">
  <div ng-include="'/tpl/deletenewsimgModal.html'"></div>
</script>
<script type="text/ng-template" id="eventsBanner.html">
 <div ng-include="'/tpl/eventsBanner.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <div class="row">
    <div class="col-xs-11">
      <h1 class="m-n font-thin h3">Event Contents</h1>
      <a id="top"></a>
    </div>
    <div class="col-sm-1">
      <span class="input-group-btn">
        <a class="btn btn-sm btn-dark float-right" ui-sref="eventlist"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
      </span>
    </div>
  </div>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formevents" id="formevents">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <input type="hidden" ng-model="events.eventID">
      <div class="row">
        <div class="col-sm-7">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Event Information
              <span class="pull-right">
                <span ng-show="editInfo2 == true">
                  <a href="" class="edit-prof" ng-click="saveInfo(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span>
                  <a href="" ng-show="editInfo2 == false" class="edit-prof" ng-click="editInfo()"><i class="fa fa-edit"></i> Edit</a>
                </span>
                <a ng-click="infoimages('event_title')"> &nbsp;<i class="icon-question"></i></a>
              </span>
            </div>
            <div class="panel-body">             
              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event Title</label>
                </label>
                <label class="col-sm-12 control-label tabtext" ng-show="editInfo2 == false">
                  <span class="content-view">{[{events.title}]}</span>
                </label>
                <div class="col-sm-12" ng-show="editInfo2 == true">
                  <input type="text" id="title" name="title" class="form-control" ng-model="events.title" required="required" maxlength="255" ng-blur="onnewstitle(events.title)">
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event URL</label>
                </label>
                <label class="col-sm-12 control-label tabtext" ng-show="editInfo2 == false">
                  <a href="<?php echo $this->config->application->baseURL; ?>/{[{events.URL}]}" target="_blank" class="edit-prof content-view"><?php echo $this->config->application->baseURL; ?>/{[{events.URL}]}</a> 
                </label>
                <div class="col-sm-12" ng-show="editInfo2 == true">
                  <em class="text-muted">Don't use space for this field.</em>
                  <div class="input-group"> 
                    <span class="input-group-btn">
                    <lable class="form-control"><?php echo $this->config->application->baseURL; ?>/</lable>
                    </span>
                      <input type="text" id="url" name="url" class="form-control" ng-model="events.URL" required="required" maxlength="255" ng-change="events.URL = events.URL.split(' ').join('')">
                  </div>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event Short Description</label>
                </label>
                <label class="col-sm-12 control-label tabtext" ng-show="editInfo2 == false">
                  <span class="content-view">{[{events.shortDesc}]}</span>
                </label>
                <div class="col-sm-12" ng-show="editInfo2 == true">
                  <em class="text-muted">(maximum 500 characters only)</em>
                  <textarea ng-model="events.shortDesc" maxlength="500" required="required" class="form-control" style="resize:vertical;" ></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="row form-group">
                <div class="col-sm-12">
                  <label class="control-label">Content</label>
                </div>
                <label class="col-sm-12 control-label tabtext" ng-show="editInfo2 == false">
                  <div class="content-view" ng-bind-html="eventcontent"></div>
                </label>
                <div class="col-sm-12" ng-show="editInfo2 == true">
                  <textarea class="ck-editor" ng-model="events.content" required="required"></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>
          </div>


        <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Payment Reciept Email Info
              <span class="pull-right">
                <span ng-show="editEmailInfo2 == true">
                  <a href="" class="edit-prof" ng-click="saveEmailInfo(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span ng-show="editEmailInfo2 == false">
                  <a href="" class="edit-prof" ng-click="editEmailInfo()"><i class="fa fa-edit"></i> Edit</a>
                </span>
                <a ng-click="infoimages('event_title')"> &nbsp;<i class="icon-question"></i></a>
              </span>
            </div>
            <div class="panel-body">

              <div class="row form-group">
                <div class="col-sm-12">
                  <label class="control-label">Subject</label>
                </div>
                <label class="col-sm-12 control-label tabtext" ng-show="editEmailInfo2 == false">
                  <span class="content-view">{[{events.recieptSubject}]}</span>
                </label>
                <div class="col-sm-12" ng-show="editEmailInfo2 == true">                  
                  <input type="text" id="recieptsubject" name="recieptsubject" class="form-control" ng-model="events.recieptSubject" required="required" maxlength="255" >
                  <br>
                </div>
                <div class="col-sm-12">
                  <label class="control-label">Payment Reciept Content</label>
                </div>
                <label class="col-sm-12 control-label tabtext" ng-show="editEmailInfo2 == false">                  
                  <div class="content-view" ng-bind-html="recieptContent"></div>
                </label>
                <div class="col-sm-12" ng-show="editEmailInfo2 == true">
                  <textarea class="ck-editor" ng-model="events.recieptContent" required="required"></textarea>
                  <br>
                </div>
              </div>             

            </div>
          </div>
        </div>

        <div class="col-sm-5">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Banner
              <span class="pull-right">
                <span ng-show="editBanner2 == true">
                  <a href="" class="edit-prof" ng-click="saveBanner(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span ng-show="editBanner2 == false">
                  <a href="" class="edit-prof" ng-click="editBanner()"><i class="fa fa-edit"></i> Edit</a>
                </span>
              </span>
            </div>
            <div class="panel-body">
              <div class="input-group m-b" ng-show="editBanner2 == true">
                <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="eventsBanner('banner')"><i class="icon-folder"></i> Media Library</a>
                </span>
              </div>

              <div>
                <div class="line line-dashed b-b line-lg"></div>
                <img ng-show="events.banner" err-src="" ng-src="<?php echo $this->config->application->amazonlink; ?>/uploads/eventsbanner/{[{ events.banner }]}" style="width: 100%">
                <input type="hidden" id="banner" name="banner" class="form-control" ng-model="events.banner"  placeholder="Paste thumbnail link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(events.banner)">
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Donation Amount
              <span class="pull-right">
                <span ng-show="editAmounts2 == true">
                  <a href="" class="edit-prof" ng-click="saveAmounts(prices,events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span ng-show="editAmounts2 == false">
                  <a href="" class="edit-prof" ng-click="editAmounts()"><i class="fa fa-edit"></i> Edit</a>
                </span>
              </span>
              <!-- <button type="button" ng-click="chkprices(prices)">chk prices</button> -->
            </div>
            <div class="panel-body">
              <div class="row" ng-show="editAmounts2 == true">
                <div class="col-sm-7">
                  <div class="input-group">
                    <input type="text" id="amounts" name="amounts" class="form-control" ng-model="price.amounts" name="amounts" placeholder="Amount" only-digits>
                    <span class="input-group-btn">
                      <a class="btn btn-success" ng-show="showUpdate == false" ng-click="addprice(price)" ng-disabled="price.amounts == null"><i class="fa fa-plus"></i></a>
                      <a class="btn btn-success" ng-show="showUpdate == true" ng-click="updatePrice(price)" ng-disabled="price.amounts == null"><i class="fa fa-exchange"></i></a>
                      <a class="btn btn-default" ng-show="showUpdate == true" ng-click="priceCancel()"><i class="glyphicon glyphicon-ban-circle"></i></a>
                    </span>
                  </div>
                </div>
              </div>

              <ul class="list-group no-radius">
                <li ng-repeat="p in prices track by $index"class="list-group-item service-price" ng-hide="currentEditPrice == $index">
                  <span><i class="fa fa-dollar"></i> {[{p.amounts}]}</span>
                  <span class="service-price" ng-show="editAmounts2 == true">
                    <span class="price-control-action"><a href="" ng-click="price.amounts=p.amounts;showUpdate = true; editPrice($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="delete($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
                </li>
              </ul>

              <div class="row">
                <div class="col-sm-7">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.otheramount" ng-disabled="editAmounts2 == false">
                      <i></i>
                      Display Other Amount?
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <div class="row">
                <div class="col-sm-6">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.paymentInfo" ng-disabled="editPaymentInfo2 == false">
                      <i></i>
                      Display Payment Information?
                    </label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <span class="pull-right">
                    <span ng-show="editPaymentInfo2 == true">
                      <a href="" class="edit-prof" ng-click="savePaymentInfo(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                      <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                    </span>
                    <span ng-show="editPaymentInfo2 == false">
                      <a href="" class="edit-prof" ng-click="editPaymentInfo()"><i class="fa fa-edit"></i> Edit</a>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <div class="row">
                <div class="col-sm-6">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.billingInfo" ng-disabled="editbillingInfo2 == false">
                      <i></i>
                      Display Billing Information?
                    </label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <span class="pull-right">
                    <span ng-show="editbillingInfo2 == true">
                      <a href="" class="edit-prof" ng-click="savebillingInfo(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                      <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                    </span>
                    <span ng-show="editbillingInfo2 == false">
                      <a href="" class="edit-prof" ng-click="editbillingInfo()"><i class="fa fa-edit"></i> Edit</a>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div> -->


          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              How did you learn about?
              <span class="pull-right">
                <span ng-show="edithdyla2 == true">
                  <a href="" class="edit-prof" ng-click="savehdyla(events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span ng-show="edithdyla2 == false">
                  <a href="" class="edit-prof" ng-click="edithdyla()"><i class="fa fa-edit"></i> Edit</a>
                </span>
              </span>
            </div>            
            <label class="control-label tabtext" ng-show="edithdyla2 == false">
              <span class="content-view">{[{events.hdyla}]}</span>
            </label>
            <div class="panel-body" ng-show="edithdyla2 == true">
              <em class="text-muted">(e.g. How did you learn about the  Earth Citizens Walk in Houston?)</em>
              <input type="text" id="hdyla" name="hdyla" class="form-control" ng-model="events.hdyla" name="hdyla" required="required" >
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Center Names
              <span class="pull-right">
                <span ng-show="editCenterName2 == true">
                  <a href="" class="edit-prof" ng-click="saveCname(cnames,events)"><i class="glyphicon glyphicon-floppy-saved"></i> Save</a>&nbsp;&nbsp;&nbsp;
                  <a href="" class="edit-prof" ng-click="cancelEdit()"><i class="icon-close"></i> Cancel</a>
                </span>
                <span ng-show="editCenterName2 == false">
                  <a href="" class="edit-prof" ng-click="editCenterName()"><i class="fa fa-edit"></i> Edit</a>
                </span>
              </span>
            </div>
            <div class="panel-body">
              <div class="row" ng-show="editCenterName2 == true">
                <div class="col-sm-7">
                  <div class="input-group">
                    <input type="text" id="centernames" name="centernames" class="form-control" ng-model="cname.centernames" name="centernames">
                    <span class="input-group-btn">
                      <a class="btn btn-success" ng-show="cnameUpdate == false"ng-click="addCname(cname)" ng-disabled="cname.centernames == null"><i class="fa fa-plus"></i></a>
                      <a class="btn btn-success" ng-show="cnameUpdate == true" ng-click="updateCname(cname)" ng-disabled="cname.centernames == null"><i class="fa fa-exchange"></i></a>
                      <a class="btn btn-default" ng-show="cnameUpdate == true" ng-click="cancelCname()"><i class="glyphicon glyphicon-ban-circle"></i></a>
                    </span>
                  </div>
                </div>
              </div>

              <ul class="list-group no-radius">
                <li ng-repeat="cnames in cnames track by $index"class="list-group-item" ng-hide="currentEditCname == $index">
                  <span>{[{cnames.centernames}]}</span>
                  <span class="service-price" ng-show="editCenterName2 == true">
                    <span class="price-control-action"><a href="" ng-click="cname.centernames=cnames.centernames;cnameUpdate = true; editCname($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="deleteCname($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>



      </div>

    </div>
  </fieldset>
</form>




