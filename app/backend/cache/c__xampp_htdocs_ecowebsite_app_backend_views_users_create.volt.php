<?php echo $this->getContent(); ?>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" name="userform">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="loader" ng-show="imageloader">
        <div class="loadercontainer">
          Saving Data...
          <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">

              <div class="form-group">
                <label class="col-sm-12 control-label">Username    <span class="label bg-danger" ng-if="usernametaken == true">Username already taken. <br/></span></label>
                <div class="col-sm-12">
                  <input type="text" id="username" name="username" class="form-control" ng-model="user.username" ng-change="chkUsername(user)" required="required">
                  <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 control-label">Email Address    <span class="label bg-danger" ng-if="emailtaken == true">Email Address already taken.</span><span class="label bg-danger" ng-show="invalidemail == true">Invalid Email Address.</span></label>
                <div class="col-sm-12">

                  <input type="text" id="email" name="email" class="form-control" ng-model="user.email" ng-blur="chkEmail(user)" required="required">
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 control-label">Password   <span class="label bg-danger" ng-if="passwordMin == true">Passwords is too small, minimum of 6 digits.</span></label>
                <div class="col-sm-12">
                  <input type="password" id="password" name="password" class="form-control" ng-model="user.password" ng-blur="minPass(user)" required="required">
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>
              <div class="form-group">
              <label class="col-sm-12 control-label">Confirm Password   <span class="label bg-danger" ng-if="passwordInvalid == true">Passwords do not match.</span></label>
                <div class="col-sm-12">
                  <input type="password" id="confirm_password" name="confirm_password" class="form-control" ng-model="user.confirm_password" ng-blur="chkPass(user)" required="required">
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-heading font-bold">
                  Restrictions
                </div>
                <div class="panel-body">
                  <div class="row">
                    <?php foreach ($tblroles as $tblroles ) {
                      if ($tblroles->status == 1){
                      // if ($tblroles->roleDescription == 'Users' || $tblroles->roleDescription == 'News' || $tblroles->roleDescription == 'Donations' || $tblroles->roleDescription == 'Pages' || $tblroles->roleDescription == 'Sliders' || $tblroles->roleDescription == 'Sliders' || $tblroles->roleDescription == 'Vendor' || $tblroles->roleDescription == 'Settings'){
                        ?>
                        <div class="col-sm-12">
                          <div class="checkbox">
                            <label class="i-checks">
                              <input type="checkbox" value="<?php echo $tblroles->roleCode; ?>" ng-model="user.<?php echo $tblroles->roleCode; ?>"><i></i> <?php echo $tblroles->roleDescription; ?>
                            </label>
                          </div>
                          <div class="col-sm-12" ng-if="user.<?php echo $tblroles->roleCode; ?>">
                            <div class="row">
                            <?php
                              if($tblroles->roleDescription == 'Donations'){
                                $subrole = explode(",", $tblroles->rolePage);
                                $n=0;
                                foreach ($subrole as $subrole ) { ?>

                                  <div class="col-sm-6">
                                    <label class="i-checks">
                                      <input type="checkbox" value="<?php echo trim($subrole); ?>" ng-model="user.<?php echo trim($subrole);?>"><i></i> <?php echo trim($subrole);?>
                                    </label>
                                  </div>
                                <?php
                                }
                              }?>
                            </div>
                          </div>
                          <div class="line line-dashed b-b line-lg pull-in"></div>
                        </div>
                        <?php
                      }
                    } ?>
                  </div>
                  <!-- <button type="button" class="btn btn-success" ng-click="checksub(user)"><i class="glyphicon glyphicon-save" ></i> chk</button> -->
                </div>
              </div>

            </div>
          </div>
        </div>


        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              User Information
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                    <label class="col-sm-12 control-label">Firstname</label>
                    <div class="col-sm-12">
                      <input type="text" id="firstname" name="firstname" class="form-control" ng-model="user.firstname" required="required">
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12 control-label">Lastname</label>
                    <div class="col-sm-12">
                      <input type="text" id="lastname" name="lastname" class="form-control" ng-model="user.lastname" required="required">
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12 control-label">Gender</label>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="radio">
                            <label class="i-checks">
                              <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                              <i></i>
                              Male
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="radio">
                            <label class="i-checks">
                              <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                              <i></i>
                              Female
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 propic">
                  <!-- <a href="" class="select-profile-pic" id="change-picture" ngf-change="prepare(file)" ngf-select ng-model="file" ngf-multiple="false" required="required"><i class="fa fa-file-photo-o"></i></a> -->
                  <!-- <img ngf-src="files[0]" name="profpic" id="profpic" ngf-default-src="{[{base_url}]}/images/default_images/default-page-thumb.png" required="required"> -->
                  <label class="label_profile_pic btn btn-default" id="change-picture" ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">Select Photo</label>
                  <img src="{[{base_url}]}/images/default_images/default-page-thumb.png" name="profpic" id="profpic" ng-if="imageselected == false">
                  <img ngf-src="files[0]" ngf-default-src="{[{base_url}]}/images/default_images/default-page-thumb.png" name="profpic" id="profpic" ng-if="imageselected == true">
                  <!-- <button type="button" ng-click="chkimage(files)">clear image</button> -->
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                  <div class="line line-dashed b-b line-lg"></div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-12 control-label">Birthday</label>
                <div class="col-sm-12">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input type="hidden" ng-model="user.birthday2">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.birthday" is-open="$parent.opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-12 control-label">State</label>
                <div class="col-lg-12">
                  <select ui-jq="chosen" class="w-md" name="state" ng-model="user.state" required="required">
                    <optgroup label="Alaskan/Hawaiian Time Zone">
                      <option value="AK">Alaska</option>
                      <option value="HI">Hawaii</option>
                    </optgroup>
                    <optgroup label="Pacific Time Zone">
                      <option value="CA">California</option>
                      <option value="NV">Nevada</option>
                      <option value="OR">Oregon</option>
                      <option value="WA">Washington</option>
                    </optgroup>
                    <optgroup label="Mountain Time Zone">
                      <option value="AZ">Arizona</option>
                      <option value="CO">Colorado</option>
                      <option value="ID">Idaho</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NM">New Mexico</option>
                      <option value="ND">North Dakota</option>
                      <option value="UT">Utah</option>
                      <option value="WY">Wyoming</option>
                    </optgroup>
                    <optgroup label="Central Time Zone">
                      <option value="AL">Alabama</option>
                      <option value="AR">Arkansas</option>
                      <option value="IL">Illinois</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="OK">Oklahoma</option>
                      <option value="SD">South Dakota</option>
                      <option value="TX">Texas</option>
                      <option value="TN">Tennessee</option>
                      <option value="WI">Wisconsin</option>
                    </optgroup>
                    <optgroup label="Eastern Time Zone">
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="IN">Indiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="OH">Ohio</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WV">West Virginia</option>
                    </optgroup>
                  </select>
                  <div class="line line-dashed b-b line-lg pull-in"></div>

                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Country</label>
                <label class="col-lg-10 control-label">Soon, Now only in USA</label>
              </div>

            </div>

          </div>
        </div>
        <div class="col-sm-12">
          <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="userform.$invalid || invalidemail == true || passwordInvalid == true || passwordMin == true"  ng-click="submitData(user, files)"><i class="glyphicon glyphicon-save" ></i> Create User</button>
            </footer>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
</fieldset>
</form>