<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletepageimgModal.html">
  <div ng-include="'/tpl/deletepageimgModal.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Page</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formPage" id="formPage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
            <div class="panel-body">

              <!-- <label class="col-sm-12 control-label">
                <label for="title">Menu Page Category</label>
              </label>
              <div class="col-sm-12">
                <select class="form-control" ng-model="page.menutitle" >
                  <option value="what-is-eco"> What is ECO? </option>
                  <option value="learn"> Learn </option>
                  <option value="donate"> Donate</option>
                </select>
                <br>
              </div> -->

              <label class="col-sm-12 control-label">
                <label for="title">Title</label>
              </label>
              <div class="col-sm-12">
                <!-- ngIf: user.usernametaken == true -->
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title,page)">
                <br>
                <b class="pull-left">Page Slugs: &nbsp;&nbsp; </b>
                <input type="hidden" style="text-transform: lowercase;" ng-model="page.slugs">
                <div ng-show="editslug" class="col-sm-3 pull-left">
                  <input type="text" id="pageslugs" class="form-control" ng-model="page.slugs" ng-keypress="onslugs(page.slugs);">
                </div>
                <span ng-bind="page.slugs" class="pull-left mg-left"></span>

                <div ng-show="editslug">
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(page.title)">cancel</a>
                  <button type="button" class="btn btn-primary btn-xs pull-right mg-left" ng-disabled="validslugs" ng-click="setslug(page.slugs)">ok</button>
                </div>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editpageslug(page.slugs)">edit slug</a>
              
                <br>
                <br>
              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="form-group">
                <label class="col-sm-12 control-label">Body Content</label>
                <div class="col-sm-12">

                  <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>

                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>




          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Banner
              <label class="pull-right i-switch bg-info m-t-xs m-r" ng-init="page.banneropt = true">
               <input type="checkbox" ng-model="page.banneropt" ng-checked="checked" ng-init="checked=true">
               <i></i>
             </label>
            </div>
            <div class="panel-body" ng-show="page.banneropt">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{ page.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner"  placeholder="Paste Banner link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.banner)" id="banner">
              <label ng-show="n" class="control-label p" style="color:#a94442;" ng-cloak>
                This field is required.</label>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <b>Banner Text</b> <br><br>
              <input type="text" id="bannertitle" name="bannertitle" class="form-control" ng-model="page.bannertitle" placeholder="Banner Title">
              <div class="row">
                <div class="col-xs-3">
                  Font size:
                  <select class="form-control" ng-model="page.titlefontsize">
                    <option ng-repeat="val in tfsize" value="{[{val.val}]}">{[{val.val}]}</option>
                  </select>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <textarea id="bannertext" name="bannertext" class="form-control" ng-model="page.bannertext" placeholder="Banner Text" maxlength="400" class="form-control" style="resize:vertical;"></textarea>
              <div class="row">
                <div class="col-xs-3">
                  Font size:
                  <select class="form-control" ng-model="page.descriptionfontsize">
                    <option ng-repeat="val in dfsize" value="{[{val.val}]}">{[{val.val}]}</option>
                  </select>
                </div>
              </div>
              <div class="line line-dashed line-lg b-b"></div>
              <div class="font-bold">Button
                <em>(Optional)</em></div>
                <div class="line line-dashed line-lg b-b"></div>
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">Button Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnname">
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">Button Link</label>
                  <div class="col-sm-10">
                    <input type="text" name="" ng-space class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.btnlink" placeholder="e.g.: http://www">
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

             <!--    <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">Button Color</label>
                  <div class="col-sm-10">
                  <input colorpicker="" ng-model="page.btncolor" ng-init="page.btncolor = '#ff9d0f' " type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left ng-touched" required="required" style="width:90px;margin-right:5%">
                  <div class="colorpicker-colorbox form-control pull-left" style="background-color:{[{page.btncolor}]}; width:90px"></div>
                  </div>
                </div> -->

            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta
            </div>
            <div class="panel-body">
              Title
              <input type="text" id="metatitle" name="metatitle" class="form-control" ng-model="page.metatitle" placeholder="Meta Title">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              Description
              <input type="text" id="metadescription" name="metadescription" class="form-control" ng-model="page.metadescription" placeholder="Meta Description">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              Keyword
              <input type="text" id="metakeyword" name="metakeyword" class="form-control" ng-model="page.metakeyword" placeholder="Meta Keyword">
            </div>
          </div>
        </div>

      </div>


      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-right bg-light lter">
            <button type="button" class="btn btn-default" ng-click="reset()">Clear Form</button>
            <button type="submit" ng-disabled="formPage.$invalid" class="btn btn-success">Submit</button>
          </footer>
        </div>
      </div>
    </div>
  </fieldset>
</form>


<div class="panel-body">
  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
  <div class="loader" ng-show="imageloader">
    <div class="loadercontainer">

      <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
      </div>
      Uploading your images please wait...

    </div>

  </div>

  <div ng-show="imagecontent && page.banneropt">

    <div class="col-sml-12">
      <div class="dragdropcenter">
        <div ngf-drop ngf-select ng-model="files" class="drop-box"
        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
        accept="image/*,application/pdf">Drop images here or click to upload</div>
      </div>
    </div>

    <div class="line line-dashed b-b line-lg"></div>

    <div class="col-sm-3" ng-repeat="data in data">
     <a href="" ng-click="deletepageimg(dlt)" class="closebutton">&times;</a>
     <input type="hidden" id="" name="id" ng-init="dlt.id=data.id" class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
     <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
     <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}');">
     </div>
   </div>

 </div>


</div>

