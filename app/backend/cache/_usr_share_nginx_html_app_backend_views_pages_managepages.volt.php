<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="updatepage.html">
  <div ng-include="'/tpl/updatepageModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Page List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Pages
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" ng-click="search(searchtext)" type="button">Go!</button>
          </span>
        </div>
      </div>
      <div class="col-sm-1 m-b-xs">
        <div class="input-group">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
      <div class="col-sm-2"></div>
      <div class="col-sm-4">        
        <div class="input-group">
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
        </div> 
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>

            <th style="width:40%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'title','DESC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'title','ASC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'menutitle','DESC')"> Menu Category
                  <span ng-show="sortIn == 'menutitle' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'menutitle','ASC')"> Menu Category
                  <span ng-show="sortIn == 'menutitle' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td>
              <a href="<?php echo $this->config->application->baseURL; ?>/{[{ mem.pageslugs }]}" target="_blank">{[{ mem.title }]}</a>
            </td>
            <td>{[{ mem.menutitle }]}</td>

            <td  ng-if="mem.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-success">Active</span></div>
              <div class="checkstatuscontent float-left">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.pageid,searchtext,bigCurrentPage)">
                  <i></i>
                </label>
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
            </td>
            <td  ng-if="mem.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-light">Deactivated</span></div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-click="setstatus(mem.status,mem.pageid,searchtext,bigCurrentPage)">
                  <i></i>
                </label>                
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.pageid"><i class="fa fa-check"></i></spand></div>
            </td>
            <td>
              <a href="" ng-click="updatepage(mem.pageid)"><span class="label bg-warning" >Edit</span></a>
              <a href="" ng-click="deletepage(mem.pageid)"> <span class="label bg-danger">Delete</span>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-8">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small> 
        </div>
        <div class="col-sm-4">        
          <div class="input-group">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
</div>
</div>
