<div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <h1 class="m-n font-thin h3 text-black">Dashboard</h1>
          <small class="text-muted">ECO Content Management System</small>
        </div>
        <div class="col-sm-6 text-right hidden-xs">
          <!-- <div class="inline m-r text-left">
            <div class="m-b-xs">1290 <span class="text-muted">Unique Visitors</span></div>
            <div ng-init="data1=[ 106,108,110,105,110,109,105,104,107,109,105,100,105,102,101,99,98 ]" 
              ui-jq="sparkline" 
              ui-options="{[{ data1 }]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}" 
              class="sparkline inline">loading...
            </div>
          </div>
          <div class="inline text-left">
            <div class="m-b-xs">$30,000 <span class="text-muted">Page Visits</span></div>
            <div ng-init="data2=[ 105,102,106,107,105,104,101,99,98,109,105,100,108,110,105,110,109 ]" 
              ui-jq="sparkline" 
              ui-options="{[{data2}]}, {type:'bar', height:20, barWidth:5, barSpacing:1, barColor:'#dce5ec'}" 
              class="sparkline inline">loading...
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <!-- / main header -->
    <div class="wrapper-md" ng-controller="loadDonationsCtrl">
      <!-- stats -->
      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
            ECO Donations
            </div>
            <div class="row row-sm text-center">
              <div class="col-xs-6">
                <div class="panel padder-v item">
                  <div class="h1 text-info font-thin h1">{[{ userscount }]}</div>
                  <span class="text-muted text-xs">Donated</span>
                </div>
              </div>
              <div class="col-xs-6">
                <a href class="block panel padder-v bg-primary item">
                  <span class="text-white font-thin h1 block">{[{ memberscount }]}</span>
                  <span class="text-muted text-xs">Members</span>
                  <span class="bottom text-right">
                    <i class="fa fa-user text-muted m-r-sm"></i>
                  </span>
                </a>
              </div>
              <div class="col-xs-12 m-b-md">
                <div class="r bg-light dker item hbox no-border">
               <!--  <div class="col w-xs v-middle hidden-md">
                  <div ng-init="data1=[70,30]" ui-jq="sparkline" ui-options="{[{data1}]}, {type:'pie', height:40, sliceColors:['{[{app.color.warning}]}','#fff']}" class="sparkline inline"></div>
                </div> -->
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>${[{ donationcount }]}</span></div>
                  <span class="text-muted text-xs">Donations</span>
                  <span class="bottom text-right">
                    <i class="fa fa-dollar text-muted m-r-sm"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Nepal Donations
          </div>
          <div class="row row-sm text-center">
            <div class="col-xs-12">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{ userscountNepal }]}</div>
                <span class="text-muted text-xs">Donated</span>
              </div>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-light dker item hbox no-border">
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>${[{ donationcountNepal }]}</span></div>
                  <span class="text-muted text-xs">Donations</span>
                  <span class="bottom text-right">
                    <i class="fa fa-dollar text-muted m-r-sm"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            NY Walk Donations
          </div>
          <div class="row row-sm text-center">
            <div class="col-xs-12">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{ userscountNy }]}</div>
                <span class="text-muted text-xs">Donated</span>                
              </div>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-light dker item hbox no-border">
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>${[{ donationcountNy }]}</span></div>
                  <span class="text-muted text-xs">Donations</span>
                  <span class="bottom text-right">
                    <i class="fa fa-dollar text-muted m-r-sm"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Houston Walk Donations
          </div>
          <div class="row row-sm text-center">
            <div class="col-xs-12">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{ userscountHouston }]}</div>
                <span class="text-muted text-xs">Donated</span>
              </div>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-light dker item hbox no-border">
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>${[{ donationcountHouston }]}</span></div>
                  <span class="text-muted text-xs">Donations</span>
                  <span class="bottom text-right">
                    <i class="fa fa-dollar text-muted m-r-sm"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="panel panel-default">
          <div class="panel-heading font-bold">Spline</div>
          <div class="panel-body">
            <div ui-jq="plot" ui-options="
              [
                { data: {[{d}]}, points: { show: true, radius: 6}, splines: { show: true, tension: 0.45, lineWidth: 5, fill: 0 } }
              ], 
              {
                colors: ['{[{app.color.info}]}'],
                series: { shadowSize: 3 },
                xaxis:{ 
                  font: { color: '#ccc' },
                  position: 'bottom',
                  ticks: [
                    [ 1, 'Jan' ], [ 2, 'Feb' ], [ 3, 'Mar' ], [ 4, 'Apr' ], [ 5, 'May' ], [ 6, 'Jun' ], [ 7, 'Jul' ], [ 8, 'Aug' ], [ 9, 'Sep' ], [ 10, 'Oct' ], [ 11, 'Nov' ], [ 12, 'Dec' ]
                  ]
                },
                yaxis:{ font: { color: '#ccc' } },
                grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
                tooltip: true,
                tooltipOpts: { content: '%x.1 is %y.4',  defaultTheme: false, shifts: { x: 0, y: 20 } }
              }
            " style="height:240px" >
            </div>
          </div>
        </div> -->
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Seattle Walk Donations
          </div>
          <div class="row row-sm text-center">
            <div class="col-xs-12">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{ userscountSeattle }]}</div>
                <span class="text-muted text-xs">Donated</span>
              </div>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-light dker item hbox no-border">
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>${[{ donationcountSeattle }]}</span></div>
                  <span class="text-muted text-xs">Donations</span>
                  <span class="bottom text-right">
                    <i class="fa fa-dollar text-muted m-r-sm"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
      </div>
      <!-- / stats -->

      <!-- service -->
      <div class="panel hbox hbox-auto-xs no-border">
        <!-- <div class="col wrapper">
          <i class="fa fa-circle-o text-info m-r-sm pull-right"></i>
          <h4 class="font-thin m-t-none m-b-none text-primary-lt">System Performance</h4>
          <span class="m-b block text-sm text-muted">Service report of this year (updated 1 hour ago)</span>
          <div ui-jq="plot" ui-options="
            [
              { data: {[{d4}]}, lines: { show: true, lineWidth: 1, fill:true, fillColor: { colors: [{opacity: 0.2}, {opacity: 0.8}] } } }
            ], 
            {
              colors: ['{[{app.color.light}]}'],
              series: { shadowSize: 3 },
              xaxis:{ show:false },
              yaxis:{ font: { color: '#a1a7ac' } },
              grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#dce5ec' },
              tooltip: true,
              tooltipOpts: { content: '%s of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 10, y: -25 } }
            }
          " style="height:240px" >
          </div>
        </div>
        <div class="col wrapper-lg w-lg bg-light dk r-r">
          <h4 class="font-thin m-t-none m-b">Reports</h4>
          <div class="">
            <div class="text-center-folded">
              <span class="pull-right text-primary">60%</span>
              <span class="hidden-folded">Memory</span>
            </div>
            <progressbar value="60" class="progress-xs m-t-sm bg-white" animate="true" type="primary"></progressbar>
            <div class="text-center-folded">
              <span class="pull-right text-info">35%</span>
              <span class="hidden-folded">Disk</span>
            </div>
            <progressbar value="35" class="progress-xs m-t-sm bg-white" animate="true" type="info"></progressbar>
            <div class="text-center-folded">
              <span class="pull-right text-warning">25%</span>
              <span class="hidden-folded">Server Process</span>
            </div>
            <progressbar value="25" class="progress-xs m-t-sm bg-white" animate="true" type="warning"></progressbar>
          </div>
          <p class="text-muted">Dales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis</p>
        </div> -->
      </div>
      <!-- / service -->
    </div>
  </div>
  <!-- / main -->
  <!-- right col -->
  <div class="col w-md bg-white-only b-l bg-auto bg-auto-right no-border-xs">
    <!-- <tabset class="nav-tabs-alt" justified="true">
      <tab>
        <tab-heading>
          <i class="glyphicon glyphicon-list text-md text-muted wrapper-sm"></i>
        </tab-heading>
        <div class="wrapper-md">
          <div class="m-b-sm text-md">Time Line</div>
    <div class="padder-md">    -->   
      <!-- streamline -->
      <!-- <div class="streamline b-l m-b">
        <div class="sl-item">
          <div class="m-l">
            <div class="text-muted">2 minutes ago</div>
            <p><a href class="text-info">God</a> is now following you.</p>
          </div>
        </div>
        <div class="sl-item">
          <div class="m-l">
            <div class="text-muted">11:30</div>
            <p>Join comference</p>
          </div>
        </div>
        <div class="sl-item b-success b-l">
          <div class="m-l">
            <div class="text-muted">10:30</div>
            <p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
          </div>
        </div>
        <div class="bg-info wrapper-sm m-l-n m-r-n m-b r r-2x">
          Create tasks for the team
        </div>
        <div class="sl-item b-primary b-l">
          <div class="m-l">
            <div class="text-muted">Wed, 25 Mar</div>
            <p>Finished task <a href class="text-info">Testing</a>.</p>
          </div>
        </div>
        <div class="sl-item b-warning b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 10 Mar</div>
            <p>Trip to the moon</p>
          </div>
        </div>
        <div class="sl-item b-info b-l">
          <div class="m-l">
            <div class="text-muted">Sat, 5 Mar</div>
            <p>Prepare for presentation</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Sun, 11 Feb</div>
            <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 17 Jan</div>
            <p>Follow up to close deal</p>
          </div>
        </div>
      </div> -->
      <!-- / streamline -->
    <!-- </div>
        </div>
      </tab>
      <tab>
        <tab-heading>
          <i class="glyphicon glyphicon-map-marker text-md text-muted wrapper-sm"></i>
        </tab-heading>
        <div class="wrapper-md">
          <div class="m-b-sm text-md">Latest Registered User</div>
          <ul class="list-group no-bg no-borders pull-in">
            <li class="list-group-item">
              <a herf class="pull-left thumb-sm avatar m-r">
                <img src="/img/a4.jpg" alt="..." class="img-circle">
                <i class="on b-white bottom"></i>
              </a>
              <div class="clear">
                <div><a href>Chris Fox</a></div>
                <small class="text-muted">Designer, Blogger</small>
              </div>
            </li>
            <li class="list-group-item">
              <a herf class="pull-left thumb-sm avatar m-r">
                <img src="/img/a5.jpg" alt="..." class="img-circle">
                <i class="on b-white bottom"></i>
              </a>
              <div class="clear">
                <div><a href>Mogen Polish</a></div>
                <small class="text-muted">Writter, Mag Editor</small>
              </div>
            </li>
            <li class="list-group-item">
              <a herf class="pull-left thumb-sm avatar m-r">
                <img src="/img/a6.jpg" alt="..." class="img-circle">
                <i class="busy b-white bottom"></i>
              </a>
              <div class="clear">
                <div><a href>Joge Lucky</a></div>
                <small class="text-muted">Art director, Movie Cut</small>
              </div>
            </li>
            <li class="list-group-item">
              <a herf class="pull-left thumb-sm avatar m-r">
                <img src="/img/a7.jpg" alt="..." class="img-circle">
                <i class="away b-white bottom"></i>
              </a>
              <div class="clear">
                <div><a href>Folisise Chosielie</a></div>
                <small class="text-muted">Musician, Player</small>
              </div>
            </li>
            <li class="list-group-item">
              <a herf class="pull-left thumb-sm avatar m-r">
                <img src="/img/a8.jpg" alt="..." class="img-circle">
                <i class="away b-white bottom"></i>
              </a>
              <div class="clear">
                <div><a href>Aron Gonzalez</a></div>
                <small class="text-muted">Designer</small>
              </div>
            </li>
          </ul>
          <div class="text-center">
            <a href class="btn btn-sm btn-primary padder-md m-b">More Missions</a>
          </div>
        </div>
      </tab>
      <tab>
        <tab-heading>
          <i class="glyphicon glyphicon-transfer text-md text-muted wrapper-sm"></i>
        </tab-heading>
        <div class="wrapper-md">
          <div class="m-b-sm text-md">Latest Transaction</div>
          <ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t">
            <li class="list-group-item">
              <span class="text-muted">Transfer to Jacob at 3:00 pm</span>
              <span class="block text-md text-info">B 15,000.00</span>
            </li>
            <li class="list-group-item">
              <span class="text-muted">Got from Mike at 1:00 pm</span>
              <span class="block text-md text-primary">B 23,000.00</span>
            </li>
            <li class="list-group-item">
              <span class="text-muted">Sponsored ORG at 9:00 am</span>
              <span class="block text-md text-warning">B 3,000.00</span>
            </li>
            <li class="list-group-item">
              <span class="text-muted">Send to Jacob at 8:00 am</span>
              <span class="block text-md">B 11,000.00</span>
            </li>
          </ul>
        </div>
      </tab>      
    </tabset> -->

  </div>
  <!-- / right col -->
</div>