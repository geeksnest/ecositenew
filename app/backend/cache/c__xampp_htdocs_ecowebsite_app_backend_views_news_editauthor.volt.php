<?php echo $this->getContent(); ?>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Authors</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateAuthor(author)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              Authors Information
            </div>


              <div class="panel-body">

                <label class="col-sm-2 control-label">
                  <label for="title">Name</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="name" name="name" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.name" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 

                <label class="col-sm-2 control-label">
                  <label for="title">Location</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="location" name="location" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.location" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 

                <label class="col-sm-2 control-label">
                  <label for="title">Occupation</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="occupation" name="occupation" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="author.occupation" required="required">
                  
                  <div class="line line-dashed b-b line-lg"></div>
                </div> 

                <label class="col-sm-2 control-label">
                  <label for="title">Author Since</label>
                </label>
                <div class="col-sm-10">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="author.authorsince" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>                  
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
            
                <label class="col-sm-2 control-label">
                  <label for="title">About the Author</label>
                </label>
                <div class="col-sm-10">
                  <textarea class="ck-editor" ng-model="author.about" required="required" class="form-control" style="resize:vertical;"></textarea>
                </div>
                
              </div>

          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Photo of the Author
            </div>
            <div class="panel-body">
              <img ng-show="author.photo" src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{ author.photo }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg"></div>
              <input type="text" id="photo" name="photo" class="form-control" ng-model="author.photo"  placeholder="Paste Photo link here..." required="required" onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(author.photo)">
            </div>
          </div>
        </div>



      </div>


      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>
    </div>
  </fieldset>
</form>

<div class="panel-body">
  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
  <div class="loader" ng-show="imageloader">
    <div class="loadercontainer">

      <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
      </div>
      Uploading your images please wait...

    </div>

  </div>

  <div ng-show="imagecontent">

    <div class="col-sml-12">
      <div class="dragdropcenter">
        <div ngf-drop ngf-select ng-model="files" class="drop-box" 
        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
        accept="image/*,application/pdf">Drop images here or click to upload</div>
      </div>
    </div>

    <div class="line line-dashed b-b line-lg"></div>

    <div class="col-sm-3" ng-repeat="data in imagelist">
     <a href="" ng-click="deleteauthorimg(dlt)" class="closebutton">&times;</a>
     <input type="hidden" id="" name="id" ng-init="dlt.id=data.id" class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
     <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
     <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/{[{data.filename}]}');">
     </div>
   </div>

 </div>


</div>


