<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="reviewclub.html">
  <div ng-include="'/tpl/reviewclub.html'"></div>
</script>
<script type="text/ng-template" id="notification2.html">
  <div ng-include="'/tpl/notification2.html'"></div>
</script>
<script type="text/ng-template" id="appDisappProj.html">
  <div ng-include="'/tpl/appDisappProj.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">ECO CLUB List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row text-center">
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 ">
          List of Clubs
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row" ng-show="advancesearch == false">
        <div class="col-xs-5 ">
          <div class="input-group">
            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
            </span>
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
            </span>
          </div>
        </div>
      </div>
      <div class="checkbox">
        <label class="i-checks">
          <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by Date
        </label>
      </div>
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row" ng-show="advancesearch == true">
        <div class="col-md-7">
          <div class="row">

            <div class="col-xs-12">
              Date
            </div>
            <div class="col-xs-4" style="padding-right:0px;">
              Year:<br/>
              <select ng-model="recurring.year" class="input-sm form-control">
                <option value="">----</option>
                <?php for ($year=2014; $year <= date('Y'); $year++) {
                  echo "<option value='".$year."'>".$year."</option>";
                }?>
              </select>
            </div>
            <div class="col-xs-5" style="padding:0px 5px;">
              Month:<br/>
              <select ng-model="recurring.month" class="input-sm form-control">
                <option value="">----</option>
                <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); 
                ?>
                <?php foreach ($formonths as $index => $formonth) {
                  echo "<option value='".$index."'>".$formonth."</option>";
                }?>
              </select>
            </div>
            <div class="col-xs-3" style="padding:0px 15px 0px 0px;">
              Day:<br/>
              <select ng-model="recurring.day" class="input-sm form-control">
                <option value="">--</option>
                <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                echo "<option value='".$days."'>".$days."</option>";} 
                ?>
              </select>
            </div>
          </div>

        </div>
        <div class="col-md-1"><br/><br/>
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="searchbydate(recurring)">Go!</button>
          </span>
          <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
            </span>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped b-t b-light">
      <thead class="manage">
        <tr>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'name','DESC')"> Club Name
                <span ng-show="sortIn == 'name' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'name','ASC')"> Club Name
                <span ng-show="sortIn == 'name' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'firstname','DESC')"> Created by
                <span ng-show="sortIn == 'firstname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'firstname','ASC')"> Created by
                <span ng-show="sortIn == 'firstname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'datecreated','DESC')"> Date Created
                <span ng-show="sortIn == 'datecreated' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'datecreated','ASC')"> Date Created
                <span ng-show="sortIn == 'datecreated' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'date_created','DESC')"> Date Registered
                <span ng-show="sortIn == 'date_created' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'date_created','ASC')"> Date Registered
                <span ng-show="sortIn == 'date_created' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'website','DESC')"> Website
                <span ng-show="sortIn == 'website' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'website','ASC')"> Website
                <span ng-show="sortIn == 'website' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'contactnumber','DESC')"> Contact number
                <span ng-show="sortIn == 'contactnumber' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'contactnumber','ASC')"> Contact number
                <span ng-show="sortIn == 'contactnumber' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'contactperson','DESC')"> Contact person
                <span ng-show="sortIn == 'contactperson' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'contactperson','ASC')"> Contact person
                <span ng-show="sortIn == 'contactperson' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th  style="width:10%">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="data in data.data">
          <td>{[{ data.name }]}</td>
          <td>{[{ data.firstname }]} {[{ data.lastname }]}</td>
          <td>{[{ data.datecreated | dateToISO | date:'mediumDate' }]}</td>
          <td>{[{ data.date_created | dateToISO | date:'mediumDate' }]}</td>
          <td>{[{ data.website }]}</td>
          <td>{[{ data.contactnumber }]}</td>
          <td>{[{ data.contactperson }]}</td>
          <td>
            <label ng-if="data.status == 'Submitted'" class="label bg-info inline m-t-sm">New</label>
            <label ng-if="data.status == 'Approved'" class="label bg-success inline m-t-sm">Approved</label>
            <label ng-if="data.status == 'Disapproved'" class="label bg-danger inline m-t-sm">Disapproved</label>
          </td>
          <td>
            <div class="row">
              <div class="col-xs-4">
                <a href="" ng-click="review(data.id)" class="manage-action" title="Review"><i class="fa fa-search"></i></a>
              </div>
              <div class="col-xs-4">
                <a href="" ng-click="delete(data.id)" ng-if="data.status != 'Approved'" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <footer class="panel-footer">
    <div class="row">
      <div class="col-sm-6">
        <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
      </div>
      <div class="col-sm-2">
      </div>
      <div class="col-sm-4">
        <div class="input-group">
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
        </div>
      </div>
    </div>
  </footer>

</div>
