<?php echo $this->getContent(); ?>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of Registered Users</h1>
  <a id="top"></a>
</div>
<script type="text/ng-template" id="editMemberModal.html">
  <div ng-include="'/tpl/editMemberModal.html'"></div>
</script>
<script type="text/ng-template" id="resendconfirmModal.html">
  <div ng-include="'/tpl/resendconfirmModal.html'"></div>
</script>
<script type="text/ng-template" id="resendcompleteModal.html">
  <div ng-include="'/tpl/resendcompleteModal.html'"></div>
</script>
<script type="text/ng-template" id="deleteMemberModal.html">
  <div ng-include="'/tpl/deleteMemberModal.html'"></div>
</script>
<script type="text/ng-template" id="resetpassModal.html">
  <div ng-include="'/tpl/resetpassModal.html'"></div>
</script>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-12">          
          <div class="row">
            <div class="col-xs-11">
              Manage Members
            </div>
            <div class="col-sm-1">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-4">

          <div class="input-group" ng-hide="advancesearch">
            <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
            </span>
          </div>

          <div class="checkbox">
            <label class="i-checks">
            <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by
            </label>
          </div>

          <div ng-show="advancesearch">

            <div class="radio">
              <label class="i-checks">
                <input type="radio" name="searchby" value="status" ng-model="searchby" checked><i></i> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </label>
              <label class="i-checks">
                <input type="radio" name="searchby" value="birthday" ng-model="searchby"><i></i> Birthday
              </label> 
            </div>

            <div ng-show="searchby == 'status'">
              <div class="row">
                <div class="col-sm-11">
                  <select class="input-sm form-control" name="searchtext" ng-model="searchtext">
                    
                    <option value="0">Unconfirmed Email</option>
                    <option value="1">Completed</option>
                    <option ng-value="2">Incomplete</option>
                  </select>
                </div>
                <div class="col-sm-1">
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                  </span>
                </div>
              </div>
            </div>

            <div class="form-group" ng-show="searchby == 'birthday'">

              <div class="row">

                <div class="col-sm-5">
                  Month:<br/>
                  <select ng-model="searchbd.bmonth" class="input-sm form-control">
                    <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); ?>
                    <?php foreach ($formonths as $index => $formonth) { 
                      echo "<option value='".$index."'>".$formonth."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-3">
                  Day:<br/>
                  <select ng-model="searchbd.bday" class="input-sm form-control">
                    <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                    echo "<option value='".$days."'>".$days."</option>";
                    } ?>
                  </select>
                </div>
                <div class="col-sm-3">
                  Year:<br/>
                  <select ng-model="searchbd.byear" class="input-sm form-control">
                    <?php for ($year=1900; $year < 2015; $year++) { 
                      echo "<option value='".$year."'>".$year."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <br/>
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="searchbydate(searchbd)">Go!</button>
                  </span>
                </div>
              </div>

            </div>

          </div>

        </div>

        <div class="col-sm-4"> 
          
        </div> 

        <div class="col-sm-4">          
          <div class="input-group" ng-hide="searchby == 'birthday'">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div>         
          <div class="input-group" ng-show="searchby == 'birthday'">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPagebydate(bigCurrentPage,searchbd)" style="margin:0px;"></pagination>
          </div>
        </div> 
      </div>
    </div>
    <div class="table-responsive">
      <input type="hidden">
     <table class="table table-striped m-b-none">
        <thead class="manage">
          <tr>
            
            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('username','DESC')"> Username 
                  <span ng-show="sortIn == 'username' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('username','ASC')"> Username 
                  <span ng-show="sortIn == 'username' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>              
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('firstname','DESC')"> Firstname 
                  <span ng-show="sortIn == 'firstname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('firstname','ASC')"> Firstname 
                  <span ng-show="sortIn == 'firstname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>  
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('lastname','DESC')"> Lastname 
                  <span ng-show="sortIn == 'lastname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('lastname','ASC')"> Lastname 
                  <span ng-show="sortIn == 'lastname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span> 
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('birthday','DESC')"> Birhtday 
                  <span ng-show="sortIn == 'birthday' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('birthday','ASC')"> Birhtday 
                  <span ng-show="sortIn == 'birthday' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span> 
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('email','DESC')"> Email 
                  <span ng-show="sortIn == 'email' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('email','ASC')"> Email 
                  <span ng-show="sortIn == 'email' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span> 
            </th>

            <th style="width:12%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('location','DESC')"> Location 
                  <span ng-show="sortIn == 'location' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('location','ASC')"> Location 
                  <span ng-show="sortIn == 'location' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:8%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('zipcode','DESC')"> Zip Code  
                  <span ng-show="sortIn == 'zipcode' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('zipcode','ASC')"> Zip Code  
                  <span ng-show="sortIn == 'zipcode' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType('status','DESC')"> Status 
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType('status','ASC')"> Status 
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th  style="width:15%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td>{[{ mem.username }]}</td>
            <td>{[{ mem.firstname }]} </td>
            <td>{[{ mem.lastname }]}</td>
            <td>{[{ mem.birthday }]}</td>
            <td>{[{ mem.email }]}</td>
            <td>{[{ mem.location }]}</td>
            <td>{[{ mem.zipcode }]}</td>
            <td ng-if="mem.status == 0">Unconfirmed Email</td>
            <td ng-if="mem.status == 1">Completed</td>
            <td ng-if="mem.status == 2">Incomplete</td>
            <td>
              <a href="" style="display: block;" ng-click="resendcom(mem.email)" ng-if="mem.status == 2" ><span class="label bg-primary">Resend Email Completion</span></a> 
              <a href="" style="display: block;" ng-click="resendcon(mem.email)" ng-if="mem.status == 0" ><span class="label bg-info">Resend Email Confirmation</span></a> 
              
              <a href="" ng-click="edituser(mem.userid)"><span class="label bg-warning">Edit</span></a> 
              <a href="" ng-click="deleteuser(mem.userid, mem.firstname)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6">
        <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
          <div class="input-group" ng-hide="searchby == 'birthday'">
            <!-- <footer class="panel-footer bg-light lter"> -->
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm " boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)"></pagination>
            <!-- </footer> -->
          </div>
          <div class="input-group" ng-show="searchby == 'birthday'">
            <!-- <footer class="panel-footer bg-light lter"> -->
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm " boundary-links="true" ng-click="setPagebydate(bigCurrentPage,searchbd)"></pagination>
            <!-- </footer> -->
          </div>
        </div>
      </div>
    </footer>
  </div>
</div>