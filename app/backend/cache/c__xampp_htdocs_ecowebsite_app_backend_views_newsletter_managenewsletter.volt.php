<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletenewsletter.html">
    <div ng-include="'/tpl/deletenewsletterModal.html'"></div>
</script>
<script type="text/ng-template" id="updatenewsletter.html">
    <div ng-include="'/tpl/updatenewsletterModal.html'"></div>
</script>
<script type="text/ng-template" id="sendnewsletter.html">
    <div ng-include="'/tpl/sendnewsletterModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">News Letter List</h1>
    <a id="top"></a>
</div>
<div class="wrapper-md">
   <div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Manage News Letter
            </div>
            <div class="row wrapper">
                <div class="col-sm-5 m-b-xs">
                    <div class="input-group">
                        <input class="input-sm form-control" placeholder="Search" type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                    <thead>
                        <tr>
                            <th style="width:20px;">
                                <label class="i-checks m-b-none">
                                    <input type="checkbox"><i></i>
                                </label>
                            </th>
                            <th style="width:80%">Title</th>
                            <th style="width:20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="mem in data.data">
                            <td>
                                <label class="i-checks m-b-none">
                                    <input name="post[]" type="checkbox"><i></i>
                                </label>
                            </td>
                            <td>{[{ mem.title }]}</td>
                        </td>
                        <td>
                            <a href="" ng-click="updatenewsletter(mem.newsletterid)"><span class="label bg-warning" >Edit</span></a>
                            <a href="" ng-click="deletenewsletter(mem.newsletterid)"> <span class="label bg-danger">Delete</span>
                            <a href="" ng-click="sendnewsletter(mem.newsletterid)"> <span class="label bg-primary">Send</span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <ul class="pagination">
                <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
                    <a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a>
                </li>
                <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
                    <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
                </li>
                <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
                    <a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a>
                </li>
            </ul>
        </footer>
    </div>
</div>
</div>
</div>
