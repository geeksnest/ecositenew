<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="addtags.html">
 <div ng-include="'/tpl/addtags.html'"></div>
</script>

<script type="text/ng-template" id="tagsDelete.html">
 <div ng-include="'/tpl/tagsDelete.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News Tags</h1>
  <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

    <div class="row">

      <div class="col-sm-8">

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Tags list
          </div>


          <div class="panel-body">
            <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addtags()"><i class="fa fa-plus" style="width=100%;"></i>Add New Tag
            </button>
          </div>

          <div class="panel-body">



            <div class="row wrapper">
              <div class="col-sm-5 m-b-xs">
                <div class="input-group">
                  <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                  </span>
                </div>
              </div>
              <div class="col-sm-1 m-b-xs">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
                  </span>
                </div>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-striped b-t b-light">
                <thead class="manage">
                  <tr>
                    <th  style="width:75%">
                      <a href="" ng-click="sortType = 'tags'; sortReverse = !sortReverse">Tag name
                        <span ng-show="sortType == 'tags' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'tags' && sortReverse" class="fa fa-caret-up"></span>
                      </a>
                    </th>
                    <th style="width:25%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="mem in data.data | filter:searchFish | orderBy:sortType:sortReverse" current-page="currentPage">
                    <td> <span editable-text="mem.tags" onbeforesave="updatetags($data, mem.id, mem)" e-pattern="[a-zA-Z0-9'\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ mem.tags }]}</span></td>

                    <td>
                      <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                      <a href="" ng-click="tagsDelete(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>


        </div>
      </div>

    </div>

    <div class="row">
      <div class="panel-body">
        <footer class="panel-footer text-center bg-light lter">
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
        </footer>
      </div>
    </div>

  </div>
</fieldset>

