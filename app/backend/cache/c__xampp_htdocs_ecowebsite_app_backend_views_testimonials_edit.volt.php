<?php echo $this->getContent(); ?>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Testimonial</h1>
  <a id="top"></a>
</div>

<form name="formtesti" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="update(info)">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="panel panel-default">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
               <input type="hidden" ng-model="info.name"    ng-model="info.id" disabled="">                  
              <div class="form-group pull-in clearfix">
                <div class="col-sm-6">
                  <label>Your name</label>
                  <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="info.name"    ng-model="info.name" disabled="">
                </div>
                <div class="col-sm-6">
                  <label>Email</label>
                  <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email"  ng-model="info.email"  placeholder="" disabled="">
                </div>
              </div>
              <div class="form-group">
                <label>Company/Organization</label>
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" ng-model="info.comporg"  placeholder="" ng-model="contact.url" disabled="">
              </div>
              <div class="form-group">
                <label>Message</label>
                <textarea class="form-control" rows="6" ng-model="info.message" placeholder="" required></textarea>
              </div>
            </div>
            <footer class="panel-footer text-right bg-light lter">
              <button type="submit" class="btn btn-success" ng-disabled="formtesti.$invalid">Save Update</button>
            </footer>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
</form>