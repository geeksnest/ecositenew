<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepageimgModal.html">
  <div ng-include="'/tpl/deletepageimgModal.html'"></div>
</script>
<script type="text/ng-template" id="mediagallery.html">
  <div ng-include="'/tpl/mediagallery.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Homepage Content</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formPage" id="formPage">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Information
            </div>
            <div class="panel-body">

              <label class="col-sm-12 control-label">
                <label for="title">Title</label>
              </label>
              <div class="col-sm-12">
                <input type="hidden" ng-model="page.id">
                <input type="text" ng-disabled="page.title == 'SHOP'" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required">
                <br>
              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <label class="col-sm-6 control-label">
                <label for="title">Sub page (optional)</label>
              </label>
              <label class="col-sm-6 control-label">
                <label for="title">Font color</label>
              </label>
              <div class="col-sm-6">
              <select ng-disabled="page.title == 'SHOP'" class="form-control" ng-model="page.subpage" >
                  <option value="" stlye="display:none">-Select-</option>
                  <option ng-repeat="submenulink in submenulink" value="{[{submenulink.submenuID}]}" ng-hide="submenulink.subname == 'NEWS'">{[{ submenulink.subname }]}</option>
                </select>
                <br>
              </div>
               <div class="col-sm-6">
                    <input required colorpicker class="form-control" ng-model="page.subpagecolor" type="text">
                </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-12 control-label">Body Content</label>
                <div class="col-sm-12">

                  <textarea class="ck-editor" name="myeditor" id="myeditor" ng-model="page.content" required="required"></textarea>

                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Background Image

              <!-- <a class="btn m-b-xs btn-xs btn-info pull-right" ng-click="stylepreview(page)"><i class="icon icon-eyeglasses"></i> Quick Preview</a> -->
             </label>
            </div>
            <div class="panel-body">
              <img ng-show="page.banner" src="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{ page.banner }]}" style="width: 100%">
              <div class="line line-dashed b-b line-lg" id="addme"></div>
              <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner"  placeholder="Paste Banner link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(page.banner)" id="banner" required>
              <label ng-show="n" class="control-label p" style="color:#a94442;" ng-cloak>
                This field is required.</label>
                <div class="line line-dashed b-b line-lg"></div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Circle/Buttons
              <button type="button" class="btn btn-info btn-sm btn-addon pull-right" ng-hide="addbtn==false"  ng-click="media('featured','banner','left')" ng-disabled="total==4 || sidebar.img"><i class="glyphicon  glyphicon-plus-sign "></i>Add Image</button>
               <p class="text-muted">Maximum of 4 images</p>
            </div>
            <div class="panel-body">
               <div class="col-sm-12" ng-show="addme">
             <label class="control-label p break" style="color:#a94442;" ng-cloak>
              <span ng-bind="notvalid"> </span> </label> <br>
            </div>
            <div class="col-sm-12" ng-show="addbtn==false">
              <button type="button" class="btn btn-default btn-xs pull-right" ng-click="leftCancel()"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
              <button type="button" class="btn btn-default btn-xs pull-right" ng-click="updateleftsidebar(currentEdit,sidebar)"><i class="glyphicon glyphicon-edit"></i>Update</button>
            <br> <br>
            </div>

              <div class="col-sm-12">
              <div style="margin-bottom:20px" ng-if="sidebar.img" class="col-sm-12 bg-light">
                  &nbsp;&nbsp;<i class="icon-arrow-right"></i> Image
                  <button type="button" class="btn btn-default btn-xs pull-right" ng-click="cancelprep()" ng-disabled="total==4" ng-if="sidebar.img && addbtn == true"><i class="glyphicon glyphicon-ban-circle"></i> Clear </button>
                  <button type="button" ng-if="sidebar.img" class="btn btn-default btn-xs pull-right" ng-click="media('featured','banner','left')"><i class="fa fa-pencil"></i> Change Image </button> <br><br>
                  <div class="">
                  <div class="col-sm-12">
                     <div ng-if="sidebar.img" style="background-image:url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{sidebar.img }]}');" class="square-img"></div><br>
                  </div>
                    <div class="col-sm-12" ng-if="sidebar.img">
                    <div class="form-group">
                      <input type="text" id="left" class="form-control" ng-model="sidebar.title" placeholder="Title*">
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" ng-model="sidebar.desc" placeholder="Short Description*"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" ng-model="sidebar.imglink" placeholder="Image-Link*">
                    </div>
                    <button type="button" class="btn btn-success btn-sm pull-right" ng-hide="addbtn==false" ng-click="addimage(sidebar)" ng-disabled="!sidebar.img || !sidebar.title || !sidebar.desc || !sidebar.imglink">
                    Add</button>
                    </div>
                    
                    
                  </div>
                </div>
                <div style="margin-bottom:20px" class="col-sm-12">
                <ul class="list-group no-radius sortrow" ui:sortable ng:model="page.image" id="list" >
                  <li ng-repeat="data in page.image track by $index" class="{[{page.image.length == 1 ? 'col-sm-12' : 'col-sm-6'}]} list-group-item sort-img" ng-hide="currentEdit == $index">
                  <span class="pull-right" style="font-size:14px"><a href="" ng-click="deleteleft($index,data.id)"><i class="fa fa-times-circle"></i></a></span>
                  <span class="pull-right" style="font-size:14px"><a href="" ng-click="editleft($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <div style="background-image:url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.img }]}');" class="bilog-img"></div>
                    <p class="img-title break" ng-bind="data.title"></p>
                    <p class="img-desc break" ng-bind="data.desc"></p>
                  </li>
                </ul>
                </div>

              </div>
            </div>
          </div>

        </div>

      </div>


      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-right bg-light lter">
            <button type="submit" ng-disabled="formPage.$invalid" class="btn btn-success">Update</button>
          </footer>
        </div>
      </div>
    </div>
  </fieldset>
</form>


<div class="panel-body">
  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
  <div class="loader" ng-show="imageloader">
    <div class="loadercontainer">

      <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
      </div>
      Uploading your images please wait...

    </div>

  </div>

  <div ng-show="imagecontent">

    <div class="col-sml-12">
      <div class="dragdropcenter">
        <div ngf-drop ngf-select ng-model="files" class="drop-box"
        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
        accept="image/*,application/pdf">Drop images here or click to upload</div>
      </div>
    </div>

    <div class="line line-dashed b-b line-lg"></div>

    <div class="col-sm-3" ng-repeat="data in data">
     <a href="" ng-click="deletepageimg(dlt)" class="closebutton">&times;</a>
     <input type="hidden" id="" name="id" ng-init="dlt.id=data.id" class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
     <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
     <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimages/{[{data.filename}]}');">
     </div>
   </div>

 </div>


</div>