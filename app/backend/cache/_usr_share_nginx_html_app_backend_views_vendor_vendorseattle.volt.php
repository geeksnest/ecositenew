<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deleteVendor.html">
  <div ng-include="'/tpl/deleteVendor.html'"></div>
</script>

<script type="text/ng-template" id="viewVendor.html">
  <div ng-include="'/tpl/viewVendor.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of Seattle Expo Vendors</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row text-center">
   <div class="col-xs-4">
    <div class="panel padder-v item">
      <div class="h1 text-info font-thin h1">${[{totalcollections}]}</div>
      <span class="text-muted text-xs">Total Collections</span>
      <span class="bottom text-right">
        <i class="fa fa-dollar text-muted m-r-sm"></i>
      </span>
    </div>
  </div>
  <div class="col-xs-4">
    <a class="block panel padder-v bg-primary item">
      <span class="text-white font-thin h1 block">{[{totalvendors}]}</span>
      <span class="text-muted text-xs">Total Vendors</span>
      <span class="bottom text-right">
        <i class="fa fa-user text-muted m-r-sm"></i>
      </span>
    </a>
  </div>
</div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-11 ">
          Manage Vendors
        </div>
        <div class="col-sm-1">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-4 ">
          <div class="input-group" ng-hide="advancesearch">
            <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext, paytypesort)">Go!</button>
            </span>
          </div>        
        </div>      
        <div class="col-xs-6 ">
          <div class="row">
            <div class="col-xs-2">
              Sort Pay Type
            </div>
            <div class="col-xs-3 input-group">
              <select class="input-sm form-control" ng-model="paytypesort" ng-change="paytype(searchtext, paytypesort)">
                <option value="all">All</option>
                <option value="CreditCard">CreditCard</option>
                <option value="eCHECK">eCHECK</option>
                <option value="Paypal">Paypal</option>
                <option value="Mail Check">Mail Check</option>
              </select>        
            </div>          
          </div>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'transactionId','DESC')"> TransactionID
                  <span ng-show="sortIn == 'transactionId' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'transactionId','ASC')"> TransactionID
                  <span ng-show="sortIn == 'transactionId' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'orgname','DESC')"> Business/Organization
                  <span ng-show="sortIn == 'orgname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'orgname','ASC')"> Business/Organization
                  <span ng-show="sortIn == 'orgname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'email','DESC')"> Email
                  <span ng-show="sortIn == 'email' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'email','ASC')"> Email
                  <span ng-show="sortIn == 'email' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>            

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'vendortype','DESC')"> Vendor Type
                  <span ng-show="sortIn == 'vendortype' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'vendortype','ASC')"> Vendor Type
                  <span ng-show="sortIn == 'vendortype' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>        

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'tables','DESC')"> Table
                  <span ng-show="sortIn == 'tables' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'tables','ASC')"> Table
                  <span ng-show="sortIn == 'tables' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>      

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>   

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'paymentmode','DESC')"> Pay Type
                  <span ng-show="sortIn == 'paymentmode' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'paymentmode','ASC')"> Pay Type
                  <span ng-show="sortIn == 'paymentmode' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>   

            <th style="width:14%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'billinginfofname','DESC')"> Billing Info
                  <span ng-show="sortIn == 'billinginfofname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'billinginfofname','ASC')"> Billing Info
                  <span ng-show="sortIn == 'billinginfofname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>  

            <th style="width:8%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'datetimestamp','DESC')"> Timestamp (UTC)
                  <span ng-show="sortIn == 'datetimestamp' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'datetimestamp','ASC')"> Timestamp (UTC)
                  <span ng-show="sortIn == 'datetimestamp' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>      
            
            <th  style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="don in data.data">
            <td>{[{ don.transactionId }]}</td>
            <td>{[{ don.orgname }]}</td>
            <td>{[{ don.email }]}</td>
            <td>{[{ don.vendortype }]}</td>
            <td>{[{ don.tables }]}</td>
            <td>
              <div ng-show="don.status == 1" class="pagestatuscontent fade-in-out"><span class="label bg-info">Paid</span></div>
              <div ng-hide="don.status == 1"class="pagestatuscontent fade-in-out"><span class="label bg-danger">Unpaid</span></div>
            </td>
            <td>{[{ don.paymentmode }]}</td>
            <td>{[{ don.billinginfofname }]} {[{ don.billinginfolname }]}</td>
            <td>{[{ don.datetimestamp }]}</td>
            <td>
              <div class="row">
                <div class="col-xs-6">
                  <a href="" ng-click="view(don.id)" class="manage-action" title="view"><i class="icon-eye"></i></a>
                </div>
                <div class="col-xs-6">
                  <a href="" ng-click="delete(don.id)" class="manage-action" title="delete"><i class="fa fa-trash-o"></i></a>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
          <div class="input-group float-right">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,paytypesort,sortIn,sortBy)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>
    </footer>

  </div>
</div>