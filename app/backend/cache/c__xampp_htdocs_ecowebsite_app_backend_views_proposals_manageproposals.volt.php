<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="reviewproposalModal.html">
  <div ng-include="'/tpl/reviewproposalModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Proposal List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Proposal
    </div>
    <div class="row wrapper">
      <div class="col-sm-4">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>
        <div class="col-sm-3">

        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Company</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
        
          <tr ng-repeat="prop in data.data | filter:searchtext">
                    
            <td  ng-if="prop.unread == 0"><span class="label bg-warning">New</span></td>           
            <td  ng-if="prop.unread == 0"><b>{[{ prop.name }]}</b></td>           
            <td  ng-if="prop.unread == 0"><b>{[{ prop.email }]}</b></td>
            <td  ng-if="prop.unread == 0"><b>{[{ prop.company }]}</b></td>
            <td  ng-if="prop.unread == 0"><b>{[{ prop.body }]}</b></td>
            <td  ng-if="prop.unread == 0"><b>{[{ prop.date }]}</b></td>
            <td  ng-if="prop.unread == 0"><a href="" ng-click="reviewproposal(prop.id)"><span class="btn m-b-xs w-xs btn-info">Review</span></a></td>


                       
            <td  ng-if="prop.unread == 1"><span class="label btn-primary">Reviewed</span></td>           
            <td  ng-if="prop.unread == 1"><p>{[{ prop.name }]}</p></td>           
            <td  ng-if="prop.unread == 1"><p>{[{ prop.email }]}</p></td>
            <td  ng-if="prop.unread == 1"><p>{[{ prop.company }]}</p></td>
            <td  ng-if="prop.unread == 1"><p>{[{ prop.body }]}</p></td>
            <td  ng-if="prop.unread == 1"><p>{[{ prop.date }]}</p></td>
            <td  ng-if="prop.unread == 1"><a href="" ng-click="reviewproposal(prop.id)"><span class="btn m-b-xs w-xs btn-info">Review</span></a></td>


                       
            <td  ng-if="prop.unread == 2"><span class="label btn-success">Replied</span></td>           
            <td  ng-if="prop.unread == 2"><p>{[{ prop.name }]}</p></td>           
            <td  ng-if="prop.unread == 2"><p>{[{ prop.email }]}</p></td>
            <td  ng-if="prop.unread == 2"><p>{[{ prop.company }]}</p></td>
            <td  ng-if="prop.unread == 2"><p>{[{ prop.body }]}</p></td>
            <td  ng-if="prop.unread == 2"><p>{[{ prop.date }]}</p></td>
            <td  ng-if="prop.unread == 2"><a href="" ng-click="reviewproposal(prop.id)"><span class="btn m-b-xs w-xs btn-info">Review</span></a></td>

          </tr>
          
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <ul class="pagination">
        <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
        <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
          <a ng-click="paging(page.num)">{[{ page.num }]}</a>
        </li>
        <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
      </ul>   

    </div>
  </div>
</footer>
</div>
</div>
