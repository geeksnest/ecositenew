<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletenews.html">
  <div ng-include="'/tpl/deleteNewsModal.html'"></div>
</script>
<script type="text/ng-template" id="updatenews.html">
  <div ng-include="'/tpl/updatenewsModal.html'"></div>
</script>
<script type="text/ng-template" id="editNewsBanner.html">
  <div ng-include="'/tpl/editNewsBanner.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/{[{bannerFolder}]}/{[{data.img}]}) 0px 0px;width: 100%;height: 300px;background-size: cover; background-position:center center; position: relative; padding-top:50px;" class="file-control">

    <div ng-show="data.showtext == 'true'" class="banner-content-wrapper-green" style=";background-color:{[{data.tbgcolor}]};">
      <div class="mtitle2" style="font-size:{[{data.titlefontsize}]}px;color:{[{data.tfcolor}]};"><span>  {[{data.title}]} </span> </div>
      <div class="mbody2" style="font-size:{[{data.descriptionfontsize}]}px;color:{[{data.dfcolor}]};">{[{data.description}]} </div>                
    </div>
    <div class="text-front">
      <a href="" ng-click="editimage(data.imgID)" class="file-control-action btn btn-dark"><i class="fa fa-edit"></i> Edit</a> 
    </div>
  </div>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-11">
          Manage News
        </div>

        <div class="col-sm-1">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
        <div class="input-group" ng-hide="featuredcat == 1">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext,sortbydate)">Go!</button>
          </span>
        </div>
        <select ng-show="featuredcat == 1" class="input-sm form-control" ng-model="sortbyfeaturednews" ng-change="sortby(sortbyfeaturednews,sortbydate)">
          <option value="null">All</option>
          <option value="1">Upcoming Programs</option>
          <option value="2">Recent Activities</option>
          <option value="3">Webinar Archive</option>
          <option value="4">Mindful Living Tips</option>
        </select> 
      </div>
      <div class="col-sm-4 m-b-xs">
        <div class="input-group w-md" ng-hide="featuredcat == 1">               
          <span class="input-group-btn">
            <input type="hidden" ng-model="news.date2">
            <input id="date" name="date" class="input-sm form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="sortbydate" ng-change="sortdate(searchtext,sortbydate)" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
            <button type="button" class="btn btn-sm btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </div>
        <div class="input-group w-md" ng-show="featuredcat == 1">               
          <span class="input-group-btn">
            <input type="hidden" ng-model="news.date2">
            <input id="date" name="date" class="input-sm form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="sortbydate" ng-change="sortdate(sortbyfeaturednews,sortbydate)" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
            <button type="button" class="btn btn-sm btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </div>
      </div>      
      <div class="col-sm-3 m-b-xs">
        <div class="input-group float-right">
          <pagination ng-hide="featuredcat == 1" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,sortbydate)" style="margin:0px;"></pagination>
          <pagination ng-show="featuredcat == 1" total-items="featbigTotalItems" ng-model="featbigCurrentPage" max-size="featmaxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(featbigCurrentPage,searchtext,sortbydate)" style="margin:0px;"></pagination>
        </div> 
      </div>
      <div class="col-sm-12 m-b-xs">      
        <label class="i-checks">
          <input type="checkbox" name="featuredcat" ng-model='featuredcat' ng-checked="featuredcat == 1" ng-click="viewfeatcat()">
          <i></i> Sort/View by featured categories
        </label>
      </div>
      <div class="col-sm-5 m-b-xs">  
      </div>
      <!-- <div class="col-sm-5 m-b-xs" ng-show="featuredcat">      
        <select class="form-control" ng-model="featuredcategory">
          <option value="0">All</option>
          <option value="1">Upcoming Programs</option>
          <option value="2">Recent Activites</option>
          <option value="3">Webinar Archive</option>
          <option value="4">Mindful Living Tips</option>
        </select> 
      </div> -->
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr ng-hide="featuredcat == 1"><!-- 
            <th style="width:40%">Title</th>
            <th style="width:20%">Author</th>
            <th style="width:15%">Date</th>
            <th style="width:15%">News Status</th> -->


            <th style="width:40%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'title','DESC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'title','ASC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'author.name','DESC')"> Author
                  <span ng-show="sortIn == 'author.name' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'author.name','ASC')"> Author
                  <span ng-show="sortIn == 'author.name' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'date','DESC')"> Date
                  <span ng-show="sortIn == 'date' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'date','ASC')"> Date
                  <span ng-show="sortIn == 'date' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> News Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> News Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            
            <th style="width:10%">Action</th>
          </tr>
          <tr ng-show="featuredcat == 1">
            <!-- <th style="width:37%">Title</th>
            <th style="width:15%">Author</th>
            <th style="width:10%">Date</th>
            <th style="width:12%">Featured Category</th> -->
            <!-- <th style="width:8%"></th> -->
            <!-- <th style="width:15%">News Status</th> -->

            <th style="width:1%"></th>

            <th style="width:37%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'title','DESC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'title','ASC')"> Title
                  <span ng-show="sortIn == 'title' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'author.name','DESC')"> Author
                  <span ng-show="sortIn == 'author.name' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'author.name','ASC')"> Author
                  <span ng-show="sortIn == 'author.name' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'date','DESC')"> Date
                  <span ng-show="sortIn == 'date' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'date','ASC')"> Date
                  <span ng-show="sortIn == 'date' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:12%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'featurednews','DESC')"> Featured Category
                  <span ng-show="sortIn == 'featurednews' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'featurednews','ASC')"> Featured Category
                  <span ng-show="sortIn == 'featurednews' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> News Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> News Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-hide="featuredcat == 1" ng-repeat="mem in list.data">
            <td><a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}" target="_blank">{[{ mem.title }]}</a></td>
            <td>{[{ mem.name }]}</td>
            <td>{[{ mem.date }]}</td>
            <td  ng-if="mem.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-info">Active</span></div>
              <div class="checkstatuscontent float-left">
                <label class="i-switch bg-success m-t-xs m-r">
                  <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,bigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <i></i>
                </label>
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>
            <td ng-if="mem.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-light">Deactivated</span></div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,bigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <i></i>
                </label>                
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>
            <td ng-if="mem.status == 2">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-warning">Draft</span></div>
                           
                <a class="publishstatus label bg-success" href="" ng-click="setstatus(mem.status,mem.newsid,searchtext,featbigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <span class=""><i class="fa fa-upload"></i> Publish</span>
                </a>                
              
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>
            <td>
              <div class="row">
                <div class="col-xs-6">
                  <a href="" ng-click="editnews(mem.newsid)" class="manage-action" title="Edit"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-xs-6">
                  <a ng-hide="mem.feat == 1" href="" ng-click="deletenews(mem.newsid)" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
                </div>
              </div>
            </td>
          </tr>


          <tr ng-show="featuredcat == 1" ng-repeat="mem in datafeat.data">
            <td class="center">
              <span ng-if="mem.feat == 1"><i class="fa fa-star manage-action text-warning"></i></span>              
              <div ng-if="mem.feat == 0">
                <div class="checkstatuscontent" ng-hide="mem.status == 2 || mem.status == 0">
                  
                  <a class="manage-action" ui-toggle-class="button" ng-click="setfeatstatus(mem.feat,mem.featurednews,mem.newsid,sortbyfeaturednews,featbigCurrentPage,mem.newslocation,mem.newsslugs)">
                    <i class="fa fa-star-o text"></i>
                    <i class="fa fa-star text-active text-warning"></i>
                  </a>                
                </div>
              </div>
            </td>
            <td>
              <a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}" target="_blank">
                {[{ mem.title }]}
              </a>
            </td>
            <td>{[{ mem.name }]}</td>
            
            <td>{[{ mem.date }]}</td>

            <td>
              <span ng-if="mem.featurednews == 1">Upcoming Programs</span>
              <span ng-if="mem.featurednews == 2">Recent Activites</span>
              <span ng-if="mem.featurednews == 3">Webinar Archive</span>
              <span ng-if="mem.featurednews == 4">Mindful Living Tips</span>
            </td>            

            <td ng-if="mem.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-info">Active</span></div>
              <div class="checkstatuscontent float-left">
                <label class="i-switch bg-success m-t-xs m-r">
                  <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,featbigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <i></i>
                </label>
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>
            <td ng-if="mem.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-light">Deactivated</span></div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,featbigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <i></i>
                </label>                
              </div>
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>
            <td  ng-if="mem.status == 2">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-warning">Draft</span></div>
                           
                <a class="publishstatus label bg-success" href="" ng-click="setstatus(mem.status,mem.newsid,searchtext,featbigCurrentPage,mem.newslocation,mem.newsslugs)">
                  <span class=""><i class="fa fa-upload"></i> Publish</span>
                </a>                
              
              <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow ==mem.newsslugs"><i class="fa fa-check"></i></spand></div>
            </td>

            <td>
              <div class="row">
                <div class="col-xs-6">
                  <a href="" ng-click="editnews(mem.newsid)" class="manage-action" title="Edit"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-xs-6">
                  <a ng-hide="mem.feat == 1" href="" ng-click="deletenews(mem.newsid)" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-9" ng-hide="featuredcat == 1">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small>            
        </div>
        <div class="col-sm-9" ng-show="featuredcat == 1">       
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{featbigTotalItems}]} items</small>        
        </div>
        <div class="col-sm-3">        
          <div class="input-group float-right">
            <pagination ng-hide="featuredcat == 1" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,sortbydate)" style="margin:0px;"></pagination>
            <pagination ng-show="featuredcat == 1" total-items="featbigTotalItems" ng-model="featbigCurrentPage" max-size="featmaxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(featbigCurrentPage,searchtext,sortbydate)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
  </div>
</div>
