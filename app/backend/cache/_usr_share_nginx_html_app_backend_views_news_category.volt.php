<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="addcategory.html">
 <div ng-include="'/tpl/addcategory.html'"></div>
</script>
<script type="text/ng-template" id="editcategory.html">
 <div ng-include="'/tpl/editcategory.html'"></div>
</script>
<script type="text/ng-template" id="deletecategory.html">
 <div ng-include="'/tpl/deletecategory.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News Category</h1>
  <a id="top"></a>
</div>
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="row">
      <div class="col-sm-8">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Category list
          </div>
          <div class="panel-body">
            <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus" style="width=100%;"></i>Add New Category</button>
          </div>
          <div class="row wrapper">
            <div class="col-sm-5 m-b-xs">
              <div class="input-group">
                <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                </span>
              </div>
            </div>
            <div class="col-sm-1 m-b-xs">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
                </span>
              </div>
            </div>
          </div>


          <div class="table-responsive">
            <table class="table table-striped b-t b-light">
              <thead class="manage">
                <tr>
                  <th>
                    
                  </th>
                  <th  style="width:75%">
                    <a href="" ng-click="sortType = 'categoryname'; sortReverse = !sortReverse">Category name
                      <span ng-show="sortType == 'categoryname' && !sortReverse" class="fa fa-caret-down"></span>
                      <span ng-show="sortType == 'categoryname' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                  </th>
                  <th style="width:25%">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="mem in data.data | filter:searchFish | orderBy:sortType:sortReverse" current-page="currentPage">
                  <td>
                  </td>
                  <td>{[{ mem.categoryname }]}</td>                
                  <td>
                    <a href="" ng-click="editcategory(mem.categoryid,mem.categoryname,mem.categoryslugs)"><span class="label bg-warning" >Edit</span></a>
                    <a href="" ng-click="deletecategory(mem.categoryid)"> <span class="label bg-danger">Delete</span>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <footer class="panel-footer">
            <ul class="pagination">
              <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
                <a class="fa fa-chevron-left" href="" ng-click="paging(data.before,searchtext)"></a>
              </li>
              <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
                <a ng-click="paging(page.num,searchtext)"> {[{ page.num }]}</a>
              </li>
              <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
                <a href="" class="fa fa-chevron-right" ng-click="paging(data.next,searchtext)"> </a>
              </li>
            </ul>
          </footer>
        </div>
      </div>

      <div class="col-sm-4" ng-show="editcategoryshow">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Edit Category
          </div>
          <div class="panel-body">
            <form class="bs-example form-horizontal" name="form" novalidate ng-submit="updatecategory(category)">
              
              <div class="row wrapper">
               <div class="col-sm-10">
                 <input type="hidden" ng-model="category.id" value="{[{id}]}">         
                 <input type="hidden" ng-model="category.catslugs" placeholder="{[{catslugs}]}">         
                 <input type="text" id="catnames" name="catnames" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="category.catnames" placeholder="{[{catnames}]}">
               </div>
             </div>
             <footer class="panel-footer">
               <button type="button" class="btn btn-default" ng-click="cancelupdatecategory()">Cancel</button>
               <button type="submit" class="btn btn-success" >Save</button>
             </footer>
           </form>
         </div>
       </div>
     </div>


   </div>
 </div>
</fieldset>

