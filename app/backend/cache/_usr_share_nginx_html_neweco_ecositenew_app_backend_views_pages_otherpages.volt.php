<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletePageModal.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <div class="row">
    <div class="col-sm-6">

      <h1 class="m-n font-thin h3">Other Page</h1>  
    </div>
    <div class="col-sm-6">
      <button class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="newpage()"><i class="fa fa-plus"></i>Create New Page</button>
    </div>
  </div> 
  <a id="top"></a>
</div>


<div class="panel-body">

  <fieldset ng-disabled="isSaving">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="row">
      <div class="col-sm-3">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Pages   
          </div>
          <div class="panel-body">

            <table class="table table-striped m-b-none">
              <thead class="manage">
                <tr>
                  <th style="width:100%;" class="text-center" colspan="3">Title</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="data in data">                    
                  <td>
                    <a href=""><span ng-bind="data.title"></span></a> 
                  </td>                  
                  <td>
                    <a href="" ng-click="editpage(data.pageID,data.title,data.slugs,data.content_1,data.content_2,data.content_3,data.content_4,data.content_5)" class="pull-right"><i class="fa fa-edit"></i></a>
                  </td>                 
                  <td>
                    <a href="" ng-click="deletepage(data.pageID)" class="pull-right"><i class="fa fa-times"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>
      </div>
      <div class="col-sm-9" ng-if="showFrom">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Page Information
          </div>
          <div class="panel-body">
            <!-- <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert> -->
            <div class="form-group">
              <button class="btn btn-default pull-right mar-l" ng-show="cancelbot" ng-click="cancel()">Cancel</button>

              <button class="btn btn-success btn-addon pull-right" ng-show="savebot" ng-disabled="savebtn" ng-click="savepage(page)"><i class="glyphicon glyphicon-save"></i>Save Page</button>

              <button class="btn btn-success btn-addon pull-right" ng-show="uploadbot" ng-disabled="savebtn" ng-click="updatepage(page)"><i class="glyphicon glyphicon-save"></i>Update Page</button>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label for="title">Title</label>
              <input type="hidden" id="pageID" name="pageID" class="form-control" ng-model="page.pageID" >
              <input type="text" id="title" name="title" class="form-control" ng-model="page.title" ng-keyup="onpagetitle(page.title,page)">
              <br>
              <b>Page Slugs: </b>
              <input type="hidden" style="text-transform: lowercase;" ng-model="page.slugs">
              <span style="text-transform: lowercase;" ng-bind="page.slugs"></span>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label class="control-label">Content 1</label>
              <textarea class="ck-editor" ng-model="page.content_1"></textarea>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label class="control-label">Content 2</label>
              <textarea class="ck-editor" ng-model="page.content_2"></textarea>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label class="control-label">Content 3</label>
              <textarea class="ck-editor" ng-model="page.content_3"></textarea>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label class="control-label">Content 4</label>
              <textarea class="ck-editor" ng-model="page.content_4"></textarea>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="form-group">
              <label class="control-label">Content 5</label>
              <textarea class="ck-editor" ng-model="page.content_5"></textarea>
            </div>

          </div>
        </div>
      </div>
    </div>

  </fieldset>
</div>

