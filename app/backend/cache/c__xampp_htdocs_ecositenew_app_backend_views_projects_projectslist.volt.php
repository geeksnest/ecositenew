<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="reviewProj.html">
  <div ng-include="'/tpl/reviewProj.html'"></div>
</script>
<script type="text/ng-template" id="checkdonProj.html">
  <div ng-include="'/tpl/checkdonProj.html'"></div>
</script>
<script type="text/ng-template" id="deleteMyProj.html">
  <div ng-include="'/tpl/deleteMyProj.html'"></div>
</script>
<script type="text/ng-template" id="editNewsBanner.html">
  <div ng-include="'/tpl/editNewsBanner.html'"></div>
</script>
<script type="text/ng-template" id="appDisappProj.html">
  <div ng-include="'/tpl/appDisappProj.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Projects</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/{[{bannerFolder}]}/{[{dataBanner.img}]}) 0px 0px;width: 100%;height: 300px;background-size: cover; background-position:center center; position: relative; padding-top:50px;" class="file-control">

    <div ng-show="dataBanner.showtext == 'true'" class="banner-content-wrapper-green" style=";background-color:{[{dataBanner.tbgcolor}]};">
      <div class="mtitle2" style="font-size:{[{dataBanner.titlefontsize}]}px;color:{[{dataBanner.tfcolor}]};"><span>  {[{dataBanner.title}]} </span> </div>
      <div class="mbody2" style="font-size:{[{dataBanner.descriptionfontsize}]}px;color:{[{dataBanner.dfcolor}]};">{[{dataBanner.description}]} </div>
          <div ng-if="dataBanner.linkpath && dataBanner.linkname" class="learnheroes">
          <a href="{[{dataBanner.linkpath}]}" style="padding: 12px 17px;">{[{dataBanner.linkname}]}</a>
        </div>                 
    </div> 
    <div class="text-front">
      <a href="" ng-click="editimage(dataBanner.imgID)" class="file-control-action btn btn-dark"><i class="fa fa-edit"></i> Edit</a> 
    </div>
  </div>
</div>
<div class="wrapper-md">
<div class="row text-center">
</div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-11 ">
          List of Projects
        </div>
        <div class="col-sm-1">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh(searchtext,timestamp.year,timestamp.month,timestamp.day)"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-5 ">
          <div class="input-group" ng-hide="advancesearch">
            <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
            </span>
          </div>
          <div ng-show="advancesearch">
            <!-- SEARCH BY TIMESTAMP -->
            <div class="form-group">
              <div class="row">

                <div class="col-sm-3">
                  Year:<br/>
                  <select ng-model="timestamp.year" class="input-sm form-control">
                    <?php for ($year=2014; $year <= date('Y'); $year++) {
                      echo "<option value='".$year."'>".$year."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-5">
                  Month:<br/>
                  <select ng-model="timestamp.month" class="input-sm form-control">
                    <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); ?>
                    <?php foreach ($formonths as $index => $formonth) {
                      echo "<option value='".$index."'>".$formonth."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-3">
                  Day:<br/>
                  <select ng-model="timestamp.day" class="input-sm form-control">
                    <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                    echo "<option value='".$days."'>".$days."</option>";
                  } ?>
                </select>
              </div>
              <div class="col-sm-1">
                <br/>
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="searchtimestamp(timestamp,advancesearch)">Go!</button>
                </span>
              </div>
            </div>
          </div>
        </div>

        <div class="checkbox">
          <label class="i-checks">
            <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by
          </label>
        </div>
      </div>
    </div>
  </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>
            <th style="width:5%">
            
            </th>
            <th style="width:40%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'projTitle','DESC')"> Title
                  <span ng-show="sortIn == 'projTitle' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'projTitle','ASC')"> Title
                  <span ng-show="sortIn == 'projTitle' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'firstname','DESC')"> Author
                  <span ng-show="sortIn == 'firstname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'firstname','ASC')"> Author
                  <span ng-show="sortIn == 'firstname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'projLoc','DESC')"> Location
                  <span ng-show="sortIn == 'projLoc' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'projLoc','ASC')"> Location
                  <span ng-show="sortIn == 'projLoc' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'date_created','DESC')"> Date Created
                  <span ng-show="sortIn == 'date_created' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'date_created','ASC')"> Date Created
                  <span ng-show="sortIn == 'date_created' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th  style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="data in data">
            <td>
              <label ng-if="data.projStatus == '0'" class="label bg-warning inline m-t-sm">New</label>
              <label ng-if="data.projStatus == '1'" class="label bg-success inline m-t-sm">Approved</label>
              <label ng-if="data.projStatus == '2'" class="label bg-info inline m-t-sm">Published</label>
              <label ng-if="data.projStatus == '3'" class="label bg-danger inline m-t-sm">Disapproved</label>
              <label ng-if="data.projStatus == '5'" class="label bg-primary inline m-t-sm">Finished</label>
            </td>
            <td ng-click="toview(data.projSlugs)" style="cursor:pointer;">{[{ data.projTitle }]}</td>
            <td>{[{ data.firstname }]} {[{ data.lastname }]}</td>
            <td>{[{ data.projLoc }]}</td>
            <td>{[{ data.date_created}]}<!-- {[{ data.date_created | dateToISO | date:'mediumDate' }]} -->
            </td>
            <td>
              <div class="row">
                <div class="col-xs-4">
                  <a ng-if="data.projStatus == '2'" href="" ng-click="checkdon(data.projID)" class="manage-action" title="Donations"><i class="fa fa-suitcase"></i></a>
                </div>
                <div class="col-xs-4">
                  <a href="" ng-click="review(data.projID)" class="manage-action" title="Review"><i class="fa fa-search"></i></a>
                </div>
                <div class="col-xs-4">
                <a href="" ng-click="delete(data.projID)" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
          <div class="input-group">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,timestamp,advancesearch)" style="margin:0px;"></pagination>
          </div>
        </div>
      </div>
    </footer>

  </div>
</div>