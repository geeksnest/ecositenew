<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="updatepage.html">
  <div ng-include="'/tpl/updatepageModal.html'"></div>
</script>
<script type="text/ng-template" id="sentletter.html">
  <div ng-include="'/tpl/sentletterModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Subscribers List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Subcribers
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
       <div class="input-group">
        <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
        <span class="input-group-btn">
          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
        </span>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <input type="hidden" ng-init='pagedata = <?php echo $data; ?>'>
    <table class="table table-striped b-t b-light">
      <thead>
        <tr>
          <!-- <th style="width:20px;"><label class="i-checks m-b-none"><input type="checkbox"><i></i></label></th> -->
          <th style="width:25%">Email</th>
          <th style="width:25%">Status</th>
          <th style="width:25%">Date Subscribe</th>
          <th style="width:25%">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="subs in data.data">
          <!-- <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td> -->
          <td>{[{ subs.NMSemail }]}</td>
          <td><span>{[{ subs.NMSstatTXT }]}</span></td>
          <td>{[{ subs.NMSdate }]}</td>
          <td>
            <a href="" ng-click="updatepage(subs.NMSid)"><span class="label bg-warning" >Edit</span></a>
            <a href="" ng-click="deletepage(subs.NMSid)"> <span class="label bg-danger">Delete</span></a>
            <a href="" ng-click="viewsentletter(subs.NMSemail)"> <span class="label bg-info">View Sent Letters</span></a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <footer class="panel-footer">
    <ul class="pagination">
      <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
      <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
        <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
      </li>
      <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
    </ul>   

  </div>
</div>
</footer>
</div>
</div>