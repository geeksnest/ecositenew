<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="editMemberModal.html">
<form class="bs-example form-horizontal" name="form" novalidate>
    <div class="modal-header">
        <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>
    <div class="modal-body" ng-show="process==true">
        <p ng-show="success==false">Updating user profile record.</p>
        <p ng-show="success==true">Success!</p>
    </div>
    <div class="modal-body" ng-show="process==false">
      ARE YOU SURE YOU WANT TO DELETE?
    </div>
    <div class="modal-footer" ng-hide="process==true">                  
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
</form>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit Featured Project</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" ng-submit="updatepage(page)" name="formFeature">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="row">
      <div class="col-sm-8">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Note: Field mark with <font style="color:red;font-size:14px">*</font> is required.
            <br><br>
            Featured Project Information
          </div>
          <div class="panel-body">
            <input type="hidden" ng-model='page.pageid'>
              <label class="col-sm-2 control-label"><label for="title">Title <font style="color:red;font-size:24px">*</font></label> </label>
              <div class="col-sm-10">
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Body Content <font style="color:red;font-size:24px">*</font></label>
                <div class="col-sm-10">
               <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
              </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="checkbox">
                <label class="i-checks">
                  <input type="checkbox" ng-model='page.check' ng-true-value="1" ng-false-value="0">
                  <i></i>
                  Active Project
                </label>
           </div>

           <div class="checkbox">
                <label class="i-checks">
                  <input type="checkbox" ng-model='page.check2' ng-true-value="1" ng-false-value="0">
                  <i></i>
                  Publish Project
                </label>
           </div>

           <div class="line line-dashed b-b line-lg pull-in"></div>

            <div ng-if="page.check">
              <label class="col-sm-2 control-label"><label for="slides">Select Slideshow <font style="color:red;font-size:24px">*</font></label> </label>
              <div class="col-sm-10">
                <select id="slides" name="slides" class="form-control" ng-model="page.slides" required="required">
                  <option value="{[{ data.album_name }]}" ng-repeat="data in dataSlide"> {[{ data.album_name }]} </option>
                </select>
              </div>
            </div>

           </div>

        </div>
      </div>


      <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Edit Featured Project Cover
          </div>
           <div class="panel-body">
             <img ng-src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ page.banner }]}" style="width: 100%" ng-show="page.banner">
              <div class="line line-dashed b-b line-lg pull-in"></div>
             <input type="text" id="banner" name="banner" class="form-control" ng-model="page.banner" ng-change="banners(page.banner)" placeholder="Paste Banner link here...">
              <div class="line line-dashed b-b line-lg pull-in"></div>
          </div>
        </div>
      </div>
      
    </div>


    <div class="row" >
       <div class="panel-body">
           <footer class="panel-footer text-right bg-light lter">
                <button type="button" class="btn btn-default" ui-sref="managefeaturedproject">Cancel</button>
                <button type="submit" class="btn btn-success" ng-disabled="formFeature.$invalid">Submit</button>
           </footer>
       </div>


  <div class="wrapper-md ">
      <div class="row">
          <div class="panel panel-default">
          <div class="panel-heading font-bold"> Upload New Photo</div>

          <div class="panel-body">
            <div class="col-lg-12">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          <div class="padd">
                              <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Add files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input  id="digitalAssets" type="file" name="files[]" multiple>
                              </span>
                              <!-- The global progress bar -->

                              <div class="line line-dashed b-b line-lg pull-in"></div>

                              <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                              </div>
                              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data"> 
                             <div class="gallery digital-assets-gallery"> 

                                <div class="clearfix"></div>
                            </div>
                            </form>

      <div class="col-sm-4" ng-repeat="data in data">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="panel-heading">
              <ul class="nav nav-pills pull-right">
                  <li>
                    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="huhuClick(dlt)" name="form" enctype="multipart/form-data">
                        <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.id"class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
                        <div class="buttonPanel{[{ data.id }]}">
                         <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                        </div>
                     </form>
                  </li>
              </ul>
              <br>
              <h1 class="m-n font-thin h3 text-black">Images</h1>
              <input type="text" id="imgpath" name="imgpath" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="data.imgpath">
            </div>
           
             <img  style="width:350px;height:250px;position:relative; " ng-src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ data.imgpath }]}">
              <!-- Input for Image Title and Description -->
          
          </div>
        </div>
      </div>
                          </div>
                          <div class="widget-foot">
                          </div>
                        </div>
                      </div>  
                      </div>       
                  </div>
          </div>
      </div>
</div>
</div>

    </div>

  </div>
  </fieldset>
