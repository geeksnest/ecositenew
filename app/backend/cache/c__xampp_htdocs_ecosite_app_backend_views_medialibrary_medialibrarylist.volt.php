<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="reviewVideo.html">
  <div ng-include="'/tpl/reviewVideo.html'"></div>
</script>
<script type="text/ng-template" id="checkdonProj.html">
  <div ng-include="'/tpl/checkdonProj.html'"></div>
</script>
<script type="text/ng-template" id="deleteMyVideo.html">
  <div ng-include="'/tpl/deleteMyVideo.html'"></div>
</script>
<script type="text/ng-template" id="editNewsBanner.html">
  <div ng-include="'/tpl/editNewsBanner.html'"></div>
</script>
<script type="text/ng-template" id="appDisappProj.html">
  <div ng-include="'/tpl/appDisappProj.html'"></div>
</script>
<script type="text/ng-template" id="managetags.html">
  <div ng-include="'/tpl/managetags.html'"></div>
</script>
<script type="text/ng-template" id="tagsDelete.html">
  <div ng-include="'/tpl/tagsDelete.html'"></div>
</script>
<script type="text/ng-template" id="addtags.html">
  <div ng-include="'/tpl/addtags.html'"></div>
</script>
<script type="text/ng-template" id="managecategory.html">
  <div ng-include="'/tpl/managecategory.html'"></div>
</script>
<script type="text/ng-template" id="deletecategory.html">
  <div ng-include="'/tpl/deletecategory.html'"></div>
</script>
<script type="text/ng-template" id="addcategory.html">
  <div ng-include="'/tpl/addcategory.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Media Library</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/{[{bannerFolder}]}/{[{dataBanner.img}]}) 0px 0px;width: 100%;height: 300px;background-size: cover; background-position:center center; position: relative; padding-top:50px;" class="file-control">

    <div ng-show="dataBanner.showtext == 'true'" class="banner-content-wrapper-green" style=";background-color:{[{dataBanner.tbgcolor}]};">
      <div class="mtitle2" style="font-size:{[{dataBanner.titlefontsize}]}px;color:{[{dataBanner.tfcolor}]};"><span>  {[{dataBanner.title}]} </span> </div>
      <div class="mbody2" style="font-size:{[{dataBanner.descriptionfontsize}]}px;color:{[{dataBanner.dfcolor}]};">{[{dataBanner.description}]} </div>                
    </div>
    <div class="text-front">
      <a href="" ng-click="editimage(dataBanner.imgID)" class="file-control-action btn btn-dark"><i class="fa fa-edit"></i> Edit</a> 
    </div>
  </div>
</div>
<div class="wrapper-md">
  <div class="row text-center">
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-6 ">
          List of Videos
        </div>
        <div class="col-sm-6">
          <button class="btn btn-sm btn-primary float-right" style="margin-left:1em;" type="button" ng-click="managetags()">
            Manage Tags
          </button>
          <button class="btn btn-sm btn-primary float-right" type="button" ng-click="managecategory()">
            Manage Categories
          </button>
          
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row" ng-show="advancesearch == false">
        <div class="col-xs-5 ">
          <div class="input-group">
            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext,status)">Go!</button>
            </span>
            <select class="input-sm form-control" ng-model="status">
              <option value="" style="display:none">-Status-</option>
              <option value="Submitted">New</option>
              <option value="Approved">Approved</option>
              <option value="Disapproved">Disapproved</option>
              <option value="Resubmitted">Resubmitted</option>
            </select>
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
            </span>
          </div>
        </div>
      </div>
      <div class="checkbox">
        <label class="i-checks">
          <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by Date
        </label>
      </div>
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row" ng-show="advancesearch == true">
        <div class="col-md-7">
          <div class="row">

            <div class="col-xs-12">
              Date
            </div>
            <div class="col-xs-4" style="padding-right:0px;">
              Year:<br/>
              <select ng-model="recurring.year" class="input-sm form-control">
                <option value="">----</option>
                <?php for ($year=2014; $year <= date('Y'); $year++) {
                  echo "<option value='".$year."'>".$year."</option>";
                }?>
              </select>
            </div>
            <div class="col-xs-5" style="padding:0px 5px;">
              Month:<br/>
              <select ng-model="recurring.month" class="input-sm form-control">
                <option value="">----</option>
                <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); 
                ?>
                <?php foreach ($formonths as $index => $formonth) {
                  echo "<option value='".$index."'>".$formonth."</option>";
                }?>
              </select>
            </div>
            <div class="col-xs-3" style="padding:0px 15px 0px 0px;">
              Day:<br/>
              <select ng-model="recurring.day" class="input-sm form-control">
                <option value="">--</option>
                <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                echo "<option value='".$days."'>".$days."</option>";} 
                ?>
              </select>
            </div>
          </div>

        </div>
        <div class="col-md-1"><br/><br/>
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="searchbydatepub(recurring)">Go!</button>
          </span>
          <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
            </span>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped b-t b-light">
      <thead class="manage">
        <tr>
          <th style="width:40%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,status,'title','DESC')"> Title
                <span ng-show="sortIn == 'title' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,status,'title','ASC')"> Title
                <span ng-show="sortIn == 'title' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,status,'firstname','DESC')"> Author
                <span ng-show="sortIn == 'firstname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,status,'firstname','ASC')"> Author
                <span ng-show="sortIn == 'firstname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,status,'datepublished','DESC')"> Date Approved
                <span ng-show="sortIn == 'datepublished' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,status,'datepublished','ASC')"> Date Approved
                <span ng-show="sortIn == 'datepublished' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,status,'datecreated','DESC')"> Date Created
                <span ng-show="sortIn == 'datecreated' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,status,'datecreated','ASC')"> Date Created
                <span ng-show="sortIn == 'datecreated' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
              <a href="" ng-click="sortType(searchtext,status,'status','DESC')"> Status
                <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
              </a>
            </span>
            <span ng-show="sortBy == 'DESC'">
              <a href="" ng-click="sortType(searchtext,status,'status','ASC')"> Status
                <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
              </a>
            </span>
          </th>
          <th  style="width:10%">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="data in data.data">
          <td ng-click="toview(data.slugs)" style="cursor:pointer;">{[{ data.title }]}</td>
          <td>{[{ data.firstname }]} {[{ data.lastname }]}</td>
          <td ng-if="data.datepublished==undefined">---/--/----</td>
          <td ng-if="data.datepublished!=undefined">{[{ data.datepublished | dateToISO | date:'mediumDate' }]}</td>
          <td>{[{ data.datecreated | dateToISO | date:'mediumDate' }]}</td>
          <td>
            <label ng-if="data.status == 'Submitted'" class="label bg-info inline m-t-sm">New</label>
            <label ng-if="data.status == 'Resubmitted'" class="label bg-warning inline m-t-sm">Resubmitted</label>
            <label ng-if="data.status == 'Approved'" class="label bg-success inline m-t-sm">Approved</label>
            <label ng-if="data.status == 'Disapproved'" class="label bg-danger inline m-t-sm">Disapproved</label>
            <i title="Featured Video" class="manage-action fa fa-star-o" ng-if="data.featured =='true'"></i>
          </td>
          <td>
            <div class="row">
              <div class="col-xs-4">
                <a href="" ng-click="review(data.slugs)" class="manage-action" title="Review"><i class="fa fa-search"></i></a>
              </div>
              <div class="col-xs-4">
                <a href="" ng-click="delete(data.id)" ng-if="data.status != 'Approved'" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <footer class="panel-footer">
    <div class="row">
      <div class="col-sm-6">
        <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
      </div>
      <div class="col-sm-2">
      </div>
      <div class="col-sm-4">
        <div class="input-group">
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,status)" style="margin:0px;"></pagination>
        </div>
      </div>
    </div>
  </footer>

</div>
