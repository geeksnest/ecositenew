<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="authorDelete.html">
  <div ng-include="'/tpl/authorDelete.html'"></div>
</script>

<script type="text/ng-template" id="authorEdit.html">
  <div ng-include="'/tpl/authorEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Authors</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formAuthor">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Authors List
            </div>


            <div class="panel-body">



              <div class="row wrapper">
                <div class="col-sm-5 m-b-xs">
                  <div class="input-group">
                    <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                    <span class="input-group-btn">
                      <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                    </span>
                  </div>
                </div>
                <div class="col-sm-1 m-b-xs">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                  <thead class="manage">
                    <tr>                               
                      <!-- <th style="width:25%">Name</th>
                      <th style="width:25%">Location</th>
                      <th style="width:20%">Occupation</th>
                      <th style="width:20%">Author Since</th> -->

                      <th  style="width:25%">
                        <a href="" ng-click="sortType = 'name'; sortReverse = !sortReverse">Name
                          <span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
                          <span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
                        </a>
                      </th>
                      <th  style="width:25%">
                        <a href="" ng-click="sortType = 'location'; sortReverse = !sortReverse">Location
                          <span ng-show="sortType == 'location' && !sortReverse" class="fa fa-caret-down"></span>
                          <span ng-show="sortType == 'location' && sortReverse" class="fa fa-caret-up"></span>
                        </a>
                      </th>
                      <th  style="width:20%">
                        <a href="" ng-click="sortType = 'occupation'; sortReverse = !sortReverse">Occupation
                          <span ng-show="sortType == 'occupation' && !sortReverse" class="fa fa-caret-down"></span>
                          <span ng-show="sortType == 'occupation' && sortReverse" class="fa fa-caret-up"></span>
                        </a>
                      </th>
                      <th  style="width:20%">
                        <a href="" ng-click="sortType = 'authorsince'; sortReverse = !sortReverse">Author Since
                          <span ng-show="sortType == 'authorsince' && !sortReverse" class="fa fa-caret-down"></span>
                          <span ng-show="sortType == 'authorsince' && sortReverse" class="fa fa-caret-up"></span>
                        </a>
                      </th>
                      <th style="width:10%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="mem in data.data | filter:searchFish | orderBy:sortType:sortReverse" current-page="currentPage">
                     
                      <td>{[{ mem.name }]}</td>
                      <td>{[{ mem.location }]}</td>
                      <td>{[{ mem.occupation }]}</td>
                      <td>{[{ mem.authorsince }]}</td>
                      
                      <td>
                        <a href="" ng-click="editauthor(mem.authorid)"><span class="label bg-warning" >Edit</span></a>
                        <a href="" ng-click="deleteauthor(mem.authorid)"> <span class="label bg-danger">Delete</span></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>



            </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
          <footer class="panel-footer text-center bg-light lter">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)"></pagination>
          </footer>
        </div>
      </div>

    </div>
  </fieldset>
</form>