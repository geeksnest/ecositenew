<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="review.html">
  <div ng-include="'/tpl/heroesoncallModalReview.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Heroes On Duty</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Request
    </div>
    <div class="row wrapper">
      <div class="col-sm-6 m-b-xs" ng-hide="advancesearch">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" ng-click="search(searchtext)" type="button">Go!</button>
          </span>
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
      <div class="col-sm-6 m-b-xs" ng-show="advancesearch">
        From 
        <input type="date" id="from" ng-model="dfrom">
        To
        <input type="date" id="to" ng-model="dto">
        <button type="button" class="btn btn-default" ng-click="filterdate(dfrom,dto)"><i class="glyphicon glyphicon-search"></i> Filter By Date</button>
        <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
      </div>
      <br>
      <div class="checkbox col-sm-12">
        <label class="i-checks">
          <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Filter By Date
        </label>
      </div>
    </div>

    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>
            <th style="width:30%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'name','DESC')"> Name
                  <span ng-show="sortIn == 'name' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'name','ASC')"> Name
                  <span ng-show="sortIn == 'name' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'email','DESC')"> E-Mail
                  <span ng-show="sortIn == 'email' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'email','ASC')"> E-Mail
                  <span ng-show="sortIn == 'email' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>


            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'phone','DESC')"> Phone #
                  <span ng-show="sortIn == 'phone' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'phone','ASC')"> Phone #
                  <span ng-show="sortIn == 'phone' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

             <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'gender','DESC')"> Gender
                  <span ng-show="sortIn == 'gender' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'gender','ASC')"> Gender
                  <span ng-show="sortIn == 'gender' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
             <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'datesent','DESC')"> Date
                  <span ng-show="sortIn == 'datesent' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'datesent','ASC')"> Date
                  <span ng-show="sortIn == 'datesent' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td>
              {[{ mem.name }]}
            </td>
            <td>
              {[{ mem.email }]}
            </td>
             <td>
              {[{ mem.phone }]}
            </td>
             <td>
              {[{ mem.gender}]}
            </td>

            <td  ng-if="mem.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-success">Reviewed </span></div>
            </td>
            <td  ng-if="mem.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-warning">New</span></div>
            </td>
            <td>
              {[{ mem.datesent }]}
            </td>
            <td>
              <a href="" ng-click="review(mem.id, bigCurrentPage, searchtext)"><span class="label bg-primary">Review</span></a>
              <a href="" ng-click="deletepage(mem.id, bigCurrentPage, searchtext)"> <span class="label bg-danger">Delete</span>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-8">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small> 
        </div>
        <div class="col-sm-4">        
          <div class="input-group">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
</div>
</div>
