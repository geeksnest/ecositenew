<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="deleteOtherDonationModal.html">
  <div ng-include="'/tpl/deleteOtherDonationModal.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of ECO NY Walk Donations</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
<div class="row text-center">
	<div class="col-xs-4">
    <div class="panel padder-v item">
      <div class="h1 text-info font-thin h1">$<?php echo $amounts; ?></div>
      <span class="text-muted text-xs">Total Donations</span>
      <span class="bottom text-right">
        <i class="fa fa-dollar text-muted m-r-sm"></i>
      </span>
    </div>
  </div>
  <div class="col-xs-4">
    <a href="" class="block panel padder-v bg-primary item">
      <span class="text-white font-thin h1 block"><?php echo $usersdonated; ?></span>
      <span class="text-muted text-xs">Total Donors</span>
      <span class="bottom text-right">
        <i class="fa fa-user text-muted m-r-sm"></i>
      </span>
    </a>
  </div>
  <div class="col-xs-4 form-group">
 <!--  <form name="updateamounts" novalidate>
    <div class="col-xs-5 ">
      <label>Amount Added</label>
		  <input type="text" class="form-control" ng-model="donate.amount" name="amount" ng-init="donate.amount=<?php echo $donamounts; ?>">
    </div>
    <div class="col-xs-5">
      <label>Users Added</label>
		  <input type="text" class="form-control" ng-model="donate.users" name="users" ng-init="donate.users=<?php echo $donamounts; ?>">
    </div>
    <div class="col-xs-10" style="text-align: left;">
      <input type="button" value="Add numbers to current results." style="margin-top: 5px;" ng-click="updateAmounts(donate)">
      <i class="fa fa-check-square" ng-show="updatedamount"></i>
    </div>
  </form> -->
</div>
</div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-11 ">
        Manage NY Donation
        </div>
        <div class="col-sm-1">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh(searchtext,timestamp.year,timestamp.month,timestamp.day)"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
    </div>
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-5 ">
          <div class="input-group" ng-hide="advancesearch">
            <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
            </span>
          </div>

          <div ng-show="advancesearch">

            <!-- SEARCH BY TIMESTAMP -->
            <div class="form-group">

              <div class="row">

                <div class="col-sm-3">
                  Year:<br/>
                  <select ng-model="timestamp.year" class="input-sm form-control">
                    <?php for ($year=2014; $year <= date('Y'); $year++) { 
                      echo "<option value='".$year."'>".$year."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-5">
                  Month:<br/>
                  <select ng-model="timestamp.month" class="input-sm form-control">
                    <?php $formonths = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'); ?>
                    <?php foreach ($formonths as $index => $formonth) { 
                      echo "<option value='".$index."'>".$formonth."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-sm-3">
                  Day:<br/>
                  <select ng-model="timestamp.day" class="input-sm form-control">
                    <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                    echo "<option value='".$days."'>".$days."</option>";
                  } ?>
                </select>
              </div>
              <div class="col-sm-1">
                <br/>
                <span class="input-group-btn">
                  <button class="btn btn-sm btn-default" type="button" ng-click="searchtimestamp(timestamp,advancesearch)">Go!</button>
                </span>
              </div>
            </div>

          </div>

        </div>

        <div class="checkbox">
          <label class="i-checks">
            <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by Timestamp 
          </label>
        </div>
      </div>
    </div>
  </div>
    <div class="table-responsive">
      <input type="hidden" ng-init='pagedata = <?php echo $data; ?>'>
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>
            <th style="width:12%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'transactionId','DESC')"> TransactionID
                  <span ng-show="sortIn == 'transactionId' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'transactionId','ASC')"> TransactionID
                  <span ng-show="sortIn == 'transactionId' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:24%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'useremail','DESC')"> Email
                  <span ng-show="sortIn == 'useremail' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'useremail','ASC')"> Email
                  <span ng-show="sortIn == 'useremail' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:17%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'paymentmode','DESC')"> Payment Mode / Last 4 Digits
                  <span ng-show="sortIn == 'paymentmode' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'paymentmode','ASC')"> Payment Mode / Last 4 Digits
                  <span ng-show="sortIn == 'paymentmode' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'amount','DESC')"> Amount
                  <span ng-show="sortIn == 'amount' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'amount','ASC')"> Amount
                  <span ng-show="sortIn == 'amount' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:14%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'billinginfofname','DESC')"> Billing Info
                  <span ng-show="sortIn == 'billinginfofname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'billinginfofname','ASC')"> Billing Info
                  <span ng-show="sortIn == 'billinginfofname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:13%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'howdidyoulearn','DESC')"> How did he/she know?
                  <span ng-show="sortIn == 'howdidyoulearn' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'howdidyoulearn','ASC')"> How did he/she know?
                  <span ng-show="sortIn == 'howdidyoulearn' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:12%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'datetimestamp','DESC')"> Timestamp (UTC)
                  <span ng-show="sortIn == 'datetimestamp' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'datetimestamp','ASC')"> Timestamp (UTC)
                  <span ng-show="sortIn == 'datetimestamp' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th  style="width:5%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="don in data.data">
            <td>{[{ don.transactionId }]}</td>
            <td>{[{ don.useremail }]}</td>
            <td>{[{ don.paymentmode }]}/{[{ don.lastccba }]}</td>
            <td>{[{ don.amount }]}</td>
            <td>{[{ don.billinginfofname }]} {[{ don.billinginfolname }]}</td>
            <td>{[{ don.howdidyoulearn }]}/{[{ don.cname }]}</td>
            <td>{[{ don.datetimestamp }]}</td>
            <td>
              <a href="" ng-click="delete(don.id)" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </div>

  <footer class="panel-footer">
    <div class="row">
      <div class="col-sm-12 center">
        <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
      </div>
      <div class="col-sm-12 center">
        <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm center" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,sortIn,sortBy)"></pagination>
      </div>
    </div>
  </footer>

  </div>
</div>