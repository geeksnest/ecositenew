<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deleteAlbumImage.html">
  <div ng-include="'/tpl/deleteAlbumImage.html'"></div>
</script>

<script type="text/ng-template" id="editAlbumImage.html">
  <div ng-include="'/tpl/editAlbumImage.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Update Banner Album</h1>
  <a id="top"></a>
</div>

<div class="loader" ng-show="imageloader">
  <div class="loadercontainer">    
    <div class="spinner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
    Please wait while uploading your images...
  </div>
</div>

<div class="wrapper-md" ng-hide="imageloader">
  <form name="formUpload" id="formUpload">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        <div class="row">
        <div class="col-sm-12">
            <button ng-show="addphoto == true" class="btn m-b-xs btn-sm btn-success btn-addon pull-right" ng-click="uploadAlbum(file,data)"><i class="fa fa-upload"></i> Upload</button>

            <button ng-show="addphoto == false || addphoto == true" class="btn m-b-xs btn-sm btn-info btn-addon pull-right mar-right" id="change-picture" ngf-change="prepare(files,file)" ngf-select ng-model="files" ngf-multiple="true" required="required"><i class="glyphicon glyphicon-plus"></i> Add Image</button>
          </div>
        </div>
      </div>
      <div class="panel-body" ng-show="addphoto == true">
        <div class="row"> 
          <div class="col-sm-12" ng-show="bigImages == true">
            <span class="label bg-danger textAt"><i class="fa fa-exclamation-circle"></i> The following images you wish to upload is too big. Please resize the images below 2MB and try again.</span>
            <div class="col-sm-12" ng-repeat="bigfile in bigfile track by $index">
              <br>
              <label><span ng-bind="bigfile.name"></span></label>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

          </div>                
          <div class="col-sm-12 propic">
            <div class="row"> 
              <div class="col-sm-4 album-padding" ng-repeat="file in file track by $index">
                <a href="" ng-click="remove($index)" ng-if="imageselected == true" class="pull-right remove"><i class="fa fa-times-circle"></i> Remove</a>
                <img ngf-src="file" name="profpic" id="profpic" ng-if="imageselected == true">
              </div>
            </div>
          </div>
        </div>  
      </div>  
    </div> 
  </form>
</div>

<div class="wrapper-md ">
  <div class="panel panel-default">
    <div class="panel-heading font-bold">
      <div class="row">
        <div class="col-sm-12">
          {[{data.album_name}]}
          <input id="album_id" type="hidden" name="album_id" ng-model="data.album_id" class="form-control">
          <input id="album_name" type="hidden" name="album_name" ng-model="data.album_name" class="form-control">
          <input id="foldername" type="hidden" name="foldername" ng-model="data.foldername" class="form-control">
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="row">   
            <div class="col-sm-4 album-wrap" ng-repeat="img in img"> 
              <div style="background: url(<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{img.foldername}]}/{[{img.img}]}) 0px 0px;background-size: cover; background-position:center center; position: relative;height:200px;" class="file-control"> 

                <div class="col-sm-12 no-border bg-imgtitle">          
                  <span class="text-lt">{[{img.title}]}</span>                            
                </div> 

                <div class="file-control-action text-front">
                  <a href="" ng-click="editimage(img.imgID)" class="file-control-action btn opt-btn"><i class="fa fa-edit"></i> Edit</a> 
                  <a href="" ng-click="deleteimage(img.imgID)" class="file-control-action btn opt-btn" ng-show="img.sort != 1"><i class="fa fa-times-circle"></i> Delete</a>
                </div>

                <div class="file-control-action sort-slider">                  
                  <select class="form-control" ng-model="img.sort" ng-change="sortSlider(img.imgID,img.sort,data.album_id)">
                    <option ng-selected="img.sort == images"  ng-repeat="images in images track by $index" value="{[{images}]}">{[{images}]}</option>
                  </select>
                </div>

              </div>        
            </div>
          </div>              
        </div>
      </div>
    </div>
  </div>
</div>

