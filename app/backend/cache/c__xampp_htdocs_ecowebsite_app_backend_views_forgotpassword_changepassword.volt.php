<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/app.css'); ?>
    <script>
        var API_URL = '<?php echo $this->config->application->apiURL; ?>';
  </script>
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">-->
  <style type="text/css">
  .tokenexpired
  {
    color:#F00;

  }
  </style>
</head>

<body ng-controller="changepasswordctrl">
  <div class="container w-xxl w-auto-xs" >
    <div class="m-b-lg">
      <a href class="navbar-brand block m-t"><!--<?php echo $app->name; ?>--></a>
      <div class="wrapper text-center">
        <img src='/img/ecologo.png' style="width:300px;height:178px;">
      </div>

      <div class="wrapper text-center tokenexpired" ng-show="authError">
        <h3>The Password Reset Token has expired.</h3>
      </div>
      <div class="wrapper text-center" ng-show="authsuccess">
        <h3>Password Change Success</h3>
        <br>
        <p>
        Back to <strong><a href="/ecoadmin">Log in</a></strong> Site
        </p>
      </div>
        
      <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="send(forgot)" name="formNews" id="formNews" ng-show="validform">
        <div class="list-group list-group-sm">
        <input type="hidden" ng-model="forgot.email"  ng-init="forgot.email='<?php echo $email ?>'">
        <input type="hidden" ng-model="forgot.token" ng-init="forgot.token='<?php echo $token ?>'">
          <div class="list-group-item">
            <input type="password" id="password" class="form-control" ng-model="forgot.password" name="forgot.password" required="required" placeholder="Password" ng-keyup="onpassword()">
          </div>
          <div class="list-group-item">
            <input type="password" id="repassword" class="form-control" ng-model="forgot.repassword" name="forgot.repassword" required="required" placeholder="Re-Type Password" ng-keyup="onpassword()">
          </div>
       
          <span ng-show="IsMatch">Password Does not match!</span>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block btn-success" ng-disabled='form.$invalid'>Reset Password</button>
        <div class="line line-dashed"></div>
      </form>
      <p class="text-center"><small>Dont share your password to anyone.</small></p>

          
    </div>
  </div>
  <?php echo $this->tag->javascriptInclude('js/jquery/jquery.min.js'); ?>
  <!-- JS -->
  <?php echo $this->tag->javascriptInclude('js/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/checklist-model.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-bootstrap-tpls.min.js'); ?>
  
  <script type="text/javascript">
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("repassword");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
  </script>

  <script type="text/javascript">
    var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate'
    ])
    .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

   })
    .controller('changepasswordctrl', function($scope, $http) {


        $http({
          url: API_URL + "/checktoken/check/<?php echo $token ?>",
          method: "POST",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
        }).success(function (data, status, headers, config) {
          console.log(data.msg);
          if(data.msg == 'valid')
          {
            $scope.validform = true;
            $scope.authError = false;
          }
          else
          {
            $scope.validform = false;
            $scope.authError = true;
          }
        }).error(function(data, status, headers, config) {

        });

         $scope.send = function(forgot) {
           console.log(forgot);
        $http({
            url:API_URL + "/updatepassword/token",
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(forgot)

        }).success(function(data, status, headers, config) {
            
            $scope.validform = false;
            $scope.authsuccess = true;

        }).error(function(data, status, headers, config) {
           

        });

    };






  });
  </script>
</body>
</html>