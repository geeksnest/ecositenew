<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="previewletter.html">
   <div ng-include="'/tpl/previewletter.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Create News Letter</h1>
    <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveNewsLetter(newsletter)" name="formNewLetter">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="row">
                <div class="col-sm-10">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            News Letter Information
                        </div>
                        <div class="panel-body">
                            <label class="col-sm-2 control-label">
                            Letter Title
                            </label>
                            <div class="col-sm-10">
                                <!-- ngIf: user.usernametaken == true -->
                                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="newsletter.title" required="required">
                            </div>
                            <br>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">News Content</label>
                            <div class="col-sm-10">

                                <textarea class="ck-editor" ng-model="newsletter.body">
                             



                                </textarea>
                                <br>

                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <a class="btn btn-default btn-block" ng-click = "previewletter(newsletter.body)"><i class="fa fa-bars pull-left"></i> Preview News Letter</a>

                            </div>
                        </div>

                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            <div class="row">
                <div class="panel-body">
                    <footer class="panel-footer text-right bg-light lter">
                        <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </footer>
                </div>
            </div>
       
        </div>
    </fieldset>
 </form>

<div class="wrapper-md ">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading font-bold"> Upload New Photo</div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="widget">
                            <div class="widget-content">
                                <div class="padd">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Add files...</span>
                                            <input id="digitalAssets" type="file" name="files[]" multiple>
                                        </span>
                                        <div class="line line-dashed b-b line-lg pull-in"></div>
                                        <div id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data">
                                            <div class="gallery digital-assets-gallery">
                                                <div class="clearfix"></div>
                                            </div>
                                        </form>

                                        <div class="col-sm-4" ng-repeat="data in data">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="panel-heading">
                                                        <ul class="nav nav-pills pull-right">
                                                            <li>
                                                                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltnewsimageClick(dlt)" name="form" enctype="multipart/form-data">
                                                                    <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid" class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                                                                    <div class="buttonPanel{[{ data.imgid }]}">
                                                                        <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                                                                    </div>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    <br>
                                                    <br>
                                                    </div>
                                                    <input type="text" class="form-control" ng-model="data.imgpath">
                                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                                    <img style="width:350px;height:250px;position:relative; " ng-src="<?php echo $this->config->application->apiURL; ?>/images/sliderimages/{[{ data.imgpath }]}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>