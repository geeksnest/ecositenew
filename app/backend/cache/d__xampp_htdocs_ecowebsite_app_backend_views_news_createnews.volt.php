<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletenewsimgModal.html">
  <div ng-include="'/tpl/deletenewsimgModal.html'"></div>
</script>
<script type="text/ng-template" id="addcategory.html">
 <div ng-include="'/tpl/addcategory.html'"></div>
</script>
<script type="text/ng-template" id="selectImageThumbnails.html">
 <div ng-include="'/tpl/selectImageThumbnails.html'"></div>
</script>
<script type="text/ng-template" id="addtags.html">
 <div ng-include="'/tpl/addtags.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create News</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveNews(news)" name="formNews" id="formNews">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Information
            </div>
            <div class="panel-body">
              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">News Title</label>
                </label>
                <div class="col-sm-12">
                  <!-- ngIf: user.usernametaken == true -->
                  <input type="text" id="title" name="title" class="form-control" ng-model="news.title" name="title" required="required" maxlength="255" ng-blur="onnewstitle(news.title)">
                  <br>
                  <b>News Slugs: </b>
                  <input type="hidden" style="text-transform: lowercase;" ng-model="news.slugs"><span style="text-transform: lowercase;" ng-bind="news.slugs"></span>
                  <br>   
                </div>              
              </div>
              
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Summary</label>
                </label>
                <div class="col-sm-12">
                  <em class="text-muted">(maximum 1000 characters only)</em>
                  <textarea ng-model="news.summary" maxlength="1000" required="required" class="form-control" style="resize:vertical;"></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="row form-group">
                <div class="col-sm-12">
                  <label class="control-label">News Content</label>                
                  <span class="pull-right">
                    <a class="btn btn-default"  ng-click="selectthumbnail('clipboard')"><i class="icon-folder"></i> Media Library</a>
                  </span>
                  <br>
                  <br>
                </div>
                <div class="col-sm-12">
                  <textarea class="ck-editor" ng-model="news.body" required="required"></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row form-group">
                <div class="panel-heading font-bold">
                  Meta
                </div>
                <div class="col-sm-12">
                  <input type="text" id="metatitle" name="metatitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatitle" required="required" placeholder="Meta Title">
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
                <div class="col-sm-12">
                  <input type="text" id="metadesc" name="metadesc" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metadesc" required="required" placeholder="Meta Description">
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
                <div class="col-sm-12">
                  <input type="text" id="metakeyword" name="metakeyword" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metakeyword" required="required" placeholder="Meta Keyword">
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-sm-4">  

          <div class="panel panel-default">
            <div class="panel-body">
            <footer class="text-right bg-light lter">
                <button type="button" id="draft" name="draft" class="btn btn-default" ng-disabled="formNews.$invalid" ng-click="preview(news)"><i class="glyphicon glyphicon-eye-open"></i> Preview</button>
                <button type="button" id="draft" name="draft" class="btn btn-default" ng-disabled="formNews.$invalid" ng-click="savedraft(news)"><i class="glyphicon glyphicon-edit"></i> Save Draft</button>
                <button type="submit" id="submit" name="submit" ng-disabled="formNews.$invalid" class="btn btn-success"><i class="fa fa-upload"></i> Publish</button>
              </footer>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Publish Date
            </div>
            <div class="panel-body">
              <div class="row form-group">
                <div class="col-sm-12">
                  <em class="text-muted">Select date here.</em>
                  <div class="input-group w-md">               
                    <span class="input-group-btn">
                      <input type="hidden" ng-model="news.date2">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="news.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>   
                  </div>
                </div>
              </div>
            </div>
          </div>   

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <div class="row">
                <div class="col-xs-6">Category</div>              
                <div class="col-xs-6 text-right">
                  <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addcategory()"><i class="fa fa-plus" style="width=100%;"></i>Add Category</button>
                </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row form-group">
                <div class="col-sm-12" id="chosen">
                  <div>
                    <!-- <select chosen allow-single-deselect class="form-control" ng-model="news.category" options="category" ng-options="cat.categoryid as cat.categoryname for cat in category" required="required">
                  </select> -->
                  <!-- <em class="text-muted">Select categories.</em> -->
                  <select chosen multiple options="category" class="form-control m-b" ng-options="cat as cat.categoryname for cat in category track by cat.categoryid" ng-model="news.category" required="required">
                  </select>  
                  <label><em class="text-muted">Select category or add if the category doesnt exist.</em></label>
                </div>
              </div>
            </div>
          </div>
        </div>  

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            <div class="row">
              <div class="col-xs-6">Tags</div>                
              <div class="col-xs-6 text-right">
                <!-- <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addtags()"><i class="fa fa-plus" style="width=100%;"></i>Add Tags</button> -->
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="row form-group">
              <div class="col-sm-12" id="chosen">
                <div>
                  <!-- <em class="text-muted">Select tags here.</em> -->
                  <!-- <select chosen multiple options="tag" class="form-control m-b" ng-options="item as item.tags for item in tag track by item.id" ng-model="news.tag" required="required">
                  </select> -->

                  <ui-select multiple tagging tagging-label="false" ng-model="news.tag" theme="bootstrap" ng-disabled="disabled" class="form-control m-b">
                    <ui-select-match placeholder="Select tags...">{[{ $item }]}</ui-select-match>
                    <ui-select-choices repeat="ta in tag">{[{ ta }]}</ui-select-choices>
                  </ui-select>
                  <label><em class="text-muted">For tags that doesnt exist just type and press enter. This will be added to your database of tags once the page is published.</em></label>
                </div>  
              </div>
              
            </div>
          </div>
        </div>            

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Author
          </div>
          <div class="panel-body">
            <div class="row form-group">
              <div class="col-sm-12">
                <em class="text-muted">Please select the Author here.</em>
                <select class="form-control" ng-model="news.author" required="required">
                  <option ng-repeat="mem in author track by $index" value="{[{mem.authorid}]}">{[{mem.name}]}</option>
                </select> 
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Thumbnail
          </div>
          <div class="panel-body">
            <div class="input-group m-b">
              <span class="input-group-btn">
                <a class="btn btn-default"  ng-click="selectthumbnail('thumb')"><i class="icon-folder"></i> Media Library</a>
              </span>
            </div>

            <div>
              <div class="line line-dashed b-b line-lg"></div>
              <img ng-show="news.imagethumb" src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimages/{[{ news.imagethumb }]}" style="width: 100%">
              <input type="hidden" id="banner" name="banner" class="form-control" ng-model="news.imagethumb"  placeholder="Paste thumbnail link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(news.imagethumb)">
            </div>

            <div>
              <input type="hidden" ng-model='news.videothumb' ng-value='paste(news)' class="form-control" rows="4" style="resize:vertical;">
              <div style="padding:10px;" ng-bind-html="vidpath"></div>
              <div ng-model="previewvideo">
                {[{ upVid.path }]}
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Featured in:
          </div>
          <div class="panel-body">
            <div class="row form-group">
              <label class="col-sm-12 control-label"><em class="text-muted">This field is optional.</em></label>
              <div class="col-sm-12">
                <select class="form-control" ng-model="news.featurednews">
                  <option value=""></option>
                  <option value="1">Upcoming Programs</option>
                  <option value="2">Recent Activities</option>
                  <option value="3">Webinar Archive</option>
                  <option value="4">Mindful Living Tips</option>
                </select> 
                <br>                  
                <label class="i-checks" ng-show="news.featurednews">
                  <input type="checkbox" name="status" ng-checked="news.feat == 1" ng-model='news.feat'>
                  <i></i> Set this as the featured?
                </label>
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
          </div>
        </div>


      </div>



    </div>

    <!-- <div class="row">
      <div class="panel-body">
        <footer class="panel-footer text-right bg-light lter">
          <button type="button" id="draft" name="draft" class="btn btn-default" ng-disabled="formNews.$invalid" ng-click="savedraft(news)"><i class="glyphicon glyphicon-edit"></i> Save Draft</button>
          <button type="submit" id="submit" name="submit" ng-disabled="formNews.$invalid" class="btn btn-success"><i class="fa fa-upload"></i> Publish</button>
        </footer>
      </div>
    </div> -->

  </div>
</fieldset>
</form>




