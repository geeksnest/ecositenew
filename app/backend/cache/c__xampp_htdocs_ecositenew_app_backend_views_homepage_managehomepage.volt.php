<?php echo $this->getContent(); ?>
<style>
.sortable{
    overflow-y: auto;
    overflow-x: hidden;
}
</style>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="homepageUpdate.html">
  <div ng-include="'/tpl/homepageUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Homepage</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="panel-heading">
      Homepage Content List
    </div>
    <form name="Form" >
      <table class="table table-striped b-t b-light sortable" ui:sortable ng:model="data.data">
        <thead>
          <tr>
            <th style="width:70%">Title</th>
            <th style="width:10%">Date Created</th>
            <th style="width:20%">Action</th>
          </tr>
        </thead>
        <tbody class="table table-striped b-t b-light sortable" ui:sortable ng:model="data">

          <tr ng-repeat="content in data track by $index" data-id="data[$index].id">
            <td>{[{ content.title }]}</td>
            <td>{[{ content.datecreated | date:'mediumDate' }]}</td>
            <td>
              <button ng-show="content.title !='NEWS'" ng-click="update(content.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>
              <button ng-show="content.title !='NEWS'" ng-click="delete(content.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
              <a ng-show="content.title =='NEWS'">
                <i class="icon-question text-info pointer" title=""  custom-popover popover-html="This Data is not Editable and Deletable" popover-placement="left" popover-label=""></i>
              </a>
            </td>
          </tr>

        </tbody>
      </table>
      
    </form>
    <div class="col-sm-12 m-b-xs">
     <button class="btn btn-success btn-addon btn-lg" ng-click="updatesort(data)"><i class="fa fa-plus"></i>Update</button>
    </div>
    <footer class="panel-footer">
      <div class="row">

      </div>
    </footer>
  </div>
</div>
