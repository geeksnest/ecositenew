<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/animate.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font-awesome.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/simple-line-icons.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/app.css'); ?>
  <script>
        var API_URL = '<?php echo $this->config->application->apiURL; ?>';
  </script>
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">-->
</head>

<body ng-controller="forgotpasswordctrl">
  <div class="container w-xxl w-auto-xs" >
  <div class="m-b-lg">
  <a href class="navbar-brand block m-t"><!--<?php echo $app->name; ?>--></a>
    <div class="wrapper text-center" style="margin-top: 100px;">
      <img src='/img/ecologo.png' style="width:300px;height:178px;">
    </div>
    <form name="formForgotpassword" class="form-validation" method="post">
      <div class="text-danger wrapper text-center" ng-show="authError">
        <?php echo $this->getContent(); ?>
      </div>
      <div class="list-group list-group-sm" ng-show="authError" style="text-align: center;">
        {[{ alerts }]}
      </div>
      
      <div class="list-group list-group-sm" style="text-align: center;">
        <!-- <span class="list-group list-group-sm" ng-if="chkuremail == true">The link to reset your password has been sent to your email address. Please check your inbox.<br></span> -->
        <span class="list-group list-group-sm" ng-if="sending == true">Processing your request...<br></span>
      </div>
      
      <div class="list-group list-group-sm" ng-if="formforgot == true">
        <div class="list-group-item">
        
          <!-- <input type="text" class="form-control" ng-model="news.author" name="news.author" required="required" placeholder="Email Address"> -->
          <?php echo $this->tag->textField(array('email', 'size' => '30', 'class' => 'form-control no-border', 'id' => 'inputEmail', 'placeholder' => 'Enter Valid Email Address', 'ng-model' => 'send.email', 'required' => 'required')); ?>
        </div>
      </div>
      <button ng-if="formforgot == true" type="submit" class="btn btn-lg btn-primary btn-block btn-success" ng-click="send(send)" ng-disabled='formForgotpassword.$invalid'>Send Password Reset</button>
      <div class="line line-dashed"></div>
      <p class="text-center" ng-if="formforgot == true"><small>Dont share your password to anyone.</small></p>
    </form>
  </div>
</div>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('js/jquery/jquery.min.js'); ?>
<!-- angular -->
<?php echo $this->tag->javascriptInclude('js/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/checklist-model.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-bootstrap-tpls.min.js'); ?>

<!-- APP -->
 

 <script type="text/javascript">
  var app = angular.module('app', [])
  .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

   })
  .controller('forgotpasswordctrl', function($scope, $http) {
   

    $scope.formforgot = true;
    $scope.chkuremail = false;
    $scope.sending = false;
    $scope.send = function(send){
      $scope.chkuremail = false;
      $scope.sending = true;
      console.log("send email");
      console.log(send.email);
      $http({
        url: API_URL + "/forgotpassword/resetpassword",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(send)
      }).success(function (data, status, headers, config) { 
        $scope.formforgot = false;
        $scope.authError = true;
        $scope.alerts = data.msg;                  
        console.log('success');
        $scope.sending = false;
        $scope.chkuremail = true;
      }).error(function (data, status, headers, config) {
        console.log('not success');
      });
  
    }   

  });

 </script>
</body>
</html>