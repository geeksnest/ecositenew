<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="review.html">
  <div ng-include="'/tpl/callingheroesModalReview.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Calling Heroes</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Request
    </div>
    <div class="row wrapper">
      <div class="col-sm-6 m-b-xs" ng-hide="advancesearch">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" ng-click="search(searchtext)" type="button">Go!</button>
          </span>
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
      <!-- <div class="col-sm-6 m-b-xs" ng-show="advancesearch">
        From 
        <input type="date" id="from" ng-model="dfrom">
        To
        <input type="date" id="to" ng-model="dto">
        <button type="button" class="btn btn-default" ng-click="filterdate(dfrom,dto)"><i class="glyphicon glyphicon-search"></i> Filter By Date</button>
        <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
      </div> -->
      <div class="col-sm-12 m-b-xs" ng-show="advancesearch">
       <div class="col-sm-3">
           From 
         <!-- <input type="date" id="from" ng-model="dfrom"> -->
         <div class="input-group">
           <input type="text" class="form-control" datepicker-popup="{[{format1}]}" 
           ng-model="dfrom" is-open="opened1" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="Start Date" />
           <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="open($event,'opened1')"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </div>
       </div> 
       <div class="col-sm-3">
       To
         <div class="input-group">
         <!--  <span class="input-group-btn"> -->
            <input type="text" class="form-control" datepicker-popup="{[{format2}]}" 
            ng-model="dto" is-open="opened2" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="End Date" />
            <span class="input-group-btn">
             <button type="button" class="btn btn-default" ng-click="open($event,'opened2')"><i class="glyphicon glyphicon-calendar"></i></button>
           </span>
          <!-- </span> -->
        </div>
       </div>
         <div class="col-sm-4">
         <br>
         <div class="input-group">
         <button type="button" class="btn btn-default" ng-click="filterdate(dfrom,dto)"><i class="glyphicon glyphicon-search"></i> Filter By Date</button>
         <button class="btn btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
         </div>
         </div>
       </div>
      <br>
      <div class="checkbox col-sm-12">
        <label class="i-checks">
          <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Filter By Date
        </label>
      </div>
    </div>


    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>
            <th style="width:30%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'organization','DESC')"> Organization
                  <span ng-show="sortIn == 'organization' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'organization','ASC')"> Organization
                  <span ng-show="sortIn == 'organization' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'taxid','DESC')"> Tax ID
                  <span ng-show="sortIn == 'taxid' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'taxid','ASC')"> Tax ID
                  <span ng-show="sortIn == 'taxid' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>


            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'phone','DESC')"> Phone #
                  <span ng-show="sortIn == 'phone' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'phone','ASC')"> Phone #
                  <span ng-show="sortIn == 'phone' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:5%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th style="width:10%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'datesent','DESC')"> Date
                  <span ng-show="sortIn == 'datesent' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'datesent','ASC')"> Date
                  <span ng-show="sortIn == 'datesent' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td>
              {[{ mem.organization }]}
            </td>
            <td>
              {[{ mem.taxid }]}
            </td>
            <td>
              {[{ mem.phone }]}
            </td>

            <td  ng-if="mem.status == 1">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-success">Reviewed </span></div>
            </td>
            <td  ng-if="mem.status == 0">
              <div class="pagestatuscontent fade-in-out"><span class="label bg-warning">New</span></div>
            </td>
            <td>
              {[{ mem.datesent }]}
            </td>
            <td>
              <a href="" ng-click="review(mem.id, bigCurrentPage, searchtext)"><span class="label bg-primary">Review</span></a>
              <a href="" ng-click="deletepage(mem.id, bigCurrentPage, searchtext)"> <span class="label bg-danger">Delete</span>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-8">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small> 
        </div>
        <div class="col-sm-4">        
          <div class="input-group">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
  </div>
</div>
