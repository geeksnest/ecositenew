<!DOCTYPE html>
<?php include "../app/config/config.php"; ?>
<html lang="en" data-ng-app="app">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <!-- Title and other stuffs -->
        <?php echo $this->tag->getTitle(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
    <link rel="shortcut icon" type="image/ico" href="/images/template_images/favicon.png" />  
    <script src="jwplayer-7.2.4/jwplayer.js"></script>
    <script src="https://content.jwplatform.com/libraries/2vfWHgLx.js"></script>

        <!-- Stylesheets -->
        <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?> 
        <?php echo $this->tag->stylesheetLink('css/animate.css'); ?> 
        <?php echo $this->tag->stylesheetLink('css/font-awesome.min.css'); ?> 
        <?php echo $this->tag->stylesheetLink('css/simple-line-icons.css'); ?> 
        <?php echo $this->tag->stylesheetLink('css/font.css'); ?> 
        <?php echo $this->tag->stylesheetLink('css/app.css'); ?>
        <?php echo $this->tag->stylesheetLink('css/custom.css'); ?>
        <?php echo $this->tag->stylesheetLink('css/template_css/custom-style.css'); ?>
        <!-- jquery fileuploader -->
        <?php echo $this->tag->stylesheetLink('css/jquery.fileupload.css'); ?> 
        <?php echo $this->tag->stylesheetLink('js/tokeninput/styles/token-input.css'); ?> 
        <?php echo $this->tag->stylesheetLink('js/select2/dist/css/select2.min.css'); ?>
        <?php echo $this->tag->stylesheetLink('js/jquery/chosen/chosen.css'); ?>
        <?php echo $this->tag->stylesheetLink('js/jquery/chosen/chosen-spinner.css'); ?>
        <?php echo $this->tag->stylesheetLink('js/angular/chosen/chosen.css'); ?>
        <?php echo $this->tag->stylesheetLink('css/isteven-multi-select.css'); ?>

        <?php echo $this->tag->stylesheetLink('js/angular/angular-xeditable/dist/css/xeditable.css'); ?>
        <?php echo $this->tag->stylesheetLink('js/angular/angular-ui-select/dist/select.css'); ?>
        <?php echo $this->tag->stylesheetLink('js/angular/angular-bootstrap-colorpicker-master/css/colorpicker.css'); ?>

        <!-- HTML5 Support for IE -->
        <!--[if lt IE 9]>
      <script src="js/html5shim.js"></script>
      <![endif]-->

        <!-- Favicon 
        <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">-->
    </head>

    <body ng-controller="AppCtrl">
        <?php echo $this->getContent(); ?>
        <div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
            <div class="app-header navbar">
                <!-- navbar header -->
                <div class="navbar-header bg-info dker">
                    <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
                        <i class="glyphicon glyphicon-cog"></i>
                    </button>
                    <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
                        <i class="glyphicon glyphicon-align-justify"></i>
                    </button>
                    <!-- brand -->
                    <a href="#/" class="navbar-brand text-lt">
                        <img src="/img/minieco.png" alt=".">
                    </a>
                    <!-- / brand -->
                </div>
                <!-- / navbar header -->

                <!-- navbar collapse -->
                <div class="collapse navbar-collapse box-shadow bg-info dker">
                    <!-- buttons -->
                    <div class="nav navbar-nav m-l-sm hidden-xs">
                        <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
                            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
                        </a>
                        <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
                            <i class="icon-user fa-fw"></i>
                        </a>
                    </div>

                    <!-- / buttons -->

                    <!-- link and dropdown -->
                    <ul class="nav navbar-nav hidden-sm">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
                                <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="#">
                                        <span class="badge bg-info pull-right">5</span>
                                        <span translate="header.navbar.new.NEWS">Task</span>
                                    </a>
                                </li>
                                <li><a href="#" translate="header.navbar.new.BLOG">User</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="badge bg-danger pull-right">4</span>
                                        <span translate="header.navbar.new.PAGE">Email</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="badge bg-danger pull-right">4</span>
                                        <span translate="header.navbar.new.USER">Email</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- / link and dropdown -->

                    <!-- search form -->
                    <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <!-- / search form -->

                    <!-- nabar right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden-xs">
                            <a ui-fullscreen></a>
                        </li>
                        <li class="dropdown">
                            <a href class="dropdown-toggle">
                                <i class="icon-bell fa-fw"></i>
                                <span class="visible-xs-inline">Notifications</span>
                                <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
                            </a>
                            <!-- dropdown -->
                            <div class="dropdown-menu w-xl animated fadeInUp">
                                <div class="panel bg-white">
                                    <div class="panel-heading b-light bg-light">
                                        <strong>You have <span>2</span> notifications</strong>
                                    </div>
                                    <div class="list-group">
                                        <a href class="media list-group-item">
                                            <span class="pull-left thumb-sm">
                                                <img src="/img/a0.jpg" alt="..." class="img-circle">
                                            </span>
                                            <span class="media-body block m-b-none">
                                                Use awesome animate.css<br>
                                                <small class="text-muted">10 minutes ago</small>
                                            </span>
                                        </a>
                                        <a href class="media list-group-item">
                                            <span class="media-body block m-b-none">
                                                1.0 initial released<br>
                                                <small class="text-muted">1 hour ago</small>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="panel-footer text-sm">
                                        <a href class="pull-right"><i class="fa fa-cog"></i></a>
                                        <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                                    </div>
                                </div>
                            </div>
                            <!-- / dropdown -->
                        </li>
                        <li class="dropdown">
                            <form  name="form">
                                <input id="setid" type="hidden"  value="<?php echo $userid; ?>">
                            </form>
                            <a href class="dropdown-toggle clear" data-toggle="dropdown">
                                <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                                    <img style="height:40px" src="<?php echo $this->config->application->amazonlink."/uploads/userimages/".$imgg; ?>" alt="...">
                                    <i class="on md b-white bottom"></i>
                                </span>
                                <span class="hidden-sm hidden-md"><?php echo $fullname; ?></span> <b class="caret"></b>
                            </a>
                            <!-- dropdown -->
                            <ul class="dropdown-menu animated fadeInRight w">
                                <!-- <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                                    <div>
                                        <p>300mb of 500mb used</p>
                                    </div>
                                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
                        </li> -->
                        <?php
                        if(in_array('settingsrole', $roles)){
                        ?>
                        <li>
                            <a ui-sref="managesettings">
                                <!-- <span class="badge bg-danger pull-right">30%</span> -->
                                <span translate="aside.nav.system_stuff.SETTINGS">Settings</span>
                            </a>
                        </li>
                        <?php } ?>
                        <li>
                            <a ui-sref="editprofile({userid:'<?php echo $userid; ?>'})">
                                <span translate="aside.nav.profile.EDIT_PROFILE">Edit Profile</span>
                            </a>
                        </li>
                        <li><!-- ui-sref="app.docs" -->
                           <!--  <a> 
                                <span class="label bg-info pull-right">new</span> Help
                            </a> -->
                        </li>
                        <li class="divider"></li>
                        <li>
                        
                            <a href="<?php echo $this->config->application->baseURL . '/ecoadmin/index/logout' ?>" name="logout">Logout  </a>
                        
                            
                        </li>
                    </ul>
                    <!-- / dropdown -->
                    </li>
                    </ul>
                    <!-- / navbar right -->

                </div>
                <!-- / navbar collapse -->
            </div>

            <!-- menu -->
            <div class="app-aside hidden-xs bg-light dker">
                <div class="aside-wrap">
                    <div class="navi-wrap">
                        <!-- user -->
                        <div class="clearfix hidden-xs text-center hide" id="aside-user">
                            <div class="dropdown wrapper">
                                <a ui-sref="app.page.profile">
                                    <span class="thumb-lg w-auto-folded avatar m-t-sm">
                                        <img src="<?php echo $this->config->application->amazonlink."/uploads/userimages/".$imgg; ?>" alt="...">
                                    </span>
                                </a>
                                <a href class="dropdown-toggle hidden-folded">
                                    <span class="clear">
                                        <span class="block m-t-sm">
                                            <strong class="font-bold text-lt"><?php echo $fullname; ?></strong> 
                                            <b class="caret"></b>
                                        </span>
                                        <span class="text-muted text-xs block"><?php echo $agentType; ?></span>
                                    </span>
                                </a>
                                <!-- dropdown -->
                                <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                                    <!-- <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
                                        <span class="arrow top hidden-folded arrow-info"></span>
                                        <div>
                                            <p>300mb of 500mb used</p>
                                        </div>
                                    <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
                                    </li> -->
                                    <?php
                                    if(in_array('settingsrole', $roles)){
                                        ?>                                    
                                    <li>
                                        <a ui-sref="managesettings">Settings</a>
                                    </li>
                                    <?php } ?>
                                    <li>
                                        <a ui-sref="editprofile({userid:'<?php echo $userid; ?>'})">Profile</a>
                                    </li>
                                    <!-- <li>
                                        <a href>
                                            <span class="badge bg-danger pull-right">3</span> Notifications
                                        </a>
                                    </li> -->
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo $this->config->application->baseURL . '/ecoadmin/index/logout' ?>">Logout</a>
                                    </li>
                                </ul>
                                <!-- / dropdown -->
                            </div>
                            <div class="line dk hidden-folded"></div>
                        </div>
                        <!-- / user -->

                        <!-- nav -->
                        <nav ui-nav class="navi">
                            <!-- first -->
                            <ul class="nav">
                                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                                    <span translate="aside.nav.HEADER">Navigation</span>
                                </li>
                                <li>
                                    <a ui-sref="dashboard">
                                        <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                                        <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
                                    </a>
                                </li>
                                <?php
                                if(in_array('usersrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.users')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>

                                        <i class="glyphicon glyphicon-user icon"></i>
                                        <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="userlist">
                                                <span translate="aside.nav.users.USER_LIST">User List</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="members">
                                                <span translate="aside.nav.users.MEMBERS">Members List</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="userscreate">
                                                <span translate="aside.nav.users.CREATE_USER">Create User</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>  
                                <?php
                                }
                                if(in_array('donationsrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.donations')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">

                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-usd icon text-info-dker"></i>
                                        <span class="font-bold" translate="aside.nav.donations.DONATIONS">Donations</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <?php 
                                        if(in_array('donationlist', $page['donation'])){
                                        ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="donationlist">
                                                <span translate="aside.nav.donations.Donations_List">Donations List</span>
                                            </a>
                                        </li>
                                        <?php }
                                        if(in_array('bnbregistration', $page['donation'])){ ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="bnbregistration">
                                                <span translate="aside.nav.donations.BNB_Registration">BNB Registration</span>
                                            </a>
                                        </li>
                                        <?php }
                                        if(in_array('donationothers', $page['donation'])){ ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="donationothers">
                                                <span translate="aside.nav.donations.Donations_Others">Other Donations</span>
                                            </a>
                                        </li>
                                        <?php }
                                        if(in_array('nyearthcitizenwalk', $page['donation'])){ ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="nyearthcitizenwalk">
                                                <span translate="aside.nav.donations.Donations_NYWALK">NY Walk Donations</span>
                                            </a>
                                        </li>
                                        <?php }
                                        if(in_array('houstonearthcitizenswalk', $page['donation'])){ ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="houstonearthcitizenswalk">
                                                <span translate="aside.nav.donations.Donations_HOUSTON">HOUSTON Walk Donations</span>
                                            </a>
                                        </li>
                                        <?php }
                                        if(in_array('seattlenaturalhealingexpo', $page['donation'])){ ?>
                                        <li ui-sref-active="active">
                                            <a ui-sref="seattlenaturalhealingexpo">
                                                <span translate="aside.nav.donations.Donations_SEATTLE">SEATTLE Expo Donations</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php
                                }
                                if(in_array('vendorrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.vendor')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">

                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <!-- <i class="glyphicon glyphicon-usd icon text-info-dker"></i> -->
                                        <i class="glyphicon glyphicon-heart-empty icon text-info-dker"></i>
                                        <span class="font-bold" translate="aside.nav.vendor.VENDOR">Vendor</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="vendorseattle">
                                                <span translate="aside.nav.vendor.Vendor_Seattle">Donations List</span>
                                            </a>
                                        </li>                                        
                                    </ul>
                                </li>
                                <?php
                                }
                                if(in_array('programrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.programs')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">

                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <!-- <i class="glyphicon glyphicon-usd icon text-info-dker"></i> -->
                                        <i class="fa fa-gears icon text-info-dker"></i>
                                        <span class="font-bold" translate="aside.nav.programs.PROGRAMS">Vendor</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="heroesleadershiptraining">
                                                <span translate="aside.nav.programs.hlt">Donations List</span>
                                            </a>
                                        </li>                                        
                                    </ul>
                                </li>
                                <?php
                                }
                                if(in_array('projectsrole', $roles)){
                                ?>
                                <li ui-sref-actives="active">
                                    <a ui-sref="projectslist">
                                        <i class="fa fa-leaf icon text-success"></i>
                                        <span class="font-bold" translate="aside.nav.projects.PROJECTS">Projects</span>
                                    </a>
                                </li>
                                <?php
                                }
                                if(in_array('medialibraryrole', $roles)){
                                ?>
                                <li ui-sref-actives="active">
                                    <a ui-sref="medialibrarylist">
                                        <i class="fa fa-video-camera icon text-success"></i>
                                        <span class="font-bold">Media Library</span>
                                    </a>
                                </li>
                                <?php
                                }

                                if(in_array('clubrole', $roles)){
                                ?>
                                <li ui-sref-actives="active">
                                    <a ui-sref="clublist">
                                        <i class="fa fa-users icon text-success"></i>
                                        <span class="font-bold">Club</span>
                                    </a>
                                </li>
                                <?php
                                }
                                // if(in_array('pearmarksrole', $roles)){
                                ?>
                                <!-- <li ng-class="{active:$state.includes('app.peacemarks')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-tree-conifer icon text-info-dker"></i>
                                        <span class="font-bold" translate="aside.nav.map.MAPS">Peace Maps</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="peacemarks({pageid: page.id})">
                                                <span translate="aside.nav.map.Create_Peacemap">Create Peacemap</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="managepeacemarks">
                                                <span translate="aside.nav.map.Manage_Peacemap">Manage Peacemaps</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li> -->
                                <?php
                                // }
                                if(in_array('newsrole', $roles)){
                                ?>
                                <!-- news rainier -->
                                <li ng-class="{active:$state.includes('app.news')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">

                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
                                        <span class="font-bold" translate="aside.nav.news.NEWS">News and Article</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="createnews">
                                                <span translate="aside.nav.news.Create_News">Create News</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="managenews">
                                                <span translate="aside.nav.news.Manage_News">Manage News</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="category">
                                                <span translate="aside.nav.news.Category">News Category</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="createauthor">
                                                <span translate="aside.nav.news.Create_Author">Create Author</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="manageauthor">
                                                <span translate="aside.nav.news.Manage_Author">Manage Author</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="newstags">
                                                <span translate="aside.nav.news.News_Tags">News Tags</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                                }
                                if(in_array('homepagesrole', $roles)){
                                ?>
                                <!-- homepage ryan -->
                                <li>
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-th-large icon text-primary"></i>
                                        <span class="font-bold">Homepage</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="toppage">
                                                <span>Top page</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="createhomepage">
                                                <span>Create</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="managehomepage">
                                                <span>Manage</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                               
                                <?php
                                }
                                if(in_array('pagesrole', $roles)){
                                ?>
                                <!-- pages rainier -->
                                <li ng-class="{active:$state.includes('app.pages')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-th-large icon text-success"></i>
                                        <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="createpage">
                                                <span translate="aside.nav.pages.Create_Page">Create Page</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="managepages">
                                                <span translate="aside.nav.pages.Manage_Page">Manage Page</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="otherpages">
                                                <span >Other Page</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                               
                                <?php
                                }
                                 if(in_array('callheroesrole', $roles)){
                                ?>
                                    <li ng-class="{active:$state.includes('app.callheroes')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>

                                        <i class="icon-users"></i>
                                        <span class="font-bold" translate="aside.nav.callheroes.TITILE">Call Heroes</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="callheroes">
                                                <span translate="aside.nav.callheroes.CALLHEROES">Call Heroes</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="heroesonduty">
                                                <span translate="aside.nav.callheroes.HEROESONDUTY">Heroes on Duty</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>     
                                <?php
                                
                                }if(in_array('organizationsrole', $roles)){
                                ?>
                                <li ui-sref-actives="active">
                                    <a href>
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>

                                         <i class="fa fa-globe"></i>
                                        <span class="font-bold"> Organizations </span>
                                    </a>
                                    <ul class="nav nav-sub dk">

                                        <li ui-sref-active="active">
                                            <a ui-sref="atw">
                                                Around the World
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                        <a ui-sref="friends-and-allies">
                                                Friends and Allies
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                                <?php }
                                if(in_array('calendarrole', $roles)){
                                ?>
                                <!-- <li ng-class="{active:$state.includes('app.calendar')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-calendar icon text-info-dker"></i>
                                        <span class="font-bold" translate="aside.nav.calendar.CALENDAR">Calendar</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="addcalendar">
                                                <span translate="aside.nav.calendar.Add_Calendar">Add Activity</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="viewcalendar">
                                                <span translate="aside.nav.calendar.View_Calendar">View Activity</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li> -->
                                <?php
                                }
                                ?>
                                <li class="line dk"></li>

                                <!-- <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                                    <span translate="aside.nav.others.OTHERS">Others</span>
                                </li> -->
                                <?php
                                if(in_array('proposalesrole', $roles)){
                                ?>
                                <!-- proposals uson -->
                                <!-- <li ng-class="{active:$state.includes('app.proposals')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon  glyphicon-list-alt icon"></i>
                                        <span class="font-bold" translate="aside.nav.proposals.PROPOSALS">Proposals</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">

                                            <a ui-sref="manageproposals">
                                                <span translate="aside.nav.proposals.Manage_Proposals">Manage Proposals</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li> -->
                                <?php
                                }
                                if(in_array('newslettersrole', $roles) || in_array('subscriberrole', $roles)){
                                ?>
                                <!-- <li ng-class="{active:$state.includes('app.newsletter')}">

                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon glyphicon-envelope icon"></i>
                                        <span class="font-bold" translate="aside.nav.newsletter.NEWSLETTER">News</span>
                                    </a> -->
                                    <?php
                                            if(in_array('subscriberrole', $roles)){
                                            ?>
                                    <!-- <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="addsubscriber">

                                                <span translate="aside.nav.newsletter.Add_Subscriber">Add Subscribers</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="subscriberslist">
                                                <span translate="aside.nav.newsletter.Subscribers_List">Subscribers List</span>
                                            </a>
                                        </li>
                                    </ul> -->
                                    <?php
                                           } if(in_array('newslettersrole', $roles)){
                                            ?>
                                    <!-- <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="createnewsletter">
                                                <span translate="aside.nav.newsletter.Create_Newsletter">Create News</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="managenewsletter">
                                                <span translate="aside.nav.newsletter.Manage_Newsletter">Manage News</span>
                                            </a>
                                        </li>
                                    </ul> -->
                                    <?php
                                            }
                                            ?>
                                </li>
                                <?php
                                }
                                if(in_array('imagesrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.images')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="glyphicon  glyphicon-picture icon"></i>
                                        <span class="font-bold" translate="aside.nav.others.images.IMAGES">Images</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="slider">
                                                <span translate="aside.nav.others.images.SLIDER">Slider</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="banner">
                                                <span translate="aside.nav.others.images.BANNER">Banner</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php                                
                                }
                                if(in_array('eventmanagerrole', $roles)){
                                ?>
                                <li ng-class="{active:$state.includes('app.eventmanager')}">
                                    <a href class="auto">
                                        <span class="pull-right text-muted">
                                            <i class="fa fa-fw fa-angle-right text"></i>
                                            <i class="fa fa-fw fa-angle-down text-active"></i>
                                        </span>
                                        <i class="icon-calendar text-primary"></i>
                                        <span class="font-bold" translate="aside.nav.others.eventmanager.EVENT_MANAGER">Event Manager</span>
                                    </a>
                                    <ul class="nav nav-sub dk">
                                        <li ui-sref-active="active">
                                            <a ui-sref="createevent">
                                                <span translate="aside.nav.others.eventmanager.CREATE_EVENT">Create Event</span>
                                            </a>
                                        </li>
                                        <li ui-sref-active="active">
                                            <a ui-sref="eventlist">
                                                <span translate="aside.nav.others.eventmanager.EVENT_LIST">Event List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                                }
                                if(in_array('testimonialsrole', $roles)){
                                ?>
                                <!-- <li ui-sref-active="active">
                                    <a ui-sref="testimonials">
                                        <i class="glyphicon glyphicon-italic icon"></i>
                                        <span class="font-bold" translate="aside.nav.others.TESTIMONIALS">Testimonials</span>
                                    </a>
                                </li> -->
                                <?php
                                }
                                if($MegaAdmin){
                                ?>
                                <!-- <li ui-sref-active="active">
                                    <a ui-sref="menucreator">
                                        <i class="glyphicon  glyphicon-list icon"></i>
                                        <span class="font-bold" translate="aside.nav.others.MENUCREATOR">Menu Creator</span>
                                    </a>
                                </li> -->
                                <?php
                                }
                                if(in_array('menucreaterole', $roles)){
                                ?>
                                <li ui-sref-actives="active">
                                    <a ui-sref="menu_creator">
                                        <i class="fa fa-reorder"></i>
                                        <span class="font-bold">Menu Creator</span>
                                    </a>
                                </li>
                                <?php }  ?>
                                <li class="line dk hidden-folded"></li>

                                <!-- <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                                    <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
                                </li> -->
                                <li>
                                    <!-- <a ui-sref="app.page.profile">
                                        <i class="icon-user icon text-success-lter"></i>
                                        <b class="badge bg-success dk pull-right">30%</b>
                                        <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
                                    </a> -->
                                    <a ui-sref="editprofile({userid:'<?php echo $userid; ?>'})">
                                        <i class="icon-user icon text-success-lter"></i>
                                        <span class="font-bold" translate="aside.nav.profile.EDIT_PROFILE">Profile</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a> 
                                        <i class="glyphicon glyphicon-tasks icon"></i>
                                        <span translate="aside.nav.your_stuff.LOGS">LOGS</span>
                                    </a>
                                </li> -->
                                <?php
                                if(in_array('settingsrole', $roles)){
                                ?>
                                <li class="line dk hidden-folded"></li>

                                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                                    <span translate="aside.nav.system_stuff.SYSTEM">System</span>
                                </li>
                                <!-- SETTINGS -->
                                <li ui-sref-actives="active">
                                    <a ui-sref="managesettings">
                                        <i class="glyphicon glyphicon-cog icon"></i>
                                        <span class="font-bold" translate="aside.nav.system_stuff.SETTINGS">SETTINGS</span>
                                    </a>
                                </li>
                                <?php 
                                }
                                ?>


                            </ul>
                            <!-- / third -->

                        </nav>
                        <!-- nav -->

                        <!-- aside footer -->
                        <div class="wrapper m-t">
                            <div class="text-center-folded">
                                <span class="pull-right pull-none-folded">60%</span>
                                <span class="hidden-folded" translate="aside.MEMORY">Milestone</span>
                            </div>
                            <progressbar value="60" class="progress-xxs m-t-sm dk" type="info"></progressbar>
                            <div class="text-center-folded">
                                <span class="pull-right pull-none-folded">35%</span>
                                <span class="hidden-folded" translate="aside.DISK">Release</span>
                            </div>
                            <progressbar value="35" class="progress-xxs m-t-sm dk" type="primary"></progressbar>
                        </div>
                        <!-- / aside footer -->
                    </div>
                </div>
            </div>
            <!-- / menu -->
            <!-- content -->
            <div class="app-content">
                <div ui-butterbar></div>
                <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside"></a>
                <div class="app-content-body fade-in-up" ui-view></div>
            </div>
            <!-- /content -->
            <!-- aside right -->
            <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
                <div class="vbox">
                    <div class="wrapper b-b b-light m-b">
                        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a> Chat
                    </div>
                    <div class="row-row">
                        <div class="cell">
                            <div class="cell-inner padder">
                                <!-- chat list -->
                                <div class="m-b">
                                    <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="...">
                                    </a>
                                    <div class="clear">
                                        <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                            <span class="arrow left pull-up"></span>
                                            <p class="m-b-none">Hi John, What's up...</p>
                                        </div>
                                        <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
                                    </div>
                                </div>
                                <div class="m-b">
                                    <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="...">
                                    </a>
                                    <div class="clear">
                                        <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                                            <span class="arrow right pull-up arrow-light"></span>
                                            <p class="m-b-none">Lorem ipsum dolor :)</p>
                                        </div>
                                        <small class="text-muted">1 minutes ago</small>
                                    </div>
                                </div>
                                <div class="m-b">
                                    <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="...">
                                    </a>
                                    <div class="clear">
                                        <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                            <span class="arrow left pull-up"></span>
                                            <p class="m-b-none">Great!</p>
                                        </div>
                                        <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
                                    </div>
                                </div>
                                <!-- / chat list -->
                            </div>
                        </div>
                    </div>
                    <div class="wrapper m-t b-t b-light">
                        <form class="m-b-none">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Say something">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">SEND</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- / aside right -->

            <!-- footer -->
            <div class="app-footer wrapper b-t bg-light">
                <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span> &copy; 2015 Copyright.
            </div>
            <!-- / footer -->

        </div>
        <script>
                    var API_URL = '<?php echo $this->config->application->apiURL; ?>';
                    var BASE_URL = '<?php echo $this->config->application->baseURL; ?>';
                    var Amazon_link = '<?php echo $this->config->application->amazonlink; ?>';
        </script>

        <!-- JS -->
        <?php echo $this->tag->javascriptInclude('js/jquery/jquery.min.js'); ?>
        <!-- angular -->
        <?php echo $this->tag->javascriptInclude('js/angular/angular.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/angular-cookies.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/angular-animate.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/angular-ui-router.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/angular-translate.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/ngStorage.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/ui-load.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/ui-jq.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/ui-validate.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/ui-bootstrap-tpls.min.js'); ?>
        <!-- added -->
        <?php echo $this->tag->javascriptInclude('js/angular/angular-route.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/angular/angular-route.min.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/angular-sanitize.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/checklist-model.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/chosen/angular-chosen.min.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/angular-xeditable/dist/js/xeditable.min.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/angular-ui-select/dist/select.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/angularUtils-master/src/directives/pagination/dirPagination.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/angular-bootstrap-colorpicker-master/js/bootstrap-colorpicker-module.js'); ?>

        <!-- ng upload -->
        <?php echo $this->tag->javascriptInclude('js/angular/ng-file-upload/ng-file-upload.min.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/angular/ng-file-upload/ng-file-upload-shim.js'); ?>

        <!-- angular-timer -->
        <?php echo $this->tag->javascriptInclude('vendors/angular-timer/dist/angular-timer.js'); ?>
        <?php echo $this->tag->javascriptInclude('vendors/humanize-duration/humanize-duration.js'); ?>
        <?php echo $this->tag->javascriptInclude('vendors/momentjs/moment.js'); ?>

        <!-- APP -->
        <?php echo $this->tag->javascriptInclude('js/app.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/services.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/controllers.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/filters.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/directives.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/ckeditor/ckeditor.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/ckeditor/styles.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/ckeditor/adapters/jquery.js'); ?>

        <!-- SLIDER IMAGE -->
        <?php echo $this->tag->javascriptInclude('js/sliderimage/angular-file-upload-shim.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/angular-file-upload.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/upload.js'); ?>

        <!-- jquery fileuploader -->
        <?php echo $this->tag->javascriptInclude('js/sliderimage/vendor/jquery.ui.widget.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/load-image.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/canvas-to-blob.min.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.iframe-transport.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-process.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-image.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-audio.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-video.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-validate.js'); ?>

        <!-- tags -->
        <?php echo $this->tag->javascriptInclude('js/tokeninput/src/jquery.tokeninput.js'); ?> 
        <?php echo $this->tag->javascriptInclude('js/select2/dist/js/select2.min.js'); ?>

        <?php echo $this->tag->javascriptInclude('js/jquery/chosen/chosen.js'); ?>

        <?php echo $this->tag->javascriptInclude('js/jquery/chosen/chosen.jquery.js'); ?>
        <script type="text/javascript" src="/js/angular/angular-ui-sortable/sortable.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

        <!-- rainier global variable path windows -->

    </body>

</html>
