<?php
namespace Modules\Backend\Forms;

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;


class CreateuserForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $husername = new Hidden('huserName');
            $this->add($husername);
            $hemail = new Hidden('huserEmail');
            $this->add($hemail);            
        } 

        //User Account
        $name = new Text('username', array('class' => 'form-control', 'ng-model' => 'user.username', 'ng-pattern' => '/^[a-zA-Z0-9]{4,10}$/', 'required' => 'required'));
        $name->setLabel('Username');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The username is required'
                ))
            ));
        $this->add($name);

        //Email
        $email = new Text('emailaddress', array('class' => 'form-control', 'ng-model' => 'user.emailaddress', 'required'=>'required'));
        $email->setLabel('Email Address');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
                )),
            new Email(array(
                'message' => 'The Email Address is required'
                ))
            ));
        $this->add($email);

        if (!isset($options['edit']) && !$options['edit'] || !isset($options['editown']) && !$options['editown']) {
            //Password
            $password = new Password('password', array('class' => 'form-control' , 'ng-model' => 'user.password','required'=>'required'));
            $password->setLabel('Password');
            $password->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The password is required'
                    )), 
                new StringLength(array(
                  'max' => 20,
                  'min' => 8,
                  'messageMaximum' => 'Thats an exagerated password.',
                  'messageMinimum' => 'Password should be Minimum of 8 characters.'            
                  )),

                ));
            $this->add($password);        

            //Re Password
            $confirm_password = new Password('confirm_password', array('class' => 'form-control' , 'required' => 'required', 'ng-model' => 'user.confirm_password', 'ui-validate'=> ' \'$value == user.password\' ', 'ui-validate-watch' => " 'user.password' "));
            $confirm_password->setLabel('Re-Type Password');
            $confirm_password->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Retyping your password is required'
                    )),
                new Confirmation(array(
                 'message' => 'Password doesn\'t match confirmation',
                 'with' => 'password'
                 )) 
                ));
            $this->add($confirm_password);
        }else{
            //Password
            $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'Password'));
            $password->setLabel('Password');            
            $this->add($password);   

            //Re Password
            $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
            $repassword->setLabel('Re-Type Password'); 
            $this->add($repassword);     

            //Old password
            $oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
            $oldpassword->setLabel('Old Password');
            $oldpassword->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The old password is required'
                    ))
                ));
            $this->add($oldpassword);  

        }



        //User Profile
        //firstname
        $firstname = new Text('firstname', array('class' => 'form-control' , 'ng-model' => 'user.firstname', 'required' => 'required'));
        $firstname->setLabel('Firstname');
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'First Name is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Firstname should have at least 2 minimum characters'            
              ))
            ));
        $this->add($firstname);
        //lastname
        $lastname = new Text('lastname', array('class' => 'form-control' ,'ng-model' => 'user.lastname', 'required' => 'required'));
        $lastname->setLabel('Lastname');
        $lastname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Lastname is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Lastname should have at least 2 minimum characters'            
              ))            
            ));
        $this->add($lastname);

        //Birthday
        $birthday = new Text('birthday', array('class' => 'form-control' , 'required' => 'required', 'datepicker-popup' => '{[{format}]}', 'ng-model' => 'user.birthday', 'is-open' => 'opened', 'datepicker-options' => 'dateOptions', 'ng-required' => 'true' , 'close-text' => 'Close'));
        $birthday->setLabel('Birthday');
        $this->add($birthday);        
     

        // $attr = array(
        //     'name' => 'groupName',
        //     'selected' => 'selected'
        // );

        // $radio1 = new Radio("radio1", $attr);
        // $radio2 = new Radio("radio2", $attr);;
        // $this->add($radio1);
        // $this->add($radio2);


        //CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}