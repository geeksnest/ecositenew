
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
    <link rel="shortcut icon" type="image/ico" href="/images/template_images/favicon.png" />  

  <!-- Stylesheets -->
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/animate.css') }}
  {{ stylesheet_link('css/font-awesome.min.css') }}
  {{ stylesheet_link('css/simple-line-icons.css') }}
  {{ stylesheet_link('css/font.css') }}
  {{ stylesheet_link('css/app.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon 
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">-->
</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController">
  <div class="m-b-lg">
  <a href class="navbar-brand block m-t"><!--{{app.name}}--></a>
    <div class="wrapper text-center">
      <img src='/img/ecologo.png'>
    </div>
    <form name="form" class="form-validation" method="post" action="<?php echo $base_url.'/ecoadmin';?>">
      <div class="text-danger wrapper text-center" ng-show="authError">
          {{ content() }}
      </div>     
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          {{ text_field('username', 'size': "30", 'class': "form-control no-border", 'id': "inputUsername", 'placeholder': 'Username', 'ng-model': 'user.password', 'required':'required') }}
        </div>
        <div class="list-group-item">
          {{ password_field('password', 'size': "30", 'class': "form-control no-border", 'id': "inputPassword", 'placeholder': 'Password', 'ng-model': 'user.password', 'required': 'required') }}
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block btn-success" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a href="<?php echo $base_url.'/ecoadmin/forgotpassword';?>">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
</div>
<!-- JS -->
{{ javascript_include('js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('js/angular/angular.min.js') }}
{{ javascript_include('js/angular/angular-cookies.min.js') }}
{{ javascript_include('js/angular/angular-animate.min.js') }}
{{ javascript_include('js/angular/angular-ui-router.min.js') }}
{{ javascript_include('js/angular/angular-translate.js') }}
{{ javascript_include('js/angular/ngStorage.min.js') }}
{{ javascript_include('js/angular/ui-load.js') }}
{{ javascript_include('js/angular/ui-jq.js') }}
{{ javascript_include('js/angular/ui-validate.js') }}
{{ javascript_include('js/angular/checklist-model.js') }}
{{ javascript_include('js/angular/ui-bootstrap-tpls.min.js') }}

<!-- APP -->
  <script src="js/app.js"></script>
  <script src="js/services.js"></script>
  <script src="js/controllers.js"></script>
  <script src="js/filters.js"></script>
  <script src="js/directives.js"></script>
</body>
</html>