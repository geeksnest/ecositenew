{{ content()}}
<script language="javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

function spacer(item){
  var tmp = "";
  var item_length = item.value.length;
  var item_length_minus_1 = item.value.length - 1;
    for (index = 0; index < item_length; index++){
        if (item.value.charAt(index) != ' '){
            tmp += item.value.charAt(index);
        }else{
            if (tmp.length > 0){
                if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1){
                tmp += item.value.charAt(index);
                } 
            }  
        } 
    }
  item.value = tmp;
}
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Offline Registration</h1>
    <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" ng-submit="addToMember(bnb)" name="formBNB">
    <fieldset ng-disabled="isSaving">
        <div class="bg-light lter b-b wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="wrapper-md">
                <div class="wrapper-md">
                    <div class="panel panel-default">

                        <div class="panel-heading font-bold">
                            Note: Field mark with <font style="color:red;font-size:14px">*</font> is required.
                            <br><br>
                            BNB Account Information 
                        </div>

                        <div class="panel-body">
                            <input type="hidden" ng-init='pagedata = data'>

                            <label class="col-sm-2 control-label"><label for="email">Email Address <font style="color:red;font-size:24px">*</font></label></label>
                            <div class="col-sm-10">
                                <span class="label bg-danger" ng-if="bnb.emailtaken == true">Email Address already taken.<br></span>
                                <input type="text" id="email" name="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.email" required="required" style="width:500px" placeholder="Example: myemail@domain.ext">
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <label class="col-sm-2 control-label"><label for="NMSemail">Firstname</label> </label>
                            <div class="col-sm-10">
                                <input type="text" id="fname" name="fname" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.fname" style="width:500px" onChange="spacer(email);return false">                    
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <label class="col-sm-2 control-label"><label for="NMSemail">Lastname</label> </label>
                            <div class="col-sm-10">
                                <input type="text" id="lname" name="lname" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.lname" style="width:500px" onChange="spacer(email);return false">                              
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Birthday</label>
                                <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                                <div class="col-lg-10">
                                    Month:
                                    <select ng-model="bnb.bmonth" class="input-sm form-control w-sm inline v-middle" style="width:105px">
                                        <?php foreach ($formonths as $index => $formonth) { 
                                        echo "<option value='".$index."'>".$formonth."</option>";
                                        }?>
                                    </select>
                                    Day:
                                    <select ng-model="bnb.bday" class="input-sm form-control w-sm inline v-middle" style="width:80px">
                                        <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                                        echo "<option value='".$days."'>".$days."</option>";
                                        } ?>
                                    </select>
                                    Year:
                                    <select ng-model="bnb.byear" class="input-sm form-control w-sm inline v-middle" style="width:80px">
                                        <?php for ($year=1900; $year < 2015; $year++) { 
                                        echo "<option value='".$year."'>".$year."</option>";
                                        }?>
                                    </select>
                                </div>        
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Gender</label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label class="i-checks" style="padding-right:15px">
                                            <input type="radio" name="gender" value="Male" ng-model="bnb.gender"><i></i>Male
                                        </label>
                                        <label class="i-checks" style="padding-right:15px">
                                            <input type="radio" name="gender" value="Female" ng-model="bnb.gender"><i></i>Female
                                        </label>
                                        <label class="i-checks" style="padding-right:15px">
                                            <input type="radio" name="gender" value="Other" ng-model="bnb.gender"><i></i>Other
                                        </label>
                                    </div>
                                    
                                </div>
                            </div> 

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <label class="col-sm-2 control-label"><label for="zip">Zipcode</label> </label>
                            <div class="col-sm-10">
                                <input type="text" id="zip" name="zip" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.zip" style="width:300px" ng-maxlength="20" maxlength="20">
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Location</label>
                                <div class="col-lg-10">
                                    <select  id="location" name="location" class="location form-control m-b" ng-model="bnb.location" ng-init="bnb.location = countries[229]" ng-options="bn.name for bn in countries" style="width:300px">
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="loca" name="loca" ng-model="bnb.loca">
                                                        
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">How did you learn about ECO?</label>
                                <div class="col-lg-10">
                                    <select class="form-control" id="howdidyoulearn" name="howdidyoulearn" ng-model="bnb.howdidyoulearn" style="width:300px">
                                        <option value=""></option>                                        
                                        <option value="ECO event">ECO event</option>
                                        <option value="ECO Program Graduates">ECO Program Graduates</option>
                                        <option value="Body &amp; Brain or Dahn Yoga Centers">Body &amp; Brain or Dahn Yoga Centers</option>
                                        <option value="Invitation Email">Invitation Email</option>                                        
                                    </select>
                                </div>
                            </div>
                            
                            <div class="line line-dashed b-b line-lg pull-in" ng-show="bnb.howdidyoulearn == 'Body &amp; Brain or Dahn Yoga Centers'"></div>

                            <div class="form-group" ng-show="bnb.howdidyoulearn == 'Body &amp; Brain or Dahn Yoga Centers'">
                                <label class="col-lg-2 control-label">Center Name</label>
                                <div class="col-lg-10">
                                    <select class="form-control" id="centersname" name="centersname" ng-model="bnb.centersname" style="width:300px">
                                        <option value=""></option>
                                        <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                    </select>
                                </div>
                            </div>
                            
<!--                             <div class="line line-dashed b-b line-lg pull-in"></div>
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label">I would like my friend to become an Earth Citizen as well.</label>
                                <div class="col-lg-10">
                                    <input type="text" id="refer" name="refer" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.refer" style="width:500px" placeholder="Enter friend's email address">
                                    <span style="font-size: 10px;">Thank you for introducing ECO to your friend. ECO will send them an invitation email.</span>
                                </div>
                            </div> -->

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Payment Type </label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="paymode" ng-model="bnb.paymode" value="None" ng-click="tpaynone()" ng-checked="true"><i></i>None
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="paymode" ng-model="bnb.paymode" value="Credit" ng-click="tpaycreditcash()" ><i></i>Credit Card
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="paymode" ng-model="bnb.paymode" value="Cash" ng-click="tpaycreditcash()" ><i></i>Cash
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="paymode" ng-model="bnb.paymode" value="Check" ng-click="tpaycheck()" ><i></i>Check
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="paymode" ng-model="bnb.paymode" value="ECO Program" ng-click="ecoprogram()" ><i></i>ECO Program
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div class="form-group" ng-if="tycreditcashcheck">
                                <label class="col-sm-2 control-label"><label for="amt">Amount <font style="color:red;font-size:24px">*</font></label> </label>
                                <div class="col-sm-10">
                                    <input type="text" id="amt" name="amt" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.amt" onkeypress="return isNumberKey(event)" style="width:300px" ng-maxlength="15" maxlength="15" placeholder="number only" required>
                                </div>
                            </div>

                            <div class="form-group" ng-if="tycheck">

                                <div class="line line-dashed b-b line-lg pull-in"></div>

                                <label class="col-sm-2 control-label">Check Number <font style="color:red;font-size:24px">*</font></label>
                                <div class="col-sm-10">
                                    <table class="table-responsive bnb">
                                        <tr>
                                            <td>
                                                <input type="text" name="rn" class="form-control inline" ng-model="bnb.rn" style="width:180px" onkeypress="return isNumberKey(event)" placeholder="number only" required>
                                            </td>
                                            <td>&nbsp; - &nbsp;</td>
                                            <td>
                                                <input type="text" name="an" class="form-control inline" ng-model="bnb.an" style="width:180px" onkeypress="return isNumberKey(event)" placeholder="number only" required>
                                            </td>
                                            <td>&nbsp; - &nbsp;</td>
                                            <td>
                                                <input type="text" name="cn" class="form-control inline" ng-model="bnb.cn" style="width:180px" onkeypress="return isNumberKey(event)" placeholder="number only" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Routing Number</td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>Account Number</td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td>Check Number</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            
                            <div class="panel-body">
                                <footer class="panel-footer text-right bg-light lter">
                                    <button type="submit" class="btn btn-success" ng-disabled="formBNB.$invalid">Submit</button>
                                     <!-- onclick="ValidateEmail(document.formBNB.email)" -->
                                    <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
                                </footer>
                            </div>

                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
</form>                    