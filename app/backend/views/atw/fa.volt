{{ content() }}

<script type="text/ng-template" id="deletepage.html">
  <div ng-include="'/tpl/deletePageModal.html'"></div>
</script>
<script type="text/ng-template" id="review.html">
  <div ng-include="'/tpl/faReview.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Eco Friends and Allies List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage List
    </div>
    <div class="row wrapper">
      <div class="col-sm-6 m-b-xs" ng-hide="advancesearch || catsearch">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" ng-click="search(searchtext)" type="button">Go!</button>
           <button class="btn btn-sm btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
       <div class="col-sm-12 m-b-xs" ng-show="advancesearch">
       <div class="col-sm-3">
           From 
         <!-- <input type="date" id="from" ng-model="dfrom"> -->
         <div class="input-group">
           <input type="text" class="form-control" datepicker-popup="{[{format1}]}" 
           ng-model="dfrom" is-open="opened1" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="Start Date" />
           <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="open($event,'opened1')"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </div>
       </div> 
       <div class="col-sm-3">
       To
         <div class="input-group">
         <!--  <span class="input-group-btn"> -->
            <input type="text" class="form-control" datepicker-popup="{[{format2}]}" 
            ng-model="dto" is-open="opened2" datepicker-options="dateOptions" ng-required="true" close-text="Close" placeholder="End Date" />
            <span class="input-group-btn">
             <button type="button" class="btn btn-default" ng-click="open($event,'opened2')"><i class="glyphicon glyphicon-calendar"></i></button>
           </span>
          <!-- </span> -->
        </div>
       </div>
         <div class="col-sm-4">
         <br>
         <div class="input-group">
         <button type="button" class="btn btn-default" ng-click="filterdate(dfrom,dto)"><i class="glyphicon glyphicon-search"></i> Filter By Date</button>
         <button class="btn btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
         </div>
         </div>
       </div>
       <div class="col-sm-6 m-b-xs" ng-show="catsearch">
         <div class="col-sm-6">
           <select class="form-control" ng-model="searchcat">
            <option value="Profit"> Profit </option>
            <option value="NonProfit"> NonProfit </option>
          </select> 
         </div>
        <div class="col-sm-6">
         <button type="button" class="btn btn-default" ng-click="search(searchcat);searchtext=''"><i class="glyphicon glyphicon-search"></i> Filter By Category</button>
         <button class="btn btn-default" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
         </div>
       </div>

    <!--   <div class="col-sm-2"></div> -->
     <!--  <div class="col-sm-4">        
        <div class="input-group">
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,searchcat)" style="margin:0px;"></pagination>
        </div> 
      </div> -->
      <div class="checkbox col-sm-12">
        <label class="i-checks">
          <input type="checkbox" name="advancesearch" ng-model="advancesearch" ng-change="catsearch=false"><i></i> Filter By Date
        </label> &nbsp;&nbsp;&nbsp;
        <label class="i-checks">
          <input type="checkbox" name="catsearch" ng-model="catsearch" ng-change="advancesearch=false"><i></i> Filter By Category
        </label>
      </div>

    </div>
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'organization','DESC')"> Organization
                  <span ng-show="sortIn == 'orgname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'organization','ASC')"> Organization
                  <span ng-show="sortIn == 'orgname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
            <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'add1','DESC')"> Address
                  <span ng-show="sortIn == 'add1' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'add1','ASC')"> Address
                  <span ng-show="sortIn == 'add1' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">
            <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'category','DESC')"> Organization Category
                  <span ng-show="sortIn == 'category' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'category','ASC')"> Organization Category
                  <span ng-show="sortIn == 'category' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

             <th style="width:15%">
            <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'contactperson','DESC')"> Contact Person
                  <span ng-show="sortIn == 'contactperson' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'contactperson','ASC')"> Contact Person
                  <span ng-show="sortIn == 'contactperson' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'phone','DESC')"> Contact
                  <span ng-show="sortIn == 'phone' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'phone','ASC')"> Contact
                  <span ng-show="sortIn == 'phone' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>
            <th style="width:15%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'email','DESC')"> Contact Email Address
                  <span ng-show="sortIn == 'email' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'email','ASC')"> Contact Email Address
                  <span ng-show="sortIn == 'email' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'date','DESC')"> Date Registered
                  <span ng-show="sortIn == 'date' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'date','ASC')"> Date Registered
                  <span ng-show="sortIn == 'date' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td class="break">
             <span class="break"> {[{ mem.organization }]} </span> 
            </td>
            <td class="break"> <span class="break"> {[{ mem.add1 }]}</span>  </td>
            <td class="break"> <span class="break"> {[{ mem.category }]} </span>  </td>
            <td class="break"> <span class="break"> {[{ mem.contactperson }]} </span>  </td>
            <td class="break"> <span class="break"> {[{ mem.phone }]} </span>  </td>
            <td class="break"> <span class="break"> {[{ mem.email }]}</span> </td>
            <td class="break"> <span class="break">  {[{ mem.date }]}</span>  </td>
            <td>
              <a href="" ng-click="review(mem.id,bigCurrentPage,searchtext,dfrom,dto)"> <span class="label bg-info"> View </span>
              </a> &nbsp;
              <a href="" ng-click="deletepage(mem.id,bigCurrentPage,searchtext,dfrom,dto)"> <span class="label bg-danger">Delete</span>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
    <br>
      <div class="row">
        <div class="col-sm-8">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems}]} items</small> 
        </div>
        <div class="col-sm-4">        
          <div class="input-group">
            <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext,searchcat)" style="margin:0px;"></pagination>
          </div> 
        </div>
      </div>   
    </footer>
</div>
</div>
