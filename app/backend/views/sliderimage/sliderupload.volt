{{ content() }}
<script type="text/ng-template" id="editMemberModal.html">
<form class="bs-example form-horizontal" name="form" novalidate>
    <div class="modal-header">
        <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>
    <div class="modal-body" ng-show="process==true">
        <p ng-show="success==false">Updating user profile record.</p>
        <p ng-show="success==true">Success!</p>
    </div>
    <div class="modal-body" ng-show="process==false">
    ARE YOU SURE YOU WANT TO DELETE?
    </div>
    <div class="modal-footer" ng-hide="process==true">                  
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
</form>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Upload Slider Image</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md ">
      <div class="row">
  		  	<div class="panel panel-default">
  		    <div class="panel-heading font-bold"> Upload New Photo</div>

  		    <div class="panel-body">
  		    	<div class="col-lg-12">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          <div class="padd">
                              <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Add files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input  id="digitalAssets" type="file" name="files[]" multiple>
                              </span>
                              <!-- The global progress bar -->
                              <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                              </div>
                              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data"> 
                             <div class="gallery digital-assets-gallery"> 

                                <div class="clearfix"></div>
                            </div>
                            </form>

      <div class="col-sm-6" ng-repeat="data in data">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="panel-heading">
              <ul class="nav nav-pills pull-right">
                  <li>
                    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="huhuClick(dlt)" name="form" enctype="multipart/form-data">
                        <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                        <div class="buttonPanel{[{ data.imgid }]}">
                         <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                        </div>
                     </form>
                  </li>
              </ul>
              <h1 class="m-n font-thin h3 text-black">{[{ data.imgtitle }]}</h1>
              <small class="text-muted">{[{ data.description }]}</small>
            </div>
            <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="imgInfo(info)" name="form" enctype="multipart/form-data">
             <fieldset ng-disabled="isSaving">
             <img  style="width:100%;height:250px;position:relative; " src="<?php echo $api_url; ?>/images/sliderimages/{[{ data.imgpath }]}">
              <!-- Input for Image Title and Description ---------------------->
              <input type="hidden" id="" name="imgid" ng-init="info.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="info.id">
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Title:</label>
                <div class="col-sm-10">
                  <input type="text" onclick="this.focus();this.select()" id="" name="imgtitle" value="{[{ data.imgtitle }]}" class="form-control" ng-model="info.title" ng-init="info.title=data.imgtitle"
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Description:</label>
                <div class="col-sm-10">
                <input type="text" id="" name="imgdescription" value="" class="form-control" ng-init="info.description=data.description"  ng-model="info.description">
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="buttonPanel{[{ data.imgid }]}">
                <button class="btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus" style='width=100%;'></i>Update Information</button>
                <span ng-show="saveboxmessage == data.imgid"> <i class="fa fa-check"></i> Saved!</span>
              </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
                          </div>
                          <div class="widget-foot">
                          </div>
                        </div>
                      </div>  
                      </div>       
                  </div>
  		  	</div>
      </div>
</div>
</div>
 <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Uploaded Photo's</h1>
        <a id="top"></a>
</div>
<div class="wrapper-md ">
  <div class="row">
    
  </div>
</div>


<script type="text/javascript">
	                 /* File Uploader */
var BASE_URL = 'http://ecoapi/';
/* File Uploader */
$(function () {
    'use strict';
    var newArray = new Array();
    $('#digitalAssets').fileupload({
        url: BASE_URL + 'server/php/index.php',
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        limitMultiFileUploads: 4,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $.get( BASE_URL + 'utility/ajaxfileuploader/'+ file.name + '/Sample description' ).done(function( data ) {
                  $('.digital-assets-gallery').prepend(data);
                  console.log(data);
                });
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>











