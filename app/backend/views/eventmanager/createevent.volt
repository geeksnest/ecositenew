{{ content() }}

<script type="text/ng-template" id="helperModal.html">
  <div ng-include="'/tpl/helperModal.html'"></div>
</script>
<script type="text/ng-template" id="deletenewsimgModal.html">
  <div ng-include="'/tpl/deletenewsimgModal.html'"></div>
</script>
<script type="text/ng-template" id="eventsBanner.html">
 <div ng-include="'/tpl/eventsBanner.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Event</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formevents" id="formevents" ng-submit="publishEvent(events,prices,cnames)">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-7">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Event Information
              <span class="pull-right">
                <a ng-click="infoimages('event_title')"> &nbsp;<i class="icon-question"></i></a>
              </span>
            </div>
            <div class="panel-body">
              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event Title</label>
                </label>
                <div class="col-sm-12">
                  <!-- ngIf: user.usernametaken == true -->
                  <input type="text" id="title" name="title" class="form-control" ng-model="events.title" required="required" maxlength="255" ng-blur="onnewstitle(events.title)">
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event URL</label>
                </label>
                <div class="col-sm-12">
                  <em class="text-muted">Don't use space for this field.</em>
                  <div class="input-group"> 
                    <span class="input-group-btn">
                      <lable class="form-control"><?php echo $this->config->application->baseURL; ?>/</lable>
                    </span>   
                    <input type="text" id="url" name="url" class="form-control" ng-model="events.url" required="required" maxlength="255" ng-change="events.url = events.url.split(' ').join('')">
                  </div>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="row">
                <label class="col-sm-12 control-label">
                  <label for="title">Event Short Description</label>
                </label>
                <div class="col-sm-12">
                  <em class="text-muted">(maximum 500 characters only)</em>
                  <textarea ng-model="events.shortDesc" maxlength="500" required="required" class="form-control" style="resize:vertical;"></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="row form-group">
                <div class="col-sm-12">
                  <label class="control-label">Content</label>
                </div>
                <div class="col-sm-12">
                  <textarea class="ck-editor" ng-model="events.content" required="required"></textarea>
                  <br>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>
          </div>


        <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Payment Reciept Email Info              
              <span class="pull-right">
                <a ng-click=""><i class="icon-question"></i></a>
              </span>
            </div>
            <div class="panel-body">

              <div class="row form-group">
                <div class="col-sm-12">
                  <label class="control-label">Subject</label>
                </div>
                <div class="col-sm-12">                  
                  <input type="text" id="recieptsubject" name="recieptsubject" class="form-control" ng-model="events.recieptSubject" required="required" maxlength="255" >
                  <br>
                </div>
                <div class="col-sm-12">
                  <label class="control-label">Payment Reciept Content</label>
                </div>
                <div class="col-sm-12">
                  <textarea class="ck-editor" ng-init="events.recieptContent = '<?php echo $defaultreceipt;?>'" ng-model="events.recieptContent" required="required"></textarea>
                  <br>
                </div>
              </div>             

            </div>
          </div>
        </div>

        <div class="col-sm-5">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Banner
            </div>
            <div class="panel-body">
              <div class="input-group m-b">
                <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="eventsBanner('banner')"><i class="icon-folder"></i> Media Library</a>
                </span>
              </div>

              <div>
                <div class="line line-dashed b-b line-lg"></div>
                <img ng-show="events.banner" err-src="" ng-src="<?php echo $this->config->application->amazonlink; ?>/uploads/eventsbanner/{[{ events.banner }]}" style="width: 100%">
                <input type="hidden" id="banner" name="banner" class="form-control" ng-model="events.banner" placeholder="Paste thumbnail link here..." onClick="this.setSelectionRange(0, this.value.length)" ng-change="cutlink(events.banner)" required="required">
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Donation Amount
              <!-- <button type="button" ng-click="chkprices(prices)">chk prices</button> -->
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-7">
                  <div class="input-group">
                    <input type="text" id="amounts" name="amounts" class="form-control" ng-model="price.amounts" name="amounts" placeholder="Amount" only-digits>
                    <span class="input-group-btn">
                      <a class="btn btn-success" ng-show="showUpdate == false" ng-click="addprice(price)" ng-disabled="price.amounts == null"><i class="fa fa-plus"></i></a>
                      <a class="btn btn-success" ng-show="showUpdate == true" ng-click="updatePrice(price)" ng-disabled="price.amounts == null"><i class="fa fa-exchange"></i></a>
                      <a class="btn btn-default" ng-show="showUpdate == true" ng-click="priceCancel()"><i class="glyphicon glyphicon-ban-circle"></i></a>
                    </span>
                  </div>
                </div>
              </div>

              <ul class="list-group no-radius">
                <li ng-repeat="p in prices track by $index"class="list-group-item service-price" ng-hide="currentEditPrice == $index">
                  <span><i class="fa fa-dollar"></i> {[{p.amounts}]}</span>
                  <span class="service-price">
                    <span class="price-control-action"><a href="" ng-click="price.amounts=p.amounts;showUpdate = true; editPrice($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="delete($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
                </li>
              </ul>

              <div class="row">
                <div class="col-sm-7">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.otheramount">
                      <i></i>
                      Display Other Amount?
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <div class="row">
                <div class="col-sm-6">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.paymentInfo">
                      <i></i>
                      Display Payment Information?
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <div class="row">
                <div class="col-sm-6">
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" value="" ng-model="events.billingInfo">
                      <i></i>
                      Display Billing Information?
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div> -->


          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              How did you learn about?
            </div>
            <div class="panel-body">
              <em class="text-muted">(e.g. How did you learn about the  Earth Citizens Walk in Houston?)</em>
              <input type="text" id="hdyla" name="hdyla" class="form-control" ng-model="events.hdyla" name="hdyla" required="required" >
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Center Names
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-7">
                  <div class="input-group">
                    <input type="text" id="centernames" name="centernames" class="form-control" ng-model="cname.centernames" name="centernames">
                    <span class="input-group-btn">
                      <a class="btn btn-success" ng-show="cnameUpdate == false"ng-click="addCname(cname)" ng-disabled="cname.centernames == null"><i class="fa fa-plus"></i></a>
                      <a class="btn btn-success" ng-show="cnameUpdate == true" ng-click="updateCname(cname)" ng-disabled="cname.centernames == null"><i class="fa fa-exchange"></i></a>
                      <a class="btn btn-default" ng-show="cnameUpdate == true" ng-click="cancelCname()"><i class="glyphicon glyphicon-ban-circle"></i></a>
                    </span>
                  </div>
                </div>
              </div>

              <ul class="list-group no-radius">
                <li ng-repeat="cnames in cnames track by $index"class="list-group-item" ng-hide="currentEditCname == $index">
                  <span>{[{cnames.centernames}]}</span>
                  <span class="service-price">
                    <span class="price-control-action"><a href="" ng-click="cname.centernames=cnames.centernames;cnameUpdate = true; editCname($index)"><i class="fa fa-pencil-square"></i></a></span>
                    <span class="price-control-action"><a href="" ng-click="deleteCname($index)"><i class="fa fa-times-circle"></i></a></span>
                  </span>
                </li>
              </ul>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
              <footer class="text-right bg-light lter">
                <button type="submit" id="submit" name="submit" ng-disabled="formevents.$invalid || cnames.length == 0 || prices.length == 0" class="btn btn-success"><i class="fa fa-upload"></i> Publish Event</button>
              </footer>
            </div>
          </div>


        </div>



      </div>

    </div>
  </fieldset>
</form>




