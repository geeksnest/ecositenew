{{ content() }}
<script type="text/ng-template" id="deleteuser.html">
  <div ng-include="'/tpl/deleteUserModal.html'"></div>
</script>
<script type="text/ng-template" id="updateuser.html">
  <div ng-include="'/tpl/updateUserModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-11 ">
          Manage User
        </div>
        <div class="col-sm-1">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default float-right" type="button" ng-click="refresh()"><i class="icon-refresh"></i></button>
          </span>
        </div>
      </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
         <div class="input-group" ng-hide="advancesearch">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>

        <div ng-show="advancesearch">          
          <div class="row">
            <div class="col-sm-11">
              <select class="input-sm form-control" name="searchtext" ng-model="searchtext">
                <option value="1">Active</option>
                <option value="0">Disabled</option>
              </select>
            </div>
            <div class="col-sm-1">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <div class="checkbox">
          <label class="i-checks">
            <input type="checkbox" name="advancesearch" ng-model="advancesearch"><i></i> Search by Status
          </label>
        </div>

      </div>
    </div>
    <div class="table-responsive">
      <!-- <input type="hidden" ng-init='pagedata = {{ data }}'> -->
      <input type="hidden" ng-init="admin = '<?php echo $listnum; ?>'" ng-model="admin">
      <table class="table table-striped b-t b-light">
        <thead class="manage">
          <tr>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'firstname','DESC')"> Name
                  <span ng-show="sortIn == 'firstname' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'firstname','ASC')"> Name
                  <span ng-show="sortIn == 'firstname' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'email','DESC')"> Email
                  <span ng-show="sortIn == 'email' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'email','ASC')"> Email
                  <span ng-show="sortIn == 'email' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'username','DESC')"> Username
                  <span ng-show="sortIn == 'username' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'username','ASC')"> Username
                  <span ng-show="sortIn == 'username' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:20%">
              <span ng-show="sortBy == 'ASC'">
                <a href="" ng-click="sortType(searchtext,'status','DESC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'ASC'" class="fa fa-caret-down"></span>
                </a>
              </span>
              <span ng-show="sortBy == 'DESC'">
                <a href="" ng-click="sortType(searchtext,'status','ASC')"> Status
                  <span ng-show="sortIn == 'status' && sortBy == 'DESC'" class="fa fa-caret-up"></span>
                </a>
              </span>
            </th>

            <th style="width:10%"></th>
            <th style="width:20%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-hide="user.userid == '<?php echo $sessionUserID; ?>'" ng-repeat="user in data.data">
            <td>{[{ user.firstname }]} {[{ user.lastname }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td>
              <span class="label bg-success" ng-show="{[{ user.status==1 }]}">Active</span>
              <span class="label bg-label bg-light" ng-show="{[{ user.status==0 }]}">Disabled</span>
            </td>
            <td>
              <div class="checkstatuscontent float-left">
                <label class="i-switch bg-success m-t-xs m-r" ng-show="{[{ user.status==1 }]}">
                  <input type="checkbox" checked="" ng-click="setstatus(user.userid,user.userLevel,user.status,bigCurrentPage)">
                  <i></i>
                </label> 
              </div>
              <div class="checkstatuscontent float-left">
              <label class="i-switch bg-danger m-t-xs m-r" ng-show="{[{ user.status==0 }]}">
                  <input type="checkbox" ng-click="setstatus(user.userid,user.userLevel,user.status,bigCurrentPage)">
                  <i></i>
                </label>
              </div>
            </td>
            <td>
              <div class="row">
                <div class="col-xs-2">
                  <a href="" ng-click="updateuser(user.userid)" class="manage-action" title="Edit"><i class="fa fa-edit"></i></a>                
                </div>
                <div class="col-xs-2">
                  <a href="" ng-click="deleteuser(user.userid)" ng-hide="{[{ user.userLevel==1 }]}" class="manage-action" title="Delete"><i class="fa fa-trash-o"></i></a>                
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{bigTotalItems - admin}]} items</small>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">                    
          <div class="input-group">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage,searchtext)" style="margin:0px;"></pagination>
          </div>          
        </div>
      </div>
          
    </footer>
     </div>
  </div>
</div>