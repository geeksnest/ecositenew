<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Create PeaceMap</h1>
    <a id="top"></a>
</div>
<div class="wrapper-md">
    <div class="panel b-a" ng-controller="MapCtrl">
        <div class="panel-heading"><h4 class="m-t-none">Click to add a marker!</h4></div>
        <div id="map_canvas" style="min-height:400px" ui-map="myMap"
             ui-event="{'map-click': 'addMarker($event, $params)', 'map-zoom_changed': 'setZoomMessage(myMap.getZoom())' }"
             ui-options="mapOptions">
        </div>
        <div class="panel-body">

            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

            <p>{[{zoomMessage}]}</p>
            <!--            <ul class="list-unstyled list-inline">
                            <li ng-repeat="marker in myMarkers">
            
                                <a class="btn btn-default m-b-sm" ">
                                    Pan to Marker {[{$index}]}
                                </a>
                            </li>
                        </ul>-->

            <div ng-repeat="marker in myMarkers" class="label bg-dark pull-left" style="margin-left: 10px;"><a href ng-click="myMap.panTo(marker.getPosition())" >Pan to Marker <span ng-show="marker.get('labelContent')">{[{ marker.get('labelContent'); }]}</span><span ng-hide="marker.get('labelContent');">{[{$index}]}</span></a> <a href="" ng-click="deleteMark($index)"><i class="fa fa-times text-danger text"></i></a></div> 

            <!-- this is the confusing part. we have to point the map marker directive
                at an existing google.maps.Marker object, so it can hook up events -->
            <div ng-repeat="marker in myMarkers" ui-map-marker="myMarkers[$index]"
                 ui-event="{'map-click': 'openMarkerInfo(marker)'}">
            </div>
            <br/><br/>
            <form role="form" name="form1" ng-submit="saveMap(map)" class="form-validation ng-pristine ng-invalid ng-invalid-required" >
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Enter Title" ng-model="map.title" required>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" placeholder="Description" ng-model="map.description" required>
                </div>
                <button type="submit" class="btn btn-sm btn-primary">Save</button>
            </form> 

            <div ui-map-info-window="myInfoWindow">
                <div class="m-b-sm">Marker </div>
                <div class="m-b-sm">
                    <div class="pull-left m-t-xs">Title: </div>
                    <input ng-model="currentMarkerTitle" class="form-control input-sm w-sm m-l-lg">
                </div>
                <a class="btn btn-default btn-sm m-l-lg m-b-sm" ng-click="setMarkerPosition(currentMarker, currentMarkerTitle)"> <span class="badge bg-success" ng-show="saveMarker"><i class="icon-check" ></i></span> Save</a>
            </div>
        </div>
    </div>
</div>