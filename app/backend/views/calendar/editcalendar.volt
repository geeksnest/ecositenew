{{ content() }}
<script type="text/ng-template" id="editMemberModal.html">

<form class="bs-example form-horizontal" name="form" novalidate>

    <div class="modal-header">
        <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>

    <div class="modal-body" ng-show="process==true">
        <p ng-show="success==false">Updating user profile record.</p>
        <p ng-show="success==true">Success!</p>
    </div>

    <div class="modal-body" ng-show="process==false">
      ARE YOU SURE YOU WANT TO DELETE?
    </div>

    <div class="modal-footer" ng-hide="process==true">                  
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>

</form>

</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Calendar of Activity</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" ng-submit="updatepage(act)" name="formcalen">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="row">
      <div class="col-sm-8">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Note: Field mark with <font style="color:red;font-size:14px">*</font> is required.
            <br><br>
            Activity Information
          </div>
          <div class="panel-body">

            <!-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
            <input type="hidden" ng-model='act.actid'>

              <label class="col-sm-2 control-label"><label for="title">Title  <font style="color:red;font-size:24px">*</font></label> </label>
              <div class="col-sm-10">
                <!-- ngIf: user.usernametaken == true -->
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.title" required="required">
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <label class="col-sm-2 control-label"><label for="title">Location</label> </label>
              <div class="col-sm-10">
                <!-- ngIf: user.usernametaken == true -->
                <input type="text" id="loc" name="loc" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.loc">
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Date of Activity </label>
            <div class="col-sm-10">
              <div class="input-group w-md">&nbsp;<span class="input-group-btn">&nbsp;</span></div>
            </div>        
          </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>
            
        <input type="hidden" ng-model='act.dfrom2'>
        <div class="form-group">
            <label class="col-sm-2 control-label" style="text-align:right">From <font style="color:red;font-size:24px">*</font></label>
            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
              <div class="input-group w-md">
                <span class="input-group-btn">
                  <input id="dfrom" name="dfrom" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="act.dfrom" is-open="opened" min-date="minDate" max-date="'2016-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>        
          </div>
          
          <div class="line line-dashed b-b line-lg pull-in"></div>

          <input type="hidden" ng-model='act.dto2'>
          <div class="form-group">
            <label class="col-sm-2 control-label" style="text-align:right">To</label>
            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
              <div class="input-group w-md">
                <span class="input-group-btn">
                  <input id="dto" name="dto" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="act.dto" is-open="opened" min-date="act.dfrom" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" type="text"  disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>        
          </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <input type="hidden" ng-model='act.mytime2'>
              <div class="form-group">
            <label class="col-sm-2 control-label">Time of Activity</label>
            <div class="col-lg-10">
              <div ng-controller="TimepickerDemoCtrl" class="m-b-lg">
                <timepicker ng-model="act.mytime" hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>
                <br>
              </div>
            </div>
          </div>

            <div class="line line-dashed b-b line-lg pull-in"></div> 

            <label class="col-sm-2 control-label"><label for="title">Short Description <font style="color:red;font-size:24px">*</font></label> </label>
              <div class="col-sm-10">
                <!-- ngIf: user.usernametaken == true -->
                <input type="text" id="shortdesc" name="shortdesc" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model="act.shortdesc" required="required">
              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Full Description <font style="color:red;font-size:24px">*</font></label>
                <div class="col-sm-10">

               <textarea class="ck-editor" ng-model="act.body" required="required"></textarea>
              </div>
            </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

           </div>       
        </div>
      </div>

      <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Activity Cover Photo
          </div>
          <div class="panel-body">
             <img ng-src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ act.banner }]}" style="width: 100%" ng-show="act.banner">
              <div class="line line-dashed b-b line-lg pull-in"></div>
             <input type="text" id="banner" name="banner" class="form-control ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern" ng-model= "act.banner" ng-change = "banners(act.banner)" placeholder="Paste cover photo link here...">
              <div class="line line-dashed b-b line-lg pull-in"></div>

          </div>
        </div>
      </div>
      
    </div>

    <div class="row" >
       <div class="panel-body">
           <footer class="panel-footer text-right bg-light lter">
                <button type="submit" class="btn btn-success" ng-disabled="">Submit</button>
                <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
           </footer>
       </div>

 </form>

      <div class="wrapper-md ">
      <div class="row">
          <div class="panel panel-default">
          <div class="panel-heading font-bold"> Upload Cover Photo</div>

          <div class="panel-body">
            <div class="col-lg-12">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          <div class="padd">
                              <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Add Photo...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input  id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                              </span>
                              <!-- The global progress bar -->

                              <div class="line line-dashed b-b line-lg pull-in"></div>

                              <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                              </div>
                              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data"> 
                             <div class="gallery digital-assets-gallery"> 

                                <div class="clearfix"></div>
                            </div>
                            </form>

      <div class="col-sm-4" ng-repeat="data in data">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="panel-heading">
              <ul class="nav nav-pills pull-right">
                  <li>
                    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="huhuClick(dlt)" name="form" enctype="multipart/form-data">
                        <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.id"class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
                        <div class="buttonPanel{[{ data.id }]}">
                         <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                        </div>
                     </form>
                  </li>
              </ul>
              <br>
              <br>
              <input type="text" class="form-control"  ng-model="data.imgpath">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <img  style="width:350px;height:250px;position:relative; " ng-src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ data.imgpath }]}">
            </div>
           
          
          
          </div>
        </div>
      </div>
                          </div>
                          <div class="widget-foot">
                          </div>
                        </div>
                      </div>  
                      </div>       
                  </div>
          </div>
      </div>
</div>
</div>

    </div>

  </div>
  </fieldset>
