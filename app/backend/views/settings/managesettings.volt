{{ content() }}

<script type="text/javascript">  
  function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Settings</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading"><span class="h4">Maintenance</span></div>
        <div class="panel-body">

          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance">
            <div ng-if="value1 == 0" >

              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">In order to to turn on maintenance mode you must fill all the fields.</span>          
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Massage</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="on.maintenance_msg" required="required" ng-model="on.maintenance_msg">
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Time</label>
                <div class="col-sm-4">
                  <div class="input-group bootstrap-touchspin">
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input ui-jq="TouchSpin" type="text" value="" class="form-control" data-min="0" data-max="100" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" required="required" name="on.maintenance_time" ng-model="on.maintenance_time" onkeypress='return isNumberKey(event)'> 
                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                  </div>
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">Off</spam> 
                <div class="m-b-sm col-sm-2">
                  <label class="i-switch i-switch-lg m-t-xs m-r">
                    <input type="checkbox" value="1" name="maintenance_on" ng-model="maintenance_on" required="required">
                    <i></i>
                  </label>
                </div>
                <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">On</spam> 
              </div>              
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="col-sm-12">
                <button type="submit" class="btn btn-sm  btn-success" ng-click="savesettings(on)" ng-disabled="formmaintenance.$invalid" style="float:right;">Save change</button>
              </div>            
            </div>
          </form>


          <div ng-if="value1 == 1">
            <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance">
              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">Maintenance mode is turned on.</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">{[{ value2 }]}</span>          
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">{[{ value3 }]} hours maintenance</span> 
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">

                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">On</spam> 
              <div class="m-b-sm col-sm-2">
                <label class="i-switch i-switch-lg m-t-xs m-r">
                  <input type="checkbox" value="0" name="maintenance_off" ng-model="maintenance_off" required="required">
                  <i></i>
                </label>
              </div>              
              <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">Off</spam> 
            </div>              
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="col-sm-12">
              <button type="submit" class="btn btn-sm btn-primary" id="off"  ng-click="savesettingsoff()" ng-disabled="formmaintenance.$invalid" style="float:right;">Save change</button>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading"><span class="h4">Change Logo</span></div>
        <div class="panel-body">      
          <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Upload Logo</span>
            <input id="digitalAssets" type="file" name="files[]">
          </span>
          <br><br>
          <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
          </div>
          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data">
            <div class="gallery digital-assets-gallery">
              <div class="clearfix"></div>
            </div>
          </form>
        <div  ng-repeat="info in data">
          <div class="data panel-default">
            <!-- <img style="width:auto;height:250px;position:relative; " src="<php echo $this->config->application->apiURL; ?>/images/pubbanner/{[{ info.imgpath }]}"> -->
          </div>
          <div class="row">
            <div class="panel-body">
              <footer class="panel-footer text-right bg-light lter">
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                <button type="submit" ng-click="savelogo(info.imgpath)" class="btn btn-success">Save</button>
              </footer>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading"><span class="h4">Google Analytics</span></div>
      <div class="panel-body">      
        <form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" ng-submit="save(scrept)" name="analyticform"> 
          <fieldset ng-disabled="isSaving"> 
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="row">
              <div class="panel-body">
                <label class=" control-label">Script</label>
                <div class="form-group">
                  <textarea ng-model="scrept.gscript" style="height:200px;width:400px" required="required"></textarea><br>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                  <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="analyticform.$invalid">Save</button>
                </footer>
              </div>
            </div>
          </fieldset> 
        </form>      
      </div>
    </div>
  </div>

</div>
</div>

