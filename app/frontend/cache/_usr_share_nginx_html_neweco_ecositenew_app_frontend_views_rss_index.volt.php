<?php 
?>


<?php 
   $data_db = $postinfo;

  header('Content-Type: text/xml');

 ?>
<rss version="2.0">
<channel>
 <title>RSS Title</title>
 <description>Earth Citizens Organization RSS feed</description>
 <link><?php echo $this->config->application->baseURL ?>/news/rss</link>
 <lastBuildDate><?php echo date('r'); ?> </lastBuildDate>


 <?php
 foreach($data_db as $key => $value){

  ?>
  <item>
    <title><![CDATA[<?php echo $data_db[$key]->title ?>]]></title>
    <pubDate><?php 
      $dates = '';
      if (substr($data_db[$key]->date, 5, -3) == 01)
      {
        $dates = 'Jan';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 02)
      {
        $dates = 'Feb';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 03)
      {
        $dates = 'Mar';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 04)
      {
        $dates = 'Apr';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 05)
      {
        $dates = 'May';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 06)
      {
        $dates = 'Jun';
      }elseif (substr($data_db[$key]->date, 5, -3) == 07)
      {
        $dates = 'Jul';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 08)
      {
        $dates = 'Aug';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 09)
      {
        $dates = 'Sep';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 10)
      {
        $dates = 'Oct';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 11)
      {
        $dates = 'Nov';
      }
      elseif (substr($data_db[$key]->date, 5, -3) == 12)
      {
        $dates = 'Dec';
      }
      echo $dates;
      echo ' '.substr($data_db[$key]->date,-2) ;
      echo ', '.substr($data_db[$key]->date, 0, -6);

    ?>
    </pubDate>
    <link><?php echo $this->config->application->baseURL ?>/news/<?php echo $data_db[$key]->newsslugs ?></link>

    <description><![CDATA[<?php  echo substr(strip_tags($data_db[$key]->body),0,400).'...'; ?>]]></description>

  </item>

  <?php
  }

  ?>
</channel>
</rss>