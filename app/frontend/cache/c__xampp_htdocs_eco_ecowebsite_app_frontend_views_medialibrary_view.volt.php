<a href="" id="errorget"></a>
<a href="" id="successget"></a>
<div class="container-fluid" ng-controller="viewvideoCtrl">

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">Learn</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary">Media library</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="" ng-bind="media.title"></a> 
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-9">
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-12">
              <span class="h4-title" ng-bind="media.title"></span>
            </div>
          </div>
          <div class="col-sm-12"><br>
            <div class="col-sm-12">
              <span ng-bind="media.shortdesc">
              </span>
            </div>
          </div>
          <div class="col-sm-12"><br>
            <div class='col-sm-8'>
             Published on <span ng-bind="convertToDate(media.datepublished) | date:'longDate'"></span> by <span ng-bind="media.firstname"></span> <span ng-bind="media.lastname"></span>
           </div>
           <div class="col-sm-4 likesviews">
              <i class="fa fa-heart-o cursor-pointer" ng-show="usrlike=='false'" ng-click="like(media.id,userAgent)" title="Like"></i>
              <i class="fa fa-heart text-danger cursor-pointer" ng-show="usrlike=='true'" ng-click="like(media.id,userAgent)" title="Unlike"></i>
               <span ng-bind="countlikes"></span> Likes | <i class="icon-eye"></i> <span>Views</span> <span ng-bind="views"></span>
           </div>
          </div>
          <div class="col-sm-12"><br>
            <div class="vidoe-width">
              <div ng-bind-html="preview" ng-if="media.videotype == 'embed'"></div>
              <div ng-show="media.videotype == 'upload'">
                <div id="myElement">Loading the player...</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="line line-dashed b-b line-lg"></div>
        </div>
        <div class="col-sm-12">
          <span ng-bind-html="media.description">
          </span>
        </div>
        <div class="col-sm-12">
          <div class="line line-dashed b-b line-lg"></div>
        </div>
        <div class="col-sm-12">
          <div class="blog-social">
            <div class="blog-social-item blog-fb-like float-left">
              <div class="fb-like" data-href="<?php echo $this->config->application->baseURL; ?>/blog/view/{[{media.shortdesc}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
              <div id="fb-root"></div>
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=217644384961866";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>


            </div>
            <div class="blog-social-item float-left margin-left">
              <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
              </script>
            </div>
            <div style="clear:both"></div>
            <div id="disqus_thread"></div>
            <script type="text/javascript">
              /* * * CONFIGURATION VARIABLES * * */
              var disqus_shortname = 'earthcitizensorganization';

              /* * * DON'T EDIT BELOW THIS LINE * * */
              (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>


          </div>
        </div>
        <div class="line line-dashed b-b line-lg"></div>

      
      </div>
      <div class="col-sm-3">
        <span class="title2">Donate Your Content</span><br><br>
        <p>
          Your content can help inspire thousands of promising leaders for a more sustainable future of our world. Please consider donating your content here so that it can benefit our leaders and public who care for people and the planet.
        </p>
        <b>By donating your content:</b><br><br>
        <p>
          - Your name will be recognized as contributor to our cause
          <br><br>
          - You grant ECO a non-exclusive license to use your content for a year for our media library and online education
          <br><br>
          - ECO will provide link to your information so that interested people may work directly with you.
        </p>
        <div class="yellow-button-slim"><a href="/medialibrary/addvideo">Donate Your Content</a></div> 
        <hr>
        <span class="title2">Categories</span>
        <ul class="sidebar">
          <li class="pad">
            <a href="<?php echo $this->config->application->baseURL; ?>/medialibrary">All</a><br/>
          </li>
          <?php
          foreach ($liftofcategory as $category ) {
            ?>
            <li class="pad">
              <a href="<?php echo '/medialibrary/category/'.$category->categoryslugs; ?>"><?php echo $category->categoryname; ?></a><br/>
            </li>
            <?php
          }

          ?>
        </ul>
        <!-- <hr> -->
        <br>
        <span class="title2">Tags </span>
        <div class="listCont">
          <div class="tagsL">
            <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { ?>
            <a href="/medialibrary/tag/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
            <?php
            $count > 5 ? $count =1 : $count++;
            } ?>
          </div>
        </div>
        <!-- <hr> -->
        <br>
        <span class="title2">Archives</span>
        <ul class="sidebar">
          <?php
          $getarchives = $archivelist;
          foreach ($getarchives as $key => $value) { ?>
          <li class="pad"><a href="/medialibrary/archives/<?php echo $getarchives[$key]->month . "-" . $getarchives[$key]->year;?>"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a></li>
          <?php } ?>
        </ul>
        <!-- <hr> -->        
      </div>
    </div>
  </div>
</div>