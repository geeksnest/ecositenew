<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <a href="#">JOIN</a> <a><i class="fa fa-arrow-right"></i></a><a href=""><?php echo $title ?></a> 
    </div>
  </div>
</div>
<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <p>Regional ECO groups are organizing events in various forms. ECO events will allow you to find how ECO works in local communities and meet with like-minded people who care for people and the planet. You can find more information about each event by visiting our event detail page.</p>
    </div>
  </div>
</div>
<div class="eco-container wrapper-proj" ng-controller="eventslistCtrl">
  <div class="row">
    <div class="col-sm-12">
      <h4>Explore <span ng-bind="eventcounts"></span> active ECO Events</h4>
    </div> 
  </div> 
    <div class="row list-title-club" ng-repeat="data in data" >
        
         <div class="col-md-4 club-thumb-container">
            <a href="{[{BASE_URL}]}/{[{data.URL}]}" class="news-thumb-link">
              <div >
                <div class="club-img" style="background-image: url({[{amazonlink}]}/uploads/eventsbanner/{[{data.banner}]});"></div>
              </div>
            </a>
          </div>         

          <div class="col-md-8 ">
            <div class="row">
              <div class="col-sm-12"> 

                <div class="row">
                  <div class="col-sm-12">
                    <span class="size30 font1 news-title"><span ng-bind="data.title"></span></span>
                  </div>
                  <div class="col-sm-12">
                    <div class="font1 size18 word-wrap">
                      <span  ng-bind="data.shortDesc"></span>
                      <br/><br/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div style="clear:both"></div>
            <br>
          </div>
      </div>
  <div class="center loadmorebot-wrap">
    <button ng-click="showmore()" ng-show="loadmorebot" class="proj-list-show-more">Load More</button>
  </div> 
</div>