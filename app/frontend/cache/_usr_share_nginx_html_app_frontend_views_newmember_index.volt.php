<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Earth Citizens Organization</title>

    <link rel="shortcut icon" type="image/ico" href="/images/template_images/favicon.png" />

    <link rel="stylesheet" href="/css/template_css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/template_css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/template_css/main.css">

    <link rel="stylesheet" href="/css/template_css/slidercss.css"> 
    <link rel="stylesheet" type="text/css" media="screen, print, projection"  href="/css/template_css/dropdown_css.css"></link>      

    <link rel="stylesheet" type="text/css" href="/css/template_css/initcarousel-1.css">

    <!--  <link rel="stylesheet" href="/css/template_css/index.css"> -->
    <link rel="stylesheet" href="/css/template_css/responsivemobilemenu.css">

    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>

    <!-- FROM MAIN -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">   
    <link rel="stylesheet" type="text/css" href="/css/fr/color.css"/>
    <link rel="stylesheet" type="text/css" href="/css/fr/reset.css"/>
    <link rel="stylesheet" href="css/fr/horizontal.css"/>
    <!-- END FROM MAIN --> 
    <link rel="stylesheet" href="/css/template_css/temp_index.css">    

</head>

<body ng-controller="indexMainCtrl">
    <div class="container-fluid">
        <div class="eco-container top">
        </div>
    </div>
    <div class="container-fluid">
        <div class="navstickyfs">
            <div class="eco-container" style="">
                <div id="ecologo" ><a href=""><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;margin-bottom: 15px;"></a></div>
                
            </div>
        </div>
        <div class="clearfix"></div>        
    </div>
</div>
<div class="container-fluid">
    <div class="mob-ecologo"  style="width:115px;margin:auto;padding-top:5px;"><a href="http://www.earthcitizens.org/indextemp"><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;"></a></div>    
</div>

<!--===begin container===-->  
<div class="container-fluid" ng-controller='newmemberCtrl' >
    <div class="eco-container" >
        <div class="wrapper-md " >
            
                
                <div class="col-lg-12 panel-body" >
                    
                    <form name="form" novalidate class="ng-pristine ng-valid" style="color:#777;fonts:0.8em sans-serif bold">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="panel panel-default">
                                    <div class="panel-heading font-bold"><span>Complete your registration, please fill up all the fields.</span></div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-sm-12" style="padding-top:10px;">
                                        <label class="control-label">Email address:</label> 
                                        <!-- <span class="formerror" ng-show="emailtaken != ''" style="color: #f80000;">Email already taken.</span> -->
                                        <!-- <span class="formerror" ng-show="emailtaken2 != ''" style="color: #f80000;">Email Not Found.</span> -->
                                        <input type="email" class="form-control rounded" name="" ng-model="user.mememail" name="email"  disabled />
                                        <input type="hidden" class="form-control rounded" name="" ng-model="user.mememail" name="email"  required />
                                        <input type="hidden" value="<?php echo $members_id; ?>" id="memsid">
                                    </div>

                                    <div class="col-sm-12" style="padding-top:10px;">
                                        <label class="control-label">Username:*</label>                              
                                        <span class="formerror" ng-show="usernametaken != ''" style="color: #f80000;">Username has already been taken.</span>
                                        <input type="text" class="form-control rounded" name="" ng-model="user.username" name="username" required />
                                    </div> 

                                    <div class="col-sm-12" style="padding-top:10px;">
                                        <label class="control-label">Temporary Password:*</label>
                                        <span class="formerror" ng-show="temppass != ''" style="color: #f80000;">Invalid Temporary Password.</span>
                                        <input type="password" class="form-control rounded" name="" ng-model="user.temppassword" name="temppassword" required />
                                    </div>

                                    <div class="col-sm-6" style="padding-top:10px;">
                                        <label class="control-label">New Password:*</label>
                                        <span class="formerror" ng-show="passmin == true">Shoud be minimum of 6 characters.</span>  
                                        <input type="password" class="form-control rounded" name="" ng-model="user.password" name="password" required />
                                    </div>

                                    <div class="col-sm-6" style="padding-top:10px;">
                                        <label class="control-label">Re-Type New Password:*</label>
                                        <span class="formerror" ng-show="user.password != user.repassword">Your Password Should be Equal.</span>
                                        <input type="password" class="form-control rounded" name="" ng-model="user.repassword" name="repassword" required/>
                                    </div>
                                    
                                    <div class="col-sm-6" style="padding-top:10px;">
                                        <label class="control-label">First Name:*</label>
                                        <input type="text" class="form-control rounded" name="" ng-model="user.memfname" name="fname" required/>
                                    </div>

                                    <div class="col-sm-6" style="padding-top:10px;">
                                        <label class="control-label">Last Name:*</label>
                                        <input type="text" class="form-control rounded" name="" ng-model="user.memlname" name="lname" required/>
                                    </div> 
                                    
                                    <div class="col-sm-12"><br/>
                                        <label class="control-label">Date of Birth</label>
                                        <div class="col-sm-12">
                                            <div style="float:left;padding-right:10px;">
                                                Month:
                                                <select ng-model="user.membmonth" class="form-control " style="width:150px">
                                                    <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                                                    foreach ($formonths as $index => $formonth) { 
                                                        echo "<option value='".$index."'>".$formonth."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div style="float:left;padding-right:10px;">
                                                Day:
                                                <select ng-model="user.membday" class="form-control " style="width:80px">
                                                    <?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
                                                    echo "<option value='".$days."'>".$days."</option>";
                                                } ?>
                                            </select>
                                        </div>
                                        <div style="float:left;padding-right:10px;">
                                            Year:
                                            <select ng-model="user.membyear" class="form-control " style="width:100px">
                                                <?php for ($year=1960; $year < 2015; $year++) { 
                                                    echo "<option value='".$year."'>".$year."</option>";
                                                }?>
                                            </select>
                                        </div>        
                                    </div>        
                                </div> 

                                <div class="col-sm-12"><br/>                                       
                                <label class="control-label">Gender:*</label>
                                    <div class="col-sm-12">  
                                        <div class="radio">
                                            <label class="i-checks" style="padding-right:10px;">
                                                <input type="radio" name="gender" value="Male" ng-model="user.memgender" required="required"><i></i>Male
                                            </label>
                                            <label class="i-checks" style="padding-right:10px;">
                                                <input type="radio" name="gender" value="Female" ng-model="user.memgender" required="required"><i></i>Female
                                            </label>
                                            <label class="i-checks" style="padding-right:10px;">
                                                <input type="radio" name="gender" value="Other" ng-model="user.memgender" required="required"><i></i>Other
                                            </label>
                                        </div>
                                                                         
                                    </div>                             
                                </div> 
                                <div class="col-sm-12">
                                    <label class="control-label">Location:*</label>
                                    <select id="location" name="location"  ng-init="user.location = countries[2]" ng-model="user.location" class="location form-control m-b" ng-options="bn.name for bn in countries" style="width:350px">

                                    </select> 
                                </div>     

                                <div class="col-sm-12">
                                    <label class="control-label" style="padding-top:10px;">Zipcode:*</label>
                                    <input type="text" id="zip" name="zip" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.memzip" required="required" style="width:250px">   
                                </div>                                

                                <div class="col-sm-12">
                                    <label class="control-label" style="padding-top:10px;">How did you learn about ECO?</label>
                                    <select class="form-control" id="howdidyoulearn" name="howdidyoulearn" ng-model="user.howdidyoulearn" style="width:300px">
                                        <option value=""></option>                                        
                                        <option value="ECO event">ECO event</option>
                                        <option value="ECO Program Graduates">ECO Program Graduates</option>
                                        <option value="Body &amp; Brain or Dahn Yoga Centers">Body &amp; Brain or Dahn Yoga Centers</option>
                                        <option value="Invitation Email">Invitation Email</option>                                        
                                    </select>
                                </div>     

                                <div class="col-sm-12" ng-show="user.howdidyoulearn == 'Body &amp; Brain or Dahn Yoga Centers'">
                                    <label class="control-label" style="padding-top:10px;">Center Name:</label>
                                    <select class="form-control" id="centersname" name="centersname" ng-model="user.centersname" style="width:300px">
                                        <option value=""></option>
                                        <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                    </select>
                                </div>      

                                <div class="col-sm-12">
                                    <label class="control-label" style="padding-top:10px;">I would like my friend to become an Earth Citizen as well.</label>
                                    <input type="text" id="refer" name="refer" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.refer" style="width:500px" placeholder="Enter friend's email address">   
                                    <span style="font-size: 10px;">Thank you for introducing ECO to your friend. ECO will send them an invitation email.</span>
                                </div>  

                               <!--  <div class="form-group">
                                <label class="col-lg-2 control-label">I would like my friend to become an Earth Citizen as well.</label>
                                <div class="col-lg-10">
                                    <input type="text" id="refer" name="refer" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="bnb.refer" style="width:500px" placeholder="Enter friend's email address">
                                    <span style="font-size: 10px;">Thank you for introducing ECO to your friend. ECO will send them an invitation email.</span>
                                </div>
                                </div>        -->                                                    
                                  

                                <div class="col-sm-12"  style="padding-top:20px;">                                    
                                    <label ng-show="process == true">Processing Registration...</label>
                                    <input type="button" name="" id="abouteco" value="Submit" ng-show="process == false" class="btn btn-sm btn-primary" ng-click="register(user)" ng-disabled="form.$invalid" />                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-sm-12">
                    <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:17px;color:#777;">
                        <p>
                            ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
                        </p>
                    </div>
                </div>
            
        </div>
    </div>
</div>   

<div class="container-fluid">
    <div  class="footer-bg">
        <div class="eco-container">
                       <!--  <div class="row">
                            <div class="col-md-3" style="color: #ffffff !important; float: left;">
                                <span>MISSION STATEMENT<br/><br/></span>
                                <div style="width:230px; padding-right: 15px;">
                                    <span style="font-size: 12px;">
                                        Earth Citizens Organization (ECO) is a nonprofit that promotes natural health and mindful living for a sustainable world. An Earth Citizen is a person who understands that we are all citizens of the Earth and takes responsibility for its well-being.<br/><br/>
                                    </span>
                                </div>

                            </div>
                            <div class="col-md-3" style="color: #ffffff !important; float: left;">
                                <span>NAVIGATION<br/><br/></span>
                                <div style="width:190px; padding-right: 55px;">
                                    <span style="font-size: 12px;">
                                        <nav  class="nav" style="padding-top: 0px;">
                                            <a href="#abouteco" style="color:#ffffff;">ABOUT US</a><br/>
                                            <a href="#ouractivities" style="color:#ffffff;">OUR ACTIVITIES</a><br/>
                                            <a href="#ourprograms" style="color:#ffffff;">OUR PROGRAMS</a><br/>
                                            <a href="http://heroesconnect.org" style="color:#ffffff;">HEROES CONNECT</a><br/>
                                            <a href="#supportus" style="color:#ffffff;">SUPPORT US</a><br/>
                                            <a href="../donation" style="color:#ffffff;">DONATE</a><br/><br/>
                                        </nav>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3" style="color: #ffffff !important; float: left;">
                                <span>ADD US<br/><br/></span>
                                <div style="width:190px; padding-right: 55px;">
                                    <span style="font-size: 12px;">
                                        <ul>
                                            <li><a href="https://www.youtube.com/user/goearthcitizens" style="color:#ffffff;"><i class="fa fa-youtube" style="color: #ffffff;"></i>&nbsp;&nbsp;YouTube</a><br/><br/></li>
                                            <li><a href="https://www.facebook.com/EarthCitizensOrganization" style="color:#ffffff;"><i class="fa fa-facebook" style="color: #ffffff;"></i>&nbsp;&nbsp;Facebook</a><br/><br/></li>
                                            <li><a href="https://twitter.com/goearthcitizens" style="color:#ffffff;"><i class="fa fa-twitter" style="color: #ffffff;"></i>&nbsp;&nbsp;Twitter</a></li>
                                        </ul><br/><br/>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3" style="color: #ffffff !important; float: left;">
                                <span>CONTACT US<br/><br/></span>
                                <div style="width:230px; padding-right: 15px;">
                                    <span style="font-size: 12px;">
                                        Earth Citizens Organization<br/>
                                        340 Jordan Road Sedona, AZ 86336
                                        <br/><br/>
                                        <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Email: goearthcitizens@gmail.com
                                        <br/><br/>
                                    </span>
                                </div>
                            </div>                    
                        </div>
                        <div class="row" style="padding: 10px 0px 10px 0px;">
                            <div  style="  width: 100%;  background-color: #ffffff;  height: 3px;"></div>
                        </div> -->
                        <div class="row" style="padding-top: 2px;">
                            <p style="font-size: 10px; text-align: center;color:#ffffff;">
                                ©2015 Earth Citizens Organization All rights reserved
                            </p>
                        </div>
                    </div>
                </div>
            </div>  



    <script>
        var API_URL = '<?php echo $this->config->application->apiURL; ?>';
        var BASE_URL = '<?php echo $this->config->application->baseURL; ?>';
    </script>

<!-- <script src="js/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" src="/js/template_js/jquery-1.9.1.min.js"></script>

<!-- <script src="/js/template_js/jquery.min.js"></script> -->
<script src="/js/template_js/bootstrap.min.js"></script>

<!-- dropdown -->
<!-- <script src="/js/template_js/jquery.js"></script> -->
<script src="/js/template_js/dropdown_js.js"></script>
<!-- stiky nav -->
<!-- <script src="/js/stickytop.js"></script> -->
<!-- end dropdown -->

<script src="/js/template_js/main.js"></script>


<!-- <script type="text/javascript" src="/js/fr/jquery.counterup.min.js"></script>
<script type="text/javascript" src="/js/fr/waypoint.min.js"></script>
<script>
    $(document).ready(function () {

        $(".mapp").hide();
        $(".map-btncl").hide();
        $(".map-btn").click(function () {
            $(".mapp").toggle("slow");
        });
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });        
</script> -->
<script type="text/javascript" src="/js/angular/angular.min.js"></script> 
<script type="text/javascript" src="/js/angular/angular-route.js"></script> 
<script type="text/javascript" src="/js/angular/angular-ui-router.min.js"></script> 
<script type="text/javascript" src="/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/js/angular/angular-sanitize.js"></script>

<!-- Angular -->
<script type="text/javascript" src="/js/angular/angular-cookies.min.js"></script>
<script type="text/javascript" src="/js/angular/angular-animate.min.js"></script>
<script type="text/javascript" src="/js/angular/angular-translate.js"></script>
<script type="text/javascript" src="/js/angular/ngStorage.min.js"></script>
<script type="text/javascript" src="/js/angular/ui-load.js"></script>
<script type="text/javascript" src="/js/angular/ui-jq.js"></script>
<script type="text/javascript" src="/js/angular/ui-bootstrap-tpls.min.js"></script>
<!-- <script type="text/javascript" src="/js/angular/angular-summernote-master/src/angular-summernote.js"></script> -->

<!-- <script type="text/javascript" src="/js/angular/timepickerpop-master/src/timepickerpop.js"></script>
<script type="text/javascript" src="/js/angular/angular-jwt/dist/angular-jwt.min.js"></script>
<script type="text/javascript" src="/js/angular/a0-angular-storage/dist/angular-storage.min.js"></script>
<script type="text/javascript" src="/js/angular/angular-loading-master/spin.min.js"></script>
<script type="text/javascript" src="/js/angular/angular-loading-master/angular-loading.js"></script> -->

<!-- ng upload -->
<!-- <script type="text/javascript" src="/js/angular/ng-file-upload/ng-file-upload.min.js"></script>
<script type="text/javascript" src="/js/angular/ng-file-upload/ng-file-upload-shim.js"></script> -->

<!-- angular-timer -->
<!-- <script type="text/javascript" src="/vendors/angular-timer/dist/angular-timer.js"></script>
<script type="text/javascript" src="/vendors/humanize-duration/humanize-duration.js"></script>
<script type="text/javascript" src="/vendors/momentjs/moment.js"></script> -->

<script type="text/javascript" src="/js/fr/app.js"></script>
<script type="text/javascript" src="/js/config/config.js"></script>
    <?php echo @$otherjvascript ?>
<script type="text/javascript" src="/js/fr/filter.js"></script>
<!-- <script type="text/javascript" src="/js/directives.js"></script> -->
<!-- <script type="text/javascript" src="/js/fr/directives.js"></script> -->
<!-- <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script> -->
<!-- <script type="text/javascript" src="/js/angular/angular-ckeditor-master/angular-ckeditor.js"></script> -->
<!-- <script type="text/javascript" src="/js/ckeditor/styles.js"></script> -->
<!-- <script type="text/javascript" src="/js/ckeditor/adapters/jquery.js"></script> -->
<!-- <script type="text/javascript" src="/js/fr/news.js"></script> -->


    <!-- Calendar Demo -->

    <script src="/fcalendar/calendar.js"></script>

    <script type="text/javascript" src="/js/jquery.fancybox.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

        });
    </script>


    <?php echo $script_google; ?>
    
</body>
</html>





