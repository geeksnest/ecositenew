<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <a href="#">SERVE</a> <a><i class="fa fa-arrow-right"></i></a> <a href=""><?php echo $title; ?></a> 
    </div>
  </div>
</div>
<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-9">
      <p>Welcome to the Find a HERO Next Door Campaign. Through this campaign, we desire to encourage people to share their own HERO stories and inspire others to find the greatness within.</p>
      <p>Any individual or group that is actively engaged in caring for people and the planet can join. The applicant who receives the most votes each month will be the winner and will receive a $1,000 ECO grant.</p>
      <p>This campaign is currently sponsored by Microsoft.</p>
      <p>Here is the list of our applicants for this month. Please review their info and vote. You can vote through Twitter and Facebook, one vote per social media platform per day.</p>
    </div>
    <!-- <div class="col-sm-1"></div> -->
    <div class="col-sm-3">
      <p><b>Apply for Contest</b></p>
      <p>Your content can help inspire thousands of promising leaders for a more sustainable future of our world. Pleas consider donating your content here so that it can benefit our leaders and public who care for people and the planet.</p>
      <p>By donating your content:</p>
      <p>- Your name will be recognized as contributor to our cause</p>
      <p>- You grant ECO a non-exclusive license to use your content for a year for our media library and online education</p>
      <p>- ECO will provide link to your information so that interested people may work directly with you.</p>
      <!-- <p><div class="learnheroes"><a href="<?php echo $linkURL; ?>"><?php echo $linkName; ?></a></div> </p> -->
    </div>
  </div>
</div>