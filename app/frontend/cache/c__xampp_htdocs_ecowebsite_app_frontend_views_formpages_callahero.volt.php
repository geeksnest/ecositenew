<div class="row" ng-controller="HeroesCallCtrl">
  <div class="col-sm-12">
    <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="callHereSave(user)">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>Name of Organization</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.organization" name="organization" required="true"  >
          </div>
          <div class="form-group">
            <label>Organization’s Tax ID</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.taxid" name="taxid" required="true" >
          </div>
          <div class="form-group">
            <label>Contact Phone Number</label>
            <input type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" phone-input  placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" >
          </div>
          <div class="form-group">
            <label>Official Address</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.address" name="address" required="true" >
          </div>
          <div class="form-group">
            <label>Help Location (if different from above)</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.helplocaiton" name="helplocaiton" required="true">
          </div>
          <div class="form-group">
            <label>Reasons for needing help</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.reasonhelp" name="reasonhelp" required="true" >
          </div>
          <div class="form-group">
            <label>When you need help</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.whyhelp" name="whyhelp" required="true" >
          </div>
          <div class="form-group">
            <label>Desired qualification for helpers</label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.qualification" name="qualification" required="true" >
          </div>
          <div class="checkbox">
            <label class="i-checks">
              <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)"><i></i> 
              <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
              <label ng-show="chkerror"  class="control-label" style="color:#a94442; margin-left:-40px;">This fields is required.</label>
            </label>
          </div>
        </div>
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <footer class="panel-footer text-right bg-light lter">
          <button type="submit" class="btn btn-success"  >Submit</button>
        </footer>
      </div>
    </form>
  </div>
</div>