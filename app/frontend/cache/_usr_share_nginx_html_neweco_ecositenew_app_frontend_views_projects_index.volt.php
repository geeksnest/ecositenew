<?php echo $this->getContent() ?>
<div class="container-fluid" ng-controller="viewProjListCtrl" ng-init="loadmore()">

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a><a href="{[{BASE_URL}]}/#serve">SERVE</a> <a><i class='fa fa-arrow-right'></i></a> <a href>ECO Projects</a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj" ng-if="newProj">
    <div class="row">
      <div class="col-sm-12">
        <h4>New & Noteworthy</h4>
      </div> 

      <div class="col-sm-4" ng-repeat="data in newProj | limitTo:3">
        <div class="proj-list-wrap">
          <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
            <div class="proj-thumb" style="background-image: url('{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}');"></div>
          </a>
          <div class="proj-content">
            <div class="row"> 
              <div class="col-sm-12">
                <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
                  <label class="proj-title1 proj-list-title" ng-bind="data.projTitle"></label>
                </a>
              </div>    
              <div class="col-sm-12">
                <label class="proj-font1 capitalize" ng-bind="data.firstname"></label>          
              </div>    
              <div class="col-sm-12">
                <p class="proj-font1 proj-list-desc" ng-bind="data.projShortDesc"></p>
              </div>     
              <div class="col-sm-12 chart-wrap">
                <label class="proj-font1"><span class="black" ng-bind="data.projTotalPercentage"></span>% funded</label>          
                <div class="chart"><div style="width: {[{data.projTotalPercentage}]}%;"></div></div>
              </div> 
              <div class="col-sm-6">
                <label class="proj-font1"><span class="black">$<span ng-bind="data.projTotalDonations | putcomma"></span></span><br> pledged</label>
              </div>
              <div class="col-sm-6" ng-show="data.projStatus == '2'">
                <!-- angular-timer -->
                <timer end-time="data.countStart | charToint">
                  <label class="proj-font1">
                    <label ng-show="days != 0">
                      <span class="black" ng-bind="days"></span><br> days to go
                    </label> 
                    <label  ng-show="days == 0">
                      <span class="black">
                        <span ng-bind="hours"></span>:<span ng-bind="minutes"></span>:<span ng-bind="seconds"></span>
                      </span><br> hours to go
                    </label> 
                  </label>        
                </timer>
                <!-- end angular-timer -->
              </div>
              <div class="col-sm-6" ng-show="data.projStatus == '5'">
              <a href="" class="green-font" title="Completed"><i class="fa fa-check"></i></a>
              </div>
              <div class="col-sm-12">  
                <label class="proj-font1"><img src="/images/pin.png" width="18">  <span ng-bind="data.projLoc"></span></label>          
              </div>
            </div> 
          </div> 
        </div>
      </div>
    </div>
    <div class="load-more-div">
      <div ng-show="loadingGIFn" class="load-more-loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj" ng-if="newProj">
    <div class="row">
      <div class="col-sm-12">
        <h4>What’s Popular</h4>
      </div> 

      <div class="col-sm-4" ng-repeat="data in newProj | orderBy : predicate : reverse | limitTo:3">
        <div class="proj-list-wrap">
          <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
            <div class="proj-thumb" style="background-image: url('{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}');"></div>
          </a>
          <div class="proj-content">
            <div class="row"> 
              <div class="col-sm-12">
                <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
                  <label class="proj-title1 proj-list-title" ng-bind="data.projTitle"></label>
                </a>
              </div>    
              <div class="col-sm-12">
                <label class="proj-font1 capitalize" ng-bind="data.firstname"></label>          
              </div>    
              <div class="col-sm-12">
                <p class="proj-font1 proj-list-desc" ng-bind="data.projShortDesc"></p>
              </div>      
              <div class="col-sm-12 chart-wrap">
                <label class="proj-font1"><span class="black" ng-bind="data.projTotalPercentage"></span>% funded</label>          
                <div class="chart"><div style="width: {[{data.projTotalPercentage}]}%;"></div></div>
              </div>  
              <div class="col-sm-6">
                <label class="proj-font1"><span class="black">$<span ng-bind="data.projTotalDonations | putcomma"></span></span><br> pledged</label>
              </div>
              <div class="col-sm-6" ng-show="data.projStatus == '2'">
                <!-- angular-timer -->
                <timer end-time="data.countStart | charToint">
                  <label class="proj-font1">
                    <label ng-show="days != 0">
                      <span class="black" ng-bind="days"></span><br> days to go
                    </label> 
                    <label  ng-show="days == 0">
                      <span class="black">
                        <span ng-bind="hours">:</span><span ng-bind="minutes"></span>:<span ng-bind="seconds"></span>
                      </span><br> hours to go
                    </label> 
                  </label>        
                </timer>
                <!-- end angular-timer -->
              </div>
              <div class="col-sm-6" ng-show="data.projStatus == '5'">
              <a href="" class="green-font" title="Completed"><i class="fa fa-check"></i></a>
              </div>
              <div class="col-sm-12">  
                <label class="proj-font1"><img src="/images/pin.png" width="18">  <span ng-bind="data.projLoc"></span></label>          
              </div>
            </div> 
          </div> 
        </div>
      </div>
    </div>
    <div class="load-more-div">
      <div ng-show="loadingGIFn" class="load-more-loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj" ng-if="oyaaa">
    <div class="row">
      <div class="col-sm-12">
        <h4>Explore <span ng-bind="allCount"></span> live ECO Projects</h4>
      </div> 
    </div> 
    <div class="row">
      <div class="col-sm-3" ng-repeat="data in oyaaa">
        <div class="proj-list-wrap">
          <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
            <div class="proj-thumb2" style="background-image: url('{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}');"></div>
          </a>
          <div class="row proj-content"> 
            <div class="col-sm-12">
              <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
                <label class="proj-title1 proj-list-title" ng-bind="data.projTitle"></label>
              </a>
            </div>    
            <div class="col-sm-12">
              <label class="proj-font1 capitalize" ng-bind="data.firstname"></label>         
            </div>    
            <div class="col-sm-12">
              <p class="proj-font1 proj-list-desc" ng-bind="data.projShortDesc"></p>
            </div>     
            <div class="col-sm-12 chart-wrap">
              <label class="proj-font1"><span class="black" ng-bind="data.projTotalPercentage"></span>% funded</label>          
              <div class="chart"><div style="width: {[{data.projTotalPercentage}]}%;"></div></div>
            </div>  
            <div class="col-sm-6">
              <label class="proj-font1"><span class="black">$<span ng-bind="data.projTotalDonations | putcomma"></span></span><br> pledged</label>
            </div>
            <div class="col-sm-6" ng-show="data.projStatus == '2'">
              <!-- angular-timer -->
              <timer end-time="data.countStart | charToint">
                <label class="proj-font1">
                  <label ng-show="days != 0">
                    <span class="black" ng-bind="days"></span><br> days to go
                  </label> 
                  <label  ng-show="days == 0">
                    <span class="black">
                      <span ng-bind="hours">:</span><span ng-bind="minutes"></span>:<span ng-bind="seconds"></span>
                    </span><br> hours to go
                  </label> 
                </label>        
              </timer>
              <!-- end angular-timer -->
            </div>
            <div class="col-sm-6" ng-show="data.projStatus == '5'">
              <a href="" class="green-font" title="Completed"><i class="fa fa-check"></i></a>
            </div>
            <div class="col-sm-12">  
              <label class="proj-font1"><img src="/images/pin.png" width="18">  <span ng-bind="data.projLoc"></span></label>          
            </div>
          </div> 
        </div>
      </div>
    </div>
    <div class="load-more-div">
      <button ng-hide="loading" class="proj-list-show-more" ng-click="loadmorebot()" ng-disabled="loading"><span>Load More</span>
      </button>
      <div ng-show="loadingGIF" class="load-more-loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj" ng-if="finished">
    <div class="row">
      <div class="col-sm-12">
        <h4>Explore <span ng-bind="allCountf"></span> successful ECO Projects</h4>
      </div> 
    </div> 
    <div class="row">
      <div class="col-sm-3" ng-repeat="data in finished">
        <div class="proj-list-wrap">
          <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
            <div class="proj-thumb2" style="background-image: url('{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}');"></div>
          </a>
          <div class="row proj-content"> 
            <div class="col-sm-12">
              <a href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}">
                <label class="proj-title1 proj-list-title" ng-bind="data.projTitle"></label>
              </a>
            </div>    
            <div class="col-sm-12">
              <label class="proj-font1 capitalize" ng-bind="data.firstname"></label>         
            </div>    
            <div class="col-sm-12">
              <p class="proj-font1 proj-list-desc" ng-bind="data.projShortDesc"></p>
            </div>     
            <div class="col-sm-12 chart-wrap">
              <label class="proj-font1"><span class="black" ng-bind="data.projTotalPercentage"></span>% funded</label>          
              <div class="chart"><div style="width: {[{data.projTotalPercentage}]}%;"></div></div>
            </div>  
            <div class="col-sm-6">
              <label class="proj-font1"><span class="black">$<span ng-bind="data.projTotalDonations | putcomma"></span></span><br> pledged</label>
            </div>
            <div class="col-sm-6" ng-show="data.projStatus == '2'">
              <!-- angular-timer -->
              <timer end-time="data.countStart | charToint">
                <label class="proj-font1">
                  <label ng-show="days != 0">
                    <span class="black" ng-bind="days"></span><br> days to go
                  </label> 
                  <label  ng-show="days == 0">
                    <span class="black">
                      <span ng-bind="hours">:</span><span ng-bind="minutes"></span>:<span ng-bind="seconds"></span>
                    </span><br> hours to go
                  </label> 
                </label>        
              </timer>
              <!-- end angular-timer -->
            </div>
            <div class="col-sm-6" ng-show="data.projStatus == '5'">
              <a href="" class="green-font" title="Completed"><i class="fa fa-check"></i></a>
            </div>
            <div class="col-sm-12">  
              <label class="proj-font1"><img src="/images/pin.png" width="18">  <span ng-bind="data.projLoc"></span></label>          
            </div>
          </div> 
        </div>
      </div>
    </div>
    <div class="load-more-div">
      <button ng-hide="loadingf" class="proj-list-show-more" ng-click="loadmorebotf()" ng-disabled="loadingf"><span>Load More</span>
      </button>
      <div ng-show="loadingGIFf" class="load-more-loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="eco-container" ng-hide="finished || oyaaa || newProj">           
    <div class="pages-404">
      <div class="row">
        <div class="col-sm-12">
          <div class="text-center m-b-lg">
            <span class="">No Data Available</span>
          </div>
        </div>
        <div class="col-sm-12">
        </div>
      </div>
    </div>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
  </div>


</div>