<div class="container-fluid" ng-controller="medialistCtrl">
  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">Learn</a> <a><i class='fa fa-arrow-right'></i></a> <a href="">Media library</a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-9">
        <div class="row">
          <div class="col-sm-12">
            <h4>Featured</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="col-sm-6">
              <a href="/medialibrary/{[{featured.slugs}]}" class="cursor-pointer">
                <div ng-if="featured.videotype == 'embed'" style="background-image: url({[{ featured.embed | returnYoutubeThumb }]});width:100%;height:200px;background-size: cover;">
                  <div class="youtube-play-ml" style="background-image: url(/images/icons/play-icon-jw.png);"></div>
                </div>
                <div ng-if="featured.videotype == 'upload'" style="background-image: url({[{ amazonlink }]}/videos/thumbs/{[{ featured.videothumb }]});width:100%;height:200px;background-size: cover;">
                  <div class="youtube-play-ml" style="background-image: url(/images/icons/play-icon-jw.png);"></div>
                </div>
              </a>
            </div>
            <div class="col-sm-6">
              <a href="/medialibrary/{[{featured.slugs}]}" class="cursor-pointer no-dec">
                <h4 ng-bind="featured.title"></h4>
              </a>
              by <span ng-bind="featured.firstname"></span> <span ng-bind="featured.lastname"></span>
              <br><span ng-bind="views"></span> Views |
              <timer start-time="featured.timeStarted |charToint">
                <span ng-show="days > 0">
                  <span ng-show="days == 1">a day ago</span>
                  <span ng-show="days > 1">{[{days}]} days ago {[{weeks}]}</span>
                </span>
                <span ng-show="hours > 0 && days == 0">
                  <span ng-show="hours == 1">an hour ago</span>
                  <span ng-show="hours > 1">{[{hours}]} hours ago</span>
                </span>
                <span ng-show="minutes > 0  && hours == 0 && days == 0">
                  <span ng-show="minutes == 1">a minute ago</span>
                  <span ng-show="minutes > 1">{[{minutes}]} minutes ago</span>
                </span>
                <span ng-show="seconds > 0  && minutes == 0 && hours == 0 && days == 0">
                  a seconds ago
                </span>
              </timer>
              <br><br>
              <p ng-bind="featured.shortdesc">
              </p>
            </div>
          </div>
        </div>
        <div class="row" ng-hide="pupular==null || pupular[0].likes==0">
          <div class="col-sm-12">
            <div class="line line-dashed b-b line-lg"></div>
            <h4>What's Popular</h4>
          </div>
        </div>
        <div ng-hide="pupular==null || pupular[0].likes==0">
          <div class="col-sm-4 cursor-pointer" ng-repeat="pupular1 in pupular" ng-hide="pupular1.likes==0">
            <a href="/medialibrary/{[{pupular1.slugs}]}" class="cursor-pointer no-dec">
              <div ng-if="pupular1.videotype == 'embed'" style="background-image: url({[{ pupular1.embed | returnYoutubeThumb }]});width:100%;height:144px;background-size: cover;">
                <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
              </div>
              <div ng-if="pupular1.videotype == 'upload'" style="background-image: url({[{ amazonlink }]}/videos/thumbs/{[{ pupular1.videothumb }]});width:100%;height:144px;background-size: cover;">
                <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
              </div>
            </a>
            <a href="/medialibrary/{[{pupular1.slugs}]}" class="cursor-pointer no-dec">
              <div class="hnv-title"><b><span ng-bind="pupular1.title"></span></b></div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="line line-dashed b-b line-lg"></div>
            <h4>New & Noteworthy</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div ng-repeat="videos in allList" class="col-sm-4 cursor-pointer hnv">
              <a href="/medialibrary/{[{videos.slugs}]}" class="cursor-pointer no-dec">
                <div ng-if="videos.videotype == 'embed'" style="background-image: url({[{ videos.embed | returnYoutubeThumb }]});width:100%;height:144px;background-size: cover;">
                  <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
                </div>
                <div ng-if="videos.videotype == 'upload'" style="background-image: url({[{ amazonlink }]}/videos/thumbs/{[{ videos.videothumb }]});width:100%;height:144px;background-size: cover;">
                  <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
                </div>
              </a>
              <a href="/medialibrary/{[{videos.slugs}]}" class="cursor-pointer no-dec">
                <div class="hnv-title"><b><span ng-bind="videos.title"></span></b></div>
              </a><br><br><br>
            </div>
          </div>
        </div>
        <div class="load-more-div">
          <button ng-hide="loading" class="proj-list-show-more" ng-click="loadmorebot()" ng-disabled="loading"><span>Load More</span>
          </button>
          <div ng-show="loadingGIF" class="load-more-loading">
            <div class="sk-fading-circle">
              <div class="sk-circle1 sk-circle"></div>
              <div class="sk-circle2 sk-circle"></div>
              <div class="sk-circle3 sk-circle"></div>
              <div class="sk-circle4 sk-circle"></div>
              <div class="sk-circle5 sk-circle"></div>
              <div class="sk-circle6 sk-circle"></div>
              <div class="sk-circle7 sk-circle"></div>
              <div class="sk-circle8 sk-circle"></div>
              <div class="sk-circle9 sk-circle"></div>
              <div class="sk-circle10 sk-circle"></div>
              <div class="sk-circle11 sk-circle"></div>
              <div class="sk-circle12 sk-circle"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <span class="title2">Donate Your Content</span><br><br>
        <p>
          Your content can help inspire thousands of promising leaders for a more sustainable future of our world. Please consider donating your content here so that it can benefit our leaders and public who care for people and the planet.
        </p>
        <b>By donating your content:</b><br><br>
        <p>
          - Your name will be recognized as contributor to our cause
          <br><br>
          - You grant ECO a non-exclusive license to use your content for a year for our media library and online education
          <br><br>
          - ECO will provide link to your information so that interested people may work directly with you.
        </p>
        <div class="yellow-button-slim"><a href="/medialibrary/addvideo">Donate Your Content</a></div> 
        <hr>
        <span class="title2">Categories</span>
        <ul class="sidebar">
          <li class="pad">
            <a href="<?php echo $this->config->application->baseURL; ?>/medialibrary">All</a><br/>
          </li>
          <?php
          foreach ($liftofcategory as $category ) {
            ?>
            <li class="pad">
              <a href="<?php echo '/medialibrary/category/'.$category->categoryslugs; ?>"><?php echo $category->categoryname; ?></a><br/>
            </li>
            <?php
          }

          ?>
        </ul>
        <!-- <hr> -->
        <br>
        <span class="title2">Tags </span>
        <div class="listCont">
          <div class="tagsL">
            <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { 
              ?>
              <a href="/medialibrary/tag/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
              <?php
              $count > 5 ? $count =1 : $count++;
            } ?>
          </div>
        </div>
        <!-- <hr> -->
        <br>
        <span class="title2">Archives</span>
        <ul class="sidebar">
         <?php
         $getarchives = $archivelist;
         foreach ($getarchives as $key => $value) { 
          ?>
          <li class="pad">
            <a href="/medialibrary/archives/<?php echo $getarchives[$key]->month . "-" . $getarchives[$key]->year;?>">
              <span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></span>
            </a>
          </li>
          <?php } ?>
        </ul>
        <!-- <hr> -->        
      </div>
    </div>


  </div>
</div>