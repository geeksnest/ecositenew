<?php echo $this->getContent() ?>
<style type="text/css">
  iframe {
    width: 100%;
    height: 100%;
  }
</style>


<!-- ===== ECOCLUBS ===== -->
<div class="container-fluid" ng-controller="Clubctrl"> 
  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#join">JOIN ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="">Earth Citizens Clubs</a> 
      </div>
    </div>
  </div>

  <div class="eco-container" id="newslist">   
    <div class="row"> 
      <div class="col-sm-12">
        <h1>Earth Citizens Clubs</h1>        
      </div>
      <div class="col-xs-12 greyborder1" id="latestlist">      

        <div class="row list-title-club" ng-repeat="mem in data.data" >
        
         <div class="col-md-4 club-thumb-container">
            <span class="news-thumb-link">
              <div >
                <div class="club-img" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/club/{[{mem.image}]});"></div>
              </div>
            </span>
          </div>         

          <div class="col-md-8 ">
            <div class="row">
              <div class="col-sm-12"> 

                <div class="row">
                  <div class="col-sm-12">
                    <span class="size30 font1 news-title"><span ng-bind="mem.name"></span></span>
                  </div>
                  <div class="col-sm-12">
                      <strong>
                      <span ng-bind="mem.city"></span>, <span ng-bind="mem.state"></span>                    
                      </strong><br>
                      <strong>
                        <span class="orange"><a class="clubemail" href="mailto:{[{ mem.emailadd }]}"><span ng-bind="mem.emailadd"></span></a></span>
                      </strong>
                    <br/><br/>
                  </div>
                  <div class="col-sm-12">
                    <div class="font1 size18 word-wrap">
                      <span ng-bind="mem.description"></span>
                      <br/><br/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div style="clear:both"></div>
            <br>
          </div>
      </div>

         <button ng-hide="hideloadmore" class="club-list-show-more" ng-click="showmore()" ng-disabled="loading"><span ng-show="loading"><img src="/images/template_images/newsloading.gif"></span><span ng-hide="loading">Show More</span></button>
         <button ng-show="nomoreclubs" class="club-list-show-more" ng-disabled="nomoreclubs"><span>end of the list</span></button>
        <hr>   
        
      </div>
    </div>
  </div>
</div> 

