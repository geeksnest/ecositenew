<a href="" id="errorget"></a>
<a href="" id="successget"></a>
<div class="container-fluid" ng-controller="editvideoCtrl">

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">LEARN</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary/mymedialibrary">My Media library</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="">Edit Video</a> 
      </div>
    </div>
  </div>

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h4>Update Your Video</h4>
      </div>
      <div class="col-sm-12">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eu quam erat. Vivamus sed neque nec nibh aliquet euismod. Donec scelerisque facilisis nulla bibendum maximus.
      </div>
    </div>
    <div class="create-form">
      <div class="row"> 
        <div class="col-sm-12" ng-show="media.status == 'Disapproved'">
          <div class="line line-dashed b-b line-lg"></div>
          <alert type="danger">
            This Video was declined by the Eco Board.<br><br>
            Reason: <span ng-bind="media.disapprovemsg"></span>
          </alert>
          <div class="line line-dashed b-b line-lg"></div>
        </div>  
      </div>  
      <!-- <div class="row">  

        <div class="col-sm-8">  -->

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-danger" ng-show="errorget">
                <span class="" ng-bind="errorget"></span>
              </div>
              <div class="form-group wrapper-sm bg-success" ng-show="successget">
                <span class="" ng-bind="successget"></span>
              </div> 
              

              <div class="form-group">
                <input type="hidden" ng-init="media.authorid = userAgent" ng-model="media.authorid"/>
                Content Title <label class="req">*</label>
                <input type="text" class="form-control" ng-model="media.title" name="title" id="title" required ng-disabled="loadingg == true"/>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

             <!--  <div class="form-group">
                Short Description <label class="req">*</label>
                <textarea class="form-control textarea" rows="4" ng-model="media.shortdesc" name="shortdesc" id="shortdesc" maxlength="200"></textarea>
              </div>
              <div class="line line-dashed b-b line-lg"></div> -->

               <div class="form-group">
                  Content Description: <label class="req">*</label> <label ng-show="n && !media.description" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label> <input type="text" id="desc" style="opacity:0;">
                  <!-- <div ckeditor="options" ng-model="media.description" name="description" id="description" ready="onReady()" ng-disabled="loadingg == true"></div> -->
                  <textarea class="ck-editor" ng-model="media.description" name="description" id="description" ng-disabled="loadingg == true"></textarea>
                </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                  Contact Name: <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="media.name" name="name" id="name" required ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                 <div class="form-group">
                  Contact Phone Number: <label class="req">*</label>
                  <input id="phone" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" ng-model="media.phone" placeholder="(XXX) XXXX XXX" phone ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" only-digits name="phone" ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                 <div class="form-group">
                  Email Address: <label class="req">*</label>
                  <input type="email" class="form-control" ng-model="media.email" name="email" id="email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Website Address: <label style="font-size:12px">(optional)</label>
                  <input type="text" class="form-control" ng-model="media.web" name="web" id="web" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" placeholder="http://" ng-disabled="loadingg == true">
                  <label ng-show="form.web.$invalid && !form.web.$pristine && form.web.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                 Facebook Page: <label style="font-size:12px">(optional)</label>
                  <input type="text" class="form-control" ng-model="media.fb" name="fb" id="fb" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" placeholder="http://" ng-disabled="loadingg == true">
                  <label ng-show="form.fb.$invalid && !form.fb.$pristine && form.fb.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group" id="chosen">
                Tags <label class="req">*</label> <input type="text" id="tags" style="opacity:0;">
                <ui-select multiple tagging tagging-label="false" ng-model="media.tag" name="tag" id="tag" theme="bootstrap" ng-disabled="disabled">
                  <ui-select-match placeholder="Select Tags" style="padding-left:5px;">{[{ $item }]}</ui-select-match>
                  <ui-select-choices repeat="ta in tag">{[{ ta }]}</ui-select-choices>
                </ui-select>
                <label ng-show="t && (media.tag=='' || !media.tag)" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group" id="chosen">
                Category <label class="req">*</label> <input type="text" id="cats" style="opacity:0;">
                <select chosen multiple options="category" class="form-control m-b" ng-options="cat as cat.categoryname for cat in category track by cat.categoryid" ng-model="media.category" name="category" id="category">
                </select> 
                <label ng-show="cat && (media.category=='' || !media.category)" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">

                <alert ng-repeat="resultget in resultget" type="{[{resultget.type }]}" close="closeAlert1($index)">
                  <span class="" ng-bind="resultget.msg"></span>            
                  <div ng-repeat="cNTS in cNTS">
                    - <span ng-bind="cNTS.msg"></span><br>
                  </div>
                </alert>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="row">               
                  <div class="col-sm-12 create-proj-thumb" ng-if="media.videotype == 'embed'">                  
                    <div class="vidoe-width">
                      <label>Video</label> <br> 
                      <div ng-bind-html="preview"></div>
                    </div>
                  </div>               
                  <div class="col-sm-6 create-proj-thumb" ng-show="media.videotype == 'upload'"> 
                      <label>Video</label> <br> 
                      <div class="vidoe-play-ml">
                        <div id="myElement">Loading the player...</div>
                      </div>
                  </div>
                  <div class="col-sm-6 create-proj-thumb" ng-show="media.videotype == 'upload'">
                    <label>Thumbnail</label> <br> 
                    <alert ng-repeat="videoAlerts in videoAlerts" type="{[{videoAlerts.type }]}" close="closeAlert($index)">{[{ videoAlerts.msg }]}</alert>
                    <img class="vidoe-play-ml" ngf-src="thumbImg" ng-if="imageselected == true">
                    <img class="vidoe-play-ml" ng-src="{[{ amazonlink }]}/videos/thumbs/{[{media.videothumb}]}" ng-if="imageselected == false">
                  </div>  
                  <div class="col-sm-12 create-proj-thumb">
                    <div class="line line-dashed b-b line-lg"></div>
                  </div> 
                  <div class="col-sm-12 create-proj-thumb" ng-show="media.videotype == 'upload'"> 
                    <div class="label_profile_pic pointer-wrap" id="change-video" ngf-change="prepareThumb(filesT)" ngf-select ng-model="filesT" ngf-multiple="false" required="required">
                      <img class="video-icon" src="/images/icons/thumbnail-icon.png"><br>
                      <label>Click here to change video thumbnail</label>
                      <br>
                    </div>
                  </div>  
                </div>
                <div class="line line-dashed b-b line-lg"></div>
              </div>
              

              <div class="form-group">
              <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
               <span ng-bind="alert.msg"></span> </alert>
                <div class="col-md-12 center" ng-show="loadingg == true">
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                  <span ng-bind="loadingMsg"></span>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                  <button type="submit" class="btn btn-info" ng-if="media.status != 'Disapproved'" ng-click="save(media,'1',thumbImg,videoname)">Save Video</button>
                  <button type="submit" class="btn btn-info" ng-if="media.status == 'Disapproved'" ng-click="save(media,'2',thumbImg,videoname)">Resubmit Video</button>
                </footer>
              </div>
            </div>

          </div>
        <!-- </div>
      </div> -->
    </div>
  </div>
</div>