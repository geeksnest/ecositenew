<div class="container-fluid" ng-controller="mymedialistCtrl">
  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">LEARN</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary/mymedialibrary">My Media library</a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <h4>Videos</h4>
      </div>
      <div class="col-sm-12">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eu quam erat. Vivamus sed neque nec nibh aliquet euismod. Donec scelerisque facilisis nulla bibendum maximus.
      </div>
    </div>
    <div class="row">      
        <div class="panel-body">
          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <a class="btn btn-info btn-lg pull-right" href="/medialibrary/addvideo"> Add New Video</a>
        <br><br>
        <div class="line line-dashed b-b line-lg"></div>
      </div>
      <div class="col-sm-3" ng-repeat="data in oyaaa">
        <div class="proj-list-wrap">
          <a href="{[{ previewlink }]}/{[{ data.slugs }]}" title="Preview">
            <div ng-if="data.videotype == 'embed'" class="proj-thumb2" style="background-image: url({[{ data.embed | returnYoutubeThumb }]});"></div>
            <div ng-if="data.videotype == 'upload'" class="proj-thumb2"style="background-image: url({[{ amazonlink }]}/videos/thumbs/{[{data.videothumb}]});">
            </div>
          </a>
          <div class="row proj-content"> 
            <div class="col-sm-12">
              <a href="{[{ previewlink }]}/{[{ data.slugs }]}">
                <label class="proj-title1 proj-list-title pointer-wrap" ng-bind="data.title"></label>
              </a>
            </div>    
            <div class="col-sm-12">
              <p class="proj-font1 proj-list-desc" ng-bind="data.shortdesc"></p>
            </div>     
            <div class="col-sm-12"><br>
              Published : <span ng-if="data.datepublished !=null">{[{ data.datepublished | dateToISO | date:'mediumDate' }]}</span>
              <span ng-if="data.datepublished==null">----</span>
            </div>
            <div class="col-sm-12" >
              <i class="fa fa-heart text-danger"></i> <span ng-bind="data.likes"></span> Likes 
            </div>
            <div class="col-sm-12" >
              <i class="icon-eye"></i> <span ng-bind="data.viewscount"></span> Views 
            </div>
            <div class="col-sm-12"><br>
              <button class="btn btn-info btn-sm" ng-if="data.status=='Submitted'" ng-bind="data.status"></button>
              <button class="btn btn-info btn-sm" ng-if="data.status=='Resubmitted'" ng-bind="data.status"></button>
              <button class="btn btn-success btn-sm" ng-if="data.status=='Approved'" ng-bind="data.status"></button>
              <button class="btn btn-danger btn-sm" ng-if="data.status=='Disapproved'" ng-bind="data.status"></button>
              <button ng-if="data.showvideo=='Disabled'" title="This Video will not show" class="btn btn-default btn-sm disabled" ng-bind="data.showvideo"></button>
            </div>  
          </div>
          <div class="panel-footer bg-light lter">
              <ul class="nav nav-pills nav-sm">
                <li ng-if="data.showvideo == 'Disabled'"><a title="Enable | Show this Video" ng-click="showvideo(data.id,'Enabled')"><i class="fa fa-circle"></i></a></li>
                <li ng-if="data.showvideo == 'Enabled'"><a title="Disable | Hide this Video" ng-click="showvideo(data.id,'Disabled')"><i class="fa fa-circle-o"></i></a></li>
                <li><a href="{[{ mymedialink }]}/{[{ data.slugs }]}" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a></li>
                <li><a ng-click="deletevideo(data.id)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a></li>
              </ul>
            </div> 
        </div>
      </div>
    </div>
    <div class="load-more-div">
      <button ng-hide="loading" class="proj-list-show-more" ng-click="loadmorebot()" ng-disabled="loading"><span>Load More</span>
      </button>
      <div ng-show="loadingGIF" class="load-more-loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    </div>
  </div>
</div>