<a href="" id="errorget"></a>
<a href="" id="successget"></a>
<div class="container-fluid" ng-controller="addvideoCtrl">

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">Learn</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary/mymedialibrary">My Media library</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="">Add video</a> 
      </div>
    </div>
  </div>

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h4>Submit Your Video</h4>
      </div>
      <div class="col-sm-12">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eu quam erat. Vivamus sed neque nec nibh aliquet euismod. Donec scelerisque facilisis nulla bibendum maximus.
      </div>
    </div>
    <div class="create-form">  
      <!-- <div class="row">  

        <div class="col-sm-8">  -->

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-danger" ng-show="errorget">
                <span class="" ng-bind="errorget"></span>
              </div>
              <div class="form-group wrapper-sm bg-success" ng-show="successget">
                <span class="" ng-bind="successget"></span>
              </div>
              <alert ng-repeat="resultget in resultget" type="{[{resultget.type }]}" close="closeAlert1($index)">
                <span class="" ng-bind="resultget.msg"></span>            
                <div ng-repeat="cNTS in cNTS">
                  - <span ng-bind="cNTS.msg"></span><br>
                </div>
              </alert>

              <div class="form-group">
                Video type <label class="req">*</label>  
                <div class="radio">
                  <label class="i-checks">                
                    <input type="radio" name="type" value="upload" ng-model="media.type" id="type" name="type" ng-checked="media.type == 'upload'"><i></i> Upload
                  </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label class="i-checks">
                    <input type="radio" name="type" value="embed" ng-model="media.type" id="type" name="type" ><i></i> Embed
                  </label>
                </div>
              </div>
              <div ng-show="media.type == 'embed'">
                <div class="form-group">
                  <textarea class="form-control textarea" rows="2"  id="embed" name="embed" ng-model="media.embed" ng-change="youtube(media.embed)"></textarea>                
                  <div class="line line-dashed b-b line-lg"></div>
                  <div ng-bind-html="preview"></div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
              </div>
              <div ng-show="media.type == 'upload'">
                <div class="form-group">
                  <div class="row"> 
                    <div class="col-sm-12 create-proj-thumb">
                      <alert ng-repeat="videoAlerts in videoAlerts" type="{[{videoAlerts.type }]}" close="closeAlert($index)">{[{ videoAlerts.msg }]}</alert>
                    </div>                     
                    <div class="col-sm-12 propic create-proj-thumb" ng-hide="hideBig">
                      <div class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="change-video" ngf-change="prepare(files)" ngf-select ngf-drag-over-class="'dragover'" ng-model="files" ngf-multiple="false" accept='video/mp4'>
                        <img class="video-icon" src="/images/icons/video-icon.png"><br>
                        <label>Choose a video from your computer</label> | <label>MP4 only</label> | <label>Maximun of 20MB</label>
                        <br>
                      </div>
                    </div>
                    <div class="col-sm-6 create-proj-thumb" ng-show="videoToUpload">
                      <div class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="change-video" ngf-change="prepare(files)" ngf-select ngf-drag-over-class="'dragover'" ng-model="files" ngf-multiple="false" accept='video/mp4'>
                        <img class="video-icon" src="/images/icons/video-icon.png"><br>
                        <label>Choose a video from your computer</label> | <label>MP4 only</label> | <label>Maximun of 20MB</label>
                        <br>
                      </div>
                      <label>Video</label>  
                      <div class="line line-dashed b-b line-lg"></div>
                      <div id="video">
                        <video class="vidoe-play-ml" controls ngf-src="videoToUpload"></video>
                      </div>                      
                      <div class="line line-dashed b-b line-lg"></div>
                    </div> 
                    <div class="col-sm-6 create-proj-thumb" ng-show="videoToUpload">
                      <div class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="thumbnail" ngf-change="prepareThumb(filesT)" ngf-select ng-model="filesT" ngf-multiple="false" accept='image/jpeg,image/png,image/gif'>
                        <img class="video-icon" ng-st src="/images/icons/thumbnail-icon.png"><br>
                        <label>Choose a image from your computer</label>
                        <br>
                      </div>
                      <label>Thumbnail</label>
                      <div class="line line-dashed b-b line-lg"></div>
                      <div ng-show="imageselected == true">
                        <!-- <div class="vidoe-play-ml" ngf-background="thumbImg" ></div> -->
                        <img class="vidoe-play-ml" ngf-src="thumbImg">                     
                        <div class="line line-dashed b-b line-lg"></div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>
              <div ng-if="showForm">
                <div class="form-group">
                  <input type="hidden" ng-init="media.authorid = userAgent" ng-model="media.authorid"/>
                  Title <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="media.title" name="title" id="title" required/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  Short Description <label class="req">*</label>
                  <textarea class="form-control textarea" rows="4" ng-model="media.shortdesc" name="shortdesc" id="shortdesc" maxlength="200"></textarea>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  Description <label class="req">*</label>
                  <div ckeditor="options" ng-model="media.description" name="description" id="description" ready="onReady()"></div>
                  <!-- <textarea class="ck-editor" ng-model="media.description" name="description" id="description"></textarea> -->
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group" id="chosen">
                  Tags <label class="req">*</label>
                  <ui-select multiple tagging tagging-label="false" ng-model="media.tag" name="tag" id="tag" theme="bootstrap" ng-disabled="disabled">
                  <ui-select-match placeholder="Select Tags" style="padding-left:5px;">{[{ $item }]}</ui-select-match>
                  <ui-select-choices repeat="ta in tag">{[{ ta }]}</ui-select-choices>
                </ui-select>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group" id="chosen">
                Category <label class="req">*</label>
                <select chosen multiple options="category" class="form-control m-b" ng-options="cat as cat.categoryname for cat in category track by cat.categoryid" ng-model="media.category" name="category" id="category">
                </select> 
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              

              <div class="form-group">
                <div class="col-md-12 center" ng-show="loadingg == true">
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                  <span ng-bind="loadingMsg"></span>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                  <button type="submit" class="btn btn-info" ng-disabled="loadingg == true" ng-click="save(media,videoToUpload,thumbImg)">Submit Video</button>
                </footer>
              </div>
            </div>
          </div>

        </div>
        <!-- </div>
      </div> -->
    </div>
  </div>
</div>