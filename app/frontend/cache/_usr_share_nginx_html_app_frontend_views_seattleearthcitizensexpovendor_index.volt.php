
<div class="donation-fluid interior-donation-seattle" style="position: relative;">
    <div id="interior" class="row eco-container">
        <!-- <div class="overlaywhite"> -->
    </div>
    <div class="banner-content-wrapper-green">
        <div style="color:#404040">
            <font class="mtitle2" style="font-size:42px;">NATURAL HEALING EXPO 2015</font> <br>
            <p class="mbody2" style="padding-bottom:0px;font-size:16px;">We will embrace our commonalities and celebrate our hope for a bright future through community sharing of environmentally friendly products, entertainment an interactive programs.</p>
        </div>
    </div>
</div>


<!--===begin container===-->
<div  ng-controller='SeattleVendorCtrl' class="container-fluid">
    <div class="eco-container" >
        <div class="wrapper-md " >
            <div class="row">
                <div class="col-md-12 panel-body" style="padding-top: 30px;" ng-hide="sucess_process == true || backtoform == true || processing == true">
                    <div class="gp">

                        <p class="donate-title2">WELCOME SEATTLE NATURAL HEALING EXPO VENDORS!<br/></p>
                        <p>
                            This year marks our first year of many to bring Natural Healing to the Puget Sound area!
                            <br/><br/>
                            Join us on Sunday, October 18th from 11am-5pm at the Lynnwood Convention Center.
                            <br/><br/>
                            Come share your planet-loving products and services to more than 300 like minded people from around the area.
                            <br/><br/>
                            We will provide 1 6-foot table and 2 chairs, plus sample surrounding space for any other displays you want to set up.  Please let us know in the "notes" section of your registration if you have any special requests for your booth, and we will do our best to accomodate you.  Please also let us know if you would like to have a booth against a wall on which you can tack a poster or banner.
                            <br/><br/>
                            Booths this year will be $50 for non-profit, $100 for-profit and an extra $56 for electricity. Each booth space will come with one table and two chairs. Table clothes are provided.
                            <br/><br/>
                            To register, please fill out the form below. Spaces are limited so please register early! Please contact us by email at <a href="mailto:seattle@earthcitizens.org">seattle@earthcitizens.org</a> if you have any questions.
                            <br/><br/>
                            Natural Healing Expo Registration Form
                            <br/>
                            Brought to you by Seattle Earth Citizens
                            <br/><br/> 
                            Date of Event: Sunday, Oct 18, 2015
                            <br/><br/>
                            Location: Lynnwood Convention Center 
                            <br/><br/>
                            <a href="<?php echo $this->config->application->baseURL; ?>/seattlenaturalhealingexpo"> Click here to purchase General Admission.</a>                                                    
                        </p>
                    </div>
                </div>
                <div class="col-md-9 panel-body" ng-hide="sucess_process == true || backtoform == true || processing == true">
                    <form name="formSeattleVendor" id="formSeattleVendor" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label class="control-label">Thank you for joining us! Please complete the following.</label>  
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label label-fr">Business or Organization Name *</label>
                                    <input type="text" id="orgname" name="orgname" class="form-control" ng-model="vendor.orgname" required="required">
                                </div>
                                <div class="form-group">                        
                                    <label class="control-label label-fr">What will you offer at the Expo? *</label>
                                    <input type="text" id="expooffer" name="expooffer" class="form-control" ng-model="vendor.expooffer" required="required">  
                                </div>
                                <div class="form-group">                        
                                    <label class="control-label label-fr">Contact Person *</label>
                                    <input type="text" id="contactperson" name="contactperson" class="form-control" ng-model="vendor.contactperson" required="required">  
                                </div>
                                <div class="form-group">  
                                    <div class="row">
                                        <div class="col-sm-5">                      
                                            <label class="control-label label-fr">Phone *</label>
                                            <input type="text" id="phone" name="phone" class="form-control" ng-model="vendor.phone" required="required" only-digits>
                                        </div>
                                        <div class="col-sm-7">
                                            <label class="control-label label-fr">Email * <span class="label-fr bg-danger" ng-if="invalidemail == true">Invalid email address. Ex: name@example.com</span></label>
                                            <input type="text" id="email" name="email" class="form-control" ng-model="vendor.email" ng-blur="emailcheck(vendor)" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                        
                                    <label class="control-label label-fr">Address *</label>
                                    <input type="text" id="address" name="address" class="form-control" ng-model="vendor.address" required="required">  
                                </div>
                                <div class="form-group">                        
                                    <label class="control-label label-fr">Please let us know if you have any special requests for your booth, and we will do our best to accomodate you.</label>
                                    <textarea class="form-control" ng-model="vendor.note" style="resize:vertical;"></textarea>
                                </div>
                                <div class="form-group">                        
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input type="radio" name="type" value="Profit" ng-model="vendortype" ng-change="selecttype(vendor)"><i></i> Profit &nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label class="i-checks">
                                            <input type="radio" name="type" value="Non-Profit" ng-model="vendortype" ng-change="selecttype(vendor)"><i></i> Non-Profit
                                        </label>
                                    </div> 
                                </div>


                                <div ng-if="vendortype == 'Profit'">                        
                                    <div class="form-group">                        
                                        <label class="control-label label-fr">Number of Tables required * <em class="text-muted">($100 per table)</em> </label>
                                        <input type="text" id="table" name="table" class="form-control" ng-model="vendor.table" ng-change="computepayment(vendor,vendortype, electricity)" required="required" only-digits>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label label-fr">Electricity <em class="text-muted">(add an additional $56)</em> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>  
                                        <label class="i-checks">
                                            <input type="radio" name="electricity" value="yes" ng-model="electricity" ng-change="addelectricity(vendor,electricity, 'yes', vendortype)" required="required"><i></i> Yes &nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label class="i-checks">
                                            <input type="radio" name="electricity" value="no" ng-model="electricity" ng-change="addelectricity(vendor,electricity, 'no', vendortype)" required="required"><i></i> No
                                        </label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label label-fr">Total Payment</label>
                                        <input type="hidden" id="totalpayment" name="totalpayment" class="form-control" ng-model="vendor.totalpay" required="required">
                                        <input type="text" id="totalpayment" name="totalpayment" class="form-control" ng-model="vendor.totalpayment" required="required" disabled>
                                    </div>
                                </div>

                                <div ng-if="vendortype == 'Non-Profit'">                        
                                    <div class="form-group">                        
                                        <label class="control-label label-fr">Number of Tables required * <em class="text-muted">($50 per table)</em> </label>
                                        <input type="text" id="table" name="table" class="form-control" ng-model="vendor.table" ng-change="computepayment(vendor,vendortype, electricity)" required="required" only-digits>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label label-fr">Electricity <em class="text-muted">(add an additional $56)</em> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> 
                                        <label class="i-checks">
                                            <input type="radio" name="electricity" value="yes" ng-model="electricity" ng-change="addelectricity(vendor,electricity, 'yes', vendortype)" required="required"><i></i> Yes &nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label class="i-checks">
                                            <input type="radio" name="electricity" value="no" ng-model="electricity" ng-change="addelectricity(vendor,electricity, 'no', vendortype)" required="required"><i></i> No
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label label-fr">Total Payment</label>
                                        <input type="hidden" id="totalpayment" name="totalpayment" class="form-control" ng-model="vendor.totalpay" required="required">
                                        <input type="text" id="totalpayment" name="totalpayment" class="form-control" ng-model="vendor.totalpayment" required="required" disabled>
                                    </div>
                                </div>

                                <div ng-if="vendortype">
                                    <div class="form-group"> 
                                        <label class="i-checks">
                                            <input type="radio" name="mailus" value="no" ng-model="mailus" ng-click="selectpaymethod('no')"><i></i> Pay Online &nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label class="i-checks">
                                            <input type="radio" name="mailus" value="yes" ng-model="mailus" ng-click="selectpaymethod('yes')"><i></i> Mail Check
                                        </label>
                                    </div>
                                    
                                    <div class="form-group" ng-if="mailus == 'yes'">
                                        <div class="row" style="padding-left:15px;">
                                            <div class="col-md-12">
                                                <label class="control-label label-fr">Please make checks payable to: ECO</label><br>
                                                <label class="control-label label-fr">Mail it to: 10702 NE 68th St, Kirkland, WA 98033</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="button" name="" id="abouteco" value="Submit" ng-click="submitvendor(vendor,vendortype,electricity)" class="btn btn-info" ng-disabled="formSeattleVendor.$invalid" style="float:right;"/>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="form-group" ng-if="mailus == 'no'">
                                        <div class="panel-heading">
                                            <label class="control-label bold">Payment Form</label>  
                                        </div>
                                        Payment Method:
                                        <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                                            <option value='CreditCard'>Credit Card</option>
                                            <option value='ach2'>Checking/Savings Account</option>
                                            <option value='paypal2'>PayPal</option>
                                        </select>
                                        <!-- CREDIT CARD BILLING-->
                                         <div ng-if="typetransact2 == 'CreditCard'" class="creditcard">
                                           <br/>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    <img src="/images/template_images/visa.png" style="width:65px;">
                                                    <img src="/images/template_images/amex.png" style="width:65px;">
                                                    <img src="/images/template_images/mastercard.png" style="width:65px;">
                                                    <img src="/images/template_images/discover.png" style="width:65px;">
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Credit Card Number: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="text" ng-model="cc.ccn" name="" required="required" only-digits />
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    CVV Number: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control " ng-model="cc.cvvn" name="" required="required" only-digits />
                                                </div>
                                                <div class="col-md-4">                            
                                                </div>
                                                <div class="col-md-8">

                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Credit Card Expiration: *
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <span>Month</span><br/>
                                                            <select ng-model="cc.expiremonth" class="form-control" required="required">
                                                                <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                                                                <?php foreach ($formonths as $index => $formonth) { 
                                                                    echo "<option value='".$index."'>".$formonth."</option>";
                                                                }?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span>Year</span><br/>
                                                            <select ng-model="cc.expireyear" class="form-control" required="required">
                                                                <?php for ($year=date('Y'); $year < 2050; $year++) { 
                                                                    echo "<option value='".$year."'>".$year."</option>";
                                                                }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-12">
                                                    <span style="font-weight:bold;">Billing Information</span>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    First Name: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.billingfname" name="billingfname" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Last Name: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.billinglname" name="billinglname" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    Address Line 1: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.al1" name="al1" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    Address Line 2:
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.al2" name="al2"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    City: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.city" name="city" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    State:
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.state" name="city"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    ZIP/Postal Code: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="cc.zip" name="zip" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    Country: *
                                                </div>
                                                <div class="col-md-8">
                                                    <select ng-model="cc.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required="required">
                                                    </select>
                                                </div>
                                            </div>                            
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Email Address: *
                                                </div>
                                                <div class="col-md-8">
                                                    <span class="label-fr bg-danger" ng-if="ccinvalidemail == true">Invalid email address. Ex: name@example.com</span>
                                                    <input type="email" class="form-control" ng-model="cc.ccemail" name="email" ng-blur="ccemailcheck(cc)" required/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                How did you learn about the Natural Healing Expo 2015 in Seattle?
                                                <select class="form-control m-b" ng-model="cc.howdidyoulearn" ng-change="changeme()">
                                                    <option value="ECO event">ECO event</option>
                                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                                    <option value="Invitation Email">Invitation Email</option>
                                                </select>
                                            </div>
                                            <div class="form-group" ng-if="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                                Center Name
                                                <select class="location form-control m-b" ng-model="cc.cname" required="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                                </select>
                                            </div> 
                                            <div class="form-group">                                            
                                                <input type="button" name="" value="Submit" ng-click="submitvendorcc(vendor,cc,vendortype,electricity)" class="btn btn-info" ng-disabled="formSeattleVendor.$invalid" style="float:right;"/>
                                            </div> 
                                        </div>
                                        <!-- END FOR CREDIT CARD-->

                                        <!-- CHECK Authorize-->
                                        <div ng-if="typetransact2 == 'ach2' " class="ach" >
                                            <br/>

                                            <div class="form-group row" >
                                                <div class="col-md-4"> 
                                                    Account Holder Name: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="check.accountname" name="accountname" required/>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4"> 
                                                    Your bank name: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="check.bankname" name="bankname" required/>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-12"> 
                                                    Enter the routing code and account number as they appear on the bottom of your check:
                                                </div>
                                                <div class="col-md-12">
                                                    <img src="/images/template_images/checksample.png">
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4"> 
                                                    Bank Routing Number: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.bankrouting" name="bankrouting" required only-digits/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"> 
                                                    Bank Account Number: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.bankaccountnumber" name="bankaccountnumber" required only-digits />

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"> 
                                                    Select Account Type:
                                                </div>
                                                <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                                                    <input type="radio" name="at" ng-model="check.at" value="CHECKING" required="required">&nbsp;&nbsp;Checking account<br/>
                                                    <input type="radio" name="at" ng-model="check.at" value="BUSINESSCHECKING" required="required">&nbsp;&nbsp;Business checking account<br/>
                                                    <input type="radio" name="at" ng-model="check.at" value="SAVINGS" required="required">&nbsp;&nbsp;Savings account
                                                </div>
                                            </div>

                                            <div class="form-group row" >
                                                <div class="col-md-12"> 

                                                    <input type="checkbox"  ng-model="check.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'" required>*

                                                    <span style="text-align: center;">
                                                        By entering my account number about and Clicking authorize, I authorize my payment to be processed 
                                                        as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service 
                                                        provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account. 
                                                        <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these 
                                                        authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group row" >
                                                <div class="col-md-12">
                                                    <span style="font-weight:bold;">Billing Information</span>
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    First Name: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.billingfname" name="billingfname" required />

                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Last Name: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.billinglname" name="billinglname" required />

                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Address Line 1: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.al1" name="al1" required />

                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Address Line 2:
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="check.al2" name="al2" />
                                                </div>
                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    City: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.city" name="city" required />

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    State:
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="check.state" name="city" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    Province:
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control " type="text" ng-model="check.province" name="province" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    ZIP/Postal Code: *
                                                </div>
                                                <div class="col-md-8">

                                                    <input class="form-control " type="text" ng-model="check.zip" name="zip" required />

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    Country: *
                                                </div>
                                                <div class="col-md-8">

                                                    <select ng-model="check.country" ng-init="check.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'" required="">

                                                    </select>
                                                </div>
                                            </div>                           
                                            <div class="form-group row" >
                                                <div class="col-md-4">
                                                    Email Address: *
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
                                                    <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                How did you learn about the Natural Healing Expo 2015 in Seattle?
                                                <select class="form-control m-b" ng-model="check.howdidyoulearn" ng-change="changeme()">
                                                    <option value="ECO event">ECO event</option>
                                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                                    <option value="Invitation Email">Invitation Email</option>
                                                </select>
                                            </div>
                                            <div class="form-group" ng-if="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                                Center Name
                                                <select class="location form-control m-b" ng-model="check.cname" required="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                                </select>
                                            </div>

                                            <div class="form-group">                                            
                                                <input type="button" name="" value="Submit" ng-click="submitvendorecheck(vendor,check,vendortype,electricity)" class="btn btn-info" ng-disabled="formSeattleVendor.$invalid" style="float:right;"/>
                                            </div> 
                                        </div>
                                        <!-- END of CHECK Authorize For Donate Part-->

                                        <!-- Paypal Form Payment -->                  
                   
                                        <div  ng-if="typetransact2 == 'paypal2' ">
                                            <br/>
                                            <!-- <form class="paypalform" name="paypalform" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypalsingle"> -->

                                                <div style="clear:both; ">
                                                    <input type="hidden" name="cmd" value="_donations">
                                                    <input type="hidden" name="business" value="accounting@earthcitizens.org">
                                                    <input type="hidden" name="lc" value="US">
                                                    <input type="hidden" name="custom" id="paypalcustom" ng-model="paypalcustom" value="">
                                                    <input type="hidden" name="item_name" value="Seattle Natural Healing Expo Vendors!">
                                                    <input type="hidden" name="amount" ng-value="vendor.totalpayment">
                                                    <input type="hidden" name="currency_code" value="USD">
                                                    <input type="hidden" name="no_note" value="0">
                                                    <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                                                </div>
                                                <div class="form-group">
                                                    How did you learn about the Natural Healing Expo 2015 in Seattle?
                                                    <select class="form-control m-b" ng-model="paypal.howdidyoulearn" ng-change="changeme()">
                                                        <option value="ECO event">ECO event</option>
                                                        <option value="ECO Program Graduates">ECO Program Graduates</option>
                                                        <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                                        <option value="Invitation Email">Invitation Email</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" ng-if="paypal.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                                    Center Name
                                                    <select class="location form-control m-b" ng-model="paypal.cname" required="paypal.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                                        <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                                    </select>
                                                </div>


                                                <div class="form-group">
                                                    <a href="" ng-click="submitvendorpaypal(vendor, vendortype, paypal.howdidyoulearn, paypal.cname)" ng-hide="formSeattleVendor.$invalid"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="paypalbutton" alt="PayPal - The safer, easier way to pay online!" style="float:right;"></a>
                                                </div>

                                            <!-- </form> -->

                                        </div>
                                        <!-- END of Paypal -->
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="col-md-12 panel-body" ng-show="processing == true">
                    <div class="panel panel-default">
                        <div class="form-group padding20">
                            <label class="control-label center">Processing your payment please wait.....</label>
                        </div>
                    </div>
                </div>
 
                <div class="col-md-12" ng-show="backtoform == true">
                <div class="padding20 error-bg">
                        <label class="control-label">{[{gateway_error}]}<br> <a href="" ng-click="back_click()">Back to form </a></label>
                    </div>
                </div>

                <div class="col-md-12" ng-show="sucess_process == true">
                    <div class="padding20 sucess-bg">
                        <label class="control-label">{[{sucess_msg}]}</label>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:16px;color:#777;">
                        <p>
                            ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
                        </p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>




