


<div class="donation-fluid interior-headerBanner2" ng-controller='DonateCtrl' >
  <div id="interior" class="row eco-container">
    <div class="col-lg-8 donation-banner" style="margin-top:0px;">
      <div class="banner-content-wrapper2" style="padding: 4%;">
        <div id="donation-banner-title" class="mtitle" style="font-size: 35px; line-height: 35px; padding-bottom:5px;">
          BE A PART OF THE <b>CHANGE</b> <br>
          <span style="font-size: 28px; " id="sub-content">that benefits people and the planet.</span>
        </div>
        <div class="mbody">
          <span>Your donation is the primary means that allows ECO to fulfills its mission. Start to be the change now.</span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 donation-banner1" style="margin-top:10px;">
      <div class="textalign-right">
        <div class="headtext13">THANK YOU TO THE </div>
        <div class="headtext2"><span  ng-bind="userscount"></span></div> <!-- Only display ZERO  -->
        <div class="headtext21">EARTH CITIZENS</div>
        <div class="headtext13">WHO HAVE DONATED TO ECO</div>
      </div>
      </div>
  </div>
</div>


<!--===begin container===-->
<div  ng-controller='DonateCtrl' class="container-fluid"  style="margin-top:30px;">
  <div class="eco-container" >
    <div class="wrapper-md " >
      <div class="row">

        <div class="col-md-6">

          <div class="form-group col-lg-12"  style="padding:0px;margin-top:0px;">

            <span class="donate-title2">DONATE ONLINE</span><br/><br/>
            <div class="row">

              <div class="form-group col-sm-4">
                <span style="font-size:16px;">
                  Donation Amount:<br/>
                </span>
                <span style="font-size:12px;">
                  (Minimum $10.00)
                </span>

              </div>
              <div class="form-group col-sm-5">
                <div class="input-group m-b">
                  <span class="input-group-addon h big-num">$</span>
                  <input type="text" class="form-control h big-num" id="donateamount" name="donateamount" value="10.00" ng-model="donateamount" ng-change="change(amount)" only-digits>
                </div>
              </div>
              <div class="form-group col-sm-12" ng-show="donateamount < 10">
                <p style="color:#fd5555;font-size:15px;">Please a minimum of $10 for your donation.</p>
              </div>
            </div>

            <span class="donate-title1" style="color:#000000;">Personal Information</span><br/><br/>

            <div>
              <div>
                <input type="radio" name="donate" id="donate" ng-model="new" ng-checked='true' />&nbsp;&nbsp;New Account<br>
                <input type="radio" name="donate" id="donate2" ng-model="new" value='already' />&nbsp;&nbsp;I already have an account<br>
                <input type="radio" name="donate" id="donate2" ng-model="new" value='woreg' />&nbsp;&nbsp;Donate without Registration<br>
              </div>
            </div>

          </div>


          <div class="alreadyhaveaccount" ng-show="new == 'already'">
            <a id="signup"></a>
            <p style="color:#528ac2;"ng-show="statusmessage != ''" ng-bind="statusmessage"></p>
            <label ng-show="process1 == true"  style="color:#777;font-size:15px;">Checking your info...</label> <br/>
            <form ng-show="paypalformshow1 == {[{ disp}]}" name="formlogin" novalidate>

              <div class="col-lg-12" style="padding:0px;">
                <div class="form-group">
                  <span class="formerror col-lg-12" ng-show="errormessage != ''"  style="padding:0px;color:#fd5555;font-size:15px;" ng-bind="errormessage"></span>
                </div>
                <div class="form-group">
                  <input class="form-control " type="text" name="email" ng-model="userd.email" name="email" required placeholder ="Email Address" />
                </div>
                <div class="form-group">
                  <input class="form-control " type="password" name="password" ng-model="userd.password" name="password" required placeholder ="Password" />
                  <a ng-show="process == true"  style="color:#777;font-size:15px;">Checking Credentials</a>
                </div>

                <div style="float:right;">
                  <a href="" ng-click="setEmailFunc('createPassword')"style="color:#777;font-size:12px;"><u>Forgot your password?</u></a>
                  <a href="" ng-click="setEmailFunc('resendActivation')"  style="color:#777;font-size:12px;"><u>Resend Activation Link?</u></a>
                </div>
              </div>
              <div class="col-lg-12" style="padding:0px;margin-top:10px;">
                <button type="submit" ng-click="clickDonate(userd)"  class="danate-button">Donate Now</button>
              </div>
            </form>
            <form name="sendemailform" ng-show="showemail == true && statusmessage == ''">
              <div class="inputcont">
                <div style="margin-top:-20px;">
                  <p ng-show="emailPurpose == 'resendActivation'" style="color:#777;font-size:15px;">You activation link will be sent yo your email.</p>
                  <p ng-show="emailPurpose == 'createPassword'" style="color:#777;font-size:15px;"> Your new password will be sent to your email.</p>

                </div>
                <input class="form-control " type="text" name="email" ng-model="send.email" name="sendemail" required placeholder ="Enter your Email Address" /><br/>
                <input type="button" class="btn m-b-xs w-xs btn-info" value="Send" ng-click="sendemail(send)" ng-hide="sendemailform.$invalid" style="padding:5px;"/>
                <input type="button" class="btn m-b-xs w-xs btn-info" value="Cancel" ng-click="hideForm()" style="padding:5px;" />
              </div>
            </form>
            <div>
              <div ng-show="membername">
                <p  style="color:#528ac2;font-size:15px;margin-bottom:0px; font-size: 30px;">
                  Welcome back <strong><span style="text-transform: capitalize;" ng-bind="membername"></span></strong>!
                </p>
                <p style="font-size:14px;margin-bottom:0px;" ng-show="reccur_success==1">
                  Please enter submit your payment information below to complete your donation.
                </p>
              </div>
              <div class="form-group row" style="padding-left:15px;padding-top:30px;">
                <p style="font-size:14px;margin-bottom:0px;" ng-show="reccur_success==2"> Your transaction is now being processed. </p>
                <p style="font-size:16px;margin-bottom:0px;" ng-show="reccur_success==3"><span ng-bind="reccur_message"></span><br/><br/>
                  <a href="" ng-click="back_click()" ng-show="back_button==2 || back_button==3" >Back to form </a>
                </p>
              </div>
            </div>
            <div ng-show="paypalformshow == true">
              <div style="font-size: 15px; padding:15px 0px 10px 0px; color:#333;" ng-show="reccur_success==1">
                <div style="margin-bottom:10px;">
                  <div class="form-group row" style="padding-left:15px;">
                    <div class="col-md-4" style="padding:0px;">
                      Payment Method:
                    </div>
                    <div class="col-md-8">
                      <select name="typetransact" class="form-control" ng-model="typetransact" ng-show="paypalformshow == true">
                        <option value=''>Credit Card</option>
                        <option value='ach'>Checking/Savings Account</option>
                        <option value='paypal'>PayPal</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row" style="margin:20px 0px 0px 120px;">
                  <p  ng-show="donateamount < 10" style="color:#fd5555;font-size:15px;">Please a minimum of $10 for your donation.</p>
                </div>
              </div>
            </div>


            <!-- CREDIT CARD REOCCUR BILLING  -->
            <form ng-hide="typetransact == 'creditcardreccur' || typetransact == 'single' || typetransact == 'reccur' || typetransact == 'ach' || typetransact == 'paypal' || reccur_success==3 || reccur_success==2" ng-if="creditcard123" class="creditcard" ng-submit="submitcredit(usercc, donateamount, userd.email)">
              <br/>

              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">

                </div>
                <div class="col-md-8">
                  <img src="/images/template_images/visa.png" style="width:65px;">
                  <img src="/images/template_images/amex.png" style="width:65px;">
                  <img src="/images/template_images/mastercard.png" style="width:65px;">
                  <img src="/images/template_images/discover.png" style="width:65px;">
                </div>
              </div>
              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                  Credit Card Number: *
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="usercc.ccn" name="" required only-digits />
                </div>
              </div>
              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                  CVV Number: *
                </div>
                <div class="col-md-8">
                  <input type="text" class="form-control " ng-model="usercc.cvvn" name="" required only-digits />
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                </div>
              </div>
              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                  Credit Card Expiration: *
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-xs-6">
                      <span>Month</span><br/>
                      <select ng-model="usercc.expiremonth" class="form-control">
                        <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                        <?php foreach ($formonths as $index => $formonth) {
                          echo "<option value='".$index."'>".$formonth."</option>";
                        }?>
                      </select>
                    </div>
                    <div class="col-xs-6">
                      <span>Year</span><br/>
                      <select ng-model="usercc.expireyear" class="form-control">
                        <?php for ($year=date('Y'); $year < 2050; $year++) {
                          echo "<option value='".$year."'>".$year."</option>";
                        }?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-12">
                  <span style="font-weight:bold;">Billing Information</span>
                </div>
              </div>

              <div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    First Name: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="usercc.billingfname" name="billingfname" ng-required="!usercc.bi" />
                  </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    Last Name: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="usercc.billinglname" name="billinglname" ng-required="!usercc.bi" />
                  </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    Address Line 1: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="usercc.al1" name="al1" ng-required="!usercc.bi" />
                  </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    Address Line 2:
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="usercc.al2" name="al2" />
                  </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    City: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="usercc.city" name="city" ng-required="!usercc.bi" />
                  </div>
                </div>
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-4">
                    State:
                  </div>
                  <div class="col-md-8">
                   <input class="form-control " type="text" ng-model="usercc.state" name="city" />
                 </div>
               </div>
               <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                  ZIP/Postal Code: *
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="usercc.zip" name="zip" ng-required="!usercc.bi" />
                </div>
              </div>
              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-4">
                  Country: *
                </div>
                <div class="col-md-8">
                  <select ng-model="usercc.country"  ng-init="usercc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="!usercc.bi">
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-12">
                <span style="margin-left:0px;"><input type="checkbox" name="recurpayment" ng-model="usercc.recurpayment" value="recurpayment"></span>
                <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
              </div>
            </div>

            <div ng-if="usercc.recurpayment">
              <div class="form-group row" style="padding-left:15px;" ng-hide="true">
                <div class="col-md-4">
                  Start Date: *
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-4">
                      <span>Year</span><br/>
                      <select ng-init="usercc.start_year = year[0]" ng-model="usercc.start_year" class="form-control" ng-required="usercc.recurpayment" style="padding:3px;">
                        <option ng-repeat="year in donyear track by $index" value="{[{year.val}]}">{[{year.val}]}</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <span>Month</span><br/>
                      <select ng-init="usercc.start_month = month[<?php echo date("m") - 1; ?>]" ng-model="usercc.start_month" class="form-control" ng-required="usercc.recurpayment" style="padding:3px;">
                        <option ng-repeat="month in donmonth track by $index" value="{[{month.val}]}">{[{month.name}]}</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <span>Day</span><br/>
                      <select ng-init="usercc.start_day = day[<?php echo date("d") - 1; ?>]" ng-model="usercc.start_day" class="form-control" ng-options="d.val for d in day" ng-required="usercc.recurpayment" style="padding:3px;">
                        <option>sdfgsdfg</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                <div class="col-md-4">
                  Select Duration: *
                </div>
                <div class="col-md-4">
                  <input type="text" class="hidden" ng-init="usercc.reccur_length = 1" name="reccur_length" ng-model="usercc.reccur_length" ng-required="usercc.recurpayment" />
                  <select class="form-control col-md-4" name="reccur_count" ng-model="usercc.reccur_count" ng-required="usercc.recurpayment">
                    <option value="">how many</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="9999">never end</option>
                  </select>
                </div>
                <div class="col-md-4">
                  months
                </div>

                <div class="col-md-8" ng-hide="true">
                  <select class="form-control col-md-8" ng-required="usercc.recurpayment" ng-model='usercc.reccur_unit' ng-init="usercc.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">

              </div>
              <div class="col-md-8" ng-hide="donateamount < 10">
                <button type="submit"  class="btn m-b-xs w-xs btn-info" style="float:right;">Donate</button>
              </div>
            </div>
          </form>
          <!-- END FOR CREDIT CARD-->

          <!-- CHECK Authorize-->
          <form  ng-show="typetransact == 'ach' && reccur_success!=3 && reccur_success!=2" class="ach" ng-submit="submitecheck(userecheck, donateamount, userd.email)">

            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Account Holder Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.accountname" name="accountname" ng-required="true"/>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Your bank name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.bankname" name="bankname" ng-required="true"/>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-12">
                Enter the routing code and account number as they appear on the bottom of your check:
              </div>
              <div class="col-md-12">
                <img src="/images/template_images/checksample.png">
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Bank Routing Number: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.bankrouting" name="bankrouting" ng-required="true" only-digits/>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Bank Account Number: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.bankaccountnumber" name="bankaccountnumber" ng-required="true" only-digits />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Select Account Type:
              </div>
              <div class="col-md-8" ng-init="userecheck.at = 'CHECKING'">
                <input type="radio" name="at" ng-model="userecheck.at" value="CHECKING">&nbsp;&nbsp;Checking account<br/>
                <input type="radio" name="at" ng-model="userecheck.at" value="BUSINESSCHECKING">&nbsp;&nbsp;Business checking account<br/>
                <input type="radio" name="at" ng-model="userecheck.at" value="SAVINGS">&nbsp;&nbsp;Savings account
              </div>
            </div>

            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-12">
                <input type="checkbox"  ng-model="userecheck.autorz" name="autorz" value="1" ng-required="true">*
                <span style="text-align: center;">
                  By entering my account number about and Clicking authorize, I authorize my payment to be processed
                  as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                  provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                  <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                  authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                </span>
              </div>
            </div>

            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-12">
                <span style="font-weight:bold;">Billing Information</span>
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                First Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.billingfname" name="billingfname" ng-required="true" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Last Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.billinglname" name="billinglname" ng-required="true" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Address Line 1: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.al1" name="al1" ng-required="true" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Address Line 2:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.al2" name="al2" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                City: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.city" name="city" ng-required="true" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                State:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.state" name="city" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Province:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.province" name="province" />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                ZIP/Postal Code: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheck.zip" name="zip" required />
              </div>
            </div>
            <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-4">
                Country: *
              </div>
              <div class="col-md-8">
                <select ng-model="userecheck.country" ng-init="userecheck.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
                </select>
              </div>
            </div>
            <!-- eCheck RECURR PAYMENT -->
            <!-- <div class="form-group row" style="padding-left:15px;">
              <div class="col-md-12">
                <span style="margin-left:0px;"><input type="checkbox" name="recurpayment2" ng-model="userecheck.recurpayment2" value="recurpayment2"></span>
                <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
              </div>
            </div> -->

            <div ng-if="userecheck.recurpayment2">
              <div class="form-group row" >
                <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                <div class="col-md-4">
                  Select Duration: *
                </div>
                <div class="col-md-4">
                  <input type="text" class="hidden" ng-init="userecheck.reccur_length = 1" name="reccur_length" ng-model="userecheck.reccur_length"/>
                  <select class="form-control col-md-4" name="reccur_count" ng-model="userecheck.reccur_count">
                    <option value="">how many</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="9999">never end</option>
                  </select>
                </div>
                <div class="col-md-4">
                  months
                </div>

                <div class="col-md-8" ng-hide="true">
                  <select class="form-control col-md-8" ng-required="userecheck.recurpayment" ng-model='userecheck.reccur_unit' ng-init="userecheck.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">

              </div>

              <div class="col-md-8">
                <button type="submit"  ng-hide="donateamount < 10" class="btn m-b-xs w-xs btn-info" style="float:right;">Donate</button>
              </div>
            </div>
          </form>
          <div  ng-show="typetransact == 'paypal' ">

            <div class="form-group row" ng-show="typetransact == 'paypal'">
              <div class="col-md-12">
                <span style="margin-left:0px;"><input type="checkbox" name="recurpayment3" ng-model="recurpayment3" value="recurpayment3"></span>
                <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
              </div>
            </div>

            <form  ng-hide="recurpayment3" class="paypalform" action="<?php echo $this->config->application->paypalURL;?>" method="post" target="_top" style="margin-left:0px; margin-top: 20px;">


              <div style="clear:both; ">

                <input type="hidden" name="cmd" value="_donations">
                <input type="hidden" name="business" value="<?php echo $this->config->application->paypalAccount;?>">
                <input type="hidden" name="lc" value="US">
                <input type="hidden" name="custom" ng-value="user.email">
                <input type="hidden" name="item_name" value="Earth Citizens Organization">
                <input type="hidden" name="amount" value="10.00" ng-value="donateamount">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="no_note" value="0">
                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                <div class="form-group row" style="padding-left:15px;">
                  <div class="col-md-12">
                    <input  type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" ng-hide="donateamount < 10" style="float:right;">
                  </div>
                </div>
              </div>

              <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>

            <form ng-show="recurpayment3" action="<?php echo $this->config->application->paypalURL;?>" method="post" style="margin-top: 20px; float: left;">
              <input type="hidden" name="cmd" value="_xclick-subscriptions">
              <!--You will need to set the email to your PayPal email or Secure Merchant ID -->
              <input type="hidden" name="custom" ng-value="user.email">
              <input type="hidden" name="business" value="<?php echo $this->config->application->paypalAccount;?>">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="item_name" value="Donate to Earth Citizen Organization">
              <input type="hidden" name="item_number" value="0000">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="no_shipping" value="2">
              <input type="hidden" name="src" value="1">
              <input type="hidden" name="p3" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHosted">


              <div class="form-group row" >
                <div class="col-md-12">
                  How many times would you like this to recur? (including this payment) <br><br>
                </div>
                <div class="col-md-4">
                  Select Duration: *
                </div>
                <div class="col-md-4">
                  <select name="srt" class="location form-control m-b">
                    <option value="0">Never End</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
                  <input type="hidden" name="a3" ng-value="donateamount">
                  <input type="hidden" name="t3" value="M">
                </div>
                <div class="col-md-4">
                  months <br><br>
                </div>
              </div>




              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-12">
                  <input   type="image" src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"  style="float:right;">
                  <img  alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </div>
              </div>
            </form>
          </div>
          <a id="causes"></a>

        </div>


        <!-- REGISTRATION PART -->

        <div ng-controller='registrationCtrl' class="newaccount" ng-hide="new == 'already' || new == 'woreg'" name="new-account" id="new-account">
          <form name="form" novalidate class="ng-pristine ng-valid" style="color:#777;fonts:0.8em sans-serif bold">
            <div class="form-group">
              First Name: *
              <input class="form-control " type="text"  ng-model="user.fname" name="firstname" required />
            </div>
            <div class="form-group">
              Last Name: *
              <input class="form-control " type="text" name="" ng-model="user.lname" name="lastname" required />
            </div>
            <div class="form-group">
              Username: *
              <span class="formerror" ng-show="usernametaken != ''" style="color: #fd5555"> Username has already been taken.</span>
              <input type="text" class="form-control " ng-model="user.username" name="username" id="username" onkeypress="return noSpace(event)" required />
            </div>
            <div class="form-group">
              Email Address: *
              <span class="formerror" ng-show="emailtaken == true" style="color: #fd5555">
                This email is already registered.
                <a href="" ng-click="chtli()" class="chtli" id="chtli">Click here to log in.</a>
              </span>
              <span class="formerror" ng-show="notyetconfirm == true" style="color: #fd5555">
                This email is already registered but has not been confirmed.
                <a href="" ng-click="chtr(user)" class="chtli" id="chtr">
                  Click here to resend confirmation email.
                </a>
              </span>
              <span class="formerror" ng-show="notyetcompleted == true" style="color: #fd5555">
                This email is already registered but has not been completed.
                <a href="" ng-click="chtrc(user)" class="chtli" id="chtr">
                  Click here to resend completion email.
                </a>
              </span>
              <span class="formerror" ng-if="invalidemail == true" style="color: #fd5555"><br/>
                This is not a valid email. Example: myname@example.com
              </span>
              <input type="email" class="form-control " ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
            </div>
            <div class="form-group">
              Password: *
              <span class="formerror" ng-show="passmin == true" style="color: #fd5555"> Shoud be minimum of 6 characters.</span>
              <input type="password" class="form-control " name="" ng-model="user.password" name="password" required />
            </div>
            <div class="form-group">
              Re-Type Password: *
              <span class="formerror" ng-show="comparepass == true" style="color: #fd5555">Your Password Should be Equal.</span>
              <input type="password" class="form-control " name="" ng-model="user.repassword" name="repassword" required ng-change="retypepass(user)"/>
            </div>

            <div class="form-group" style="margin-top: 10px;">

              <div style="padding-bottom:10px;">Date of birth: </div>

              <div class="row">

                <div class="col-md-3">
                  <span class="datelabel">Year: *</span><select ng-init="user.byear = year[0]" ng-model="user.byear" class="form-control" ng-options="y.val for y in year"></select>
                </div>
                <div class="col-md-1" style="margin-top: 25px;">/</div>

                <div class="col-md-2"style="margin-top: 25px;">
                 <span>(Optional)</span>
               </div>

               <div class="col-md-6">

                <div class="col-xs-6" style="padding-left:0px;">
                  <span class="datelabel">Month: </span>
                  <select ng-init="user.bmonth = month[0]" ng-model="user.bmonth" class="form-control" ng-options="m.val for m in month"></select>
                </div>
                <div class="col-xs-6" style="padding-right:0px;">
                  <span class="datelabel">Day:  </span>
                  <select ng-init="user.bday = day[0]" ng-model="user.bday"  class="form-control" ng-options="d.val for d in day"></select>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            Gender: *<br>
            <div class="radio">
              <label class="i-checks">
                <input class="radioinput" type="radio" value="male" ng-model="user.gender" name="3">
                <i></i>
                Male
              </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <label class="i-checks">
                <input class="radioinput" type="radio" value="female" ng-model="user.gender" name="4">
                <i></i>
                Female
              </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>

          </div>
          <div class="form-group">
            Locaton: *
            <select  ng-init="user.location = countries[229]" ng-model="user.location" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
            </select>
          </div>
          <div class="form-group">
            Zip Code *
            <input type="text" name="" class="form-control" ng-model="user.zipcode" name="zipcode" onkeypress="return isNumberKey(event)" required />
          </div>
          <div class="form-group">
            How did you learn about ECO?
            <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
              <option value="ECO event">ECO event</option>
              <option value="ECO Program Graduates">ECO Program Graduates</option>
              <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
              <option value="Invitation Email">Invitation Email</option>
            </select>
          </div>
          <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
            Center Name
            <select class="location form-control m-b" ng-model="user.cname" >
              <option value="NA">Not Applicable</option>
              <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
            </select>
          </div>
          <div class="form-group">
            I would like my friend to become an Earth Citizen as well.
            <span class="formerror" ng-show="form.refer.$error.email" style="color: #fd5555">
              This is not a valid email
            </span>
            <input type="email" class="form-control " ng-model="user.refer" name="refer" placeholder="Enter your friend's email address" />
            <span style="font-size: 10px;">
              Thank you for introducing ECO to your friend. ECO will send them an invitation email.
            </span>
          </div>


          <!--  DONATE PART FOR REGISTRATION -->

          <div class="reganddonate" style="margin-top:50px;">

            <div style="margin-bottom:10px;">

              <div class="form-group row">
                <div class="col-md-12">
                  <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/>
                </div>
                <div class="col-md-4">
                  Payment Method:
                </div>
                <div class="col-md-8">
                  <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                    <option value=''>Credit Card</option>
                    <option value='ach2'>Checking/Savings Account</option>
                    <option value='paypal2'>PayPal</option>
                  </select>
                </div>
              </div>

            </div>
            <div class="form-group row" style="margin:20px 0px 0px 120px;">
              <p  ng-show="donateamount < 10" style="color:#fd5555;font-size:15px;">
                Please a minimum of $10 for your donation.
              </p>
            </div>
            <!-- CREDIT CARD REOCCUR BILLING FOR DONATION PART -->
            <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
              <br/>
              <div class="form-group row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                  <img src="/images/creditcards/amex.png" width="49px" height="29px">
                  <img src="/images/creditcards/discover.png" width="49px" height="29px">
                  <img src="/images/creditcards/mastercard.png" width="49px" height="29px">
                  <img src="/images/creditcards/visa.png" width="49px" height="29px">
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Credit Card Number: *
                </div>
                <div class="col-md-8">
                  <input class="form-control" type="text" ng-model="userccreg.ccn" name="" ng-required="typetransact2 == ''" only-digits />
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  CVV Number: *
                </div>
                <div class="col-md-8">
                  <input type="text" class="form-control " ng-model="userccreg.cvvn" name="" ng-required="typetransact2 == ''" only-digits />
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Credit Card Expiration: *
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-xs-6">
                      <span>Month</span><br/>
                      <select ng-init="userccreg.expiremonth = month[<?php echo date("m") - 1; ?>]" ng-model="userccreg.expiremonth" class="form-control">
                        <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                        <?php foreach ($formonths as $index => $formonth) {
                          echo "<option value='".$index."'>".$formonth."</option>";
                        }?>
                      </select>
                    </div>
                    <div class="col-xs-6">
                      <span>Year</span><br/>
                      <select ng-model="userccreg.expireyear" class="form-control">
                        <?php for ($year=date('Y'); $year < 2050; $year++) {
                          echo "<option value='".$year."'>".$year."</option>";
                        }?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-12">
                  <span style="font-weight:bold;">Billing Information</span>
                </div>
              </div>

              <div>
                <div class="form-group row" >
                  <div class="col-md-4">
                    First Name: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="userccreg.billingfname" name="billingfname" ng-required="typetransact2 == ''" />
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-4">
                    Last Name: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="userccreg.billinglname" name="billinglname" ng-required="typetransact2 == ''" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    Address Line 1: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="userccreg.al1" name="al1" ng-required="typetransact2 == ''" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    Address Line 2:
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="userccreg.al2" name="al2" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    City: *
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="userccreg.city" name="city" ng-required="typetransact2 == ''" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    State:
                  </div>
                  <div class="col-md-8">
                   <input class="form-control " type="text" ng-model="userccreg.state" name="city" />
                 </div>
               </div>
               <div class="form-group row">
                <div class="col-md-4">
                  ZIP/Postal Code: *
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="userccreg.zip" name="zip" ng-required="typetransact2 == ''" />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  Country: *
                </div>
                <div class="col-md-8">
                  <select ng-model="userccreg.country" ng-init="userccreg.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == ''">
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <span style="margin-left:0px;"><input type="checkbox" name="recurpayment" ng-model="userccreg.recurpayment" value="recurpayment"></span>
                <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
              </div>
            </div>

            <div ng-if="userccreg.recurpayment">
              <div class="form-group row" ng-hide="true">
                <div class="col-md-4">
                  Start Date: *
                </div>
                <div class="col-md-8">
                  <div class="row" >
                    <div class="col-md-4">
                      <span>Year</span><br/>

                      <input type="hidden" value="{'val': <?php echo date('Y'); ?>}" ng-model="userccreg.start_year">

                    </div>
                    <div class="col-md-4">
                      <span>Month</span><br/>
                      <select ng-init="userccreg.start_month = donmonth[<?php echo date("m") - 1; ?>]" ng-model="userccreg.start_month" class="form-control" ng-required="userccreg.recurpayment" style="padding:3px;">
                        <option ng-repeat="month in donmonth track by $index" value="{[{month.val}]}">{[{month.name}]}</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <span>Day</span><br/>
                      <select ng-init="userccreg.start_day = donday[<?php echo date("d") - 1; ?>]" ng-model="userccreg.start_day" class="form-control" ng-options="d.val for d in donday" ng-required="userccreg.recurpayment" style="padding:3px;">
                        <option></option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                <div class="col-md-4">
                  Select Duration: *
                </div>
                <div class="col-md-4">
                  <input type="text" class="hidden" ng-init="userccreg.reccur_length = 1" name="reccur_length" ng-model="userccreg.reccur_length" ng-required="userccreg.recurpayment" />
                  <select class="form-control col-md-4" name="reccur_count" ng-model="userccreg.reccur_count" ng-required="userccreg.recurpayment">
                    <option value="">how many</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="9999">never end</option>
                  </select>
                </div>
                <div class="col-md-4">
                  months
                </div>

                <div class="col-md-8" ng-hide="true">
                  <select class="form-control col-md-8" ng-required="userccreg.recurpayment" ng-model='userccreg.reccur_unit' ng-init="userccreg.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                  </select>
                </div>
              </div>
            </div>
          </div>
          <!-- END FOR CREDIT CARD-->

          <!-- CHECK Authorize For Donate Part-->
          <div ng-show="typetransact2 == 'ach2' " class="ach" >

            <div class="form-group row" >
              <div class="col-md-4">
                Account Holder Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.accountname" name="accountname" ng-required="typetransact2 == 'ach2'"/>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Your bank name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.bankname" name="bankname" ng-required="typetransact2 == 'ach2'"/>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-12">
                Enter the routing code and account number as they appear on the bottom of your check:
              </div>
              <div class="col-md-12">
                <img src="/images/template_images/checksample.png">
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Bank Routing Number: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.bankrouting" name="bankrouting" ng-required="typetransact2 == 'ach2'" only-digits/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Bank Account Number: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.bankaccountnumber" name="bankaccountnumber" ng-required="typetransact2 == 'ach2'" only-digits />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Select Account Type:
              </div>
              <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                <input type="radio" name="at" ng-model="userecheckreg.at" value="CHECKING">&nbsp;&nbsp;Checking account<br/>
                <input type="radio" name="at" ng-model="userecheckreg.at" value="BUSINESSCHECKING">&nbsp;&nbsp;Business checking account<br/>
                <input type="radio" name="at" ng-model="userecheckreg.at" value="SAVINGS">&nbsp;&nbsp;Savings account
              </div>
            </div>

            <div class="form-group row" >
              <div class="col-md-12">
                <input type="checkbox"  ng-model="userecheckreg.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'">*
                <span style="text-align: center;">
                  By entering my account number about and Clicking authorize, I authorize my payment to be processed
                  as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                  provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                  <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                  authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                </span>
              </div>
            </div>

            <div class="form-group row" >
              <div class="col-md-12">
                <span style="font-weight:bold;">Billing Information</span>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                First Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.billingfname" name="billingfname" ng-required="typetransact2 == 'ach2'" />
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Last Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.billinglname" name="billinglname" ng-required="typetransact2 == 'ach2'" />
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Address Line 1: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.al1" name="al1" ng-required="typetransact2 == 'ach2'" />
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Address Line 2:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.al2" name="al2" />
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                City: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.city" name="city" ng-required="typetransact2 == 'ach2'" />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                State:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.state" name="city" />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Province:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.province" name="province" />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                ZIP/Postal Code: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="userecheckreg.zip" name="zip" ng-required="typetransact2 == 'ach2'" />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Country: *
              </div>
              <div class="col-md-8">
                <select ng-model="userecheckreg.country" ng-init="userecheckreg.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'">
                </select>
              </div>
            </div>

            <!-- eCheck RECURR PAYMENT -->
            <!-- <div class="form-group row">
              <div class="col-md-12">
                <span style="margin-left:0px;"><input type="checkbox" name="recurpayment2" ng-model="userecheckreg.recurpayment2" value="recurpayment2"></span>
                <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
              </div>
            </div> -->

            <div ng-if="userecheckreg.recurpayment2">              
              <div class="form-group row" >
                <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                <div class="col-md-4">
                  Select Duration: *
                </div>
                <div class="col-md-4">
                  <input type="text" class="hidden" ng-init="userecheckreg.reccur_length = 1" name="reccur_length" ng-model="userecheckreg.reccur_length" ng-required="userecheckreg.recurpayment" />
                  <select class="form-control col-md-4" name="reccur_count" ng-model="userecheckreg.reccur_count" ng-required="userecheckreg.recurpayment">
                    <option value="">how many</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="9999">never end</option>
                  </select>
                </div>
                <div class="col-md-4">
                  months
                </div>

                <div class="col-md-8" ng-hide="true">
                  <select class="form-control col-md-8" ng-required="userecheckreg.recurpayment" ng-model='userecheckreg.reccur_unit' ng-init="userecheckreg.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                  </select>
                </div>
              </div>

            </div>
          </div>
          <!-- END of CHECK Authorize For Donate Part-->

          <a id="causes"></a>
          <!-- Paypal Form Payment -->
          <!-- Registration with Paypal -->

          <div  ng-show="typetransact2 == 'paypal2' ">

            <div class="form-group row" ng-show="typetransact2 == 'paypal2'">

              <div class="col-md-12">
                <span style="margin-left:0px;">
                  <input type="checkbox" name="recurpayment3" ng-model="pp.recurpayment3" value="recurpayment3">
                </span>
                <span style="font-weight:bold;">  &nbsp; I would like to make monthly recurring donations.</span>

              </div>
            </div>

            <div  ng-hide="pp.recurpayment3" class="paypalform">
              <div style="clear:both; "></div>
              <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </div>

            <div ng-show="pp.recurpayment3">
              <div class="form-group row" >
                <div class="col-md-12">How many times would you like this to recur? (including this payment) <br><br></div>
                <div class="col-md-4">
                  Select Duration: *<br><br>
                </div>
                <div class="col-md-4">
                  <select name="srt" ng-model="pp.srt" class="location form-control m-b" ng-required="pp.recurpayment3">
                    <option value="0">Never End</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
                  <input type="hidden" name="a3" ng-model="a3" ng-value="donateamount">
                </div>
                <div class="col-md-4">
                  months
                </div>
              </div>

            </div>
          </div>
          <!-- END of Registration with Paypal -->
          <a id="causes"></a>
        </div>

                


        <span class="formerror" ng-show="user.password != user.repassword" style="color: #fd5555">
          The password you inputed did not match. <br/>
        </span>
        <span class="formerror" ng-show="invalidemail == true" style="color: #fd5555">
          The email you have entered is invalid. <br/>
        </span>
        <div class="form-group" ng-hide="user.password != user.repassword || invalidemail == true">
          <div class="formerror" ng-show="errorpayment!='' && process == false" style="color: #fd5555" >
            <span ng-bind="errorpayment"></span>
          </div>
          <label ng-show="process == true">Processing Registration...</label>

          <input type="button" name="" id="abouteco" value="Submit"  class="btn btn-sm btn-primary" ng-show="process == false"  ng-click="register(user, donateamount,'Donation')" ng-disabled="form.$invalid" style="float:right;"/>

        </div>
        <div ng-show="(usernametaken != '' || emailtaken != '' || passmin == true) && process == false">
          <span class="formerror" ng-show="usernametaken != ''" style="color: #fd5555">Username has already been taken.</span> <br/>
          <span class="formerror" ng-show="emailtaken != ''"  style="color: #fd5555">Email already taken.</span>
          <span class="formerror" ng-show="passmin == true" style="color: #fd5555">Shoud be minimum of 6 characters.</span>
        </div>
      </form>
      <!-- END of Registration with Paypal -->


      <form class="paypalform" action="<?php echo $this->config->application->paypalURL;?>" method="post" target="_top" id="paypalsingle">

        <div style="clear:both; ">
          <input type="hidden" name="cmd" value="_donations">
          <input type="hidden" name="business" value="<?php echo $this->config->application->paypalAccount;?>">
          <input type="hidden" name="lc" value="US">
          <input type="hidden" name="custom" id="paypalcustom" ng-value="user.email">
          <input type="hidden" name="item_name" value="Earth Citizens Organization">
          <input type="hidden" name="amount" ng-value="donateamount">
          <input type="hidden" name="currency_code" value="USD">
          <input type="hidden" name="no_note" value="0">
          <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
        </div>

        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </form>
      <form action="<?php echo $this->config->application->paypalURL;?>" method="post" id="paypalreoccur">
        <input type="hidden" name="cmd" value="_xclick-subscriptions">
        <!--You will need to set the email to your PayPal email or Secure Merchant ID -->
        <input type="hidden" name="custom" id="paypalcustom" ng-value="user.email">
        <input type="hidden" name="business" value="<?php echo $this->config->application->paypalAccount;?>">
        <input type="hidden" name="lc" value="US">
        <input type="hidden" name="item_name" value="Donate to Earth Citizen Organization">
        <input type="hidden" name="item_number" value="0000">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="no_shipping" value="2">
        <input type="hidden" name="src" value="1">
        <input type="hidden" name="p3" value="1">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHosted">
        <input type="hidden" name="t3" value="M">
        <input type="hidden" name="srt" ng-value="pp.srt">
        <input type="hidden" name="a3" ng-value="donateamount">
      </form>

    </div>

    <!--========================  DONATE PART FOR OTHERS ========================-->

    <div class="reganddonate" style="margin-top:20px;" ng-show="new == 'woreg'">

      <div style="margin-bottom:10px;">

        <div class="form-group row">
          <div class="col-md-12">
            <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/>
          </div>
        </div>
        <div class="form-group row" >
          <div class="col-md-4">
            Payment Method:
          </div>
          <div class="col-md-8">
            <select name="typetransact2" class="form-control" ng-model="typetransact2" >
              <option value=''>Credit Card</option>
              <option value='ach2'>Checking/Savings Account</option>
              <option value='paypal2'>PayPal</option>
            </select>
          </div>
        </div>

      </div>



      <!-- CREDIT CARD  BILLING FOR DONATION PART -->
      <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
        <br/>
        <form name="formCredit" id="formCredit" method="post" action="">
          <div class="form-group row" >
            <div class="col-md-4">
            </div>
            <div class="col-md-8">
              <img src="/images/template_images/visa.png" style="width:65px;">
              <img src="/images/template_images/amex.png" style="width:65px;">
              <img src="/images/template_images/mastercard.png" style="width:65px;">
              <img src="/images/template_images/discover.png" style="width:65px;">
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Credit Card Number: *
            </div>
            <div class="col-md-8">
              <input class="form-control" type="text" ng-model="ccworeg.ccn" name="" required="required" only-digits />
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              CVV Number: *
            </div>
            <div class="col-md-8">
              <input type="text" class="form-control " ng-model="ccworeg.cvvn" name="" required="required" only-digits />
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-8">

            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Credit Card Expiration: *
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-xs-6">
                  <span>Month</span><br/>
                  <select ng-model="ccworeg.expiremonth" class="form-control" required="required">
                    <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                    <?php foreach ($formonths as $index => $formonth) {
                      echo "<option value='".$index."'>".$formonth."</option>";
                    }?>
                  </select>
                </div>
                <div class="col-xs-6">
                  <span>Year</span><br/>
                  <select ng-model="ccworeg.expireyear" class="form-control" required="required">
                    <?php for ($year=date('Y'); $year < 2050; $year++) {
                      echo "<option value='".$year."'>".$year."</option>";
                    }?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-12">
              <span style="font-weight:bold;">Billing Information</span>
            </div>
          </div>

          <div>
            <div class="form-group row" >
              <div class="col-md-4">
                First Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.billingfname" name="billingfname" required="required"/>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Last Name: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.billinglname" name="billinglname" required="required"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Address Line 1: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.al1" name="al1" required="required"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Address Line 2:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.al2" name="al2"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                City: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.city" name="city" required="required"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                State:
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.state" name="city"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                ZIP/Postal Code: *
              </div>
              <div class="col-md-8">
                <input class="form-control " type="text" ng-model="ccworeg.zip" name="zip" required="required"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4">
                Country: *
              </div>
              <div class="col-md-8">
                <select ng-model="ccworeg.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required="required">
                </select>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Email Address: *
              </div>
              <div class="col-md-8" ng-controller="emailcheckCTRL">
                <input type="email" class="form-control " ng-model="ccworeg.email" name="email" ng-change="ccemailcheck(ccworeg)" required/>
                <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
              </div>
            </div>
            <div class="form-group">
              How did you learn about the ECO?
              <select class="form-control m-b" ng-model="ccworeg.howdidyoulearn" ng-change="changeme()">
                <option value="ECO event">ECO event</option>
                <option value="ECO Program Graduates">ECO Program Graduates</option>
                <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                <option value="Invitation Email">Invitation Email</option>
              </select>
            </div>
            <div class="form-group" ng-if="ccworeg.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" ng-controller="loadcenternameCTRL">
              Center Name
              <select class="location form-control m-b" ng-model="ccworeg.cname" required="ccworeg.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12" ng-hide="invalidemail == true">
              <label ng-show="process == true" style="color:#4E990B;">Processing your donation...</label>
              <div class="formerror" ng-show="errorpayment!='' && process == false" style="color: #fd5555" >{[{ errorpayment }]} </div>
              <input type="button" name="" id="" value="Submit"  class="btn btn-sm btn-primary" ng-show="reccur_success == 1"  ng-click="submitcreditworeg(ccworeg,donateamount)" ng-disabled="formCredit.$invalid || donateamount < 10" style="float:right;"/>
            </div>
          </div>
        </form>
      </div>
      <!-- END FOR CREDIT CARD-->

      <!-- CHECK Authorize For Donate Part-->
      <div ng-show="typetransact2 == 'ach2' " class="ach" >
        <br/>
        <form name="formAuthorizeCheck" id="formAuthorize" method="post" action="">

          <div class="form-group row" >
            <div class="col-md-4">
              Account Holder Name: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.accountname" name="accountname" required/>

            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Your bank name: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.bankname" name="bankname" required/>

            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-12">
              Enter the routing code and account number as they appear on the bottom of your check:
            </div>
            <div class="col-md-12">
              <img src="/images/template_images/checksample.png">
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Bank Routing Number: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.bankrouting" name="bankrouting" required only-digits/>

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              Bank Account Number: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.bankaccountnumber" name="bankaccountnumber" required only-digits />

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              Select Account Type:
            </div>
            <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
              <input type="radio" name="at" ng-model="checkworeg.at" value="CHECKING" required="required">&nbsp;&nbsp;Checking account<br/>
              <input type="radio" name="at" ng-model="checkworeg.at" value="BUSINESSCHECKING" required="required">&nbsp;&nbsp;Business checking account<br/>
              <input type="radio" name="at" ng-model="checkworeg.at" value="SAVINGS" required="required">&nbsp;&nbsp;Savings account
            </div>
          </div>

          <div class="form-group row" >
            <div class="col-md-12">

              <input type="checkbox"  ng-model="checkworeg.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'" required>*

              <span style="text-align: center;">
                By entering my account number about and Clicking authorize, I authorize my payment to be processed
                as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
              </span>
            </div>
          </div>

          <div class="form-group row" >
            <div class="col-md-12">
              <span style="font-weight:bold;">Billing Information</span>
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              First Name: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.billingfname" name="billingfname" required />

            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Last Name: *
            </div>
            <div class="col-md-8">
              <input class="form-control " type="text" ng-model="checkworeg.billinglname" name="billinglname" required />
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Address Line 1: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.al1" name="al1" required />

            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              Address Line 2:
            </div>
            <div class="col-md-8">
              <input class="form-control " type="text" ng-model="checkworeg.al2" name="al2" />
            </div>
          </div>
          <div class="form-group row" >
            <div class="col-md-4">
              City: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.city" name="city" required />

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              State:
            </div>
            <div class="col-md-8">
              <input class="form-control " type="text" ng-model="checkworeg.state" name="city" />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              Province:
            </div>
            <div class="col-md-8">
              <input class="form-control " type="text" ng-model="checkworeg.province" name="province" />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              ZIP/Postal Code: *
            </div>
            <div class="col-md-8">

              <input class="form-control " type="text" ng-model="checkworeg.zip" name="zip" required />

            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              Country: *
            </div>
            <div class="col-md-8">

              <select ng-model="checkworeg.country" ng-init="check.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'" required="">

              </select>
            </div>
          </div>
          <div class="form-group row" ng-controller="emailcheckCTRL" >
            <div class="col-md-4">
              Email Address: *
            </div>
            <div class="col-md-8">
              <input type="email" class="form-control " ng-model="checkworeg.email" name="email" ng-change="echeckemailcheck(checkworeg)" required/>
              <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
            </div>
          </div>

          <div class="form-group">
            How did you learn about the ECO?
            <select class="form-control m-b" ng-model="checkworeg.howdidyoulearn" ng-change="changeme()">
              <option value="ECO event">ECO event</option>
              <option value="ECO Program Graduates">ECO Program Graduates</option>
              <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
              <option value="Invitation Email">Invitation Email</option>
            </select>
          </div>
          <div class="form-group" ng-if="checkworeg.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" ng-controller="loadcenternameCTRL">
            Center Name
            <select class="location form-control m-b" ng-model="checkworeg.cname" required="checkworeg.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
              <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
            </select>
          </div>

          <div class="form-group row" style="padding-left:15px;">
            <div class="col-md-12" ng-hide="invalidemail == true">
              <label ng-show="process == true" style="color:#4E990B;">
                Processing your donation...
              </label>
              <div class="formerror" ng-show="errorpayment!='' && process == false" style="color: #fd5555" >
                <span ng-bind="errorpayment"></span>
              </div>
              <input type="button" name="" id="abouteco" value="Submit" ng-click="submitecheckworeg(checkworeg, donateamount, user)" class="btn btn-sm btn-primary" ng-disabled="formAuthorizeCheck.$invalid || donateamount < 10" style="float:right;"/>
            </div>

          </div>


        </form>
      </div>
      <!-- END of CHECK Authorize For Donate Part-->
      <a id="causes"></a>
      <!-- Paypal Form Payment -->


      <div  ng-show="typetransact2 == 'paypal2' ">
        <br/>
        <form class="paypalform" name="paypalform" action="<?php echo $this->config->application->paypalURL;?>" method="post" target="_top" id="paypalsingle">

          <div style="clear:both; ">
            <input type="hidden" name="cmd" value="_donations">
            <input type="hidden" name="business" value="<?php echo $this->config->application->paypalAccount;?>">
            <input type="hidden" name="lc" value="US">
            <input type="hidden" name="custom" id="paypalcustom" ng-model="paypalcustom" value="">
            <input type="hidden" name="item_name" value="Donate to Earth Citizen Organization">
            <input type="hidden" name="amount" ng-value="donateamount">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="no_note" value="0">
            <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
          </div>
          <div class="form-group">
            How did you learn about the ECO?
            <select class="form-control m-b" ng-model="paypal.howdidyoulearn" ng-change="changeme()">
              <option value="ECO event">ECO event</option>
              <option value="ECO Program Graduates">ECO Program Graduates</option>
              <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
              <option value="Invitation Email">Invitation Email</option>
            </select>
          </div>
          <div class="form-group" ng-if="paypal.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" ng-controller="loadcenternameCTRL">
            Center Name
            <select class="location form-control m-b" ng-model="user.cname" required="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
              <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
            </select>
          </div>

          <div ng-hide="paypalbutton">

            <a href="" ng-click="submitpaypalworeg(paypal.howdidyoulearn, paypal.cname)" ng-hide="paypalform.$invalid || donateamount < 10"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="paypalbutton" alt="PayPal - The safer, easier way to pay online!" style="float:right;"></a>

          </div>
        </form>
      </div>
      <!-- END of Paypal -->
      <a id="causes"></a>


    </div>
  </div>
  <div class="col-md-6" ng-hide="new == 'already'">



    <div class="gp">

      <p class="phar">BECOME AN EARTH CITIZEN BY REGISTERING AND MAKING A DONATION</p>
      <p>
        As a responsible citizen of the Earth, I agree to develop my body, mind, and spirit to their greatest capacity and use them to better the life that I share with others both locally and globally.
      </p>
      <p>
        As an honorable citizen of the Earth, I agree to demonstrate the highest human virtues and the greatness of the human spirit in order to encourage and inspire others to manifest their best for the benefit of all.
      </p>
      <p>
        As a caring citizen of the Earth, I agree to practice mindful living to create positive changes in my lifestyle and work with my fellow Earth Citizens to create a peaceful sustainable world.
      </p>
      <p>
        <span style="font-weight:bold;">By becoming an Earth Citizen, you will:</span>
        <ul style="list-style: circle;padding-left:17px;">
          <li>Receive E-newsletter once a month, starting from April</li>
          <li>Be invited to Earth Citizen Webinar, once a month</li>
          <li>Have access to ECO media library (this will be built in two months)</li>
        </ul>
      </p>

      <hr>


      <p class="phar">WHY GIVE?</p>
      <p>
        To pursue the projects that advance our mission, ECO relies on donations from institutions and individuals who support our vision for a peaceful and sustainable world.
      </p>
      <p>
        <span style="font-weight:bold;">Donated funds are used for:</span>
        <ul style="list-style: circle;padding-left:17px;">
          <li>Running leadership programs below cost to make it available to a larger audience</li>
          <li>Supporting outreach activities</li>
          <li>Developing campus facilities in Northern Arizona</li>
        </ul>
      </p>

      <a href="<?php echo $this->config->application->baseURL; ?>/project-funds">More information about the projects</a>

      <hr>


      <p class="phar">WAYS OF GIVING</p>
      <p>
        With a donation of any size, you are acknowledging ECO’s work and its vision. Your support is the primary source of strength by which ECO pursues its vision and fulfills its mission.
      </p>
      <p>
        <span style="font-weight:bold;">The most typical ways of giving include:</span>
        <ul style="list-style: circle;padding-left:17px;">
          <li>Living online, by mail or by direct deposit</li>
          <li>Planned Giving</li>
          <li>Endowments</li>
          <li>Corporate Sponsorship</li>
        </ul>
      </p>

      <a href="<?php echo $this->config->application->baseURL; ?>/ways-of-giving">More information about Ways of Giving</a>


    </div>

  </div>
  <div class="col-md-12">
    <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:16px;color:#777;">
      <p>
        ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
      </p>
    </div>
  </div>
</div>
</div>
</div>
</div>







