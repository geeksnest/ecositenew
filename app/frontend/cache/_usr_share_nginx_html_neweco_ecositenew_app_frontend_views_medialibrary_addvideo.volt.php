
<a href="" id="errorget"></a>
<a href="" id="successget"></a>
<div class="container-fluid" ng-controller="addvideoCtrl">

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">LEARN</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary/mymedialibrary">My Media library</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="">Add video</a> 
      </div>
    </div>
  </div>

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h4>Donate Your Content</h4>
      </div>
      <div class="col-sm-12">
       <p>Your content can help inspire thousands of promising leaders for a more sustainable future of our world. Please consider donating your content here so that it can benefit our leaders and public who care for people and the planet.</p> 

        By donating your content:
        <ul style="list-style-type:disc;padding: 10px 25px">
          <li class=""> Your name will be recognized as contributor to our cause</li>
          <li class="">You grant ECO a non-exclusive license to use your content for a year for our media library and online education</li>
          <li class="">ECO will provide link to your information so that interested people may work directly with you.</li>
        </ul>
       
        Thank you for supporting ECO with your talents and contents.
      </div>
    </div>
    <div class="create-form">  
      <!-- <div class="row">  

        <div class="col-sm-8">  -->

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-danger" ng-show="errorget" ng-cloak>
                <span class="" ng-bind="errorget"></span>
              </div>
              <div class="form-group wrapper-sm bg-success" ng-show="successget" ng-cloak>
                <span class="" ng-bind="successget"></span>
              </div>
              <alert ng-repeat="resultget in resultget" type="{[{resultget.type }]}" close="closeAlert1($index)">
                <span class="" ng-bind="resultget.msg"></span>            
                <div ng-repeat="cNTS in cNTS">
                  - <span ng-bind="cNTS.msg"></span><br>
                </div>
              </alert>

             <!--  <div class="form-group">
                Video type <label class="req">*</label>  
                <div class="radio">
                  <label class="i-checks">                
                    <input type="radio" name="type" value="upload" ng-model="media.type" id="type" name="type" ng-checked="media.type == 'upload'"><i></i> Upload
                  </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label class="i-checks">
                    <input type="radio" name="type" value="embed" ng-model="media.type" id="type" name="type" ><i></i> Embed
                  </label>
                </div>
              </div> -->
             <!--  <div ng-show="media.type == 'embed'">
                <div class="form-group">
                  <textarea class="form-control textarea" rows="2"  id="embed" name="embed" ng-model="media.embed" ng-change="youtube(media.embed)"></textarea>                
                  <div class="line line-dashed b-b line-lg"></div>
                  <div ng-bind-html="preview"></div>
                  <div class="line line-dashed b-b line-lg"></div>
                </div>
              </div> -->
              <div>
              <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(media,videoToUpload,thumbImg)">
                <div class="form-group">
                  <input type="hidden" ng-init="media.authorid = userAgent" ng-model="media.authorid">
                  Content Title <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="media.title" name="title" id="title" required ng-disabled="loadingg == true" />
                </div>
                <div class="line line-dashed b-b line-lg"></div>

               <!--  <div class="form-group">
                  Content Description <label class="req">*</label>
                  <textarea class="form-control textarea" rows="4" ng-model="media.shortdesc" name="shortdesc" id="shortdesc" maxlength="200"></textarea>
                </div>
                <div class="line line-dashed b-b line-lg"></div> -->

                <div class="form-group">
                  Content Description: <label class="req">*</label> <label ng-show="n && !media.description" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label> <input type="text" id="desc" style="opacity:0;">
                  <!-- <div ckeditor="options" ng-model="media.description" name="description" id="description" ready="onReady()" ng-disabled="loadingg == true"></div> -->
                  <textarea class="ck-editor" ng-model="media.description" name="description" id="description" ng-disabled="loadingg == true"></textarea>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Contact Name: <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="media.name" name="name" id="name" required ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                 <div class="form-group">
                  Contact Phone Number: <label class="req">*</label>
                  <input id="phone" type="tel" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-phone" ng-model="media.phone" placeholder="(XXX) XXXX XXX" phone ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" only-digits name="phone" ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                 <div class="form-group">
                  Email Address: <label class="req">*</label>
                  <input type="email" class="form-control" ng-model="media.email" name="email" id="email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required ng-disabled="loadingg == true"/>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Website Address: <label style="font-size:12px">(optional)</label>
                  <input type="text" class="form-control" ng-model="media.web" name="web" id="web" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" placeholder="http://" ng-disabled="loadingg == true">
                  <label ng-show="form.web.$invalid && !form.web.$pristine && form.web.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                 Facebook Page: <label style="font-size:12px">(optional)</label>
                  <input type="text" class="form-control" ng-model="media.fb" name="fb" id="fb" ng-pattern="/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{2,5})?(\/.*)?$/" placeholder="http://" ng-disabled="loadingg == true">
                  <label ng-show="form.fb.$invalid && !form.fb.$pristine && form.fb.$error" class="control-label p" style="color:#a94442;" ng-cloak>
                    Invalid URL Pattern.</label>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group" id="chosen">
                  Tags <label class="req">*</label> <input type="text" id="tags" style="opacity:0;">
                  <ui-select multiple tagging tagging-label="false" ng-model="media.tag" name="tag" id="tag" theme="bootstrap" ng-disabled="disabled || loadingg == true">
                  <ui-select-match placeholder="Select Tags" style="padding-left:5px;">{[{ $item }]}</ui-select-match>
                  <ui-select-choices repeat="ta in tag">{[{ ta }]}</ui-select-choices>
                </ui-select>
                <label ng-show="t && (media.tag=='' || !media.tag)" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group" id="chosen">
                Category <label class="req">*</label> <input type="text" id="cats" style="opacity:0;">
                <select chosen multiple options="category" class="form-control m-b" ng-options="cat as cat.categoryname for cat in category track by cat.categoryid" ng-model="media.category" name="category" id="category" ng-disabled="cate">
                </select> 
                <label ng-show="cat && (media.category=='' || !media.category)" class="control-label" style="font-size:14px;color:#a94442;" ng-cloak>
                  This field is required.</label>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
               <div>
                <div class="form-group">
                  <div class="row"> 
                    <div class="col-sm-12 create-proj-thumb">
                      <alert ng-repeat="videoAlerts in videoAlerts" type="{[{videoAlerts.type }]}" close="closeAlert($index)">{[{ videoAlerts.msg }]}</alert>
                    </div>                     
                    <div class="col-sm-6 propic create-proj-thumb" ng-hide="hideBig">
                      <fieldset class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="change-video" ngf-change="prepare(files)" ngf-select ngf-drag-over-class="'dragover'" ng-model="files" ngf-multiple="false" accept='video/mp4' ng-disabled="loadingg==true">
                        <img class="video-icon" src="/images/icons/video-icon.png"><br>
                        <label>Choose a video from your computer</label> | <label>MP4 only</label> <!-- | <label>Maximun of 20MB</label> -->
                        <br>
                      </fieldset>
                    </div>
                    <div class="col-sm-6 create-proj-thumb" ng-show="videoToUpload">
                      <fieldset class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="change-video" ngf-change="prepare(files)" ngf-select ngf-drag-over-class="'dragover'" ng-model="files" ngf-multiple="false" accept='video/mp4' ng-disabled="loadingg==true">
                        <img class="video-icon" src="/images/icons/video-icon.png"><br>
                        <label>Choose a video from your computer</label> | <label>MP4 only</label> <!-- | <label>Maximun of 20MB</label> -->
                        <br>
                      </fieldset>
                      <label>Video</label>  
                      <div class="line line-dashed b-b line-lg"></div>
                      <div id="video">
                        <video class="vidoe-play-ml" controls ngf-src="videoToUpload"></video>
                      </div>                      
                      <div class="line line-dashed b-b line-lg"></div>
                    </div> 
                    <div class="col-sm-6 create-proj-thumb">
                      <fieldset class="label_profile_pic border-dash browse-img-wrap pointer-wrap drop-box" id="thumbnail" ngf-change="prepareThumb(filesT)" ngf-select ng-model="filesT" ngf-multiple="false" accept='image/jpeg,image/png,image/gif' ng-disabled="loadingg==true">
                        <img class="video-icon" ng-st src="/images/icons/thumbnail-icon.png"><br>
                        <label>Choose an image from your computer</label>
                        <br>
                      </fieldset>
                      <label>Thumbnail</label>
                      <div class="line line-dashed b-b line-lg"></div>
                      <div ng-show="imageselected == true">
                        <!-- <div class="vidoe-play-ml" ngf-background="thumbImg" ></div> -->
                        <img class="vidoe-play-ml" ngf-src="thumbImg">                     
                        <div class="line line-dashed b-b line-lg"></div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="">
              <p class="font-bold">Terms and Conditions</p>
              <p>I, the donor, warrant that the contents are original and that the donor owns the entire title, right, and interest in the contents, including the right to prepare derivative works and to reproduce; distribute by sale, by rental, lease or lending; to perform publicly; and to display the contents.</p>

              <p>I, the donor, allow ECO, the license to use the content free for its organizational mission in its events, programs and websites for one year term. I agree that this license will be renewed annually automatically, and can be cancelled annually by written request from the donor.</p>
                <label class="i-checks">
                  <input name="checkbox" type="checkbox" ng-model="media.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(media.agree)" ng-init="media.agree=false" ng-disabled="loadingg==true"><i></i> 
                  <a href="" class="text-info">I accept the Terms and Conditions.</a> <br><br>
                  <p ng-show="chkerror && media.agree==false" class="control-label col-sm-12" style="font-size:14px;color:#a94442; margin-left:-40px;" ng-cloak>This field is required.</p>
                </label>
                <p>We will review your submission. After review and confirmation, your content will be posted in our media library so that people can use it.</p>
              </div>

              <div class="form-group">
              <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
               <span ng-bind="alert.msg"></span> </alert>
                <div class="col-md-12 center" ng-show="loadingg == true">
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                  <span ng-bind="loadingMsg"></span>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                  <button type="submit" class="btn btn-info" ng-disabled="loadingg == true">Submit Video</button>
                </footer>
              </div>
              </form>
            </div>
          </div>

        </div>
        <!-- </div>
      </div> -->
    </div>
  </div>
</div>