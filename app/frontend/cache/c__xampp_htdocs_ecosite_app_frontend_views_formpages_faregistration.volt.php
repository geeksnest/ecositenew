<style type="text/css">
  .help-block,.p{
    font-size: 14px;
  }
  .n{
    font-size: 12px;
  }
</style>

<div class="row" ng-controller="FaCtrl">
  <div class="col-sm-12">
    <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitfa(user)">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group has-feedback">
            <label>Name of Organization * </label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.organization" name="organization" required="true">
            <i ng-show="form.organization.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.organization.$invalid && !form.organization.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Address 1 * </label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.add1" name="add1" required="true">
            <i ng-show="form.add1.$valid" class="glyphicon glyphicon-ok form-control-feedback" ng-cloak></i>
            <i ng-show="form.add1.$invalid && !form.add1.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Address 2 *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.add2" name="add2" required="true" >
            <i ng-show="form.add2.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.add2.$invalid && !form.add2.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Country *</label>
            <select ng-model="user.country"  ng-init="user.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries">
            </select>
            <i ng-show="form.country.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.country.$invalid && !form.country.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>City *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.city" name="city" required="true" >
            <i ng-show="form.city.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.city.$invalid && !form.city.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>State *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.state" name="state" required="true" >
            <i ng-show="form.state.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.state.$invalid && !form.state.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Zip/Postal Code *</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.zip" name="zip" required="true" maxlength="20" >
            <i ng-show="form.zip.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.zip.$invalid && !form.zip.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          
          <div class="form-group">
            <div class="">Organization Category *</div> <br>
            <div class="btn-group">
             <label class="i-checks">
            <input name="cat" type="radio" ng-model="user.cat" value="Profit" class="ng-dirty ng-invalid ng-invalid-required"><i></i>
            Profit
            </label>  
            &nbsp; &nbsp;&nbsp;
            <label class="i-checks">
            <input name="cat" type="radio" ng-model="user.cat" value="Nonprofit" class="ng-dirty ng-invalid ng-invalid-required"><i></i>
             NonProfit
            </label> 
            <br>
             <label ng-show="n && !user.cat" class="control-label p" style="color:#a94442;" ng-cloak>
            Category field is required.</label>

            </div>
          </div>
          <div class="form-group">
            <div class=""> Year of Establishment * </div> <br>
            <select ng-model="user.year" name="year" class="input-sm form-control w-sm inline v-middle" style="width:80px" required="true">
            <?php for ($year= 1960 ; $year <= date('Y'); $year++) { echo "<option value='".$year."'>".$year."</option>";}?>
            </select> <br> 
            <label ng-show="nbday && !user.year" class="control-label p" style="color:#a94442;" ng-cloak>
            Year field is required.</label>
          </div>
          <div class="form-group has-feedback">
            <label>Organization Website Address * </label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.web" name="web" required="true">
            <i ng-show="form.web.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.web.$invalid && !form.web.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Organization Mission Statement * </label> <label class="n">(Less than 500 words)</label>
            <textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.mission" name="mission" required="true" placeholder="" maxlength="500">
            </textarea>
            <i ng-show="form.mission.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.mission.$invalid && !form.mission.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group has-feedback">
            <label>Contact Person * </label>
            <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.contactperson" name="contactperson" required="true" >
            <i ng-show="form.contactperson.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.contactperson.$invalid && !form.contactperson.$pristine" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
          </div>

          <div class="form-group has-feedback">
            <label>Contact Phone Number * </label>
            <input type="tel" name="phone" class="form-control ng-pristine ng-invalid ng-invalid-required ng-invalid-pattern input-phone" phone  placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/" name="phone" required="true" ng-keypress="ph(user.phone)" id="ph">
            <i ng-show="form.phone.$valid && user.phone" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.phone.$invalid && !form.phone.$pristine || user.phone==''" class="glyphicon glyphicon-remove form-control-feedback"  ng-cloak></i>
            <label ng-show="form.phone.$error.pattern" class="control-label p" style="color:#a94442;" ng-cloak>
            Invalid pattern.</label>
          </div>

          <div class="form-group has-feedback">
            <label>Contact Email Address * </label>
             <input type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="user.email"  name="email" required="true">
             <i ng-show="form.email.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
             <i ng-show="form.email.$invalid && !form.email.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>

          <div class="form-group has-feedback">
            <label>Reason for Partnership * </label> <label class="n">(Less than 500 words)</label>
            <textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.reason" name="reason" required="true" placeholder="" maxlength="500">
            </textarea>
            <i ng-show="form.reason.$valid" class="glyphicon glyphicon-ok form-control-feedback"  ng-cloak></i>
            <i ng-show="form.reason.$invalid && !form.reason.$pristine" class="glyphicon glyphicon-remove form-control-feedback" ng-cloak></i>
          </div>
          <div class="form-group">
            <div class="col-xs-9 col-xs-offset-3">
              <div id="messages"></div>
            </div>
          </div>
          <div class="checkbox">
            <label class="i-checks">
              <input name="checkbox" type="checkbox" ng-model="user.agree" class="ng-dirty ng-invalid ng-invalid-required" ng-click="chkbox(user.agree)" ng-init="user.agree=false"><i></i> 
              <a href="" class="text-info">By clicking the checkbox, I certify that the above information is correct.</a> <br>
              <label ng-show="chkerror && user.agree==false" class="control-label p" style="color:#a94442; margin-left:-40px;" ng-cloak>This field is required.</label>
            </label>
          </div>
           <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
           <span ng-bind="alert.msg"></span> </alert>
          <div class="col-sm-6">
            <div class="col-sm-2">
             <button type="submit" class="btn btn-success">Submit</button>
           </div>
           <div class="col-sm-2">  
           <div class="loadercontainer" ng-cloak ng-show="loader">
              <div class="spinner" style="margin:0px;width:auto;height:30px">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
              </div>
            </div>
          </div>
        </div>

        </div>
      </div>
    </form>
  </div>
</div>