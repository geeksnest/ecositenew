<style type="text/css">
  #iCare-menu-false{
    display: none !important;
  }
  #iCare-menu-true{
    display: block !important;
  }
</style>

<!--===begin container===-->
<div  ng-controller='iCareCtrl' class="container-fluid"  style="margin-top:0px;">


  <div class="container-fluid">
    <div class="iCare-banner">
      <div class="row">
        <div class="col-md-4">
          <div class="banner-iCare-page" style="background-image: url({[{amazonlink}]}/uploads/slider/{[{iCareImg1.foldername}]}/{[{iCareImg1.img}]});">
          </div> 
        </div> 
        <div class="col-md-4">
          <div class="banner-iCare-page">
            <a href=""><img class="banner-inner-img" src="/images/iCare/iCare_2020.png"></a>
          </div>
        </div> 
        <div class="col-md-4">
          <div class="banner-iCare-page" style="background-image: url({[{amazonlink}]}/uploads/slider/{[{iCareImg2.foldername}]}/{[{iCareImg2.img}]});">
          </div> 
        </div> 
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="bluebar-iCare-bb">
    <div class="eco-container">
    <span class="f-white-r f-20"><span class="f-white f-28">I</span>nvolve, <span class="f-white f-28">C</span>hange &amp; <span class="f-white f-28">A</span>ct to <span class="f-white f-28">R</span>ecover the <span class="f-white f-28">E</span>arth</span>
    </div>
    </div>
  </div> 

  <div ng-bind-html="content_1"></div>
  <!--<div class="eco-container">
     <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-12">
          <p>
            <span class="f-orange f-28">OUR GOAL:</span> <span class="f-blue">Building a network of 1 million leaders and 100 million Earth Citizens around the world by 2020</span> as the foundation and driving force for global collaboration to change the world and save the Earth.
          </p>
          <p class="f-orange f-30 center">
            We need your help
          </p>
        </div>
      </div>

      <div class="row">    
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-4">
              <img src="/images/iCare/org.png" class="icare-icon">
            </div>
            <div class="col-sm-8">
              <p>
                <span class="f-orange f-16">10,000<br> Organizations </span><br>
                <span class="f-14">Work with ECO. Become a partner for the meaningful change</span>
                <div class="yellow-button-sre pointer-wrap f-white f-14">
                  <a href="/eco-around-the-world">
                    <span>
                      Register Your Group
                    </span>
                  </a>
                </div>
              </p>
            </div>
          </div>
        </div>  
        <div class="col-sm-4">  
          <div class="row">
            <div class="col-sm-4"> 
              <img src="/images/iCare/citizen.png" class="icare-icon">  
            </div>
            <div class="col-sm-8">   
              <p>
                <span class="f-orange f-16">100 Million<br> Earth Citizens  </span><br>
                <span class="f-14">Be part of the change that benefit people and the planet</span>
                <div class="yellow-button-sre pointer-wrap f-white f-14">
                  <a href="/iCare2020#join">
                    <span>
                      Become an Earth Citizen
                    </span>
                  </a>
                </div>
              </p>
            </div>
          </div>
        </div>  
        <div class="col-sm-4"> 
          <div class="row">
            <div class="col-sm-4">  
              <img src="/images/iCare/fund.png" class="icare-icon">   
            </div>
            <div class="col-sm-8">
              <p>
                <span class="f-orange f-16">1 TRILLION<br> Dollar Fund </span><br>
                <span class="f-14">Live with less and use the saved resources to recover the Earth</span>
                <div class="yellow-button-sre pointer-wrap f-white f-14">
                  <a href="/donation">
                    <span>
                      Make a Donation
                    </span>
                  </a>
                </div>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <p class="f-blue f-30 center padding-top30">
            Do you Care?
          </p>
        </div>
      </div>
    </div>
  </div> -->
  <div ng-bind-html="content_2"></div>
<!--   <div class="container-fluid bg-blue">
    <div class="row">
      <div class="col-md-7 dcv">      
        <div class="pad-t-20 wd-tdr">
          <span class="f-white f-30 t-r">Why Should You Care?</span>         
          <p class="c-white pad-t-10 mp">
            International Agreement to deal with Climate Change was adopted by 195 countries at the Paris climate conference in December 2015, to become effective from 2020. However, as we live in carbon-based economy, any activity for production and consumption will add carbon dioxide emission. Unless we change our value system, mindset, lifestyle and culture, no governments can implement the agreement with legislation and technologies alone.
          </p>
        </div>
      </div>
      <div class="col-md-5">
        <div class="iCare-img-bg" style="background-image: url(/images/iCare/cop.jpg);"></div>      
      </div>
    </div>
  </div>  -->
  <div ng-bind-html="content_3"></div>
  <!-- <div class="container-fluid">
    <div class="eco-container">
      <div class="row">
        <div class="col-sm-12">
          <p class="center pad-t-30">
            <span class="f-Syncopate f-30">WHAT IS</span>
            <span><img src="/images/iCare/iCare_2020_s.png" class="iCare_2020_s" style="width:230px;margin-top:-15px;"></span>
            <span class="f-Syncopate f-30">PROPOSING?</span>
          </p>            
        </div>
        <div class="col-sm-12">
          <p>
            The spontaneous reduction of CO2 emission can happen when we <span class="f-blue">acknowledge the Earth as the Central Value</span>, practice <span class="f-orange">Mindfully Living with Less for a Sustainable World</span>, and seek our growth not in material expansion, but in <span class="f-green">Realization of our Inner Value</span>. When we make this internal change, the external systems will follow the change naturally. ICARE 2020 seeks to create a network of 100 Million Earth Citizens by 2020 to make this change possible.
          </p>            
        </div>
      </div>
    </div>
  </div>   -->
  <div ng-bind-html="content_4"></div>
  <!-- <div class="container-fluid bg-blue">
    <div class="row">
      <div class="col-md-5">
        <div class="iCare-img-bg" style="background-image: url(/images/iCare/poc.jpg);"></div>   
      </div>
      <div class="col-md-7 dcv">      
        <div class="pad-t-20 wd-tdl">
          <span class="f-white f-30 t-r">sHOW tHAT You Care</span>         
          <p class="c-white pad-t-10 mp">
            We are the first and only generation that have the opportunity to change the world and save the Earth through our internal change and personal choice. Each one of us is very special simply because we live on this planet at this critical time. Do you care? Then, please join and start making changes.
          </p>
      <a href="" id="join"></a>  
        </div>
      </div>
    </div>
  </div>  -->

  <div class="eco-container border-orange mar-t-30 mar-b-20" ng-controller='iCareDonateCtrl'>
    <div class="wrapper-md" ng-controller='iCareRegistrationCtrl'>
      <div class="row">
        <div class="col-sm-12">
          <div class="pad-t-20">    
            <p class="center">
              <span class="f-orange f-30">JOIN</span>  
              <span><img src="/images/iCare/iCare_2020_s.png" style="width:180px;margin-top:-12px;"></span>   
            </p>
          </div>
        </div>

        <div class="col-md-6">
        <input type="hidden" ng-init="withdon = '0'">          
          <div>
            <div class="form-group">
              First Name: *
              <input class="form-control " type="text"  ng-model="user.fname" name="firstname" required />
            </div>
            <div class="form-group">
              Last Name: *
              <input class="form-control " type="text" name="" ng-model="user.lname" name="lastname" required />
            </div>
            <div class="form-group">
              Email Address: *
              <span class="formerror" ng-show="emailtaken == true" style="color: #fd5555">
                This email is already registered.
                <a href="<?php echo $this->config->application->baseURL; ?>/donation" ng-click="chtli()" class="chtli" id="chtli">Click here to log in</a> 
                and choose the option  I already have an account.
              </span>
              <span class="formerror" ng-show="notyetconfirm == true" style="color: #fd5555">
                This email is already registered but has not been confirmed.
                <a href="" ng-click="chtr(user)" class="chtli" id="chtr">Click here to resend confirmation email.</a>
              </span>
              <span class="formerror" ng-show="notyetcompleted == true" style="color: #fd5555">
                This email is already registered but has not been completed.
                <a href="" ng-click="chtrc(user)" class="chtli" id="chtr">Click here to resend completion email.</a>
              </span>
              <span class="formerror" ng-if="invalidemail == true" style="color: #fd5555"><br/>
                This is not a valid email. Example: myname@example.com
              </span>
              <input type="email" class="form-control " ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
            </div>
            <div class="form-group">
              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="withdon" value="0" ng-model="withdon" checked>
                  <i></i>
                  I will take the ICARE Pledge.
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="withdon" value="1" ng-model="withdon">
                  <i></i>
                  I will take the ICARE Pledge and register as an Earth Citizen with $10 donation.
                </label>
              </div>
            </div>
            <div class="form-group" ng-show="withdon == '0'">
              <label ng-show="process == true">Processing Registration...</label>
              <button type="button" name="join" id="join" class="btn m-b-xs w-xs btn-info" ng-click="regWd(user.email,user.fname,user.lname)" style="float:right;" ng-disabled="process == true">Join</button>
            </div>                    
          </div>
          
          <!-- REGISTRATION PART -->

          <div class="newaccount"  name="new-account" id="new-account"  ng-if="withdon == '1'">          
            <!-- <p class="phar">Earth Citizen Account Information</p> -->            
            <p>
              <span style="font-weight:bold;">Earth Citizen Account Information</span>
            </p>
            <form name="form" novalidate class="ng-pristine ng-valid" style="color:#777;fonts:0.8em sans-serif bold">
              <div class="form-group">
                Username: *
                <span class="formerror" ng-show="usernametaken != ''" style="color: #fd5555"> Username has already been taken.</span>
                <input type="text" class="form-control " ng-model="user.username" name="username" id="username" onkeypress="return noSpace(event)" required />
              </div>
              <div class="form-group">
                Password: *
                <span class="formerror" ng-show="passmin == true" style="color: #fd5555"> 
                  Shoud be minimum of 6 characters.
                </span>
                <input type="password" class="form-control " name="" ng-model="user.password" name="password" required />
              </div>
              <div class="form-group">
                Re-Type Password: *
                <span class="formerror" ng-show="comparepass == true" style="color: #fd5555">
                  Your Password Should be Equal.
                </span>
                <input type="password" class="form-control " name="" ng-model="user.repassword" name="repassword" required ng-change="retypepass(user)"/>
              </div>

              <div class="form-group" style="margin-top: 10px;">

                <div style="padding-bottom:10px;">Date of birth: </div>

                <div class="row">

                  <div class="col-md-3">
                    <span class="datelabel">Year: *</span><select ng-init="user.byear = year[0]" ng-model="user.byear" class="form-control" ng-options="y.val for y in year"></select>
                  </div>
                  <div class="col-md-1" style="margin-top: 25px;">/</div>

                  <div class="col-md-2"style="margin-top: 25px;">
                    <span>(Optional)</span>
                  </div>

                  <div class="col-md-6">

                    <div class="col-xs-6" style="padding-left:0px;">
                      <span class="datelabel">Month: </span>
                      <select ng-init="user.bmonth = month[0]" ng-model="user.bmonth" class="form-control" ng-options="m.val for m in month"></select>
                    </div>
                    <div class="col-xs-6" style="padding-right:0px;">
                      <span class="datelabel">Day:  </span>
                      <select ng-init="user.bday = day[0]" ng-model="user.bday"  class="form-control" ng-options="d.val for d in day"></select>
                    </div>

                  </div>

                </div>

              </div>

              <div class="form-group">
                Gender: *<br>
                <div class="radio">
                  <label class="i-checks">
                    <input class="radioinput" type="radio" value="male" ng-model="user.gender" name="3">
                    <i></i>
                    Male
                  </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <label class="i-checks">
                    <input class="radioinput" type="radio" value="female" ng-model="user.gender" name="4">
                    <i></i>
                    Female
                  </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
              </div>
              <div class="form-group">
                Locaton: *
                <select  ng-init="user.location = countries[229]" ng-model="user.location" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
                </select>
              </div>
              <div class="form-group">
                Zip Code *
                <input type="text" name="" class="form-control" ng-model="user.zipcode" name="zipcode" onkeypress="return isNumberKey(event)" required />
              </div>
              <div class="form-group">
                How did you learn about ECO?
                <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
                  <option value="ECO event">ECO event</option>
                  <option value="ECO Program Graduates">ECO Program Graduates</option>
                  <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                  <option value="Invitation Email">Invitation Email</option>
                </select>
              </div>
              <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                Center Name
                <select class="location form-control m-b" ng-model="user.cname" >
                  <option value="NA">Not Applicable</option>
                  <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                </select>
              </div>
              <div class="form-group">
                I would like my friend to become an Earth Citizen as well.
                <span class="formerror" ng-show="form.refer.$error.email" style="color: #fd5555">This is not a valid email</span>
                <input type="email" class="form-control " ng-model="user.refer" name="refer" placeholder="Enter your friend's email address" />
                <span style="font-size: 10px;">Thank you for introducing ECO to your friend. ECO will send them an invitation email.</span>
              </div>

              <!--  DONATE PART FOR REGISTRATION -->

              <div class="reganddonate" style="margin-top:50px;">

                <div style="margin-bottom:10px;">
                  <div class="row">
                    <div class="col-md-12">                              
                      <p>
                        <span style="font-weight:bold;">Payment Information</span>
                      </p>
                      <!-- <p class="phar">Payment Information</p> -->
                      <!-- <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/> -->
                    </div>          
                    <div class="col-sm-4">
                      <span style="font-size:16px;">
                        Donation Amount:<br/>
                      </span>
                      <span style="font-size:12px;">
                        (Minimum $10.00)
                      </span>
                    </div>
                    <div class="col-sm-4">                                    
                      <div class="input-group m-b">
                        <span class="input-group-addon h big-num">$</span>
                        <input type="text" class="form-control h big-num" id="donateamount" name="donateamount" value="10.00" ng-model="donateamount" ng-change="change(amount)" only-digits>
                      </div>
                    </div>
                    <div class="col-sm-12" ng-show="donateamount < 10">
                      <p  style="color:#fd5555;font-size:15px;">Please a minimum of $10 for your donation.</p>
                    </div>
                  </div>
                  <div class="row mar-t-20">
                    <div class="col-sm-4">
                      Payment Method:
                    </div>
                    <div class="col-sm-8">
                      <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                        <option value=''>Credit Card</option>
                        <option value='ach2'>Checking/Savings Account</option>
                        <option value='paypal2'>PayPal</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- CREDIT CARD REOCCUR BILLING FOR DONATION PART -->
                <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
                  <br/>
                  <div class="form-group row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-8">
                      <img src="/images/creditcards/amex.png" width="49px" height="29px">
                      <img src="/images/creditcards/discover.png" width="49px" height="29px">
                      <img src="/images/creditcards/mastercard.png" width="49px" height="29px">
                      <img src="/images/creditcards/visa.png" width="49px" height="29px">
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Credit Card Number: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control" type="text" ng-model="userccreg.ccn" name="" ng-required="typetransact2 == ''" only-digits />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      CVV Number: *
                    </div>
                    <div class="col-md-8">
                      <input type="text" class="form-control " ng-model="userccreg.cvvn" name="" ng-required="typetransact2 == ''" only-digits />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Credit Card Expiration: *
                    </div>
                    <div class="col-md-8">
                      <div class="row">
                        <div class="col-xs-6">
                          <span>Month</span><br/>
                          <select ng-model="userccreg.expiremonth" class="form-control">
                            <option ng-repeat="month in donmonth track by $index" value="{[{month.val}]}">{[{month.name}]}</option>
                          </select>
                        </div>
                        <div class="col-xs-6">
                          <span>Year</span><br/>
                          <select ng-model="userccreg.expireyear" class="form-control">
                            <option ng-repeat="year in donyear track by $index" value="{[{year.val}]}">{[{year.val}]}</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-12">
                      <span style="font-weight:bold;">Billing Information</span>
                    </div>
                  </div>

                  <div class="form-group row" >
                    <div class="col-md-4">
                      First Name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.billingfname" name="billingfname" ng-required="typetransact2 == ''" />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Last Name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.billinglname" name="billinglname" ng-required="typetransact2 == ''" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Address Line 1: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.al1" name="al1" ng-required="typetransact2 == ''" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Address Line 2:
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.al2" name="al2" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      City: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.city" name="city" ng-required="typetransact2 == ''" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      State:
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.state" name="city" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      ZIP/Postal Code: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userccreg.zip" name="zip" ng-required="typetransact2 == ''" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Country: *
                    </div>
                    <div class="col-md-8">
                      <select ng-model="userccreg.country" ng-init="userccreg.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == ''">
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-12">
                      <span style="margin-left:0px;"><input type="checkbox" name="recurpayment" ng-model="userccreg.recurpayment" value="recurpayment"></span>
                      <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
                    </div>
                  </div>

                  <div ng-if="userccreg.recurpayment">
                    <!-- <div class="form-group row" ng-hide="true">
                      <div class="col-md-4">
                        Start Date: *
                      </div>
                      <div class="col-md-8">
                        <div class="row" >
                          <div class="col-md-4">
                            <span>Year</span><br/>

                            <input type="hidden" value="{'val': <?php echo date('Y'); ?>}" ng-model="userccreg.start_year">

                          </div>
                          <div class="col-md-4">
                            <span>Month</span><br/>
                            <select ng-init="userccreg.start_month = donmonth[<?php echo date("m") - 1; ?>]" ng-model="userccreg.start_month" class="form-control" ng-options="m.val for m in donmonth" ng-required="userccreg.recurpayment" style="padding:3px;"></select>
                          </div>
                          <div class="col-md-4">
                            <span>Day</span><br/>
                            <select ng-init="userccreg.start_day = donday[<?php echo date("d") - 1; ?>]" ng-model="userccreg.start_day" class="form-control" ng-options="d.val for d in donday" ng-required="userccreg.recurpayment" style="padding:3px;"></select>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="form-group row" >
                      <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                      <div class="col-md-4">
                        Select Duration: *
                      </div>
                      <div class="col-md-4 ph">
                        <input type="text" class="hidden" ng-init="userccreg.reccur_length = 1" name="reccur_length" ng-model="userccreg.reccur_length" ng-required="userccreg.recurpayment" />
                        <select class="form-control col-md-4" name="reccur_count" ng-model="userccreg.reccur_count">
                          <option value="" selected disabled>how many</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                          <option value="9999">never end</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        months
                      </div>

                      <div class="col-md-8" ng-hide="true">
                        <select class="form-control col-md-8" ng-required="userccreg.recurpayment" ng-model='userccreg.reccur_unit' ng-init="userccreg.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END FOR CREDIT CARD-->

                <!-- CHECK Authorize For Donate Part-->
                <div ng-show="typetransact2 == 'ach2' " class="ach" >

                  <div class="form-group row" >
                    <div class="col-md-4">
                      Account Holder Name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.accountname" name="accountname" ng-required="typetransact2 == 'ach2'"/>
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Your bank name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.bankname" name="bankname" ng-required="typetransact2 == 'ach2'"/>
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-12">
                      Enter the routing code and account number as they appear on the bottom of your check:
                    </div>
                    <div class="col-md-12">
                      <img src="/images/template_images/checksample.png">
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Bank Routing Number: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.bankrouting" name="bankrouting" ng-required="typetransact2 == 'ach2'" only-digits/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Bank Account Number: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.bankaccountnumber" name="bankaccountnumber" ng-required="typetransact2 == 'ach2'" only-digits />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Select Account Type:
                    </div>
                    <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                      <input type="radio" name="at" ng-model="userecheckreg.at" value="CHECKING">&nbsp;&nbsp;Checking account<br/>
                      <input type="radio" name="at" ng-model="userecheckreg.at" value="BUSINESSCHECKING">&nbsp;&nbsp;Business checking account<br/>
                      <input type="radio" name="at" ng-model="userecheckreg.at" value="SAVINGS">&nbsp;&nbsp;Savings account
                    </div>
                  </div>

                  <div class="form-group row" >
                    <div class="col-md-12">
                      <input type="checkbox"  ng-model="userecheckreg.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'">*
                      <span style="text-align: center;">
                        By entering my account number about and Clicking authorize, I authorize my payment to be processed
                        as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                        provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                        <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                        authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                      </span>
                    </div>
                  </div>

                  <div class="form-group row" >
                    <div class="col-md-12">
                      <span style="font-weight:bold;">Billing Information</span>
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      First Name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.billingfname" name="billingfname" ng-required="typetransact2 == 'ach2'" />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Last Name: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.billinglname" name="billinglname" ng-required="typetransact2 == 'ach2'" />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Address Line 1: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.al1" name="al1" ng-required="typetransact2 == 'ach2'" />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      Address Line 2:
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.al2" name="al2" />
                    </div>
                  </div>
                  <div class="form-group row" >
                    <div class="col-md-4">
                      City: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.city" name="city" ng-required="typetransact2 == 'ach2'" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      State:
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.state" name="city" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Province:
                    </div>
                    <div class="col-md-8">
                      <input class="form-control" type="text" ng-model="userecheckreg.province" name="province" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      ZIP/Postal Code: *
                    </div>
                    <div class="col-md-8">
                      <input class="form-control " type="text" ng-model="userecheckreg.zip" name="zip" ng-required="typetransact2 == 'ach2'" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      Country: *
                    </div>
                    <div class="col-md-8">
                      <select ng-model="userecheckreg.country" ng-init="userecheckreg.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'">
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <!-- <div class="col-md-12">
                      <span style="margin-left:0px;">
                        <input type="checkbox" name="recurpayment2" ng-model="userecheckreg.recurpayment2" value="recurpayment2">
                      </span>
                      <span style="font-weight:bold;"> &nbsp; I would like to make monthly recurring donations.</span>
                    </div> -->
                  </div>

                  <div ng-if="userecheckreg.recurpayment2">
                    <!-- <div class="form-group row" style="padding-left:15px;" ng-hide="true">
                      <div class="col-md-4">
                        Start Date: *
                      </div>
                      <div class="col-md-8" ng-hide="true">
                        <div class="row">
                          <div class="col-md-4">
                            <span>Year</span><br/>
                            <select ng-model="userecheckreg.start_year" class="form-control " ng-options="y.val for y in donyear"  ng-required="userecheckreg.recurpayment2" style="padding-left:3px;"></select>
                          </div>
                          <div class="col-md-4" >
                            <span>Month</span><br/>
                            <select ng-init="userecheckreg.start_month = donmonth[<?php echo date("m") - 1; ?>]" ng-model="userecheckreg.start_month" class="form-control" ng-options="m.val for m in donmonth" ng-required="userecheckreg.recurpayment2" style="padding-left:3px;"></select>
                          </div>
                          <div class="col-md-4" >
                            <span>Day</span><br/>
                            <select ng-init="userecheckreg.start_day = donday[<?php echo date("d") - 1; ?>]" ng-model="userecheckreg.start_day" class="form-control" ng-options="d.val for d in donday" ng-required="userecheckreg.recurpayment2" style="padding-left:3px;"></select>
                          </div>
                        </div>
                      </div>

                    </div> -->
                    <div class="form-group row" >
                      <div class="col-md-12">How many times would you like this to recur? (including this payment)<br><br></div>
                      <div class="col-md-4">
                        Select Duration: *
                      </div>
                      <div class="col-md-4 ph">
                        <input type="text" class="hidden" ng-init="userecheckreg.reccur_length = 1" name="reccur_length" ng-model="userecheckreg.reccur_length" ng-required="userecheckreg.recurpayment" />
                        <select class="form-control col-md-4" name="reccur_count" ng-model="userecheckreg.reccur_count">
                          <option value="" selected disabled>how many</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                          <option value="9999">never end</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        months
                      </div>

                      <div class="col-md-8" ng-hide="true">
                        <select class="form-control col-md-8" ng-required="userecheckreg.recurpayment" ng-model='userecheckreg.reccur_unit' ng-init="userecheckreg.reccur_unit =  reccur_unit[0]" name="unit" ng-options="ru.name for ru in reccur_unit">
                        </select>
                      </div>
                    </div>

                  </div>

                </div>
                <!-- END of CHECK Authorize For Donate Part-->


                <a id="causes"></a>
                <!-- Paypal Form Payment -->
                <!-- Registration with Paypal -->

                <div  ng-show="typetransact2 == 'paypal2' ">

                  <div class="form-group row" ng-show="typetransact2 == 'paypal2'">
                    <div class="col-md-12">
                      <span style="margin-left:0px;"><input type="checkbox" name="recurpayment3" ng-model="pp.recurpayment3" value="recurpayment3"></span>
                      <span style="font-weight:bold;">  &nbsp; I would like to make monthly recurring donations.</span>
                    </div>
                  </div>

                  <div ng-hide="pp.recurpayment3" class="paypalform">
                    <div style="clear:both; "></div>
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                  </div>

                  <div ng-show="pp.recurpayment3">
                    <div class="form-group row" >
                      <div class="col-md-12">How many times would you like this to recur? (including this payment) <br><br></div>
                      <div class="col-md-4">
                        Select Duration: *<br><br>
                      </div>
                      <div class="col-md-4">
                        <select name="srt" ng-model="pp.srt" class="location form-control m-b" ng-required="pp.recurpayment3">
                          <option value="0">Never End</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                        <input type="hidden" name="a3" ng-model="a3" ng-value="donateamount">
                      </div>
                      <div class="col-md-4">
                        months
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END of Registration with Paypal -->
                <a id="causes"></a>
              </div>




              <span class="formerror" ng-show="user.password != user.repassword" style="color: #fd5555">
                The password you inputed did not match. <br/>
              </span>
              <span class="formerror" ng-show="invalidemail == true" style="color: #fd5555">
                The email you have entered is invalid. <br/>
              </span>
              <div class="form-group" ng-hide="user.password != user.repassword || invalidemail == true">
                <div class="formerror" ng-show="errorpayment!='' && process == false" style="color: #fd5555" >{[{ errorpayment }]} </div>
                <label ng-show="process == true">Processing Registration...</label>



                <button type="button" name="" id="abouteco" class="btn m-b-xs w-xs btn-info" ng-show="process == false"  ng-click="register(user, donateamount,'iCare2020',typetransact2)" ng-disabled="form.$invalid" style="float:right;">Join</button>

              </div>
              <div ng-show="(usernametaken != '' || emailtaken != '' || passmin == true) && process == false">
                <span class="formerror" ng-show="usernametaken != ''" style="color: #fd5555">Username has already been taken.</span> <br/>
                <span class="formerror" ng-show="emailtaken != ''"  style="color: #fd5555">Email already taken.</span>
                <span class="formerror" ng-show="passmin == true" style="color: #fd5555">Shoud be minimum of 6 characters.</span>
              </div>
            </form>



            <!-- END of Registration with Paypal -->


            <form class="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypalsingle">

              <div style="clear:both; ">
                <input type="hidden" name="cmd" value="_donations">
                <input type="hidden" name="business" value="silver.uson@mailinator.com">
                <input type="hidden" name="lc" value="US">
                <input type="hidden" name="custom" id="paypalcustom" ng-value="user.email">
                <input type="hidden" name="item_name" value="Earth Citizens Organization">
                <input type="hidden" name="amount" ng-value="donateamount">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="no_note" value="0">
                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
              </div>

              <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>

            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypalreoccur">
              <input type="hidden" name="cmd" value="_xclick-subscriptions">
              <!--You will need to set the email to your PayPal email or Secure Merchant ID -->
              <input type="hidden" name="custom" id="paypalcustom" ng-value="user.email">
              <input type="hidden" name="business" value="silver.uson@mailinator.com">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="item_name" value="Donate to Earth Citizen Organization">
              <input type="hidden" name="item_number" value="0000">
              <input type="hidden" name="no_note" value="1">
              <input type="hidden" name="no_shipping" value="2">
              <input type="hidden" name="src" value="1">
              <input type="hidden" name="p3" value="1">
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHosted">
              <input type="hidden" name="t3" value="M">
              <input type="hidden" name="srt" ng-value="pp.srt">
              <input type="hidden" name="a3" ng-value="donateamount">
            </form>

          </div>

        </div>
        <div class="col-md-6">
          <div class="gp">

            <!-- <div ng-bind-html="content.content_2"></div> -->

            <p class="phar">Take the ICARE Pledge</p>
            <p>
              I will remember the Earth as the central value for all the choices I make in my daily life.
            </p>
            <p>
              I will practice <em>Mindfully Living with Less</em> and use all the resources, material and financial, that I have saved by practicing it for the cause of recovering the Earth
            </p>
            <p>
              I will recover natural health in my body, mind and spirit and use their strengths to benefit all people and the planet.
            </p>
            <div ng-if="withdon == '1'">
              <p><br><br><br><br>
                <span style="font-weight:bold;">Donate and become an Earth Citizen</span>
              </p>
              <p>              
                <span style="font-weight:bold;">By becoming an Earth Citizen, you will:</span>
                <ul style="list-style: circle;padding-left:17px;">
                  <li>Receive ECO Newsletter</li>
                  <li>Create an Earth Citizen Club</li>
                  <li>Have access to Media Library</li>
                  <li>Join ECO online event</li>
                </ul>
                <br><br>
              </p>
            </div>
            <p>              
              <em>
                By signing up for the pledge, you will be added to our email list and begin to receive information about the Earth Citizen Movement and ICARE 2020 Campaign.
              </em>
            </p>
            <!-- <hr> -->

            <style type="text/css">
              /*#mc_embed_signup{background:#fff; clear:left; border: 1px solid #ABA3A3; padding:15px; font-size: 16px}*/
              /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
              We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
            </style>
            <!-- <div id="mc_embed_signup">
              <form class="ng-pristine ng-valid" action="//earthcitizens.us10.list-manage.com/subscribe/post?u=ee2030630b6c43fa9af0cfb36&amp;id=fc51891e6f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll" class>
                  <p class="phar">
                    E-NEWSLETTER SUBSCRIPTION ONLY <br><span class="thinphar">(without registering as an Earth Citizen)</span>
                  </p>                                

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="mc-field-group form-group">
                        <span for="mce-EMAIL">Email Address:  <span class="asterisk">*</span></span>
                        <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" required>
                      </div>
                      <div class="mc-field-group form-group">
                        <span for="mce-FNAME">First Name: </span>
                        <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
                      </div>
                      <div class="mc-field-group form-group">
                        <span for="mce-LNAME">Last Name: </span>
                        <input type="text" value="" name="LNAME" class="form-control" id="mce-LNAME">
                      </div>
                      <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                      </div>  
                      <div style="position: absolute; left: -5000px;">
                        <input type="text" name="b_ee2030630b6c43fa9af0cfb36_fc51891e6f" tabindex="-1" value="">
                      </div>
                      <div class="clear">
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-sm btn-primary" ng-disabled="mc-embedded-subscribe-form.$invalid" style="float:right;">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div> -->
            <!--End mc_embed_signup-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="eco-col-4 mar-t-50" style="font-style: italic; font-size:16px;color:#777;">
            <p>
              ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

