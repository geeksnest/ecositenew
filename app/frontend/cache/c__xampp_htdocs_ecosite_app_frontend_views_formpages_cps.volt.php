<div style="height: 30px;"></div>
<div class="eco-container"> 
  <div class="row padding-bot30 padding-top30" id="circle-links">
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/curriculum">
        <img src="/images/template_images/curriculum-circle.jpg" class="sublink-img">
        <p class="title1">Curriculum</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/programs">
        <img src="/images/template_images/programs-circle.jpg" class="sublink-img">
        <p class="title1">Programs</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/support-for-graduates">
        <img src="/images/template_images/support-for-graduates-circle.jpg" class="sublink-img">
        <p class="title1">Support for Graduates</p>
      </a>
    </div>
  </div>
</div>