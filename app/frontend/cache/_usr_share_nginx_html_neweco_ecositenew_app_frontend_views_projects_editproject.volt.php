<?php echo $this->getContent() ?>
<a href="" id="errorget"></a>
<a href="" id="donation"></a>
<a href="" id="notification"></a>
<div class="container-fluid" ng-controller="editprojectCtrl">

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a><a href="/#serve">SERVE</a><a><i class='fa fa-arrow-right'></i></a> <a href="/projects">ECO Project</a> <a><i class='fa fa-arrow-right'></i></a> <a href=""><span ng-bind="proj.projTitle"></span></a> 
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <h4>Submit Your ECO Project</h4>
      </div>
      <div class="col-sm-12">
       Do you have a great idea for an outreach project that would help a cause that you feel passionate about? Fill out the application form below to have ECO help initiate the support you need in making a difference.
      </div>
      <div class="col-sm-12" ng-show="proj.projStatus == '3'">
        <div class="line line-dashed b-b line-lg"></div>
        <alert type="danger">
          This project is declined by the Eco Board.<br><br>
          Reason: <span ng-bind="proj.msg"></span>
        </alert>
        <div class="line line-dashed b-b line-lg"></div>
      </div>
      <div class="col-sm-12">
        <div class="panel-body">
          <alert ng-repeat="resultget in resultget" type="{[{resultget.type }]}" close="closeAlert($index)">
            <span class="" ng-bind="resultget.msg"></span>            
            <div ng-repeat="cNTS in cNTS">
              - <span ng-bind="cNTS.msg"></span><br>
            </div>
          </alert>
          <div ng-show="isLoading" class="load-more-loading">
            <div class="sk-fading-circle">
              <div class="sk-circle1 sk-circle"></div>
              <div class="sk-circle2 sk-circle"></div>
              <div class="sk-circle3 sk-circle"></div>
              <div class="sk-circle4 sk-circle"></div>
              <div class="sk-circle5 sk-circle"></div>
              <div class="sk-circle6 sk-circle"></div>
              <div class="sk-circle7 sk-circle"></div>
              <div class="sk-circle8 sk-circle"></div>
              <div class="sk-circle9 sk-circle"></div>
              <div class="sk-circle10 sk-circle"></div>
              <div class="sk-circle11 sk-circle"></div>
              <div class="sk-circle12 sk-circle"></div>
            </div> 
            <span ng-bind="botMsg"></span>            
          </div>
          <div ng-cloak class="form-group wrapper-sm bg-success" ng-show="successget">
            <span class="" ng-bind="successget"></span>
          </div>  
        </div>
      </div>
      <div class="col-sm-12" ng-if="proj.projStatus == '0'">
        <div class="proj-panel panel panel-success">
          <div class="proj-panel-heading panel-heading">
            You have submitted this project, please wait for the approval.
          </div>
        </div>
      </div>
    </div>
    <ul class="nav nav-tabs">
      <li><a href="#item1" data-toggle="tab" ng-click="socialz=false">Basic <label class="req">*</label></a></li>
      <li><a href="#item2" data-toggle="tab" ng-click="socialz=false">Story <label class="req">*</label></a></li>
      <li><a href="#mailing" data-toggle="tab" ng-click="socialz=false">Mailing Address <label class="req">*</label></a></li>
      <li><a href="#item3" data-toggle="tab" ng-click="socialz=true">Others <label class="req">*</label></a></li>
      <li ng-show="proj.projStatus == '2' || proj.projStatus == '5'"><a href="#item4" data-toggle="tab" ng-click="socialz=false">My Donations</a></li>
      <li class="litab" ng-if="proj.projStatus == '4' || proj.projStatus == '3' || proj.projStatus == '0' ">
        <button type="submit" class="btn btn-info" ng-click="saveProj(proj, files, type)">Save Project</button>
      </li>

      <!-- <li class="litab" ng-show="proj.projStatus == '1' || proj.projStatus == '2'">
        <button type="submit" class="btn btn-info" ng-show="socialz" ng-click="updateSocial(proj)">Update Social Info</button>
      </li> -->

      <li class="litab" ng-if="proj.projStatus == '4' || proj.projStatus == '3'">
        <button type="submit" class="btn btn-success"  ng-click="submitProj(proj,proj.projID,thumbchanged)">Submit Project</button>
      </li>

      <li class="litab" ng-if="proj.projStatus == '1'">
        <button type="submit" class="btn btn-success"  ng-click="publish(proj,proj.projID)">Publish Now</button>
      </li>


    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="item1">

        <div class="row create-form">
          <div class="col-sm-10">
            <div class="panel panel-default">


              <div ng-show="loadingg == true">
                <div id="overlay">
                  <div id="loading">
                    <div class="three-quarters-loader"></div><div><span ng-bind="loadingMsg"></span></div>

                  </div>
                </div>
              </div>
              <div ng-disabled="proj.projStatus == '1' || proj.projStatus == '2' || proj.projStatus == '5'">
                <div class="panel-body">
                  <div class="form-group">
                    <input type="hidden" ng-model="proj.projAuthorID"/>
                    <input type="hidden" ng-model="proj.projID"/>
                    Project Title <label class="req">*</label>
                    <input type="text" class="form-control" ng-model="proj.projTitle" id="projTitle"  name="projTitle">
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                    Short Description <label class="req">*</label>
                    <textarea class="form-control textarea" ng-model="proj.projShortDesc" id="projShortDesc" name="projShortDesc" maxlength="200"></textarea>
                  </div>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                    Project Location <label class="req">*</label>
                    <select class="location form-control" ng-model="proj.projLoc" id="projLoc" name="projLoc">
                      <option ng-repeat="cn in countries" value="{[{cn.name}]}, {[{cn.code}]}">
                        <span>{[{cn.name}]}, {[{cn.code}]}</span>
                      </option>
                    </select>
                  </div>

                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group">
                    Fundraising Duration <label class="req">*</label>
                    <div class="radio">
                      <label class="i-checks">
                        <input type="radio" id="type" name="type" value="days" ng-model="type" ng-checked="type == 'days'"><i></i> Number of Days &nbsp;&nbsp;&nbsp;
                      </label>
                      <label class="i-checks">
                        <input type="radio" id="type" name="type" value="date" ng-model="type"><i></i> End on Date & Time
                      </label>
                    </div>
                    <div ng-show="type == 'days'">
                      <span class="text-muted">1-60 days, we recommend 30 or less</span>
                      <input type="text" class="form-control don5t10n" ng-model="proj.days" id="days" name="days" only-digits  ng-change="limitDays(proj.days)">
                    </div>

                    <div ng-show="type == 'date'" class="row">
                      <div class="col-md-6">
                        <span class="text-muted">Select date here.</span>
                        <div class="input-group w-md">
                          <span class="input-group-btn">
                            <input type="hidden" ng-model="proj.datedue">
                            <input id="datedue" name="datedue" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="proj.datedue" is-open="opened" datepicker-options="dateOptions" min-date="minDate"  ng-required="true" close-text="Close" type="text" disabled>
                            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon icon-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <br>
                        <timepicker-pop input-time="time1" class="input-group" disabled="disabled"
                        show-meridian='showMeridian'> </timepicker-pop>
                      </div>

                    </div>
                    <div class="line line-dashed b-b line-lg"></div>
                    <div style="height:0px"><input type="text" id="left" style="opacity:0;
                    filter:alpha(opacity=0);"></div>
                    <div class="form-group">
                      Fundraising Goal <label class="req">*</label><br>
                      <span class="f-12">
                      Limited to $5,000. If the budget for your project exceeds $5,000, please contact us at <a href="mailto:support@earthcitizens.org">support@earthcitizens.org</a> 
                      </span>
                      <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control don5t10n" ng-model="proj.projGoal" id="projGoal" name="projGoal" only-digits ng-change="limitAmount(proj.projGoal)">

                      </div>
                    </div>

                    <div class="form-group">
                      Project Image <label class="req">*</label>

                      <div class="row">
                        <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                          <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlertz($index)">{[{ imgAlerts.msg }]}</alert>
                        </div>
                        <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                          <div class="line line-dashed b-b line-lg"></div>
                          <input type="hidden" ng-model="proj.projThumb"/>
                          <img src="{[{amazonlink}]}/uploads/projects/{[{proj.projThumb}]}">
                        </div>
                        <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == true">
                          <div class="line line-dashed b-b line-lg"></div>
                          <img ngf-src="projImg[0]">
                        </div>
                        <div class="col-sm-12 propic create-proj-thumb" ng-hide="proj.projStatus == '1' || proj.projStatus == '2' || proj.projStatus == '5'">
                          <div class="label_profile_pic border-dash browse-img-wrap" id="change-picture" ngf-change="prepare(files)" accept='image/*' ngf-select ng-model="files" ngf-multiple="false" id="files" name="files" required="required">
                            <a href="">Choose an image from your computer</a><br>
                            <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                            <label>At least 1024x768 pixels</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>

      <div class="tab-pane" id="item2">
        <div class="row create-form">
          <div class="col-sm-10">
            <p ng-bind-html="projDesc" ng-if="proj.projStatus == '1' || proj.projStatus == '2' || proj.projStatus == '5'"></p>
            <div class="panel panel-default" ng-hide="proj.projStatus == '1' || proj.projStatus == '2' || proj.projStatus == '5'">
              <div class="panel-body">
                <div>
                  Write your story here. <label class="req">*</label><br>
                  <div ckeditor="options" ng-model="proj.projDesc"  ready="onReady()"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="mailing">
        <div class="row create-form">
          <div class="col-sm-10">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  Contact Name <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="proj.projCName" id="projCName"  name="projCName">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Address 1 <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="proj.projCAddress1" id="projCAddress1"  name="projCAddress1">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Address 2
                  <input type="text" class="form-control" ng-model="proj.projCAddress2" id="projCAddress2"  name="projCAddress2">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  City <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="proj.projCCity" id="projCCity"  name="projCCity">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  State <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="proj.projCState" id="projCState"  name="projCState">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Zip/Postal Code <label class="req">*</label>
                  <input type="text" class="form-control" ng-model="proj.projCZip" id="projCZip"  name="projCZip" maxlength="10">
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  Country <label class="req">*</label>
                    <select class="location form-control" ng-model="proj.projCCountry" id="projCCountry" name="projCCountry">
                      <option ng-repeat="pcn in countries" value="{[{pcn.name}]}, {[{pcn.code}]}">
                        <span>{[{pcn.name}]}, {[{pcn.code}]}</span>
                      </option>
                    </select>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="item3">
        <div class="row create-form">
          <div class="col-sm-10">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  Website:
                  <input type="text" class="form-control" ng-model="proj.projWeb"  />
                </div>
                <div class="form-group for-more">
                  Social Media:

                  <ul class="soc">
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-facebook custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.facebook" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-youtube custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.youtube" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-twitter custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.twitter" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-google custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.google" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-pinterest custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.pinterest" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-linkedin custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.linkedin" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-tumblr custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.tumblr" ></span>
                      </div>
                    </li>
                    <li>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <a class="soc-instagram custom" href=""></a>
                        </span>
                        <span><input type="text" class="form-control" ng-model="proj.instagram" ></span>
                      </div>
                    </li>
                  </ul>

                </div>
                <footer class="panel-footer text-right bg-light lter" ng-if="proj.projStatus != '5'">
                  <button type="submit" class="btn btn-info"  ng-click="updateSocial(proj)">Update Social Info</button>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="item4">
        <div class="row create-form">
          <div class="col-sm-10">
            <div class="panel panel-success">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-sm-4">
                    Total Donors: <span ng-bind="donCount.donors"></span>
                  </div>
                  <div class="col-sm-3">
                    Donations: $<span ng-bind="donCount.total | putcomma"></span>
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
              </div>
              <div class="list-group bg-white">
                <div class="list-group-item" ng-repeat="don in don | orderBy: 'datetimestamp':true">
                  <div class="row">
                    <div class="col-sm-4">
                      <span ng-bind="don.billinginfofname"></span> <span ng-bind="don.billinginfolname"></span>
                    </div>
                    <div class="col-sm-3">                      
                      $<span ng-bind="don.amount"></span>
                    </div>
                    <div class="col-sm-2">
                      <img ng-if="don.paymentmode == 'CreditCard'" class="card-icons" src="images/creditcards/credit_card-icon.png">
                      <img ng-if="don.paymentmode == 'e-Check'" class="card-icons" src="images/creditcards/e_check-icon.png">
                      <img ng-if="don.paymentmode == 'Paypal'" class="card-icons" src="images/creditcards/paypal-icon.png">
                    </div>
                    <div class="col-sm-3">
                      <span ng-bind="don.datetimestamp | dateToWord"></span>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>

  </div>
</div>


<!-- -->