<div class="container-fluid" ng-controller="categorylistCtrl">
  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">LEARN</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary">Media library</a> <a><i class='fa fa-arrow-right'></i></a> <a href=""> Category/<span ng-bind="catname"></span></a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-9">        

        <div class="row">
          <div class="col-sm-12">
            <h4>Category | <span ng-bind="catname"></span> </h4><br>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div ng-repeat="videos in data" class="col-sm-4 cursor-pointer hnv">
              <a href="/medialibrary/{[{videos.slugs}]}" class="cursor-pointer no-dec">
                <div ng-if="videos.videotype == 'embed'" style="background-image: url({[{ videos.embed | returnYoutubeThumb }]});width:100%;height:144px;background-size: cover;">
                  <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
                </div>
                <div ng-if="videos.videotype == 'upload'" style="background-image: url({[{ amazonlink }]}/videos/thumbs/{[{ videos.videothumb }]});width:100%;height:144px;background-size: cover;">
                  <div class="youtube-play" style="background-image: url(/images/icons/play-icon-jw.png);position:inherit !important;"></div>
                </div>
              </a>
              <a href="/medialibrary/{[{videos.slugs}]}" class="cursor-pointer no-dec">
                <div class="hnv-title"><b><span ng-bind="videos.title"></span></b></div>
              </a>
            </div>
          </div>
        </div>
        <div class="load-more-div">
          <button ng-hide="loading" class="proj-list-show-more" ng-click="loadmorebot()" ng-disabled="loading"><span>Load More</span>
          </button>
          <div ng-show="loadingGIF" class="load-more-loading">
            <div class="sk-fading-circle">
              <div class="sk-circle1 sk-circle"></div>
              <div class="sk-circle2 sk-circle"></div>
              <div class="sk-circle3 sk-circle"></div>
              <div class="sk-circle4 sk-circle"></div>
              <div class="sk-circle5 sk-circle"></div>
              <div class="sk-circle6 sk-circle"></div>
              <div class="sk-circle7 sk-circle"></div>
              <div class="sk-circle8 sk-circle"></div>
              <div class="sk-circle9 sk-circle"></div>
              <div class="sk-circle10 sk-circle"></div>
              <div class="sk-circle11 sk-circle"></div>
              <div class="sk-circle12 sk-circle"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <span class="title2">Donate Your Content</span><br><br>
        <p>
          Your content can help inspire thousands of promising leaders for a more sustainable future of our world. Please consider donating your content here so that it can benefit our leaders and public who care for people and the planet.
        </p>
        <b>By donating your content:</b><br><br>
        <p>
          - Your name will be recognized as contributor to our cause
          <br><br>
          - You grant ECO a non-exclusive license to use your content for a year for our media library and online education
          <br><br>
          - ECO will provide link to your information so that interested people may work directly with you.
        </p>
        <div class="yellow-button-slim"><a href="/medialibrary/addvideo">Donate Your Content</a></div> 
        <hr>
        <span class="title2">Categories</span>
        <ul class="sidebar">
          <li class="pad">
            <a href="<?php echo $this->config->application->baseURL; ?>/medialibrary">All</a><br/>
          </li>
          <?php
          foreach ($liftofcategory as $category ) {
            ?>
            <li class="pad">
              <a href="<?php echo '/medialibrary/category/'.$category->categoryslugs; ?>"><?php echo $category->categoryname; ?></a><br/>
            </li>
            <?php
          }

          ?>
        </ul>
        <!-- <hr> -->
        <br>
        <span class="title2">Tags </span>
        <div class="listCont">
          <div class="tagsL">
            <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { 
              ?>
              <a href="/medialibrary/tag/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?>
              </a>
              <?php
              $count > 5 ? $count =1 : $count++;
            } ?>
          </div>
        </div>
        <!-- <hr> -->
        <br>
        <span class="title2">Archives</span>
        <ul class="sidebar">
          <?php
          $getarchives = $archivelist;
          foreach ($getarchives as $key => $value) { ?>
          <li class="pad">
            <a href="/medialibrary/archives/<?php echo $getarchives[$key]->month . "-" . $getarchives[$key]->year;?>">
              <span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></span>
            </a>
          </li>
          <?php } ?>
        </ul>
        <!-- <hr> -->        
      </div>
    </div>


  </div>
</div>