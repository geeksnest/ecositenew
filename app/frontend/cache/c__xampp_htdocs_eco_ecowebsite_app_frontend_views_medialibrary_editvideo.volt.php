<a href="" id="errorget"></a>
<a href="" id="successget"></a>
<div class="container-fluid" ng-controller="editvideoCtrl">

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/#learn">Learn</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/medialibrary/mymedialibrary">My Media library</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="">Edit Video</a> 
      </div>
    </div>
  </div>

  <div class="eco-container eco-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h4>Update Your Video</h4>
      </div>
      <div class="col-sm-12">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eu quam erat. Vivamus sed neque nec nibh aliquet euismod. Donec scelerisque facilisis nulla bibendum maximus.
      </div>
    </div>
    <div class="create-form">
      <div class="row"> 
        <div class="col-sm-12" ng-show="media.status == 'Disapproved'">
          <div class="line line-dashed b-b line-lg"></div>
          <alert type="danger">
            This Video was declined by the Eco Board.<br><br>
            Reason: <span ng-bind="media.disapprovemsg"></span>
          </alert>
          <div class="line line-dashed b-b line-lg"></div>
        </div>  
      </div>  
      <!-- <div class="row">  

        <div class="col-sm-8">  -->

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-danger" ng-show="errorget">
                <span class="" ng-bind="errorget"></span>
              </div>
              <div class="form-group wrapper-sm bg-success" ng-show="successget">
                <span class="" ng-bind="successget"></span>
              </div> 
              <div class="form-group">

                <alert ng-repeat="resultget in resultget" type="{[{resultget.type }]}" close="closeAlert1($index)">
                  <span class="" ng-bind="resultget.msg"></span>            
                  <div ng-repeat="cNTS in cNTS">
                    - <span ng-bind="cNTS.msg"></span><br>
                  </div>
                </alert>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="row">               
                  <div class="col-sm-12 create-proj-thumb" ng-if="media.videotype == 'embed'">                  
                    <div class="vidoe-width">
                      <label>Video</label> <br> 
                      <div ng-bind-html="preview"></div>
                    </div>
                  </div>               
                  <div class="col-sm-6 create-proj-thumb" ng-show="media.videotype == 'upload'"> 
                      <label>Video</label> <br> 
                      <div class="vidoe-play-ml">
                        <div id="myElement">Loading the player...</div>
                      </div>
                  </div>
                  <div class="col-sm-6 create-proj-thumb" ng-show="media.videotype == 'upload'">
                    <label>Thumbnail</label> <br> 
                    <alert ng-repeat="videoAlerts in videoAlerts" type="{[{videoAlerts.type }]}" close="closeAlert($index)">{[{ videoAlerts.msg }]}</alert>
                    <img class="vidoe-play-ml" ngf-src="thumbImg" ng-if="imageselected == true">
                    <img class="vidoe-play-ml" ng-src="{[{ amazonlink }]}/videos/thumbs/{[{media.videothumb}]}" ng-if="imageselected == false">
                  </div>  
                  <div class="col-sm-12 create-proj-thumb">
                    <div class="line line-dashed b-b line-lg"></div>
                  </div> 
                  <div class="col-sm-12 create-proj-thumb" ng-show="media.videotype == 'upload'"> 
                    <div class="label_profile_pic pointer-wrap" id="change-video" ngf-change="prepareThumb(filesT)" ngf-select ng-model="filesT" ngf-multiple="false" required="required">
                      <img class="video-icon" src="/images/icons/thumbnail-icon.png"><br>
                      <label>Click here to change video thumbnail</label>
                      <br>
                    </div>
                  </div>  
                </div>
                <div class="line line-dashed b-b line-lg"></div>
              </div>

              <div class="form-group">
                <input type="hidden" ng-init="media.authorid = userAgent" ng-model="media.authorid"/>
                Title <label class="req">*</label>
                <input type="text" class="form-control" ng-model="media.title" name="title" id="title" required/>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                Short Description <label class="req">*</label>
                <textarea class="form-control textarea" rows="4" ng-model="media.shortdesc" name="shortdesc" id="shortdesc" maxlength="200"></textarea>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                Description <label class="req">*</label>
                <div ckeditor="options" ng-model="media.description" name="description" id="description" ready="onReady()"></div>
                <!-- <textarea class="ck-editor" ng-model="media.description" name="description" id="description"></textarea> -->
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group" id="chosen">
                Tags <label class="req">*</label>
                <ui-select multiple tagging tagging-label="false" ng-model="media.tag" name="tag" id="tag" theme="bootstrap" ng-disabled="disabled">
                  <ui-select-match placeholder="Select Tags" style="padding-left:5px;">{[{ $item }]}</ui-select-match>
                  <ui-select-choices repeat="ta in tag">{[{ ta }]}</ui-select-choices>
                </ui-select>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group" id="chosen">
                Category <label class="req">*</label>
                <select chosen multiple options="category" class="form-control m-b" ng-options="cat as cat.categoryname for cat in category track by cat.categoryid" ng-model="media.category" name="category" id="category">
                </select> 
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              

              <div class="form-group">
                <div class="col-md-12 center" ng-show="loadingg == true">
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                  <span ng-bind="loadingMsg"></span>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                  <button type="submit" class="btn btn-info" ng-if="media.status != 'Disapproved'" ng-click="save(media,'1',thumbImg,videoname)">Save Video</button>
                  <button type="submit" class="btn btn-info" ng-if="media.status == 'Disapproved'" ng-click="save(media,'2',thumbImg,videoname)">Resubmit Video</button>
                </footer>
              </div>
            </div>

          </div>
        <!-- </div>
      </div> -->
    </div>
  </div>
</div>