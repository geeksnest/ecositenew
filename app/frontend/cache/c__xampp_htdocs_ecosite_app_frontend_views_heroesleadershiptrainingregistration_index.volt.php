


<div class="donation-fluid" style="width: 100%;padding-bottom: 50px;height: 300px;background: url(https://earthcitizens.s3.amazonaws.com/uploads/pageimages/rsz_c62a9ce9fb1eb282d4936262902d5ec22a6e15a77fe322168bpimgpsh_fullsize_distr.jpg) no-repeat 50% 43% !important;background-size: cover !important; position:relative;">
    <div id="interior" class="row eco-container">
        
    </div>
    <div class="banner-content-wrapper-green">
        <div>
            <font class="mtitle2">Welcome to HEROSHIP</font> <br>
            <p class="mbody2" style="padding-bottom:0px;">Find the HERO within and develop your greatness.</p>
        </div>
    </div>
</div>


<!--===begin container===-->
<div  ng-controller='hltCtrl' class="container-fluid"  style="margin-top:30px;">
    <div class="eco-container" >
        <div class="wrapper-md " >
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group" style="padding-left:0px;padding-top:0px;">
                        <span class="donate-title2">REGISTER NOW</span><br/><br/>
                        <p style="font-size:14px;margin-bottom:0px;" ng-show="reccur_success==2"> Your transaction is now being processed. </p>
                        <p style="font-size:16px;margin-bottom:0px;" ng-show="reccur_success==3"> {[{ reccur_message }]} <br/><br/>
                            <a href="" ng-click="back_click()" ng-show="back_button==2 || back_button==3" >Back to form </a>
                        </p>
                    </div>
                <div ng-show="reccur_success==1" class="form-group col-lg-12"  style="padding:0px;margin-top:0px;">

                <div style="padding-left:0px">

                    <div class="form-group col-sm-12" style="padding-left:0px">

                        <span style="font-size:16px;">
                        <h4>All Inclusive: $ 300.00</h4><br/></span>
                        <input type="hidden" ng-model="inclusivepay" ng-init="inclusivepay = '10'">
                    </div>



                    <div class="form-group row" >
                        <div class="col-sm-4">
                            <span style="font-size:16px;">
                                Choose Training Date:<br/>
                            </span>
                        </div>
                        <div class="col-sm-8">
                            <select name="trainingDate" class="form-control" ng-model="trainingDate">
                                <option value=''>program dates</option>
                                <option value='Wed,*Jan*6*–*Sun,*Jan*10,*2016'>Wed, Jan 6 – Sun, Jan 10, 2016</option>
                                <option value='Sun,*Feb*7*–*Thu,*Feb*11,*2016'>Sun, Feb 7 – Thu, Feb 11, 2016</option>
                                <option value='Sun,*Mar*6*–*Thu,*Mar*10,*2016'>Sun, Mar 6 – Thu, Mar 10, 2016</option>
                                <option value='Sun,*Apr*17*–*Thu,*Apr*21,*2016'>Sun, Apr 17 – Thu, Apr 21, 2016</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group row" >
                        <div class="col-md-12">
                            <span class="donate-title1" style="color:#000000;">Participant Information</span><br/><br/>
                        </div>
                    </div>

                    <div>
                        <div class="form-group row" >
                            <div class="col-md-4">
                                First Name: *
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.fname" name="fname" required/>
                            </div>
                        </div>
                        <div class="form-group row" >
                            <div class="col-md-4">
                                Last Name: *
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.lname" name="lname" required="required"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                Address Line 1: *
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.address1" name="al1" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                Address Line 2:
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.address2" name="al2"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                City: *
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.city" name="city" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                State:
                            </div>
                            <div class="col-md-8">
                             <input class="form-control " type="text" ng-model="user.state" name="city"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                ZIP/Postal Code: *
                            </div>
                            <div class="col-md-8">
                                <input class="form-control " type="text" ng-model="user.userzip" name="zip" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                Country: *
                            </div>
                            <div class="col-md-8">
                                <select ng-model="user.country" ng-init="user.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" >
                            <div class="col-md-4">
                                Email Address: *
                            </div>
                            <div class="col-md-8">
                                <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="emailcheck(user.email)" required/>
                                <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                            </div>
                        </div>
                        <div class="form-group">
                            How did you learn about the HEROES Leadership Training?
                            <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
                                <option value="ECO event">ECO event</option>
                                <option value="ECO Program Graduates">ECO Program Graduates</option>
                                <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                <option value="Invitation Email">Invitation Email</option>
                            </select>
                        </div>
                        <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                            Center Name
                            <select class="location form-control m-b" ng-model="user.cname" required="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                            </select>
                        </div>
                    </div>

<!--  DONATE PART FOR OTHERS -->

                <div class="reganddonate" style="margin-top:20px;">

                    <div style="margin-bottom:10px;">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/>
                            </div>
                        </div>
                        <div class="form-group row" >
                            <div class="col-md-4">
                                Payment Method:
                            </div>
                            <div class="col-md-8">
                                <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                                    <option value=''>Credit Card</option>
                                    <option value='ach2'>Checking/Savings Account</option>
                                    <option value='paypal2'>PayPal</option>
                                </select>
                            </div>
                        </div>

                    </div>


<!-- CREDIT CARD  BILLING FOR DONATION PART -->
                    <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
                        <br/>
                        <form name="formCredit" id="formCredit" method="post" action="">



                            <div class="form-group row" >
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <img src="/images/template_images/visa.png" style="width:65px;">
                                    <img src="/images/template_images/amex.png" style="width:65px;">
                                    <img src="/images/template_images/mastercard.png" style="width:65px;">
                                    <img src="/images/template_images/discover.png" style="width:65px;">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Credit Card Number: *
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" ng-model="cc.ccn" name="" required only-digits />
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    CVV Number: *
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control " ng-model="cc.cvvn" name="" required only-digits />
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Credit Card Expiration: *
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <span>Month</span><br/>
                                            <select ng-model="cc.expiremonth" class="form-control" required="required">
                                              <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                                              <?php foreach ($formonths as $index => $formonth) {
                                                echo "<option value='".$index."'>".$formonth."</option>";
                                              }?>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <span>Year</span><br/>
                                            <select ng-model="cc.expireyear" class="form-control" required="required">
                                              <?php for ($year=date('Y'); $year < 2050; $year++) {
                                                echo "<option value='".$year."'>".$year."</option>";
                                              }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-12">
                                    <label class="i-checks">
                                        <input type="checkbox" ng-model="ccInfo" ng-click="ccBillInfo(user,cc)">
                                        <i></i>
                                        My Billing Information is the same as Participant Information
                                    </label>
                                </div>
                            </div>

                            <div>
                                <div class="form-group row" >
                                    <div class="col-md-12">
                                        <span style="font-weight:bold;">Billing Information</span>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        First Name: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.billingfname" name="billingfname" required/>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Last Name: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.billinglname" name="billinglname" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Address Line 1: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.al1" name="al1" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Address Line 2:
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.al2" name="al2"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        City: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.city" name="city" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        State:
                                    </div>
                                    <div class="col-md-8">
                                       <input class="form-control " type="text" ng-model="cc.state" name="state"/>
                                   </div>
                               </div>
                               <div class="form-group row">
                                <div class="col-md-4">
                                    ZIP/Postal Code: *
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" ng-model="cc.zip" name="zip" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Country: *
                                </div>
                                <div class="col-md-8">
                                    <select ng-model="cc.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Email Address: *
                                </div>
                                <div class="col-md-8">
                                    <input type="email" class="form-control" ng-model="cc.billingemail" name="email" ng-change="billingemailcheck(cc.billingemail)" required/>
                                    <span class="formerror" ng-if="billingemailcheckinvalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                How did you learn about the HEROES Leadership Training?
                                <select class="form-control m-b" ng-model="cc.howdidyoulearn" ng-change="changeme()">
                                    <option value="ECO event">ECO event</option>
                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                    <option value="Invitation Email">Invitation Email</option>
                                </select>
                            </div>
                            <div class="form-group" ng-if="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                Center Name
                                <select class="location form-control m-b" ng-model="cc.cname" required="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                </select>
                            </div> -->
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12" ng-hide="invalidemail == true">
                                <button type="button" class="btn btn-success btn-addon btn-lg" ng-show="reccur_success == 1"  ng-click="submitcredit(cc,user)" ng-disabled="formCredit.$invalid || trainingDate == '' || trainingDate == null" style="float:right;"><i class="fa fa-pagelines"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                    </div>
<!-- END FOR CREDIT CARD-->

<!-- CHECK Authorize For Donate Part-->
                    <div ng-show="typetransact2 == 'ach2' " class="ach" >
                    <br/>

                        <form name="formAuthorizeCheck" id="formAuthorize" method="post" action="">

                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Account Holder Name: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.accountname" name="accountname" required/>

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Your bank name: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankname" name="bankname" required/>

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-12">
                                    Enter the routing code and account number as they appear on the bottom of your check:
                                </div>
                                <div class="col-md-12">
                                    <img src="/images/template_images/checksample.png">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Bank Routing Number: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankrouting" name="bankrouting" required only-digits/>

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Bank Account Number: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankaccountnumber" name="bankaccountnumber" required only-digits />

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Select Account Type:
                                </div>
                                <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                                    <input type="radio" name="at" ng-model="check.at" value="CHECKING" required="required">&nbsp;&nbsp;Checking account<br/>
                                    <input type="radio" name="at" ng-model="check.at" value="BUSINESSCHECKING" required="required">&nbsp;&nbsp;Business checking account<br/>
                                    <input type="radio" name="at" ng-model="check.at" value="SAVINGS" required="required">&nbsp;&nbsp;Savings account
                                </div>
                            </div>

                            <div class="form-group row" >
                                <div class="col-md-12">

                                    <input type="checkbox"  ng-model="check.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'" required>*

                                    <span style="text-align: center;">
                                        By entering my account number about and Clicking authorize, I authorize my payment to be processed
                                        as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                                        provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                                        <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                                        authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                                    </span>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <div class="col-md-12">
                                    <label class="i-checks">
                                        <input type="checkbox" ng-model="checkInfo" ng-click="checkBillInfo(user,check)">
                                        <i></i>
                                        My Billing Information is the same as Participant Information
                                    </label>
                                </div>
                            </div>

                            <div>
                                <div class="form-group row" >
                                    <div class="col-md-12">
                                        <span style="font-weight:bold;">Billing Information</span>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        First Name: *
                                    </div>
                                    <div class="col-md-8">

                                        <input class="form-control " type="text" ng-model="check.billingfname" name="billingfname"  required/>

                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Last Name: *
                                    </div>
                                    <div class="col-md-8">

                                        <input class="form-control " type="text" ng-model="check.billinglname" name="billinglname"  required/>

                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Address Line 1: *
                                    </div>
                                    <div class="col-md-8">

                                        <input class="form-control " type="text" ng-model="check.al1" name="al1"  required/>

                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Address Line 2:
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="check.al2" name="al2" />
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        City: *
                                    </div>
                                    <div class="col-md-8">

                                        <input class="form-control " type="text" ng-model="check.city" name="city"  required/>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        State:
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="check.state" name="state" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Province:
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="check.province" name="province" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        ZIP/Postal Code: *
                                    </div>
                                    <div class="col-md-8">

                                        <input class="form-control " type="text" ng-model="check.zip" name="zip" required/>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Country: *
                                    </div>
                                    <div class="col-md-8">

                                        <select ng-model="check.country" ng-init="check.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'" required>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Email Address: *
                                    </div>
                                    <div class="col-md-8">
                                        <input type="email" class="form-control" ng-model="check.billingemail" name="email" ng-change="billingemailcheck(check.billingemail)" required/>
                                        <span class="formerror" ng-if="billingemailcheckinvalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                How did you learn about the HEROES Leadership Training?
                                <select class="form-control m-b" ng-model="check.howdidyoulearn" ng-change="changeme()">
                                    <option value="ECO event">ECO event</option>
                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                    <option value="Invitation Email">Invitation Email</option>
                                </select>
                            </div>
                            <div class="form-group" ng-if="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                Center Name
                                <select class="location form-control m-b" ng-model="check.cname" required="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                </select>
                            </div> -->

                            <div class="form-group row" style="padding-left:15px;">
                                <div class="col-md-12" ng-hide="invalidemail == true">
                                    <button type="button" ng-click="submitecheck(check,user)" class="btn btn-success btn-addon btn-lg" ng-disabled="formAuthorizeCheck.$invalid || trainingDate == '' || trainingDate == null" style="float:right;"><i class="fa fa-pagelines"></i> Submit</button>
                                </div>
                                
                            </div>


                        </form>
                    </div>
<!-- END of CHECK Authorize For Donate Part-->


                    <a id="causes"></a>
<!-- Paypal Form Payment -->


                    <div  ng-show="typetransact2 == 'paypal2' ">
                    <br/>
                        <form class="paypalform" name="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypalsingle">

                            <div style="clear:both; ">
                                <input type="hidden" name="cmd" value="_donations">
                                <input type="hidden" name="business" value="accounting@earthcitizens.org">
                                <input type="hidden" name="lc" value="US">
                                <input type="hidden" name="custom" id="paypalcustom" ng-model="paypalcustom" value="">
                                <input type="hidden" name="item_name" value="HEROES Leadership Training">
                                <input type="hidden" name="amount" ng-value="inclusivepay">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="no_note" value="0">
                                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                            </div>
                            <!-- <div class="form-group">
                                How did you learn about the HEROES Leadership Training?
                                <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
                                    <option value="ECO event">ECO event</option>
                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                    <option value="Invitation Email">Invitation Email</option>
                                </select>
                            </div>
                            <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                Center Name
                                <select class="location form-control m-b" ng-model="user.cname" required="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                </select>
                            </div> -->


                            <div ng-hide="paypalbutton">

                                <a href="" ng-click="submitpaypal(user.howdidyoulearn, user.cname)" ng-hide="paypalform.$invalid || trainingDate == '' || trainingDate == null"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="paypalbutton" alt="PayPal - The safer, easier way to pay online!" style="float:right;"></a>

                            </div>

                        </form>
                        <!--<form name="formPaypal" id="formPaypal" method="post" action="">   -->

                            <!--<div style="clear:both; ">-->
                                <!--<div class="form-group row" style="padding-left:15px;">-->
                                    <!--<div class="col-md-12">-->
                                        <!--<input  type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" ng-hide="donateamount < 20" style="float:right;">-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->

                            <!--<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->

                        <!--</form>-->

                    </div>

<!-- END of Paypal -->


                    <a id="causes"></a>


                </div>



                    </div>





<!-- END of Registration with Paypal -->




                </div>


                <div class="col-md-6 panel-body" style="padding-top: 65px;">



                    <div class="gp">

                        <p class="donate-title2">HEROES LEADERSHIP TRAINING<br/></p>
                        <p>
                            A 5-day training encompassing the attitudes, knowledge, and skills that are essential in organizing and leading Earth Citizen activities in local communities. Program focuses on understanding the background and direction of Earth Citizen Movement, hands on learning about eco-friendly living and leadership skill development.
                            <br/><br/>
                            To make this program accessible to all individuals with the heart and mind to help others and the planet, we offer this program at a very affordable fee of $300, accommodations and meals included.
                                            
                        </p>
                    </div>

                </div>
                </div>
                <div class="col-md-12">
                    <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:16px;color:#777;">
                        <p>
                            ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>




