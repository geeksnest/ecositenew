<?php echo $this->getContent() ?>
<div class="container-fluid" ng-controller="viewProjCtrl">
  <div class="eco-container wrapper-proj" ng-if="data != null">
    <div class="row">
      <div class="col-md-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a><a href="{[{BASE_URL}]}/#serve">Serve</a> <a><i class='fa fa-arrow-right'></i></a> <a href="{[{BASE_URL}]}/projects">ECO Project</a> <a><i class='fa fa-arrow-right'></i></a> <a href ng-bind="data.projTitle"></a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj page-404" ng-if="data == null">
    <div class="row">
    <div class="col-md-12">
      The page you are trying to reach is broken. Click <a href="{[{BASE_URL}]}/projects">here</a> to go back to Projects.
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj" ng-if="data != null">
    <div class="row">
      <div class="col-md-9">
        <h5 ng-bind="data.projTitle"></h5>
        <p ng-bind="data.projShortDesc"></p>
        <p class="capitalize">by <a href=""><span ng-bind="data.firstname"></span> <span ng-bind="data.lastname"></span></a></p>
      </div>
      <div class="col-md-3">
      </div>
    </div>

    <div class="row">
      <div class="col-md-9">
        <img class="view-proj-thumb" src="{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}">      
        <p></p>
        <p class="text-right"><img src="/images/pin.png" width="18"> <span ng-bind="data.projLoc"></span></p>       
        <h4>About this Project</h4> 
        <p ng-bind-html="projDesc"></p>
      </div>
      <div class="col-md-3">
        <ul class="num">
          <li>
            <span class="big-num" ng-bind="projdonors"></span>
            <br>
            donors
            <br><br>
          </li>
          <li>
            <span class="big-num">$ <span ng-bind="projdon | putcomma"></span></span><br>
            donated of $<span ng-bind="data.projGoal | putcomma"></span> goal
            <br><br>
          </li>
          <li ng-show="data.projStatus == '2'">
            <!-- angular-timer -->
            <timer end-time="daystogo">
              <span class="big-num">
                <span ng-show="days != 0">{[{days}]}</span>
                <span ng-show="days == 0">{[{hours}]}:{[{minutes}]}:{[{seconds}]}</span>
              </span> <br> 
              <span ng-show="days != 0">days to go</span>
              <span ng-show="days == 0">hours to go</span>            
            </timer>
            <!-- end angular-timer -->
            <br><br>
          </li>    
        </ul> 
        <div class="proj-donate w1"  ng-show="data.projStatus == '2'">          
          <div class="form-group center">               
            <div class="input-group m-b">
              <span class="input-group-addon h big-num">$</span>
              <input type="text" class="form-control h big-num" ng-model="donateAmount" only-digits>
              <span class="input-group-addon h big-num">.00</span>
            </div>
            <button class="btn m-b-xs btn-sm btn-success btn-addon w2" ng-disabled="donateAmount == null || donateAmount == ''" ng-click="donate(donateAmount, data.projSlugs)"><i class="fa fa-leaf"></i>Donate to this Project</button>
          </div>
        </div>
        <div class="proj-donate w1"  ng-show="data.projStatus == '5'">          
          <div class="form-group center">               
            <div class="input-group m-b">
              <br><br>
              <span class="h big-num">Completed</span>
            </div>
          </div>
        </div>
        <div class="for-more" ng-show="data.projWeb || data.facebook || data.youtube || data.twitter || data.google || data.pinterest || data.linkedin || data.tumblr || data.instagram">
          <br><br>
          <b> For more information about this project:</b><br><br>
          <span ng-show="data.projWeb != null">
            Website:<br>
            <a href="{[{data.projWeb}]}" target="_blank">
              <span class="social-link text-word-wrap" ng-bind="data.projWeb"></span>
            </a><br><br>
          </span>
          <span ng-show="data.facebook || data.youtube || data.twitter || data.google || data.pinterest || data.linkedin || data.tumblr || data.instagram">
            Social Media:<br>
            <ul class="soc">
              <li ng-show="data.facebook"><a class="soc-facebook" ng-href="{[{data.facebook}]}" target="_blank" ></a></li>
              <li ng-show="data.youtube"><a class="soc-youtube" ng-href="{[{data.youtube}]}" target="_blank"></a></li>
              <li ng-show="data.twitter"><a class="soc-twitter" ng-href="{[{data.twitter}]}" target="_blank"></a></li>
              <li ng-show="data.google"><a class="soc-google" ng-href="{[{data.google}]}" target="_blank"></a></li>
              <li ng-show="data.pinterest"><a class="soc-pinterest" ng-href="{[{data.pinterest}]}" target="_blank"></a></li>
              <li ng-show="data.linkedin"><a class="soc-linkedin" ng-href="{[{data.linkedin}]}" target="_blank"></a></li>
              <li ng-show="data.tumblr"><a class="soc-tumblr" ng-href="{[{data.tumblr}]}" target="_blank"></a></li>
              <li ng-show="data.instagram"><a class="soc-instagram soc-icon-last" ng-href="{[{data.instagram}]}"></a></li>
            </ul>
          </span>
        </div> 
        
      </div>
    </div>

    <!-- Social Media -->
    <div class="row">
      <div class="col-md-9">
        <div class="line line-dashed b-b line-lg"></div>
        <div class="blog-social proj-social-wrap">
          <!-- Facebook -->
          <div class="blog-social-item blog-fb-like float-left">
            <div class="fb-like" data-href="{[{BASE_URL}]}/projects/{[{data.projSlugs}]}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
            <div id="fb-root"></div>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=217644384961866";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
          </div>
          <!-- End Facebook -->

          <!-- Tweeter -->
          <div class="blog-social-item float-left margin-left">
            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
            </script>
          </div>
          <!-- End Tweeter -->

          <!-- Disqus -->
          <div style="clear:both"></div>
          <div id="disqus_thread"></div>
          <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'earthcitizensorganization';

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
          <!-- End Disqus -->
        </div>
      </div>
    </div>
    
    

  </div>




</div>