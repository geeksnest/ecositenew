<?php echo $this->getContent() ?>
<style type="text/css">
  iframe {
    width: 100%;
    height: 100%;
  }
</style>


<!-- ===== NEWS ===== -->




<div class="container-fluid" ng-controller="NewsCtrl"> 


  <div class="eco-container" id="newslist">   
    <div class="row"> 
      <div class="col-sm-12">
        <h1>News and Articles</h1>        
      </div>
      <div class="col-xs-9 greyborder" id="latestlist">      

        <div class="row list-title-blog" ng-repeat="mem in data.data" >
        
         <div class="col-md-3 news-thumb-container">
            <a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}" class="news-thumb-link">

              <div ng-show="mem.videothumb" class="featured-blog-img" style="background-image: url({[{ mem.videothumb | returnYoutubeThumb }]});">
                <div class="youtube-play" style="background-image: url(/images/template_images/yt.png);"></div>
              </div>

              <div ng-show="mem.imagethumb">
                <div class="featured-blog-img" style="background-image: url(<?php echo $this->config->application->amazonlink; ?>/uploads/newsimages/{[{mem.imagethumb}]});"></div>
              </div>

            </a>
          </div>         

          <div class="col-md-7 ">
            <div class="row">
              <div class="col-sm-12"> 

                <div class="row">
                  <div class="col-sm-12">
                    <a href="<?php echo $this->config->application->baseURL; ?>/news/{[{mem.newsslugs}]}"><span class="size23 font1 news-title"><span ng-bind="mem.title"></span></span></a>
                    
                  </div>

                  <div class="col-sm-12">
                      <strong>  <!-- <span ng-cloak>{[{category.categoryname}]}{[{$last ? '' : ', '}]}</span> -->                    
                        <span class="orange thin-font1" ng-repeat="category in mem.categories">
                        <a href="<?php echo $this->config->application->baseURL; ?>/news/category/{[{ category.categoryslugs}]}">
                        
                        <span ng-bind="category.categoryname"></span><span ng-bind="$last ? '':', '"></span>
                                                
                        </a>
                        
                        </span>                      
                      </strong><br>
                    <span ng-show="news.name !=''" class="thin-font1">
                      by 
                      <strong>
                        <span class="orange"><a href="<?php echo $this->config->application->baseURL; ?>/news/author/{[{ mem.authorid }]}"><span ng-bind="mem.name"></span></a></span>
                      </strong>
                      <span class="boxdate2"><span ng-bind="mem.date"></span></span>
                    </span> 
                    <br/><br/>
                  </div>
                  <div class="col-sm-12">
                    <div class="font1 size12 word-wrap">
                      <span ng-bind="mem.summary"></span>
                      <br/><br/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div style="clear:both"></div>
            <br>
          </div>

          <div class="col-xs-2 boxdate">
            <div class="datebox">
              <div >
                <div style="font-size:35px;"><span ng-bind="mem.date | day"></span></div>
                <span ng-bind="mem.date | getmonth"></span>   
               <span ng-bind="mem.date | year"></span>               
              </div>
            </div>
          </div>
      </div>

         <button ng-hide="hideloadmore" class="news-list-show-more" ng-click="showmorenews()" ng-disabled="loading"><span ng-show="loading"><img src="/images/template_images/newsloading.gif"></span><span ng-hide="loading">Show More</span></button>
         <button ng-show="nomorenews" class="news-list-show-more" ng-disabled="nomorenews"><span>end of the list</span></button>
        <hr>   
        
      </div>
      <div class="col-xs-3">
        <hr class="margin-top0">
        <!-- <span class="title2">Featured Categories </span>
        <ul class="pad2">
          <li class="pad">
            <a href="<?php echo '/news/category/upcoming-programs' ?>">Upcoming Programs</a><br/>
          </li>
          <li class="pad">
            <a href="<?php echo '/news/category/recent-activites' ?>">Recent Activites</a><br/>
          </li>
          <li class="pad">
            <a href="<?php echo '/news/category/webinar-archive' ?>">Webinar Archive</a><br/>
          </li>
          <li class="pad">
            <a href="<?php echo '/news/category/mindful-living-tips' ?>">Mindful Living Tips</a><br/>
          </li>
        </ul>
        <hr> -->
        <span class="title2">News Categories </span>
        <ul class="pad2">
          <li class="pad">
            <a href="<?php echo $this->config->application->baseURL; ?>/news">All</a><br/>
          </li>
         <?php
          foreach ($liftofcategory as $category ) {
            ?>
          <li class="pad">
            <a href="<?php echo '/news/category/'.$category->categoryslugs; ?>"><?php echo $category->categoryname; ?></a><br/>
          </li>
          <?php
            }

           ?>
        </ul>
        <hr>
        <span class="title2">Tags </span>
        <div class="listCont">
          <div class="tagsL">
            <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { ?>
            <a href="/news/tags/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
            <?php
            $count > 5 ? $count =1 : $count++;
            } ?>
          </div>
        </div>
        <hr>
        <span class="title2">Archives</span>
        <ul class="pad2">
         <?php
            $getarchives = $archivelist;
            foreach ($getarchives as $key => $value) { ?>
            <li class="pad"><a href="/news/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a></li>
            <?php } ?>
        </ul>
        <hr>
        <a href="<?php echo $this->config->application->baseURL; ?>/news/rss" target="_blank">
        <img src="/images/template_images/rss_feed.gif">
        RSS Feed
        </a>

      </div>
    </div>
  </div>

</div> 
<!-- ===== NEWS END ===== -->

