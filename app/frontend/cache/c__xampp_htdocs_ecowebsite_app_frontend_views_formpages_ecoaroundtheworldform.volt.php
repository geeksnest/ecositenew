        <div class="row" ng-controller="AtwCtrl">
          <div class="col-sm-12">
            <form name="form" id="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="submitatw(user,files)">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="form-group">
                    <label>Name of Organization</label><input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.org" name="organization" required="true"  >
                  </div>
                  <div class="form-group">
                    <label>Year of Establishment</label>
                    <select ng-model="user.syear" class="form-control ng-pristine ng-invalid ng-invalid-required  " class="required" name="selyear" id="selyear" required="true" style="width:200px;">
                      <?php for($year= 1960 ; $year < date('Y'); $year++){ 
                        echo "<option value='".$year."'>".$year."</option>";
                      }?>
                    </select>
                    <label ng-show="ddate"  class="control-label" style="color:#a94442;">This field is required.</label>
                  </div>

                  <div class="form-group">
                    <label>Organization Mission Statement</label>
                    <textarea ng-model="user.statement"  class="form-control" rows="6" placeholder="Type your message" required="true" ></textarea>
                  </div>

                  <div class="form-group">
                    <label>Organization Profile Photo</label>
                    <div style="clear:both;"></div>
                    <div class="row col-sm-6"> 
                      <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                        <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
                      </div>
                      <div class="col-sm-12 create-proj-thumb">
                        <div class="line line-dashed b-b line-lg"></div>                    
                        <img src="{[{base_url}]}/images/default_images/default_image.jpg" ng-if="imageselected == false">
                        <img ngf-src="projImg[0]" ng-if="imageselected == true">
                      </div>
                      <div class="col-sm-12 propic create-proj-thumb">
                        <div class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">
                          <a href="">Choose an image from your computer</a><br>
                          <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                          <label>At least 1024x768 pixels</label>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div style="clear:both;"></div>


                  <div class="form-group">
                    <label>Website</label><input placeholder="http://" type="url" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.website" name="website" required="true"  >
                  </div>
                  <div class="form-group">
                    <label>Facebook Page</label><input placeholder="http://" type="url" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.facebook" name="facebook" required="true"  >
                  </div>
                  <div class="form-group">
                    <label>Contact Email</label><input placeholder="" type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" name="email" required="true"  >
                  </div>
                </div>
                <input placeholder="http://" type="hidden"  ng-model="user.image" name="website" ng-value="user.image='dummy'" >
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                <footer class="panel-footer text-right bg-light lter">
                   <div class="col-md-12 center" ng-show="loadingg == true">
                  <span ng-bind="loadingMsg"></span>
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                  <button type="submit" class="btn btn-success">Submit</button>
                </footer>
              </div>
            </form>
          </div>
        </div>