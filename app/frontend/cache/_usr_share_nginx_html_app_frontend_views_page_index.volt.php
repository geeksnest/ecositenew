<?php echo $body; ?>

<!-- ===== START of WHAT IS ECO ===== -->

    <?php if($pageslugs == 'more-about-eco' || $pageslugs == 'partners-and-sponsors' || $pageslugs == 'how-to-start-change' || $pageslugs == 'join'){ ?>
    
    <div class="container-fluid">
        <div class="eco-container">
            <div class="row padding-bot30 padding-top30" id="circle-links">
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/more-about-eco">
                        <img src="/images/template_images/more-about-eco-circle.jpg" class="sublink-img">
                        <p class="title1">More About ECO</p>
                    </a>
                </div>
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/partners-and-sponsors">
                        <img src="/images/template_images/partners-and-supporters-circle.jpg" class="sublink-img">
                        <p class="title1">Partners & Supporters</p>
                    </a>
                </div>
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/how-to-start-change">
                        <img src="/images/template_images/how-to-start-to-change-circle.jpg" class="sublink-img">
                        <p class="title1">How to Start to Change</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <?php }?>


<!-- ===== END of WHAT IS ECO ===== -->

<!-- ===== START of JOIN ===== -->



<!-- ===== END of JOIN ===== -->

<!-- ===== START of LEARN ===== -->

    <?php if($pageslugs == 'curriculum' || $pageslugs == 'programs' || $pageslugs == 'support-for-graduates'){ ?>

        
        <div style="height: 30px;"></div>
        
        <div class="eco-container"> 
            <div class="row padding-bot30 padding-top30" id="circle-links">
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/curriculum">
                        <img src="/images/template_images/curriculum-circle.jpg" class="sublink-img">
                        <p class="title1">Curriculum</p>
                    </a>
                </div>
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/programs">
                        <img src="/images/template_images/programs-circle.jpg" class="sublink-img">
                        <p class="title1">Programs</p>
                    </a>
                </div>
                <div class="col-xs-4 cen">
                    <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/support-for-graduates">
                        <img src="/images/template_images/support-for-graduates-circle.jpg" class="sublink-img">
                        <p class="title1">Support for Graduates</p>
                    </a>
                </div>
            </div>
        </div>

    <?php }?>

<!-- ===== END of LEARN ===== -->


<!-- ===== START of SERVE ===== -->
    <?php if($pageslugs == 'heroes-news'){ ?>


    <?php }?>

<!-- ===== END of SERVE ===== -->


<!-- ===== START of STORE ===== -->

<!-- ===== END of STORE ===== -->

<!-- ===== START of DONATE ===== -->

    <?php if($pageslugs == 'project-funds' || $pageslugs == 'ways-of-giving'){ ?>
        <div style="height: 30px;"></div>
        <div class="eco-container"> 
            <div class="row padding-bot30 padding-top30" id="circle-links">
            <div class="col-xs-2 cen">
            </div>
            <div class="col-xs-4 cen">
                <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/project-funds">
                    <img src="/images/template_images/eco-project-funds-thumb.jpg" class="sublink-img">
                    <p class="title1">Project Funds</p>
                </a>
            </div>
            <div class="col-xs-4 cen">
                <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/ways-of-giving">
                    <img src="/images/template_images/eco-ways-of-giving-thumb.jpg" class="sublink-img">
                    <p class="title1">Ways of Givving</p>
                </a>
            </div>
            <div class="col-xs-2 cen">
            </div>
        </div>
        </div>
    <?php }?>



<!-- ===== END of DONATE ===== -->