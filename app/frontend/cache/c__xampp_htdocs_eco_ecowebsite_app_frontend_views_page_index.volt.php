<div class="eco-container wrapper-proj">
  <div class="row">
    <div class="col-sm-12">
      <a href="#">ECO</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php if($pageslugs == 'heroes-on-duty' || $pageslugs == 'call-hero' ){ ?>
      <a href="/call-heroes">CALL HEROES</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }else if($pageslugs =='find-hero' || $pageslugs == 'call-heroes' || $pageslugs == 'projects' ){?>
      <a href="/#/#serve">SERVE</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }
      else if($pageslugs =='earth-citizen-training' || $pageslugs == 'programs' || $pageslugs == 'events' || $pageslugs == 'eco-clubs') { ?>
      <a href="/#/#join">JOIN</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }
      else if($pageslugs =='curriculum' || $pageslugs =='support-for-graduates' || $pageslugs =='medialibrary') { ?>
      <a href="/#/#learn">LEARN</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }else if($pageslugs =='simple-ways-to-give' || $pageslugs =='more-ways-of-giving' || $pageslugs =='project-funds'){ ?>
      <a href="/donation">DONATE</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php } else{ ?>
      <a href="/#/whatiseco">WHAT IS ECO</a> <a><i class="fa fa-arrow-right"></i></a> 
      <?php }?>
      <a href=""><?php echo $title ?></a> 
    </div>
  </div>
</div>
<div id="atw"> </div>

<?php echo $body; ?>

<?php 
 /*
 * Kyben Bogie \m/(-_^)\m/ oohhaa
 *
 * @getRender param(foldername, page)
 * @Pages Location /veiws/formpages/*page*
 */

 // formPage Value (page-slug => form-page)
 $formPage = array( 
  "eco-around-the-world"  => "ecoAroundTheWorld",    //Page Eco Around The World
  "eco-around-the-world-registration"=> "ecoAroundTheWorldform",    //Page Eco Around The World FROM
  "friends-and-allies-registration"  => "faregistration",    //Page Eco Around The World
  "heroes-on-duty"        => "heroesOnCall",         //Page Heroes On Call
  "call-hero"             => "callAhero",            //Page Call A Hero
  "more-about-eco"        => "mph",                  //=>Comabined Name Page (mph)
  "partners-and-sponsors" => "mph",                  //Page More About ECO, Partners & Supporters and How to Start to Change 
  "how-to-start-change"   => "mph",                  //
  "join"                  => "mph",                  //
  "curriculum"            => "cps",                  //=>Comabined Name Page (cps)
  "programs"              => "cps",                  //Page Curriculum, Programs and  Support for Graduates
  "support-for-graduates" => "cps",                  //
  "project-funds"         => "pw",                   //=>Comabined Name Page (pw)
  "ways-of-giving"        => "pw",                   //Page Project Funds and  Ways of Givving 
  "create-your-club"      => "club"                  //CLUB CREATE FORM  
  );

 // Return render page
 echo $this->view->getRender('formpages', $formPage[$pageslugs]); 
 ?>








