<?php echo $this->getContent() ?>
<div class="container-fluid">
  <div class="eco-container signup-container">
    <div class="wrapper-form-login" ng-controller="LoginCtrl">

      <div class="m-b-lg">
        <div class="wrapper text-center">
          <strong>Login to make amazing things.</strong>
        </div>
        <form name="formLogin" class="form-validation ng-pristine ng-invalid ng-invalid-required">
          <div class="text-danger wrapper text-center" ng-bind="loginerror">

          </div>
          <div class="form-group">
            <div class="list-group-item">
              <input type="text" placeholder="Username or Email" class="form-control no-border center" ng-model="user.email">
            </div>
            <div class="list-group-item">
              <input type="password" placeholder="Password" class="form-control no-border center" ng-model="user.password">
            </div>
          </div>
          <div class="checkbox m-b-md m-t-none">
            
          </div>
          <button type="button" style="background:#72BB4C;color:#fff" class="btn button-w btn-block" ng-click="login(user)">Log in</button>

          <div class="line line-dashed"></div>          
          
          <a style="background:#5386c1;color:#fff" class="btn button-w btn-block" href="{[{BASE_URL}]}/earth-citizenship"> Sign up & Join Us</a>
        </form>
      </div>
      
    </div>
  </div>
</div>

<!-- log in token ang ginagawa ko pero bakit di naman na encrypt -->