<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Earth Citizens Organization</title>


    <link rel="shortcut icon" type="image/ico" href="/images/template_images/favicon.png" />

    <script language="javascript">
        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function required_field(){
            var frm = document.formBNB;
            if(frm.paymode.Check=='Credit' || frm.paymode.Check=='Cash'){
                if(frm.amt==""){
                    alert('Please fill required fields.');
                }
            }else if(frm.paymode.Check=='Check'){
                if(frm.amt=="" || frm.rn=="" || frm.an=="" || frm.cn==""){
                    alert('Please fill required fields.');
                }
            }
        }
        function spacer(item){
            var tmp = "";
            var item_length = item.value.length;
            var item_length_minus_1 = item.value.length - 1;

            for (index = 0; index < item_length; index++){
                if (item.value.charAt(index) != ' '){
                    tmp += item.value.charAt(index);
                }else{
                    if (tmp.length > 0){
                        if (item.value.charAt(index+1) != ' ' && index != item_length_minus_1){
                            tmp += item.value.charAt(index);
                        }
                    }
                }
            }
            item.value = tmp;
        }

        // $('#chtli').on("click",function(){
        //   $("#donate2").attr('checked', true).trigger('click'); 
        // });
    </script>


    <link rel="stylesheet" href="/css/template_css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/template_css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/template_css/main.css">

    <link rel="stylesheet" href="/css/template_css/slidercss.css">
    <link rel="stylesheet" type="text/css" media="screen, print, projection"  href="/css/template_css/dropdown_css.css">

    <link rel="stylesheet" type="text/css" href="/css/template_css/initcarousel-1.css">

    <!--  <link rel="stylesheet" href="/css/template_css/index.css"> -->
    <link rel="stylesheet" href="/css/template_css/responsivemobilemenu.css">

    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>

    <!-- FROM MAIN -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/fr/color.css"/>
    <link rel="stylesheet" type="text/css" href="/css/fr/reset.css"/>
    <link rel="stylesheet" href="css/fr/horizontal.css"/>
    <!-- END FROM MAIN -->
    <link rel="stylesheet" href="/css/template_css/temp_index.css">

</head>

<body ng-controller="indexMainCtrl">
<?php echo $this->getContent() ?>
<!-- EFNRE START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->
<div class="container-fluid">
    <div class="eco-container top">
        <div class="row">
            <div class="col-xs-4 toptop1">
                <div class="top-contact" style="float: left;">
                    <span><a href="mailto:info@earthcitizens.org" style="color: #5386C1 "><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;info@earthcitizens.org</a></span>
                </div>
            </div>
            <div class="col-xs-4 toptop2">
                <div class="top-contact" style="text-align:center;">
                    <span style="color: #5386C1;  "><i class="fa fa-user"> </i>&nbsp;&nbsp;Earth Citizens Count: {[{ userscount }]}</span>
                    <span style="color: #5386C1;  " class="join"><a ng-href="donation/##new-account">Join</a></span>
                </div>

            </div>
            <div class="col-xs-4 toptop3" style="padding-top:5px">
                <div class="top-social-icon" style="text-align: right;">
                    <span>&nbsp;&nbsp;&nbsp;<a href="https://www.youtube.com/user/goearthcitizens"><i class="fa fa-youtube"></i></a></span>
                    <span>&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/EarthCitizensOrganization" ><i class="fa fa-facebook"></i></a></span>
                    <span>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/goearthcitizens" ><i class="fa fa-twitter"></i></a></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MENU START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->
  <div class="container-fluid">
    <div class="navsticky">
      <div class="eco-container">
        <div id="ecologo"><a href="<?php echo $this->config->application->baseURL; ?>"><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;"></a></div>
        <div class="ecositenav">
          <nav  class="nav">
            <ul class="dropdown">
              <li class="drop">
                <a href="<?php echo $this->config->application->baseURL; ?>/#whatiseco" id="selected4">
                  <div id="menu_icon_about" class="menudiv1">                                    
                    <div class="menuicon1"></div>
                    <div class="p">WHAT IS ECO?</div>
                  </div>
                </a>
                <ul class="sub_menu" id="sub_menu2">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/page/index/more-about-eco">MORE ABOUT ECO</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/partners-and-sponsors">PARTNERS AND SPONSORS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/how-to-start-change">HOW TO START CHANGE</a></li>                          
                </ul>
              </li>
              <li class="drop">
                <a href="<?php echo $this->config->application->baseURL; ?>/join" id="selected2">
                  <div id="menu_icon_training" class="menudiv2">                                    
                    <div class="menuicon2"></div> 
                    <div class="p">JOIN</div>
                  </div>
                </a>
              </li>
              <li class="drop">
                <a href="<?php echo $this->config->application->baseURL; ?>/#learn" id="selected3">
                  <div id="menu_icon_peace_parks" class="menudiv3">                                    
                    <div class="menuicon3"></div> 
                    <div class="p">LEARN</div>
                  </div>
                </a>
                <ul class="sub_menu" id="sub_menu3">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/page/index/curriculum">CURRICULUM</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/programs">PROGRAMS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/support-for-graduates">SUPPORT FOR GRADUATES</a></li>
                </ul>
              </li>
              <li class="drop">
                <a href="<?php echo $this->config->application->baseURL; ?>/#serve" id="selected1">
                  <div id="menu_icon_what_is_eco" class="menudiv4">                                    
                    <div class="menuicon4"></div> 
                    <div class="p">SERVE</div>
                  </div>
                </a>
                <ul class="sub_menu" id="sub_menu4">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/news">HEROES NEWS</a></li>
                </ul>
              </li>
              <li class="drop">
                <a href="http://www.cafepress.com/earthcitizensorganization" target="_blank" id="selected6">
                  <div id="menu_icon_support_us" class="menudiv6">                                    
                    <div class="menuicon6"></div> 
                    <div class="p">SHOP</div>
                  </div>
                </a>
              </li>
              <li class="drop">
                <a href="<?php echo $this->config->application->baseURL; ?>/donation" id="selected5">
                  <div id="menu_icon_support_us" class="menudiv5">                                    
                    <div class="menuicon5"></div> 
                    <div class="p">DONATE</div>
                  </div>
                </a>   
                <ul class="sub_menu" id="sub_menu5">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/page/index/funds-to-support-ecos-projects">FUNDS TO SUPPORT ECO'S PROJECTS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/ways-of-giving">WAYS OF GIVING</a></li>
                </ul>
              </li>
            </ul>
            <div class="clearfix"></div>
          </nav>
        </div>
        <div class="clearfix"></div>
      </div>  
      <div class="container-fluid"><div class="bluebar"></div></div>    
    </div>    
  </div>

  <div class="container-fluid">
    <div class="mob-ecologo"  style="width:115px;margin:auto;padding-top:5px;"><a href="<?php echo $this->config->application->baseURL; ?>"><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;"></a></div>
    <div class="navsticky">
      <div class="mobileMenu mobileMenu2">
        <nav role="navigation" class="navbar navbar-default navbar-static-top">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="<?php echo $this->config->application->baseURL; ?>" class="navbar-brand"><p>ECO</p></a>
          </div>
          <!-- Collection of nav links and other content for toggling -->
          <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li>
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo $this->config->application->baseURL; ?>/##what-is-eco">WHAT IS ECO<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/more-about-eco">MORE ABOUT ECO</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/partners-and-sponsors">PARTNERS AND SPONSORS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/how-to-start-change">HOW TO START CHANGE</a></li>
                </ul>
              </li>
              <li>
                <a href="<?php echo $this->config->application->baseURL; ?>/join">JOIN</span></a>

              </li>
              <li>
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo $this->config->application->baseURL; ?>/##learn">LEARN<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/page/index/curriculum">CURRICULUM</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/programs">PROGRAMS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/support-for-graduates">SUPPORT FOR GRADUATES</a></li>
                </ul>

              </li>
              <li>
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo $this->config->application->baseURL; ?>/##serve">SERVE<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/news">HEROES NEWS</a></li>
                </ul>
              </li>
              <li>
                <a href="http://www.cafepress.com/earthcitizensorganization" target="_blank">SHOP</span></a>
              </li>
              <li>
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo $this->config->application->baseURL; ?>/donation">DONATE<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a style="padding-top: 15px;" href="<?php echo $this->config->application->baseURL; ?>/page/index/funds-to-support-ecos-projects">FUNDS TO SUPPORT ECO'S PROJECTS</a></li>
                  <li><a href="<?php echo $this->config->application->baseURL; ?>/page/index/ways-of-giving">WAYS OF GIVING</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>

<div class="donation-fluid interior-donation-seattle">
    <div id="interior" class="row eco-container">
        <div class="overlaywhite">
            <div style="color:#404040">
                <font class="mtitle">EARTH, BODY AND BRAIN EXPO 2015</font> <br>
                <p class="mbody" style="padding-bottom:0px;">We will embrace our commonalities and celebrate our hope for a bright future through community sharing of environmentally friendly products, entertainment an interactive programs.</p>
            </div>
        </div>
    </div>
</div>


<!--===begin container===-->
<div  ng-controller='DonateSeattleCtrl' class="container-fluid"  style="margin-top:30px;">
    <div class="eco-container" >
        <div class="wrapper-md " >
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group" style="padding-left:0px;padding-top:0px;">
                        <span class="donate-title2">REGISTER NOW</span><br/><br/>
                        <p style="font-size:14px;margin-bottom:0px;" ng-show="reccur_success==2"> Your transaction is now being processed. </p>
                        <p style="font-size:16px;margin-bottom:0px;" ng-show="reccur_success==3"> {[{ reccur_message }]} <br/><br/>
                            <a href="" ng-click="back_click()" ng-show="back_button==2 || back_button==3" >Back to form </a>
                        </p>
                    </div>
                <div ng-show="reccur_success==1" class="form-group col-lg-12"  style="padding:0px;margin-top:0px;">

                <div style="padding-left:0px">

                    <div class="form-group col-sm-4" style="padding-left:0px">
                        
                        <span style="font-size:16px;">
                        Donation Amount:<br/></span>
                        <span style="font-size:12px;">
                        (Minimum $10.00)</span>
                        
                    </div>
                    <div class="form-group col-sm-8">
                        <input type="hidden" name="donateamount" ng-init="donateamount=10.00" />
                        <input type="radio" name="donateamount" value="10.00" ng-model="donateamount" ng-click="fixamount2(10.00 ,false)"><span style="font-size:16px;">&nbsp;$&nbsp;10.00</span><br/>
                        <input type="radio" name="donateamount" value="50.00" ng-model="donateamount" ng-click="fixamount2(50.00, false)"><span style="font-size:16px;">&nbsp;$&nbsp;50.00</span><br/>
                        <input type="radio" name="donateamount" value="500.00" ng-model="donateamount" ng-click="fixamount2(500.00, false)"><span style="font-size:16px;">&nbsp;$&nbsp;500.00</span><br/>
                        <input type="radio" name="donateamount" value="1000.00" ng-model="donateamount" ng-click="fixamount2(1000.00, false)"><span style="font-size:16px;">&nbsp;$&nbsp;1000.00</span><br/>

                        <input type="radio" name="donateamount" ng-click="otheramount()"><span style="font-size:16px;">&nbsp;Other</span><br/>
                        <!-- <span style="font-size:16px;">&nbsp;$&nbsp;</span> -->
                        <input type="text" class="form-control" name="donateamount" ng-model="otheramountdon" ng-show="others == true" ng-change="fixamount2(otheramountdon, true)" only-digits/><br/>

                        <div class="form-group col-sm-12" ng-show="amountlimit == true">                        
                          <p  style="color:#fd5555;font-size:15px;">Please a minimum of $10 for your donation.</p>
                        </div> 
                    </div>
                    
                </div> 

<!--  DONATE PART FOR OTHERS -->

                    <div class="reganddonate" style="margin-top:20px;">   
               
                        <div style="margin-bottom:10px;">
                            
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/>
                                </div> 
                            </div> 
                            <div class="form-group row" > 
                                <div class="col-md-4">
                                    Payment Method:
                                </div>
                                <div class="col-md-8">
                                    <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                                        <option value=''>Credit Card</option>
                                        <option value='ach2'>Checking/Savings Account</option>
                                        <option value='paypal2'>PayPal</option>
                                    </select>
                                </div>
                            </div>                      
                                                 
                        </div>



<!-- CREDIT CARD  BILLING FOR DONATION PART -->
                    <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
                        <br/>
                        <form name="formCredit" id="formCredit" method="post" action="">  

                   

                            <div class="form-group row" >
                                <div class="col-md-4">
                                    <!-- emailaddress -->
                                    <!-- <input type="hidden" class="form-control rounded" ng-model="user.email" name="email" required/> -->
                                </div>
                                <div class="col-md-8">
                                    <img src="/images/template_images/visa.png" style="width:65px;">
                                    <img src="/images/template_images/amex.png" style="width:65px;">
                                    <img src="/images/template_images/mastercard.png" style="width:65px;">
                                    <img src="/images/template_images/discover.png" style="width:65px;">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Credit Card Number: *
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" ng-model="cc.ccn" name="" required="required" only-digits />
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    CVV Number: *
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control " ng-model="cc.cvvn" name="" required="required" only-digits />
                                </div>
                                <div class="col-md-4">                            
                                </div>
                                <div class="col-md-8">

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Credit Card Expiration: *
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <span>Month</span><br/>
                                            <select ng-model="cc.expiremonth" class="form-control" required="required">
                                              <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 7 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                                              <?php foreach ($formonths as $index => $formonth) { 
                                                echo "<option value='".$index."'>".$formonth."</option>";
                                              }?>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <span>Year</span><br/>
                                            <select ng-model="cc.expireyear" class="form-control" required="required">
                                              <?php for ($year=date('Y'); $year < 2050; $year++) { 
                                                echo "<option value='".$year."'>".$year."</option>";
                                              }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-12">
                                    <span style="font-weight:bold;">Billing Information</span>
                                </div>
                            </div>

                            <div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        First Name: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.billingfname" name="billingfname" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <div class="col-md-4">
                                        Last Name: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.billinglname" name="billinglname" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Address Line 1: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.al1" name="al1" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        Address Line 2:
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.al2" name="al2"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        City: *
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control " type="text" ng-model="cc.city" name="city" required="required"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        State:
                                    </div>
                                    <div class="col-md-8">
                                       <input class="form-control " type="text" ng-model="cc.state" name="city"/>
                                   </div>
                               </div>
                               <div class="form-group row">
                                <div class="col-md-4">
                                    ZIP/Postal Code: *
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" ng-model="cc.zip" name="zip" required="required"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Country: *
                                </div>
                                <div class="col-md-8">
                                    <select ng-model="cc.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required="required">
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Email Address: *
                                </div>
                                <div class="col-md-8">
                                    <input type="email" class="form-control rounded" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
                                    <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                                </div>
                            </div>
                                <div class="form-group">
                                    How did you learn about the Earth Citizens Expo in Seattle?
                                    <select class="form-control m-b" ng-model="cc.howdidyoulearn" ng-change="changeme()">
                                        <option value="ECO event">ECO event</option>
                                        <option value="ECO Program Graduates">ECO Program Graduates</option>
                                        <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                        <option value="Invitation Email">Invitation Email</option>
                                    </select>
                                </div>
                                <div class="form-group" ng-if="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                    Center Name
                                    <select class="location form-control m-b" ng-model="cc.cname" required="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                        <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                    </select>
                                </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-md-12" ng-hide="invalidemail == true">
                                <input type="button" name="" id="" value="Submit"  class="btn btn-sm btn-primary" ng-show="reccur_success == 1"  ng-click="submitcredit(cc,user.email, 'seattle')" ng-disabled="formCredit.$invalid || donateamount < 10" style="float:right;"/>
                            </div>
                            <!-- <div class="col-md-12" ng-if="invalidemail == true">
                                <span class="formerror" style="font-size: 12px;color: #fd5555">The email address you entered is not valid. Example: myname@example.com</span>
                            </div>  -->
                        </div>
                    </form>
                    </div>
<!-- END FOR CREDIT CARD-->

<!-- CHECK Authorize For Donate Part-->
                    <div ng-show="typetransact2 == 'ach2' " class="ach" >
                    <br/>

                        <form name="formAuthorizeCheck" id="formAuthorize" method="post" action="">

                            <div class="form-group row" >
                                <div class="col-md-4"> 
                                    Account Holder Name: *
                                </div>
                                <div class="col-md-8">
                                    <!-- emailaddress -->
                                    <!-- <input type="hidden" class="form-control rounded" ng-model="user.email" name="email" required/> -->

                                    <input class="form-control " type="text" ng-model="check.accountname" name="accountname" required/>

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4"> 
                                    Your bank name: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankname" name="bankname" required/>

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-12"> 
                                    Enter the routing code and account number as they appear on the bottom of your check:
                                </div>
                                <div class="col-md-12">
                                    <img src="/images/template_images/checksample.png">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4"> 
                                    Bank Routing Number: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankrouting" name="bankrouting" required only-digits/>

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"> 
                                    Bank Account Number: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.bankaccountnumber" name="bankaccountnumber" required only-digits />

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"> 
                                    Select Account Type:
                                </div>
                                <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                                    <input type="radio" name="at" ng-model="check.at" value="CHECKING" required="required">&nbsp;&nbsp;Checking account<br/>
                                    <input type="radio" name="at" ng-model="check.at" value="BUSINESSCHECKING" required="required">&nbsp;&nbsp;Business checking account<br/>
                                    <input type="radio" name="at" ng-model="check.at" value="SAVINGS" required="required">&nbsp;&nbsp;Savings account
                                </div>
                            </div>

                            <div class="form-group row" >
                                <div class="col-md-12"> 

                                    <input type="checkbox"  ng-model="check.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'" required>*

                                    <span style="text-align: center;">
                                        By entering my account number about and Clicking authorize, I authorize my payment to be processed 
                                        as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service 
                                        provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account. 
                                        <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these 
                                        authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                                    </span>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <div class="col-md-12">
                                    <span style="font-weight:bold;">Billing Information</span>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    First Name: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.billingfname" name="billingfname" required />

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Last Name: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.billinglname" name="billinglname" required />

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Address Line 1: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.al1" name="al1" required />

                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Address Line 2:
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" ng-model="check.al2" name="al2" />
                                </div>
                            </div>
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    City: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.city" name="city" required />

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    State:
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" ng-model="check.state" name="city" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Province:
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control " type="text" ng-model="check.province" name="province" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    ZIP/Postal Code: *
                                </div>
                                <div class="col-md-8">

                                    <input class="form-control " type="text" ng-model="check.zip" name="zip" required />

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    Country: *
                                </div>
                                <div class="col-md-8">

                                    <select ng-model="check.country" ng-init="check.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'" required="">

                                    </select>
                                </div>
                            </div>                           
                            <div class="form-group row" >
                                <div class="col-md-4">
                                    Email Address: *
                                </div>
                                <div class="col-md-8">
                                    <input type="email" class="form-control rounded" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
                                    <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                                </div>
                            </div>

                            <div class="form-group">
                                How did you learn about the Earth Citizens Expo in Seattle?
                                <select class="form-control m-b" ng-model="check.howdidyoulearn" ng-change="changeme()">
                                    <option value="ECO event">ECO event</option>
                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                    <option value="Invitation Email">Invitation Email</option>
                                </select>
                            </div>
                            <div class="form-group" ng-if="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                Center Name
                                <select class="location form-control m-b" ng-model="check.cname" required="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                </select>
                            </div>

                            <div class="form-group row" style="padding-left:15px;">
                                <div class="col-md-12" ng-hide="invalidemail == true">
                                    <input type="button" name="" id="abouteco" value="Submit" ng-click="submitecheck(check,user.email,'seattle')" class="btn btn-sm btn-primary" ng-disabled="formAuthorizeCheck.$invalid || donateamount < 10" style="float:right;"/>
                                </div>
                                <!-- <div class="col-md-12" ng-if="invalidemail == true">
                                    <span class="formerror" style="font-size: 12px;color: #fd5555">The email address you entered is not valid. Example: myname@example.com</span>
                                </div>  -->
                            </div>


                        </form>
                    </div>
<!-- END of CHECK Authorize For Donate Part-->

                                      
                    <a id="causes"></a>
<!-- Paypal Form Payment -->
                   
                   
                    <div  ng-show="typetransact2 == 'paypal2' ">
                    <br/>
                        <form class="paypalform" name="paypalform" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypalsingle">

                            <div style="clear:both; ">
                                <input type="hidden" name="cmd" value="_donations">
                                <input type="hidden" name="business" value="accounting@earthcitizens.org">
                                <input type="hidden" name="lc" value="US">
                                <input type="hidden" name="custom" id="paypalcustom" ng-model="paypalcustom" value="">
                                <input type="hidden" name="item_name" value="SEATTLE EARTH CITIZENS EXPO">
                                <input type="hidden" name="amount" ng-value="donateamount">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="no_note" value="0">
                                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                            </div>
                            <div class="form-group">
                                How did you learn about the Earth Citizens Expo in Seattle?
                                <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
                                    <option value="ECO event">ECO event</option>
                                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                                    <option value="Invitation Email">Invitation Email</option>
                                </select>
                            </div>
                            <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                                Center Name
                                <select class="location form-control m-b" ng-model="user.cname" required="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"> {[{ data.centername }]} </option>
                                </select>
                            </div>


                            <div ng-hide="paypalbutton">
                                
                                <a href="#" ng-click="submitpaypal(user.howdidyoulearn, user.cname)" ng-hide="paypalform.$invalid || donateamount < 10"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="paypalbutton" alt="PayPal - The safer, easier way to pay online!" style="float:right;"></a>
                                
                            </div>
                            
                        </form>
                        <!--<form name="formPaypal" id="formPaypal" method="post" action="">   -->

                            <!--<div style="clear:both; ">-->
                                <!--<div class="form-group row" style="padding-left:15px;">-->
                                    <!--<div class="col-md-12">-->
                                        <!--<input  type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" ng-hide="donateamount < 20" style="float:right;">-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->

                            <!--<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->

                        <!--</form>-->

                    </div>  

<!-- END of Paypal -->

                                      
                    <a id="causes"></a>
                    

                </div>              
                        
                        

                    </div>
                        
                  
                    


<!-- END of Registration with Paypal -->

                    

                    
                </div>
                    
                
                <div class="col-md-6 panel-body" style="padding-top: 65px;">


                    
                    <div class="gp">
                        
                        <p class="donate-title2">EARTH, BODY AND BRAIN EXPO 2015 SEATTLE, WA <br/></p>
                        <p>
                            This event is unique in calling each individual to action. We invite you to tap into your potential to general change in the world thus helping to create a harmonious planet.
                            <br/><br/>
                            <b>WHEN :</b><br/>
                            SUNDAY, OCTOBER 18, 2015<br/>
                            11:00 A.M. - 5:00 P.M.
                            <br/><br/>
                            <b>WHERE :</b><br/>
                            Lynwood Convention Center<br/>
                            3711 196th St SW<br/>
                            Lynnwood, WA 98036
                            <br/><br/>
                            <b>HOW YOU CAN PARTICIPATE</b><br/>
                            The Earth, Body and Brain Expo is a one-of-a-kind event designed for the whole family. Here’s how YOU can get involved.
                            <br/><br/>
                            <b>Attend:</b><br/>
                            Bring your family and friends to experience live demonstrations, educational discussions, fun and interactive activities for people of all ages, and shopping with a broad palette of earth-friendly vendors.<br>
                            <b>Tickets: $10, children ages 12 & under FREE</b>
                            <br/><br/>
                            <b>Exhibit:</b><br/>
                            Expose your planet-loving products and services to more than 600 like-minded from all over the Puget sound area.<br>
                            <b>Table Fee: $100 for profit, $145 with electricity, $50 or non-profit.</b>
                            <br/><br/>
                            <b>Present/Demonstrate:</b><br/>
                            Help brain awareness and education to the community about products and practices that nurture humanity and the planet.
                            <br/><br/>
                            <b>Sponsor:</b><br/>
                            Make a financial or product contribution and receive tax deduction in 2015 for your charitable donation.
                            <br/><br/>
                            For more information, please call 206.240.6515 or email <a href="mailto:seattle@earthcitizens.org">seattlearthcitizens.org</a>.                           
                        </p>
                    </div>

                </div>
                </div>
                <div class="col-md-12">
                    <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:16px;color:#777;">
                        <p>
                            ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="container-fluid">
    <div  class="footer-bg">
        <div class="eco-container">
            <div class="row">
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>MISSION STATEMENT<br/><br/></span>
                    <div style="width:212px; padding-right: 15px;">
                                <span style="font-size: 12px;">
                                    Earth Citizens Organization (ECO) is a non-profit that promotes natural health and mindful living for a sustainable world. An Earth Citizen is a person who understands that we are all citizens of the Earth and takes responsibility for its well-being.<br/><br/>
                                </span>
                    </div>

                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>NAVIGATION<br/><br/></span>
                    <div style="width:190px; padding-right: 55px;">
                                <span style="font-size: 12px;">
                                    <nav  class="nav" style="padding-top: 0px;">
                                        <a href="<?php echo $this->config->application->baseURL; ?>/#whatiseco" style="color:#ffffff;">WHAT IS ECO?</a><br/>
                                        <a href="<?php echo $this->config->application->baseURL; ?>/join" style="color:#ffffff;">JOIN</a><br/>
                                        <a href="<?php echo $this->config->application->baseURL; ?>/#learn" style="color:#ffffff;">LEARN</a><br/>
                                        <a href="<?php echo $this->config->application->baseURL; ?>/#serve" style="color:#ffffff;">SERVE</a><br/>
                                        <a href="http://www.cafepress.com/earthcitizensorganization" target="_blank" style="color:#ffffff;">SHOP</a><br/>
                                        <a href="<?php echo $this->config->application->baseURL; ?>/donation" style="color:#ffffff;">DONATE</a><br/><br/>
                                    </nav>
                                </span>
                    </div>
                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>ADD US<br/><br/></span>
                    <div style="width:190px; padding-right: 55px;">
                                <span style="font-size: 12px;">
                                    <ul>
                                        <li><a href="https://www.youtube.com/user/goearthcitizens" style="color:#ffffff;"><i class="fa fa-youtube" style="color: #ffffff;"></i>&nbsp;&nbsp;YouTube</a><br/><br/></li>
                                        <li><a href="https://www.facebook.com/EarthCitizensOrganization" style="color:#ffffff;"><i class="fa fa-facebook" style="color: #ffffff;"></i>&nbsp;&nbsp;Facebook</a><br/><br/></li>
                                        <li><a href="https://twitter.com/goearthcitizens" style="color:#ffffff;"><i class="fa fa-twitter" style="color: #ffffff;"></i>&nbsp;&nbsp;Twitter</a></li>
                                    </ul><br/><br/>
                                </span>
                    </div>
                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>CONTACT US<br/><br/></span>
                    <div style="width:212px; padding-right: 15px;">
                                <span style="font-size: 12px;">
                                    Earth Citizens Organization<br/>
                                    340 Jordan Road Sedona, AZ 86336
                                    <br/><br/>
                                    <a href="mailto:info@earthcitizens.org" style="color: #fff "><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Email: info@earthcitizens.org</a>
                                    <br/><br/>
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="padding: 10px 0px 10px 0px;">
                <div  style="width: 100%;background-color: #ffffff;height: 3px;"></div>
            </div>
            <div class="row" style="padding-top: 2px;">
                <p style="font-size: 10px; text-align: center;color:#ffffff;">
                    ©2015 Earth Citizens Organization All rights reserved
                </p>
            </div>
        </div>
    </div>
</div>

<script>
    var API_URL = '<?php echo $this->config->application->apiURL; ?>';        
</script>

<script type="text/javascript" src="/js/template_js/jquery-1.9.1.min.js"></script>


<script src="/js/template_js/jquery.min.js"></script>
<script src="/js/template_js/bootstrap.min.js"></script>
<script src="/js/template_js/jquery.js"></script>
<script src="/js/template_js/dropdown_js.js"></script>
<!-- <script src="/js/template_js/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
<!-- <script src="/js/template_js/dropdown_hover_ajax.js"></script> -->
<!-- end dropdown -->
<!-- carousel -->
<!-- <script src="/js/template_js/amazingcarousel.js"></script> -->
<!-- <script src="/js/template_js/initcarousel-1.js"></script> -->
<!-- end carousel -->
<!-- <script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script> -->
<!-- <script src="/js/template_js/vendor/bootstrap.min.js"></script> -->
<!-- <script src="/js/ckeditor/ckeditor.js"></script> -->
<!-- <script src="/js/ckeditor/styles.js"></script> -->

<!-- <script src="/js/template_js/main.js"></script> -->





<!-- JS FROM MAIN -->

<!--==========================================BOOSTRAP JS=============================================-->
<script type="text/javascript" src="/js/fr/jquery.counterup.min.js"></script>
<script type="text/javascript" src="/js/fr/waypoint.min.js"></script>
<script>
    $(document).ready(function () {

        $(".mapp").hide();
        $(".map-btncl").hide();
        $(".map-btn").click(function () {
            $(".mapp").toggle("slow");
        });
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });        
</script>

<script type="text/javascript" src="/js/angular/angular.min.js"></script>
<script type="text/javascript" src="/js/angular/angular-ui-router.min.js"></script>
<script type="text/javascript" src="/js/angular/ui-validate.js"></script>
<script type="text/javascript" src="/js/fr/jquery.simplemodal.1.4.4.min.js"></script>

<script type="text/javascript" src="/js/fr/app.js"></script>

<!-- <script src="/js/stickytop.js"></script> -->

<script src="/fcalendar/ui-bootstrap-tpls-0.9.0.js"></script>
<script src="/fcalendar/moment.js"></script>
<script src="/fcalendar/fullcalendar.js"></script>
<script src="/fcalendar/gcal.js"></script>
<script src="/fcalendar/calendar.js"></script>
<!-- <script src="/fcalendar/calendarDemo.js"></script> -->

<?php echo $script_google; ?>
    
</body>
</html>




