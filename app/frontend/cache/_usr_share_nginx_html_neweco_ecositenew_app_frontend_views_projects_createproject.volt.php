<!-- <?php echo $this->getContent() ?> -->
<a href="" id="errorget"></a>
<div class="container-fluid" ng-controller="createCtrl">

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a><a href="/#serve">SERVE</a> <a><i class='fa fa-arrow-right'></i></a> <a href="/projects">ECO Project</a>  <a><i class='fa fa-arrow-right'></i></a> <a href="">Create Project</a> 
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <h4>Submit Your ECO Project</h4>
      </div>
      <div class="col-sm-12">
        Do you have a great idea for an outreach project that would help a cause that you feel passionate about? Fill out the application form below to have ECO help initiate the support you need in making a difference.
      </div>
    </div>
    <div class="row create-form">  

      <div class="col-sm-8"> 
        <!-- <form>       -->

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group wrapper-sm bg-danger" ng-show="errorget">
                <span class="" ng-bind="errorget"></span>
              </div>
              <div class="form-group">
                <input type="hidden" ng-init="proj.projAuthorID = userAgent" ng-model="proj.projAuthorID"/>
                Project Title <label class="req">*</label>
                <input type="text" class="form-control" ng-model="proj.projTitle" name="" required/>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                Short Description <label class="req">*</label>
                <textarea class="form-control textarea" ng-model="proj.projShortDesc" maxlength="200"></textarea>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                Project Location <label class="req">*</label>
                <select class="location form-control" ng-model="proj.projLoc">
                  <option ng-repeat="cn in countries" value="{[{cn.name}]}, {[{cn.code}]}">{[{cn.name}]}, {[{cn.code}]}</option>
                </select>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="form-group">
                Fundraising Duration<label class="req">*</label>  
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="type" value="days" ng-model="type" ng-checked="type == 'days'"><i></i> Number of Days &nbsp;&nbsp;&nbsp;
                  </label>
                  <label class="i-checks">                
                    <input type="radio" name="type" value="date-time" ng-model="type"><i></i> End on Date & Time
                  </label>
                </div>
                <div ng-show="type == 'days'">
                  <span class="text-muted">1-60 days, we recommend 30 or less</span>
                  <input type="text" class="form-control don5t10n" ng-model="proj.days" id="days" name="days" only-digits ng-change="limitDays(proj.days)">
                </div>
                <div ng-show="type == 'date-time'" class="row">
                  <div class="col-md-6">
                    <span class="text-muted">Select date here.</span>
                    <div class="input-group w-md">               
                      <span class="input-group-btn">
                        <input type="hidden" ng-model="proj.datedue">
                        <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="proj.datedue" is-open="opened" datepicker-options="dateOptions"  min-date="minDate" ng-required="true" close-text="Close" type="text" disabled>
                        <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon icon-calendar"></i></button>  
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <br>
                    <timepicker-pop input-time="time1" class="input-group" disabled="disabled"
                    show-meridian='showMeridian' readonly> </timepicker-pop>                  
                  </div>

                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  Fundraising Goal <label class="req">*</label><br>
                  <span class="f-12">
                  Limited to $5,000. If the budget for your project exceeds $5,000, please contact us at <a href="mailto:support@earthcitizens.org">support@earthcitizens.org</a> 
                  </span>       
                  <div class="input-group m-b">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control don5t10n" ng-model="proj.projGoal" name="" only-digits ng-change="limitAmount(proj.projGoal)">
                  </div>
                </div>

                <div class="form-group">
                  Project Image <label class="req">*</label>

                  <div class="row"> 
                    <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                      <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
                    </div>
                    <div class="col-sm-12 create-proj-thumb">
                      <div class="line line-dashed b-b line-lg"></div>                    
                      <img src="{[{base_url}]}/images/default_images/default_image.jpg" ng-if="imageselected == false">
                      <img ngf-src="projImg[0]" ng-if="imageselected == true">
                    </div>
                    <div class="col-sm-12 propic create-proj-thumb">
                      <div class="label_profile_pic border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">
                        <a href="">Choose an image from your computer</a><br>
                        <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                        <label>At least 1024x768 pixels</label>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12 center" ng-show="loadingg == true">
                  <span ng-bind="loadingMsg"></span>
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                  <footer class="panel-footer text-right bg-light lter">
                    <button type="submit" class="btn btn-info"  ng-click="saveProj(proj, projImg)">Save Project</button>
                  </footer>
                </div>
              </div>
            </div>

          </div>
        <!-- </form> -->
      </div>
    </div>
  </div>
</div>

<!-- -->