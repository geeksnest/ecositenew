<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Models\Featuredprojects as Featuredprojects;
use Modules\Frontend\Models\News as News;
use Modules\Frontend\Models\Pages as Pages;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Donationlogothers as Donationlogothers;
use Modules\Frontend\Models\Calendar as Calendar;
use Modules\Frontend\Models\Settings as Settings;



class PageController extends ControllerBase {

    public function initialize() {

    }

    public function indexAction($pageslugs) {
        $this->angularLoader(array(
            'AtwCtrl' => '/js/fr/scripts/controllers/atw.js',
            'HeroesOnDutyCtrl' => '/js/fr/scripts/controllers/callHeroes/heroesOnDuty.js',       
            'CallheroesFactory' => '/js/fr/scripts/factory/callheroes/_callheroes.js',  
            'Createclubctrl' => '/js/fr/scripts/controllers/club/create.js',                       
            'configJS'  => '/js/config/config.js',             
            )); 
      
        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
           return $this->response->redirect('maintenance/');
        }
        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);

        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;        
        
        // View by pageslugs
        $service_url = $this->config->application->apiURL.'/pages/getpage/' . $pageslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        if ($decoded == null) {
            $this->eventsAction($pageslugs);
            $this->view->pagelayout = 'events';
            $this->view->pick("events/index");
        }else{

            if ($decoded->title=="CALL HERO") {
                $_title = "REQUEST";
            }else{
                $_title = $decoded->title;
            }


            $this->view->title = $_title;
            $this->view->pageid = $decoded->pageid;
            $this->view->pageslugs = $decoded->pageslugs;
            $this->view->pagefeatures = $decoded->pagefeatures;
            $this->view->body = $decoded->body;
            $this->view->banner = $decoded->banner;
            $this->view->banneropt = $decoded->banneropt;
            $this->view->bannertitle = $decoded->bannertitle;
            $this->view->titlefontsize = $decoded->titlefontsize;
            $this->view->bannertext = $decoded->bannertext;
            $this->view->descriptionfontsize = $decoded->descriptionfontsize;
            $this->view->showtext = $decoded->showtext;
            $this->view->tbgcolor = $decoded->tbgcolor;
            $this->view->tfcolor = $decoded->tfcolor;
            $this->view->dfcolor = $decoded->dfcolor;
            $this->view->disonli = 1;
            $this->view->btnname = $decoded->btnname;
            $this->view->btnlink = $decoded->btnlink;

            $this->view->metatitle = $decoded->metatitle;
            $this->view->metadesc = $decoded->metadescription;
            $this->view->metakeyword = $decoded->metakeyword;
        }

        //Pages Circles
        $service_url = $this->config->application->apiURL.'/pages/getsubpage';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded2 = json_decode($curl_response);
        $findPageSlugs = $decoded2;

        $count = 0;    
        foreach ($findPageSlugs as $key => $value) {                    
            $subs = $findPageSlugs[$key]->subpage;
            foreach ($subs as $key => $value) {
                $slug = str_replace('/', '', $subs[$key]->link);
                if($slug == $pageslugs){
                    $this->view->pageSubMenus = $decoded2[$count]->subpage;
                }
            }
            $count ++;
        }

       

    }

    public function eventsAction($eventsURL) {
        
        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
             
           return $this->response->redirect('maintenance/');
        
        }


        $this->view->donation = 3;
        $this->view->banneropt = 'true';


        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);

        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;        
        
        // View by pageslugs
        $service_url = $this->config->application->apiURL.'/events/getEventContents/' . $eventsURL;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        if ($decoded == null) {            
            return $this->response->redirect($this->config->application->baseURL . '/page404');
        }else{
            if($decoded->status == 0){
                return $this->response->redirect($this->config->application->baseURL . '/page404');
            }else{
                $this->view->eventID = $decoded->eventID;
                $this->view->title = $decoded->title;
                $this->view->URL = $decoded->URL;
                $this->view->shortDesc = $decoded->shortDesc;
                $this->view->content = $decoded->content;
                $this->view->banner = $decoded->banner;
                $this->view->paymentInfo = $decoded->paymentInfo;
                $this->view->billingInfo = $decoded->billingInfo;
                $this->view->otheramount = $decoded->otheramount;
                $this->view->hdyla = $decoded->hdyla;
                $this->view->recieptSubject = $decoded->recieptSubject;
                $this->view->recieptContent = $decoded->recieptContent;
                $this->view->status = $decoded->status;
                $this->view->prices = $decoded->prices;
                $this->view->cnames = $decoded->cnames;
                $this->view->cnames2 = $decoded->cnames;
                $this->view->cnames3 = $decoded->cnames;
                $this->view->minAmounts = $decoded->minPrice;

                $this->view->metatitle = $decoded->title;
                $this->view->metadesc = $decoded->shortDesc;
                $this->view->metakeyword = $decoded->URL;

                foreach ($this->view->minAmounts as $minAmounts ) {$this->view->min = $minAmounts->minamounts;}
            }
        }



    }
    public function eventslistAction() {
        $this->view->title = "EVENTS";
        $this->view->banner = "eco-around-the-world.jpg";
        $this->view->showtext = "true";
        $this->view->showlink = false;
        $this->view->bannertitle = "EVENTS";
        $this->view->bannertext = "ECO events will allow you to find how ECO works in local communities and meet with like-minded people who care for people and the planet.";        
        $this->view->URL = "";
        $this->view->news = 2;
        $this->view->banneropt = 'true';

    }
    public function findaheroAction() {
        $this->view->title = "FIND A HERO";
        $this->view->banner = "eco-around-the-world.jpg";
        $this->view->showtext = "true";
        $this->view->showlink = true;
        $this->view->linkURL = "coming-soon";
        $this->view->linkName = "Apply for Contest";
        $this->view->bannertitle = "FIND A HERO";
        $this->view->bannertext = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";        
        $this->view->URL = "/earth-citizenship";
        $this->view->news = 2;
    }

    public function ecoaroundtheworldAction($slugs){
        $service_url = $this->config->application->apiURL.'/atw/fereview/' . $slugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        if ($decoded == null) {
            return $this->response->redirect('/page404');
        }

        $this->view->slugs = $decoded;
        $this->view->orgname = $decoded->orgname;
        $this->view->address = $decoded->address;
        $this->view->news = $decoded->news;
        $this->view->syear = $decoded->syear;
        $this->view->mission = $decoded->statement;
        $this->view->activities = $decoded->activities;
        $this->view->banner = $decoded->logo;
        $this->view->donation  = 1;
        $this->view->banneropt = 'false';
    }
}
