<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
use Modules\Frontend\Models\Featuredprojects as Featuredprojects;
use Modules\Frontend\Models\News as News;
use Modules\Frontend\Models\Pages as Pages;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Donationlogothers as Donationlogothers;
use Modules\Frontend\Models\Calendar as Calendar;
use Modules\Frontend\Models\Settings as Settings;



class CallheroesController extends ControllerBase {

    public function onConstruct(){        
        $this->view->news = 0;
    }

    public function initialize() {

    }

    public function heroesondutyAction($pageslugs) {


        $this->angularLoader(array(
            'HeroesOnDutyCtrl' => '/js/fr/scripts/controllers/callHeroes/heroesOnDuty.js',             
            // 'Login'     => '/js/fr/scripts/factory/login.js',                       
            'configJS'  => '/js/config/config.js',             
        )); 

        
        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
           return $this->response->redirect('maintenance/');
        }


        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';
        $curl = curl_init($service_url_news);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false)  {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);

        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;        
    }


}
