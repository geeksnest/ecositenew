<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;

class MedialibraryController extends ControllerBase
{


  public function onConstruct(){  

    $curl = curl_init($this->config->application->apiURL. '/medialibrary/listcategory');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) 
    {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->liftofcategory =  $decoded;


    $curl = curl_init($this->config->application->apiURL.'/medialibrary/listtags');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if($curl_response == false){
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->newstagslist = $decoded;

    $curl = curl_init($this->config->application->apiURL.'/medialibrary/getarchivesfrontend');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if($curl_response == false){
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Addition info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->archivelist = $decoded;

    
    $curl = curl_init($this->config->application->apiURL.'/medialibrary/banner');
    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
      echo curl_errno($curl);
      curl_close($curl);
      die();
    }
    $decoded2 = json_decode($curl_response);
    
    $this->view->banner = $decoded2[0]->img;
    $this->view->bannertitle = $decoded2[0]->title;
    $this->view->bannertext = $decoded2[0]->description;
    $this->view->titlefontsize = $decoded2[0]->titlefontsize;
    $this->view->descriptionfontsize = $decoded2[0]->descriptionfontsize;
    $this->view->showtext = $decoded2[0]->showtext;
    $this->view->tbgcolor = $decoded2[0]->tbgcolor;
    $this->view->tfcolor = $decoded2[0]->tfcolor;
    $this->view->dfcolor = $decoded2[0]->dfcolor;
    $this->view->btnname = $decoded2[0]->linkname;
    $this->view->btnlink = $decoded2[0]->linkpath;
    $this->view->news = 4;
    $this->view->mediadonate = false;
  }
    public function initialize() {
    }
    
    public function indexAction(){
        $this->angularLoader(array(
            'medialistCtrl' => '/js/fr/scripts/controllers/medialibrary/medialist.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 2;
        $this->view->project = 1;
    }

    public function viewAction() {

        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/view.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 2;
        $this->view->project = 1;
    }

    public function tagAction() {

        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/tag.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 2;
        $this->view->project = 1;
    }

    public function categoryAction() {

        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/category.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 2;
        $this->view->project = 1;
    }

    public function archivesAction() {

        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/archives.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        $this->view->donation = 2;
        $this->view->project = 1;
    }

    public function mymedialibraryAction() {
        $this->angularLoader(array(
            'mymedialistCtrl' => '/js/fr/scripts/controllers/medialibrary/mymedialist.js',             
            'configJS' => '/js/config/config.js',             
        )); 

        $this->view->donation = 1;
        $this->view->project = 1;
    }

    public function addvideoAction() {

        $this->angularLoader(array(
            'addvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/addvideo.js',             
            'configJS' => '/js/config/config.js',             
        )); 
        
        $this->view->donation = 1;
        $this->view->project = 1;
        $this->view->breadcrumbs = "<a href>Learn</a> <a><i class='fa fa-arrow-right'></i></a> <a href>My Media library</a>";
    }

    public function editvideoAction(){

        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/editvideo.js',             
            'configJS' => '/js/config/config.js',             
        )); 

       $this->view->donation = 1;
       $this->view->project = 1; 
    }

    public function previewvideoAction(){
        $this->angularLoader(array(
            'editvideoCtrl' => '/js/fr/scripts/controllers/medialibrary/editvideo.js',             
            'configJS' => '/js/config/config.js',             
        )); 

       $this->view->donation = 1;
       $this->view->project = 1; 
    }

    
}

