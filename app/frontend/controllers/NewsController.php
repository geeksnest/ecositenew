<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;

use Modules\Frontend\Models\Settings as Settings;

class NewsController extends ControllerBase
{
  

  public function initialize() {
    $curl = curl_init($this->config->application->apiURL. '/news/getcategoryfrontend');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) 
    {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->liftofcategory =  $decoded;


    $curl = curl_init($this->config->application->apiURL.'/news/gettagsfrontend');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if($curl_response == false){
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->newstagslist = $decoded;


    $curl = curl_init($this->config->application->apiURL.'/news/getarchivesfrontend');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if($curl_response == false){
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Addition info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->archivelist = $decoded;


    $curl = curl_init($this->config->application->apiURL.'/news/newsbanner');

    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
      echo curl_errno($curl);
      curl_close($curl);
      die();
    }
    $decoded2 = json_decode($curl_response);
    
    $this->view->banner = $decoded2[0]->img;
    $this->view->bannertitle = $decoded2[0]->title;
    $this->view->bannertext = $decoded2[0]->description;
    $this->view->titlefontsize = $decoded2[0]->titlefontsize;
    $this->view->descriptionfontsize = $decoded2[0]->descriptionfontsize;
    $this->view->showtext = $decoded2[0]->showtext;
    $this->view->tbgcolor = $decoded2[0]->tbgcolor;
    $this->view->tfcolor = $decoded2[0]->tfcolor;
    $this->view->dfcolor = $decoded2[0]->dfcolor;
    $this->view->news = 1;
  }


  public function indexAction()
  {

   $page = Settings::findFirst("id=1");
   if ($page->value1 == 1) {

     return $this->response->redirect('maintenance/');
        // $this->view->message = $msg ;
   }      

  
   // GOOGLE ANALYTICS
        $service_url_analytics = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_analytics);
        
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;

        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// 

}
public function newsAction()
{

  

}

public function viewAction($newsslugs)
{


  $service_url = $this->config->application->apiURL . '/news/view/' . $newsslugs;

  $curl = curl_init($service_url);
  curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $curl_response = curl_exec($curl);
  if ($curl_response == false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
  }
  curl_close($curl);
  $decoded = json_decode($curl_response);

  if ($decoded[0] == "") {
    return $this->response->redirect($this->config->application->baseURL . '/page404');
  }
  
  $this->view->newsid = $decoded[0]->newsid;
  $this->view->title = $decoded[0]->title;
  $this->view->newsslugs = $decoded[0]->newsslugs;
  $this->view->author = $decoded[0]->author;
  $this->view->summary = $decoded[0]->summary;
  $this->view->body = $decoded[0]->body;
  $this->view->imagethumb = $decoded[0]->imagethumb;
  $this->view->videothumb = $decoded[0]->videothumb;
  $this->view->category = $decoded[0]->category;
  $this->view->date = $decoded[0]->date;
  $this->view->categoryname = $decoded[0]->categoryname;
  $this->view->categoryid= $decoded[0]->categoryid;
  $this->view->name = $decoded[0]->name;
  $this->view->authorid = $decoded[0]->authorid;
  $this->view->about = $decoded[0]->about;
  $this->view->image = $decoded[0]->image;

  $this->view->metatitle = $decoded[0]->metatitle;
  $this->view->metadesc = $decoded[0]->metadesc;
  $this->view->metakeyword = $decoded[0]->metakeyword;

  




  $service_url_news_tags = $this->config->application->apiURL. '/news/frontend/listtags/' . $newsslugs;


  $curl = curl_init($service_url_news_tags);

  curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

  $curl_response = curl_exec($curl);

  if ($curl_response === false) 
  {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
  }

  curl_close($curl);
  $decoded = json_decode($curl_response);

  $this->view->newstags = $decoded;


  $service_url_news_categories = $this->config->application->apiURL. '/news/frontend/listcategories/' . $newsslugs;


  $curl = curl_init($service_url_news_categories);

  curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

  $curl_response = curl_exec($curl);

  if ($curl_response === false) 
  {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
  }

  curl_close($curl);
  $decoded = json_decode($curl_response); 

  $this->view->newscat = $decoded;

  
}

public function authorAction($authorid){
    

    $service_url = $this->config->application->apiURL . '/news/author/' . $authorid;

    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response == false) {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);

    if ($decoded[0] == "") {
      return $this->response->redirect($this->config->application->baseURL . '/page404');
    }

    $this->view->metatitle = $decoded[0]->name;
    $this->view->name = $decoded[0]->name;

}

public function categoryAction($catname){ 

  $service_url_news_catname = $this->config->application->apiURL. '/news/category/' . $catname;
  $curl = curl_init($service_url_news_catname);
  curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $curl_response2 = curl_exec($curl);
  if ($curl_response2 === false)   {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
  }
  curl_close($curl);
  $decoded2 = json_decode($curl_response2);
  // $this->view->catname =  $decoded[0]->categoryname;
  if($catname == 'upcoming-programs'){
    $this->view->catname2 =  'Upcoming Programs';
    $this->view->metatitle =  'Upcoming Programs';
    $this->view->metacat =  'Upcoming Programs';
  }else if($catname == 'recent-activities'){
    $this->view->catname2 =  'Recent Activities';    
    $this->view->metatitle =  'Recent Activities';    
    $this->view->metacat =  'Recent Activities';    
  }else if($catname == 'webinar-archive'){
    $this->view->catname2 =  'Webinar Archive';    
    $this->view->metatitle =  'Webinar Archive';    
    $this->view->metacat =  'Webinar Archive';    
  }else if($catname == 'mindful-living-tips'){
    $this->view->catname2 =  'Mindful Living Tips';
    $this->view->metatitle =  'Mindful Living Tips';
    $this->view->metacat =  'Mindful Living Tips';
  }else{
    $this->view->catname2 =  $decoded2->categoryname;
    $this->view->metatitle =  $decoded2->categoryname;
    $this->view->metacat =  $decoded2->categoryname;
  }
  

  

}

public function tagsAction($tagname){ 

  $service_url_news_tag = $this->config->application->apiURL. '/news/tags/' . $tagname;
  $curl = curl_init($service_url_news_tag);
  curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $curl_response2 = curl_exec($curl);
  if ($curl_response2 === false)   {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
  }
  curl_close($curl);
  $decoded2 = json_decode($curl_response2);
  $this->view->tagname2 =  $decoded2->tags;
  $this->view->metatitle =  $decoded2->tags;
  $this->view->metatag =  $decoded2->tags;
  

  }

  public function previewAction() {
    $this->view->title = $_GET['title'];
    $this->view->summary = $_GET['summary'];
    $this->view->body = $_GET['body'];
    $this->view->date = $_GET['date'];

    $this->view->metatitle = $_GET['metatitle'];
    $this->view->metadesc = $_GET['metadesc'];
    $this->view->metakeyword = $_GET['metakeyword'];

    $this->view->author = $_GET['author'];
    $this->view->authorname = $_GET['authorname'];
    $this->view->authorimage = $_GET['authorimage'];
    $this->view->authorabout = $_GET['authorabout'];
    $this->view->tag = $_GET['tag'];
    $this->view->cat = $_GET['category'];
    $this->view->categoryname = $_GET['categoryname'];  

    $this->view->imagethumb = $_GET['imagethumb'];     
    $this->view->videothumb = $_GET['videothumb'];    


    $service_url_news_cat = $this->config->application->apiURL. '/news/getcategoryfrontend';

    $curl = curl_init($service_url_news_cat);

    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $curl_response = curl_exec($curl);

    if ($curl_response === false) 
    {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('error occured during curl exec. Additional info: ' . var_export($info));
    }

    curl_close($curl);
    $decoded = json_decode($curl_response);

    $this->view->liftofcategory =  $decoded;

     



  }


  public function archiveAction() { 

    
  }



}