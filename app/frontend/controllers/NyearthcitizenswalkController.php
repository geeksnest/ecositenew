<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;
use Modules\Frontend\Models\Memberconfirmation as Memberconfirmation;
use Modules\Frontend\Models\Members as Members;
use Modules\Frontend\Models\Donationlog as Donationlog;
use Modules\Frontend\Models\Settings as Settings;


class NyearthcitizenswalkController extends ControllerBase {

    public function initialize() {
        
    }

    public function indexAction() {

        $page = Settings::findFirst("id=1");
        if ($page->value1 == 1) {
             
           return $this->response->redirect('maintenance/');
        // $this->view->message = $msg ;
        }


        $service_url = $this->config->application->apiURL . '/pages/getpage/18';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        // GOOGLE ANALYTICS
        $service_url_news = $this->config->application->apiURL. '/settings/loadscript';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
      
        $this->view->script_google = $decoded->gscript;        

        $this->view->donation = 1;
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// 
        $data = "";
        $this->view->usersdonated = Donationlog::count(array("distinct" => "useremail"));
        $amount = Donationlog::sum(array("column" => "amount"));
        $this->view->amounts = number_format($amount, 2, '.', '');
        $this->view->totaltrans = count($data);

        $this->view->metatitle = 'NY Walk';
        $this->view->metadesc = 'Join the Earth Citizens Walk in New York to promote Mindful Living for a Sustainable World as our true Legacy for Future Generations.';
        $this->view->metacat = 'NY Walk';
        $this->view->metatag = 'NY Walk';
        $this->view->metakeyword = 'NY Walk';
    }

    
}
