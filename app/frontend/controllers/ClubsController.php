<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;

use Modules\Frontend\Models\Settings as Settings;

class ClubsController extends ControllerBase
{

  public function initialize() {

  }


  public function indexAction()
 {
  $this->angularLoader(array(
    'Clubctrl' => '/js/fr/scripts/controllers/club/clublist.js',           
    'configJS' => '/js/config/config.js',             
    )); 
  $page = Settings::findFirst("id=1");
  if ($page->value1 == 1) {

    return $this->response->redirect('maintenance/');
       // $this->view->message = $msg ;
  }      
  // GOOGLE ANALYTICS
       $service_url_analytics = $this->config->application->apiURL. '/settings/loadscript';

       $curl = curl_init($service_url_analytics);
       
       curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       $curl_response = curl_exec($curl);
       
       if ($curl_response === false) 
       {
           $info = curl_getinfo($curl);
           curl_close($curl);
           die('error occured during curl exec. Additional info: ' . var_export($info));
       }
       
       curl_close($curl);
       $decoded = json_decode($curl_response);
     
       $this->view->script_google = $decoded->gscript;

       $service_url = $this->config->application->apiURL.'/pages/getpage/earth-citizens-clubs';
       $curl = curl_init($service_url);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);
       $curl_response = curl_exec($curl);
       if ($curl_response === false) {
           $info = curl_getinfo($curl);
           curl_close($curl);
           die('error occured during curl exec. Additioanl info: ' . var_export($info));
       }
       curl_close($curl);
       $decoded = json_decode($curl_response);
       $this->view->banner = $decoded->banner;
       $this->view->banneropt = $decoded->banneropt;
       $this->view->bannertitle = $decoded->bannertitle;
       $this->view->titlefontsize = $decoded->titlefontsize;
       $this->view->bannertext = $decoded->bannertext;
       $this->view->descriptionfontsize = $decoded->descriptionfontsize;
       $this->view->showtext = $decoded->showtext;
       $this->view->tbgcolor = $decoded->tbgcolor;
       $this->view->tfcolor = $decoded->tfcolor;
       $this->view->dfcolor = $decoded->dfcolor;
       $this->view->disonli = 1;
       $this->view->btnname = $decoded->btnname;
       $this->view->btnlink = $decoded->btnlink;
       // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);earth-citizens-clubs

       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ////

}
  

  public function viewAction($slugs)
  {
     $this->angularLoader(array(
      'Clubctrl' => '/js/fr/scripts/controllers/club/clublist.js',           
      'configJS' => '/js/config/config.js',             
      )); 
     $curl = curl_init($this->config->application->apiURL.'/feclubs/clubinfo/' . $slugs);

    curl_setopt($curl, CURLOPT_CAINFO, $this->config->curlRest);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
      echo curl_errno($curl);
      curl_close($curl);
      die();
    }
    $decoded = json_decode($curl_response);
if ($decoded) {
    $this->view->banneropt = 'false';
    $this->view->name = $decoded[0]->name;
    $this->view->description = $decoded[0]->description;
    $this->view->address1 = $decoded[0]->address1;
    $this->view->address2 = $decoded[0]->address2;
    $this->view->city = $decoded[0]->city;
    $this->view->state = $decoded[0]->state;
    $this->view->postal = $decoded[0]->postal;
    $this->view->country = $decoded[0]->country;
    $this->view->characteristics = $decoded[0]->characteristics;
    $this->view->age = $decoded[0]->age;
    $this->view->interests = $decoded[0]->interests;
    $this->view->meeting = $decoded[0]->meeting;
    $this->view->website = $decoded[0]->website;
    $this->view->facebook = $decoded[0]->facebook;
    $this->view->contactperson = $decoded[0]->contactperson;
    $this->view->contactnumber = $decoded[0]->contactnumber;
    $this->view->emailadd = $decoded[0]->emailadd;
    $this->view->date_created = $decoded[0]->date_created;
    $this->view->image = $decoded[0]->image;
    $this->view->displayclub = true;
}else{
   return $this->response->redirect($this->config->application->baseURL . '/page404');
}
   

}


}