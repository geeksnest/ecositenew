<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Earth Citizens Organization</title>


    <link rel="stylesheet" href="/css/template_css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/template_css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/template_css/main.css">

    <link rel="stylesheet" href="/css/template_css/slidercss.css"> 
    <link rel="stylesheet" type="text/css" media="screen, print, projection"  href="/css/template_css/dropdown_css.css"></link>      

    <link rel="stylesheet" type="text/css" href="/css/template_css/initcarousel-1.css">

   <!--  <link rel="stylesheet" href="/css/template_css/index.css"> -->
    <link rel="stylesheet" href="/css/template_css/responsivemobilemenu.css">

    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>

    <!-- FROM MAIN -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">   
        <link rel="stylesheet" type="text/css" href="/css/fr/color.css"/>
        <link rel="stylesheet" type="text/css" href="/css/fr/reset.css"/>
        <link rel="stylesheet" href="css/fr/horizontal.css"/>
    <!-- END FROM MAIN --> 
    <link rel="stylesheet" href="/css/template_css/temp_index.css">    

</head>

<body ng-controller="indexMainCtrl">
    <!-- EFNRE START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->
    <div class="container-fluid">
        <div class="eco-container top">
            <div class="top-contact" style="float: left;">
                <span><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;info@earthcitizens.org&nbsp;&nbsp;</span> 
            </div>
            <div class="top-social-icon" style="text-align: right;">                
                <span>&nbsp;&nbsp;&nbsp;<a href="https://www.youtube.com/user/goearthcitizens"><i class="fa fa-youtube"></i></a></span>
                <span>&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/EarthCitizensOrganization" ><i class="fa fa-facebook"></i></a></span>
                <span>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/goearthcitizens" ><i class="fa fa-twitter"></i></a></span>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="navsticky">
        <div class="eco-container">
            <div id="ecologo" ><a href="http://www.earthcitizens.org/indextemp"><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;"></a></div>
            <div class="ecositenav">
                <nav  class="nav">
                    <ul class="dropdown" >
                        <li class="drop">
                            <a href="../indextemp/#abouteco" id="selected4">
                                <div id="menu_icon_about" class="menudiv1">                                    
                                        <div class="menuicon1"></div>
                                        <div class="p">ABOUT US</div>
                                    </div>
                            </a>

                        </li>
                        <li class="drop">
                            <a href="../indextemp/#ouractivities" id="selected2">
                                <div id="menu_icon_training" class="menudiv2">                                    
                                        <div class="menuicon2"></div> 
                                        <div class="p">OUR ACTIVITIES</div>
                                    </div>
                            </a>
                        </li>
                        <li class="drop">
                            <a href="../indextemp/#ourprograms" id="selected3">
                                <div id="menu_icon_peace_parks" class="menudiv3">                                    
                                        <div class="menuicon3"></div> 
                                        <div class="p">OUR PROGRAMS</div>
                                    </div>
                            </a>
                        </li>
                        <li class="drop">
                            <a href="http://heroesconnect.org" target="_blank" id="selected1">
                                 <div id="menu_icon_what_is_eco" class="menudiv4">                                    
                                        <div class="menuicon4"></div> 
                                        <div class="p">HEROES CONNECT</div>
                                    </div>
                            </a>

                        </li>

                        <li class="drop">
                            <a href="../indextemp/#supportus" id="selected5">
                                <div id="menu_icon_support_us" class="menudiv5">                                    
                                        <div class="menuicon5"></div> 
                                        <div class="p">SUPPORT US</div>
                                    </div>
                            </a>
                        </li>
                    </ul>
                        <div class="clearfix"></div>
                    </nav>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="container-fluid"><div class="bluebar"></div></div>   
        </div>
    </div>
    <div class="container-fluid">
        <div class="mob-ecologo"  style="width:115px;margin:auto;padding-top:5px;"><a href="http://www.earthcitizens.org/indextemp"><img  class="logo-size" src="/images/template_images/ecologo1.png" style="width:115px;"></a></div>
        <div class="navsticky">
            <div class="mobileMenu mobileMenu2">
                <nav role="navigation" class="navbar navbar-default nav">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand"><p>ECO</p></a>
                    </div>
                    <!-- Collection of nav links and other content for toggling -->
                   <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="../indextemp/#abouteco">ABOUT US</a>
                        </li>
                        <li>
                            <a href="../indextemp/#ouractivities">OUR ACTIVITIES</a>
                        </li>
                        <li>
                            <a href="../indextemp/#ourprograms">OUR PROGRAMS</a>
                        </li>
                        <li>
                            <a href="http://heroesconnect.org">HEROES CONNECT</a>
                        </li>
                        <li>
                            <a href="../indextemp/#supportus">SUPPORT US</a>
                        </li>
                    </ul>
                </div>
                </nav>
            </div>
        </div>
    </div>
    
    <div class="donation-fluid interior-headerBanner2" ng-controller='DonateCtrl' >
        <div id="interior" class="row eco-container">
            <div class="col-lg-6 donation-banner" style="margin-top:20px;">                   
                <div class="donate-title">Make a Donation</div>                
                <div>
                    <p class="phar">Please log in to make your donation</p>
                    <p class="phar">Your $10 minimum donation will register you as an Earth Citizen.</p>
                    <p class="phar"><a href="#causes"  style="color:#777;font-size:15px;">Need an account? Create one now</a></p>
                </div>
                <form ng-show="paypalformshow1 == {[{ disp}]}" name="formlogin" novalidate>  
               
                <div class="col-lg-8 " style="margin-left:-18px">
                    <div class="form-group">
                        <input class="form-control rounded" type="text" name="email" ng-model="user.email" name="email" required placeholder ="Email Address" />
                    </div>
                    <div class="form-group">
                        <input class="form-control rounded" type="password" name="password" ng-model="user.password" name="password" required placeholder ="Password" />
                        <a ng-show="process == true"  style="color:#777;font-size:15px;">Checking Credentials</a>
                         <label ng-show="process == true"  style="color:#777;font-size:15px;">Checking your info...</label> <br/>
                        <span class="formerror" ng-show="errormessage != ''"  style="color:#777;font-size:15px;">{[{errormessage}]}</span>
                    </div>
                </div>
                <div class="col-lg-2 " style="margin-left:-20px">
                    <!-- <a href="" class="danate-button" ng-show="process == false" ng-click="clickDonate(user)">DONATE NOW</a> -->
                    <button type="submit" ng-click="clickDonate(user)"  class="danate-button">Donate Now</button>
                </div>
                </form>

                
                <p ng-show="statusmessage != ''" ng-bind="statusmessage"></p>
                    <form name="sendemailform" ng-show="showemail == true && statusmessage == ''">
                        <div class="inputcont">
                            <div style="margin-top:-20px;">
                                <p ng-show="emailPurpose == 'resendActivation'" style="color:#777;font-size:15px;">You activation link will be sent yo your email.</p>
                                <p ng-show="emailPurpose == 'createPassword'" style="color:#777;font-size:15px;"> Your new password will be sent to your email.</p>
                            </div>
                            <input class="form-control rounded" type="text" name="email" ng-model="send.email" name="sendemail" required placeholder ="Enter your Email Address" /><br/>
                            <input type="button" class="btn m-b-xs w-xs btn-info" value="Send" ng-click="sendemail(send)" ng-hide="sendemailform.$invalid" style="padding:5px;"/> 
                            <input type="button" class="btn m-b-xs w-xs btn-info" value="Cancel" ng-click="hideForm()" style="padding:5px;" />
                        </div>
                    </form>

                    <form  ng-show="paypalformshow == true" class="paypalform" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="margin-left:50px;">
                        <div></div>
                        <p  style="color:#777;font-size:15px;">Hello <strong>{[{membername}]}</strong>! <br/>Thank you for your support!</p>
                        <div style="font-size: 40px; padding:15px 0px 10px 0px; color:#333;text-align">$ 
                        <input type="text" name="donateamount" value="10.00" ng-model="donateamount" ng-change="change(donateamount)" style="height: 45px; width:80px; font-size: 30px; color:#000; border-radius:10px; border-style:none;" > 
                        </div>
                        <input type="hidden" name="cmd" value="_donations">
                        <input type="hidden" name="business" value="accounting@earthcitizens.org">
                        <input type="hidden" name="lc" value="US">
                        <input type="hidden" name="custom" ng-value="user.email">
                        <input type="hidden" name="item_name" value="Earth Citizens Organization">
                        <input type="hidden" name="amount" value="10.00" ng-value="donateamount">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="no_note" value="0">
                        <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                        <input ng-hide="donateamount < 10" type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <div  ng-show="donateamount < 10" class="alert alert-error">Please a minimum of $10 for your donation.</div>
                        
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                     <div class="col-lg-12 " style="margin-left:-18px;">
                        <a href="" ng-click="setEmailFunc('createPassword')"style="color:#777;font-size:15px;">Forgot your password?</a> | 
                        <a href="" ng-click="setEmailFunc('resendActivation')"  style="color:#777;font-size:15px;">Resend Activation Link?</a>
                    </div>
            </div>
            <div class="col-lg-6 donation-banner1" style="margin-top:20px;">                        
                <div class="textalign-right">
                    <div class="headtext13">THANK YOU TO THE </div>
                    <div class="headtext2"><span  ng-bind="userscount"></span></div> <!-- Only display ZERO  -->
                    <div class="headtext21">EARTH CITIZENS</div>
                    <div class="headtext13">WHO HAVE CURRENTLY RAISED</div>
                    <div class="headtext2_1"> $<span  ng-bind="donationcount"></span></div><!-- Only display ZERO -->
                    <div class="headtext21_1">IN DONATIONS</div>
                </div>
            </div>

        </div> 
    </div>

       



        <!--===begin container===-->  
        <div class="container-fluid" ng-controller='registrationCtrl' >
            <div class="eco-container" >
                <div class="wrapper-md " >
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="h1_1" style="margin-top:15.5px;">Registration</div>
                            <div class="gp">
                                <p class="phar">BECOME AN EARTH CITIZEN BY REGISTERING AND MAKING A DONATION</p>
                                <p>
                                    As a responsible citizen of the Earth, I agree to develop my body, mind, and spirit to their greatest capacity and use them to better the life that I share with others both locally and globally.
                                </p>
                                <p>
                                    As an honorable citizen of the Earth, I agree to demonstrate the highest human virtues and the greatness of the human spirit in order to encourage and inspire others to manifest their best for the benefit of all.
                                </p>
                                <p>
                                    As a caring citizen of the Earth, I agree to practice mindful living to create positive changes in my lifestyle and work with my fellow Earth Citizens to create a peaceful sustainable world.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 panel-body" >
                            <div class="h1_1" ><!-- <img src="/images/template_images/ecologo.png" width="120px" height="60px;" />  -->Sign Up</div>
                            <form name="form" novalidate class="ng-pristine ng-valid" style="color:#777;fonts:0.8em sans-serif bold">
                                <div class="form-group">
                                    Username:
                                    <span class="formerror" ng-show="usernametaken != ''">Username has already been taken.</span>
                                    <input type="text" class="form-control rounded" name="" ng-model="user.username" name="username" required />
                                </div>
                                <div class="form-group">
                                    Email address
                                    <span class="formerror" ng-show="emailtaken != ''">Email already taken.</span> 
                                    <input type="email" class="form-control rounded" name="" ng-model="user.email" name="email" required />
                                </div>
                                <div class="form-group">
                                    Password
                                    <span class="formerror" ng-show="passmin == true">Shoud be minimum of 6 characters.</span>  
                                    <input type="password" class="form-control rounded" name="" ng-model="user.password" name="password" required />
                                </div>
                                <div class="form-group">
                                    Re-Type Password
                                    <span class="formerror" ng-show="user.password != user.repassword">Your Password Should be Equal.</span>
                                    <input type="password" class="form-control rounded" name="" ng-model="user.repassword" name="repassword" required/>
                                </div>
                                <div class="form-group">
                                    <div style="padding-bottom:10px;">Birthday</div>
                                    <span class="datelabel">Day:  </span><select ng-model="user.bday"  class="btn btn-default dropdown-toggle" ng-options="d.val for d in day" required></select>
                                    <span class="datelabel">Month:</span><select  ng-model="user.bmonth"class="btn btn-default dropdown-toggle" ng-options="m.val for m in month" required></select>
                                    <span class="datelabel">Year: </span><select ng-model="user.byear" class="btn btn-default dropdown-toggle" ng-options="y.val for y in year" required></select>
                                </div>
                                <div class="form-group">
                                    Gender:<br>
                                    <div class="radio">
                                        <label class="i-checks">
                                            <input class="radioinput" type="radio" value="male" ng-model="user.gender" name="3">
                                            <i></i>
                                            Male
                                        </label>
                                        <label class="i-checks">
                                            <input class="radioinput" type="radio" value="female" ng-model="user.gender" name="4">
                                            <i></i>
                                            Female
                                        </label>
                                          <label class="i-checks">
                                            <input class="radioinput" type="radio" value="other" ng-model="user.gender" name="4">
                                            <i></i>
                                            Other
                                        </label>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    Locaton
                                        <!-- <select name="account" class="form-control m-b">
                                            <option value="option 1">lopcation</option>
                                        </select> -->
                                        <select ng-model="user.location" class="location form-control m-b" ng-options="cn.name for cn in countries" required>
                                            <option value="">United States</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        Zip Code
                                        <input type="text" name="" class="form-control" ng-model="user.zipcode" name="zipcode" required />
                                    </div>
                                    <!-- <button type="submit" class="btn btn-sm btn-primary">Submit</button> -->
                                    <label ng-show="process == true">Processing Registration...</label>
                                    <input type="button" name="" id="abouteco" value="Submit" ng-show="process == false" class="btn btn-sm btn-primary" ng-click="register(user)" ng-disabled="form.$invalid" />
                                </form>
                            </div>
                        <div class="col-sm-12">
                            <div class="eco-col-4" style="padding:50px 0px 50px 0px; font-style: italic; font-size:17px;color:#777;">
                                <p>
                                    ECO is a 501(c)3 non-profit organization and your donation to ECO is tax-deductible deductible as charitable contribution for which donor receives no material benefits in return unless specified otherwise in the donation receipt. As a member of ECO, you will receive eNewsletter and online support for your activities in your community.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
       
     <div class="container-fluid">
    <div  class="footer-bg">
        <div class="eco-container">
            <div class="row">
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>MISSION STATEMENT<br/><br/></span>
                    <div style="width:230px; padding-right: 15px;">
                        <span style="font-size: 12px;">
                            Earth Citizens Organization (ECO) is a nonprofit that promotes natural health and mindful living for a sustainable world. An Earth Citizen is a person who understands that we are all citizens of the Earth and takes responsibility for its well-being.<br/><br/>
                        </span>
                    </div>

                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>NAVIGATION<br/><br/></span>
                    <div style="width:190px; padding-right: 55px;">
                        <span style="font-size: 12px;">
                            <nav  class="nav" style="padding-top: 0px;">
                                <a href="#abouteco" style="color:#ffffff;">ABOUT US</a><br/>
                                <a href="#ouractivities" style="color:#ffffff;">OUR ACTIVITIES</a><br/>
                                <a href="#ourprograms" style="color:#ffffff;">OUR PROGRAMS</a><br/>
                                <a href="http://heroesconnect.org" style="color:#ffffff;">HEROES CONNECT</a><br/>
                                <a href="#supportus" style="color:#ffffff;">SUPPORT US</a><br/>
                                <a href="../donation" style="color:#ffffff;">DONATE</a><br/><br/>
                            </nav>
                        </span>
                    </div>
                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>ADD US<br/><br/></span>
                    <div style="width:190px; padding-right: 55px;">
                        <span style="font-size: 12px;">
                            <ul>
                                <li><a href="https://www.youtube.com/user/goearthcitizens" style="color:#ffffff;"><i class="fa fa-youtube" style="color: #ffffff;"></i>&nbsp;&nbsp;YouTube</a><br/><br/></li>
                                <li><a href="https://www.facebook.com/EarthCitizensOrganization" style="color:#ffffff;"><i class="fa fa-facebook" style="color: #ffffff;"></i>&nbsp;&nbsp;Facebook</a><br/><br/></li>
                                <li><a href="https://twitter.com/goearthcitizens" style="color:#ffffff;"><i class="fa fa-twitter" style="color: #ffffff;"></i>&nbsp;&nbsp;Twitter</a></li>
                            </ul><br/><br/>
                        </span>
                    </div>
                </div>
                <div class="col-md-3" style="color: #ffffff !important; float: left;">
                    <span>CONTACT US<br/><br/></span>
                    <div style="width:230px; padding-right: 15px;">
                        <span style="font-size: 12px;">
                            Earth Citizens Organization<br/>
                            340 Jordan Road Sedona, AZ 86336
                            <br/><br/>
                            <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Email: goearthcitizens@gmail.com
                            <br/><br/>
                        </span>
                    </div>
                </div>                    
            </div>
            <div class="row" style="padding: 10px 0px 10px 0px;">
                <div  style="  width: 100%;  background-color: #ffffff;  height: 3px;"></div>
            </div>
            <div class="row" style="padding-top: 2px;">
                <p style="font-size: 10px; text-align: center;color:#ffffff;">
                    ©2015 Earth Citizens Organization All rights reserved
                </p>
            </div>
        </div>
    </div>
</div>  

    <script>
        var API_URL = '<?php echo $this->config->application->apiURL; ?>';
    </script>

        <script src="/js/template_js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script type="text/javascript" src="/js/template_js/jquery-1.9.1.min.js"></script>


        <script src="/js/template_js/jquery.min.js"></script>
        <script src="/js/template_js/bootstrap.min.js"></script>
        <!-- dropdown -->
        <script src="/js/template_js/jquery.js"></script>
        <script src="/js/template_js/dropdown_js.js"></script>
        <script src="/js/template_js/dropdown_hover_ajax.js"></script>
        <!-- end dropdown -->
        <!-- carousel -->
        <script src="/js/template_js/amazingcarousel.js"></script>
        <script src="/js/template_js/initcarousel-1.js"></script>
        <!-- end carousel -->
        <script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="/js/template_js/vendor/bootstrap.min.js"></script>

        <script src="/js/template_js/main.js"></script>



        <script src="/js/ckeditor/ckeditor.js"></script>
        <script src="/js/ckeditor/styles.js"></script>




        <!-- JS FROM MAIN -->
        
        <!--==========================================BOOSTRAP JS=============================================-->
        <script type="text/javascript" src="/js/fr/jquery.counterup.min.js"></script>
        <script type="text/javascript" src="/js/fr/waypoint.min.js"></script>
        <script>
                    $(document).ready(function () {

                        $(".mapp").hide();
                        $(".map-btncl").hide();
                        $(".map-btn").click(function () {
                            $(".mapp").toggle("slow");
                        });

                        $('.counter').counterUp({
                            delay: 10,
                            time: 1000
                        });


                    });

        </script>

        <script type="text/javascript" src="/js/angular/angular.min.js"></script> 
        <script type="text/javascript" src="/js/angular/angular-ui-router.min.js"></script> 
        <script type="text/javascript" src="/js/angular/ui-validate.js"></script> 
        <script type="text/javascript" src="/js/fr/jquery.simplemodal.1.4.4.min.js"></script>

        <script type="text/javascript" src="/js/fr/app.js"></script>  <!-- APP JS ///////////////////////////////////////////////////////////-->

        <script type="text/javascript">
    $(document).ready(function() {
            // grab the initial top offset of the navigation 
            var stickyNavTop = $('.navsticky').offset().top;
            
            // our function that decides weather the navigation bar should have "fixed" css position or not.
            var stickyNav = function(){
                var scrollTop = $(window).scrollTop(); // our current vertical position from the top

                // if we've scrolled more than the navigation, change its position to fixed to stick to top,
                // otherwise change it back to relative
                if (scrollTop > stickyNavTop) { 
                    $('.navsticky').addClass('sticky');
                } else {
                    $('.navsticky').removeClass('sticky'); 
                }
            };

            stickyNav();
            // and run it again every time you scroll
            $(window).scroll(function() {
                stickyNav();
            });
        });
</script>

    <?php echo $script_google; ?>
    
    </body>
    </html>





