<?php echo $this->getContent() ?>

    <div class="container-fluid">
        <div class="eco-container"><br>
            <?php if($actbanner != ""){ ?>
        	<div style="background: url(<?php echo $this->config->application->apiURL; ?>/images/featurebanner/<?php echo $actbanner; ?>) 0px 0px;width: 100%;height: 400px;background-size: cover;"></div>
            <?php } ?>
            <div>
                <p style="color:#5386c1;font-size:36px;padding-top:10px;line-height:30px">
                    <?php echo $acttitle; ?><br>
                    <font style="font-size:18px"><?php echo 'Activity Date: '.date('F j, Y',strtotime($df)) .' - '. date('F j, Y',strtotime($dt)); ?></font>
                </p>
            </div>
            <div>
            	<p style="font-family: 'Noto Sans', sans-serif;text-indent:10px"><?php echo $actdesc; ?></p>            	
            </div>
            
        <div style="padding-top:20px; float:right">
            <a href="<?php echo $this->config->application->baseURL; ?>/activity/calendar/<?php echo $pageslugs; ?>">
                <span class="btn bg-info">Back to activity</span>
            </a>
        </div>
        </div>
    </div>
