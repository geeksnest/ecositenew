<?php echo $this->getContent() ?>
<div class="container-fluid">
  <div class="eco-container">
    <div ng-controller="CalenCtrl">

      <div class="wrapper-md bg-light b-b">
        <input type="button" value="List View" class="pull-right" ng-click="lisview()" style="width:100px;height:35px">
        <input type="button" value="Calendar View" class="pull-right" ng-click="calview()" style="width:120px;height:35px">
        <h1 class="m-n font-thin h3">Calendar of Activity</h1>
      </div>

      <div class="hbox hbox-auto-xs hbox-auto-sm eco-container" ng-show="calview1">
        <div class="col wrapper-md">      
          <div class="pos-rlt">
            <!-- begin view calendar -->
            <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
            <!-- end view calendar -->
          </div>
        </div>
      </div>

      <!-- list view -->
      <div class="eco-container" ng-show="lisview1">
        <?php 
        $lisva = $viewcalendar;
        foreach ($lisva as $key => $value) {
          $limitlenght = strrpos($lisva[$key]->actdesc, ' ');
          if($limitlenght > 275){
              $removedimg = strip_tags($lisva[$key]->actdesc, '<img>');
              $limitcontent = substr($removedimg,0,275);
              $limitlenght = strrpos($limitcontent, ' ');
              $limitlenght = strrpos($limitcontent, ' ');         
          }else{
              $removedimg = strip_tags($lisva[$key]->actdesc, '<img>');
              $limitlenght = strrpos($lisva[$key]->actdesc, ' ');
          }
        ?>
          <table width="100%"><tr valign="top"><td width="145px">
            <?php 
              $imgurl_check = $this->config->application->apiURL."/images/featurebanner/".$lisva[$key]->actbanner;
              if(!is_array(@getimagesize($imgurl_check))){
                $imglink = $this->config->application->apiURL."/images/featurebanner/nia.png";
              }else{
                $imglink = $this->config->application->apiURL."/images/featurebanner/".$lisva[$key]->actbanner;
              }
            ?>
            <div class="fillmyimage"><img src="<?php echo $imglink; ?>"></div>
           
            <b style="font-size:18px; text-transform:capitalize"><?php echo $lisva[$key]->acttitle; ?></b>
            <p style="color:#5386c1"><?php echo date("F j, Y", strtotime($lisva[$key]->df)).' - '.date("F j, Y", strtotime($lisva[$key]->dt)); ?></p>
            <?php 
              echo $viewcontent = substr($removedimg,0,$limitlenght).'...';
            ?>
            <a href="<?php echo $this->config->application->baseURL; ?>/activity/viewer/<?php echo $lisva[$key]->actid ."/". $pageslugs; ?>">
              <span class="label bg-info">read more</span>
            </a>
          </td></tr><tr><td colspan="2">&nbsp;</td></tr></table><br>
        <?php } ?>
      </div>

    </div>
  </div>
</div>