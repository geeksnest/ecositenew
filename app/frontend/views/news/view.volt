<?php echo $this->getContent()?>
<div class="container-fluid" ng-controller="NewsCtrl"> 
  <div class="eco-container" id="newslist">   
    <div class="row">  
    <div class="col-sm-12">
      <p class="subtitle"></p>
        <!-- <span class="newsdetails">News Details</span> -->
      <p class="subtitle"></p>
      <p class="subtitle"></p>
    </div>  

      <div class="col-sm-9  greyborder">
      
        <div class="row">

          <div class="col-sm-12">
            <span class="titlenews"><?php echo $title; ?></span>
          </div>
          <?php if($summary !=''){?>
          <div class="col-sm-12">        
            <div class="font1 size14 word-wrap">
              <br/>
              <?php echo $summary; ?>
              <br/><br/>
            </div>
          </div> 
          <?php } ?>      
          <div class="col-sm-12">
            <span class="thin-font1 orange">by <strong><a href="<?php echo $this->config->application->baseURL; ?>/news/author/<?php echo $authorid; ?>"><?php echo $name; ?></a> / <?php echo date('F d, Y', strtotime($date)) ?></span></strong>
            <hr>                 
          </div>  
          
         
          <div class="col-sm-12">
            <div class="font1 size15 center">
            <?php if($imagethumb != ''){

              ?>
              <a class="fancybox" href="<?php echo $this->config->application->amazonlink."/uploads/newsimages/".$imagethumb; ?>" data-fancybox-group="gallery">
                <div class="news-view-thumbnail" style="background-image: url(<?php echo $this->config->application->amazonlink."/uploads/newsimages/".$imagethumb; ?>);"></div>
              </a>
            <?php 
                  } 
                  if($videothumb != ''){ echo $videothumb; }?>
              <br>
              <br>
            </div>
          </div>  
          
         
          <div class="col-sm-12">
            <div class="font1 size15 vid word-wrap">
              <?php echo $body; ?>
            </div>
          </div>
          <div class="col-sm-12">            
            Category : 
            <?php 
            $getcats = $newscat;
            $lastElement = end($getcats);
            foreach ($getcats as $key => $value) {
              ?>
              <span>
              <a href="/news/category/<?php echo $getcats[$key]->categoryslugs;?>"><?php echo $getcats[$key]->categoryname; if($value != $lastElement) {echo ",";}?></a>
              </span>
              <?php } ?>
            <br>
            Tags : 
            <?php 
            $getginfo = $newstags;
            $lastTags = end($getginfo);
            foreach ($getginfo as $key => $value) {
              ?>
              <span>
              <a href="/news/tags/<?php echo $getginfo[$key]->slugs;?>"><?php echo $getginfo[$key]->tags; if($value != $lastTags) {echo ",";}?></a>
              </span>
              <?php } ?>
              <br> 
            </div>             
                  
        <div class="col-sm-12">  
          <hr class="styled-hr">    
          <div class="row">
            <div class="col-sm-12">

              <h4>About the Author</h4>
            </div>
            <div class="col-sm-2">
              <a href="<?php echo $this->config->application->baseURL; ?>/news/author/<?php echo $authorid; ?>">
                <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/saveauthorimage/<?php echo $image; ?>" class="author-img">
              </a>
            </div>
            <div class="col-sm-9">
              <p class="padding-bot10">
                <a href="<?php echo $this->config->application->baseURL; ?>/news/author/<?php echo $authorid; ?>"><span class="authorname2"><?php echo $name; ?></span></a>
                <?php echo $about; ?>
              </p>
            </div>
          </div>   
          <hr class="styled-hr">        
        </div>
        <div class="col-sm-12">
          <div class="blog-social">
            <div class="blog-social-item blog-fb-like float-left">
              <div class="fb-like" data-href="<?php echo $this->config->application->baseURL; ?>/blog/view/<?php echo $newsslugs ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
              <div id="fb-root"></div>
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=217644384961866";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>


            </div>
            <div class="blog-social-item float-left margin-left">
              <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
              </script>
            </div>
            <div style="clear:both"></div>
            <div id="disqus_thread"></div>
            <script type="text/javascript">
              /* * * CONFIGURATION VARIABLES * * */
              var disqus_shortname = 'earthcitizensorganization';

              /* * * DON'T EDIT BELOW THIS LINE * * */
              (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>


          </div>
        </div>

      </div> 
    </div>

    <div class="col-sm-3">
    <hr class="margin-top0">
    <!-- <span class="title2">Featured Categories </span>
    <ul class="pad2">
      <li class="pad">
        <a href="<?php echo '/news/category/upcoming-programs' ?>">Upcoming Programs</a><br/>
      </li>
      <li class="pad">
        <a href="<?php echo '/news/category/recent-activites' ?>">Recent Activites</a><br/>
      </li>
      <li class="pad">
        <a href="<?php echo '/news/category/webinar-archive' ?>">Webinar Archive</a><br/>
      </li>
      <li class="pad">
        <a href="<?php echo '/news/category/mindful-living-tips' ?>">Mindful Living Tips</a><br/>
      </li>
    </ul>
    <hr> -->
      <span class="title2">Categories</span>
      <ul class="pad2">
          <li class="pad">
            <a href="<?php echo $this->config->application->baseURL; ?>/news">All</a><br/>
          </li>
         <?php
          foreach ($liftofcategory as $category ) {
            ?>
          <li class="pad">
            <a href="<?php echo '/news/category/'.$category->categoryslugs; ?>"><?php echo $category->categoryname; ?></a><br/>
          </li>
          <?php
            }

           ?>
        </ul>
        <hr>
        <span class="title2">Tags </span>
        <div class="listCont">
          <div class="tagsL">
            <?php
            $gettags = $newstagslist;
            $count = 1;
            foreach ($gettags as $key => $value) { ?>
            <a href="/news/tags/<?php echo $gettags[$key]->slugs; ?>" class="tagfont<?php echo $count; ?> tagfont"><?php echo $gettags[$key]->tags; ?></a>
            <?php
            $count > 5 ? $count =1 : $count++;
            } ?>
          </div>
        </div>
        <hr>
        <span class="title2">Archives</span>
        <ul class="pad2">
         <?php
            $getarchives = $archivelist;
            foreach ($getarchives as $key => $value) { ?>
            <li class="pad"><a href="/news/archive/<?php echo $getarchives[$key]->month . "/" . $getarchives[$key]->year; ?>"></span> <?php echo $getarchives[$key]->month . " " . $getarchives[$key]->year; ?></a></li>
            <?php } ?>
        </ul>
        <hr>
        <a href="<?php echo $this->config->application->baseURL; ?>/news/rss" target="_blank">
        <img src="/images/template_images/rss_feed.gif">
        RSS Feed
        </a>

    </div>

  </div>
  <hr class="styled-hr">  
</div>
</div>


