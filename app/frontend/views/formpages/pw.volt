<div style="height: 30px;"></div>
<div class="eco-container"> 
  <div class="row padding-bot30 padding-top30" id="circle-links">
    <div class="col-xs-2 cen">
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/project-funds">
        <img src="/images/template_images/eco-project-funds-thumb.jpg" class="sublink-img">
        <p class="title1">Project Funds</p>
      </a>
    </div>
    <div class="col-xs-4 cen">
      <a class="imglink" href="<?php echo $this->config->application->baseURL; ?>/simple-ways-to-give">
        <img src="/images/template_images/eco-ways-of-giving-thumb.jpg" class="sublink-img">
        <p class="title1">Simple Ways to Give</p>
      </a>
    </div>
    <div class="col-xs-2 cen">
    </div>
  </div>
</div>