<?php echo $this->getContent() ?>
<div class="container-fluid" ng-controller="donationProjCtrl">

  <div class="eco-container wrapper-proj" ng-if="data != null">
    <div class="row">
      <div class="col-md-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class='fa fa-arrow-right'></i></a> <a href>SERVE</a> <a><i class='fa fa-arrow-right'></i></a> <a href="{[{BASE_URL}]}/projects">ECO Project</a> <a><i class='fa fa-arrow-right'></i></a> <a href ng-bind="data.projTitle"></a>
      </div>
    </div>
  </div>

  <div class="eco-container wrapper-proj page-404" ng-if="data.projStatus == '5'">
    <div class="row">
      <div class="col-md-12">
        This Project was <span class="big-num">Completed</span>. <br>You can not donate to this project. Click <a href="{[{BASE_URL}]}/projects">here</a> to check other Projects.
      </div>
    </div>
  </div>
  <div class="eco-container wrapper-proj page-404" ng-if="data == null">
    <div class="row">
      <div class="col-md-12">
        The page you are trying to reach is broken. Click <a href="{[{BASE_URL}]}/projects">here</a> to go back to Projects.
      </div>
    </div>
  </div>
  <div class="eco-container wrapper-proj" ng-if="data.projStatus == '2'">
  <fieldset ng-disabled="isProcessing">
      <div class="row">
        <div class="col-md-7">
          <div style="margin-bottom:10px;">

            <div class="form-group row">
              <div class="col-md-12">
                <span class="donate-title1" style="color:#000000;">Payment Information</span><br/><br/>
              </div>
            </div>
            <div class="form-group row" >
              <div class="col-md-4">
                Payment Method:
              </div>
              <div class="col-md-8">
                <select name="typetransact2" class="form-control" ng-model="typetransact2" >
                  <option value=''>Credit Card</option>
                  <option value='ach2'>Checking/Savings Account</option>
                  <option value='paypal2'>PayPal</option>
                </select>
              </div>
            </div>

          </div>



          <!-- CREDIT CARD  BILLING FOR DONATION PART -->
          <div ng-hide="typetransact2 == 'ach2' || typetransact2 == 'paypal2'" class="creditcard">
            <br/>
            <form name="formCredit" id="formCredit" method="post" action="">



              <div class="form-group row" >
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                  <img src="/images/template_images/visa.png" style="width:65px;">
                  <img src="/images/template_images/amex.png" style="width:65px;">
                  <img src="/images/template_images/mastercard.png" style="width:65px;">
                  <img src="/images/template_images/discover.png" style="width:65px;">
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Credit Card Number: <label class="req">*</label>
                </div>
                <div class="col-md-8">
                  <input class="form-control" type="text" ng-model="cc.ccn" name="" required="required" only-digits />
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  CVV Number: <label class="req">*</label>
                </div>
                <div class="col-md-8">
                  <input type="text" class="form-control " ng-model="cc.cvvn" name="" required="required" only-digits />
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-8">

                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Credit Card Expiration: <label class="req">*</label>
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-xs-6">
                      <span>Month</span><br/>
                      <select ng-model="cc.expiremonth" class="form-control" required="required">
                        <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                        <?php foreach ($formonths as $index => $formonth) {
                          echo "<option value='".$index."'>".$formonth."</option>";
                        }?>
                      </select>
                    </div>
                    <div class="col-xs-6">
                      <span>Year</span><br/>
                      <select ng-model="cc.expireyear" class="form-control" required="required">
                        <?php for ($year=date('Y'); $year < 2050; $year++) {
                          echo "<option value='".$year."'>".$year."</option>";
                        }?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>



              <div class="form-group row" >
                <div class="col-md-12">
                  <!-- <span style="font-weight:bold;">Billing Information</span> -->
                  <span class="donate-title1" style="color:#000000;">Billing Information</span>                
                </div>
              </div>

              <div>
                <div class="form-group row" >
                  <div class="col-md-4">
                    First Name: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.billingfname" name="billingfname" required="required"/>
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-4">
                    Last Name: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.billinglname" name="billinglname" required="required"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    Address Line 1: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.al1" name="al1" required="required"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    Address Line 2:
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.al2" name="al2"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    City: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.city" name="city" required="required"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    State:
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.state" name="city"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    ZIP/Postal Code: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input class="form-control " type="text" ng-model="cc.zip" name="zip" required="required"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4">
                    Country: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <select ng-model="cc.country" ng-init="cc.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" required="required">
                    </select>
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-4">
                    Email Address: <label class="req">*</label>
                  </div>
                  <div class="col-md-8">
                    <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
                    <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                  </div>
                </div>
                <div class="form-group">
                  How did you learn about this Project?
                  <select class="form-control m-b" ng-model="cc.howdidyoulearn" ng-change="changeme()">
                    <option value="ECO event">ECO event</option>
                    <option value="ECO Program Graduates">ECO Program Graduates</option>
                    <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                    <option value="Invitation Email">Invitation Email</option>
                  </select>
                </div>
                <div class="form-group" ng-if="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                  Center Name
                  <select class="location form-control m-b" ng-model="cc.cname" required="cc.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                    <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"><span ng-bind="data.centername"></span></option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12 center" ng-show="isProcessing">

                    Processing your donation please wait...
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                <div class="col-md-12">
                  <span class="req" ng-bind="responseMSG"></span>
                </div>
                <div class="col-md-12">
                  <input type="button" name="" id="" value="Donate Now"  class="btn btn-success" ng-click="submitcredit(cc,user.email, 'projects', data.projID, data.projTitle, amount)" ng-disabled="formCredit.$invalid" style="float:right;"/>
                </div>
              </div>
            </form>
          </div>
          <!-- END FOR CREDIT CARD-->

          <!-- CHECK Authorize For Donate Part-->
          <div ng-show="typetransact2 == 'ach2' " class="ach" >
            <br>

            <form name="formAuthorizeCheck" id="formAuthorize" method="post" action="">

              <div class="form-group row" >
                <div class="col-md-4">
                  Account Holder Name: <label class="req">*</label>
                </div>
                <div class="col-md-8">
                  <!-- emailaddress -->
                  <input class="form-control " type="text" ng-model="check.accountname" name="accountname" required/>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Your bank name: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.bankname" name="bankname" required/>

                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-12">
                  Enter the routing code and account number as they appear on the bottom of your check:
                </div>
                <div class="col-md-12">
                  <img src="/images/template_images/checksample.png">
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Bank Routing Number: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.bankrouting" name="bankrouting" required only-digits/>

                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  Bank Account Number: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.bankaccountnumber" name="bankaccountnumber" required only-digits />

                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  Select Account Type: <label class="req">*</label>
                </div>
                <div class="col-md-8" ng-init="userecheckreg.at = 'CHECKING'">
                  <input type="radio" name="at" ng-model="check.at" value="CHECKING" required="required">&nbsp;&nbsp;Checking account<br/>
                  <input type="radio" name="at" ng-model="check.at" value="BUSINESSCHECKING" required="required">&nbsp;&nbsp;Business checking account<br/>
                  <input type="radio" name="at" ng-model="check.at" value="SAVINGS" required="required">&nbsp;&nbsp;Savings account
                </div>
              </div>

              <div class="form-group row" >
                <div class="col-md-12">

                  <input type="checkbox"  ng-model="check.autorz" name="autorz" value="1" ng-required="typetransact2 == 'ach2'" required><label class="req">*</label>

                  <span style="text-align: center;">
                    By entering my account number about and Clicking authorize, I authorize my payment to be processed
                    as an electronic funds transfer or draft drawn from my account. If the payment returned unpaid, I authorize you or your service
                    provider to collect the payment at my state's return item fee by electronic funds transfer(s) or draft(s) drawn from my account.
                    <a href="https://www.achex.com/html/NSF_pop.jsp" target="_blank">Click here to view your state's returned item fee.</a> If this payment is from a corporate account, I make these
                    authorizations as an authorized corporate representative and agree that the entity will be bound by the NACHA operating rules.
                  </span>
                </div>
              </div>

              <div class="form-group row" >
                <div class="col-md-12">
                  <span class="donate-title1" style="color:#000000;">Billing Information</span> 
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  First Name: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.billingfname" name="billingfname" required />

                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Last Name: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.billinglname" name="billinglname" required />

                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Address Line 1: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.al1" name="al1" required />

                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Address Line 2:
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="check.al2" name="al2" />
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  City: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.city" name="city" required />

                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  State:
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="check.state" name="city" />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  Province:
                </div>
                <div class="col-md-8">
                  <input class="form-control " type="text" ng-model="check.province" name="province" />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  ZIP/Postal Code: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <input class="form-control " type="text" ng-model="check.zip" name="zip" required />

                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  Country: <label class="req">*</label>
                </div>
                <div class="col-md-8">

                  <select ng-model="check.country" ng-init="check.country = countries[229]" class="location form-control m-b" ng-options="cn.name for cn in countries" ng-required="typetransact2 == 'ach2'" required="">

                  </select>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-4">
                  Email Address: <label class="req">*</label>
                </div>
                <div class="col-md-8">
                  <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="emailcheck(user)" required/>
                  <span class="formerror" ng-if="invalidemail == true" style="font-size: 12px;color: #fd5555">This is not a valid email address.<br/>Example: myname@example.com</span>
                </div>
              </div>

              <div class="form-group">
                How did you learn about this Project?
                <select class="form-control m-b" ng-model="check.howdidyoulearn" ng-change="changeme()">
                  <option value="ECO event">ECO event</option>
                  <option value="ECO Program Graduates">ECO Program Graduates</option>
                  <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                  <option value="Invitation Email">Invitation Email</option>
                </select>
              </div>
              <div class="form-group" ng-if="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                Center Name
                <select class="location form-control m-b" ng-model="check.cname" required="check.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                  <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername">
                    <span ng-bind="data.centername"></span>
                  </option>
                </select>
              </div>

                <div class="col-md-12 center" ng-show="isProcessing">

                    Processing your donation please wait...
                  <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                  </div>
                </div>
                <div class="col-md-12">
                  <span class="req" ng-bind="responseMSG"></span>
                </div>

              <div class="form-group row" style="padding-left:15px;">
                <div class="col-md-12">
                  <input type="button" name="" id="abouteco" value="Donate Now" ng-click="submitecheck(check,user.email,'projects', data.projID, data.projTitle, amount)" class="btn btn-success" ng-disabled="formAuthorizeCheck.$invalid" style="float:right;"/>
                </div>

              </div>


            </form>
          </div>
          <!-- END of CHECK Authorize For Donate Part-->


          <a id="causes"></a>
          <!-- Paypal Form Payment -->


          <div  ng-show="typetransact2 == 'paypal2' ">
            <br>
            <form class="paypalform" name="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypalsingle">

              <div style="clear:both; ">
                <input type="hidden" name="cmd" value="_donations">
                <input type="hidden" name="business" value="accounting@earthcitizens.org">
                <input type="hidden" name="lc" value="US">
                <input type="hidden" name="custom" id="paypalcustom">
                <input type="hidden" name="item_name" ng-value="data.projTitle">
                <input type="hidden" name="amount" ng-value="amount">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="no_note" value="0">
                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
              </div>
              <div class="form-group">
                How did you learn about this Project?
                <select class="form-control m-b" ng-model="user.howdidyoulearn" ng-change="changeme()">
                  <option value="ECO event">ECO event</option>
                  <option value="ECO Program Graduates">ECO Program Graduates</option>
                  <option value="Body & Brain or Dahn Yoga Centers">Body & Brain or Dahn Yoga Centers</option>
                  <option value="Invitation Email">Invitation Email</option>
                </select>
              </div>
              <div class="form-group" ng-if="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'">
                Center Name
                <select class="location form-control m-b" ng-model="user.cname" required="user.howdidyoulearn == 'Body & Brain or Dahn Yoga Centers'" >
                  <option value="{[{ data.centername }]}" ng-repeat="data in dataCentername"><span ng-bind="data.centername"></span></option>
                </select>
              </div>

              <a href="" ng-click="submitpaypal(user.howdidyoulearn, user.cname, data.projID)">
                <img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="paypalbutton" alt="PayPal - The safer, easier way to pay online!" style="float:right;">
              </a>

            </form>


          </div>

          <!-- END of Paypal -->
        </div>
        <div class="col-md-5 margin-top40">

          <div class="row">
            <div class="col-md-12 center">
              <h3>You will donate</h3>
              <div class="proj-donate w1 align-auto">
                <div class="form-group center">
                  <div class="input-group m-b">
                    <span class="input-group-addon h big-num">$</span>
                    <input type="text" class="form-control h big-num" ng-model="amount" only-digits>
                    <span class="input-group-addon h big-num">.00</span>
                  </div>
                </div>
              </div>
              <h3>to</h3>
            </div>
            <div class="col-md-12">
              <p>
                <b><span ng-bind="data.projTitle"></span></b>
              </p>
              <img class="view-proj-thumb" src="{[{amazonlink}]}/uploads/projects/{[{data.projThumb}]}">
              <p></p>
              <p ng-bind="data.projShortDesc"></p>
              <p class="capitalize">by <a href=""><span ng-bind="data.firstname"></span> <span ng-bind="data.lastname"></span></a></p>
            </div>
          </div>
        </div>
      </div>

      <a id="causes"></a>
    </fieldset>
  </div>
</div>