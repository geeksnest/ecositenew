<div class="container-fluid mybanner">
    <div id="interior" class="bgatw" style="background: url('https://earthcitizens.s3.amazonaws.com/uploads/pageimages/background-what-is-eco.jpg') 0px 0px;">
      <div class="eco-container">
        <span class="orgtitle"><?php echo $orgname; ?></span>
      </div>
    </div>
  </div>
  <div class="container-fluid mybanner">
    <div id="interior" class="atwbanger" style="background: url('{[{amazonlink}]}/uploads/atw/<?php echo $banner ?>') 0px 0px;background-size: cover;">
      <div class="eco-container">
        <span class="atwbangerdesc">
        <?php echo $news; ?>
        </span>
      </div>
    </div>
  </div>
  <div class="container-fluid marginbot">
    <div class="eco-container atwcontent">
      <div class="row">
        <div class="col-sm-3">
          <img class="atwflag" src="https://earthcitizens.s3.amazonaws.com/uploads/banner/flags/kr.png">
        </div>
        <div class="col-sm-9 atwtitalign">
          <span class="atwtit">Earth Citizens Organization, <?php echo $orgname ?>, Since <?php echo $syear; ?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 full_w">
          <ul class="nav nav-tabs responsive" style="margin-top: 31px;">
            <li class="active litab"><a class="tabrad" data-toggle="tab" href="#PROFILE">PROFILE</a></li>
            <li class="litab"><a class="tabrad" data-toggle="tab" href="#ANNOUNCEMENT">ANNOUNCEMENT</a></li>
            <li class="litab"><a class="tabrad" data-toggle="tab" href="#PHOTOS">PHOTOS</a></li>
          </ul>
          <div class="tab-content responsive">
            <div id="PROFILE" class="tab-pane fade in active">
              <div class="tabmargn">
                <div class="row padatw">
                  <div class="col-sm-4" bold>Address</div>
                  <div class="col-sm-8 atwboxtextalign"><?php echo $address; ?></div>
                </div>
                <div class="row padatw"><hr></div>
                <div class="row padatw">
                  <div class="col-sm-4" bold>Mission</div>
                  <div class="col-sm-8 atwboxtextalign"><?php echo $mission; ?></div>
                </div>
                <div class="row padatw"><hr></div>
                <div class="row padatw">
                  <div class="col-sm-4" bold>Activities</div>
                  <div class="col-sm-8 atwboxtextalign"><?php echo $activities; ?></div>
                  <hr>
                </div>
              </div>
            </div>
            <div id="ANNOUNCEMENT" class="tab-pane fade">
              <h3>ANNOUNCEMENT</h3>
              <p>COMING SOON</p>
            </div>
            <div id="PHOTOS" class="tab-pane fade">
              <h3>PHOTOS</h3>
              <p>COMING SOON</p>
            </div>
          </div>
        </div>
        <div class="col-sm-4 full_w">
            <div class="atwstatbox" style="background:#FF9D0F">
              <div class="row">
                <div class="atwstattitle">ECO ACTIVITY IN <?php echo $orgname; ?></div>
              </div>

              <div class="row">
                <p class="atwboxtextalign">
                  <img class="atwstats" src="https://earthcitizens.s3.amazonaws.com/uploads/toppage/icon-white-earth-citizens.png" alt="Earth Citizens Registered"> 
                  <span class="countfontsz FFFFFF">1465</span><br>
                  <span class="countdescsz FFFFFF smsize">Earth Citizens Registered</span>
                </p>
              </div>

              <div class="row">
                <p class="atwboxtextalign">
                  <img class="atwstats" src="https://earthcitizens.s3.amazonaws.com/uploads/toppage/icon-white-heroes-trained.png" alt="Heroes Trained"> 
                  <span class="countfontsz FFFFFF">590</span><br>
                  <span class="countdescsz FFFFFF smsize">Heroes Trained</span>
                </p>
              </div>

              <div class="row">
                <p class="atwboxtextalign">
                  <img class="atwstats" src="https://earthcitizens.s3.amazonaws.com/uploads/toppage/icon-white-community-hours-served.png" alt="Community Hours Served"> 
                  <span class="countfontsz FFFFFF">1770</span><br>
                  <span class="countdescsz FFFFFF smsize">Community Hours Served</span>
                </p>
              </div>

            </div>
        </div>
      </div>

    </div>
  </div>