
<div class="eco-container wrapper-proj">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?php echo $this->config->application->baseURL; ?>">ECO</a> <a><i class="fa fa-arrow-right"></i></a> <a href=""><?php echo $title ?></a> 
    </div>
</div>
</div>


<?php echo $this->getContent() ?>

<div class="row" ng-controller="HeroesOnDutyCtrl">
    <div class="col-sm-12">
        <form name="form" class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="saved(user)">
          <div class="panel panel-default">

              <div class="panel-body">
                  <div class="form-group">
                      <label>Name</label>
                      <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.name"  required="">
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" ng-model="user.email" required="">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" placeholder="(XXX) XXXX XXX" ng-model="user.phone" ng-pattern="/\([0-9]{3}\) ([0-9]{3}) ([0-9]{3})$/" required="">
                </div>
                <div class="form-group col-sm-6" style="margin-top: 10px;">
                    <div style="padding-bottom:10px;">Date of birth: </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span class="datelabel">Year:</span>
                            <select ng-model="user.byear" class="form-control ng-pristine ng-invalid ng-invalid-required" required="">
                                <?php for ($year= 1960 ; $year < date('Y'); $year++) { 
                                  echo "<option value='".$year."'>".$year."</option>";
                              }?>
                          </select>
                      </div>
                      <div class="col-md-9">
                        <div class="col-xs-8" style="padding-left:0px;">
                            <span class="datelabel">Month: </span>
                            <select  ng-model="user.bmonth" class="form-control ng-pristine ng-invalid ng-invalid-required" required="">
                                <!-- <option ng-repeat="month in donmonth track by $index" value="{[{month.val}]}">{[{month.name}]}</option> -->
                                <?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                                <?php foreach ($formonths as $index => $formonth) { 
                                  echo "<option value='".$index."'>".$formonth."</option>";
                              }?>
                          </select>
                      </div>
                      <div class="col-xs-4" style="padding-right:0px;">
                        <span class="datelabel">Day:  </span>
                        <select ng-model="user.year" class="form-control ng-pristine ng-invalid ng-invalid-required" required="">
                            <?php for ($day= 1 ; $day < 31; $day++) { 

                                if(strlen($day)==1){
                                    $_day = trim("0".$day);
                                }else{
                                    $_day = $day;
                                }
                                echo "<option value='".$_day."'>".$_day."</option>";
                            }?>
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <div class="form-group">
          <label>When did you take the HEROES leadership training?</label>
          <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.leadtrain"  required="">
      </div>
      <div class="form-group">
          <label>Your skills or expertise</label>
          <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.skill"  required="">
      </div>
      <div class="form-group">
          <label>How many hours can you service for a week?</label>
          <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.service"  required="">
      </div>
      <div class="form-group">
          <label>For how many weeks can you be committed?</label>
          <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.commit"  required="">
      </div>
  </div>
  <footer class="panel-footer text-right bg-light lter">
      <button type="submit" class="btn btn-success" ng-disabled="form.$invalid" disabled="disabled">Submit</button>
  </footer>
</div>
</form>
</div>

</div>